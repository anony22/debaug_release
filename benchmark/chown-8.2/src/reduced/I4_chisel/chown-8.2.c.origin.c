typedef unsigned long size_t;
typedef long __off_t;
typedef long __off64_t;
struct _IO_FILE;
struct _IO_FILE;
struct _IO_FILE;
typedef struct _IO_FILE FILE;
typedef void _IO_lock_t;
struct _IO_marker {
  struct _IO_marker *_next;
  struct _IO_FILE *_sbuf;
  int _pos;
};
struct _IO_FILE {
  int _flags;
  char *_IO_read_ptr;
  char *_IO_read_end;
  char *_IO_read_base;
  char *_IO_write_base;
  char *_IO_write_ptr;
  char *_IO_write_end;
  char *_IO_buf_base;
  char *_IO_buf_end;
  char *_IO_save_base;
  char *_IO_backup_base;
  char *_IO_save_end;
  struct _IO_marker *_markers;
  struct _IO_FILE *_chain;
  int _fileno;
  int _flags2;
  __off_t _old_offset;
  unsigned short _cur_column;
  signed char _vtable_offset;
  char _shortbuf[1];
  _IO_lock_t *_lock;
  __off64_t _offset;
  void *__pad1;
  void *__pad2;
  void *__pad3;
  void *__pad4;
  size_t __pad5;
  int _mode;
  char _unused2[(15UL * sizeof(int) - 4UL * sizeof(void *)) - sizeof(size_t)];
};
typedef long __time_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __syscall_slong_t;
typedef __mode_t mode_t;
struct timespec {
  __time_t tv_sec;
  __syscall_slong_t tv_nsec;
};
struct stat {
  __dev_t st_dev;
  __ino_t st_ino;
  __nlink_t st_nlink;
  __mode_t st_mode;
  __uid_t st_uid;
  __gid_t st_gid;
  int __pad0;
  __dev_t st_rdev;
  __off_t st_size;
  __blksize_t st_blksize;
  __blkcnt_t st_blocks;
  struct timespec st_atim;
  struct timespec st_mtim;
  struct timespec st_ctim;
  __syscall_slong_t __glibc_reserved[3];
};
typedef __ino_t ino_t;
typedef __dev_t dev_t;
struct hash_table;
struct hash_table;
struct hash_table;
typedef struct hash_table Hash_table;
struct F_triple {
  char *name;
  ino_t st_ino;
  dev_t st_dev;
};
struct __dirstream;
struct __dirstream;
struct __dirstream;
typedef struct __dirstream DIR;
typedef int wchar_t;
union __anonunion___value_4 {
  unsigned int __wch;
  char __wchb[4];
};
struct __anonstruct___mbstate_t_3 {
  int __count;
  union __anonunion___value_4 __value;
};
typedef struct __anonstruct___mbstate_t_3 __mbstate_t;
typedef unsigned int wint_t;
struct hash_tuning {
  float shrink_threshold;
  float shrink_factor;
  float growth_threshold;
  float growth_factor;
  _Bool is_n_buckets;
};
typedef struct hash_tuning Hash_tuning;
typedef __mbstate_t mbstate_t;
struct mbchar {
  char const *ptr;
  size_t bytes;
  _Bool wc_valid;
  wchar_t wc;
  char buf[24];
};
struct mbuiter_multi {
  _Bool in_shift;
  mbstate_t state;
  _Bool next_done;
  struct mbchar cur;
};
typedef struct mbuiter_multi mbui_iterator_t;
typedef __gid_t gid_t;
typedef __uid_t uid_t;
typedef unsigned long uintmax_t;
struct dev_ino {
  ino_t st_ino;
  dev_t st_dev;
};
struct cycle_check_state {
  struct dev_ino dev_ino;
  uintmax_t chdir_counter;
  int magic;
};
typedef long ptrdiff_t;
struct dirent {
  __ino_t d_ino;
  __off_t d_off;
  unsigned short d_reclen;
  unsigned char d_type;
  char d_name[256];
};
typedef __builtin_va_list __gnuc_va_list;
typedef __gnuc_va_list va_list;
enum quoting_style {
  literal_quoting_style = 0,
  shell_quoting_style = 1,
  shell_always_quoting_style = 2,
  c_quoting_style = 3,
  c_maybe_quoting_style = 4,
  escape_quoting_style = 5,
  locale_quoting_style = 6,
  clocale_quoting_style = 7,
  custom_quoting_style = 8
};
enum strtol_error {
  LONGINT_OK = 0,
  LONGINT_OVERFLOW = 1,
  LONGINT_INVALID_SUFFIX_CHAR = 2,
  LONGINT_INVALID_SUFFIX_CHAR_WITH_OVERFLOW = 3,
  LONGINT_INVALID = 4
};
typedef enum strtol_error strtol_error;
struct option {
  char const *name;
  int has_arg;
  int *flag;
  int val;
};
typedef long intmax_t;
typedef __nlink_t nlink_t;
struct I_ring {
  int ir_data[4];
  int ir_default_val;
  unsigned int ir_front;
  unsigned int ir_back;
  _Bool ir_empty;
};
typedef struct I_ring I_ring;
struct _ftsent;
struct _ftsent;
struct _ftsent;
union __anonunion_fts_cycle_29 {
  struct hash_table *ht;
  struct cycle_check_state *state;
};
struct __anonstruct_FTS_28 {
  struct _ftsent *fts_cur;
  struct _ftsent *fts_child;
  struct _ftsent **fts_array;
  dev_t fts_dev;
  char *fts_path;
  int fts_rfd;
  int fts_cwd_fd;
  size_t fts_pathlen;
  size_t fts_nitems;
  int (*fts_compar)(struct _ftsent const **, struct _ftsent const **);
  int fts_options;
  struct hash_table *fts_leaf_optimization_works_ht;
  union __anonunion_fts_cycle_29 fts_cycle;
  I_ring fts_fd_ring;
};
typedef struct __anonstruct_FTS_28 FTS;
struct _ftsent {
  struct _ftsent *fts_cycle;
  struct _ftsent *fts_parent;
  struct _ftsent *fts_link;
  long fts_number;
  void *fts_pointer;
  char *fts_accpath;
  char *fts_path;
  int fts_errno;
  int fts_symfd;
  size_t fts_pathlen;
  FTS *fts_fts;
  ptrdiff_t fts_level;
  size_t fts_namelen;
  nlink_t fts_n_dirs_remaining;
  unsigned short fts_info;
  unsigned short fts_flags;
  unsigned short fts_instr;
  struct stat fts_statp[1];
  char fts_name[1];
};
typedef struct _ftsent FTSENT;
struct passwd {
  char *pw_name;
  char *pw_passwd;
  __uid_t pw_uid;
  __gid_t pw_gid;
  char *pw_gecos;
  char *pw_dir;
  char *pw_shell;
};
struct group {
  char *gr_name;
  char *gr_passwd;
  __gid_t gr_gid;
  char **gr_mem;
};
typedef unsigned long reg_syntax_t;
struct quoting_options;
struct quoting_options;
struct quoting_options;
struct quoting_options {
  enum quoting_style style;
  int flags;
  unsigned int quote_these_too[255UL / (sizeof(int) * 8UL) + 1UL];
  char const *left_quote;
  char const *right_quote;
};
struct slotvec {
  size_t size;
  char *val;
};
struct hash_entry {
  void *data;
  struct hash_entry *next;
};
struct hash_table {
  struct hash_entry *bucket;
  struct hash_entry const *bucket_limit;
  size_t n_buckets;
  size_t n_buckets_used;
  size_t n_entries;
  Hash_tuning const *tuning;
  size_t (*hasher)(void const *, size_t);
  _Bool (*comparator)(void const *, void const *);
  void (*data_freer)(void *);
  struct hash_entry *free_entry_list;
};
struct __anonstruct___fsid_t_1 {
  int __val[2];
};
typedef struct __anonstruct___fsid_t_1 __fsid_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsfilcnt_t;
typedef long __fsword_t;
struct Active_dir {
  dev_t dev;
  ino_t ino;
  FTSENT *fts_ent;
};
struct statfs {
  __fsword_t f_type;
  __fsword_t f_bsize;
  __fsblkcnt_t f_blocks;
  __fsblkcnt_t f_bfree;
  __fsblkcnt_t f_bavail;
  __fsfilcnt_t f_files;
  __fsfilcnt_t f_ffree;
  __fsid_t f_fsid;
  __fsword_t f_namelen;
  __fsword_t f_frsize;
  __fsword_t f_flags;
  __fsword_t f_spare[4];
};
struct LCO_ent {
  dev_t st_dev;
  _Bool opt_ok;
};
enum Change_status {
  CH_NOT_APPLIED = 1,
  CH_SUCCEEDED = 2,
  CH_FAILED = 3,
  CH_NO_CHANGE_REQUESTED = 4
};
enum Verbosity {
  V_high = 0, V_changes_only = 1, V_off = 2
};
struct Chown_option {
  enum Verbosity verbosity;
  _Bool recurse;
  struct dev_ino *root_dev_ino;
  _Bool affect_symlink_referent;
  _Bool force_silent;
  char *user_name;
  char *group_name;
};
enum RCH_status {
  RC_ok = 2,
  RC_excluded = 3,
  RC_inode_changed = 4,
  RC_do_ordinary_chown = 5,
  RC_error = 6
};
extern __attribute__((__nothrow__)) int *(
    __attribute__((__leaf__)) __errno_location)(void)__attribute__((__const__));
extern int close(int __fd);
extern int(__attribute__((__nonnull__(1))) open)(char const *__file,
                                                 int __oflag, ...);
extern __attribute__((__nothrow__)) int(__attribute__((__leaf__))
                                        tolower)(int __c);
extern __attribute__((__nothrow__))
size_t(__attribute__((__nonnull__(1), __leaf__)) strlen)(char const *__s)
    __attribute__((__pure__));
extern int fclose(FILE *__stream);
int dup_safer(int fd);
extern __attribute__((__nothrow__)) int(
    __attribute__((__nonnull__(1, 2), __leaf__))
    strcmp)(char const *__s1, char const *__s2) __attribute__((__pure__));
extern __attribute__((__nothrow__)) int(
    __attribute__((__nonnull__(1, 2), __leaf__))
    strncmp)(char const *__s1, char const *__s2, size_t __n)
    __attribute__((__pure__));
__attribute__((__noreturn__)) void xalloc_die(void);
extern __attribute__((__nothrow__)) void *(__attribute__((__leaf__))
                                           malloc)(size_t __size)
    __attribute__((__malloc__));
char *last_component(char const *name);
extern __attribute__((__nothrow__)) char *(__attribute__((__leaf__))
                                           gettext)(char const *__msgid)
    __attribute__((__format_arg__(1)));
void *hash_lookup(Hash_table const *table___0, void const *entry);
void *(__attribute__((__warn_unused_result__))
       hash_insert)(Hash_table *table___0, void const *entry);
void triple_free(void *x);
void *xmalloc(size_t n) __attribute__((__malloc__));
char *xstrdup(char const *string) __attribute__((__malloc__));
extern DIR *fdopendir(int __fd);
DIR *rpl_fdopendir(int fd);
extern
    __attribute__((__nothrow__)) int(__attribute__((__nonnull__(2), __leaf__))
                                     fstat)(int __fd, struct stat *__buf);
DIR *rpl_fdopendir(int fd) {
  struct stat st;
  int tmp;
  int *tmp___0;
  DIR *tmp___1;

  {
                         
              
                                  
     
                                             
                                   
                    
                                  
     
    tmp___1 = fdopendir(fd);
    return (tmp___1);
  }
}
int fd_safer(int fd);
int fd_safer(int fd) {
  int f;
  int tmp;
  int e;
  int *tmp___0;
  int *tmp___1;

  {
                  
                    
                            
                
                                     
                     
                  
                                     
                     
               
       
     
    return (fd);
  }
}
                          
int volatile exit_failure = (int volatile)1;
extern __attribute__((__nothrow__)) unsigned short const **(
    __attribute__((__leaf__)) __ctype_b_loc)(void)__attribute__((__const__));
extern __attribute__((__nothrow__)) int(__attribute__((__leaf__))
                                        ferror_unlocked)(FILE *__stream);
extern __attribute__((__nothrow__))
size_t(__attribute__((__leaf__)) __ctype_get_mb_cur_max)(void);
extern __attribute__((__nothrow__)) void(__attribute__((__leaf__))
                                         free)(void *__ptr);
extern __attribute__((__nothrow__, __noreturn__)) void(__attribute__((__leaf__))
                                                       abort)(void);
extern
    __attribute__((__nothrow__)) void *(__attribute__((__nonnull__(1),
                                                       __leaf__))
                                        memset)(void *__s, int __c, size_t __n);
extern __attribute__((__nothrow__)) char *(__attribute__((__nonnull__(1),
                                                          __leaf__))
                                           strchr)(char const *__s, int __c)
    __attribute__((__pure__));
extern __attribute__((__nothrow__)) char *(__attribute__((__nonnull__(1),
                                                          __leaf__))
                                           strrchr)(char const *__s, int __c)
    __attribute__((__pure__));
int mbscasecmp(char const *s1, char const *s2);
extern __attribute__((__nothrow__))
wint_t(__attribute__((__leaf__)) towlower)(wint_t __wc);
size_t hash_string(char const *string, size_t n_buckets);
Hash_table *(__attribute__((__warn_unused_result__))
             hash_initialize)(size_t candidate, Hash_tuning const *tuning,
                              size_t (*hasher)(void const *, size_t),
                              _Bool (*comparator)(void const *, void const *),
                              void (*data_freer)(void *));
void hash_free(Hash_table *table___0);
extern __attribute__((__nothrow__, __noreturn__)) void(__attribute__((
    __leaf__)) __assert_fail)(char const *__assertion, char const *__file,
                              unsigned int __line, char const *__function);
extern __attribute__((__nothrow__)) int(__attribute__((__leaf__))
                                        mbsinit)(mbstate_t const *__ps)
    __attribute__((__pure__));
extern __attribute__((__nothrow__))
size_t(__attribute__((__leaf__))
       mbrtowc)(wchar_t *__restrict __pwc, char const *__restrict __s,
                size_t __n, mbstate_t *__restrict __p);
unsigned int const is_basic_table[8];
__inline static _Bool is_basic(char c) {

   

    return ((_Bool)((is_basic_table[(int)((unsigned char)c) >> 5] >>
                     ((int)((unsigned char)c) & 31)) &
                    1U));
   

}
size_t strnlen1(char const *string, size_t maxlen);
__inline static void mbuiter_multi_next(struct mbuiter_multi *iter) {
  int tmp;
  size_t tmp___0;
  size_t tmp___1;
  int tmp___2;
  _Bool tmp___3;

   
                          
             
     
                         
                      
     
                                               
                  
                                  
                                                 
                                    
            
                                                       
                 
                                               
                                                                     
                                                    
                                                  
       
                                
               
                                         
                                                 
                       
                                                                       
                                                    
                                    
                                      
              
                                                      
                                                  
                                        
                
                                       
                                        
                                                        
                                                        
                                                                           
                                                          
                                                        
             
                                       
                                                
                                                                           
                                                          
                                                        
             
           
                                        
                                                               
                        
                                      
           
         
       
     
                               
           
   
}
void *xrealloc(void *p, size_t n);
extern int fcntl(int __fd, int __cmd, ...);
int dup_safer(int fd) {
  int tmp;

  {
    tmp = fcntl(fd, 0, 3);
    return (tmp);
  }
}
extern __attribute__((__nothrow__)) void *(
    __attribute__((__nonnull__(1, 2), __leaf__))
    memcpy)(void *__restrict __dest, void const *__restrict __src, size_t __n);
extern __attribute__((__nothrow__)) int(
    __attribute__((__nonnull__(1, 2), __leaf__))
    stat)(char const *__restrict __file, struct stat *__restrict __buf);
extern __attribute__((__nothrow__)) int(__attribute__((__leaf__))
                                        fchown)(int __fd, __uid_t __owner,
                                                __gid_t __group);
                             
                             
                                
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                       
                                                                       
                                                                       
                                                                       
                                                                       
                                                                       
                                                                     
                                                                     
                                                                       
                                                                       
                                                                       
                                                                       
                                                                       
                                                                       
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                       
                                                                       
                                                                       
                                                                       
                                                                       
                                                                     
                                                                       
                                                                     
                                                                       
                                                                       
                                                                       
                                                                       
                                                                       
                                                                     
                                                                       
                                                                        
                                
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                   
void cycle_check_init(struct cycle_check_state *state);
_Bool cycle_check(struct cycle_check_state *state, struct stat const *sb);
__inline static _Bool is_zero_or_power_of_two(uintmax_t i) {

   

    return ((_Bool)((i & (i - 1UL)) == 0UL));
   

}
void cycle_check_init(struct cycle_check_state *state) {

   

                                        
                           
           
   

}
_Bool cycle_check(struct cycle_check_state *state, struct stat const *sb) {
  _Bool tmp;

  {
                                     
                                              
                                                                   
                                                      
                                        
     
                               
                                                               
                                                                 
                            
         
       
     
                             
                                                        
              
                                        
                          
       
                                                
                                                
     
    return ((_Bool)0);
  }
}
extern void error(int __status, int __errnum, char const *__format, ...);
char const *quote(char const *name);
void close_stdout(void);
extern struct _IO_FILE *stdout;
extern struct _IO_FILE *stderr;
extern __attribute__((__noreturn__)) void _exit(int __status);
int close_stream(FILE *stream);
char *quotearg_colon(char const *arg);
static char const *file_name;
static _Bool ignore_EPIPE;
void close_stdout(void) {
  char const *write_error;
  char const *tmp;
  char *tmp___0;
  int *tmp___1;
  int *tmp___2;
  int tmp___3;
  int *tmp___4;
  int tmp___5;

   
                                   
                       
                         
                                     
                                
                  
         
              
         
                                                   
                          
                        
                                              
                                       
                                                             
                
                                       
                                                
         
                                 
       
     
                                   
                       
                               
     
           
   
}
extern __attribute__((__nothrow__))
size_t(__attribute__((__leaf__)) __fpending)(FILE *__fp);
int close_stream(FILE *stream) {
  _Bool some_pending;
  size_t tmp;
  _Bool prev_fail;
  int tmp___0;
  _Bool fclose_fail;
  int tmp___1;
  int *tmp___2;
  int *tmp___3;

  {
                             
                                       
                                      
                                      
                             
                                        
                    
                  
            
                        
                           
                      
                
                                       
                              
                 
                               
                                           
                           
             
                        
           
         
       
     
    return (0);
  }
}
int set_cloexec_flag(int desc, _Bool value);
int set_cloexec_flag(int desc, _Bool value) {
  int flags;
  int tmp;
  int newflags;
  int tmp___0;
  int tmp___1;

  {
                            
                
                     
                  
                            
              
                             
       
                         
                              
                   
              
                                           
                            
                     
         
       
     
    return (-1);
  }
}
extern __attribute__((__nothrow__)) int(__attribute__((__leaf__))
                                        fchdir)(int __fd);
extern int(__attribute__((__nonnull__(2))) openat)(int __fd, char const *__file,
                                                   int __oflag, ...);
extern __attribute__((__nothrow__)) void *(
    __attribute__((__nonnull__(1), __leaf__))
    memchr)(void const *__s, int __c, size_t __n) __attribute__((__pure__));
extern __attribute__((__nothrow__)) void *(
    __attribute__((__nonnull__(1, 2), __leaf__))
    memmove)(void *__dest, void const *__src, size_t __n);
extern __attribute__((__nothrow__)) int(
    __attribute__((__nonnull__(1, 2), __leaf__))
    lstat)(char const *__restrict __file, struct stat *__restrict __buf);
size_t triple_hash(void const *x, size_t table_size);
_Bool triple_compare_ino_str(void const *x, void const *y);
char *last_component(char const *name) {
  char const *base;
  char const *p;
  _Bool saw_slash;

  {
                    
                         
               

                                        
                         
       
             
     
              
             
              {

                
                             
       
                                 {
        saw_slash = (_Bool)1;
      }       
                        
                   
                               
         
       
      p++;
    }
  while_break___0:;
    return ((char *)base);
  }
}
                                 
                           
extern __attribute__((__nothrow__)) int(
    __attribute__((__nonnull__(1, 2), __leaf__))
    memcmp)(void const *__s1, void const *__s2, size_t __n)
    __attribute__((__pure__));
extern int(__attribute__((__nonnull__(1))) closedir)(DIR *__dirp);
extern struct dirent *(__attribute__((__nonnull__(1))) readdir)(DIR *__dirp);
DIR *opendir_safer(char const *name);
                                       
extern int fprintf(FILE *__restrict __stream, char const *__restrict __format,
                   ...);
char *quotearg_n_style(int n, enum quoting_style s, char const *arg);
char const *quote_n(int n, char const *name);
__attribute__((__noreturn__)) void usage(int status);
extern
    __attribute__((__nothrow__)) void *(__attribute__((__warn_unused_result__,
                                                       __leaf__))
                                        realloc)(void *__ptr, size_t __size);
__inline static void *xnmalloc(size_t n, size_t s) __attribute__((__malloc__));
__inline static void *xnmalloc(size_t n, size_t s) __attribute__((__malloc__));
__inline static void *xnmalloc(size_t n, size_t s) {
  int tmp;
  void *tmp___0;

  {
                                              
               
            
               
     
                              
                   
     
    tmp___0 = xmalloc(n * s);
    return (tmp___0);
  }
}
strtol_error xstrtoul(char const *s, char **ptr, int strtol_base,
                      unsigned long *val, char const *valid_suffixes);
extern __attribute__((__nothrow__)) unsigned long(__attribute__((
    __nonnull__(1), __leaf__)) strtoul)(char const *__restrict __nptr,
                                        char **__restrict __endptr, int __base);
static strtol_error bkm_scale___0(unsigned long *x, int scale_factor) {

   

                                                                  
                                
                               
     
                                      
    return ((strtol_error)0);
   

}
static strtol_error bkm_scale_by_power___0(unsigned long *x, int base,
                                           int power) {
  strtol_error err;
  strtol_error tmp;
  int tmp___0;

  {
                          
              {
                      
              
                     
                         
       
      tmp = bkm_scale___0(x, base);
      err = (strtol_error)((unsigned int)err | (unsigned int)tmp);
    }
  while_break:;
    return (err);
  }
}
strtol_error xstrtoul(char const *s, char **ptr, int strtol_base,
                      unsigned long *val, char const *valid_suffixes) {
  char *t_ptr;
  char **p;
  unsigned long tmp;
  strtol_error err;
  char const *q;
  unsigned char ch;
  unsigned short const **tmp___0;
  int *tmp___1;
  char *tmp___2;
  int *tmp___3;
  int *tmp___4;
  int base;
  int suffixes;
  strtol_error overflow;
  char *tmp___5;
  char *tmp___6;

  {
                          
                           
                                 
                                                              
                                                                     
                                                    
                                       
       
            
                                                            
                                                                   
                                                  
                                     
     
              
              
            
                 
     
          
                           
               
                                
                                                         
                         
       
          
                             
     
               
                        
                               
     
                                 
                 
                                     
                                                
                           
                    
                                                       
                        
                      
                  
                                     
           
                
                                   
         
              
                                 
       
            
                                   
                          
                                     
                             
                                   
         
                              
       
     
                          
                 
                   
     
                         {
                  
                   
                                                   
                     
                   
                                                        
       
                                            
                   {
                                          
                        
         
                                         
                       
         
                                         
                       
         
                          
               
                                         
                        
         
                          
              
                    
        suffixes++;
                          
                    
      }
                             
                     
       
                             
                         
       
                             
                     
       
                             
                     
       
                             
                     
       
                              
                     
       
                              
                      
       
                             
                      
       
                             
                     
       
                              
                     
       
                             
                     
       
                             
                     
       
                              
                     
       
                              
                      
       
                             
                     
       
                             
                     
       
                          
            
                                          
                            
                
                                           
                            
            
                                 
                            
            
                                                       
                            
            
                                                       
                            
             
                                                       
                            
            
                                                       
                            
            
                                                       
                            
            
                                                       
                            
             
                                        
                            
            
                                                       
                            
            
                                                       
                            
                   
                 
      return ((strtol_error)((unsigned int)err | 2U));
                     
                                                                       
      *p += suffixes;
                 {
        err = (strtol_error)((unsigned int)err | 2U);
      }
    }
               
                 
  }
}
void *xmemdup(void const *p, size_t s) __attribute__((__malloc__));
extern
    __attribute__((__nothrow__)) void *(__attribute__((__leaf__))
                                        calloc)(size_t __nmemb, size_t __size)
        __attribute__((__malloc__));
void *xmalloc(size_t n) __attribute__((__malloc__));
void *xmalloc(size_t n) {
  void *p;
  void *tmp;

  {
    tmp = malloc(n);
    p = tmp;
             
                     
                     
       
     
    return (p);
  }
}
void *xrealloc(void *p, size_t n) {

   

                      
             
                     
                     
       
     
    return (p);
   

}
void *xmemdup(void const *p, size_t s) __attribute__((__malloc__));
void *xmemdup(void const *p, size_t s) {
  void *tmp;
  void *tmp___0;

  {
    tmp = xmalloc(s);
    tmp___0 = memcpy(tmp, p, s);
    return (tmp___0);
  }
}
char *xstrdup(char const *string) __attribute__((__malloc__));
char *xstrdup(char const *string) {
  size_t tmp;
  char *tmp___0;

  {
    tmp = strlen(string);
    tmp___0 = (char *)xmemdup((void const *)string, tmp + 1UL);
    return (tmp___0);
  }
}
__attribute__((__nothrow__))
FTS *(__attribute__((__warn_unused_result__, __leaf__))
      fts_open)(char *const *argv, int options,
                int (*compar)(FTSENT const **, FTSENT const **));
FTS *xfts_open(char *const *argv, int options,
               int (*compar)(FTSENT const **, FTSENT const **));
_Bool cycle_warning_required(FTS const *fts, FTSENT const *ent);
FTS *xfts_open(char *const *argv, int options,
               int (*compar)(FTSENT const **, FTSENT const **)) {
  FTS *fts;
  FTS *tmp;
  int *tmp___0;

  {
    tmp = fts_open(argv, options | 512, compar);
    fts = tmp;
                                                           
                                   
                              
                                                     
                                                                     
                                                 
                                        
       
                   
     
    return (fts);
  }
}
_Bool cycle_warning_required(FTS const *fts, FTSENT const *ent) {
  int tmp;

  {
                                
                                    
                
              
                
       
           {
    _L:
                                 {
                                  {
                                    {
            tmp = 1;
          }       
                    
           
        }       
                  
         
      }       
                
       
    }
    return ((_Bool)tmp);
  }
}
__attribute__((__noreturn__)) void xalloc_die(void);
void xalloc_die(void) {
  char *tmp;

   
                                      
                                           
            
   
}
extern int printf(char const *__restrict __format, ...);
extern int fputs_unlocked(char const *__restrict __s,
                          FILE *__restrict __stream);
char const version_etc_copyright[47];
void version_etc_arn(FILE *stream, char const *command_name,
                     char const *package, char const *version,
                     char const *const *authors, size_t n_authors);
void version_etc_va(FILE *stream, char const *command_name, char const *package,
                    char const *version, va_list authors);
void version_etc(FILE *stream, char const *command_name, char const *package,
                 char const *version, ...) __attribute__((__sentinel__));
void version_etc_arn(FILE *stream, char const *command_name,
                     char const *package, char const *version,
                     char const *const *authors, size_t n_authors) {
  char *tmp;
  char *tmp___0;
  char *tmp___1;
  char *tmp___2;
  char *tmp___3;
  char *tmp___4;
  char *tmp___5;
  char *tmp___6;
  char *tmp___7;
  char *tmp___8;
  char *tmp___9;
  char *tmp___10;

   
                       
                                                                      
            
                                                   
     
                         
                                                      
             
                                                               
                                                                              
                                                                           
                                                                 
                                                  
                           
                  
     
                           
                  
     
                           
                  
     
                           
                  
     
                           
                  
     
                           
                  
     
                           
                  
     
                           
                  
     
                           
                  
     
                           
                  
     
                        
         
            
         
                                          
                                                           
                      
         
                                                 
                                                                           
                      
         
                                                      
                                                                          
                            
                      
         
                                                           
                                                                          
                                            
                      
         
                                                               
                                                                          
                                                            
                      
         
                                                                   
                                                                          
                                                                            
                      
         
                                                                       
                                                                          
                                                                           
                            
                      
         
                                                                            
                                                                          
                                                                           
                                            
                      
         
                                                                                
                                                                          
                                                                           
                                                            
                      
                 
                       
                                                                          
                                                                           
                                                                           
                                                            
                      
                
           
   
}
void version_etc_va(FILE *stream, char const *command_name, char const *package,
                    char const *version, va_list authors) {
  size_t n_authors;
  char const *authtab[10];
  char const *tmp;

   
                          
               

                             
                                                      
                                 
                                                                  
                           
         
              
                         
       
                  
     
              
                                                           
                                                               
           
   
}
void version_etc(FILE *stream, char const *command_name, char const *package,
                 char const *version, ...) __attribute__((__sentinel__));
void version_etc(FILE *stream, char const *command_name, char const *package,
                 char const *version, ...) {
  va_list authors;

   
                                         
                                                                    
                              
           
   
}
                                        
                                                                         
                                                                         
                                                                         
                                                                         
                                                                         
                                                                         
                                                                         
                                                                         
                                                                         
                                                                         
                                                                         
                                                          
char const *parse_user_spec(char const *spec, uid_t *uid, gid_t *gid,
                            char **username, char **groupname);
extern void endpwent(void);
extern struct passwd *getpwnam(char const *__name);
extern void endgrent(void);
extern struct group *getgrgid(__gid_t __gid);
extern struct group *getgrnam(char const *__name);
char *(__attribute__((__warn_unused_result__)) umaxtostr)(uintmax_t i,
                                                          char *buf___1);
static char const *parse_with_separator(char const *spec, char const *separator,
                                        uid_t *uid, gid_t *gid, char **username,
                                        char **groupname);
static char const *E_invalid_user = "invalid user";
static char const *E_invalid_group = "invalid group";
static char const *E_bad_spec = "invalid spec";
static char const *parse_with_separator(char const *spec, char const *separator,
                                        uid_t *uid, gid_t *gid, char **username,
                                        char **groupname) {
  char const *error_msg;
  struct passwd *pwd;
  struct group *grp;
  char *u;
  char const *g;
  char *gname;
  uid_t unum;
  gid_t gnum;
  char *tmp;
  size_t ulen;
  struct passwd *tmp___0;
  _Bool use_login_group;
  int tmp___1;
  unsigned long tmp___2;
  strtol_error tmp___3;
  char buf___1[(((sizeof(uintmax_t) * 8UL) * 146UL) / 485UL + 1UL) + 1UL];
  char *tmp___4;
  char *tmp___5;
  struct group *tmp___6;
  unsigned long tmp___7;
  strtol_error tmp___8;
  char const *tmp___9;

  {
    gname = (char *)((void *)0);
    unum = *uid;
    gnum = *gid;
    error_msg = (char const *)((void *)0);
                              
                     
                    
                            
    if ((unsigned long)separator == (unsigned long)((void *)0)) {
                 {
        u = xstrdup(spec);
      }
    } else {
      ulen = (size_t)(separator - spec);
      if (ulen != 0UL) {
        u = (char *)xmemdup((void const *)spec, ulen + 1UL);
        *(u + ulen) = (char)'\000';
      }
    }
    if ((unsigned long)separator == (unsigned long)((void *)0)) {
      g = (char const *)((void *)0);
    } else {
                                               
                                      
             {
        g = separator + 1;
      }
    }
    if ((unsigned long)u != (unsigned long)((void *)0)) {
                          
                                           
             {
        tmp___0 = getpwnam((char const *)u);
        pwd = tmp___0;
      }
                                                             
                                                                     
                                                               
                        
                  
                        
           
                
                      
         
                                         
                              
                                 
                
                   
                                                                                
                                            
                                          
                                                  
                                      
                      
                                           
               
                    
                                         
             
                  
                                       
           
         
             {
        unum = pwd->pw_uid;
                                                             
                                                                       
                               
                                 
                      
                                     
                    
                                                            
                                
             
                                                   
                       
           
         
      }
                 
    }
    if ((unsigned long)g != (unsigned long)((void *)0)) {
                                                                  {
                                    
                                            
               {
          tmp___6 = getgrnam(g);
          grp = tmp___6;
        }
                                                               
                                                                        
                                            
                                          
                                                  
                                      
                      
                                            
               
                    
                                          
             
                  
                                        
           
               {
          gnum = grp->gr_gid;
        }
                   
        gname = xstrdup(g);
      }
    }
                                                                {
      *uid = unum;
      *gid = gnum;
      *username = u;
      *groupname = gname;
                              
    }       
                          
     
                    
    tmp___9 = (char const *)gettext(error_msg);
    return (tmp___9);
  }
}
char const *parse_user_spec(char const *spec, uid_t *uid, gid_t *gid,
                            char **username, char **groupname) {
  char const *colon;
  char const *tmp;
  char const *error_msg;
  char const *tmp___0;
  char const *dot;
  char const *tmp___1;
  char const *tmp___2;

  {
    tmp = (char const *)strchr(spec, ':');
    colon = tmp;
    tmp___0 = parse_with_separator(spec, colon, uid, gid, username, groupname);
                        
                {
                     {
        tmp___1 = (char const *)strchr(spec, '.');
        dot = tmp___1;
        if (dot) {
          tmp___2 =
              parse_with_separator(spec, dot, uid, gid, username, groupname);
                        {
            error_msg = (char const *)((void *)0);
          }
        }
      }
    }
    return (error_msg);
  }
}
char *(__attribute__((__warn_unused_result__)) umaxtostr)(uintmax_t i,
                                                          char *buf___1) {
  char *p;

  {
                                                                      
                 
              {
          
      *p = (char)(48UL + i % 10UL);
                
                        
                         
       
    }
  while_break:;
    return (p);
  }
}
#pragma weak pthread_key_create
#pragma weak pthread_getspecific
#pragma weak pthread_setspecific
#pragma weak pthread_key_delete
#pragma weak pthread_self
#pragma weak pthread_cancel
                                                    
                  
                  

   
                                                                     
              
                                                           
                                             
            
                      
     
   
 
int open_safer(char const *file, int flags, ...);
struct dev_ino *get_root_dev_ino(struct dev_ino *root_d_i);
struct dev_ino *get_root_dev_ino(struct dev_ino *root_d_i) {
  struct stat statbuf;
  int tmp;

  {
                               
             {
      return ((struct dev_ino *)((void *)0));
    }
                                      
                                      
                      
  }
}
                                   
                                        
                                               
int set_char_quoting(struct quoting_options *o, char c, int i);
char *quotearg_char(char const *arg, char ch);
char *quotearg_char_mem(char const *arg, size_t argsize, char ch);
__inline static char *xcharalloc(size_t n) __attribute__((__malloc__));
__inline static char *xcharalloc(size_t n) __attribute__((__malloc__));
__inline static char *xcharalloc(size_t n) {
  void *tmp;
  void *tmp___0;
  void *tmp___1;

  {
                             {
      tmp = xmalloc(n);
      tmp___1 = tmp;
    }       
                                          
                        
     
    return ((char *)tmp___1);
  }
}
extern __attribute__((__nothrow__)) int(__attribute__((__leaf__))
                                        iswprint)(wint_t __wc);
                                           
                                                                   
                                           
                                                  
                                                             
                                                             
                                                             
                                                              
static struct quoting_options default_quoting_options;
int set_char_quoting(struct quoting_options *o, char c, int i) {
  unsigned char uc;
  unsigned int *p;
  struct quoting_options *tmp;
  int shift;
  int r;

  {
    uc = (unsigned char)c;
            
              
            
                                     
     
                                                                       
    shift = (int)((unsigned long)uc % (sizeof(int) * 8UL));
    r = (int)((*p >> shift) & 1U);
    *p ^= (unsigned int)(((i & 1) ^ r) << shift);
    return (r);
  }
}
static struct quoting_options
quoting_options_from_style(enum quoting_style style) {
  struct quoting_options o;

  {
    o.style = style;
                
    memset((void *)(o.quote_these_too), 0, sizeof(o.quote_these_too));
    return (o);
  }
}
static char const *gettext_quote(char const *msgid, enum quoting_style s) {
  char const *translation;
  char const *tmp;

  {
    tmp = (char const *)gettext(msgid);
    translation = tmp;
                                                             
                                  
                           
       
     
    return (translation);
  }
}
static size_t
quotearg_buffer_restyled(char *buffer, size_t buffersize, char const *arg,
                         size_t argsize, enum quoting_style quoting_style,
                         int flags, unsigned int const *quote_these_too,
                         char const *left_quote, char const *right_quote) {
  size_t i;
  size_t len;
  char const *quote_string;
  size_t quote_string_len;
  _Bool backslash_escapes;
  _Bool unibyte_locale;
  size_t tmp;
  _Bool elide_outer_quotes;
  unsigned char c;
  unsigned char esc;
  _Bool is_right_quote;
  int tmp___0;
  int tmp___1;
  size_t m;
  _Bool printable;
  unsigned short const **tmp___2;
  mbstate_t mbstate;
  wchar_t w;
  size_t bytes;
  size_t tmp___3;
  size_t j;
  int tmp___4;
  int tmp___5;
  size_t ilim;
  int tmp___6;
  size_t tmp___7;

  {
                    
                                   
                                 
                                 
                                   
                                         
    elide_outer_quotes = (_Bool)((flags & 2) != 0);
                                            
                  
     
                                            
                  
     
                                            
                  
     
                                           {
                  
    }
                                            
                  
     
                                            
                  
     
                                            
                  
     
                                            
                  
     
                                            
                  
     
                        
         
                                          
                                  
         
                              
                 

                               
                                       
         
              
                         
       
                 
     
                                 
                        
                                 
                      
         
                                 
                                  
                      
  case_6:
                                           {
      left_quote = gettext_quote("`", quoting_style);
      right_quote = gettext_quote("\'", quoting_style);
    }
                             {
      quote_string = left_quote;
                {

                             
                               
         
                  {

                                {
            *(buffer + len) = (char)*quote_string;
          }
          len++;
                               
        }
      while_break___1:
        quote_string++;
      }
    while_break___0:;
    }
                                 
    quote_string = right_quote;
    quote_string_len = strlen(quote_string);
                      
         
                                          
                                  
         
                              
                 

                               
                                       
         
              
                             
       
                     
     
                        
                                 
                      
         
                                  
                      
                 
            
  switch_break:
    i = (size_t)0;
    while (1) {

                                           {
        tmp___6 = (int const) * (arg + i) == 0;
      }       
                               
       
      if (tmp___6) {
        goto while_break___3;
      }
                                
                              
                               
                                                
                                                     
                                                                           
                               
                                       
                                               
               
                                        
             
           
         
       
      c = (unsigned char)*(arg + i);
                        
                        
       
                         
                     
       
                        
                        
       
                        
                        
       
                         
                     
       
                         
                     
       
                         
                     
       
                        
                    
       
                         
                     
       
                         
                     
       
                          
                      
       
                          
                      
       
                         
                     
       
                          
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                          
                     
       
                         
                         
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                              
               
                              
                                 
                                         
         
                   

                                 
                                         
           
                
                               
         
                       
                                
                                              
                                                        
                         

                                       
                                              
                 
                      
                                     
               
                             
                         

                                       
                                              
                 
                      
                                     
               
                             
             
           
         
                               
              
                        
                      
         
       
                            
            
                                              
                        
       
                                              
                        
       
                              
               
                               
                                       
       
                            
               
                      
                                
                                                      
                                                        
                           
             
                                                        
                           
             
                                                        
                           
             
                                                        
                           
             
                                                        
                           
             
                                                        
                           
             
                                                        
                           
             
                                                        
                           
             
                                                        
                           
             
                                    
                  
                                     
                                             
             
                                                  
                     
                       

                                     
                                            
               
                    
                                   
             
                           
                       

                                     
                                             
               
                    
                                   
             
                           
                       

                                     
                                             
               
                    
                                   
             
                           
                       

                                     
                                            
               
                    
                                    
             
                            
                                  
                             
                                  
                            
           
         
       
                            
                       
                            
                      
                            
               
                               
                    
               
                               
                    
            
                               
                    
            
                               
                              
            
                               
                              
           
                               
                              
            
                               
                    
            
              
                              
                                 
                                 
                         
           
         
       
                       
                                              
                                 
                                         
         
       
             
                              
                
                          
       
                            
             
                                            
                                               
              
                                 
       
                     
                              
       
            
                     
                              
       
            
                                              
                                 
                                         
         
       
                            
                
                                              
                                 
                                         
         
                   

                                 
                                         
           
                
                                
         
                        
                   

                                 
                                         
           
                
                                
         
                        
                   

                                 
                                         
           
                
                                
         
                        
       
                            
            
                            
                       
                           
                      
                                  
                                                                              
              
                                                       
                      
                             
                                              
                                
         
                   
                                                                            
                          
                             
                                  
                  
                                                
                                   
                                    
                    
                                                  
                                     
                           

                                        
                                            
                                            
                     
                          
                                          
                   
                      
                 
                                
                                      
                      
                                         
                                                          
                                  
                               

                                         
                                              
                       
                                                                      
                                         
                       
                                                                      
                                         
                       
                                                                      
                                         
                       
                                                                      
                                         
                       
                                                                       
                                         
                       
                                              
                                
                                                     
                                       
                                            
                                     
                          
                     
                                    
                   
                 
                                              
                               
                                      
                 
                           
               
             
           
                                                           
                        
                                  
           
         
                        
       
                    
                    
              
                                
                           
                 
                         
                       

                                      
                                 
                                           
                                                   
                   
                             

                                           
                                                   
                     
                          
                                          
                   
                                  
                             

                                           
                                                                   
                     
                          
                                          
                   
                                  
                             

                                           
                                                                         
                     
                          
                                          
                   
                                 
                                                         
                        
                          
                 
                      
                 
                                     
                             

                                           
                                                   
                     
                          
                                          
                   
                                 
                                            
                 
               
                                    
                                      
               
                         

                                       
                                            
                 
                      
                                      
               
                             
                  
                                            
             
                            
                         
           
         
       
                      
                              
                    
              
                                 
               
                                
                                                                               
                                                              
                                                                        
                          
             
                  
                        
           
                
               
                                
                         
           
         
       
                 
                               
                                       
       
                 

                               
                                       
         
              
                              
       
                      
    store_c:
                {

                              {
          *(buffer + len) = (char)c;
        }
        len++;
                              
      }
    while_break___24:;
    __Cont:
      i++;
    }
  while_break___3:;
                     
                                              
                                 
                                         
         
       
     
                      {
                               {
                  {

                               
                                  
           
                    {

                                  {
              *(buffer + len) = (char)*quote_string;
            }
                  
                                  
          }
        while_break___26:
          quote_string++;
        }
      while_break___25:;
      }
    }
                           
                                     
     
    return (len);
  force_outer_quoting_style:
    tmp___7 = quotearg_buffer_restyled(
        buffer, buffersize, arg, argsize, quoting_style, flags & -3,
        (unsigned int const *)((void *)0), left_quote, right_quote);
                     
  }
}
static char slot0[256];
static unsigned int nslots = 1U;
static struct slotvec slotvec0 = {sizeof(slot0), slot0};
static struct slotvec *slotvec = &slotvec0;
static char *quotearg_n_options(int n, char const *arg, size_t argsize,
                                struct quoting_options const *options) {
  int e;
  int *tmp;
  unsigned int n0;
  struct slotvec *sv;
  size_t n1;
  _Bool preallocated;
  int tmp___0;
  struct slotvec *tmp___1;
  size_t size;
  char *val;
  int flags;
  size_t qsize;
  size_t tmp___2;
  int *tmp___3;

  {
    tmp = __errno_location();
             
                         
    sv = slotvec;
                
              
     
                       
                             
                                                                              
                                                
                     
              
                     
       
                                               
                     
       
                         
                                                
              
                     
       
                                                                         
                   
                         
                       
       
                                                                            
                                
     
    size = (sv + n)->size;
    val = (sv + n)->val;
    flags = (int)(options->flags | 1);
    tmp___2 = quotearg_buffer_restyled(
        val, size, arg, argsize, (enum quoting_style)options->style, flags,
        (unsigned int const *)(options->quote_these_too),
        (char const *)options->left_quote, (char const *)options->right_quote);
    qsize = tmp___2;
    if (size <= qsize) {
                         
                            
                                                         
                          
       
                             
                          
      quotearg_buffer_restyled(val, size, arg, argsize,
                               (enum quoting_style)options->style, flags,
                               (unsigned int const *)(options->quote_these_too),
                               (char const *)options->left_quote,
                               (char const *)options->right_quote);
    }
                                 
                 
    return (val);
  }
}
char *quotearg_n_style(int n, enum quoting_style s, char const *arg) {
  struct quoting_options o;
  struct quoting_options tmp;
  char *tmp___0;

  {
    tmp = quoting_options_from_style(s);
    o = tmp;
    tmp___0 = quotearg_n_options(n, arg, (size_t)-1,
                                 (struct quoting_options const *)(&o));
    return (tmp___0);
  }
}
char *quotearg_char_mem(char const *arg, size_t argsize, char ch) {
  struct quoting_options options;
  char *tmp;

  {
                                      
                                      
    tmp = quotearg_n_options(0, arg, argsize,
                             (struct quoting_options const *)(&options));
    return (tmp);
  }
}
char *quotearg_char(char const *arg, char ch) {
  char *tmp;

  {
    tmp = quotearg_char_mem(arg, (size_t)-1, ch);
    return (tmp);
  }
}
char *quotearg_colon(char const *arg) {
  char *tmp;

  {
    tmp = quotearg_char(arg, (char)':');
    return (tmp);
  }
}
char const *quote_n(int n, char const *name) {
  char const *tmp;

  {
    tmp = (char const *)quotearg_n_style(n, (enum quoting_style)6, name);
    return (tmp);
  }
}
char const *quote(char const *name) {
  char const *tmp;

  {
    tmp = quote_n(0, name);
    return (tmp);
  }
}
                         
void set_program_name(char const *argv0);
extern char *program_invocation_name;
extern char *program_invocation_short_name;
extern int fputs(char const *__restrict __s, FILE *__restrict __stream);
char const *program_name = (char const *)((void *)0);
void set_program_name(char const *argv0) {
  char const *slash;
  char const *base;
  int tmp;
  int tmp___0;

   
                                                             
                                                                                
              
     
                                              
                                                             
                       
            
                   
     
                             
                                                        
                         
                     
                                              
                       
                           
                                                        
         
       
     
                         
                                            
           
   
}
extern __attribute__((__nothrow__)) char *(
    __attribute__((__nonnull__(1, 2), __leaf__))
    stpcpy)(char *__restrict __dest, char const *__restrict __src);
extern DIR *(__attribute__((__nonnull__(1))) opendir)(char const *__name);
extern
    __attribute__((__nothrow__)) int(__attribute__((__nonnull__(1), __leaf__))
                                     dirfd)(DIR *__dirp);
DIR *opendir_safer(char const *name) {
  DIR *dp;
  DIR *tmp;
  int fd;
  int tmp___0;
  DIR *newdp;
  int e;
  int f;
  int tmp___1;
  int *tmp___2;
  int *tmp___3;

  {
                        
             
            {
      tmp___0 = dirfd(dp);
      fd = tmp___0;
                   {
                     {
          tmp___1 = dup_safer(fd);
          f = tmp___1;
          newdp = rpl_fdopendir(f);
                                       
                       
                       
                     
           
                       
                                       
                       
          dp = newdp;
        }
      }
    }
    return (dp);
  }
}
int openat_safer(int fd, char const *file, int flags, ...);
int openat_safer(int fd, char const *file, int flags, ...) {
  mode_t mode;
  va_list ap;
  int tmp;
  int tmp___0;

  {
    mode = (mode_t)0;
                     
                                    
                                          
                           
     
    tmp = openat(fd, file, flags, mode);
    tmp___0 = fd_safer(tmp);
    return (tmp___0);
  }
}
int open_safer(char const *file, int flags, ...) {
  mode_t mode;
  va_list ap;
  int tmp;
  int tmp___0;

  {
    mode = (mode_t)0;
                     
                                    
                                          
                           
     
    tmp = open(file, flags, mode);
    tmp___0 = fd_safer(tmp);
    return (tmp___0);
  }
}
                                                
                        
                        
          
             
                 
              
              
              
              
              
              
              
               
               
               
               
               
               
               
                          
                          
                   
                   
               
                                  
               
                                  
                  

   
                                                 
                 
     
                                        
                         
                         
                                
                                                                
                                 
                         
                                
                                                                
                                 
                 
                                   
                                 
                                  
                         
                  
                         
           
                
                       
         
                       
                                     
                                   
                                    
                           
                    
                           
             
                  
                         
           
                          
                             
           
                
                           
         
                                 
                                   
                                                 
                                                     
                                              
                  
                         
           
                             
                
                                   
                         
                  
                                                     
                                                           
                                                                             
                                 
                    
                                                      
                                                             
                                                                               
                                  
                              
                        
                               
                 
                                  
                      
                                                             
                                                                               
                                   
                              
                        
                               
                 
                                  
               
                                 
             
                                
           
                              
         
                       
                       
                       
         
                                         
                                   
                                         
                                   
       
                
                                 
                               
                                
                       
                
                       
         
              
                     
       
                     
                   
       
                                 
                               
                                
                       
                
                       
         
              
                     
       
                     
                    
       
                 
            
                                     
                                     
                 
                                   
                                                         
                                       
                                       
                
                                  
         
                                   
                                                         
                                       
                                       
                
                                  
         
                           
                               
         
             
             
                                    
                               
         
       
                     
                                 
     
   
 
                                        
                                                              
                                                                     
extern __attribute__((__nothrow__, __noreturn__)) void(__attribute__((__leaf__))
                                                       exit)(int __status);
extern int optind;
extern __attribute__((__nothrow__)) int(__attribute__((__leaf__)) getopt_long)(
    int ___argc, char *const *___argv, char const *__shortopts,
    struct option const *__longopts, int *__longind);
#pragma weak pthread_mutex_init
#pragma weak pthread_mutex_lock
#pragma weak pthread_mutex_unlock
#pragma weak pthread_mutex_destroy
#pragma weak pthread_rwlock_init
#pragma weak pthread_rwlock_rdlock
#pragma weak pthread_rwlock_wrlock
#pragma weak pthread_rwlock_unlock
#pragma weak pthread_rwlock_destroy
#pragma weak pthread_once
#pragma weak pthread_cond_init
#pragma weak pthread_cond_wait
#pragma weak pthread_cond_signal
#pragma weak pthread_cond_broadcast
#pragma weak pthread_cond_destroy
#pragma weak pthread_mutexattr_init
#pragma weak pthread_mutexattr_settype
#pragma weak pthread_mutexattr_destroy
#pragma weak pthread_self
#pragma weak pthread_cancel
extern struct passwd *getpwuid(__uid_t __uid);
void i_ring_init(I_ring *ir, int default_val);
int i_ring_push(I_ring *ir, int val);
int i_ring_pop(I_ring *ir);
_Bool i_ring_empty(I_ring const *ir);
void i_ring_init(I_ring *ir, int default_val) {
  int i;

   
                            
                      
                     
          
               

                     
                         
       
                                   
          
     
              
                                     
           
   
}
_Bool i_ring_empty(I_ring const *ir) {

   

    return ((_Bool)ir->ir_empty);
   

}
int i_ring_push(I_ring *ir, int val) {
  unsigned int dest_idx;
  int old_val;

  {
    dest_idx = (ir->ir_front + (unsigned int)(!ir->ir_empty)) % 4U;
    old_val = ir->ir_data[dest_idx];
                                
                            
                                  
                                                                       
     
                            
    return (old_val);
  }
}
int i_ring_pop(I_ring *ir) {
  int top_val;
  _Bool tmp;

  {
                                           
              
              
     
    top_val = ir->ir_data[ir->ir_front];
                                                   
                                     {
      ir->ir_empty = (_Bool)1;
    }       
                                                     
     
    return (top_val);
  }
}
_Bool(__attribute__((__warn_unused_result__))
      hash_rehash)(Hash_table *table___0, size_t candidate);
void *hash_delete(Hash_table *table___0, void const *entry);
__inline static size_t rotr_sz(size_t x, int n) {

   

    return (((x >> n) | (x << (8UL * sizeof(x) - (unsigned long)n))) &
            0xffffffffffffffffUL);
   

}
static struct hash_tuning const default_tuning = {
    (float)0.0, (float)1.0, (float)0.8, (float)1.414, (_Bool)0};
void *hash_lookup(Hash_table const *table___0, void const *entry) {
  struct hash_entry const *bucket;
  size_t tmp;
  struct hash_entry const *cursor;
  _Bool tmp___0;

  {
    tmp = (*(table___0->hasher))(entry, (size_t)table___0->n_buckets);
    bucket = (struct hash_entry const *)(table___0->bucket + tmp);
                                                                            
              
     
                                                                   {
      return ((void *)0);
    }
                    
               

                    
                         
       
                                                                
                                      
              
                                                                                
                      
                                        
         
       
                                                       
     
               
                       
  }
}
                                                          
               
                   

   
                      
               
                                  
                
                         
       
                                                      
               
     
               
                   
   
 
static _Bool is_prime(size_t candidate) {
  size_t divisor;
  size_t square;
  int tmp;

  {
                        
                               
              {

                               
                                     
                           
         
              
                         
       
                
                              
                
    }
  while_break:;
                             {
      tmp = 1;
    }       
              
     
    return ((_Bool)tmp);
  }
}
static size_t next_prime(size_t candidate) {
  _Bool tmp;

  {
                           
                             
     
                     
               

                                              
                                  
                  
                           
         
              
                         
       
                       
     
               
    return (candidate);
  }
}
static size_t raw_hasher(void const *data, size_t n) {
  size_t val;
  size_t tmp;

  {
    tmp = rotr_sz((size_t)data, 3);
    val = tmp;
    return (val % n);
  }
}
static _Bool raw_comparator(void const *a, void const *b) {

   

    return ((_Bool)((unsigned long)a == (unsigned long)b));
   

}
static _Bool check_tuning(Hash_table *table___0) {
  Hash_tuning const *tuning;
  float epsilon;

  {
    tuning = table___0->tuning;
                                                                   {
      return ((_Bool)1);
    }
                   
                                                    
                                                                         
                                                                
                                                           
                                                                 
                                        
                                                            
                                                                     
                                               
                                    
                 
               
             
           
         
       
     
                                        
                      
  }
}
static size_t compute_bucket_size(size_t candidate, Hash_tuning const *tuning) {
  float new_candidate;
  int tmp;

  {
                               {
      new_candidate =
          (float)((float const)candidate / tuning->growth_threshold);
                                                        {
        return ((size_t)0);
      }
                                        
    }
                                      
                                              
               
            
               
     
                                                                
                         
     
                       
  }
}
Hash_table *(__attribute__((__warn_unused_result__))
             hash_initialize)(size_t candidate, Hash_tuning const *tuning,
                              size_t (*hasher)(void const *, size_t),
                              _Bool (*comparator)(void const *, void const *),
                              void (*data_freer)(void *)) {
  Hash_table *table___0;
  _Bool tmp;

  {
                                                              
                           
     
                                                                  
                                   
     
                                                         
                                                                 
                                         
     
                  
                               
     
                               
                                  
               
                
     
                                                                  
                                
                
     
                                                    
                                                            
                                                                         
                
     
                             
                                                                              
                                          
                                     
                               
                                       
                                       
    table___0->free_entry_list = (struct hash_entry *)((void *)0);
    return (table___0);
       
                            
                                       
  }
}
void hash_free(Hash_table *table___0) {
  struct hash_entry *bucket;
  struct hash_entry *cursor;
  struct hash_entry *next;

   
                                
                                 
                                   
                   

                                       
                                                          
                             
           
                             
                            
                       

                            
                                     
               
                                                       
                                    
             
                           
           
                   
         
                   
       
     
                               
               

                                                                              
                             
       
                            
                 

                      
                               
         
                            
                             
                      
       
                    
               
     
                  
                                        
               

                    
                             
       
                          
                           
                    
     
                  
                                    
                            
           
   
}
static struct hash_entry *allocate_entry(Hash_table *table___0) {
  struct hash_entry *new;

  {
                                    {
      new = table___0->free_entry_list;
                                             
    }       
                                                      
     
    return (new);
  }
}
static void free_entry(Hash_table *table___0, struct hash_entry *entry) {

   

                            
                                             
                                       
           
   

}
static void *hash_find_entry(Hash_table *table___0, void const *entry,
                             struct hash_entry **bucket_head, _Bool delete) {
  struct hash_entry *bucket;
  size_t tmp;
  struct hash_entry *cursor;
  void *data;
  struct hash_entry *next;
  _Bool tmp___0;
  void *data___0;
  struct hash_entry *next___0;
  _Bool tmp___1;

  {
                                                              
                                     
                                                                            
              
     
                          
                                                                    
                         
     
                                                              
              
            
                                                                              
                    
         
                            
                     
                             
                                
                            
                                        
                  
                                     
           
         
                      
       
     
                    
               

                          
                         
       
                                                                        
                    
              
                                             
                                                       
                      
               
                                          
                       
                                    
                                          
                                            
           
                            
         
       
                            
     
               
    return ((void *)0);
  }
}
static _Bool transfer_entries(Hash_table *dst, Hash_table *src, _Bool safe) {
  struct hash_entry *bucket;
  struct hash_entry *cursor;
  struct hash_entry *next;
  void *data;
  struct hash_entry *new_bucket;
  size_t tmp;
  size_t tmp___0;
  struct hash_entry *new_entry;
  struct hash_entry *tmp___1;

  {
                         
               

                                                                        
                         
       
                         
                              
                   

                        
                                 
           
                              
                                                                     
                                         
                                                                                
                    
           
                              
                                 
                                            
                                      
                  
                                    
                                    
                                    
           
                        
         
                      
                            
                                                        
                   
                      
         
                                                                       
                                           
                                                                              
                  
         
                               
                                        
                              
                                                                       
                              
           
                                 
                                             
                                       
                
                                  
                                  
         
                                 
                                
       
           
               
     
               
    return ((_Bool)1);
  }
}
_Bool(__attribute__((__warn_unused_result__))
      hash_rehash)(Hash_table *table___0, size_t candidate) {
  Hash_table storage;
  Hash_table *new_table;
  size_t new_size;
  size_t tmp;
  _Bool tmp___0;
  _Bool tmp___1;
  _Bool tmp___2;

  {
                                                            
                   
                    
                        
     
                                           
                        
     
                         
                       
                                                                            
                                                                         
                        
     
                                    
                             
                                                                  
                                          
                                     
                                          
                                          
                                                  
                                                  
    new_table->free_entry_list = table___0->free_entry_list;
                                                               
                 {
                                      
                                            
                                                        
                                                  
                                                            
                                                              
      return ((_Bool)1);
    }
                                                            
                                                               
                  
                                                                 
                     
                
       
            
              
     
                                    
                      
  }
}
void *(__attribute__((__warn_unused_result__))
       hash_insert)(Hash_table *table___0, void const *entry) {
  void *data;
  struct hash_entry *bucket;
  Hash_tuning const *tuning;
  float candidate;
  float tmp;
  _Bool tmp___0;
  void *tmp___1;
  struct hash_entry *new_entry;
  struct hash_entry *tmp___2;

  {
                 
              
     
                                                                
                                                            
                    
     
                                                
                                               
                                                
                              
                                                  
                                                 
                                                  
                                   
                                   
                                                           
                                               
                
                                                            
                                                 
                                                  
         
                        
                                                       
                             
         
                                                            
                       
                             
         
                                                                       
                                                                   
                  
         
       
     
                       
                                          
                          
                                                                   
                           
       
                                      
                                     
                               
                               
                             
     
                                 
                             
                                  
    return ((void *)entry);
  }
}
void *hash_delete(Hash_table *table___0, void const *entry) {
  void *data;
  struct hash_entry *bucket;
  Hash_tuning const *tuning;
  size_t candidate;
  float tmp;
  struct hash_entry *cursor;
  struct hash_entry *next;
  _Bool tmp___0;

  {
    data = hash_find_entry(table___0, entry, &bucket, (_Bool)1);
               {
      return ((void *)0);
    }
                             
                        
                                    
                                                  
                                                 
                                                  
                                
                                                    
                                                   
                                                    
                                     
                                     
                                                             
                                                 
                  
                                                              
                                                   
                                                    
           
                                  
                                                      
                         
                                                
                       

                            
                                 
               
                                  
                                   
                            
             
                       
                                                                          
           
         
       
     
                  
  }
}
size_t hash_pjw(void const *x, size_t tablesize);
                                                      
                           
             
                 

   
                                   
                                                          
                  
                                                           
   
 
                                                            
                           
                           
              
              

   
                                   
                                   
                                 
                                   
                                                                       
                           
                      
                
                      
         
              
                    
       
            
                  
     
                            
   
 
                           
                     

   
                             
                          
                    
           
   
 
                                                  
                
           

   
                  
                        
               

                
                         
       
                                                                               
          
     
               
                           
   
 
extern __attribute__((__nothrow__)) char *(__attribute__((__leaf__))
                                           setlocale)(int __category,
                                                      char const *__locale);
extern __attribute__((__nothrow__)) int(
    __attribute__((__nonnull__(2, 3), __leaf__))
    fstatat)(int __fd, char const *__restrict __file,
             struct stat *__restrict __buf, int __flag);
__attribute__((__nothrow__)) int(__attribute__((__warn_unused_result__,
                                                __leaf__)) fts_close)(FTS *sp);
__attribute__((__nothrow__))
FTSENT *(__attribute__((__warn_unused_result__, __leaf__)) fts_read)(FTS *sp);
__attribute__((__nothrow__)) int(__attribute__((__leaf__))
                                 fts_set)(FTS *sp __attribute__((__unused__)),
                                          FTSENT *p, int instr);
extern void(__attribute__((__nonnull__(1, 4)))
            qsort)(void *__base, size_t __nmemb, size_t __size,
                   int (*__compar)(void const *, void const *));
static FTSENT *fts_alloc(FTS *sp, char const *name, size_t namelen);
static FTSENT *fts_build(FTS *sp, int type);
static void fts_lfree(FTSENT *head);
static void fts_load(FTS *sp, FTSENT *p);
static size_t fts_maxarglen(char *const *argv);
static void fts_padjust(FTS *sp, FTSENT *head);
static _Bool fts_palloc(FTS *sp, size_t more);
static FTSENT *fts_sort(FTS *sp, FTSENT *head, size_t nitems);
static unsigned short fts_stat(FTS *sp, FTSENT *p, _Bool follow);
static int fts_safe_changedir(FTS *sp, FTSENT *p, int fd, char const *dir);
static _Bool AD_compare(void const *x, void const *y) {
  struct Active_dir const *ax;
  struct Active_dir const *ay;
  int tmp;

  {
    ax = (struct Active_dir const *)x;
    ay = (struct Active_dir const *)y;
                            {
                              {
        tmp = 1;
      }       
                
       
    }       
              
     
    return ((_Bool)tmp);
  }
}
static size_t AD_hash(void const *x, size_t table_size) {
  struct Active_dir const *ax;

  {
    ax = (struct Active_dir const *)x;
    return ((uintmax_t)ax->ino % table_size);
  }
}
static _Bool setup_dir(FTS *fts) {

   

                                 
                         
                                                                       
                                                                            
                               
                          
       
            
                            
                                                                              
                                  
                          
       
                                             
     
    return ((_Bool)1);
   

}
static _Bool enter_dir(FTS *fts, FTSENT *ent) {
  struct stat const *st;
  struct Active_dir *ad;
  struct Active_dir *tmp;
  struct Active_dir *ad_from_table;
  _Bool tmp___0;

  {
                                 
                                                 
                                                     
               
                
                          
       
                                  
                                  
                        
                     
                                                                                
                                                              
                         
                             
                            
         
                                                
                                          
       
            
                                                 
                                                                   
                    
                             
                                          
       
     
    return ((_Bool)1);
  }
}
static void leave_dir(FTS *fts, FTSENT *ent) {
  struct stat const *st;
  struct Active_dir obj;
  void *found;
  FTSENT *parent;

   
                                               
                                 
                                  
                                  
                                                                   
                   
                
       
                  
            
                               
                                                                
                                      
                     

                                                               
                      
             
                                                                              
                                                                                
                                                        
                                                
                                                        
                                                
               
             
                             
           
                     
         
       
     
           
   
}
static void free_dir(FTS *sp) {

   

                                
                             
                                    
       
            
                                        
     
           
   

}
static void fd_ring_clear(I_ring *fd_ring) {
  int fd;
  int tmp;
  _Bool tmp___0;

   
               
                                                      
                    
                         
       
                                
               
                    
                  
       
     
               
           
   
}
static void fts_set_stat_required(FTSENT *p, _Bool required) {

   

               

                                      
                
       
                       
     
               
                  {
      p->fts_statp[0].st_size = (__off_t)2;
    }       
                                           
     
           
   

}
__inline static DIR *opendirat(int fd, char const *dir) {
  int new_fd;
  int tmp;
  DIR *dirp;
  int saved_errno;
  int *tmp___0;
  int *tmp___1;

  {
    tmp = openat_safer(fd, dir, 67840);
    new_fd = tmp;
                    {
      return ((DIR *)((void *)0));
    }
                                       
                                 
                                                            
                                   
                             
                    
                                   
                             
     
                  
  }
}
static void cwd_advance_fd(FTS *sp, int fd, _Bool chdir_down_one) {
  int old;
  int prev_fd_in_slot;
  int tmp;

   
                         
               

                         
                             
                  
         
       
                       
     
               
                         
                                               
                            
                                 
                               
       
            
                                   
                       
                     
         
       
     
                        
           
   
}
__inline static int diropen(FTS const *sp, char const *dir) {
  int open_flags;
  int tmp;
  int fd;
  int tmp___0;
  int tmp___1;
  int tmp___2;

  {
                              {
      tmp = 131072;
    }       
              
     
    open_flags = 67840 | tmp;
                               {
      tmp___0 = openat_safer((int)sp->fts_cwd_fd, dir, open_flags);
      tmp___2 = tmp___0;
    }       
                                            
                        
     
    fd = tmp___2;
                  
                                     
     
    return (fd);
  }
}
__attribute__((__nothrow__))
FTS *(__attribute__((__warn_unused_result__, __leaf__))
      fts_open)(char *const *argv, int options,
                int (*compar)(FTSENT const **, FTSENT const **));
FTS *(__attribute__((__warn_unused_result__, __leaf__))
      fts_open)(char *const *argv, int options,
                int (*compar)(FTSENT const **, FTSENT const **)) {
  FTS *sp;
  FTSENT *p;
  FTSENT *root;
  size_t nitems;
  FTSENT *parent;
  FTSENT *tmp;
  _Bool defer_stat;
  int *tmp___0;
  int *tmp___1;
  int *tmp___2;
  size_t maxarglen;
  size_t tmp___4;
  size_t tmp___5;
  _Bool tmp___6;
  int tmp___7;
  size_t len;
  size_t tmp___8;
  struct _ftsent *tmp___9;
  _Bool tmp___10;
  int tmp___11;

  {
                                   
                                
                          
                                   
                    
                                  
     
                      
                          
                                     
                      
                                    
       
     
                          
                                   
                    
                                  
     
    sp = (FTS *)malloc(sizeof(FTS));
                                                          
                                  
     
                                       
                            
    sp->fts_options = options;
                              
                           
                              
     
    sp->fts_cwd_fd = -100;
    tmp___4 = fts_maxarglen(argv);
    maxarglen = tmp___4;
                            {
      tmp___5 = maxarglen;
    }       
                             
     
    tmp___6 = fts_palloc(sp, tmp___5);
                   
                
     
                                                            {
      parent = fts_alloc(sp, "", (size_t)0);
                                                                
                  
       
      parent->fts_level = (ptrdiff_t)-1;
    }
                                                              
                  
            
                                   
                    
              
                    
       
     
                                
    root = (FTSENT *)((void *)0);
                       
    while (1) {

      if (!((unsigned long)*argv != (unsigned long)((void *)0))) {
        goto while_break;
      }
      tmp___8 = strlen((char const *)*argv);
      len = tmp___8;
      p = fts_alloc(sp, (char const *)*argv, len);
                                                           
                  
       
                                  
      p->fts_parent = parent;
      p->fts_accpath = p->fts_name;
                      {
                                                               {
          p->fts_info = (unsigned short)11;
          fts_set_stat_required(p, (_Bool)1);
        }       
                                                  
         
      }       
                                                
       
                   
                           
                 
             {
                                                    
        if ((unsigned long)root == (unsigned long)((void *)0)) {
          root = p;
          tmp = root;
        } else {
          tmp->fts_link = p;
                  
        }
      }
      argv++;
               
    }
  while_break:;
                 
                         
                                          
       
     
    tmp___9 = fts_alloc(sp, "", (size_t)0);
    sp->fts_cur = tmp___9;
                                                               
                
     
    (sp->fts_cur)->fts_link = root;
                                                
                             
                    
                
     
                                 
                                     
                                                 
                               
                           
                               
         
       
     
                                      
    return (sp);
  mem3:
    fts_lfree(root);
                         
  mem2:
    free((void *)sp->fts_path);
       
                     
                                
  }
}
static void fts_load(FTS *sp, FTSENT *p) {
  size_t len;
  char *cp;
  size_t tmp;
  char *tmp___0;

  {
    tmp = p->fts_namelen;
                         
    len = tmp;
    memmove((void *)sp->fts_path, (void const *)(p->fts_name), len + 1UL);
                                                   
             
                                                              
             
                                       
                                                                    
                             
              
                        
               
                                         
                                                                      
                               
         
       
     
                           
                          
                             
           
  }
}
__attribute__((__nothrow__)) int(__attribute__((__warn_unused_result__,
                                                __leaf__)) fts_close)(FTS *sp);
int(__attribute__((__warn_unused_result__, __leaf__)) fts_close)(FTS *sp) {
  FTSENT *freep;
  FTSENT *p;
  int saved_errno;
  int *tmp;
  int tmp___0;
  int *tmp___1;
  int tmp___2;
  int *tmp___3;
  int tmp___4;
  int *tmp___5;

  {
    saved_errno = 0;
                      
                      
                 

                                    
                           
         
                  
                                                                       
                          
                
                            
         
                            
       
                
                      
     
                        
                               
     
                                
                               
                                
                                
                                        
                      
                                   
                             
         
       
            
                                   
                                      
                      
                                       
                                 
         
                                     
                      
                                 
                                         
                                   
           
         
       
     
                                    
                                             
                                                    
     
                 
                     
    if (saved_errno) {
                                   
                             
      return (-1);
    }
               
  }
}
extern
    __attribute__((__nothrow__)) int(__attribute__((__nonnull__(2), __leaf__))
                                     fstatfs)(int __fildes,
                                              struct statfs *__buf);
static _Bool dirent_inode_sort_may_be_useful(int dir_fd) {
  struct statfs fs_buf;
  int tmp;

  {
                                   
                   
                        
     
                                     
                         
     
                                  
                         
     
                        
                
                      
                 
                      

    return ((_Bool)0);
  }
}
static _Bool leaf_optimization_applies(int dir_fd) {
  struct statfs fs_buf;
  int tmp;

  {
                                   
                   
                        
     
                                       
                           
     
                        
                  
                      
                 
                      

    return ((_Bool)0);
  }
}
static size_t LCO_hash(void const *x, size_t table_size) {
  struct LCO_ent const *ax;

  {
    ax = (struct LCO_ent const *)x;
    return ((uintmax_t)ax->st_dev % table_size);
  }
}
static _Bool LCO_compare(void const *x, void const *y) {
  struct LCO_ent const *ax;
  struct LCO_ent const *ay;

  {
    ax = (struct LCO_ent const *)x;
    ay = (struct LCO_ent const *)y;
    return ((_Bool)(ax->st_dev == ay->st_dev));
  }
}
static _Bool link_count_optimize_ok(FTSENT const *p) {
  FTS *sp;
  Hash_table *h;
  struct LCO_ent tmp;
  struct LCO_ent *ent;
  _Bool opt_ok;
  struct LCO_ent *t2;
  struct hash_table *tmp___0;

  {
                           
                                           
                                   
                        
     
                                                        {
      tmp___0 =
          hash_initialize((size_t)13, (Hash_tuning const *)((void *)0),
                          &LCO_hash, &LCO_compare, (void (*)(void *))(&free));
                                                   
      h = tmp___0;
                                                          {
        return ((_Bool)0);
      }
    }
                                               
                                                              
                                                              
              
                           
     
                                               
                                                          
                        
     
                                                       
                        
                                               
                                                             
                                                           
                       
                        
     
               

                                                       
                
       
                       
     
               
                    
  }
}
__attribute__((__nothrow__))
FTSENT *(__attribute__((__warn_unused_result__, __leaf__)) fts_read)(FTS *sp);
FTSENT *(__attribute__((__warn_unused_result__, __leaf__)) fts_read)(FTS *sp) {
  FTSENT *p;
  FTSENT *tmp;
  unsigned short instr;
  char *t;
  int *tmp___0;
  int tmp___1;
  int *tmp___2;
  int tmp___3;
  struct _ftsent *tmp___4;
  int tmp___5;
  int tmp___6;
  int tmp___7;
  int tmp___8;
  int tmp___9;
  int *tmp___10;
  int tmp___11;
  size_t tmp___12;
  char *tmp___13;
  FTSENT *parent;
  _Bool tmp___14;
  int *tmp___15;
  _Bool tmp___16;
  int *tmp___17;
  struct _ftsent *tmp___18;
  int *tmp___19;
  int tmp___20;
  int tmp___21;
  int tmp___22;
  int tmp___23;
  int tmp___24;
  int saved_errno;
  int *tmp___25;
  int *tmp___26;
  int *tmp___27;
  int tmp___28;
  int tmp___29;
  int *tmp___30;
  int tmp___31;
  FTSENT *tmp___32;

  {
                                                                   
                                     
            
                                   
                                       
       
     
    p = sp->fts_cur;
                         
                                     
                          
                                              
                 
     
                          
                                   
                
              
                                     
           
                                                  
                                      
                                         
                                                      
                                     
                                
                                             
                                        
                                                
                      
                                                                       
               
             
           
                             
         
       
     
                                
                            
                    
              
                                   
                                                      
                 
                                        
                                  
             
                                
                                       
                                                            
             
                                            
                       
                               
                               
             
                       
                       
           
         
       
                                                                       
                                     
                                   
                                   
                                                        
         
       
                                                                       
                                                                              
                      
                                       
                                  
                                                                 
                            
                     

                                                                    
                                   
             
                                                          
                            
           
                         
         
              
                                   
                                
                                                                   
                                       
                                           
           
                             
                                        
                                              
             
           
                     
                             
                                 
           
                         
                     
         
       
                        
                                                    
                
     
  next:
    tmp = p;
    p = p->fts_link;
    if ((unsigned long)p != (unsigned long)((void *)0)) {
                      
                        
                              {
                                        
                                     
                                      
                                        
                             
                    
                                    
             
                                                  
                        
                  
                                        
                             
                    
                                    
             
                                      
                              
           
                        
                        
                  
                        
           
                
                      
         
                      
                                  
                                         
         
                     
        fts_load(sp, p);
                      
                           
      }
                                   
                  
       
                                   
                                                
                                    
                                       
                                                     
                                    
                               
                                            
                                       
                                              
                    
                                                                     
             
           
         
                                         
       
         
                                            
                                                               
                                                      
              
                                                
       
                                  
                   
          
                            
                                                                            
    check_for_dir:
      sp->fts_cur = p;
                                  {
                                           {
                                 
                                  
                                                      
                                        
                                           
                                                                            
                                  
                                
                   
                        
                              
                 
                      
                            
               
                    
                          
             
                 {
          _L___4:
            p->fts_info = fts_stat(sp, p, (_Bool)0);
                                                               
                                       
                                                   
                                                   
                 
               
             
          }
        }       
                     

                                                   
                      
             
                                 
           
                         
         
      }
                                  
                                 
                                               
         
                                    
                        
                                        
                         
                                         
         
       
      return (p);
    }
    p = tmp->fts_parent;
                    
                      
                             {
                      
      tmp___17 = __errno_location();
      *tmp___17 = 0;
      tmp___18 = (struct _ftsent *)((void *)0);
                             
      return (tmp___18);
    }
               

                                      
                
       
                           
     
                  
                                                    
                             
                                      
                                   
                                    
                                      
                            
                  
                                   
           
                                                 
                       
                
                                      
                            
                  
                                   
           
                                      
                              
         
                       
                       
                
                       
         
              
                     
       
                     
                                      
                                 
                                
       
            
                                  
                                     
                                      
                                                       
                         
                  
                                            
                                
           
                         
                                          
                                    
                                
                                          
                                    
                                          
                                     
                                    
           
         
                            
              
                                       
                                                                     
                         
                                          
                                     
                                    
           
         
       
     
                       
                                      
            
                                      
     
                            
                 
                         
                             
       
                     
     
                                 
                                       
            
                   
     
                      
  }
}
__attribute__((__nothrow__)) int(__attribute__((__leaf__))
                                 fts_set)(FTS *sp __attribute__((__unused__)),
                                          FTSENT *p, int instr);
int(__attribute__((__leaf__)) fts_set)(FTS *sp __attribute__((__unused__)),
                                       FTSENT *p, int instr) {
  int *tmp;

  {
                     
                       
                         
                           
                             
                                       
                        
                         
             
           
         
       
     
                                         
    return (0);
  }
}
static int fts_compare_ino(struct _ftsent const **a, struct _ftsent const **b) {
  int tmp;
  int tmp___0;

  {
                                                                           {
      tmp___0 = -1;
    }       
                                                                              
                
              
                
       
                    
     
    return (tmp___0);
  }
}
static void set_stat_type(struct stat *st, unsigned int dtype) {
  mode_t type;

   
                      
                  
     
                      
                  
     
                      
                  
     
                      
                  
     
                       
                   
     
                      
                  
     
                       
                   
     
                        
         
                         
                      
         
                        
                      
         
                         
                      
         
                        
                      
          
                         
                      
         
                         
                      
          
                         
                      
                 
                     
               
                       
           
   
}
static FTSENT *fts_build(FTS *sp, int type) {
  struct dirent *dp;
  FTSENT *p;
  FTSENT *head;
  size_t nitems;
  FTSENT *cur;
  FTSENT *tail;
  DIR *dirp;
  void *oldaddr;
  int saved_errno;
  _Bool descend;
  _Bool doadjust;
  ptrdiff_t level;
  nlink_t nlinks;
  _Bool nostat;
  size_t len;
  size_t maxlen;
  size_t new_len;
  char *cp;
  int *tmp;
  DIR *tmp___0;
  DIR *tmp___1;
  int *tmp___2;
  _Bool tmp___3;
  int tmp___4;
  int dir_fd;
  int tmp___5;
  int *tmp___6;
  int tmp___7;
  char *tmp___8;
  _Bool is_dir;
  size_t tmp___9;
  int *tmp___10;
  int *tmp___11;
  size_t tmp___12;
  _Bool tmp___13;
  size_t tmp___14;
  size_t tmp___15;
  int *tmp___16;
  _Bool skip_stat;
  int tmp___17;
  int tmp___18;
  int tmp___19;
  int tmp___20;
  int tmp___21;
  int tmp___22;
  int tmp___23;
  int tmp___24;
  int tmp___25;
  int tmp___26;
  _Bool tmp___27;

  {
                      
                                 
                                  
                                                                            
                       
              
                                                                
                       
       
            
                                                              
                     
     
                                                            
                      
                                          
                                 
                              
       
                                     
     
                                  {
                                                  
    }       
                                  
                   
                             
                           
         
                  
                                    
                                     
                       
                                       
                        
                                         
         
       
     
                    
                          
                        
            
                                
                                   
                                     
                        
                  
                        
           
                                                                   
                            
                
                               
                            
         
              
                             
                          
       
     
                 
                  
            
                      
             
                              
                         
                                    
                            
                                       
                                               
           
         
                         
                  
                
                   
                                                                             
                        
             
                         
                              
                                             
                                          
               
             
                                                                       
                               
                           
                                        
                                
                              
               
             
                                      
                  
                               
           
         
              
                           
       
     
                                                                 
                                   
            
                             
     
                              
                              
                   
           
                           
            
                               
     
          
                                   
                                
                        
                                 
                
                       
               

                 
                           
                  
                               
         
              
                             
       
                                    
                                       
                               
                        
                  
                                           
                                   
                            
               
             
           
         
       
                                                   
                                                             
                                                           
                  
       
                                                    
                               
                                       
                                                      
                                                          
                        
             
                                        
                                  
                          
                          
                         
                                            
                                  
                                        
                                  
                                         
         
                                                                    
                              
                                    
                                    
           
         
                                       
       
                                                    
                               
                          
                        
                        
                       
                                          
                                
                                      
                       
                                       
       
                           
                                  
                               
                                         
                                
                                     
                                                                               
              
                                     
       
                                                                        
                                   
                                   
                                    
                                       
                                            
                             
                      
                             
               
                    
                           
             
                  
                         
           
                
                       
         
                                    
                                         
                                                              
                                                      
                                   
                                     
                         
                  
                         
           
                
                       
         
                                 
              
                                                
                                    
                       
                
                                      
                         
                  
                                        
                           
                    
                           
             
           
         
                                 
       
                         
                     
                                    
         
       
                                                  
                                                              
                 
                    
              
                           
                 
       
               
            
     
                   
               
                     
     
                   
                            
     
                              
                                   
             
              
                            
               
         
       
                         
     
                  
                                 
                                   
                                          
                                       
                                        
                                          
                                
                      
                                       
               
                                                     
                           
                    
                                          
                                
                      
                                       
               
                                          
                                  
             
                           
                           
                    
                           
             
                  
                         
           
                              
                
                                                                       
                              
         
                       
                                            
                                  
                          
                                         
         
       
     
                  
                      
                                          
       
                      
                                     
     
                           
                            
                                    
                                                                     
                         
                                              
                                              
                                                              
                                                                           
           
         
       
     
                         
                         
                                          
       
     
                  
  }
}
static unsigned short fts_stat(FTS *sp, FTSENT *p, _Bool follow) {
  struct stat *sbp;
  int saved_errno;
  int *tmp;
  int *tmp___0;
  int *tmp___1;
  int tmp___2;
  int tmp___3;
  int *tmp___4;
  int tmp___5;
  int tmp___6;
  int tmp___7;

  {
    sbp = p->fts_statp;
                             
                                
                          
       
     
                              
              
           {
                   
         
                                                          
                      
                                   
                             
                                       
                              
                                                               
                               
                                           
                           
                                          
             
           
                                     
                   
         
             {
        tmp___5 =
            fstatat(sp->fts_cwd_fd, (char const *)p->fts_accpath, sbp, 256);
        if (tmp___5) {
                                       
                                  
            
                                                      
          return ((unsigned short)10);
        }
      }
    }
                                            
                                 
                    
              
                    
       
                                                                   
                                      
                              
                      
                
                                          
                                  
                   
                                       
                            
                      
                            
               
                                               
             
           
         
       
                                 
     
                                            
                                  
     
                                            
                                 
     
                               
  }
}
static int fts_compar(void const *a, void const *b) {
  FTSENT const **pa;
  FTSENT const **pb;
  int tmp;

  {
    pa = (FTSENT const **)a;
    pb = (FTSENT const **)b;
    tmp = (*(((*(pa + 0))->fts_fts)->fts_compar))(pa, pb);
    return (tmp);
  }
}
static FTSENT *fts_sort(FTS *sp, FTSENT *head, size_t nitems) {
  FTSENT **ap;
  FTSENT *p;
  FTSENT *dummy = 0;
  int (*compare)(void const *, void const *);
  int (*tmp)(void const *, void const *);
  FTSENT **a;
  FTSENT **tmp___0;

  {
                                           
                                                       
                                                                  
              
                          
       
            
                        
     
                  
                                  
                                     
                                                               
                                    
                                                       
                                   
                      
              
                                                     
                                                            
                 
                                      
                                                         
                                     
                        
         
       
                        
     
                       
             
               

               
                         
       
                   
           
                   
                      
     
              
                                                                    
                       
               
               
               
                    
                             
       
                                        
           
     
                  
                                                          
    return (head);
  }
}
static FTSENT *fts_alloc(FTS *sp, char const *name, size_t namelen) {
  FTSENT *p;
  size_t len;

  {
    len = sizeof(FTSENT) + namelen;
    p = (FTSENT *)malloc(len);
                                                         
                                     
     
    memmove((void *)(p->fts_name), (void const *)name, namelen);
                                        
    p->fts_namelen = namelen;
                    
    p->fts_path = sp->fts_path;
                     
                                     
                                     
                       
                               
    return (p);
  }
}
static void fts_lfree(FTSENT *head) {
  FTSENT *p;

   
               
               
               
                         
       
                            
                      
     
               
           
   
}
static _Bool fts_palloc(FTS *sp, size_t more) {
  char *p;
  size_t new_len;
  int *tmp;

  {
                                               
                                    
                                 
                                         
                               
                
                        
     
                              
    p = (char *)realloc((void *)sp->fts_path, sp->fts_pathlen);
                                                         
                                 
                                         
                        
     
    sp->fts_path = p;
    return ((_Bool)1);
  }
}
static void fts_padjust(FTS *sp, FTSENT *head) {
  FTSENT *p;
  char *addr;

   
                        
                      
               

               
                         
       
                 

                                                                            
                                                                 
         
                           
                             
       
                    
                      
     
              
             
               

                                  
                             
       
                 

                                                                            
                                                                 
         
                           
                             
       
                     
                        
                        
              
                          
       
     
                   
           
   
}
static size_t fts_maxarglen(char *const *argv) {
  size_t len;
  size_t max;

  {
                    
              {

                   
                         
       
      len = strlen((char const *)*argv);
                     {
        max = len;
      }
             
    }
  while_break:;
    return (max + 1UL);
  }
}
static int fts_safe_changedir(FTS *sp, FTSENT *p, int fd, char const *dir) {
  int ret;
  _Bool is_dotdot;
  int tmp;
  int tmp___0;
  int newfd;
  int parent_fd;
  _Bool tmp___1;
  struct stat sb;
  int tmp___2;
  int *tmp___3;
  int tmp___4;
  int oerrno;
  int *tmp___5;
  int *tmp___6;

  {
              
                              
                     
                    
              
                    
       
            
                  
     
                               
                              
                                  
                      
                    
         
       
                 
     
                 
                      
                                    
                                                                     
                         
                                                     
                                 
                                 
                             
                                              
             
           
         
       
     
               
                {
      newfd = diropen((FTS const *)sp, dir);
                     {
        return (-1);
      }
    }
                              
              
            
                
                                    
                           
           
                                      
                        
                     
                      
           
                                                    
                                         
                         
                     
                      
                  
                                                      
                                           
                           
                       
                        
             
           
         
       
     
                                
                                                     
                 
     
                        
       
                 
                                   
                        
                   
                                   
                        
     
                 
  }
}
#pragma weak pthread_key_create
#pragma weak pthread_getspecific
#pragma weak pthread_setspecific
#pragma weak pthread_key_delete
#pragma weak pthread_self
#pragma weak pthread_cancel
#pragma weak pthread_mutex_init
#pragma weak pthread_mutex_lock
#pragma weak pthread_mutex_unlock
#pragma weak pthread_mutex_destroy
#pragma weak pthread_rwlock_init
#pragma weak pthread_rwlock_rdlock
#pragma weak pthread_rwlock_wrlock
#pragma weak pthread_rwlock_unlock
#pragma weak pthread_rwlock_destroy
#pragma weak pthread_once
#pragma weak pthread_cond_init
#pragma weak pthread_cond_wait
#pragma weak pthread_cond_signal
#pragma weak pthread_cond_broadcast
#pragma weak pthread_cond_destroy
#pragma weak pthread_mutexattr_init
#pragma weak pthread_mutexattr_settype
#pragma weak pthread_mutexattr_destroy
#pragma weak pthread_self
#pragma weak pthread_cancel
char const *Version = "8.2";
extern
    __attribute__((__nothrow__)) int(__attribute__((__nonnull__(2), __leaf__))
                                     fchownat)(int __fd, char const *__file,
                                               __uid_t __owner, __gid_t __group,
                                               int __flag);
__inline static int chownat(int fd, char const *file, uid_t owner,
                            gid_t group) {
  int tmp;

  {
    tmp = fchownat(fd, file, owner, group, 0);
    return (tmp);
  }
}
__inline static int lchownat(int fd, char const *file, uid_t owner,
                             gid_t group) {
  int tmp;

  {
    tmp = fchownat(fd, file, owner, group, 256);
    return (tmp);
  }
}
extern void chopt_init(struct Chown_option *chopt);
extern void chopt_free(struct Chown_option *chopt __attribute__((__unused__)));
extern char *gid_to_name(gid_t gid);
extern char *uid_to_name(uid_t uid);
extern _Bool chown_files(char **files, int bit_flags, uid_t uid, gid_t gid,
                         uid_t required_uid, gid_t required_gid,
                         struct Chown_option const *chopt);
__inline static void ignore_ptr(void *p) {

   

           
   

}
extern void chopt_init(struct Chown_option *chopt) {

   

                                         
                                                        
                                              
                              
                                   
                                           
                                            
           
   

}
extern void chopt_free(struct Chown_option *chopt __attribute__((__unused__))) {

   

           
   

}
extern char *gid_to_name(gid_t gid) {
  char
      buf___1[((((sizeof(intmax_t) * 8UL - 1UL) * 146UL) / 485UL + 1UL) + 1UL) +
              1UL];
  struct group *grp;
  struct group *tmp;
  char *tmp___1;
  char *tmp___2;
  char *tmp___3;

  {
    tmp = getgrgid(gid);
    grp = tmp;
             {
      tmp___2 = grp->gr_name;
    }       
                                                   
                        
     
    tmp___3 = xstrdup((char const *)tmp___2);
    return (tmp___3);
  }
}
extern char *uid_to_name(uid_t uid) {
  char
      buf___1[((((sizeof(intmax_t) * 8UL - 1UL) * 146UL) / 485UL + 1UL) + 1UL) +
              1UL];
  struct passwd *pwd;
  struct passwd *tmp;
  char *tmp___1;
  char *tmp___2;
  char *tmp___3;

  {
    tmp = getpwuid(uid);
    pwd = tmp;
             {
      tmp___2 = pwd->pw_name;
    }       
                                                   
                        
     
    tmp___3 = xstrdup((char const *)tmp___2);
    return (tmp___3);
  }
}
static void describe_change(char const *file, enum Change_status changed,
                            char const *user, char const *group) {
  char const *fmt;
  char const *spec;
  char *spec_allocated;
  char const *tmp;
  char *tmp___0;
  size_t tmp___1;
  size_t tmp___2;
  char *tmp___3;
  char *tmp___4;
  char *tmp___5;
  char *tmp___6;
  char *tmp___7;
  char *tmp___8;
  char *tmp___9;
  char *tmp___10;
  char *tmp___11;
  char *tmp___12;
  char *tmp___13;
  char *tmp___14;
  char *tmp___15;
  char *tmp___16;
  char const *tmp___17;

  {
                                         
                                      
                        
               
                                                                              
                                         
             
     
              {
      if (group) {
        tmp___1 = strlen(user);
        tmp___2 = strlen(group);
        spec_allocated = (char *)xmalloc(((tmp___1 + 1UL) + tmp___2) + 1UL);
        tmp___3 = stpcpy(spec_allocated, user);
        tmp___4 = stpcpy(tmp___3, ":");
        stpcpy(tmp___4, group);
        spec = (char const *)spec_allocated;
      } else {
        spec = user;
      }
    }       
                   
     
    if ((unsigned int)changed == 2U) {
      goto case_2;
    }
    if ((unsigned int)changed == 3U) {
      goto case_3;
    }
                                     {
      goto case_4;
    }
                        
  case_2:
              {
      tmp___5 = gettext("changed ownership of %s to %s\n");
      fmt = (char const *)tmp___5;
    }       
                  
                                                         
                          
              
                                                            
                          
       
                                  
     
    goto switch_break;
  case_3:
              {
      tmp___9 = gettext("failed to change ownership of %s to %s\n");
      fmt = (char const *)tmp___9;
    }       
                  
                                                                   
                            
              
                                                                 
                            
       
                                   
     
    goto switch_break;
  case_4:
              {
      tmp___13 = gettext("ownership of %s retained as %s\n");
      fmt = (char const *)tmp___13;
    }       
                  
                                                           
                            
              
                                                         
                            
       
                                   
     
                      
                 
            
  switch_break:
    tmp___17 = quote(file);
    printf(fmt, tmp___17, spec);
                                 
           
  }
}
static enum RCH_status restricted_chown(int cwd_fd, char const *file,
                                        struct stat const *orig_st, uid_t uid,
                                        gid_t gid, uid_t required_uid,
                                        gid_t required_gid) {
  enum RCH_status status;
  struct stat st;
  int open_flags;
  int fd;
  int tmp___0;
  int *tmp___1;
  int *tmp___2;
  int tmp___4;
  int tmp___5;
  int tmp___6;
  int saved_errno;
  int *tmp___7;
  int *tmp___8;

  {
                                
                      
                                     {
                                       {
        return ((enum RCH_status)5);
      }
    }
                                                   
                                                  
                            
              
                                    
       
     
                                          
                     
                                   
                           
                                                    
                                                    
                           
                        
           
                
                      
         
              
             
                                     
                             
                      
                
                      
         
                                          
       
     
                             
                       
                                  
            
                                                        
                                                          
                                            
                        
                  
                                            
                   
                                                
                            
                      
                                                
                       
                                                 
                                     
                                        
                                       
                                                  
                            
                                                  
                     
                                    
                          
                                                
                   
                 
               
             
           
                
                                      
         
              
                                    
       
     
                                 
                           
              
                                 
                           
                    
  }
}
static _Bool change_file_owner(FTS *fts, FTSENT *ent, uid_t uid, gid_t gid,
                               uid_t required_uid, gid_t required_gid,
                               struct Chown_option const *chopt) {
  char const *file_full_name;
  char const *file;
  struct stat const *file_stats;
  struct stat stat_buf;
  _Bool ok;
  _Bool do_chown;
  _Bool symlink_changed;
  char const *tmp;
  char *tmp___0;
  char const *tmp___1;
  char const *tmp___2;
  char *tmp___3;
  int tmp___4;
  char *tmp___5;
  FTSENT *tmp___6;
  char const *tmp___7;
  char *tmp___8;
  char const *tmp___9;
  char *tmp___10;
  char const *tmp___11;
  char *tmp___12;
  char const *tmp___13;
  char *tmp___14;
  _Bool tmp___15;
  char const *tmp___16;
  char *tmp___17;
  int *tmp___18;
  int tmp___19;
  int tmp___20;
  char const *tmp___21;
  char *tmp___22;
  char const *tmp___23;
  char const *tmp___24;
  char *tmp___25;
  int tmp___26;
  char *tmp___27;
  int tmp___28;
  int *tmp___29;
  enum RCH_status err;
  enum RCH_status tmp___30;
  int tmp___31;
  char const *tmp___32;
  char *tmp___33;
  char *tmp___34;
  char *tmp___35;
  int *tmp___36;
  _Bool changed;
  int tmp___37;
  enum Change_status ch_status;
  int tmp___38;
  int tmp___39;
  int tmp___40;

  {
    file_full_name = (char const *)ent->fts_path;
    file = (char const *)ent->fts_accpath;
    ok = (_Bool)1;
    symlink_changed = (_Bool)1;
                                  
                  
     
                                  
                  
     
                                   
                   
     
                                  
                  
     
                                  
                  
     
                                  
                  
     
                        
         
                         
                                
                                                                        
                                                                          
                       
                                                    
                                 
                                            
                         
                                                                            
                                                        
                      
                                          
                                                     
                                                                              
                                                     
                                                                     
               
                       
                                                                              
                                                 
                               
             
                      
                                 
                                    
                                        
                              
           
         
       
                        
     
                      
         
                          
                        
     
                      
          
                               
                                  
                             
                             
                          
       
     
                               
                                      
                                            
                                                               
     
                  
                      
         
                               
                                      
                               
                                                                
     
                  
                      
         
                               
                                       
                                                     
                                                                 
     
                  
                      
         
                                                                             
                   
                 
                                         
                           
                                                                            
                                                                               
                                                                               
                                                      
                             
       
                     
                        
     
                      
                 
                      
                
              
                          
                                                    
           {
                                        
                                          
                                                           
                                       
                                                    
                                    
                                                                   
                      
                            
               
                    
                          
             
                  
                        
           
                
                      
         
             {
      _L___3:
        file_stats = (struct stat const *)(ent->fts_statp);
                                             
                                                         
                                                                    
                                
                                         
                                                 
                                                            
                                              
                                                                      
               
                            
             
                                                          
           
         
                 
                                            
                    
                  
                                                            
               
                                                
                             
                      
                                                                
                               
                        
                               
                 
               
                    
                           
             
           
                
                       
         
                                   
      }
    }
             
                                    
                    
              
                                      
                      
                
                                        
                        
                  
                                          
                   
                                        
                                         
                                                                   
                                           
                                                                     
                               
                                                             
                                          
                                                         
                                           
                                                                            
                                                                      
                              
                                                   
                                                              
                                                                        
                                                                             
                                                                                
                       
                                         
                                                                              
                                                          
                                           
                     
                                   
                                      
                   
                 
               
             
           
         
       
     
                  {
                                           {
        tmp___28 = lchownat(fts->fts_cwd_fd, file, uid, gid);
        ok = (_Bool)(tmp___28 == 0);
                  
                                        
                                
                          
                                       
           
         
      }       
                                                                                
                                                                
                       
                                      
                          
         
                                      
                      
         
                                      
                          
         
                                      
                          
         
                                      
                          
         
                                
                 
                              
             
                                                            
                                    
                              
                 
                      
                              
                 
                            
                      
                              
                         
                
                        
       
                    {
        if (!ok) {
                                    {
            tmp___32 = quote(file_full_name);
                                    {
              tmp___33 = gettext("changing ownership of %s");
              tmp___35 = tmp___33;
            }       
                                                         
                                  
             
            tmp___36 = __errno_location();
            error(0, *tmp___36, (char const *)tmp___35, tmp___32);
          }
        }
      }
    }
                                                    {
                    {
                {
                               {
                                     
                          
                   {
                                                    {
              _L___5:
                if (gid == 4294967295U) {
                               
                } else {
                                                         
                                 
                         {
                    tmp___37 = 1;
                  }
                }
              }       
                             
               
            }
          }       
                         
           
        }       
                       
         
      }       
                     
       
      changed = (_Bool)tmp___37;
      if (changed) {
        goto _L___6;
      } else {
        if ((unsigned int const)chopt->verbosity == 0U) {
        _L___6:
          if (!ok) {
            tmp___40 = 3;
          } else {
                                   
                           
                   {
              if (!changed) {
                             
              } else {
                tmp___38 = 2;
              }
              tmp___39 = tmp___38;
            }
            tmp___40 = tmp___39;
          }
          ch_status = (enum Change_status)tmp___40;
          describe_change(file_full_name, ch_status,
                          (char const *)chopt->user_name,
                          (char const *)chopt->group_name);
        }
      }
    }
                          
                           
     
    return (ok);
  }
}
extern _Bool chown_files(char **files, int bit_flags, uid_t uid, gid_t gid,
                         uid_t required_uid, gid_t required_gid,
                         struct Chown_option const *chopt) {
  _Bool ok;
  int stat_flags;
  int tmp;
  FTS *fts;
  FTS *tmp___0;
  FTSENT *ent;
  char *tmp___1;
  int *tmp___2;
  int *tmp___3;
  _Bool tmp___4;
  char *tmp___5;
  int *tmp___6;
  int tmp___7;

  {
    ok = (_Bool)1;
                                     {
      tmp = 0;
    }       
                                        
                
              
                                             
                  
                
                                                           
                    
                  
                    
           
         
       
     
    stat_flags = tmp;
    tmp___0 = xfts_open((char *const *)files, bit_flags | stat_flags,
                        (int (*)(FTSENT const **, FTSENT const **))((void *)0));
    fts = tmp___0;
    while (1) {
      ent = fts_read(fts);
      if ((unsigned long)ent == (unsigned long)((void *)0)) {
                                     
                            
                                     
                                                 
                                         
                                                      
           
                        
         
        goto while_break;
      }
      tmp___4 = change_file_owner(fts, ent, uid, gid, required_uid,
                                  required_gid, chopt);
      ok = (_Bool)((int)ok & (int)tmp___4);
    }
  while_break:
    tmp___7 = fts_close(fts);
                       
                                            
                                   
                                                
                    
     
    return (ok);
  }
}
extern char *optarg;
extern
    __attribute__((__nothrow__)) int(__attribute__((__nonnull__(1), __leaf__))
                                     atexit)(void (*__func)(void));
extern
    __attribute__((__nothrow__)) char *(__attribute__((__leaf__))
                                        textdomain)(char const *__domainname);
extern __attribute__((__nothrow__)) char *(__attribute__((
    __leaf__)) bindtextdomain)(char const *__domainname, char const *__dirname);
__inline static void emit_ancillary_info(void) {
  char *tmp;
  char *tmp___0;
  char *tmp___1;
  char *tmp___2;
  char const *lc_messages;
  char const *tmp___3;
  char *tmp___4;
  char *tmp___5;
  int tmp___6;
  char *tmp___7;
  char *tmp___8;

   
                                       
                                                  
                                                                
                                                                           
                                                                
                      
                                                                            
                                                  
                                                                    
                          
                      
                                                       
                    
                                               
                                                          
                                                                     
                                               
       
     
                                           
                      
                                                                               
                                           
           
   
}
__inline static char *bad_cast(char const *s) {

   

    return ((char *)s);
   

}
static char *reference_file;
static struct option const long_options___1[14] = {
    {"recursive", 0, (int *)((void *)0), 'R'},
    {"changes", 0, (int *)((void *)0), 'c'},
    {"dereference", 0, (int *)((void *)0), 128},
    {"from", 1, (int *)((void *)0), 129},
    {"no-dereference", 0, (int *)((void *)0), 'h'},
    {"no-preserve-root", 0, (int *)((void *)0), 130},
    {"preserve-root", 0, (int *)((void *)0), 131},
    {"quiet", 0, (int *)((void *)0), 'f'},
    {"silent", 0, (int *)((void *)0), 'f'},
    {"reference", 1, (int *)((void *)0), 132},
    {"verbose", 0, (int *)((void *)0), 'v'},
    {"help", 0, (int *)((void *)0), -130},
    {"version", 0, (int *)((void *)0), -131},
    {(char const *)((void *)0), 0, (int *)((void *)0), 0}};
__attribute__((__noreturn__)) void usage(int status);
void usage(int status) {
  char *tmp;
  char *tmp___0;
  char *tmp___1;
  char *tmp___2;
  char *tmp___3;
  char *tmp___4;
  char *tmp___5;
  char *tmp___6;
  char *tmp___7;
  char *tmp___8;
  char *tmp___9;
  char *tmp___10;

   
                      
                                                                
                                                       
            
                                                                             
                                                                           
                                                                
                        
                                                                       
                                                                              
                                                                            
                                                                               
                                                                                
                                                                    
                                                    
                                                                             
                                                                              
                                                                                
                                                                          
                                                    
               
                                                                                
                                                                                
                                                                           
                                                                           
                                                                                
                                                                             
                                                    
                                                                                
                                                                          
                                                         
                                                    
                        
                                                                         
                                                                                
                                                                                
                                                                                
                                                                       
                                                    
                        
                                                                               
                                                                               
                                                                          
                                                                               
                                                                           
                                                                         
                                                                               
                                 
                                                    
                                                                         
                                                    
               
                                                                            
                                                    
                                                                               
                                                                                
                                                                              
                                                                 
                                                    
                
                                                                               
                                                                             
                                                                            
                                                             
                                                                               
                            
     
                 
   
}
static struct dev_ino dev_ino_buf;
int main(int argc, char **argv) {
  _Bool preserve_root;
  uid_t uid;
  gid_t gid;
  uid_t required_uid;
  gid_t required_gid;
  int bit_flags;
  int dereference;
  struct Chown_option chopt;
  _Bool ok;
  int optc;
  char *u_dummy;
  char *g_dummy;
  char const *e;
  char const *tmp;
  char const *tmp___0;
  char *tmp___1;
  char *tmp___2;
  char const *tmp___3;
  char *tmp___4;
  int tmp___5;
  struct stat ref_stats;
  char const *tmp___6;
  char *tmp___7;
  int *tmp___8;
  int tmp___9;
  char const *e___0;
  char const *tmp___10;
  char const *tmp___11;
  char const *tmp___12;
  char *tmp___13;
  int *tmp___14;
  int tmp___15;

  {
                             
    uid = (uid_t)-1;
    gid = (gid_t)-1;
                             
                             
                   
                     
                                                
                     
                                                           
                            
                          
                       
    while (1) {
      optc = getopt_long(argc, (char *const *)argv, "HLPRcfhv",
                         long_options___1, (int *)((void *)0));
      if (!(optc != -1)) {
        goto while_break;
      }
                       
                     
       
                       
                     
       
                       
                     
       
                        
                      
       
                        
                      
       
                        
                      
       
      if (optc == 131) {
                      
      }
      if (optc == 132) {
        goto case_132;
      }
                        
                      
       
                       
                     
       
                       
                     
       
                        
                      
       
      if (optc == 118) {
        goto case_118;
      }
      if (optc == -130) {
                          
      }
                         
                          
       
                          
            
                     
                        
            
                    
                        
            
                     
      goto switch_break;
             
                      
                        
             
                      
      goto switch_break;
    case_130:
      preserve_root = (_Bool)0;
      goto switch_break;
    case_131:
      preserve_root = (_Bool)1;
      goto switch_break;
    case_132:
      reference_file = optarg;
      goto switch_break;
    case_129:
      tmp = parse_user_spec((char const *)optarg, &required_uid, &required_gid,
                            &u_dummy, &g_dummy);
      e = tmp;
              
                                              
                                          
       
                        
            
                               
                        
            
                                          
                        
             
                                    
      goto switch_break;
    case_118:
      chopt.verbosity = (enum Verbosity)0;
      goto switch_break;
    case_neg_130:
      usage(0);
                        
                 
                                                                               
                                                       
              
                        
                   
               
    switch_break:;
    }
  while_break:;
                       {
      if (bit_flags == 16) {
        if (dereference == 1) {
          tmp___1 = gettext("-R --dereference requires either -H or -L");
          error(1, 0, (char const *)tmp___1);
        }
        dereference = 0;
      }
    }       
                     
     
    chopt.affect_symlink_referent = (_Bool)(dereference != 0);
                         
                  
            
                  
     
                                  
                           
                                             
                                           
              
                                                            
                                                      
                                                    
       
               
     
    if (reference_file) {
      tmp___9 = stat((char const *)reference_file, &ref_stats);
                    
                                                      
                                                            
                                     
                                                           
       
      uid = ref_stats.st_uid;
      gid = ref_stats.st_gid;
      chopt.user_name = uid_to_name(ref_stats.st_uid);
      chopt.group_name = gid_to_name(ref_stats.st_gid);
    } else {
      tmp___10 = parse_user_spec((char const *)*(argv + optind), &uid, &gid,
                                 &chopt.user_name, &chopt.group_name);
      e___0 = tmp___10;
      if (e___0) {
        tmp___11 = quote((char const *)*(argv + optind));
        error(1, 0, "%s: %s", e___0, tmp___11);
      }
      if (!chopt.user_name) {
                              {
          chopt.user_name = bad_cast("");
        }
      }
      optind++;
    }
    if (chopt.recurse) {
      if (preserve_root) {
        chopt.root_dev_ino = get_root_dev_ino(&dev_ino_buf);
        if ((unsigned long)chopt.root_dev_ino == (unsigned long)((void *)0)) {
                                
          tmp___13 = gettext("failed to get attributes of %s");
                                        
                                                                
        }
      }
    }
    bit_flags |= 1024;
    ok = chown_files(argv + optind, bit_flags, uid, gid, required_uid,
                     required_gid, (struct Chown_option const *)(&chopt));
    chopt_free(&chopt);
    if (ok) {
      tmp___15 = 0;
    } else {
      tmp___15 = 1;
    }
    exit(tmp___15);
  }
}
