typedef long __time_t;
typedef long __syscall_slong_t;
typedef __time_t time_t;
struct timespec {
  __time_t tv_sec;
  __syscall_slong_t tv_nsec;
};
typedef unsigned long size_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef __uid_t uid_t;
struct stat {
  __dev_t st_dev;
  __ino_t st_ino;
  __nlink_t st_nlink;
  __mode_t st_mode;
  __uid_t st_uid;
  __gid_t st_gid;
  int __pad0;
  __dev_t st_rdev;
  __off_t st_size;
  __blksize_t st_blksize;
  __blkcnt_t st_blocks;
  struct timespec st_atim;
  struct timespec st_mtim;
  struct timespec st_ctim;
  __syscall_slong_t __glibc_reserved[3];
};
typedef __ino_t ino_t;
typedef __dev_t dev_t;
struct hash_tuning {
  float shrink_threshold;
  float shrink_factor;
  float growth_threshold;
  float growth_factor;
  _Bool is_n_buckets;
};
typedef struct hash_tuning Hash_tuning;
struct hash_table;
struct hash_table;
struct hash_table;
typedef struct hash_table Hash_table;
typedef unsigned long uintmax_t;
typedef long __off64_t;
struct _IO_FILE;
struct _IO_FILE;
struct _IO_FILE;
typedef struct _IO_FILE FILE;
typedef void _IO_lock_t;
struct _IO_marker {
  struct _IO_marker *_next;
  struct _IO_FILE *_sbuf;
  int _pos;
};
struct _IO_FILE {
  int _flags;
  char *_IO_read_ptr;
  char *_IO_read_end;
  char *_IO_read_base;
  char *_IO_write_base;
  char *_IO_write_ptr;
  char *_IO_write_end;
  char *_IO_buf_base;
  char *_IO_buf_end;
  char *_IO_save_base;
  char *_IO_backup_base;
  char *_IO_save_end;
  struct _IO_marker *_markers;
  struct _IO_FILE *_chain;
  int _fileno;
  int _flags2;
  __off_t _old_offset;
  unsigned short _cur_column;
  signed char _vtable_offset;
  char _shortbuf[1];
  _IO_lock_t *_lock;
  __off64_t _offset;
  void *__pad1;
  void *__pad2;
  void *__pad3;
  void *__pad4;
  size_t __pad5;
  int _mode;
  char _unused2[(15UL * sizeof(int) - 4UL * sizeof(void *)) - sizeof(size_t)];
};
typedef __off_t off_t;
typedef long __ssize_t;
typedef __ssize_t ssize_t;
struct allocator;
struct allocator;
struct allocator;
struct allocator {
  void *(*allocate)(size_t);
  void *(*reallocate)(void *, size_t);
  void (*free)(void *);
  void (*die)(size_t);
};
typedef long ptrdiff_t;
typedef __builtin_va_list __gnuc_va_list;
typedef __gnuc_va_list va_list;
enum quoting_style {
  literal_quoting_style = 0,
  shell_quoting_style = 1,
  shell_always_quoting_style = 2,
  c_quoting_style = 3,
  c_maybe_quoting_style = 4,
  escape_quoting_style = 5,
  locale_quoting_style = 6,
  clocale_quoting_style = 7,
  custom_quoting_style = 8
};
enum strtol_error {
  LONGINT_OK = 0,
  LONGINT_OVERFLOW = 1,
  LONGINT_INVALID_SUFFIX_CHAR = 2,
  LONGINT_INVALID_SUFFIX_CHAR_WITH_OVERFLOW = 3,
  LONGINT_INVALID = 4
};
typedef enum strtol_error strtol_error;
struct option {
  char const *name;
  int has_arg;
  int *flag;
  int val;
};
typedef unsigned int uint32_t;
typedef int __pid_t;
typedef __pid_t pid_t;
typedef int wchar_t;
typedef int nl_item;
typedef unsigned int wint_t;
typedef long __suseconds_t;
struct timeval {
  __time_t tv_sec;
  __suseconds_t tv_usec;
};
union __anonunion___value_23 {
  unsigned int __wch;
  char __wchb[4];
};
struct __anonstruct___mbstate_t_22 {
  int __count;
  union __anonunion___value_23 __value;
};
typedef struct __anonstruct___mbstate_t_22 __mbstate_t;
typedef __mbstate_t mbstate_t;
struct mbchar {
  char const *ptr;
  size_t bytes;
  _Bool wc_valid;
  wchar_t wc;
  char buf[24];
};
struct __anonstruct___sigset_t_9 {
  unsigned long __val[1024UL / (8UL * sizeof(unsigned long))];
};
typedef struct __anonstruct___sigset_t_9 __sigset_t;
typedef __sigset_t sigset_t;
struct timezone {
  int tz_minuteswest;
  int tz_dsttime;
};
struct obstack;
struct obstack;
struct obstack;
struct _obstack_chunk {
  char *limit;
  struct _obstack_chunk *prev;
  char contents[4];
};
union __anonunion_temp_26 {
  long tempint;
  void *tempptr;
};
struct obstack {
  long chunk_size;
  struct _obstack_chunk *chunk;
  char *object_base;
  char *next_free;
  char *chunk_limit;
  union __anonunion_temp_26 temp;
  int alignment_mask;
  struct _obstack_chunk *(*chunkfun)(void *, long);
  void (*freefun)(void *, struct _obstack_chunk *);
  void *extra_arg;
  unsigned int use_extra_arg : 1;
  unsigned int maybe_empty_object : 1;
  unsigned int alloc_failed : 1;
};
struct Tokens {
  size_t n_tok;
  char **tok;
  size_t *tok_len;
  struct obstack o_data;
  struct obstack o_tok;
  struct obstack o_tok_len;
};
struct randread_source;
struct randread_source;
struct randread_source;
typedef unsigned long uint_least64_t;
typedef struct timezone *__restrict __timezone_ptr_t;
typedef uint_least64_t isaac_word;
struct isaac_state {
  isaac_word m[1 << 8];
  isaac_word a;
  isaac_word b;
  isaac_word c;
};
union __anonunion_data_31 {
  isaac_word w[1 << 8];
  unsigned char b[(unsigned long)(1 << 8) * sizeof(isaac_word)];
};
struct isaac {
  size_t buffered;
  struct isaac_state state;
  union __anonunion_data_31 data;
};
union __anonunion_buf_30 {
  char c[2UL * ((unsigned long)(1 << 8) * sizeof(isaac_word))];
  struct isaac isaac;
};
struct randread_source {
  FILE *source;
  void (*handler)(void const *);
  void const *handler_arg;
  union __anonunion_buf_30 buf;
};
struct quoting_options;
struct quoting_options;
struct quoting_options;
struct quoting_options {
  enum quoting_style style;
  int flags;
  unsigned int quote_these_too[255UL / (sizeof(int) * 8UL) + 1UL];
  char const *left_quote;
  char const *right_quote;
};
struct slotvec {
  size_t size;
  char *val;
};
struct mbuiter_multi {
  _Bool in_shift;
  mbstate_t state;
  _Bool next_done;
  struct mbchar cur;
};
typedef struct mbuiter_multi mbui_iterator_t;
enum nproc_query {
  NPROC_ALL = 0,
  NPROC_CURRENT = 1,
  NPROC_CURRENT_OVERRIDABLE = 2
};
typedef unsigned long __cpu_mask;
struct __anonstruct_cpu_set_t_24 {
  __cpu_mask __bits[1024UL / (8UL * sizeof(__cpu_mask))];
};
typedef struct __anonstruct_cpu_set_t_24 cpu_set_t;
struct md5_ctx {
  uint32_t A;
  uint32_t B;
  uint32_t C;
  uint32_t D;
  uint32_t total[2];
  uint32_t buflen;
  uint32_t buffer[32];
};
struct __pthread_internal_list {
  struct __pthread_internal_list *__prev;
  struct __pthread_internal_list *__next;
};
typedef struct __pthread_internal_list __pthread_list_t;
struct __pthread_mutex_s {
  int __lock;
  unsigned int __count;
  int __owner;
  unsigned int __nusers;
  int __kind;
  short __spins;
  short __elision;
  __pthread_list_t __list;
};
union __anonunion_pthread_mutex_t_11 {
  struct __pthread_mutex_s __data;
  char __size[40];
  long __align;
};
typedef union __anonunion_pthread_mutex_t_11 pthread_mutex_t;
union __anonunion_pthread_mutexattr_t_12 {
  char __size[4];
  int __align;
};
typedef union __anonunion_pthread_mutexattr_t_12 pthread_mutexattr_t;
struct lconv {
  char *decimal_point;
  char *thousands_sep;
  char *grouping;
  char *int_curr_symbol;
  char *currency_symbol;
  char *mon_decimal_point;
  char *mon_thousands_sep;
  char *mon_grouping;
  char *positive_sign;
  char *negative_sign;
  char int_frac_digits;
  char frac_digits;
  char p_cs_precedes;
  char p_sep_by_space;
  char n_cs_precedes;
  char n_sep_by_space;
  char p_sign_posn;
  char n_sign_posn;
  char int_p_cs_precedes;
  char int_p_sep_by_space;
  char int_n_cs_precedes;
  char int_n_sep_by_space;
  char int_p_sign_posn;
  char int_n_sign_posn;
};
struct heap;
struct heap;
struct heap;
struct heap {
  void **array;
  size_t capacity;
  size_t count;
  int (*compare)(void const *, void const *);
};
struct hash_entry {
  void *data;
  struct hash_entry *next;
};
struct hash_table {
  struct hash_entry *bucket;
  struct hash_entry const *bucket_limit;
  size_t n_buckets;
  size_t n_buckets_used;
  size_t n_entries;
  Hash_tuning const *tuning;
  size_t (*hasher)(void const *, size_t);
  _Bool (*comparator)(void const *, void const *);
  void (*data_freer)(void *);
  struct hash_entry *free_entry_list;
};
struct F_triple {
  char *name;
  ino_t st_ino;
  dev_t st_dev;
};
typedef long __clock_t;
union sigval {
  int sival_int;
  void *sival_ptr;
};
typedef union sigval sigval_t;
typedef __clock_t __sigchld_clock_t;
struct __anonstruct__kill_23 {
  __pid_t si_pid;
  __uid_t si_uid;
};
struct __anonstruct__timer_24 {
  int si_tid;
  int si_overrun;
  sigval_t si_sigval;
};
struct __anonstruct__rt_25 {
  __pid_t si_pid;
  __uid_t si_uid;
  sigval_t si_sigval;
};
struct __anonstruct__sigchld_26 {
  __pid_t si_pid;
  __uid_t si_uid;
  int si_status;
  __sigchld_clock_t si_utime;
  __sigchld_clock_t si_stime;
};
struct __anonstruct__sigfault_27 {
  void *si_addr;
  short si_addr_lsb;
};
struct __anonstruct__sigpoll_28 {
  long si_band;
  int si_fd;
};
struct __anonstruct__sigsys_29 {
  void *_call_addr;
  int _syscall;
  unsigned int _arch;
};
union __anonunion__sifields_22 {
  int _pad[128UL / sizeof(int) - 4UL];
  struct __anonstruct__kill_23 _kill;
  struct __anonstruct__timer_24 _timer;
  struct __anonstruct__rt_25 _rt;
  struct __anonstruct__sigchld_26 _sigchld;
  struct __anonstruct__sigfault_27 _sigfault;
  struct __anonstruct__sigpoll_28 _sigpoll;
  struct __anonstruct__sigsys_29 _sigsys;
};
struct __anonstruct_siginfo_t_21 {
  int si_signo;
  int si_errno;
  int si_code;
  union __anonunion__sifields_22 _sifields;
};
typedef struct __anonstruct_siginfo_t_21 siginfo_t;
union __anonunion___sigaction_handler_41 {
  void (*sa_handler)(int);
  void (*sa_sigaction)(int, siginfo_t *, void *);
};
struct sigaction {
  union __anonunion___sigaction_handler_41 __sigaction_handler;
  __sigset_t sa_mask;
  int sa_flags;
  void (*sa_restorer)(void);
};
enum __anonenum_fadvice_t_19 {
  FADVISE_NORMAL = 0,
  FADVISE_SEQUENTIAL = 2,
  FADVISE_NOREUSE = 5,
  FADVISE_DONTNEED = 4,
  FADVISE_WILLNEED = 3,
  FADVISE_RANDOM = 1
};
typedef enum __anonenum_fadvice_t_19 fadvice_t;
typedef unsigned long __rlim_t;
typedef unsigned long pthread_t;
union pthread_attr_t {
  char __size[56];
  long __align;
};
typedef union pthread_attr_t pthread_attr_t;
struct __anonstruct___data_7 {
  int __lock;
  unsigned int __futex;
  unsigned long long __total_seq;
  unsigned long long __wakeup_seq;
  unsigned long long __woken_seq;
  void *__mutex;
  unsigned int __nwaiters;
  unsigned int __broadcast_seq;
};
union __anonunion_pthread_cond_t_6 {
  struct __anonstruct___data_7 __data;
  char __size[48];
  long long __align;
};
typedef union __anonunion_pthread_cond_t_6 pthread_cond_t;
union __anonunion_pthread_condattr_t_8 {
  char __size[4];
  int __align;
};
typedef union __anonunion_pthread_condattr_t_8 pthread_condattr_t;
typedef void (*__sighandler_t)(int);
enum __rlimit_resource {
  RLIMIT_CPU = 0,
  RLIMIT_FSIZE = 1,
  RLIMIT_DATA = 2,
  RLIMIT_STACK = 3,
  RLIMIT_CORE = 4,
  __RLIMIT_RSS = 5,
  RLIMIT_NOFILE = 7,
  __RLIMIT_OFILE = 7,
  RLIMIT_AS = 9,
  __RLIMIT_NPROC = 6,
  __RLIMIT_MEMLOCK = 8,
  __RLIMIT_LOCKS = 10,
  __RLIMIT_SIGPENDING = 11,
  __RLIMIT_MSGQUEUE = 12,
  __RLIMIT_NICE = 13,
  __RLIMIT_RTPRIO = 14,
  __RLIMIT_RTTIME = 15,
  __RLIMIT_NLIMITS = 16,
  __RLIM_NLIMITS = 16
};
typedef __rlim_t rlim_t;
struct rlimit {
  rlim_t rlim_cur;
  rlim_t rlim_max;
};
typedef enum __rlimit_resource __rlimit_resource_t;
enum blanktype {
  bl_start = 0, bl_end = 1, bl_both = 2
};
struct line {
  char *text;
  size_t length;
  char *keybeg;
  char *keylim;
};
struct buffer {
  char *buf;
  size_t used;
  size_t nlines;
  size_t alloc;
  size_t left;
  size_t line_bytes;
  _Bool eof;
};
struct keyfield {
  size_t sword;
  size_t schar;
  size_t eword;
  size_t echar;
  _Bool const *ignore;
  char const *translate;
  _Bool skipsblanks;
  _Bool skipeblanks;
  _Bool numeric;
  _Bool random;
  _Bool general_numeric;
  _Bool human_numeric;
  _Bool month;
  _Bool reverse;
  _Bool version;
  _Bool obsolete_used;
  struct keyfield *next;
};
struct month {
  char const *name;
  int val;
};
struct merge_node {
  struct line *lo;
  struct line *hi;
  struct line *end_lo;
  struct line *end_hi;
  struct line **dest;
  size_t nlo;
  size_t nhi;
  struct merge_node *parent;
  struct merge_node *lo_child;
  struct merge_node *hi_child;
  unsigned int level;
  _Bool queued;
  pthread_mutex_t lock;
};
struct merge_node_queue {
  struct heap *priority_queue;
  pthread_mutex_t mutex;
  pthread_cond_t cond;
};
struct cs_status {
  _Bool valid;
  sigset_t sigs;
};
struct tempnode {
  struct tempnode *volatile next;
  pid_t pid;
  char state;
  char name[1];
};
struct sortfile {
  char const *name;
  struct tempnode *temp;
};
union __anonunion_101 {
  int __in;
  int __i;
};
union __anonunion_102 {
  int __in;
  int __i;
};
struct thread_args {
  struct line *lines;
  size_t nthreads;
  size_t total_lines;
  struct merge_node *node;
  struct merge_node_queue *queue;
  FILE *tfp;
  char const *output_temp;
};
struct timespec dtotimespec(double sec);
struct timespec dtotimespec(double sec) {
  double min_representable;
  double max_representable;
  struct timespec r;
  time_t s;
  double frac;
  long ns;

  {
    min_representable =
        (double)(~(((1L << (sizeof(time_t) * 8UL - 2UL)) - 1L) * 2L + 1L));
    max_representable =
        ((double)(((1L << (sizeof(time_t) * 8UL - 2UL)) - 1L) * 2L + 1L) *
             (double)1000000000 +
         (double)999999999) /
        (double)1000000000;
                                    {
                                                                          
      r.tv_nsec = (__syscall_slong_t)0;
    }       
                                       
                                                                         
                                                 
              
                        
                                                      
                        
                                        
                              
                          
                      
              
                            
         
                     
                       
       
     
    return (r);
  }
}
__attribute__((__noreturn__)) void xalloc_die(void);
char *last_component(char const *name) __attribute__((__pure__));
extern __attribute__((__nothrow__)) void *(__attribute__((__leaf__))
                                           malloc)(size_t __size)
    __attribute__((__malloc__));
extern __attribute__((__nothrow__)) void *(
    __attribute__((__nonnull__(1, 2), __leaf__))
    memcpy)(void *__restrict __dest, void const *__restrict __src, size_t __n);
extern __attribute__((__nothrow__)) int *(
    __attribute__((__leaf__)) __errno_location)(void)__attribute__((__const__));
extern __attribute__((__nothrow__)) int(
    __attribute__((__nonnull__(1, 2), __leaf__))
    stat)(char const *__restrict __file, struct stat *__restrict __buf);
extern
    __attribute__((__nothrow__)) int(__attribute__((__nonnull__(2), __leaf__))
                                     fstat)(int __fd, struct stat *__buf);
extern int close(int __fd);
                             
                             
                                
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                       
                                                                       
                                                                       
                                                                       
                                                                       
                                                                       
                                                                     
                                                                     
                                                                       
                                                                       
                                                                       
                                                                       
                                                                       
                                                                       
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                       
                                                                       
                                                                       
                                                                       
                                                                       
                                                                     
                                                                       
                                                                     
                                                                       
                                                                       
                                                                       
                                                                       
                                                                       
                                                                     
                                                                       
                                                                        
                                
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                   
Hash_table *(__attribute__((__warn_unused_result__))
             hash_initialize)(size_t candidate, Hash_tuning const *tuning,
                              size_t (*hasher)(void const *, size_t),
                              _Bool (*comparator)(void const *, void const *),
                              void (*data_freer)(void *));
void *(__attribute__((__warn_unused_result__))
       hash_insert)(Hash_table *table___0, void const *entry);
int hash_insert_if_absent(Hash_table *table___0, void const *entry,
                          void const **matched_ent);
extern __attribute__((__nothrow__)) void(__attribute__((__leaf__))
                                         free)(void *__ptr);
extern __attribute__((__nothrow__, __noreturn__)) void(__attribute__((
    __leaf__)) __assert_fail)(char const *__assertion, char const *__file,
                              unsigned int __line, char const *__function);
int fd_safer(int fd);
extern void error(int __status, int __errnum, char const *__format, ...);
char const *quote(char const *name);
extern __attribute__((__nothrow__)) char *(__attribute__((__leaf__))
                                           gettext)(char const *__msgid)
    __attribute__((__format_arg__(1)));
void close_stdout(void);
extern struct _IO_FILE *stdout;
extern struct _IO_FILE *stderr;
extern __attribute__((__noreturn__)) void _exit(int __status);
int close_stream(FILE *stream);
int volatile exit_failure;
char *quotearg_colon(char const *arg);
static char const *file_name;
static _Bool ignore_EPIPE;
void close_stdout(void) {
  char const *write_error;
  char const *tmp;
  char *tmp___0;
  int *tmp___1;
  int *tmp___2;
  int tmp___3;
  int *tmp___4;
  int tmp___5;

   
                                   
                       
                         
                                     
                                
                  
         
              
         
                                                   
                          
                        
                                              
                                       
                                                             
                
                                       
                                                
         
                                 
       
     
                                   
                       
                               
     
           
   
}
extern struct _IO_FILE *stdin;
int rpl_fflush(FILE *stream);
int(__attribute__((__nonnull__(1))) rpl_fseeko)(FILE *fp, off_t offset,
                                                int whence);
extern __attribute__((__nothrow__)) int(__attribute__((__leaf__))
                                        ferror_unlocked)(FILE *__stream);
int(__attribute__((__nonnull__(1))) rpl_fclose)(FILE *fp);
extern __attribute__((__nothrow__))
size_t(__attribute__((__leaf__)) __fpending)(FILE *__fp);
int close_stream(FILE *stream) {
  _Bool some_pending;
  size_t tmp;
  _Bool prev_fail;
  int tmp___0;
  _Bool fclose_fail;
  int tmp___1;
  int *tmp___2;
  int *tmp___3;

  {
                             
                                       
                                      
                                      
                                 
                                        
                    
                  
            
                        
                           
                      
                
                                       
                              
                 
                               
                                           
                           
             
                        
           
         
       
     
    return (0);
  }
}
int rpl_fcntl(int fd, int action, ...);
extern __attribute__((__nothrow__)) void *(
    __attribute__((__nonnull__(1), __leaf__))
    memchr)(void const *__s, int __c, size_t __n) __attribute__((__pure__));
extern __attribute__((__nothrow__))
size_t(__attribute__((__nonnull__(1), __leaf__)) strlen)(char const *__s)
    __attribute__((__pure__));
extern __attribute__((__nothrow__, __noreturn__)) void(__attribute__((__leaf__))
                                                       abort)(void);
                                        
extern __attribute__((__nothrow__)) void *(
    __attribute__((__nonnull__(1, 2), __leaf__))
    memmove)(void *__dest, void const *__src, size_t __n);
extern __attribute__((__nothrow__)) char *(__attribute__((__nonnull__(1),
                                                          __leaf__))
                                           strchr)(char const *__s, int __c)
    __attribute__((__pure__));
size_t triple_hash(void const *x, size_t table_size) __attribute__((__pure__));
_Bool triple_compare_ino_str(void const *x, void const *y)
    __attribute__((__pure__));
void *xmalloc(size_t n) __attribute__((__malloc__, __alloc_size__(1)));
void *xrealloc(void *p, size_t n) __attribute__((__alloc_size__(2)));
extern __attribute__((__nothrow__)) char *(__attribute__((__nonnull__(1),
                                                          __leaf__))
                                           strdup)(char const *__s)
    __attribute__((__malloc__));
int c_tolower(int c) __attribute__((__const__));
int c_strcasecmp(char const *s1, char const *s2) __attribute__((__pure__));
int c_strcasecmp(char const *s1, char const *s2) __attribute__((__pure__));
int c_strcasecmp(char const *s1, char const *s2) {
  register unsigned char const *p1;
  register unsigned char const *p2;
  unsigned char c1;
  unsigned char c2;

  {
                                   
                                   
                                                 
                 
     
              {
      c1 = (unsigned char)c_tolower((int)*p1);
      c2 = (unsigned char)c_tolower((int)*p2);
                         
                         
       
      p1++;
      p2++;
                                  
                         
       
    }
  while_break:;
    return ((int)c1 - (int)c2);
  }
}
_Bool c_isalnum(int c) __attribute__((__const__));
_Bool c_isalpha(int c) __attribute__((__const__));
_Bool c_isdigit(int c) __attribute__((__const__));
_Bool c_isspace(int c) __attribute__((__const__));
_Bool c_isalnum(int c) __attribute__((__const__));
_Bool c_isalnum(int c) {
  int tmp;

  {
                  
                    
                
              
                
       
           {
    _L:
                           {
                             {
          tmp = 1;
        }       
                  
         
      }       
                
       
    }
    return ((_Bool)tmp);
  }
}
_Bool c_isalpha(int c) __attribute__((__const__));
_Bool c_isalpha(int c) {
  int tmp;

  {
                         {
                           {
        tmp = 1;
      }       
                
       
    }       
              
     
    return ((_Bool)tmp);
  }
}
_Bool c_isdigit(int c) __attribute__((__const__));
_Bool c_isdigit(int c) {
  int tmp;

  {
                 {
                   {
        tmp = 1;
      }       
                
       
    }       
              
     
    return ((_Bool)tmp);
  }
}
_Bool c_isspace(int c) __attribute__((__const__));
_Bool c_isspace(int c) {
  int tmp;

  {
                 {
      tmp = 1;
    }       
                   
                
              
                      
                  
                
                        
                    
                  
                          
                      
                    
                            
                        
                      
                        
               
             
           
         
       
     
    return ((_Bool)tmp);
  }
}
int c_tolower(int c) __attribute__((__const__));
int c_tolower(int c) {
  int tmp;

  {
                 {
                   {
        tmp = (c - 65) + 97;
      }       
                
       
    }       
              
     
    return (tmp);
  }
}
char *last_component(char const *name) __attribute__((__pure__));
char *last_component(char const *name) {
  char const *base;
  char const *p;
  _Bool saw_slash;

  {
                    
                         
               

                                        
                         
       
             
     
              
             
              {

                
                             
       
                                 {
        saw_slash = (_Bool)1;
      }       
                        
                   
                               
         
       
      p++;
    }
  while_break___0:;
    return ((char *)base);
  }
}
__inline static unsigned char to_uchar(char ch) {

   

    return ((unsigned char)ch);
   

}
                                 
                           
ptrdiff_t __xargmatch_internal(char const *context, char const *arg,
                               char const *const *arglist, char const *vallist,
                               size_t valsize, void (*exit_fn)(void));
extern __attribute__((__nothrow__)) char *(__attribute__((__nonnull__(1),
                                                          __leaf__))
                                           getenv)(char const *__name);
extern __attribute__((__nothrow__)) int(
    __attribute__((__nonnull__(1, 2), __leaf__))
    memcmp)(void const *__s1, void const *__s2, size_t __n)
    __attribute__((__pure__));
extern __attribute__((__nothrow__)) char *(
    __attribute__((__nonnull__(1, 2), __leaf__))
    strcpy)(char *__restrict __dest, char const *__restrict __src);
                                       
ptrdiff_t argmatch(char const *arg, char const *const *arglist,
                   char const *vallist, size_t valsize)
    __attribute__((__pure__));
void argmatch_invalid(char const *context, char const *value,
                      ptrdiff_t problem);
void argmatch_valid(char const *const *arglist, char const *vallist,
                    size_t valsize);
extern int fprintf(FILE *__restrict __stream, char const *__restrict __format,
                   ...);
extern int putc_unlocked(int __c, FILE *__stream);
extern int fputs_unlocked(char const *__restrict __s,
                          FILE *__restrict __stream);
extern __attribute__((__nothrow__)) int(
    __attribute__((__nonnull__(1, 2), __leaf__))
    strncmp)(char const *__s1, char const *__s2, size_t __n)
    __attribute__((__pure__));
char *quotearg_n_style(int n, enum quoting_style s, char const *arg);
char const *quote_n(int n, char const *name);
__attribute__((__noreturn__)) void usage(int status);
static void __argmatch_die(void) {

   

             
           
   

}
void (*argmatch_die)(void) = &__argmatch_die;
ptrdiff_t argmatch(char const *arg, char const *const *arglist,
                   char const *vallist, size_t valsize)
    __attribute__((__pure__));
ptrdiff_t argmatch(char const *arg, char const *const *arglist,
                   char const *vallist, size_t valsize) {
  size_t i;
  size_t arglen;
  ptrdiff_t matchind;
  _Bool ambiguous;
  int tmp;
  size_t tmp___0;
  int tmp___1;

  {
                             
                         
    arglen = strlen(arg);
                  
              {

                            
                         
       
      tmp___1 = strncmp((char const *)*(arglist + i), arg, arglen);
                    {
        tmp___0 = strlen((char const *)*(arglist + i));
                               {
          return ((ptrdiff_t)i);
        }       
                                
                                    
                  
                                                                       
                                   
                    
                                                                                
                                                                           
                        
                                     
               
             
           
         
      }
      i++;
    }
  while_break:;
                    
                             
            
                        
     
  }
}
void argmatch_invalid(char const *context, char const *value,
                      ptrdiff_t problem) {
  char const *format;
  char *tmp;
  char *tmp___0;
  char *tmp___1;
  char const *tmp___2;
  char *tmp___3;

   
                         
                                                  
                    
            
                                                        
                        
     
                                   
                                  
                                                                
                                          
           
   
}
void argmatch_valid(char const *const *arglist, char const *vallist,
                    size_t valsize) {
  size_t i;
  char const *last_val;
  char *tmp;
  char const *tmp___0;
  char const *tmp___1;
  int tmp___2;

   
                                         
                                          
                                              
                  
               

                            
                         
       
                     
                                                      
                                             
                                         
              
                                                
                                                                         
                      
                                                        
                                               
                                           
                
                                                        
                                           
         
       
          
     
              
                                
           
   
}
ptrdiff_t __xargmatch_internal(char const *context, char const *arg,
                               char const *const *arglist, char const *vallist,
                               size_t valsize, void (*exit_fn)(void)) {
  ptrdiff_t res;
  ptrdiff_t tmp;

  {
                                                   
              
                    
                   
     
                                        
                                              
                 
    return ((ptrdiff_t)-1);
  }
}
extern
    __attribute__((__nothrow__)) void *(__attribute__((__warn_unused_result__,
                                                       __leaf__))
                                        realloc)(void *__ptr, size_t __size);
                                           
                                                                        
                                                              
__inline static void *xnmalloc(size_t n, size_t s)
    __attribute__((__malloc__, __alloc_size__(1, 2)));
__inline static void *xnmalloc(size_t n, size_t s)
    __attribute__((__malloc__, __alloc_size__(1, 2)));
__inline static void *xnmalloc(size_t n, size_t s) {
  int tmp;
  void *tmp___0;

  {
                                              
               
            
               
     
                              
                   
     
    tmp___0 = xmalloc(n * s);
    return (tmp___0);
  }
}
extern __attribute__((__nothrow__)) unsigned short const **(
    __attribute__((__leaf__)) __ctype_b_loc)(void)__attribute__((__const__));
extern __attribute__((__nothrow__))
uintmax_t(__attribute__((__leaf__)) strtoumax)(char const *__restrict __nptr,
                                               char **__restrict __endptr,
                                               int __base);
strtol_error xstrtoumax(char const *s, char **ptr, int strtol_base,
                        uintmax_t *val, char const *valid_suffixes);
static strtol_error bkm_scale(uintmax_t *x, int scale_factor) {

   

                                                                  
                                
                               
     
                                  
    return ((strtol_error)0);
   

}
static strtol_error bkm_scale_by_power(uintmax_t *x, int base, int power) {
  strtol_error err;
  strtol_error tmp;
  int tmp___0;

  {
                          
              {
                      
              
                     
                         
       
      tmp = bkm_scale(x, base);
      err = (strtol_error)((unsigned int)err | (unsigned int)tmp);
    }
  while_break:;
    return (err);
  }
}
strtol_error xstrtoumax(char const *s, char **ptr, int strtol_base,
                        uintmax_t *val, char const *valid_suffixes) {
  char *t_ptr;
  char **p;
  uintmax_t tmp;
  strtol_error err;
  char const *q;
  unsigned char ch;
  unsigned short const **tmp___0;
  int *tmp___1;
  char *tmp___2;
  int *tmp___3;
  int *tmp___4;
  int base;
  int suffixes;
  strtol_error overflow;
  char *tmp___5;
  char *tmp___6;

  {
                          
                           
                                 
                                                              
                                                                     
                                                     
                                         
       
            
                                                            
                                                                   
                                                   
                                       
     
             {
      p = ptr;
    }       
                 
     
          
                           
               
                                
                                                         
                         
       
          
                             
     
               
                        
                               
     
                                 
                 
    tmp = strtoumax(s, p, strtol_base);
                                                
                           
                    
                                                       
                        
                               
                  
                                     
           
                
                                   
         
              
                                 
       
            
                                   
                          
                                     
                             
                                   
         
                              
       
     
                          
                 
                   
     
                         {
      base = 1024;
                   
      tmp___5 = strchr(valid_suffixes, (int)*(*p));
                    {
        *val = tmp;
        return ((strtol_error)((unsigned int)err | 2U));
      }
                                            
                    
                                          
                        
         
                                         
                       
         
                                         
                       
         
                          
               
                                         
                        
         
                          
              
                    
                   
                          
                    
       
                             
                     
       
                             
                         
       
                             
                     
       
                             
                     
       
                             
                     
       
                              
                     
       
                              
                      
       
                             
                      
       
                             
                     
       
                              
                     
       
                             
                     
       
                             
                     
       
                              
                     
       
                              
                      
       
                             
                     
       
                             
                     
       
                          
            
                                      
                            
                
                                       
                            
            
                                 
                            
            
                                                   
                            
            
                                                   
                            
             
                                                   
                            
            
                                                   
                            
            
                                                   
                            
            
                                                   
                            
             
                                    
                            
            
                                                   
                            
    case_90:
      overflow = bkm_scale_by_power(&tmp, base, 7);
                            
                   
                 
                                                      
    switch_break___0:
      err = (strtol_error)((unsigned int)err | (unsigned int)overflow);
                     
                  
                                                     
       
    }
               
                 
  }
}
strtol_error xstrtoul(char const *s, char **ptr, int strtol_base,
                      unsigned long *val, char const *valid_suffixes);
extern __attribute__((__nothrow__)) unsigned long(__attribute__((
    __nonnull__(1), __leaf__)) strtoul)(char const *__restrict __nptr,
                                        char **__restrict __endptr, int __base);
static strtol_error bkm_scale___0(unsigned long *x, int scale_factor) {

   

                                                                  
                                
                               
     
                                      
    return ((strtol_error)0);
   

}
static strtol_error bkm_scale_by_power___0(unsigned long *x, int base,
                                           int power) {
  strtol_error err;
  strtol_error tmp;
  int tmp___0;

  {
                          
              {
                      
              
                     
                         
       
      tmp = bkm_scale___0(x, base);
      err = (strtol_error)((unsigned int)err | (unsigned int)tmp);
    }
  while_break:;
    return (err);
  }
}
strtol_error xstrtoul(char const *s, char **ptr, int strtol_base,
                      unsigned long *val, char const *valid_suffixes) {
  char *t_ptr;
  char **p;
  unsigned long tmp;
  strtol_error err;
  char const *q;
  unsigned char ch;
  unsigned short const **tmp___0;
  int *tmp___1;
  char *tmp___2;
  int *tmp___3;
  int *tmp___4;
  int base;
  int suffixes;
  strtol_error overflow;
  char *tmp___5;
  char *tmp___6;

  {
                          
                           
                                 
                                                              
                                                                     
                                                     
                                       
       
            
                                                            
                                                                   
                                                   
                                     
     
              
              
            
                 
     
          
                           
               
                                
                                                         
                         
       
          
                             
     
               
                        
                               
     
                                 
                 
                                     
                                                
                           
                    
                                                       
                        
                      
                  
                                     
           
                
                                   
         
              
                                 
       
            
                                   
                          
                                     
                             
                                   
         
                              
       
     
                          
                 
                   
     
                         {
                  
                   
                                                   
                     
                   
                                                        
       
                                            
                   {
                                          
                        
         
                                         
                       
         
                                         
                       
         
                          
               
                                         
                        
         
                          
              
                    
        suffixes++;
                          
                    
      }
                             
                     
       
                             
                         
       
                             
                     
       
                             
                     
       
                             
                     
       
                              
                     
       
                              
                      
       
                             
                      
       
                             
                     
       
                              
                     
       
                             
                     
       
                             
                     
       
                              
                     
       
                              
                      
       
                             
                     
       
                             
                     
       
                          
            
                                          
                            
                
                                           
                            
            
                                 
                            
            
                                                       
                            
            
                                                       
                            
             
                                                       
                            
            
                                                       
                            
            
                                                       
                            
            
                                                       
                            
             
                                        
                            
            
                                                       
                            
            
                                                       
                            
                   
                 
      return ((strtol_error)((unsigned int)err | 2U));
                     
                                                                       
      *p += suffixes;
                 {
        err = (strtol_error)((unsigned int)err | 2U);
      }
    }
               
                 
  }
}
extern
    __attribute__((__nothrow__)) long(__attribute__((__nonnull__(1), __leaf__))
                                      strtol)(char const *__restrict __nptr,
                                              char **__restrict __endptr,
                                              int __base);
__attribute__((__noreturn__)) void
xstrtol_fatal(enum strtol_error err, int opt_idx, char c,
              struct option const *long_options___2, char const *arg);
static void xstrtol_error(enum strtol_error err, int opt_idx, char c,
                          struct option const *long_options___2,
                          char const *arg, int exit_status) {
  char const *hyphens;
  char const *msgid;
  char const *option;
  char option_buffer[2];
  char *tmp;

   
                   
                                  
                  
     
                                  
                  
     
                                  
                  
     
                                  
                  
     
                        
                 
            
         
                                           
                      
         
                                                     
                      
         
                                             
                      
                
                      
                         
                           
                                      
                                             
            
                                                                
     
                         
                                                                   
           
   
}
__attribute__((__noreturn__)) void
xstrtol_fatal(enum strtol_error err, int opt_idx, char c,
              struct option const *long_options___2, char const *arg);
void xstrtol_fatal(enum strtol_error err, int opt_idx, char c,
                   struct option const *long_options___2, char const *arg) {

   

                                                                             
            
   

}
extern __attribute__((__nothrow__)) int(__attribute__((__leaf__))
                                        ferror)(FILE *__stream);
int xnanosleep(double seconds);
int(__attribute__((__nonnull__(1)))
    rpl_nanosleep)(struct timespec const *requested_delay,
                   struct timespec *remaining_delay);
int xnanosleep(double seconds) {
  struct timespec ts_sleep;
  struct timespec tmp;
  int *tmp___0;
  int tmp___1;
  int *tmp___2;
  int *tmp___3;

  {
                               
                   
               
                                   
                   
                                                                   
                                                              
                         
                         
       
                                   
                          
                                     
                            
                      
         
       
     
               
    return (0);
  }
}
int memcoll0(char const *s1, size_t s1size, char const *s2, size_t s2size);
char *quotearg_n_style_mem(int n, enum quoting_style s, char const *arg,
                           size_t argsize);
int xmemcoll0(char const *s1, size_t s1size, char const *s2, size_t s2size);
static void collate_error(int collation_errno, char const *s1, size_t s1len,
                          char const *s2, size_t s2len) {
  char *tmp;
  char *tmp___0;
  char *tmp___1;
  char *tmp___2;
  char *tmp___3;

   
                                              
                                                 
                                                                      
                                       
                                                                        
                                                                        
                                                              
                                                                         
           
   
}
int xmemcoll0(char const *s1, size_t s1size, char const *s2, size_t s2size) {
  int diff;
  int tmp;
  int collation_errno;
  int *tmp___0;

  {
    tmp = memcoll0(s1, s1size, s2, s2size);
    diff = tmp;
                                 
                               
                          
                                                                         
     
    return (diff);
  }
}
void *xcalloc(size_t n, size_t s)
    __attribute__((__malloc__, __alloc_size__(1, 2)));
void *xmemdup(void const *p, size_t s)
    __attribute__((__malloc__, __alloc_size__(2)));
__inline static void *x2nrealloc(void *p, size_t *pn, size_t s) {
  size_t n;
  void *tmp;

  {
            
            {
              {
                      
        n += (size_t)(!n);
      }
    }       
                                          
                     
       
                           
     
            
    tmp = xrealloc(p, n * s);
    return (tmp);
  }
}
extern
    __attribute__((__nothrow__)) void *(__attribute__((__leaf__))
                                        calloc)(size_t __nmemb, size_t __size)
        __attribute__((__malloc__));
extern
    __attribute__((__nothrow__)) void *(__attribute__((__nonnull__(1),
                                                       __leaf__))
                                        memset)(void *__s, int __c, size_t __n);
void *xmalloc(size_t n) __attribute__((__malloc__, __alloc_size__(1)));
void *xmalloc(size_t n) {
  void *p;
  void *tmp;

  {
    tmp = malloc(n);
    p = tmp;
             
                     
                     
       
     
    return (p);
  }
}
void *xrealloc(void *p, size_t n) __attribute__((__alloc_size__(2)));
void *xrealloc(void *p, size_t n) {

   

             
              
                
                           
       
     
    p = realloc(p, n);
             
              
                     
       
     
    return (p);
   

}
void *xcalloc(size_t n, size_t s)
    __attribute__((__malloc__, __alloc_size__(1, 2)));
void *xcalloc(size_t n, size_t s) {
  void *p;

  {
    p = calloc(n, s);
             
                   
     
    return (p);
  }
}
void *xmemdup(void const *p, size_t s)
    __attribute__((__malloc__, __alloc_size__(2)));
void *xmemdup(void const *p, size_t s) {
  void *tmp;
  void *tmp___0;

  {
    tmp = xmalloc(s);
    tmp___0 = memcpy(tmp, p, s);
    return (tmp___0);
  }
}
__attribute__((__noreturn__)) void xalloc_die(void);
void xalloc_die(void) {
  char *tmp;

   
                                      
                                           
            
   
}
extern __attribute__((__nothrow__)) int(
    __attribute__((__nonnull__(1, 2), __leaf__))
    strcmp)(char const *__s1, char const *__s2) __attribute__((__pure__));
extern
    __attribute__((__nothrow__)) int(__attribute__((__nonnull__(1), __leaf__))
                                     atexit)(void (*__func)(void));
extern __pid_t waitpid(__pid_t __pid, int *__stat_loc, int __options);
extern int printf(char const *__restrict __format, ...);
char const version_etc_copyright[47];
void version_etc_arn(FILE *stream, char const *command_name,
                     char const *package, char const *version,
                     char const *const *authors, size_t n_authors);
void version_etc_va(FILE *stream, char const *command_name, char const *package,
                    char const *version, va_list authors);
void version_etc(FILE *stream, char const *command_name, char const *package,
                 char const *version, ...) __attribute__((__sentinel__));
void version_etc_arn(FILE *stream, char const *command_name,
                     char const *package, char const *version,
                     char const *const *authors, size_t n_authors) {
  char *tmp;
  char *tmp___0;
  char *tmp___1;
  char *tmp___2;
  char *tmp___3;
  char *tmp___4;
  char *tmp___5;
  char *tmp___6;
  char *tmp___7;
  char *tmp___8;
  char *tmp___9;
  char *tmp___10;

   
                       
                                                                      
            
                                                   
     
                         
                                                      
             
                                                               
                                                                              
                                                                           
                                                                 
                                                  
                           
                  
     
                           
                  
     
                           
                  
     
                           
                  
     
                           
                  
     
                           
                  
     
                           
                  
     
                           
                  
     
                           
                  
     
                           
                  
     
                        
         
            
         
                                          
                                                           
                      
         
                                                 
                                                                           
                      
         
                                                      
                                                                          
                            
                      
         
                                                           
                                                                          
                                            
                      
         
                                                               
                                                                          
                                                            
                      
         
                                                                   
                                                                          
                                                                            
                      
         
                                                                       
                                                                          
                                                                           
                            
                      
         
                                                                            
                                                                          
                                                                           
                                            
                      
         
                                                                                
                                                                          
                                                                           
                                                            
                      
                 
                       
                                                                          
                                                                           
                                                                           
                                                            
                      
                
           
   
}
void version_etc_va(FILE *stream, char const *command_name, char const *package,
                    char const *version, va_list authors) {
  size_t n_authors;
  char const *authtab[10];
  char const *tmp;

   
                          
               

                             
                                                      
                                 
                                                                  
                           
         
              
                         
       
                  
     
              
                                                           
                                                               
           
   
}
void version_etc(FILE *stream, char const *command_name, char const *package,
                 char const *version, ...) __attribute__((__sentinel__));
void version_etc(FILE *stream, char const *command_name, char const *package,
                 char const *version, ...) {
  va_list authors;

   
                                         
                                                                    
                              
           
   
}
                                        
                                                                         
                                                                         
                                                                         
                                                                         
                                                                         
                                                                         
                                                                         
                                                                         
                                                                         
                                                                         
                                                                         
                                                          
extern __attribute__((__nothrow__)) char *(__attribute__((__leaf__))
                                           nl_langinfo)(nl_item __item);
char *(__attribute__((__warn_unused_result__)) umaxtostr)(uintmax_t i,
                                                          char *buf___1);
char const *locale_charset(void);
#pragma GCC diagnostic ignored "-Wtype-limits"
char *(__attribute__((__warn_unused_result__)) umaxtostr)(uintmax_t i,
                                                          char *buf___1) {
  char *p;

  {
    p = buf___1 + ((sizeof(uintmax_t) * 8UL) * 146UL + 484UL) / 485UL;
                 
                  
                 
            
                                     
                  
                          
                           
         
       
                
          
                     
           {
                {
            
        *p = (char)(48UL + i % 10UL);
                  
                          
                               
         
      }
    while_break___0:;
    }
    return (p);
  }
}
#pragma GCC diagnostic ignored "-Wtype-limits"
char *(__attribute__((__warn_unused_result__)) uinttostr)(unsigned int i,
                                                          char *buf___1);
char *(__attribute__((__warn_unused_result__)) uinttostr)(unsigned int i,
                                                          char *buf___1) {
  char *p;

  {
                                                                         
                 
                {
                 
            
                                   
                 
                         
                           
         
       
                
          
      *p = (char)'-';
    }       
                 
            
                                   
                 
                         
                               
         
       
                     
     
    return (p);
  }
}
extern __attribute__((__nothrow__))
size_t(__attribute__((__leaf__)) __ctype_get_mb_cur_max)(void);
extern __attribute__((__nothrow__)) int(__attribute__((__leaf__))
                                        mbsinit)(mbstate_t const *__ps)
    __attribute__((__pure__));
extern __attribute__((__nothrow__))
size_t(__attribute__((__leaf__))
       mbrtowc)(wchar_t *__restrict __pwc, char const *__restrict __s,
                size_t __n, mbstate_t *__restrict __p);
unsigned int const is_basic_table[8];
__inline static _Bool is_basic(char c) {

   

    return ((_Bool)((is_basic_table[(int)((unsigned char)c) >> 5] >>
                     ((int)((unsigned char)c) & 31)) &
                    1U));
   

}
#pragma weak pthread_key_create
#pragma weak pthread_getspecific
#pragma weak pthread_setspecific
#pragma weak pthread_key_delete
#pragma weak pthread_self
#pragma weak pthread_cancel
extern int(__attribute__((__nonnull__(1))) open)(char const *__file,
                                                 int __oflag, ...);
int __attribute__((__pure__))
strnumcmp(char const *a, char const *b, int decimal_point___0,
          int thousands_sep___0);
__inline static int __attribute__((__pure__))
fraccompare(char const *a, char const *b, char decimal_point___0) {
  char const *tmp;
  char const *tmp___0;

  {
                                                          
                                                            
                   
              
              
                                                      
                             
           
                                                
                                                      
           
         
                   
                                           
                                             
                                                                     
                                                                      
           
         
                                           
                                  
         
                                           
                                  
         
                                                  
              
                
       
            
       
                  
          
                                                                  
                         
                   

                                         
                                 
           
              
         
                       
                                                                               
              
                
            
                                                                
                           
                     

                                           
                                   
             
                
           
                         
                  
                                                                                
         
       
     
    return ((int __attribute__((__pure__)))0);
  }
}
__inline static int __attribute__((__pure__))
numcompare(char const *a, char const *b, int decimal_point___0,
           int thousands_sep___0) {
  unsigned char tmpa;
  unsigned char tmpb;
  int tmp;
  size_t log_a;
  size_t log_b;
  int __attribute__((__pure__)) tmp___0;
  int tmp___1;
  int __attribute__((__pure__)) tmp___2;
  int tmp___3;

  {
    tmpa = (unsigned char)*a;
    tmpb = (unsigned char)*b;
                          
                 
            
                                 
                                 
                                                  
                             
           
         
       
                 
                            
                                             
                     
                
                                     
                                     
                                   
             
           
                         
         
                                             
                                                       
         
                   

                                   
                                                    
                                   
             
           
              
                                   
         
                       
                                             
                     
                
                                     
                                     
                                   
             
           
                         
         
                
                                                                                
       
                 
            
                                 
                                 
                                                  
                                 
           
         
       
                     
                 

                                     
                                                  
                                 
           
                
                               
         
                   
              
                                   
                                                  
                                 
           
         
                       
                   
              
                                   
                                                  
                                 
           
         
                       
       
                     
                                           
                                                
                                                               
                           
                
                  
         
              
         
                                             
                                                  
                                                                 
                             
           
         
       
                                  
                        
                 

                                                
                               
         
                   
              
                                   
                                                  
                                 
           
         
                      
                
       
                    
                        
                 

                                                
                               
         
                   
              
                                   
                                                  
                                  
           
         
                       
                
       
                     
                           
                            
                      
                
                       
         
                                                        
       
                   
                                                  
       
                                                  
           {
                            
                   
              
                                   
                                   
                                                    
                                    
             
           
         
                        
                                             
                     
                
                                     
                                     
                                    
             
           
                          
         
                                             
                                                    
         
                   

                                   
                                                    
                                    
             
           
              
                                   
         
                        
                                             
                     
                
                                     
                                     
                                    
             
           
                          
         
                
                                                                             
             {
                   

                                   
                                                    
                                    
             
           
              
                                   
         
                        
                   

                                   
                                                    
                                    
             
           
              
                                   
         
                        
        while (1) {

          if ((int)tmpa == (int)tmpb) {
                                                    
                                    
             
          } else {
            goto while_break___17;
          }
                    {
            a++;
            tmpa = (unsigned char)*a;
                                                    
                                    
             
          }
        while_break___18:;
                    {
            b++;
            tmpb = (unsigned char)*b;
                                                    
                                    
             
          }
        while_break___19:;
        }
      while_break___17:;
                                             
                                                  
                                                                 
                             
                  
                        
           
                
               
                                               
                                                    
                                                                   
                               
             
           
         
        tmp = (int)tmpa - (int)tmpb;
        log_a = (size_t)0;
        while (1) {

          if (!((unsigned int)tmpa - 48U <= 9U)) {
            goto while_break___20;
          }
                    {
            a++;
            tmpa = (unsigned char)*a;
                                                    
                                    
             
          }
        while_break___21:
          log_a++;
        }
      while_break___20:
        log_b = (size_t)0;
        while (1) {

          if (!((unsigned int)tmpb - 48U <= 9U)) {
            goto while_break___22;
          }
                    {
            b++;
            tmpb = (unsigned char)*b;
                                                    
                                    
             
          }
        while_break___23:
          log_b++;
        }
      while_break___22:;
        if (log_a != log_b) {
          if (log_a < log_b) {
            tmp___3 = -1;
          } else {
            tmp___3 = 1;
          }
          return ((int __attribute__((__pure__)))tmp___3);
        }
                     
                                                    
         
        return ((int __attribute__((__pure__)))tmp);
      }
    }
  }
}
int __attribute__((__pure__))
strnumcmp(char const *a, char const *b, int decimal_point___0,
          int thousands_sep___0) {
  int __attribute__((__pure__)) tmp;

  {
    tmp = numcompare(a, b, decimal_point___0, thousands_sep___0);
    return (tmp);
  }
}
size_t strnlen1(char const *string, size_t maxlen) __attribute__((__pure__));
size_t strnlen1(char const *string, size_t maxlen) __attribute__((__pure__));
size_t strnlen1(char const *string, size_t maxlen) {
  char const *end;
  char const *tmp;

  {
    tmp = (char const *)memchr((void const *)string, '\000', maxlen);
    end = tmp;
                                                          {
      return ((size_t)((end - string) + 1L));
    }       
                      
     
  }
}
extern __attribute__((__nothrow__)) int(__attribute__((__leaf__))
                                        tolower)(int __c);
extern __attribute__((__nothrow__)) int(__attribute__((__leaf__))
                                        toupper)(int __c);
extern __attribute__((__nothrow__)) int(__attribute__((__leaf__)) sigprocmask)(
    int __how, sigset_t const *__restrict __set, sigset_t *__restrict __oset);
extern size_t fread_unlocked(void *__restrict __ptr, size_t __size, size_t __n,
                             FILE *__restrict __stream);
extern __attribute__((__nothrow__)) int(__attribute__((__leaf__))
                                        feof_unlocked)(FILE *__stream);
__inline static void set_uint32(char *cp, uint32_t v) {

   

                                                      
           
   

}
extern __attribute__((__nothrow__)) int(__attribute__((__leaf__))
                                        raise)(int __sig);
extern __attribute__((__nothrow__)) __pid_t fork(void);
extern ssize_t read(int __fd, void *__buf, size_t __nbytes);
extern
    __attribute__((__nothrow__)) int(__attribute__((__nonnull__(1), __leaf__))
                                     unlink)(char const *__name);
extern int fgetc(FILE *__stream);
extern void _obstack_newchunk(struct obstack *, int);
extern int _obstack_begin(struct obstack *, int, int, void *(*)(long),
                          void (*)(void *));
extern FILE *fopen(char const *__restrict __filename, char const *__restrict __modes);
void readtokens0_init(struct Tokens *t);
_Bool readtokens0(FILE *in, struct Tokens *t);
void readtokens0_init(struct Tokens *t) {

   

                         
                                  
                                       
                                                                
                                              
                                                               
                                              
                                                                   
                                              
           
   

}
static void save_token(struct Tokens *t) {
  size_t len;
  struct obstack const *__o;
  char const *s;
  struct obstack *__o1;
  void *__value;
  char *tmp;
  char *tmp___0;
  struct obstack *__o___0;
  struct obstack *__o1___0;
  struct obstack *__o___1;
  int __len;

   
                                               
                                                                           
                      
                                        
                                                                   
                                    
     
                                        
                              
            
                      
     
                                        
                                  
            
                          
     
                     
                                                                           
                                              
                                               
                                                  
                                          
     
                                        
                              
                        
                                                              
                                              
                                                      
     
                       
                                                            
                                                
                            
                             
                                                     
                                              
                                        
     
                                                                            
                                
                 
           
   
}
_Bool readtokens0(FILE *in, struct Tokens *t) {
  int c;
  int tmp;
  size_t len;
  struct obstack const *__o;
  struct obstack *__o___0;
  char *tmp___0;
  struct obstack *__o___1;
  char *tmp___1;
  struct obstack *__o___2;
  struct obstack *__o1;
  struct obstack *__o1___0;
  void *__value;
  char *tmp___2;
  char *tmp___3;
  struct obstack *__o1___1;
  void *__value___0;
  char *tmp___4;
  char *tmp___5;
  int tmp___6;
  int tmp___7;

  {
               
                      
              
                    
                                                   
                                                                          
                  
                               
                                                       
                                                    
                                          
           
                                       
                                 
                                  
                        
         
                         
       
                           
                                                   
                                                
                                      
       
                                   
                             
                         
                   
                      
       
     
              
                        
                                                              
                                              
                                                      
     
                   
                                                                  
                                            
                         
                                            
                                                                       
                                        
     
                                        
                                      
            
                          
     
                                        
                                      
            
                          
     
                         
                 
                                                                             
                                            
                                                       
                                                          
                                                  
     
                                                
                              
                             
                                                
                                                                           
                                        
     
                                        
                                      
            
                          
     
                                        
                                      
            
                          
     
                         
                 
                                                                             
                                            
                                                       
                                                          
                                                  
     
                                                
                                       
                         
                 {
      tmp___7 = 0;
    }       
                  
     
    return ((_Bool)tmp___7);
  }
}
extern int getc_unlocked(FILE *__stream);

extern __attribute__((__nothrow__)) int(__attribute__((__leaf__))
                                        fileno)(FILE *__stream);
struct randread_source *randread_new(char const *name, size_t bytes_bound);
void randread(struct randread_source *s, void *buf___1, size_t size);
int randread_free(struct randread_source *s);
extern __attribute__((__nothrow__)) int(__attribute__((__leaf__))
                                        setvbuf)(FILE *__restrict __stream,
                                                 char *__restrict __buf,
                                                 int __modes, size_t __n);
extern __attribute__((__nothrow__)) int(
    __attribute__((__nonnull__(1), __leaf__))
    gettimeofday)(struct timeval *__restrict __tv, __timezone_ptr_t __tz);
extern __attribute__((__nothrow__))
__pid_t(__attribute__((__leaf__)) getpid)(void);
extern __attribute__((__nothrow__))
__pid_t(__attribute__((__leaf__)) getppid)(void);
extern __attribute__((__nothrow__))
__uid_t(__attribute__((__leaf__)) getuid)(void);
extern __attribute__((__nothrow__))
__gid_t(__attribute__((__leaf__)) getgid)(void);
void isaac_seed(struct isaac_state *s);
void isaac_refill(struct isaac_state *s, isaac_word *result);
FILE *fopen_safer(char const *file, char const *mode);
static void randread_error(void const *file_name___3) {
  char *tmp;
  char const *tmp___1;
  int *tmp___2;
  char *tmp___3;
  int *tmp___4;

   
                        
                                          
                                   
                          
                                    
              
                                   
       
                                 
                                   
                                                  
     
            
   
}
static struct randread_source *simple_new(FILE *source,
                                          void const *handler_arg) {
  struct randread_source *s;
  struct randread_source *tmp;

  {
                                                        
            
                       
                                 
    s->handler_arg = handler_arg;
    return (s);
  }
}
static void get_nonce(void *buffer, size_t bufsize___1, size_t bytes_bound) {
  char *buf___1;
  ssize_t seeded;
  int fd;
  int tmp;
  size_t tmp___0;
  struct timeval v;
  size_t nbytes;
  unsigned long tmp___1;
  pid_t v___0;
  size_t nbytes___0;
  unsigned long tmp___2;
  pid_t v___1;
  size_t nbytes___1;
  unsigned long tmp___3;
  uid_t v___2;
  size_t nbytes___2;
  unsigned long tmp___4;
  uid_t v___3;
  size_t nbytes___3;
  unsigned long tmp___5;

   
                             
                        
                                  
             
                  
                                      
                              
              
                              
       
                                                  
                        
                            
       
                
     
                                       
                                                     
                            
              
                                               
       
                       
                                                      
                                                                     
                                                  
     
                                       
                                                         
                                
              
                                               
       
                           
                       
                                                                             
                                                      
     
                                       
                                                         
                                
              
                                               
       
                           
                        
                                                                             
                                                      
     
                                       
                                                         
                                
              
                                               
       
                           
                       
                                                                             
                                                      
     
                                       
                                                         
                                
              
                                               
       
                           
                       
                                                                             
                                                      
     
           
   
}
struct randread_source *randread_new(char const *name, size_t bytes_bound) {
  struct randread_source *tmp;
  FILE *source;
  struct randread_source *s;
  unsigned long tmp___0;

  {
                            {
      tmp = simple_new((FILE *)((void *)0), (void const *)((void *)0));
      return (tmp);
    }       
                                   
                 
                                         
                      
                                                         
         
       
                                                 
                   
                                             
                                     
                
                                
         
                                              
              
                                          
                                                                               
                               
                                        
       
                 
     
  }
}
static void readsource(struct randread_source *s, unsigned char *p,
                       size_t size) {
  size_t inbytes;
  size_t tmp;
  int fread_errno;
  int *tmp___0;
  int *tmp___1;
  int tmp___3;

   
               
                                                                   
                    
                                   
                             
                   
                      
                        
                         
       
                                   
                                           
                    
                               
              
                     
       
                                      
     
               
           
   
}
static void readisaac(struct isaac *isaac, unsigned char *p, size_t size) {
  size_t inbytes;
  isaac_word *wp;

   
                              
               

                            
                         
                                              
                                                                              
                                       
                     
                                         
               
       
                       
                                            
                                                                            
                                     
                      
                   
                      
                           
                 

                                                                      
                               
         
                                        
                     
                                                             
                          
                                      
                 
         
       
                    
                              
                                                 
                                                             
     

           
   
}
void randread(struct randread_source *s, void *buf___1, size_t size) {

   

                    
                                                    
            
                                                               
     
           
   

}
int randread_free(struct randread_source *s) {
  FILE *source;
  int tmp;
  int tmp___0;

  {
    source = s->source;
                                     
                    
                {
      tmp = rpl_fclose(source);
      tmp___0 = tmp;
    }       
                  
     
    return (tmp___0);
  }
}
void *hash_delete(Hash_table *table___0, void const *entry);
__inline static isaac_word just(isaac_word a) {
  isaac_word desired_bits;

  {
    desired_bits = ((1UL << 1) << ((1 << 6) - 1)) - 1UL;
    return (a & desired_bits);
  }
}
__inline static isaac_word ind(isaac_word const *m, isaac_word x) {
  isaac_word tmp;

  {
                                                     {
      tmp = *((isaac_word *)((char *)m +
                             (x & (unsigned long)((1 << 8) - 1) * sizeof(*m))));
    }       
                                                                 
                                                                 
     
    return (tmp);
  }
}
void isaac_refill(struct isaac_state *s, isaac_word *result) {
  isaac_word a;
  isaac_word b;
  isaac_word *m;
  isaac_word *r;
  isaac_word x;
  isaac_word y;
  isaac_word tmp;
  isaac_word tmp___0;
  isaac_word tmp___1;
  isaac_word tmp___2;
  isaac_word x___0;
  isaac_word y___0;
  isaac_word tmp___3;
  isaac_word tmp___4;
  isaac_word tmp___5;
  isaac_word tmp___6;
  isaac_word tmp___7;
  isaac_word tmp___8;
  isaac_word x___1;
  isaac_word y___1;
  isaac_word tmp___9;
  isaac_word tmp___10;
  isaac_word tmp___11;
  isaac_word tmp___12;
  isaac_word x___2;
  isaac_word y___2;
  isaac_word tmp___13;
  isaac_word tmp___14;
  isaac_word tmp___15;
  isaac_word tmp___16;
  isaac_word tmp___17;
  isaac_word tmp___18;
  isaac_word x___3;
  isaac_word y___3;
  isaac_word tmp___19;
  isaac_word tmp___20;
  isaac_word tmp___21;
  isaac_word tmp___22;
  isaac_word x___4;
  isaac_word y___4;
  isaac_word tmp___23;
  isaac_word tmp___24;
  isaac_word tmp___25;
  isaac_word tmp___26;
  isaac_word tmp___27;
  isaac_word tmp___28;
  isaac_word x___5;
  isaac_word y___5;
  isaac_word tmp___29;
  isaac_word tmp___30;
  isaac_word tmp___31;
  isaac_word tmp___32;
  isaac_word x___6;
  isaac_word y___6;
  isaac_word tmp___33;
  isaac_word tmp___34;
  isaac_word tmp___35;
  isaac_word tmp___36;
  isaac_word tmp___37;
  isaac_word tmp___38;

   
             
             
                    
             
               
               

                         
                
              
                            
       
                         
                          
              
                                   
       
                                       
                   
                                                   
                            
                   
                                                        
                            
                   
                         
                    
              
                                
       
                         
                          
                               
              
                          
                                     
       
                                           
                       
                                                       
                                
                       
                                                            
                                
                   
                         
                    
              
                                
       
                         
                          
              
                                 
       
                                            
                       
                                                        
                                 
                       
                                                             
                                 
                   
                         
                     
              
                                 
       
                         
                           
                                  
              
                           
                                        
       
                                             
                       
                                                        
                                 
                       
                                                             
                                 
                   
             
             
                                                              
                         
       
     
               
               

                         
                     
              
                                 
       
                         
                           
              
                                    
       
                                              
                       
                                                        
                                 
                       
                                                             
                                 
                   
                         
                     
              
                                 
       
                         
                           
                                 
              
                           
                                       
       
                                              
                       
                                                        
                                 
                       
                                                             
                                 
                   
                         
                     
              
                                 
       
                         
                          
              
                                 
       
                                              
                       
                                                        
                                 
                       
                                                             
                                 
                   
                         
                     
              
                                 
       
                         
                           
                                  
              
                           
                                        
       
                                              
                       
                                                        
                                 
                       
                                                             
                                 
                   
             
             
                                                                   
                             
       
     
                  
             
             
           
   
}
void isaac_seed(struct isaac_state *s) {
  isaac_word a;
  unsigned long tmp;
  isaac_word b;
  unsigned long tmp___0;
  isaac_word c;
  unsigned long tmp___1;
  isaac_word d;
  unsigned long tmp___2;
  isaac_word e;
  unsigned long tmp___3;
  isaac_word f;
  unsigned long tmp___4;
  isaac_word g;
  unsigned long tmp___5;
  isaac_word h;
  unsigned long tmp___6;
  int i;
  isaac_word tmp___7;
  isaac_word tmp___8;
  isaac_word tmp___9;
  isaac_word tmp___10;
  int i___0;
  isaac_word tmp___11;
  isaac_word tmp___12;
  isaac_word tmp___13;
  isaac_word tmp___14;
  isaac_word tmp___15;
  isaac_word tmp___16;

   
                       
                        
            
                                  
     
            
                       
                             
            
                                     
     
                
                       
                             
            
                                     
     
                
                       
                            
            
                                     
     
                
                       
                             
            
                                     
     
                
                       
                             
            
                                      
     
                
                       
                             
            
                                     
     
                
                       
                            
            
                                     
     
                
          
               

                          
                         
       
                   
                       
                       
                       
                       
                       
                       
                       
             
                        
                        
             
             
                  
             
             
                        
                         
             
             
                   
             
             
                        
                         
             
             
                   
             
             
                         
                          
             
             
                   
             
                  
                      
                      
                      
                      
                      
                      
                      
             
     
              
              
               

                              
                             
       
                       
                           
                           
                           
                           
                           
                           
                           
             
                         
                         
             
             
                  
             
             
                         
                          
             
             
                   
             
             
                         
                          
             
             
                   
             
             
                         
                          
             
             
                   
             
                      
                          
                          
                          
                          
                          
                          
                          
                 
     
                  
                             
                    
                        
                    
                    
           
   
}
#pragma GCC diagnostic ignored "-Wsuggest-attribute=pure"
                                        
                                               
int set_char_quoting(struct quoting_options *o, char c, int i);
char *quotearg_char(char const *arg, char ch);
char *quotearg_char_mem(char const *arg, size_t argsize, char ch);
                                             
__inline static char *xcharalloc(size_t n)
    __attribute__((__malloc__, __alloc_size__(1)));
__inline static char *xcharalloc(size_t n)
    __attribute__((__malloc__, __alloc_size__(1)));
__inline static char *xcharalloc(size_t n) {
  void *tmp;
  void *tmp___0;
  void *tmp___1;

  {
                             {
      tmp = xmalloc(n);
      tmp___1 = tmp;
    }       
                                          
                        
     
    return ((char *)tmp___1);
  }
}
extern __attribute__((__nothrow__)) int(__attribute__((__leaf__))
                                        iswprint)(wint_t __wc);
                                           
                                                                   
                                           
                                                  
                                                             
                                                             
                                                             
                                                              
static struct quoting_options default_quoting_options;
int set_char_quoting(struct quoting_options *o, char c, int i) {
  unsigned char uc;
  unsigned int *p;
  struct quoting_options *tmp;
  int shift;
  int r;

  {
    uc = (unsigned char)c;
            
              
            
                                     
     
                                                                       
    shift = (int)((unsigned long)uc % (sizeof(int) * 8UL));
    r = (int)((*p >> shift) & 1U);
    *p ^= (unsigned int)(((i & 1) ^ r) << shift);
    return (r);
  }
}
static struct quoting_options
quoting_options_from_style(enum quoting_style style) {
  struct quoting_options o;
  unsigned int tmp;

  {
                                    
                
                              
             
               

                            
                         
       
                                  
            
     
              
                                             
                                              
                                    
              
     
    o.style = style;
    return (o);
  }
}
static char const *gettext_quote(char const *msgid, enum quoting_style s) {
  char const *translation;
  char const *tmp;
  char const *locale_code;
  char const *tmp___0;
  int tmp___1;
  char const *tmp___2;
  int tmp___3;
  char const *tmp___4;

  {
    tmp = (char const *)gettext(msgid);
    translation = tmp;
                                                            {
      return (translation);
    }
                                   
                                                 
                       
                                            
                                 
              
                                 
       
                       
     
                                                   
                       
                                            
                            
              
                             
       
                       
     
                                
                     
            
                     
     
                     
  }
}
static size_t
quotearg_buffer_restyled(char *buffer, size_t buffersize, char const *arg,
                         size_t argsize, enum quoting_style quoting_style,
                         int flags, unsigned int const *quote_these_too,
                         char const *left_quote, char const *right_quote) {
  size_t i;
  size_t len;
  char const *quote_string;
  size_t quote_string_len;
  _Bool backslash_escapes;
  _Bool unibyte_locale;
  size_t tmp;
  _Bool elide_outer_quotes;
  unsigned char c;
  unsigned char esc;
  _Bool is_right_quote;
  int tmp___0;
  int tmp___1;
  size_t m;
  _Bool printable;
  unsigned short const **tmp___2;
  mbstate_t mbstate;
  wchar_t w;
  size_t bytes;
  size_t tmp___3;
  size_t j;
  int tmp___4;
  int tmp___5;
  size_t ilim;
  int tmp___6;
  size_t tmp___7;

  {
                    
                                   
                                 
                                 
                                   
                                         
                                                   
                                            
                  
     
                                            
                  
     
                                            
                  
     
                                            
                  
     
                                            
                  
     
                                            
                  
     
                                            
                  
     
                                            
                  
     
                                            
                  
     
                        
         
                                          
                                  
         
                              
                 

                               
                                       
         
              
                         
       
                 
     
                                 
                        
                                 
                      
         
                                 
                                  
                      
         
                                            
                                                     
                                                       
     
                              
                                
                 

                             
                               
         
                   

                                 
                                                  
           
                
                               
         
                      
                       
       
                     
     
                                 
                               
                                            
                      
         
                                          
                                  
         
                              
                 

                               
                                       
         
              
                             
       
                     
     
                        
                                 
                      
         
                                  
                      
                 
            
               
                  
               

                                            
                                               
              
                               
       
                    
                             
       
                                
                              
                               
                                                
                                                     
                                                                           
                               
                                       
                                               
               
                                        
             
           
         
       
                                    
                        
                        
       
                         
                     
       
                        
                        
       
                        
                        
       
                         
                     
       
                         
                     
       
                         
                     
       
                        
                    
       
                         
                     
       
                         
                     
       
                          
                      
       
                          
                      
       
                         
                     
       
                          
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                          
                     
       
                         
                         
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                              
               
                              
                                 
                                         
         
                   

                                 
                                         
           
                
                               
         
                       
                                
                                              
                                                        
                         

                                       
                                              
                 
                      
                                     
               
                             
                         

                                       
                                              
                 
                      
                                     
               
                             
             
           
         
                               
              
                        
                      
         
       
                            
            
                                              
                        
       
                                              
                        
       
                              
               
                               
                                       
       
                            
               
                      
                                
                                                      
                                                        
                           
             
                                                        
                           
             
                                                        
                           
             
                                                        
                           
             
                                                        
                           
             
                                                        
                           
             
                                                        
                           
             
                                                        
                           
             
                                                        
                           
             
                                    
                  
                                     
                                             
             
                                                  
                     
                       

                                     
                                            
               
                    
                                   
             
                           
                       

                                     
                                             
               
                    
                                   
             
                           
                       

                                     
                                             
               
                    
                                   
             
                           
                       

                                     
                                            
               
                    
                                    
             
                            
                                  
                             
                                  
                            
           
         
       
                            
                       
                            
                      
                            
               
                               
                    
               
                               
                    
            
                               
                    
            
                               
                              
            
                               
                              
           
                               
                              
            
                               
                    
            
              
                              
                                 
                                 
                         
           
         
       
                       
                                              
                                 
                                         
         
       
             
                              
                
                          
       
                            
             
                                            
                                               
              
                                 
       
                     
                              
       
            
                     
                              
       
            
                                              
                                 
                                         
         
       
                            
                
                                              
                                 
                                         
         
                   

                                 
                                         
           
                
                                
         
                        
                   

                                 
                                         
           
                
                                
         
                        
                   

                                 
                                         
           
                
                                
         
                        
       
                            
            
                            
                       
                           
                      
                                  
                                                                              
              
                                                       
                      
                             
                                              
                                
         
                   
                                                                            
                          
                             
                                  
                  
                                                
                                   
                                    
                    
                                                  
                                     
                           

                                        
                                            
                                            
                     
                          
                                          
                   
                      
                 
                                
                                      
                      
                                         
                                                          
                                  
                               

                                         
                                              
                       
                                                                      
                                         
                       
                                                                      
                                         
                       
                                                                      
                                         
                       
                                                                      
                                         
                       
                                                                       
                                         
                       
                                              
                                
                                                     
                                       
                                            
                                     
                          
                     
                                    
                   
                 
                                              
                               
                                       
                 
                           
               
             
           
                                                           
                        
                                  
           
         
                        
       
                    
                    
              
                                
                           
                 
                         
                       

                                      
                                 
                                           
                                                   
                   
                             

                                           
                                                   
                     
                          
                                          
                   
                                  
                             

                                           
                                                                   
                     
                          
                                          
                   
                                  
                             

                                           
                                                                         
                     
                          
                                          
                   
                                 
                                                         
                        
                          
                 
                      
                 
                                     
                             

                                           
                                                   
                     
                          
                                          
                   
                                 
                                            
                 
               
                                    
                                      
               
                         

                                       
                                            
                 
                      
                                      
               
                             
                  
                                            
             
                            
                         
           
         
       
                      
                              
                    
              
                                 
               
                                
                                                                               
                                                              
                                                                        
                          
             
                  
                        
           
                
               
                                
                         
           
         
       
                 
                               
                                       
       
                 

                               
                                       
         
              
                              
       
                      
            
                 

                               
                                    
         
              
                              
       
                      
           
          
     
                   
                     
                                              
                                 
                                         
         
       
     
                       
                                
                   

                               
                                  
           
                     

                                   
                                                    
             
                  
                                  
           
                         
                         
         
                        
       
     
                           
                                     
     
                 
  force_outer_quoting_style:
    tmp___7 = quotearg_buffer_restyled(
        buffer, buffersize, arg, argsize, quoting_style, flags & -3,
        (unsigned int const *)((void *)0), left_quote, right_quote);
    return (tmp___7);
  }
}
static char slot0[256];
static unsigned int nslots = 1U;
static struct slotvec slotvec0 = {sizeof(slot0), slot0};
static struct slotvec *slotvec = &slotvec0;
static char *quotearg_n_options(int n, char const *arg, size_t argsize,
                                struct quoting_options const *options) {
  int e;
  int *tmp;
  unsigned int n0;
  struct slotvec *sv;
  size_t n1;
  _Bool preallocated;
  int tmp___0;
  struct slotvec *tmp___1;
  size_t size;
  char *val;
  int flags;
  size_t qsize;
  size_t tmp___2;
  int *tmp___3;

  {
                             
             
                         
                 
                
              
     
                       
                             
                                                                              
                                                
                     
              
                     
       
                                               
                     
       
                         
                                                
              
                     
       
                                                                         
                   
                         
                       
       
                                                                            
                                
     
                          
                        
    flags = (int)(options->flags | 1);
    tmp___2 = quotearg_buffer_restyled(
        val, size, arg, argsize, (enum quoting_style)options->style, flags,
        (unsigned int const *)(options->quote_these_too),
        (char const *)options->left_quote, (char const *)options->right_quote);
    qsize = tmp___2;
                       {
      size = qsize + 1UL;
                            
                                                         
                          
       
      val = xcharalloc(size);
                          
                                                       
                                                                         
                                                                                
                                                                 
                                                                   
    }
                                 
                 
    return (val);
  }
}
char *quotearg_n_style(int n, enum quoting_style s, char const *arg) {
  struct quoting_options o;
  struct quoting_options tmp;
  char *tmp___0;

  {
                                        
            
    tmp___0 = quotearg_n_options(n, arg, (size_t)-1,
                                 (struct quoting_options const *)(&o));
    return (tmp___0);
  }
}
char *quotearg_n_style_mem(int n, enum quoting_style s, char const *arg,
                           size_t argsize) {
  struct quoting_options o;
  struct quoting_options tmp;
  char *tmp___0;

  {
                                        
            
    tmp___0 = quotearg_n_options(n, arg, argsize,
                                 (struct quoting_options const *)(&o));
    return (tmp___0);
  }
}
char *quotearg_char_mem(char const *arg, size_t argsize, char ch) {
  struct quoting_options options;
  char *tmp;

  {
                                      
                                      
    tmp = quotearg_n_options(0, arg, argsize,
                             (struct quoting_options const *)(&options));
    return (tmp);
  }
}
char *quotearg_char(char const *arg, char ch) {
  char *tmp;

  {
    tmp = quotearg_char_mem(arg, (size_t)-1, ch);
    return (tmp);
  }
}
char *quotearg_colon(char const *arg) {
  char *tmp;

  {
    tmp = quotearg_char(arg, (char)':');
    return (tmp);
  }
}
struct quoting_options quote_quoting_options = {(enum quoting_style)6,
                                                0,
                                                {0U},
                                                (char const *)((void *)0),
                                                (char const *)((void *)0)};
char const *quote_n(int n, char const *name) {
  char const *tmp;

  {
    tmp = (char const *)quotearg_n_options(
        n, name, (size_t)-1,
        (struct quoting_options const *)(&quote_quoting_options));
    return (tmp);
  }
}
char const *quote(char const *name) {
  char const *tmp;

  {
    tmp = quote_n(0, name);
    return (tmp);
  }
}
#pragma GCC diagnostic ignored "-Wsuggest-attribute=const"
__inline static void mbuiter_multi_next(struct mbuiter_multi *iter) {
  int tmp;
  size_t tmp___0;
  size_t tmp___1;
  int tmp___2;
  _Bool tmp___3;

   
                          
             
     
                         
                      
     
                                               
                  
                                  
                                                 
                                    
            
                                                       
                 
                                               
                                                                     
                                                     
                                                  
       
                                
               
                                         
                                                 
                       
                                                                       
                                                    
                                    
                                      
              
                                                      
                                                  
                                        
                
                                       
                                        
                                                        
                                                        
                                                                           
                                                           
                                                        
             
                                       
                                                
                                                                           
                                                           
                                                        
             
           
                                        
                                                               
                        
                                      
           
         
       
     
                               
           
   
}
                         
void set_program_name(char const *argv0);
extern char *program_invocation_name;
extern char *program_invocation_short_name;
extern int fputs(char const *__restrict __s, FILE *__restrict __stream);
extern __attribute__((__nothrow__)) char *(__attribute__((__nonnull__(1),
                                                          __leaf__))
                                           strrchr)(char const *__s, int __c)
    __attribute__((__pure__));
char const *program_name = (char const *)((void *)0);
void set_program_name(char const *argv0) {
  char const *slash;
  char const *base;
  int tmp;
  int tmp___0;

  {
                                                             
                                                                                
              
     
                                              
                                                             
                       
            
                   
     
                             
                                                        
                         
                     
                                              
                       
                           
                                                        
         
       
     
    program_name = argv0;
                                            
           
  }
}
int posix2_version(void);
int posix2_version(void) {
  long v;
  char const *s;
  char const *tmp;
  char *e;
  long i;
  long tmp___0;
  long tmp___1;
  long tmp___2;

  {
    v = 200809L;
                                                  
            
            
               
                                    
                    
                  
                
         
       
     
                               {
      tmp___2 = (-0x7FFFFFFF - 1);
    }       
                            
                    
              
                              
       
                        
     
    return ((int)tmp___2);
  }
}
extern __attribute__((__nothrow__)) char *(
    __attribute__((__nonnull__(1, 2), __leaf__))
    stpcpy)(char *__restrict __dest, char const *__restrict __src);
extern __attribute__((__nothrow__)) int(__attribute__((__leaf__))
                                        pipe)(int *__pipedes);
double physmem_total(void);
double physmem_available(void);
extern __attribute__((__nothrow__)) long(__attribute__((__leaf__))
                                         sysconf)(int __name);
double physmem_total(void) {
  double pages;
  double tmp;
  double pagesize;
  double tmp___0;

  {
                              
                
                                  
                       
                             
                                  
                                  
       
     
    return ((double)67108864);
  }
}
double physmem_available(void) {
  double pages;
  double tmp;
  double pagesize;
  double tmp___0;
  double tmp___1;

  {
                              
                
                                  
                       
                             
                                  
                                  
       
     
    tmp___1 = physmem_total();
    return (tmp___1 / (double)4);
  }
}
int dup_safer(int fd);
#pragma GCC diagnostic ignored "-Wtype-limits"
unsigned long num_processors(enum nproc_query query);
extern
    __attribute__((__nothrow__)) int(__attribute__((__leaf__))
                                     __sched_cpucount)(size_t __setsize,
                                                       cpu_set_t const *__setp);
extern __attribute__((__nothrow__)) int(__attribute__((__leaf__))
                                        sched_getaffinity)(__pid_t __pid,
                                                           size_t __cpusetsize,
                                                           cpu_set_t *__cpuset);
static unsigned long num_processors_via_affinity_mask(void) {
  cpu_set_t set;
  unsigned long count;
  int tmp;

  {
                                                  
                   
                                                                
                                                                         
                        
                       
       
     
    return (0UL);
  }
}
unsigned long num_processors(enum nproc_query query) {
  char const *envvalue;
  char const *tmp;
  _Bool tmp___0;
  char *endptr;
  unsigned long value;
  unsigned long tmp___1;
  _Bool tmp___2;
  unsigned long tmp___3;
  _Bool tmp___4;
  unsigned long nprocs___0;
  unsigned long tmp___5;
  long nprocs___1;
  long tmp___6;
  long nprocs___2;
  long tmp___7;
  unsigned long nprocs_current;
  unsigned long tmp___8;

  {
                                    
                                                    
                     
                                                                  
                   

                                            
                                                
                           
                               
             
                  
                             
           
                     
         
                  
                                            
                      
                                       
                                                   
                          
                                                                    
                       

                                      
                                                  
                               
                                       
                 
                      
                                     
               
                       
             
                           
                                    
                                
                                
                      
                              
               
                               
             
           
         
       
                                  
     
                                    
                                                   
                           
                             
                            
       
                            
                           
                            
                                           
       
            
                            
                           
                             
                                                     
                                 
                                   
                                            
         
       
                            
                                           
       
     
    return (1UL);
  }
}
extern int nanosleep(struct timespec const *__requested_time,
                     struct timespec *__remaining);
int(__attribute__((__nonnull__(1)))
    rpl_nanosleep)(struct timespec const *requested_delay,
                   struct timespec *remaining_delay) {
  int *tmp;
  time_t limit;
  time_t seconds;
  struct timespec intermediate;
  int result;
  int tmp___0;

  {
                                       {
                               
                
      return (-1);
    }       
                                                          
                                 
                  
                    
       
     
                            
                                              
                                                                       
               

                               
                         
       
                                  
              
                                                                               
                       
                   
                              
                                             
         
                        
       
                                                  
     
              
                                  
             
                                                                             
                     
  }
}
int mkstemp_safer(char *templ);
extern int(__attribute__((__nonnull__(1))) mkstemp)(char *__template);
int mkstemp_safer(char *templ) {
  int tmp;
  int tmp___0;

  {
    tmp = mkstemp(templ);
    tmp___0 = fd_safer(tmp);
    return (tmp___0);
  }
}
extern __attribute__((__nothrow__)) int(
    __attribute__((__nonnull__(1, 2), __leaf__))
    strcoll)(char const *__s1, char const *__s2) __attribute__((__pure__));
__inline static int strcoll_loop(char const *s1, size_t s1size, char const *s2,
                                 size_t s2size) {
  int diff;
  size_t size1;
  size_t tmp;
  size_t size2;
  size_t tmp___0;
  int *tmp___1;
  int *tmp___2;
  int tmp___3;

  {
              {
                                   
                   
      diff = strcoll(s1, s2);
                 
                    
              
                                     
                       
                      
                
                      
         
       
                    
                         
       
                       
                        
                           
                            
                  
                  
                      
                      
                          
                                  
       
                          
                   
       
    }
  while_break:;
    return (diff);
  }
}
int memcoll0(char const *s1, size_t s1size, char const *s2, size_t s2size) {
  int *tmp;
  int tmp___0;
  int tmp___1;

  {
                          {
      tmp___1 = memcmp((void const *)s1, (void const *)s2, s1size);
                         
                                 
                 
                   
             {
        tmp___0 = strcoll_loop(s1, s1size, s2, s2size);
        return (tmp___0);
      }
    }       
                                                     
                       
     
  }
}
__attribute__((__nothrow__)) void(__attribute__((__leaf__))
                                  md5_init_ctx)(struct md5_ctx *ctx);
__attribute__((__nothrow__)) void(__attribute__((__leaf__))
                                  md5_process_block)(void const *buffer,
                                                     size_t len,
                                                     struct md5_ctx *ctx);
__attribute__((__nothrow__)) void(__attribute__((__leaf__))
                                  md5_process_bytes)(void const *buffer,
                                                     size_t len,
                                                     struct md5_ctx *ctx);
__attribute__((__nothrow__)) void *(__attribute__((__leaf__))
                                    md5_finish_ctx)(struct md5_ctx *ctx,
                                                    void *resbuf);
__attribute__((__nothrow__)) void *(__attribute__((__leaf__))
                                    md5_read_ctx)(struct md5_ctx const *ctx,
                                                  void *resbuf);
static unsigned char const fillbuf___2[64] = {(unsigned char const)128,
                                              (unsigned char const)0};
__attribute__((__nothrow__)) void(__attribute__((__leaf__))
                                  md5_init_ctx)(struct md5_ctx *ctx);
void(__attribute__((__leaf__)) md5_init_ctx)(struct md5_ctx *ctx) {
  uint32_t tmp;

   
                                  
                         
                         
                                 
                      
                        
                        
                              
           
   
}
__attribute__((__nothrow__)) void *(__attribute__((__leaf__))
                                    md5_read_ctx)(struct md5_ctx const *ctx,
                                                  void *resbuf);
void *(__attribute__((__leaf__)) md5_read_ctx)(struct md5_ctx const *ctx,
                                               void *resbuf) {
  char *r;

  {
                       
                                          
                                                     
                                                           
                                                           
    return (resbuf);
  }
}
__attribute__((__nothrow__)) void *(__attribute__((__leaf__))
                                    md5_finish_ctx)(struct md5_ctx *ctx,
                                                    void *resbuf);
void *(__attribute__((__leaf__)) md5_finish_ctx)(struct md5_ctx *ctx,
                                                 void *resbuf) {
  uint32_t bytes;
  size_t size;
  int tmp;
  void *tmp___0;

  {
                        
                      
               
            
               
     
                       
                           
                                
                        
     
                                                 
                                                                           
                                                                                
                                               
                                                                    
    tmp___0 = md5_read_ctx((struct md5_ctx const *)ctx, resbuf);
    return (tmp___0);
  }
}
__attribute__((__nothrow__)) void(__attribute__((__leaf__))
                                  md5_process_bytes)(void const *buffer,
                                                     size_t len,
                                                     struct md5_ctx *ctx);
void(__attribute__((__leaf__))
     md5_process_bytes)(void const *buffer, size_t len, struct md5_ctx *ctx) {
  size_t left_over;
  size_t add;
  size_t tmp;
  size_t left_over___0;

   
                            
                                      
                                    
                  
              
                                
       
                
                                                                       
                                                          
                              
                                                      
                                                                    
                           
                                     
                                                     
                                                                          
                                    
       
                                                          
                 
     
                      
                                                                 
              
                                                                              
                  
     
                    
                                          
                                                                           
                           
                                  
                                                                        
                              
                                                                       
                              
       
                                            
     
           
   
}
__attribute__((__nothrow__)) void(__attribute__((__leaf__))
                                  md5_process_block)(void const *buffer,
                                                     size_t len,
                                                     struct md5_ctx *ctx);
void(__attribute__((__leaf__))
     md5_process_block)(void const *buffer, size_t len, struct md5_ctx *ctx) {
  uint32_t correct_words[16];
  uint32_t const *words;
  size_t nwords;
  uint32_t const *endp;
  uint32_t A;
  uint32_t B;
  uint32_t C;
  uint32_t D;
  uint32_t *cwp;
  uint32_t A_save;
  uint32_t B_save;
  uint32_t C_save;
  uint32_t D_save;
  uint32_t *tmp;
  uint32_t tmp___0;
  uint32_t *tmp___1;
  uint32_t tmp___2;
  uint32_t *tmp___3;
  uint32_t tmp___4;
  uint32_t *tmp___5;
  uint32_t tmp___6;
  uint32_t *tmp___7;
  uint32_t tmp___8;
  uint32_t *tmp___9;
  uint32_t tmp___10;
  uint32_t *tmp___11;
  uint32_t tmp___12;
  uint32_t *tmp___13;
  uint32_t tmp___14;
  uint32_t *tmp___15;
  uint32_t tmp___16;
  uint32_t *tmp___17;
  uint32_t tmp___18;
  uint32_t *tmp___19;
  uint32_t tmp___20;
  uint32_t *tmp___21;
  uint32_t tmp___22;
  uint32_t *tmp___23;
  uint32_t tmp___24;
  uint32_t *tmp___25;
  uint32_t tmp___26;
  uint32_t *tmp___27;
  uint32_t tmp___28;
  uint32_t *tmp___29;
  uint32_t tmp___30;

   
                                     
                                    
                          
               
               
               
               
                                                            
                                      
                        
     
               

                                                          
                         
       
                          
                 
                 
                 
                 
                 
                  
              
                                   
                       
                                                           
                
                                 
               
                             
       
                     
                 
                      
              
                                   
                           
                                                           
                
                                  
               
                             
       
                     
                 
                      
              
                                   
                           
                                                          
                
                                  
               
                             
       
                     
                 
                      
              
                                   
                           
                                                           
                
                                  
               
                             
       
                     
                 
                      
              
                                   
                           
                                                           
                
                                 
               
                             
       
                     
                 
                      
              
                                    
                            
                                                            
                
                                  
               
                             
       
                     
                 
                       
              
                                    
                             
                                                            
                
                                  
               
                             
       
                     
                 
                       
              
                                    
                             
                                                            
                
                                  
               
                             
       
                     
                 
                       
              
                                    
                             
                                                            
                
                                 
               
                             
       
                     
                 
                       
              
                                    
                             
                                                            
                
                                  
               
                             
       
                     
                 
                       
              
                                    
                             
                                                            
                
                                  
               
                              
       
                      
                 
                       
              
                                    
                             
                                                            
                
                                  
               
                              
       
                      
                 
                       
              
                                    
                             
                                                            
                
                                 
               
                              
       
                      
                 
                       
              
                                    
                             
                                                            
                
                                  
               
                              
       
                      
                 
                       
              
                                    
                             
                                                            
                
                                  
               
                              
       
                      
                 
                       
              
                                    
                             
                                                            
                
                                  
               
                              
       
                      
                 
                                                                    
                                 
               
                              
       
                      
                 
                                                                    
                                 
               
                              
       
                      
                 
                                                                    
                                  
               
                              
       
                      
                 
                                                                    
                                  
               
                              
       
                      
                 
                                                                    
                                 
               
                              
       
                      
                 
                                                                   
                                 
               
                              
       
                      
                 
                                                                     
                                  
               
                              
       
                      
                 
                                                                    
                                  
               
                              
       
                      
                 
                                                                   
                                 
               
                              
       
                      
                 
                                                                     
                                 
               
                              
       
                      
                 
                                                                    
                                  
               
                              
       
                      
                 
                                                                    
                                  
               
                              
       
                      
                 
                                                                     
                                 
               
                              
       
                      
                 
                                                                    
                                 
               
                              
       
                      
                 
                                                                    
                                  
               
                              
       
                      
                 
                                                                     
                                  
               
                              
       
                      
                 
                                                              
                                 
               
                              
       
                      
                 
                                                              
                                  
               
                              
       
                      
                 
                                                               
                                  
               
                              
       
                      
                 
                                                               
                                 
               
                              
       
                      
                 
                                                              
                                 
               
                              
       
                      
                 
                                                              
                                  
               
                              
       
                      
                 
                                                              
                                  
               
                              
       
                      
                 
                                                               
                                 
               
                              
       
                      
                 
                                                              
                                 
               
                              
       
                      
                 
                                                              
                                  
               
                              
       
                      
                 
                                                              
                                  
               
                              
       
                      
                 
                                                            
                                 
               
                              
       
                      
                 
                                                              
                                 
               
                              
       
                      
                 
                                                               
                                  
               
                              
       
                      
                 
                                                              
                                  
               
                              
       
                      
                 
                                                              
                                 
               
                              
       
                      
                 
                                                               
                                 
               
                              
       
                      
                 
                                                               
                                  
               
                              
       
                      
                 
                                                                
                                  
               
                              
       
                      
                 
                                                               
                                  
               
                              
       
                      
                 
                                                                
                                 
               
                              
       
                      
                 
                                                               
                                  
               
                              
       
                      
                 
                                                                
                                  
               
                              
       
                      
                 
                                                               
                                  
               
                              
       
                      
                 
                                                               
                                 
               
                              
       
                      
                 
                                                                
                                  
               
                              
       
                      
                 
                                                               
                                  
               
                              
       
                      
                 
                                                                
                                  
               
                              
       
                      
                 
                                                               
                                 
               
                              
       
                      
                 
                                                                
                                  
               
                              
       
                      
                 
                                                              
                                  
               
                              
       
                      
                 
                                                               
                                  
               
                              
       
                     
                  
                  
                  
                  
     
              
               
               
               
               
           
   
}
int mbsnwidth(char const *string, size_t nbytes, int flags);
extern __attribute__((__nothrow__)) int(__attribute__((__leaf__))
                                        wcwidth)(wchar_t __c);
extern __attribute__((__nothrow__)) int(__attribute__((__leaf__))
                                        iswcntrl)(wint_t __wc);
int mbsnwidth(char const *string, size_t nbytes, int flags) {
  char const *p;
  char const *plimit;
  int width;
  mbstate_t mbstate;
  wchar_t wc;
  size_t bytes;
  int w;
  int tmp;
  int tmp___0;
  size_t tmp___1;
  unsigned char c;
  char const *tmp___2;
  unsigned short const **tmp___3;
  unsigned short const **tmp___4;

  {
               
                        
              
                                       
                        
                 

                                                          
                           
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                    
                       
         
                                     
                       
         
                                     
                       
         
                                     
                       
         
                                     
                       
         
                                     
                       
         
                                     
                       
         
                                     
                       
         
                                     
                       
         
                                     
                       
         
                                     
                       
         
                                     
                       
         
                                     
                       
         
                                     
                       
         
                                     
                       
         
                                     
                       
         
                                     
                       
         
                                     
                       
         
                                     
                       
         
                                     
                       
         
                                     
                       
         
                                     
                       
         
                                     
                       
         
                                     
                       
         
                                     
                       
         
                                     
                       
         
                                     
                       
         
                                     
                       
         
                            
              
            
                
                          
                     
                                                       
                   
                                                                  
                                              
                               
                  
                      
                                   
                    
                          
             
           
                                              
                               
                         
                      
                                   
                    
                          
             
           
                             
                              
           
                          
                       
                                         
                            
             
                       
                  
                               
                                         
                         
                                          
                                
                 
                        
               
                    
                          
             
           
                     
                                                           
                        
                                 
           
         
                       
                          
                    
       
                 
                     
     
               

                                                        
                             
       
                  
          
                                  
                                
                                                      
                                  
                        
         
                
              
                           
                                    
                                                         
                                      
                            
             
                    
           
                
                      
         
       
     
                   
                   
  overflow:
    return (2147483647);
  }
}
int(__attribute__((__nonnull__(1, 2))) mbscasecmp)(char const *s1,
                                                   char const *s2);
extern __attribute__((__nothrow__))
wint_t(__attribute__((__leaf__)) towlower)(wint_t __wc);
                                                                  
                                                                    
                        
                        
          
             
                 
              
              
              
              
              
              
              
               
               
               
               
               
               
               
                          
                          
                   
                   
               
                                  
               
                                  
                  

   
                                                 
                 
     
                                        
                         
                         
                                
                                                                
                                 
                         
                                
                                                                
                                 
                 
                                   
                                 
                                  
                         
                  
                         
           
                
                       
         
                       
                                     
                                   
                                    
                           
                    
                           
             
                  
                         
           
                          
                             
           
                
                           
         
                                 
                                   
                                                 
                                                     
                                              
                  
                         
           
                             
                
                                   
                         
                  
                                                     
                                                           
                                                                             
                                 
                    
                                                      
                                                             
                                                                               
                                  
                              
                        
                               
                 
                                  
                      
                                                             
                                                                               
                                   
                              
                        
                               
                 
                                  
               
                                 
             
                                
           
                              
         
                       
                       
                       
         
                                         
                                   
                                         
                                   
       
                
                                 
                               
                                
                       
                
                       
         
              
                     
       
                     
                   
       
                                 
                               
                                
                       
                
                       
         
              
                     
       
                     
                    
       
                 
            
                                     
                                     
                 
                                   
                                                         
                                       
                                       
                
                                  
         
                                   
                                                         
                                       
                                       
                
                                  
         
                           
                               
         
             
             
                                    
                               
         
       
                     
                                 
     
   
 
                                        
                                                              
                                                                     
extern __attribute__((__nothrow__, __noreturn__)) void(__attribute__((__leaf__))
                                                       exit)(int __status);
extern int optind;
extern __attribute__((__nothrow__)) int(__attribute__((__leaf__)) getopt_long)(
    int ___argc, char *const *___argv, char const *__shortopts,
    struct option const *__longopts, int *__longind);
extern
    __attribute__((__nothrow__)) int(__attribute__((__nonnull__(1), __leaf__))
                                     pthread_mutex_init)(
        pthread_mutex_t *__mutex, pthread_mutexattr_t const *__mutexattr);
#pragma weak pthread_mutex_init
#pragma weak pthread_mutex_lock
#pragma weak pthread_mutex_unlock
#pragma weak pthread_mutex_destroy
#pragma weak pthread_rwlock_init
#pragma weak pthread_rwlock_rdlock
#pragma weak pthread_rwlock_wrlock
#pragma weak pthread_rwlock_unlock
#pragma weak pthread_rwlock_destroy
#pragma weak pthread_once
#pragma weak pthread_cond_init
#pragma weak pthread_cond_wait
#pragma weak pthread_cond_signal
#pragma weak pthread_cond_broadcast
#pragma weak pthread_cond_destroy
#pragma weak pthread_mutexattr_init
#pragma weak pthread_mutexattr_settype
#pragma weak pthread_mutexattr_destroy
#pragma weak pthread_self
#pragma weak pthread_cancel
extern __attribute__((__nothrow__))
FILE *(__attribute__((__leaf__)) fdopen)(int __fd, char const *__modes);
extern int fscanf(FILE *__restrict __stream, char const *__restrict __format,
                  ...);
extern int ungetc(int __c, FILE *__stream);
static char const *volatile charset_aliases;
static char const *get_charset_aliases(void) {
  char const *cp;
  char const *dir;
  char const *base;
  char *file_name___3;
  size_t dir_len___0;
  size_t tmp;
  size_t base_len___0;
  size_t tmp___0;
  int add_slash;
  int tmp___1;
  int fd;
  FILE *fp;
  char *res_ptr;
  size_t res_size;
  int c;
  char buf1[51];
  char buf2[51];
  size_t l1;
  size_t l2;
  char *old_res_ptr;
  int tmp___3;

  {
                                       
                                                         {
      base = "charset.alias";
      dir = (char const *)getenv("CHARSETALIASDIR");
                                                             
                               
              
                                           
                                 
         
       
      tmp = strlen(dir);
      dir_len___0 = tmp;
      tmp___0 = strlen(base);
      base_len___0 = tmp___0;
                             {
                                                                {
          tmp___1 = 1;
        }       
                      
         
      }       
                    
       
      add_slash = tmp___1;
      file_name___3 = (char *)malloc(
          ((dir_len___0 + (size_t)add_slash) + base_len___0) + 1UL);
                                                                       
                                                                      
                        
                                                     
         
                                                                   
                                                       
       
                                                                      {
        cp = "";
      }       
                                                       
                     
                  
                
                               
                                                                
                      
                    
                  
                                          
                                 
                       
                                    
                            
                                 
               
                            
                            
                      
                              
                              
                        
                               
                                
                   
                 
               
                            
                           
                                        
                                
                                         
                          
                                  
                                           
                     
                   
                 
                               
                              
                                   
                 
                            
               
                            
                                                            
                                
                                 
               
                                                
                                                
                                    
                                    
                                                   
                                                         
                      
                                                    
                                                                           
               
                                                                         
                                     
                                          
                                 
               
                                                                      
                                           
                                                                              
                    
             
                      
                           
                                  
                      
                    
                                                   
                                         
             
           
         
                                    
       
                           
    }
    return (cp);
  }
}
char const *locale_charset(void) {
  char const *codeset;
  char const *aliases;
  size_t tmp;
  size_t tmp___0;
  size_t tmp___1;
  int tmp___2;

  {
                                            
                                                               
                   
     
                                    
               

                                          
                         
       
                                         
                         
                                  
                                          
                         
              
                                                
                                                 
                                      
                                              
                             
           
         
       
                            
                           
                                
                               
     
               
                                          {
      codeset = "ASCII";
    }
    return (codeset);
  }
}
extern __attribute__((__nothrow__)) long double(
    __attribute__((__nonnull__(1), __leaf__))
    strtold)(char const *__restrict __nptr, char **__restrict __endptr);
#pragma GCC diagnostic ignored "-Wtype-limits"
#pragma GCC diagnostic ignored "-Wtype-limits"
extern __attribute__((__nothrow__)) struct lconv *(__attribute__((__leaf__))
                                                   localeconv)(void);
struct heap *heap_alloc(int (*compare___0)(void const *, void const *),
                        size_t n_reserve);
void heap_free(struct heap *heap);
int heap_insert(struct heap *heap, void *item);
void *heap_remove_top(struct heap *heap);
static size_t heapify_down(void **array, size_t count, size_t initial,
                           int (*compare___0)(void const *, void const *));
static void heapify_up(void **array, size_t count,
                       int (*compare___0)(void const *, void const *));
static int heap_default_compare(void const *a, void const *b) {

   

    return (0);
   

}
struct heap *heap_alloc(int (*compare___0)(void const *, void const *),
                        size_t n_reserve) {
  struct heap *heap;
  struct heap *tmp;

  {
    tmp = (struct heap *)xmalloc(sizeof(*heap));
    heap = tmp;
                           
                            
     
    heap->array = (void **)xnmalloc(n_reserve, sizeof(*(heap->array)));
                                   
                               
    heap->count = (size_t)0;
                      
                                  
            
                                            
     
    return (heap);
  }
}
void heap_free(struct heap *heap) {

   

                              
                       
           
   

}
int heap_insert(struct heap *heap, void *item) {

   

                                              
                                                                             
                                                                
     
    (heap->count)++;
    *(heap->array + heap->count) = item;
                                                        
    return (0);
   

}
void *heap_remove_top(struct heap *heap) {
  void *top;
  size_t tmp;

  {
                             
                         
     
    top = *(heap->array + 1);
                      
    (heap->count)--;
                                              
                                                                     
    return (top);
  }
}
static size_t heapify_down(void **array, size_t count, size_t initial,
                           int (*compare___0)(void const *, void const *)) {
  void *element;
  size_t parent;
  size_t child;
  int tmp;
  int tmp___0;

  {
                                 
    parent = initial;
               

                                     
                         
       
                           
                          
                                                            
                                                                     
                      
                  
         
       
               
                                                                                
                         
                         
       
                                           
                     
     
              
                                
    return (parent);
  }
}
static void heapify_up(void **array, size_t count,
                       int (*compare___0)(void const *, void const *)) {
  size_t k;
  void *new_element;
  int tmp;

   
              
                               
               

                     
                                                              
                                                        
                          
                           
         
              
                         
       
                                        
               
     
              
                               
           
   
}
size_t hash_string(char const *string, size_t n_buckets)
    __attribute__((__pure__));
_Bool(__attribute__((__warn_unused_result__))
      hash_rehash)(Hash_table *table___0, size_t candidate);
__inline static size_t rotr_sz(size_t x, int n) {

   

    return (((x >> n) | (x << (8UL * sizeof(x) - (unsigned long)n))) &
            0xffffffffffffffffUL);
   

}
static struct hash_tuning const default_tuning = {0.0f, 1.0f, 0.8f, 1.414f,
                                                  (_Bool)0};
static struct hash_entry *safe_hasher(Hash_table const *table___0,
                                      void const *key) {
  size_t n;
  size_t tmp;

  {
    tmp = (*(table___0->hasher))(key, (size_t)table___0->n_buckets);
    n = tmp;
                                              
              
     
    return ((struct hash_entry *)(table___0->bucket + n));
  }
}
size_t hash_string(char const *string, size_t n_buckets)
    __attribute__((__pure__));
                                                          
               
                   

   
                      
               
                                  
                
                         
       
                                                      
               
     
               
                   
   
 
static _Bool is_prime(size_t candidate) __attribute__((__const__));
static _Bool is_prime(size_t candidate) __attribute__((__const__));
static _Bool is_prime(size_t candidate) {
  size_t divisor;
  size_t square;
  int tmp;

  {
                        
                               
              {

                               
                                     
                           
         
              
                         
       
                
                              
                
    }
  while_break:;
                             {
      tmp = 1;
    }       
              
     
    return ((_Bool)tmp);
  }
}
static size_t next_prime(size_t candidate) __attribute__((__const__));
static size_t next_prime(size_t candidate) __attribute__((__const__));
static size_t next_prime(size_t candidate) {
  _Bool tmp;

  {
                           
                             
     
                     
               

                                              
                                  
                  
                           
         
              
                         
       
                       
     
               
    return (candidate);
  }
}
static size_t raw_hasher(void const *data, size_t n) {
  size_t val;
  size_t tmp;

  {
    tmp = rotr_sz((size_t)data, 3);
    val = tmp;
    return (val % n);
  }
}
static _Bool raw_comparator(void const *a, void const *b) {

   

    return ((_Bool)((unsigned long)a == (unsigned long)b));
   

}
static _Bool check_tuning(Hash_table *table___0) {
  Hash_tuning const *tuning;
  float epsilon;

  {
    tuning = table___0->tuning;
                                                                   {
      return ((_Bool)1);
    }
                   
                                                    
                                                                         
                                                                
                                                           
                                                                 
                                        
                                                            
                                                                     
                                               
                                    
                 
               
             
           
         
       
     
                                        
                      
  }
}
static size_t __attribute__((__pure__))
compute_bucket_size(size_t candidate, Hash_tuning const *tuning) {
  float new_candidate;
  int tmp;

  {
                               {
      new_candidate =
          (float)((float const)candidate / tuning->growth_threshold);
                                                        {
        return ((size_t __attribute__((__pure__)))0);
      }
                                        
    }
                                      
                                              
               
            
               
     
                                                                
                                                   
     
                                                         
  }
}
Hash_table *(__attribute__((__warn_unused_result__))
             hash_initialize)(size_t candidate, Hash_tuning const *tuning,
                              size_t (*hasher)(void const *, size_t),
                              _Bool (*comparator)(void const *, void const *),
                              void (*data_freer)(void *)) {
  Hash_table *table___0;
  _Bool tmp;

  {
                                                              
                           
     
                                                                  
                                   
     
                                                         
                                                                 
                                         
     
                  
                               
     
                               
                                  
               
                
     
                                                                          
                                
                
     
                                                    
                                                            
                                                                         
                
     
                             
                                                                              
                                          
                                     
                               
                                       
                                       
    table___0->free_entry_list = (struct hash_entry *)((void *)0);
    return (table___0);
       
                            
                                       
  }
}
static struct hash_entry *allocate_entry(Hash_table *table___0) {
  struct hash_entry *new;

  {
                                    {
      new = table___0->free_entry_list;
                                             
    }       
                                                      
     
    return (new);
  }
}
static void free_entry(Hash_table *table___0, struct hash_entry *entry) {

   

                            
                                             
                                       
           
   

}
static void *hash_find_entry(Hash_table *table___0, void const *entry,
                             struct hash_entry **bucket_head, _Bool delete) {
  struct hash_entry *bucket;
  struct hash_entry *tmp;
  struct hash_entry *cursor;
  void *data;
  struct hash_entry *next;
  _Bool tmp___0;
  void *data___0;
  struct hash_entry *next___0;
  _Bool tmp___1;

  {
                                                            
                 
                          
                                                                    
                         
     
                                                              
              
           {
                                                                              
                   {
      _L:
        data = bucket->data;
                    {
                            {
            next = bucket->next;
            *bucket = *next;
                                        
          }       
                                     
           
        }
        return (data);
      }
    }
                    
               

                          
                         
       
                                                                        
                    
              
                                             
                                                       
                      
               
                                          
                       
                                    
                                          
                                            
           
                            
         
       
                            
     
               
                       
  }
}
static _Bool transfer_entries(Hash_table *dst, Hash_table *src, _Bool safe) {
  struct hash_entry *bucket;
  struct hash_entry *cursor;
  struct hash_entry *next;
  void *data;
  struct hash_entry *new_bucket;
  struct hash_entry *new_entry;
  struct hash_entry *tmp;

  {
                         
               

                                                                        
                         
       
                         
                              
                   

                        
                                 
           
                              
                                                                                
                              
                                 
                                            
                                      
                  
                                    
                                    
                                    
           
                        
         
                      
                            
                                                        
                   
                      
         
                                                                              
                               
                                    
                          
                                                                       
                              
           
                                 
                                             
                                       
                
                                  
                                  
         
                                 
                                
       
           
               
     
               
    return ((_Bool)1);
  }
}
_Bool(__attribute__((__warn_unused_result__))
      hash_rehash)(Hash_table *table___0, size_t candidate) {
  Hash_table storage;
  Hash_table *new_table;
  size_t new_size;
  size_t tmp;
  _Bool tmp___0;
  _Bool tmp___1;
  _Bool tmp___2;

  {
                                                                    
                   
                    
                        
     
                                           
                        
     
                         
                       
                                                                            
                                                                         
                        
     
                                    
                             
                                                                  
                                          
                                     
                                          
                                          
                                                  
                                                  
    new_table->free_entry_list = table___0->free_entry_list;
                                                               
                 {
                                      
                                            
                                                        
                                                  
                                                            
                                                              
      return ((_Bool)1);
    }
                                                            
                                                               
                  
                                                                 
                     
                
       
            
              
     
                                    
                      
  }
}
int hash_insert_if_absent(Hash_table *table___0, void const *entry,
                          void const **matched_ent) {
  void *data;
  struct hash_entry *bucket;
  Hash_tuning const *tuning;
  float candidate;
  float tmp;
  _Bool tmp___0;
  void *tmp___1;
  struct hash_entry *new_entry;
  struct hash_entry *tmp___2;

  {
                 
              
     
                                                                
                                                            
                        
                                          
       
                 
     
                                                
                                               
                                                
                              
                                                  
                                                 
                                                  
                                   
                                   
                                                           
                                               
                
                                                            
                                                 
                                                  
         
                        
                                                       
                      
         
                                                            
                       
                      
         
                                                                       
                                                                   
                  
         
       
     
                       
                                          
                          
                                                                   
                    
       
                                      
                                     
                               
                               
                 
     
                                 
                             
                                  
    return (1);
  }
}
void *(__attribute__((__warn_unused_result__))
       hash_insert)(Hash_table *table___0, void const *entry) {
  void const *matched_ent;
  int err;
  int tmp;
  void const *tmp___0;
  void *tmp___1;

  {
    tmp = hash_insert_if_absent(table___0, entry, &matched_ent);
    err = tmp;
                   {
      tmp___1 = (void *)0;
    }       
                     
                              
              
                        
       
                                
     
    return (tmp___1);
  }
}
void *hash_delete(Hash_table *table___0, void const *entry) {
  void *data;
  struct hash_entry *bucket;
  Hash_tuning const *tuning;
  size_t candidate;
  float tmp;
  struct hash_entry *cursor;
  struct hash_entry *next;
  _Bool tmp___0;

  {
    data = hash_find_entry(table___0, entry, &bucket, (_Bool)1);
               {
      return ((void *)0);
    }
                             
                        
                                    
                                                  
                                                 
                                                  
                                
                                                    
                                                   
                                                    
                                     
                                     
                                                             
                                                 
                  
                                                              
                                                   
                                                    
           
                                  
                                                      
                         
                                                
                       

                            
                                 
               
                                  
                                   
                            
             
                      
                                                                          
           
         
       
     
                  
  }
}
size_t hash_pjw(void const *x, size_t tablesize) __attribute__((__pure__));
size_t triple_hash(void const *x, size_t table_size) __attribute__((__pure__));
                                                      
                           
             
                 

   
                                   
                                                          
                  
                                                           
   
 
_Bool triple_compare_ino_str(void const *x, void const *y)
    __attribute__((__pure__));
                                                            
                           
                           
              
              

   
                                   
                                   
                                 
                                   
                                                                       
                           
                      
                
                      
         
              
                    
       
            
                  
     
                            
   
 
size_t hash_pjw(void const *x, size_t tablesize) __attribute__((__pure__));
size_t hash_pjw(void const *x, size_t tablesize) {
  char const *s;
  size_t h;

  {
                  
                        
              {

                
                         
       
      h = (unsigned long)*s + ((h << 9) | (h >> (sizeof(size_t) * 8UL - 9UL)));
      s++;
    }
  while_break:;
    return (h % tablesize);
  }
}
_Bool hard_locale(int category);
extern __attribute__((__nothrow__)) char *(__attribute__((__leaf__))
                                           setlocale)(int __category,
                                                      char const *__locale);
_Bool hard_locale(int category) {
  _Bool hard;
  char const *p;
  char const *tmp;
  int tmp___0;
  int tmp___1;
  char *locale;
  char *tmp___2;
  int tmp___3;
  int tmp___4;

  {
                    
                                                                       
            
    if (p) {
             {
        tmp___0 = strcmp(p, "C");
                          {
          hard = (_Bool)0;
        }       
                                       
                             
                            
           
         
      }       
                            
                         
                     
                                                     
                  
                                                      
                               
                              
                    
                      
             
                  
             
                                                           
                    
                                                        
                                 
                                
               
             
           
                                                    
                               
         
       
    }
    return (hard);
  }
}
extern int fflush_unlocked(FILE *__stream);
extern void(__attribute__((__nonnull__(1, 4)))
            qsort)(void *__base, size_t __nmemb, size_t __size,
                   int (*__compar)(void const *, void const *));
extern int fseeko(FILE *__stream, __off_t __off, int __whence);
extern __attribute__((__nothrow__))
__off_t(__attribute__((__leaf__)) lseek)(int __fd, __off_t __offset,
                                         int __whence);
int(__attribute__((__nonnull__(1))) rpl_fseeko)(FILE *fp, off_t offset,
                                                int whence) {
  off_t pos;
  int tmp;
  off_t tmp___0;
  int tmp___1;

  {
                                                                            {
                                             
                                             {
                                                                            {
          tmp = fileno(fp);
          tmp___0 = lseek(tmp, offset, whence);
          pos = tmp___0;
                          {
            return (-1);
          }
                            
                            
                     
        }
      }
    }
                                         
                     
  }
}
extern __attribute__((__nothrow__)) int(__attribute__((__leaf__))
                                        dup2)(int __fd, int __fd2);
FILE *fopen_safer(char const *file, char const *mode) {
  FILE *fp;
  FILE *tmp;
  int fd;
  int tmp___0;
  int f;
  int tmp___1;
  int e;
  int *tmp___2;
  int *tmp___3;
  int e___0;
  int *tmp___4;
  int *tmp___5;
  int tmp___6;

  {
    tmp = fopen(file, mode);
    fp = tmp;
             
                           
                   
                    
                      
                                  
                      
                      
                                         
                         
                           
                                         
                         
                                         
           
                                   
                             
                    
                  
                                 
                      
               
                                           
                               
                       
                                           
                               
                                           
             
           
         
       
     
    return (fp);
  }
}
int filevercmp(char const *s1, char const *s2);
static char const *match_suffix(char const **str) {
  char const *match;
  _Bool read_alpha;
  _Bool tmp;
  _Bool tmp___0;

  {
                                      
                          
              {

                     
                         
       
                      {
                              
        tmp = c_isalpha((int)*(*str));
                  {
                                   {
            match = (char const *)((void *)0);
          }
        }
      }       
                                 
                                
                       
                         
           
                
                                            
                         
                                      
                                                
             
           
         
       
               
    }
  while_break:;
    return (match);
  }
}
__inline static int order(unsigned char c) {
  _Bool tmp;
  _Bool tmp___0;

  {
                                
                 {
      return (0);
    }       
                              
                
                        
              
                            
                      
                
                                      
         
       
     
  }
}
static int __attribute__((__pure__))
verrevcmp(char const *s1, size_t s1_len, char const *s2, size_t s2_len) {
  size_t s1_pos;
  size_t s2_pos;
  int first_diff;
  int s1_c;
  int tmp;
  int tmp___0;
  int s2_c;
  int tmp___1;
  int tmp___2;
  _Bool tmp___3;
  _Bool tmp___4;
  _Bool tmp___5;
  _Bool tmp___6;
  _Bool tmp___7;
  _Bool tmp___8;

  {
                       
                       
               

                               
                                 
                           
         
       
                     
                 

                              
                                                   
                        
                    
           
                
           
                                
                                                     
                          
                                   
             
                  
                                 
           
         
                               
                      
                
                                                     
                        
         
                       
                               
                      
                
                                                         
                            
         
                       
                           
                                                                
         
                 
                 
       
                     
                 

                                                   
                               
         
                 
       
                     
                 

                                                   
                               
         
                 
       
                     
                 
                                                 
                      
                                                   
                         
                                 
           
                
                               
         
                          
                      
                                                                               
         
                 
                 
       
                    
                                               
                    
                                                  
       
                                               
                    
                                                     
       
                       
                                                           
       
     
               
    return ((int __attribute__((__pure__)))0);
  }
}
int filevercmp(char const *s1, char const *s2) {
  char const *s1_pos;
  char const *s2_pos;
  char const *s1_suffix;
  char const *s2_suffix;
  size_t s1_len;
  size_t s2_len;
  int result;
  int simple_cmp;
  int tmp;
  int tmp___0;
  int tmp___1;
  int tmp___2;
  int tmp___3;
  char const *tmp___4;
  char const *tmp___5;
  int tmp___6;
  int tmp___7;

  {
                         
                     
                          
                 
     
               
                  
     
               
                 
     
                              
                       
                  
     
                              
                       
                 
     
                               
                       
                  
     
                               
                       
                 
     
                                {
                                  {
        return (-1);
      }
    }
                                 
                                   
                   
       
     
                                 
                                   
             
             
       
     
                
                
                                      
                                      
                    
                          
            
                       
     
                                    
                    
                          
            
                       
     
                                    
                    
              
            
                      
         
                               
                                            
                             
                                           
                                           
           
         
       
     
                                                    
                      
                           
            
                       
     
                     
  }
}
#pragma GCC diagnostic ignored "-Wsuggest-attribute=const"
extern int fflush(FILE *__stream);
extern __attribute__((__nothrow__)) int(__attribute__((__leaf__))
                                        __freading)(FILE *__fp);
__inline static void clear_ungetc_buffer_preserving_position(FILE *fp) {

   

                           
                                  
     
           
   

}
int rpl_fflush(FILE *stream) {
  int tmp;
  int tmp___0;
  int tmp___1;

  {
                                                              
                           
                   
            
                                   
                            
                             
                     
       
     
                                                    
    tmp___1 = fflush(stream);
    return (tmp___1);
  }
}
int fd_safer(int fd) {
  int f;
  int tmp;
  int e;
  int *tmp___0;
  int *tmp___1;

  {
                  
                    
                            
                
                                     
                     
                  
                                     
                     
               
       
     
    return (fd);
  }
}
extern int fcntl(int __fd, int __cmd, ...);
static int have_dupfd_cloexec = 0;
int rpl_fcntl(int fd, int action, ...) {
  va_list arg;
  int result;
  int target;
  int tmp;
  int *tmp___0;
  int flags;
  int tmp___1;
  int saved_errno;
  int *tmp___2;
  int *tmp___3;
  int tmp___4;
  void *p;
  void *tmp___5;

  {
    result = -1;
    __builtin_va_start(arg, action);
                        {
      goto case_1030;
    }
    goto switch_default;
  case_1030:
    tmp = __builtin_va_arg(arg, int);
    target = tmp;
                                 {
      result = fcntl(fd, action, target);
                       {
        have_dupfd_cloexec = 1;
      }       
                                     
                             
                                 
                
                                            
                           
                              
           
                                  
         
       
    }       
                                        
     
                     {
                                    {
        tmp___1 = fcntl(result, 1);
        flags = tmp___1;
                        
                  
               {
          tmp___4 = fcntl(result, 2, flags | 1);
                             {
          _L:
            tmp___2 = __errno_location();
            saved_errno = *tmp___2;
            close(result);
            tmp___3 = __errno_location();
            *tmp___3 = saved_errno;
            result = -1;
          }
        }
      }
    }
    goto switch_break;
  switch_default:
    tmp___5 = __builtin_va_arg(arg, void *);
    p = tmp___5;
    result = fcntl(fd, action, p);
    goto switch_break;
  switch_break:
    __builtin_va_end(arg);
    return (result);
  }
}
extern int fclose(FILE *__stream);
int(__attribute__((__nonnull__(1))) rpl_fclose)(FILE *fp) {
  int saved_errno;
  int fd;
  int result;
  int tmp;
  int *tmp___0;
  int tmp___1;
  int tmp___2;
  __off_t tmp___3;
  int tmp___4;
  int *tmp___5;

  {
                    
               
    fd = fileno(fp);
                {
      tmp = fclose(fp);
      return (tmp);
    }
                             
                       
                           
                                              
                           
         
                                 
                      
                                       
                                 
         
       
            
              
     
                        
                           
                                   
                             
                  
     
                    
  }
}
extern
    __attribute__((__nothrow__)) int(__attribute__((__nonnull__(1), __leaf__))
                                     sigemptyset)(sigset_t *__set);
extern
    __attribute__((__nothrow__)) int(__attribute__((__nonnull__(1), __leaf__))
                                     sigaddset)(sigset_t *__set, int __signo);
extern __attribute__((__nothrow__)) int(__attribute__((__leaf__)) sigaction)(
    int __sig, struct sigaction const *__restrict __act,
    struct sigaction *__restrict __oact);
extern
    __attribute__((__nothrow__)) int(__attribute__((__leaf__))
                                     posix_fadvise)(int __fd, off_t __offset,
                                                    off_t __len, int __advise);
void fdadvise(int fd, off_t offset, off_t len, fadvice_t advice);
void fadvise(FILE *fp, fadvice_t advice);
void fdadvise(int fd, off_t offset, off_t len, fadvice_t advice) {
  int __x;
  int tmp;

   
                                                      
              
           
   
}
void fadvise(FILE *fp, fadvice_t advice) {
  int tmp;

   
             
                       
                                                
     
           
   
}
int volatile exit_failure = (int volatile)1;
int dup_safer(int fd) {
  int tmp;

  {
    tmp = rpl_fcntl(fd, 0, 3);
    return (tmp);
  }
}
#pragma GCC diagnostic ignored "-Wtype-limits"
#pragma GCC diagnostic ignored "-Wtype-limits"
#pragma weak pthread_key_create
#pragma weak pthread_getspecific
#pragma weak pthread_setspecific
#pragma weak pthread_key_delete
#pragma weak pthread_self
#pragma weak pthread_cancel
#pragma GCC diagnostic ignored "-Wsuggest-attribute=pure"
#pragma GCC diagnostic ignored "-Wsuggest-attribute=const"
#pragma GCC diagnostic ignored "-Wtype-limits"
#pragma weak pthread_mutex_init
#pragma weak pthread_mutex_lock
#pragma weak pthread_mutex_unlock
#pragma weak pthread_mutex_destroy
#pragma weak pthread_rwlock_init
#pragma weak pthread_rwlock_rdlock
#pragma weak pthread_rwlock_wrlock
#pragma weak pthread_rwlock_unlock
#pragma weak pthread_rwlock_destroy
#pragma weak pthread_once
#pragma weak pthread_cond_init
#pragma weak pthread_cond_wait
#pragma weak pthread_cond_signal
#pragma weak pthread_cond_broadcast
#pragma weak pthread_cond_destroy
#pragma weak pthread_mutexattr_init
#pragma weak pthread_mutexattr_settype
#pragma weak pthread_mutexattr_destroy
#pragma weak pthread_self
#pragma weak pthread_cancel
#pragma GCC diagnostic ignored "-Wtype-limits"
#pragma GCC diagnostic ignored "-Wtype-limits"
#pragma GCC diagnostic ignored "-Wsuggest-attribute=const"
char const *Version = "8.16";
extern char *optarg;
extern __attribute__((__nothrow__)) int(__attribute__((__nonnull__(1, 3)))
                                        pthread_create)(
    pthread_t *__restrict __newthread, pthread_attr_t const *__restrict __attr,
    void *(*__start_routine)(void *), void *__restrict __arg);
extern int pthread_join(pthread_t __th, void **__thread_return);
extern __attribute__((__nothrow__)) int(__attribute__((
    __nonnull__(1), __leaf__)) pthread_mutex_destroy)(pthread_mutex_t *__mutex);
extern __attribute__((__nothrow__)) int(__attribute__((
    __nonnull__(1))) pthread_mutex_lock)(pthread_mutex_t *__mutex);
extern __attribute__((__nothrow__)) int(__attribute__((
    __nonnull__(1))) pthread_mutex_unlock)(pthread_mutex_t *__mutex);
extern __attribute__((__nothrow__)) int(
    __attribute__((__nonnull__(1), __leaf__))
    pthread_cond_init)(pthread_cond_t *__restrict __cond,
                       pthread_condattr_t const *__restrict __cond_attr);
extern __attribute__((__nothrow__)) int(__attribute__((
    __nonnull__(1), __leaf__)) pthread_cond_destroy)(pthread_cond_t *__cond);
extern __attribute__((__nothrow__)) int(__attribute__((
    __nonnull__(1))) pthread_cond_signal)(pthread_cond_t *__cond);
extern int(__attribute__((__nonnull__(1, 2)))
           pthread_cond_wait)(pthread_cond_t *__restrict __cond,
                              pthread_mutex_t *__restrict __mutex);
extern __attribute__((__nothrow__))
__sighandler_t(__attribute__((__leaf__)) signal)(int __sig,
                                                 void (*__handler)(int));
extern
    __attribute__((__nothrow__)) int(__attribute__((__nonnull__(1), __leaf__))
                                     sigismember)(sigset_t const *__set,
                                                  int __signo);
extern __attribute__((__nothrow__)) int(
    __attribute__((__nonnull__(1, 2), __leaf__))
    execlp)(char const *__file, char const *__arg, ...);
extern __attribute__((__nothrow__))
size_t(__attribute__((__nonnull__(2), __leaf__))
       strxfrm)(char *__restrict __dest, char const *__restrict __src,
                size_t __n);
__inline static void initialize_exit_failure(int status) {

   

                      
                                          
     
           
   

}
extern __attribute__((__nothrow__)) char *(__attribute__((__leaf__))
                                           ngettext)(char const *__msgid1,
                                                     char const *__msgid2,
                                                     unsigned long __n)
    __attribute__((__format_arg__(2), __format_arg__(1)));
extern
    __attribute__((__nothrow__)) char *(__attribute__((__leaf__))
                                        textdomain)(char const *__domainname);
extern __attribute__((__nothrow__)) char *(__attribute__((
    __leaf__)) bindtextdomain)(char const *__domainname, char const *__dirname);
__inline static unsigned long select_plural(uintmax_t n) {
  uintmax_t tmp;

  {
                                   {
      tmp = n;
    }       
                                      
     
    return (tmp);
  }
}
extern int fputc_unlocked(int __c, FILE *__stream);
extern int putchar_unlocked(int __c);
extern size_t fwrite_unlocked(void const *__restrict __ptr, size_t __size,
                              size_t __n, FILE *__restrict __stream);
extern __attribute__((__nothrow__)) void(__attribute__((__leaf__))
                                         clearerr_unlocked)(FILE *__stream);
__inline static void emit_ancillary_info(void) {
  char *tmp;
  char *tmp___0;
  char *tmp___1;
  char *tmp___2;
  char const *lc_messages;
  char const *tmp___3;
  char *tmp___4;
  char *tmp___5;
  int tmp___6;
  char *tmp___7;
  char *tmp___8;

   
                                       
                                                  
                                                                
                                              
                                                  
                                                     
                      
                                                                            
                                                  
                                                                    
                          
                      
                                                                 
                    
                                               
                                                          
                                                                     
                                               
       
     
                                           
                      
                                                                               
                                           
           
   
}
__inline static void emit_try_help(void) {
  char *tmp;

   
                                                               
                                                     
           
   
}
extern
    __attribute__((__nothrow__)) int(__attribute__((__leaf__))
                                     getrlimit)(__rlimit_resource_t __resource,
                                                struct rlimit *__rlimits);
static int decimal_point;
static int thousands_sep;
static _Bool hard_LC_COLLATE;
static _Bool hard_LC_TIME;
static char eolchar = (char)'\n';
static _Bool blanks[256];
static _Bool nonprinting[256];
static _Bool nondictionary[256];
static char fold_toupper[256];
static struct month monthtab[12] = {
    {"APR", 4}, {"AUG", 8}, {"DEC", 12}, {"FEB", 2},  {"JAN", 1},  {"JUL", 7},
    {"JUN", 6}, {"MAR", 3}, {"MAY", 5},  {"NOV", 11}, {"OCT", 10}, {"SEP", 9}};
static size_t merge_buffer_size = (size_t)262144;
static size_t sort_size;
static char const **temp_dirs;
static size_t temp_dir_count;
static size_t temp_dir_alloc;
static _Bool reverse;
static _Bool stable;
static int tab = 128;
static _Bool unique;
static _Bool have_read_stdin;
static struct keyfield *keylist;
static char const *compress_program;
static _Bool debug;
static unsigned int nmerge = 16U;
static __attribute__((__noreturn__)) void die(char const *message,
                                              char const *file);
static __attribute__((__noreturn__)) void die(char const *message,
                                              char const *file);
static void die(char const *message, char const *file) {
  char *tmp;
  char const *tmp___0;
  int *tmp___1;

   
               
                     
            
                                       
                                  
     
                                 
                                                   
            
   
}
__attribute__((__noreturn__)) void usage(int status);
void usage(int status) {
  char *tmp;
  char *tmp___0;
  char *tmp___1;
  char *tmp___2;
  char *tmp___3;
  char *tmp___4;
  char *tmp___5;
  char *tmp___6;
  char *tmp___7;
  char *tmp___8;
  char *tmp___9;
  char *tmp___10;
  char *tmp___11;
  char *tmp___12;
  char *tmp___13;
  char *tmp___14;
  char *tmp___15;
  char *tmp___16;
  char *tmp___17;
  char *tmp___18;
  char *tmp___19;

   
                      
                      
            
                                                                             
                                         
                                                            
                        
                                                                               
                                                    
                                                                                
                                                
                                                    
                                                 
                                                    
               
                                                                                
                                                                     
                                                                                
                                                           
                                                    
               
                                                                               
                                                                            
                                                                             
                                                                   
                                                    
                                                                               
                                                   
                                                    
                        
                                                                                
                                                                                
                                                                          
                                                                         
                                                    
               
                                                                                
                                                                   
                                                                                
                                                                                
                                                                             
                                                    
                                              
                                                    
                        
                                                                                
                                                                 
                                                    
                
                                                                            
                                                                             
                                                                     
                                                                                
                                                                                
                                                     
                
                                                                              
                                                                                
                                                                              
                                                                                
                                                                                
                                                                              
                                                     
                                                                              
                                                                                
                                                                      
                                                     
                
                                                                                
                                                                                
                                                               
                                                                             
                                                     
                
                                                                              
                                                                              
                                                                                
                                                                                
                                                                             
                                                                            
                                                                              
                                                                         
                                             
                         
                                                                              
                                                     
                                                                          
                                                     
                
                                                                            
                                                     
                         
                                                                               
                                                                                
                                                                              
                                                                                
                                                                              
                                                                             
                                                                                
                                                                            
                                                  
                                                     
                         
                                                                                
                                                                    
                                                                               
                                                                                
                                              
                                                     
                            
     
                 
   
}
static char const short_options[31] = {
    (char const)'-', (char const)'b', (char const)'c',   (char const)'C',
    (char const)'d', (char const)'f', (char const)'g',   (char const)'h',
    (char const)'i', (char const)'k', (char const)':',   (char const)'m',
    (char const)'M', (char const)'n', (char const)'o',   (char const)':',
    (char const)'r', (char const)'R', (char const)'s',   (char const)'S',
    (char const)':', (char const)'t', (char const)':',   (char const)'T',
    (char const)':', (char const)'u', (char const)'V',   (char const)'y',
    (char const)':', (char const)'z', (char const)'\000'};
static struct option const long_options___1[31] = {
    {"ignore-leading-blanks", 0, (int *)((void *)0), 'b'},
    {"check", 2, (int *)((void *)0), 128},
    {"compress-program", 1, (int *)((void *)0), 129},
    {"debug", 0, (int *)((void *)0), 130},
    {"dictionary-order", 0, (int *)((void *)0), 'd'},
    {"ignore-case", 0, (int *)((void *)0), 'f'},
    {"files0-from", 1, (int *)((void *)0), 131},
    {"general-numeric-sort", 0, (int *)((void *)0), 'g'},
    {"ignore-nonprinting", 0, (int *)((void *)0), 'i'},
    {"key", 1, (int *)((void *)0), 'k'},
    {"merge", 0, (int *)((void *)0), 'm'},
    {"month-sort", 0, (int *)((void *)0), 'M'},
    {"numeric-sort", 0, (int *)((void *)0), 'n'},
    {"human-numeric-sort", 0, (int *)((void *)0), 'h'},
    {"version-sort", 0, (int *)((void *)0), 'V'},
    {"random-sort", 0, (int *)((void *)0), 'R'},
    {"random-source", 1, (int *)((void *)0), 133},
    {"sort", 1, (int *)((void *)0), 134},
    {"output", 1, (int *)((void *)0), 'o'},
    {"reverse", 0, (int *)((void *)0), 'r'},
    {"stable", 0, (int *)((void *)0), 's'},
    {"batch-size", 1, (int *)((void *)0), 132},
    {"buffer-size", 1, (int *)((void *)0), 'S'},
    {"field-separator", 1, (int *)((void *)0), 't'},
    {"temporary-directory", 1, (int *)((void *)0), 'T'},
    {"unique", 0, (int *)((void *)0), 'u'},
    {"zero-terminated", 0, (int *)((void *)0), 'z'},
    {"parallel", 1, (int *)((void *)0), 135},
    {"help", 0, (int *)((void *)0), -130},
    {"version", 0, (int *)((void *)0), -131},
    {(char const *)((void *)0), 0, (int *)((void *)0), 0}};
static char const *const check_args[4] = {"quiet", "silent", "diagnose-first",
                                          (char const *)((void *)0)};
static char const check_types[3] = {(char const)'C', (char const)'C',
                                    (char const)'c'};
static char const *const sort_args[7] = {
    "general-numeric", "human-numeric",          "month", "numeric", "random",
    "version",         (char const *)((void *)0)};
static char const sort_types[6] = {(char const)'g', (char const)'h',
                                   (char const)'M', (char const)'n',
                                   (char const)'R', (char const)'V'};
static sigset_t caught_signals;
static struct cs_status cs_enter(void) {
  struct cs_status status;
  int tmp;

  {
    tmp = sigprocmask(0, (sigset_t const *)(&caught_signals), &status.sigs);
                                     
    return (status);
  }
}
static void cs_leave(struct cs_status status) {

   

                       
                                                                                
     
           
   

}
static struct tempnode *volatile temphead;
static struct tempnode *volatile *temptail = &temphead;
static Hash_table *proctab;
static size_t proctab_hasher(void const *entry, size_t tabsize) {
  struct tempnode const *node;

  {
    node = (struct tempnode const *)entry;
    return ((unsigned long)node->pid % tabsize);
  }
}
static _Bool proctab_comparator(void const *e1, void const *e2) {
  struct tempnode const *n1;
  struct tempnode const *n2;

  {
    n1 = (struct tempnode const *)e1;
    n2 = (struct tempnode const *)e2;
    return ((_Bool)(n1->pid == n2->pid));
  }
}
static pid_t nprocs;
static _Bool delete_proc(pid_t pid);
static pid_t reap(pid_t pid) {
  int status;
  pid_t cpid;
  int tmp;
  pid_t tmp___0;
  pid_t tmp___1;
  char *tmp___2;
  int *tmp___3;
  char *tmp___4;
  union __anonunion_101 __constr_expr_82;
  union __anonunion_102 __constr_expr_83;
  _Bool tmp___5;

  {
             {
      tmp = 0;
    }       
              
     
             {
      tmp___0 = pid;
    }       
                   
     
    tmp___1 = waitpid(tmp___0, &status, tmp);
    cpid = tmp___1;
                   
                                               
                                   
                                                                  
            
                     
                      
                  
                
                                      
                        
             
                                           
                                                    
                                             
                                                        
                                                                   
                                                                     
               
                    
                                                                 
                                                                   
             
                     
           
         
       
     
    return (cpid);
  }
}
static void register_proc(struct tempnode *temp) {
  void *tmp;

   
                   
                                                                             
                                                                     
                                                               
                     
                     
       
     
                          
                                                   
               
                   
     
           
   
}
static _Bool delete_proc(pid_t pid) {
  struct tempnode test;
  struct tempnode *node;
  struct tempnode *tmp;

  {
                   
                                                                         
               
                
                        
     
                          
    return ((_Bool)1);
  }
}
static void wait_proc(pid_t pid) {
  _Bool tmp;

   
                           
              
                
     
           
   
}
static void reap_exited(void) {
  pid_t tmp;

   
               
                    
                       
                      
                   
                           
         
              
                         
       
                          
     
               
           
   
}
static void reap_some(void) {

   

             
                  
           
   

}
static void reap_all(void) {

   

               

                          
                         
       
               
     
               
           
   

}
static void cleanup(void) {
  struct tempnode const *node;

   
                                             
               

                  
                         
       
                                         
                                                 
     
              
                                              
           
   
}
static void exit_cleanup(void) {
  struct cs_status cs;
  struct cs_status tmp;

   
                   
                       
               
                
                   
     
                   
           
   
}
static struct tempnode *create_temp_file(int *pfd, _Bool survive_fd_exhaustion);
static char const slashbase[12] = {
    (char const)'/', (char const)'s', (char const)'o', (char const)'r',
    (char const)'t', (char const)'X', (char const)'X', (char const)'X',
    (char const)'X', (char const)'X', (char const)'X', (char const)'\000'};
static size_t temp_dir_index;
static struct tempnode *create_temp_file(int *pfd,
                                         _Bool survive_fd_exhaustion) {
  int fd;
  int saved_errno;
  char const *temp_dir;
  size_t len;
  size_t tmp;
  struct tempnode *node;
  struct tempnode *tmp___0;
  char *file;
  struct cs_status cs;
  int *tmp___1;
  int *tmp___2;
  char const *tmp___3;
  char *tmp___4;
  int *tmp___5;
  int *tmp___6;

  {
    temp_dir = *(temp_dirs + temp_dir_index);
    tmp = strlen(temp_dir);
    len = tmp;
    tmp___0 = (struct tempnode *)xmalloc(
        ((unsigned long)(&((struct tempnode *)0)->name) + len) +
        sizeof(slashbase));
    node = tmp___0;
                      
                                                      
                                                                               
                                                
                     
                                           
                                 
     
                    
                             
                  
                       
                             
     
                                 
                           
                 
                                 
                           
                 
                                  
                                     
                                
                                    
                                                                  
                                       
                                                             
         
              
                                  
                                                                
                                     
                                                           
       
                         
                                            
     
              
    return (node);
  }
}
static FILE *stream_open(char const *file, char const *how) {
  FILE *fp;
  int tmp;
  FILE *tmp___0;

  {
    if (!file) {
      return (stdout);
    }
                                  {
      tmp = strcmp(file, "-");
                     
                                   
                   
             {
        fp = fopen_safer(file, how);
      }
                                
      return (fp);
    }
                                     
                     
  }
}
static FILE *xfopen(char const *file, char const *how) {
  FILE *fp;
  FILE *tmp;
  char *tmp___0;

  {
    tmp = stream_open(file, how);
    fp = tmp;
              
                                       
                                       
     
    return (fp);
  }
}
static void xfclose(FILE *fp, char const *file) {
  int tmp;
  int tmp___0;
  char *tmp___1;
  int tmp___2;
  char *tmp___3;
  int tmp___4;

   
                     
                   
                  
     
                   
                  
     
                        
         
                                
                  
                            
     
                      
         
                                  
                       
                                         
                                       
     
                      
                 
                             
                       
                                        
                                       
     
                      
                
           
   
}
static void dup2_or_die(int oldfd, int newfd) {
  char *tmp;
  int *tmp___0;
  int tmp___1;

   
                                 
                      
                                   
                                   
                                            
     
           
   
}
static pid_t pipe_fork(int *pipefds, size_t tries) {
  struct tempnode *saved_temphead;
  int saved_errno;
  double wait_retry;
  pid_t pid;
  struct cs_status cs;
  int tmp;
  int *tmp___0;
  int *tmp___1;
  int *tmp___2;
  size_t tmp___3;
  int *tmp___4;
  int *tmp___5;

  {
                      
                        
                  
                  
     
                                             
                  
     
              {
                      
              
                     
                         
       
                      
                                                   
                                                
      pid = fork();
                                   
                             
                
                                  
       
                   
                                   
                             
                     
                         
              
                                     
                             
                           
                
                                 
                                  
                        
         
       
    }
  while_break:;
                  
                                   
                             
                            
                            
                                   
                             
            
                     
                 
                 
              
                 
       
     
    return (pid);
  }
}
static struct tempnode *maybe_create_temp(FILE **pfp,
                                          _Bool survive_fd_exhaustion) {
  int tempfd;
  struct tempnode *node;
  struct tempnode *tmp;
  int pipefds[2];
  char *tmp___0;
  int *tmp___1;
  int tmp___2;
  char *tmp___3;

  {
                                                           
               
                
                                              
     
                          
                          {
      node->pid = pipe_fork(pipefds, (size_t)4);
                          
                      
                          
                            
                            
              
                             
                            
                                 
                        
                                     
                            
                   
                                                                              
                            
                                                      
                                         
                                                                        
           
         
       
    }
                               
                
                                                           
                                                             
     
    return (node);
  }
}
static struct tempnode *create_temp(FILE **pfp) {
  struct tempnode *tmp;

  {
    tmp = maybe_create_temp(pfp, (_Bool)0);
    return (tmp);
  }
}
static FILE *open_temp(struct tempnode *temp) {
  int tempfd;
  int pipefds[2];
  FILE *fp;
  pid_t child;
  pid_t tmp;
  char *tmp___0;
  int *tmp___1;
  int *tmp___2;
  int *tmp___3;
  char *tmp___4;
  int *tmp___5;
  int saved_errno;
  int *tmp___6;
  int *tmp___7;

  {
                             
                                
                           
     
    tempfd = open((char const *)(temp->name), 0);
                    {
      return ((FILE *)((void *)0));
    }
                                        
                
                      
                      
     
                     
                  
     
                        
             
                                 
                         
                                                              
                                   
                                                                  
     
                  
                                 
                  
                      
         
                      
                           
                  
                               
                      
                                                                          
                                                 
                                 
                                                                
                 
                      
                        
                  
                      
                                 
              
                                   
                             
                        
                                   
                             
     
                      
                
                
  }
}
static void add_temp_dir(char const *dir) {
  size_t tmp;

   
                                           
                                                                               
                                                                
     
                         
                     
                             
           
   
}
static void zaptemp(char const *name) {
  struct tempnode *volatile *pnode;
  struct tempnode *node;
  struct tempnode *next;
  int unlink_status;
  int unlink_errno;
  struct cs_status cs;
  int *tmp;
  char *tmp___0;

   
                     
                      
               
                                       
                                                                  
                         
       
                  
           
                          
     
               
                                
                           
     
                                         
                    
                                 
                             
                        
                  
                 
                             
                                                      
                                                          
     
                
                       
     
                       
           
   
}
static int struct_month_cmp(void const *m1, void const *m2) {
  struct month const *month1;
  struct month const *month2;
  int tmp;

  {
    month1 = (struct month const *)m1;
    month2 = (struct month const *)m2;
    tmp = strcmp((char const *)month1->name, (char const *)month2->name);
    return (tmp);
  }
}
static void inittables(void) {
  size_t i;
  unsigned short const **tmp;
  int tmp___0;
  unsigned short const **tmp___1;
  int tmp___2;
  unsigned short const **tmp___3;
  unsigned short const **tmp___4;
  int tmp___5;
  char const *s;
  size_t s_len;
  size_t j;
  size_t k;
  char *name;
  size_t tmp___6;
  unsigned char tmp___7;
  unsigned short const **tmp___8;
  unsigned char tmp___9;

  {
    i = (size_t)0;
    while (1) {

      if (!(i < 256UL)) {
        goto while_break;
      }
                            
                                              
                    
              
                    
       
                                 
                                
                                                      
                    
              
                    
       
                                      
                                
                                                  
                    
              
                                  
                                                    
                      
                
                      
         
       
                                        
      fold_toupper[i] = (char)toupper((int)i);
      i++;
    }
  while_break:;
                       
                    
                 

                          
                               
         
                                                               
                          
                                            
                                              
                                         
                      
              
                   

                             
                                 
           
                                    
                                             
                                                               
                        
                
                                               
                                                      
           
              
         
                      
                                   
            
       
                    
                                                                
                               
     
           
  }
}
static void specify_nmerge(int oi, char c, char const *s) {
  uintmax_t n;
  struct rlimit rlimit;
  enum strtol_error e;
  enum strtol_error tmp;
  unsigned int max_nmerge;
  rlim_t tmp___1;
  int tmp___2;
  char const *tmp___3;
  char *tmp___4;
  char const *tmp___5;
  char *tmp___6;
  char max_nmerge_buf[((sizeof(max_nmerge) * 8UL) * 146UL + 484UL) / 485UL +
                      1UL];
  char const *tmp___7;
  char *tmp___8;
  char *tmp___9;
  char *tmp___10;

   
         
                                                                               
            
                                                         
                       
                                
            
                           
     
                                               
                                
                               
                                   
                                 
              
                          
                             
                                                        
                                                                       
                         
                               
                                                           
                                                                       
                         
                
                                    
                                     
                  
                   
           
         
       
     
                                
                         
                                                      
                                                                             
                                                      
                                                                            
                                                                              
            
                                                   
     
           
   
}
static void specify_sort_size(int oi, char c, char const *s) {
  uintmax_t n;
  char *suffix;
  enum strtol_error e;
  enum strtol_error tmp;
  double mem;
  double tmp___0;

   
                                                         
            
                                
                                                     
                                       
                      
                
                                   
         
       
     
                                
                                                     
                             
                                         
                         
           
                                         
                         
           
                            
                
                                   
                            
                
                                    
                                                    
                                                   
                               
                                     
                  
                                     
           
                            
                      
         
       
     
                                
                          
               
       
                    
                           
                                                                              
                                
                
                                                                          
         
               
       
                               
     
                                                 
   
}
static size_t specify_nthreads(int oi, char c, char const *s) {
  unsigned long nthreads;
  enum strtol_error e;
  enum strtol_error tmp;
  char *tmp___0;

  {
                                                               
            
                                
                                    
     
                                
                                                   
     
                                         {
      nthreads = 0xffffffffffffffffUL;
    }
                          
                                                              
                                         
     
    return (nthreads);
  }
}
static size_t default_sort_size(void) {
  double avail;
  double tmp;
  double total;
  double tmp___0;
  double mem;
  double tmp___1;
  struct rlimit rlimit;
  size_t size;
  int tmp___2;
  int tmp___3;
  int tmp___4;
  size_t tmp___5;

  {
                              
                
                              
                    
                                    
                      
            
                                  
     
                  
    size = 0xffffffffffffffffUL;
                                                         
                       
                                   
                               
       
     
                                                         
                       
                                   
                               
       
     
                
                                                         
                       
                                                   
                                               
       
     
                             
                         
     
                                                                    {
      tmp___5 = size;
    }       
                                                                    
     
    return (tmp___5);
  }
}
static size_t size_bound;
static size_t sort_buffer_size(FILE *const *fps, size_t nfps,
                               char *const *files, size_t nfiles,
                               size_t line_bytes) {
  size_t worst_case_per_input_byte;
  size_t size;
  size_t i;
  struct stat st;
  off_t file_size;
  size_t worst_case;
  char *tmp;
  int tmp___0;
  int tmp___1;
  int tmp___3;
  int tmp___4;
  int tmp___5;
  int tmp___6;
  int tmp___7;

  {
    worst_case_per_input_byte = line_bytes + 1UL;
                                           
                  
              {

                         {
        goto while_break;
      }
                    {
        tmp___0 = fileno((FILE *)*(fps + i));
        tmp___1 = fstat(tmp___0, &st);
                          
      }       
                                                          
                           
                                  
                            
                
                                                          
                            
         
                          
       
                         
                                     
                                                           
       
                                           {
                               
      }       
                        
                             
         
                                  
       
                        
                               
                          
                                           
         
       
                                                                       
                                                                        
                            
              
                                              
                              
         
       
                         
      i++;
    }
  while_break:;
                  
  }
}
static void initbuf(struct buffer *buf___1, size_t line_bytes, size_t alloc) {
  size_t tmp;
  size_t tmp___0;

  {
    while (1) {
                                                                 
      buf___1->buf = (char *)malloc(alloc);
      if (buf___1->buf) {
        goto while_break;
      }
      alloc /= 2UL;
                                      
                     
       
    }
  while_break:
    buf___1->line_bytes = line_bytes;
    buf___1->alloc = alloc;
    tmp___0 = (size_t)0;
    buf___1->nlines = tmp___0;
    tmp = tmp___0;
    buf___1->left = tmp;
                        
                            
           
  }
}
__inline static struct line *buffer_linelim(struct buffer const *buf___1) {

   

    return ((struct line *)(buf___1->buf + buf___1->alloc));
   

}
static char *begfield(struct line const *line, struct keyfield const *key) {
  char *ptr;
  char *lim;
  size_t sword;
  size_t schar;
  size_t tmp;
  unsigned char tmp___0;
  unsigned char tmp___1;
  size_t tmp___2;
  unsigned char tmp___3;

  {
    ptr = (char *)line->text;
    lim = (ptr + line->length) - 1;
    sword = (size_t)key->sword;
                               
                    {
                {

                                                      
                      
                  
                     
                             
           
                
                           
         
        while (1) {

                                                       {
            if (!((int)*ptr != tab)) {
              goto while_break___0;
            }
          }       
                                 
           
          ptr++;
        }
      while_break___0:;
                                                      
                
         
      }
    while_break:;
    }       
                 

                                                      
                          
                  
                         
                                 
           
                
                               
         
                   

                                                        
                                     
                                   
                                   
             
                  
                                 
           
                
         
                       
                   

                                                        
                                     
                                  
                                   
             
                  
                                 
           
                
         
                       
       
                     
     
                           
                 

                                                      
                                   
                                 
                                 
           
                
                               
         
              
       
                     
     
                                                            
                
            
                   
     
    return (ptr);
  }
}
static char *limfield(struct line const *line, struct keyfield const *key) {
  char *ptr;
  char *lim;
  size_t eword;
  size_t echar;
  size_t tmp;
  unsigned char tmp___0;
  unsigned char tmp___1;
  size_t tmp___2;
  unsigned char tmp___3;

  {
                             
    lim = (ptr + line->length) - 1;
                               
    echar = (size_t)key->echar;
                       
              
     
                     
                 

                                                      
                      
                  
                     
                             
           
                
                           
         
                   

                                                        
                                      
                                   
             
                  
                                 
           
                
         
                       
                                                      
                      
                  
                  
                        
                    
             
           
         
       
                 
            
                 

                                                      
                          
                  
                         
                                 
           
                
                               
         
                   

                                                        
                                     
                                   
                                   
             
                  
                                 
           
                
         
                       
                   

                                                        
                                     
                                  
                                   
             
                  
                                 
           
                
         
                       
       
                     
     
                      {
                             
                   

                                                        
                                     
                                   
                                   
             
                  
                                 
           
                
         
                       
       
                                                             {
        ptr = lim;
      }       
                     
       
    }
    return (ptr);
  }
}
static _Bool fillbuf___7(struct buffer *buf___1, FILE *fp, char const *file) {
  struct keyfield const *key;
  char eol;
  size_t line_bytes;
  size_t mergesize;
  char *ptr;
  struct line *linelim;
  struct line *tmp;
  struct line *line;
  size_t avail;
  char *line_start;
  char *tmp___0;
  size_t readsize;
  size_t bytes_read;
  size_t tmp___1;
  char *ptrlim;
  char *p;
  char *tmp___2;
  int tmp___3;
  char *tmp___4;
  int tmp___5;
  char *tmp___6;
  unsigned char tmp___7;
  struct line *tmp___8;
  size_t line_alloc;

  {
    key = (struct keyfield const *)keylist;
    eol = eolchar;
    line_bytes = buf___1->line_bytes;
                                                                
                       
                        
     
    if (buf___1->used != buf___1->left) {
                                   
                                                                             
                             
      buf___1->used = buf___1->left;
                                  
    }
              {
      ptr = buf___1->buf + buf___1->used;
      tmp = buffer_linelim((struct buffer const *)buf___1);
      linelim = tmp;
      line = linelim - buf___1->nlines;
      avail = (size_t)(((char *)linelim - buf___1->nlines * line_bytes) - ptr);
      if (buf___1->nlines) {
        tmp___0 = line->text + line->length;
      } else {
        tmp___0 = buf___1->buf;
      }
      line_start = tmp___0;
                {

                                          
                               
         
        readsize = (avail - 1UL) / (line_bytes + 1UL);
        tmp___1 = fread_unlocked((void *)ptr, (size_t)1, readsize, fp);
        bytes_read = tmp___1;
        ptrlim = ptr + bytes_read;
                            
                                    {
                                        
                        
                                             
                                             
           
          tmp___5 = feof_unlocked(fp);
                       {
            buf___1->eof = (_Bool)1;
                                                                       
                                
             
                                                                    {
              if ((int)*(ptrlim + -1) != (int)eol) {
                tmp___4 = ptrlim;
                ptrlim++;
                *tmp___4 = eol;
              }
            }
          }
        }
        while (1) {
          p = (char *)memchr((void const *)ptr, (int)eol,
                             (size_t)(ptrlim - ptr));
          if (!p) {
            goto while_break___1;
          }
                            
          ptr = p + 1;
          line--;
          line->text = line_start;
          line->length = (size_t)(ptr - line_start);
                                         
                                  
                  
                                     
           
                              
          if (key) {
                                                    {
              line->keylim = p;
            }       
                                                                 
                                     
             
            if (key->sword != 0xffffffffffffffffUL) {
              line->keybeg = begfield((struct line const *)line, key);
            } else {
                                     
                           
                                                  
                                         
                                         
                   
                               
                 
                               
               
              line->keybeg = line_start;
            }
          }
          line_start = ptr;
        }
      while_break___1:
        ptr = ptrlim;
                           
                               
         
      }
    while_break___0:
      buf___1->used = (size_t)(ptr - buf___1->buf);
      tmp___8 = buffer_linelim((struct buffer const *)buf___1);
      buf___1->nlines = (size_t)(tmp___8 - line);
                                  {
        buf___1->left = (size_t)(ptr - line_start);
                                                                    
        return ((_Bool)1);
      }
                                                        
                                                                          
                                                             
                                                        
    }

                      
  }
}
static char const unit_order[256] = {
    (char const)0, (char const)0, (char const)0, (char const)0, (char const)0,
    (char const)0, (char const)0, (char const)0, (char const)0, (char const)0,
    (char const)0, (char const)0, (char const)0, (char const)0, (char const)0,
    (char const)0, (char const)0, (char const)0, (char const)0, (char const)0,
    (char const)0, (char const)0, (char const)0, (char const)0, (char const)0,
    (char const)0, (char const)0, (char const)0, (char const)0, (char const)0,
    (char const)0, (char const)0, (char const)0, (char const)0, (char const)0,
    (char const)0, (char const)0, (char const)0, (char const)0, (char const)0,
    (char const)0, (char const)0, (char const)0, (char const)0, (char const)0,
    (char const)0, (char const)0, (char const)0, (char const)0, (char const)0,
    (char const)0, (char const)0, (char const)0, (char const)0, (char const)0,
    (char const)0, (char const)0, (char const)0, (char const)0, (char const)0,
    (char const)0, (char const)0, (char const)0, (char const)0, (char const)0,
    (char const)0, (char const)0, (char const)0, (char const)0, (char const)6,
    (char const)0, (char const)3, (char const)0, (char const)0, (char const)0,
    (char const)1, (char const)0, (char const)2, (char const)0, (char const)0,
    (char const)5, (char const)0, (char const)0, (char const)0, (char const)4,
    (char const)0, (char const)0, (char const)0, (char const)0, (char const)8,
    (char const)7, (char const)0, (char const)0, (char const)0, (char const)0,
    (char const)0, (char const)0, (char const)0, (char const)0, (char const)0,
    (char const)0, (char const)0, (char const)0, (char const)0, (char const)0,
    (char const)0, (char const)0, (char const)1, (char const)0, (char const)0,
    (char const)0, (char const)0, (char const)0, (char const)0, (char const)0,
    (char const)0, (char const)0, (char const)0, (char const)0, (char const)0,
    (char const)0, (char const)0, (char const)0, (char const)0, (char const)0,
    (char const)0, (char const)0, (char const)0, (char const)0, (char const)0,
    (char const)0, (char const)0, (char const)0, (char const)0, (char const)0,
    (char const)0, (char const)0, (char const)0, (char const)0, (char const)0,
    (char const)0, (char const)0, (char const)0, (char const)0, (char const)0,
    (char const)0, (char const)0, (char const)0, (char const)0, (char const)0,
    (char const)0, (char const)0, (char const)0, (char const)0, (char const)0,
    (char const)0, (char const)0, (char const)0, (char const)0, (char const)0,
    (char const)0, (char const)0, (char const)0, (char const)0, (char const)0,
    (char const)0, (char const)0, (char const)0, (char const)0, (char const)0,
    (char const)0, (char const)0, (char const)0, (char const)0, (char const)0,
    (char const)0, (char const)0, (char const)0, (char const)0, (char const)0,
    (char const)0, (char const)0, (char const)0, (char const)0, (char const)0,
    (char const)0, (char const)0, (char const)0, (char const)0, (char const)0,
    (char const)0, (char const)0, (char const)0, (char const)0, (char const)0,
    (char const)0, (char const)0, (char const)0, (char const)0, (char const)0,
    (char const)0, (char const)0, (char const)0, (char const)0, (char const)0,
    (char const)0, (char const)0, (char const)0, (char const)0, (char const)0,
    (char const)0, (char const)0, (char const)0, (char const)0, (char const)0,
    (char const)0, (char const)0, (char const)0, (char const)0, (char const)0,
    (char const)0, (char const)0, (char const)0, (char const)0, (char const)0,
    (char const)0, (char const)0, (char const)0, (char const)0, (char const)0,
    (char const)0, (char const)0, (char const)0, (char const)0, (char const)0,
    (char const)0, (char const)0, (char const)0, (char const)0, (char const)0,
    (char const)0, (char const)0, (char const)0, (char const)0, (char const)0,
    (char const)0, (char const)0, (char const)0, (char const)0, (char const)0,
    (char const)0, (char const)0, (char const)0, (char const)0, (char const)0,
    (char const)0};
static int __attribute__((__pure__)) find_unit_order(char const *number) {
  _Bool minus_sign;
  char const *p;
  int nonzero;
  unsigned char ch;
  char const *tmp;
  char const *tmp___0;
  int order___0;
  int tmp___1;

  {
                                                     
                                 
                
               

                 
                
            
                                 
                                              
                               
         
                                
       
                     
                                        
                         
       
     
               
                                  {
                {
        tmp___0 = p;
        p++;
        ch = (unsigned char)*tmp___0;
                                              
                               
         
                                
      }
    while_break___1:;
    }
                 {
      order___0 = (int)unit_order[ch];
                      {
        tmp___1 = -order___0;
      }       
                            
       
      return ((int __attribute__((__pure__)))tmp___1);
    }       
                                                
     
  }
}
static int human_numcompare(char const *a, char const *b) {
  unsigned char tmp;
  unsigned char tmp___0;
  int diff;
  int __attribute__((__pure__)) tmp___1;
  int __attribute__((__pure__)) tmp___2;
  int tmp___3;
  int tmp___4;

  {
               
                               
                         
                         
       
          
     
               
               
                                   
                             
                             
       
          
     
  while_break___0:
    tmp___1 = find_unit_order(a);
    tmp___2 = find_unit_order(b);
    diff = (int)(tmp___1 - tmp___2);
              {
      tmp___4 = diff;
    }       
                                                                   
                        
     
    return (tmp___4);
  }
}
static int numcompare___3(char const *a, char const *b) {
  unsigned char tmp;
  unsigned char tmp___0;
  int tmp___1;

  {
               
                               
                         
                         
       
          
     
               
               
                                   
                             
                             
       
          
     
  while_break___0:
    tmp___1 = (int)strnumcmp(a, b, decimal_point, thousands_sep);
    return (tmp___1);
  }
}
static int nan_compare(char const *sa, char const *sb) {
  long double a;
  long double b;
  int tmp;

  {
                                       
                                          
                                       
                                          
    tmp = memcmp((void const *)(&a), (void const *)(&b), sizeof(a));
    return (tmp);
  }
}
static int general_numcompare(char const *sa, char const *sb) {
  char *ea;
  char *eb;
  long double a;
  long double tmp;
  long double b;
  long double tmp___0;
  int tmp___1;
  int tmp___2;
  int tmp___3;
  int tmp___4;
  int tmp___5;
  int tmp___6;
  int tmp___7;

  {
    tmp = strtold(sa, &ea);
    a = tmp;
    tmp___0 = strtold(sb, &eb);
    b = tmp___0;
                                                 
                                                   
                    
              
                     
       
                       
     
                                                 
                 
     
               {
      tmp___7 = -1;
    }       
                  
                    
              
                     
                      
                
                       
                         
                  
                         
                          
                    
                                            
                                
             
                              
           
                            
         
                          
       
                        
     
    return (tmp___7);
  }
}
static int getmonth(char const *month, char **ea) {
  size_t lo;
  size_t hi;
  unsigned char tmp;
  size_t ix;
  char const *m;
  char const *n;
  unsigned char tmp___0;
  unsigned char tmp___1;
  unsigned char tmp___2;
  unsigned char tmp___3;
  unsigned char tmp___4;
  unsigned char tmp___5;

  {
    lo = (size_t)0;
    hi = (size_t)12;
               
                                   
                         
                         
       
              
     
               
    while (1) {
      ix = (lo + hi) / 2UL;
      m = month;
      n = monthtab[ix].name;
      while (1) {

        if (!*n) {
                   
                            
           
          return (monthtab[ix].val);
        }
        tmp___3 = to_uchar((char)*m);
        tmp___4 = to_uchar(fold_toupper[tmp___3]);
        tmp___5 = to_uchar((char)*n);
        if ((int)tmp___4 < (int)tmp___5) {
          hi = ix;
          goto while_break___1;
        } else {
          tmp___0 = to_uchar((char)*m);
          tmp___1 = to_uchar(fold_toupper[tmp___0]);
          tmp___2 = to_uchar((char)*n);
          if ((int)tmp___1 > (int)tmp___2) {
            lo = ix + 1UL;
            goto while_break___1;
          }
        }
        m++;
        n++;
      }
    while_break___1:;
                       
                             
       
    }
  while_break___0:;
               
  }
}
static struct md5_ctx random_md5_state;
static void random_md5_state_init(char const *random_source) {
  unsigned char buf___1[16];
  struct randread_source *r;
  struct randread_source *tmp;
  char *tmp___0;
  char *tmp___1;
  int tmp___2;

   
                                                       
            
             
                                       
                                                
     
                                                    
                               
                       
                                        
                                                
     
                                    
                                                               
                                         
           
   
}
static size_t xstrxfrm(char *__restrict dest, char const *__restrict src,
                       size_t destsize) {
  int *tmp;
  size_t translated_size;
  size_t tmp___0;
  char *tmp___1;
  int *tmp___2;
  char *tmp___3;
  char *tmp___4;
  char *tmp___5;
  int *tmp___6;

  {
                             
             
    tmp___0 = strxfrm(dest, src, destsize);
    translated_size = tmp___0;
                                 
                   
                                                        
                                   
                                                
                                                                       
                                         
                                                                              
                                                           
                                                  
     
    return (translated_size);
  }
}
static int compare_random(char *__restrict texta, size_t lena,
                          char *__restrict textb, size_t lenb) {
  int xfrm_diff;
  char stackbuf[4000];
  char *buf___1;
  size_t bufsize___1;
  void *allocated;
  uint32_t dig[2][16UL / sizeof(uint32_t)];
  struct md5_ctx s[2];
  char const *lima;
  char const *limb;
  size_t guess_bufsize;
  size_t sizea;
  size_t tmp;
  size_t tmp___0;
  _Bool a_fits;
  size_t sizeb;
  size_t tmp___1;
  char *tmp___2;
  size_t tmp___3;
  size_t tmp___4;
  size_t tmp___5;
  size_t tmp___6;
  size_t tmp___7;
  int diff;
  int tmp___8;
  size_t tmp___9;

  {
                  
                       
                                   
                          
                            
                
                          
                                          
                                          
                 
                                                  
                                          
                                                          
                                        
                  
                                                    
           
                          
                                          
                                      
                         
                               
                                           
           
         
                                                         
                                                                    
                              
                
                              
         
                        
                                               
                                                         
                       
                                          
                  
                                
           
                       
                                      
                  
                                          
           
                                                                    
                                  
                
                              
         
                        
                     
                                                
                    
           
                
           
                                      
                                                    
                                                    
           
                          
                                           
                                      
                                                           
                                                         
           
                                                           
                                                                 
           
         
                                                         
                                                
                                 
         
                                                         
                                                
                                 
         
                                                            
                                                              
                         
                            
                         
                                    
                             
           
         
                                                               
                                                                         
                         
                              
                            
                  
                            
           
                                                   
                                                                       
                           
                                                          
           
         
       
                 
     
                                                        
                                            
                                                        
                                            
             
                                                                               
                   
               {
                      {
                          
                         
                
                         
         
                                                                              
                        {
          xfrm_diff = (lena > lenb) - (lena < lenb);
        }
      }
      diff = xfrm_diff;
    }
                    
    return (diff);
  }
}
static size_t debug_width(char const *text, char const *lim) {
  size_t width;
  size_t tmp;
  char const *tmp___0;

  {
                                                           
                
              {

                                                        
                         
       
      tmp___0 = text;
             
      width += (size_t)((int const) * tmp___0 == 9);
    }
  while_break:;
    return (width);
  }
}
static void mark_key(size_t offset, size_t width) {
  size_t tmp;
  char *tmp___0;

   
               
                   
               
                 
                         
       
                            
     
               
                 
                                                
                                    
            
                 
                              
                
                     
                               
         
       
                    
                             
     
           
   
}
__inline static _Bool key_numeric(struct keyfield const *key) {
  int tmp;

  {
    if (key->numeric) {
      tmp = 1;
    } else {
                                 
                
             {
                                 
                  
               {
          tmp = 0;
        }
      }
    }
    return ((_Bool)tmp);
  }
}
static void debug_key(struct line const *line, struct keyfield const *key) {
  char *text;
  char *beg;
  char *lim;
  char saved___0;
  unsigned char tmp;
  char *tighter_lim;
  long double __x;
  long double tmp___0;
  char *p;
  int tmp___1;
  _Bool found_digit;
  unsigned char ch;
  char *tmp___2;
  char *tmp___3;
  int tmp___4;
  _Bool tmp___5;
  size_t offset;
  size_t tmp___6;
  size_t width;
  size_t tmp___7;

   
                              
               
                                    
              
                                               
                                  
       
                                               
                                  
       
                             
                    
              
                         
                      
                
                                     
                        
                 
                             
                                
                       
                                   
                                 
                                 
               
                    
             
                      
                              
                                                          
                                
                    
                               
                                                          
                      
                                           
                                                                     
                                
                        
                                     
                            
                          
                                             
                       
                                                                    
                                              
                                      
                                
                                      
                         
                              
                                    
                       
                                        
                                             
                                 

                                   
                                      
                              
                                                       
                                                                
                                                 
                           
                                                 
                         
                                       
                                                          
                                               
                         
                       
                                     
                                                     
                                   
                                      
                              
                                                       
                                                                
                                                 
                           
                                                 
                         
                                       
                       
                                        
                                                 
                                               
                                        
                                  
                                        
                           
                                
                                      
                         
                                                  
                       
                            
                                        
                     
                   
                 
               
             
                             
                              
           
         
       
     
                                                                 
                     
                                                                
                    
                            
           
   
}
static void debug_line(struct line const *line) {
  struct keyfield const *key;

   
                                           
               
                           
                
                                                 
                   
                       
                             
                  
                         
                               
             
           
         
              
                         
       
     
               
           
   
}
static _Bool default_key_compare(struct keyfield const *key) {
  _Bool tmp;
  int tmp___0;

  {
                      
                  
           {
                           
                    
             {
                               
                      
               {
                                 
                        
                 {
            tmp = key_numeric(key);
            if (tmp) {
                          
            } else {
              if (key->month) {
                            
              } else {
                                   
                              
                       {
                                    
                                
                         {
                    tmp___0 = 1;
                  }
                }
              }
            }
          }
        }
      }
    }
    return ((_Bool)tmp___0);
  }
}
static void key_to_opts(struct keyfield const *key, char *opts___1) {
  char *tmp;
  char *tmp___0;
  char *tmp___1;
  char *tmp___2;
  char *tmp___3;
  char *tmp___4;
  char *tmp___5;
  char *tmp___6;
  char *tmp___7;
  char *tmp___8;
  char *tmp___9;

   
                           
                     
                 
                       
            
                             
                       
                   
                         
       
     
                                                                       
                         
                 
                           
     
                         
                         
                 
                           
     
                               
                         
                 
                           
     
                             
                         
                 
                           
     
                                                                     
                         
                 
                           
     
                     
                         
                 
                           
     
                       
                         
                 
                           
     
                      
                         
                 
                           
     
                       
                         
                 
                           
     
                       
                         
                 
                           
     
                             
           
   
}
static void key_warnings(struct keyfield const *gkey, _Bool gkey_only) {
  struct keyfield const *key;
  struct keyfield ugkey;
  unsigned long keynum;
  size_t sword;
  size_t eword;
  char tmp[((sizeof(uintmax_t) * 8UL) * 146UL + 484UL) / 485UL + 1UL];
  char
      obuf[(((sizeof(sword) * 8UL) * 146UL + 484UL) / 485UL + 1UL) * 2UL + 4UL];
  char
      nbuf[(((sizeof(sword) * 8UL) * 146UL + 484UL) / 485UL + 1UL) * 2UL + 5UL];
  char *po;
  char *pn;
  char *tmp___0;
  char *tmp___1;
  char *tmp___2;
  char *tmp___3;
  char *tmp___4;
  char *tmp___5;
  char *tmp___6;
  char *tmp___7;
  char const *tmp___8;
  char const *tmp___9;
  char *tmp___10;
  char *tmp___11;
  _Bool implicit_skip;
  _Bool tmp___12;
  int tmp___13;
  _Bool maybe_space_aligned;
  _Bool tmp___14;
  int tmp___15;
  _Bool line_offset;
  int tmp___16;
  char *tmp___17;
  size_t sword___0;
  size_t eword___0;
  char *tmp___18;
  _Bool tmp___19;
  _Bool ugkey_reverse;
  char opts___1[sizeof(short_options)];
  size_t tmp___20;
  unsigned long tmp___21;
  char *tmp___22;
  _Bool tmp___23;
  char *tmp___24;

   
                                     
                 
                                           
               

                 
                         
       
                               
                                   
                                   
                  
                  
                                            
                  
         
                                        
                                  
                                                    
                                              
                                    
                                                    
                                                 
                                                
                                     
                                                 
                                             
                                                                               
                                   
                                    
                                                 
         
                                                   
                                                   
                                                                           
                                                              
       
                                               
                                      
                                                                           
                                                      
         
       
                                  
                     
                     
              
                         
                       
                
                       
         
       
                                      
                             
                                            
                       
                           
                         
                  
                             
                           
                    
                           
             
           
                
                       
         
              
                     
       
                                            
                              
                                
                       
                
                       
         
              
                     
       
                                    
                       
                         
                             
                                    
                                  
                            
                      
                                          
                              
                        
                                                                             
                                                                            
                                                              
                 
               
                    
                   
                                      
                                 
                                                                             
                                                                            
                                                              
                        
                          
                 
                      
                 
                                        
                                   
                                                                               
                                                                              
                                                                
                   
                 
               
             
           
         
       
                       
                                    
                       
                                                 
                                                 
                           
                        
           
                           
                                                                               
                                                        
                  
                                        
                        
                                                                          
                                                          
             
           
         
       
                         
                                                                        
                                                    
         
       
                            
                                                                              
                                                      
         
       
                                                                              
                                                                              
                                                            
                                                                  
                             
                                                                      
                           
                                                                  
                                                               
                                                                  
                                                                  
                                               
               
     
              
                                                                      
                   
                          
                     
                      
                
                       
                 
                          
                   
                                            
                            
                              
                                           
                 
               
                                                                       
                                                          
                                                 
                                                              
                                                                           
                                                            
                                            
             
           
         
       
            
                  
     
                        
                    
                      
                        
                      
                                                                                
                                                
           
         
       
     
           
   
}
static int keycompare(struct line const *a, struct line const *b) {
  struct keyfield *key;
  char *texta;
  char *textb;
  char *lima;
  char *limb;
  int diff;
  char const *translate;
  _Bool const *ignore;
  size_t lena;
  size_t lenb;
  char *ta;
  char *tb;
  size_t tlena;
  size_t tlenb;
  char enda;
  char endb;
  void *allocated;
  char stackbuf[4000];
  size_t i;
  size_t size;
  size_t tmp;
  unsigned char tmp___0;
  unsigned char tmp___1;
  size_t tmp___2;
  unsigned char tmp___3;
  unsigned char tmp___4;
  int tmp___5;
  int tmp___6;
  unsigned char tmp___7;
  unsigned char tmp___8;
  unsigned char tmp___9;
  unsigned char tmp___10;
  unsigned char tmp___11;
  unsigned char tmp___12;
  unsigned char tmp___13;
  unsigned char tmp___14;
  unsigned char tmp___15;
  unsigned char tmp___16;
  char *tmp___17;
  unsigned char tmp___18;
  unsigned char tmp___19;
  char *tmp___20;
  unsigned char tmp___21;
  unsigned char tmp___22;
  size_t tmp___23;
  _Bool tmp___24;
  unsigned char tmp___25;
  unsigned char tmp___26;
  int tmp___27;

  {
    key = keylist;
    texta = (char *)a->keybeg;
    textb = (char *)b->keybeg;
    lima = (char *)a->keylim;
    limb = (char *)b->keylim;
              {
      translate = key->translate;
      ignore = key->ignore;
                                                       
                     
              
                    
       
                                                       
                     
              
                    
       
      lena = (size_t)(lima - texta);
      lenb = (size_t)(limb - textb);
                            
                    
             {
        tmp___24 = key_numeric((struct keyfield const *)key);
        if (tmp___24) {
          goto _L___2;
        } else {
          if (key->month) {
            goto _L___2;
          } else {
                              
                          
                   {
                                {
              _L___2:
                             
                              
                       {
                                 {
                  _L___1:
                    size = ((lena + 1UL) + lenb) + 1UL;
                                                  {
                      ta = stackbuf;
                                            
                    }       
                                                
                                             
                     
                    tb = (ta + lena) + 1;
                    i = (size_t)0;
                    tlena = i;
                    while (1) {

                      if (!(i < lena)) {
                        goto while_break___0;
                      }
                                   
                                                         
                                                        
                                  
                         
                             {
                      _L:
                        tmp = tlena;
                        tlena++;
                                        
                                                           
                                                                          
                               {
                          *(ta + tmp) = *(texta + i);
                        }
                      }
                      i++;
                    }
                  while_break___0:
                    *(ta + tlena) = (char)'\000';
                    i = (size_t)0;
                    tlenb = i;
                    while (1) {

                      if (!(i < lenb)) {
                        goto while_break___1;
                      }
                                   
                                                         
                                                        
                                      
                         
                             {
                      _L___0:
                        tmp___2 = tlenb;
                        tlenb++;
                                        
                                                           
                                                                              
                               {
                          *(tb + tmp___2) = *(textb + i);
                        }
                      }
                      i++;
                    }
                  while_break___1:
                    *(tb + tlenb) = (char)'\000';
                  }       
                               
                                 
                                         
                                                 
                               
                                 
                                         
                                                 
                   
                }
                if (key->numeric) {
                  diff = numcompare___3((char const *)ta, (char const *)tb);
                } else {
                                             
                          
                                                                               
                         {
                                             
                            
                                                                               
                           {
                                      {
                        tmp___5 =
                            getmonth((char const *)ta, (char **)((void *)0));
                        tmp___6 =
                            getmonth((char const *)tb, (char **)((void *)0));
                        diff = tmp___5 - tmp___6;
                      }       
                                          
                                                                      
                                
                                             
                                  
                                                                               
                                  
                                               
                                                     
                                    
                                                 
                                         
                                      
                                                                               
                                                                                
                               
                             
                           
                         
                       
                    }
                  }
                }
                             
                                  
                        
                                  
                                    
                          
                                         
                                         
                   
                 
              }       
                             
                                  
                               

                                 

                                   

                                                                           
                                                       
                                                            
                                                   
                             
                                  
                                                 
                           
                                  
                         
                                       
                                   

                                                                           
                                                       
                                                            
                                                   
                             
                                  
                                                 
                           
                                  
                         
                                       
                                                                         
                                                                              
                                                 
                           
                                
                                               
                         
                                                   
                                                                               
                                                    
                                                                                
                                                             
                                   
                                         
                         
                                
                                
                       
                                    
                                                                           
                                                                          
                                           
                     
                                   
                          
                               

                                 

                                   

                                                                           
                                                        
                                                             
                                                   
                             
                                  
                                                 
                           
                                  
                         
                                       
                                   

                                                                           
                                                        
                                                             
                                                   
                             
                                  
                                                 
                           
                                  
                         
                                       
                                                                         
                                                                              
                                                 
                           
                                
                                               
                         
                                                    
                                                    
                                                             
                                   
                                         
                         
                                
                                
                       
                                    
                                                                           
                                                                          
                                           
                     
                                   
                   
                        
                                    
                                          
                          
                                      
                                   
                            
                                      
                                   

                                                                           
                                                                                
                                                    
                             
                                  
                                                  
                           
                                           
                                  
                                                         
                                    
                                                                           
                                           
                                  
                                                         
                                    
                                                                           
                                                               
                                     
                                           
                           
                         
                                        
                              
                                          
                                          
                                
                                          
                         
                                                                               
                                                
                                   
                                         
                         
                       
                                        
                                  
                              
                                            
                       
                     
                   
                 
               
            }
          }
        }
      }
                {
        goto not_equal;
      }
                      
                 
                         
       
                                               
                                                         
                                                         
              
                                                   
                                                   
       
                                               
                                                          
                                                          
              
                                
                                
                               
                     

                                                             
                                          
                                      
                                      
               
                    
                                    
             
                    
           
                          
                     

                                                             
                                          
                                      
                                      
               
                    
                                    
             
                    
           
                          
         
       
    }
  while_break:;
               
  greater:
    diff = 1;
  not_equal:
                       
                       
           {
      tmp___27 = diff;
    }
    return (tmp___27);
  }
}
static int compare(struct line const *a, struct line const *b) {
  int diff;
  size_t alen;
  size_t blen;
  size_t tmp;
  int tmp___0;

  {
    if (keylist) {
      diff = keycompare(a, b);
                {
        return (diff);
      }       
                     
                        
                
                       
                          
           
         
       
    }
    alen = (size_t)(a->length - 1UL);
    blen = (size_t)(b->length - 1UL);
                      
                            
           {
                        
                 
             {
                              
                                                             
                                                              
               {
                           {
            tmp = alen;
          }       
                       
           
          diff = memcmp((void const *)a->text, (void const *)b->text, tmp);
                      
                              
                        
                    
                                  
             
           
        }
      }
    }
    if (reverse) {
      tmp___0 = -diff;
    } else {
      tmp___0 = diff;
    }
    return (tmp___0);
  }
}
static void write_line(struct line const *line, FILE *fp,
                       char const *output_file) {
  char *buf___1;
  size_t n_bytes;
  char *ebuf;
  char const *c;
  char wc;
  char const *tmp;
  char *tmp___0;
  int tmp___1;
  char *tmp___2;
  size_t tmp___3;

  {
    buf___1 = (char *)line->text;
    n_bytes = (size_t)line->length;
    ebuf = buf___1 + n_bytes;
                       
                  
                                  
                   

                                                          
                             
           
                  
              
                          
                             
                           
                  
                                                          
                              
             
           
                                                
                              
                                              
                                                    
           
         
                  
                         
              
                
       
           {
       
                             
      tmp___3 = fwrite_unlocked((void const *)buf___1, (size_t)1, n_bytes, fp);
                               
                                          
                                                
       
                                  
    }
           
  }
}
static _Bool check(char const *file_name___3, char checkonly) {
  FILE *fp;
  FILE *tmp;
  struct buffer buf___1;
  struct line temp;
  size_t alloc;
  uintmax_t line_number;
  struct keyfield const *key;
  _Bool nonunique;
  _Bool ordered;
  size_t tmp___0;
  struct line const *line;
  struct line const *tmp___1;
  struct line const *linebase;
  struct line const *disorder_line;
  uintmax_t disorder_line_number;
  struct line *tmp___2;
  char hr_buf[((sizeof(disorder_line_number) * 8UL) * 146UL + 484UL) / 485UL +
              1UL];
  char *tmp___3;
  char *tmp___4;
  char *tmp___5;
  int tmp___6;
  int tmp___7;
  _Bool tmp___8;

  {
    tmp = xfopen(file_name___3, "r");
    fp = tmp;
                      
    line_number = (uintmax_t)0;
                                           
    nonunique = (_Bool)(!unique);
                       
                                       {
      tmp___0 = merge_buffer_size;
    }       
                          
     
    initbuf(&buf___1, sizeof(struct line), tmp___0);
                                    
              {
      tmp___8 = fillbuf___7(&buf___1, fp, file_name___3);
                     
                         
       
      tmp___1 = (struct line const *)buffer_linelim(
          (struct buffer const *)(&buf___1));
      line = tmp___1;
      linebase = line - buf___1.nlines;
                 {
        tmp___6 = compare((struct line const *)(&temp), line - 1);
        if ((int)nonunique <= tmp___6) {
        found_disorder:
                                    {
            disorder_line = line - 1;
            tmp___2 = buffer_linelim((struct buffer const *)(&buf___1));
            disorder_line_number =
                (uintmax_t)(tmp___2 - (struct line *)disorder_line) +
                line_number;
            tmp___3 = umaxtostr(disorder_line_number, hr_buf);
            tmp___4 = gettext("%s: %s:%s: disorder: ");
            fprintf(stderr, (char const *)tmp___4, program_name, file_name___3,
                    tmp___3);
            tmp___5 = gettext("standard error");
            write_line(disorder_line, stderr, (char const *)tmp___5);
          }
          ordered = (_Bool)0;
          goto while_break;
        }
      }
      while (1) {
        line--;
                                                               
                               
         
        tmp___7 = compare(line, line - 1);
        if ((int)nonunique <= tmp___7) {
          goto found_disorder;
        }
      }
    while_break___0:
      line_number += buf___1.nlines;
                                         
                   
                       
                       
                                         
                                 
           
                                                
                                 
           
         
                      
                                
                                           
       
                                                                                
                                         
                
                                                              
                                                              
       
    }
  while_break:
    xfclose(fp, file_name___3);
                              
                            
    return (ordered);
  }
}
static size_t open_input_files(struct sortfile *files, size_t nfiles,
                               FILE ***pfps) {
  FILE **fps;
  FILE **tmp;
  int i;
  FILE *tmp___0;
  FILE *tmp___1;

  {
                                                  
                
              
          
              {

                                  
                         
       
                             {
                                                  {
          tmp___0 = open_temp((files + i)->temp);
          *(fps + i) = tmp___0;
        }       
                                                        
                               
         
      }       
                                                      
                             
       
                        
                         
       
          
    }
  while_break:;
    return ((size_t)i);
  }
}
static void mergefps(struct sortfile *files, size_t ntemps, size_t nfiles,
                     FILE *ofp, char const *output_file, FILE **fps) {
  struct buffer *buffer;
  struct buffer *tmp;
  struct line saved___0;
  struct line const *savedline;
  size_t savealloc;
  struct line const **cur;
  struct line const **tmp___0;
  struct line const **base;
  struct line const **tmp___1;
  size_t *ord;
  size_t *tmp___2;
  size_t i;
  size_t j;
  size_t t;
  struct keyfield const *key;
  size_t tmp___3;
  struct line const *linelim;
  struct line const *tmp___4;
  _Bool tmp___5;
  int tmp___6;
  struct line const *smallest;
  int tmp___7;
  struct line const *linelim___0;
  struct line const *tmp___8;
  _Bool tmp___9;
  size_t lo;
  size_t hi;
  size_t probe;
  size_t ord0;
  size_t count_of_smaller_lines;
  int cmp;
  int tmp___10;

   
                                                             
                 
                                                 
                          
                                                                   
                  
                                                                    
                   
                                                       
                  
                                           
                                         
                  
               

                          
                         
       
                                                   
                                    
              
                                     
       
                                                        
                                                                       
                    
                                                      
                                                 
                          
                                 
                                                     
            
              
                                               
                         
                   
                                     
         
                                        
                 
              
                   

                              
                                 
           
                                              
                                          
              
         
                       
       
     
              
                  
               

                          
                             
       
                     
          
     
                  
                  
               

                          
                             
       
                                                                          
                        
                               
                                        
                       
                      
       
          
     
                   
               
                        
                    
                             
       
                                     
                   
                        
                                                 
                        
                                                         
                                                                            
           
         
                         
                                                        
                                                     
                       

                               
                                                     
                                     
               
                               
                                                            
                                     
               
             
                          
                                         
                                                        
           
                                                      
                                                                      
                                   
                    
                              
                                                                     
                              
                                                                     
           
         
              
                                               
       
                                                                          
                                           
              
                                                                       
                                                          
                      
                                                        
                                                            
                                
                                                
                                                                             
                
                        
                     

                                
                                   
             
                                          
                             
             
                
           
                        
                   
                                                                   
                                    
                     
                                                
           
                                                   
                         
                     

                                
                                   
             
                                            
                                                
                                                  
                                            
                                              
                
           
                        
                        
                     

                                
                                   
             
                                            
                
           
                         
                                  
         
       
                     
                  
                 
                        
                 

                         
                               
         
                                                                   
                       
                      
                     
                
                         
                                        
                         
                    
                               
             
                  
                             
           
         
                                
       
                    
                                        
                    
                 

                                            
                               
         
                                        
            
       
                    
                                             
     
                   
                 
                      
                                                                        
                                     
       
     
                              
                      
                         
                      
                       
                      
           
   
}
static size_t mergefiles(struct sortfile *files, size_t ntemps, size_t nfiles,
                         FILE *ofp, char const *output_file) {
  FILE **fps;
  size_t nopened;
  size_t tmp;
  char *tmp___0;

  {
    tmp = open_input_files(files, nfiles, &fps);
    nopened = tmp;
                           
                          
                                         
                                                            
       
     
                                                            
    return (nopened);
  }
}
static void mergelines(struct line *__restrict t, size_t nlines,
                       struct line const *__restrict lo) {
  size_t nlo;
  size_t nhi;
  struct line *hi;
  int tmp;

  {
    nlo = nlines / 2UL;
    nhi = nlines - nlo;
    hi = (struct line *)(t - nlo);
    while (1) {
      tmp =
          compare((struct line const *)(lo - 1), (struct line const *)(hi - 1));
      if (tmp <= 0) {
        t--;
        lo--;
        *t = (struct line) * lo;
        nlo--;
        if (!nlo) {
          return;
        }
      } else {
        t--;
        hi--;
        *t = *hi;
        nhi--;
        if (!nhi) {
          while (1) {
            t--;
            lo--;
            *t = (struct line) * lo;
            nlo--;
            if (!nlo) {
              goto while_break___0;
            }
          }
        while_break___0:;
          return;
        }
      }
    }

           
  }
}
static void sequential_sort(struct line *__restrict lines, size_t nlines,
                            struct line *__restrict temp, _Bool to_temp) {
  int swap___1;
  int tmp;
  size_t nlo;
  size_t nhi;
  struct line *lo;
  struct line *hi;
  size_t tmp___0;
  struct line *dest;
  struct line const *sorted_lo;

  {
    if (nlines == 2UL) {
      tmp = compare((struct line const *)(lines + -1),
                    (struct line const *)(lines + -2));
      swap___1 = 0 < tmp;
      if (to_temp) {
        *(temp + -1) = *(lines + (-1 - swap___1));
        *(temp + -2) = *(lines + (-2 + swap___1));
      } else {
        if (swap___1) {
          *(temp + -1) = *(lines + -1);
          *(lines + -1) = *(lines + -2);
          *(lines + -2) = *(temp + -1);
        }
      }
    } else {
      nlo = nlines / 2UL;
      nhi = nlines - nlo;
      lo = (struct line *)lines;
      hi = (struct line *)(lines - nlo);
                   {
        tmp___0 = nlo;
      }       
                            
       
      sequential_sort(hi, nhi, temp - tmp___0, to_temp);
      if (1UL < nlo) {
        sequential_sort(lo, nlo, temp, (_Bool)(!to_temp));
      } else {
                      {
          *(temp + -1) = *(lo + -1);
        }
      }
      if (to_temp) {
        dest = (struct line *)temp;
        sorted_lo = (struct line const *)lines;
      } else {
        dest = (struct line *)lines;
        sorted_lo = (struct line const *)temp;
      }
      mergelines(dest, nlines, sorted_lo);
    }
           
  }
}
static struct merge_node *init_node(struct merge_node *__restrict parent,
                                    struct merge_node *__restrict node_pool,
                                    struct line *dest, size_t nthreads,
                                    size_t total_lines, _Bool is_lo_child);
static struct merge_node *merge_tree_init(size_t nthreads, size_t nlines,
                                          struct line *dest) {
  struct merge_node *merge_tree;
  struct merge_node *tmp;
  struct merge_node *root;
  struct line *tmp___0;
  struct line *tmp___1;
  struct line *tmp___2;
  size_t tmp___3;

  {
    tmp = (struct merge_node *)xmalloc((2UL * sizeof(*merge_tree)) * nthreads);
    merge_tree = tmp;
    root = merge_tree;
                                         
                           
                      
                           
                      
                       
                       
                                             
    tmp___3 = nlines;
    root->nhi = tmp___3;
                        
                                                    
                     
                            
                                                                              
    init_node(root, root + 1, dest, nthreads, nlines, (_Bool)0);
    return (merge_tree);
  }
}
static void merge_tree_destroy(struct merge_node *merge_tree) {

   

                             
           
   

}
static struct merge_node *init_node(struct merge_node *__restrict parent,
                                    struct merge_node *__restrict node_pool,
                                    struct line *dest, size_t nthreads,
                                    size_t total_lines, _Bool is_lo_child) {
  size_t nlines;
  size_t tmp;
  size_t nlo;
  size_t nhi;
  struct line *lo;
  struct line *hi;
  struct line **parent_end;
  struct line **tmp___0;
  struct merge_node *node;
  struct merge_node *__restrict tmp___1;
  struct line *tmp___2;
  struct line *tmp___3;
  size_t lo_threads;
  size_t hi_threads;
  struct merge_node *tmp___4;
  struct merge_node *tmp___5;

  {
                      
                        
           {
      tmp = parent->nhi;
    }
    nlines = tmp;
    nlo = nlines / 2UL;
    nhi = nlines - nlo;
                            
                  
                      
                                
            
                                
     
                         
    tmp___1 = node_pool;
                
    node = (struct merge_node *)tmp___1;
                 
                           
                       
                 
                           
                       
                            
    node->nlo = nlo;
    node->nhi = nhi;
    node->parent = (struct merge_node *)parent;
    node->level = parent->level + 1U;
                            
                                                                              
                         
                                  
                                         
                                                      
               
                                                                            
                          
                                                      
               
                                                                            
                          
            
                                                        
                                                        
     
    return ((struct merge_node *)node_pool);
  }
}
static int compare_nodes(void const *a, void const *b) {
  struct merge_node const *nodea;
  struct merge_node const *nodeb;

  {
    nodea = (struct merge_node const *)a;
    nodeb = (struct merge_node const *)b;
                                       
                                                                 
     
    return (nodea->level < nodeb->level);
  }
}
__inline static void lock_node(struct merge_node *node) {

   

                                    
           
   

}
__inline static void unlock_node(struct merge_node *node) {

   

                                      
           
   

}
static void queue_destroy(struct merge_node_queue *queue) {

   

                                     
                                       
                                         
           
   

}
static void queue_init(struct merge_node_queue *queue, size_t nthreads) {

   

    queue->priority_queue = heap_alloc(&compare_nodes, 2UL * nthreads);
    pthread_mutex_init(&queue->mutex, (pthread_mutexattr_t const *)((void *)0));
                                                                             
           
   

}
static void queue_insert(struct merge_node_queue *queue,
                         struct merge_node *node) {

   

                                      
    heap_insert(queue->priority_queue, (void *)node);
                            
                                        
                                      
           
   

}
static struct merge_node *queue_pop(struct merge_node_queue *queue) {
  struct merge_node *node;

  {
                                      
              {
      node = (struct merge_node *)heap_remove_top(queue->priority_queue);
                 
                         
       
                                                     
    }
  while_break:
    pthread_mutex_unlock(&queue->mutex);
                    
                            
    return (node);
  }
}
static struct line saved;
static void write_unique(struct line const *line, FILE *tfp,
                         char const *temp_output) {
  int tmp;

  {
    if (unique) {
      if (saved.text) {
        tmp = compare(line, (struct line const *)(&saved));
        if (!tmp) {
          return;
        }
      }
      saved = (struct line) * line;
    }
    write_line(line, tfp, temp_output);
           
  }
}
static void mergelines_node(struct merge_node *__restrict node,
                            size_t total_lines, FILE *tfp,
                            char const *temp_output) {
  struct line *lo_orig;
  struct line *hi_orig;
  size_t to_merge;
  size_t merged_lo;
  size_t merged_hi;
  struct line *dest;
  int tmp;
  size_t tmp___0;
  size_t tmp___1;
  size_t tmp___2;
  int tmp___3;
  size_t tmp___4;
  size_t tmp___5;
  size_t tmp___6;

  {
    lo_orig = node->lo;
    hi_orig = node->hi;
                                                              
                           
                           
                 

                                                                     
                                                                       
                               
                       
                           
                               
             
                  
                             
           
                
                           
         
                                                          
                                                           
                       
                 
                       
                              
                
                 
                       
                              
         
       
                
                                               
                                               
                                   
                   

                                                                       
                               
                       
                           
                                   
             
                  
                                 
           
                 
                       
                              
         
                       
              
                                     
                     

                                                                         
                                 
                         
                             
                                     
               
                    
                                   
             
                   
                         
                                
           
                         
         
       
                           
           {
      while (1) {

        if ((unsigned long)node->lo != (unsigned long)node->end_lo) {
          if ((unsigned long)node->hi != (unsigned long)node->end_hi) {
                               
                       
                           
                                   
             
          } else {
            goto while_break___2;
          }
        } else {
          goto while_break___2;
        }
        tmp___3 = compare((struct line const *)(node->lo - 1),
                          (struct line const *)(node->hi - 1));
        if (tmp___3 <= 0) {
          (node->lo)--;
          write_unique((struct line const *)node->lo, tfp, temp_output);
        } else {
          (node->hi)--;
          write_unique((struct line const *)node->hi, tfp, temp_output);
        }
      }
    while_break___2:
      merged_lo = (size_t)(lo_orig - node->lo);
                                               
      if (node->nhi == merged_hi) {
                  {

                                                                       
                               
                       
                           
                                   
             
                  
                                 
           
          (node->lo)--;
          write_unique((struct line const *)node->lo, tfp, temp_output);
        }
      while_break___3:;
      } else {
                                    {
          while (1) {

            if ((unsigned long)node->hi != (unsigned long)node->end_hi) {
                                 
                         
                             
                                     
               
            } else {
              goto while_break___4;
            }
            (node->hi)--;
            write_unique((struct line const *)node->hi, tfp, temp_output);
          }
        while_break___4:;
        }
      }
    }
    merged_lo = (size_t)(lo_orig - node->lo);
    merged_hi = (size_t)(hi_orig - node->hi);
    node->nlo -= merged_lo;
    node->nhi -= merged_hi;
           
  }
}
static void queue_check_insert(struct merge_node_queue *queue,
                               struct merge_node *node) {
  _Bool lo_avail;
  _Bool hi_avail;
  int tmp;
  int tmp___0;
  int tmp___1;

  {
                       {
      lo_avail = (_Bool)(node->lo - node->end_lo != 0L);
                                                        
      if (lo_avail) {
                      {
          tmp = 1;
        }       
                           
                    
                  
                    
           
         
        tmp___1 = tmp;
      } else {
                       
                           
                        
                  
                        
           
                
                      
         
                          
      }
      if (tmp___1) {
        queue_insert(queue, node);
      }
    }
           
  }
}
static void queue_check_insert_parent(struct merge_node_queue *queue,
                                      struct merge_node *node) {

   

                           
                              
                                              
                                
           {
      if (node->nlo + node->nhi == 0UL) {
        queue_insert(queue, node->parent);
      }
    }
           
   

}
static void merge_loop(struct merge_node_queue *queue, size_t total_lines,
                       FILE *tfp, char const *temp_output) {
  struct merge_node *node;
  struct merge_node *tmp;

  {
    while (1) {
      tmp = queue_pop(queue);
      node = tmp;
      if (node->level == 0U) {
                          
                                  
        goto while_break;
      }
      mergelines_node(node, total_lines, tfp, temp_output);
      queue_check_insert(queue, node);
      queue_check_insert_parent(queue, node);
                        
    }
  while_break:;
           
  }
}
static void sortlines(struct line *__restrict lines, size_t nthreads,
                      size_t total_lines, struct merge_node *node,
                      struct merge_node_queue *queue, FILE *tfp,
                      char const *temp_output);
static void *sortlines_thread(void *data) {
  struct thread_args const *args;

  {
                                            
                                                                 
                                                                         
                                                                        
                                               
    return ((void *)0);
  }
}
static void sortlines(struct line *__restrict lines, size_t nthreads,
                      size_t total_lines, struct merge_node *node,
                      struct merge_node_queue *queue, FILE *tfp,
                      char const *temp_output) {
  size_t nlines;
  size_t lo_threads;
  size_t hi_threads;
  pthread_t thread;
  struct thread_args args;
  size_t nlo;
  size_t nhi;
  struct line *temp;
  int tmp;

  {
    nlines = node->nlo + node->nhi;
    lo_threads = nthreads / 2UL;
    hi_threads = nthreads - lo_threads;
                                      
                               
                                   
                               
                       
                   
                                   
                         
                               
                                                                          
                                                                 
                       
                                                                               
                                             
                                                     
                
                      
         
              
                    
       
           {
    _L___0:
      nlo = node->nlo;
      nhi = node->nhi;
      temp = (struct line *)(lines - total_lines);
                     {
        sequential_sort(lines - nlo, nhi, temp - nlo / 2UL, (_Bool)0);
      }
                     {
        sequential_sort(lines, nlo, temp, (_Bool)0);
      }
      node->lo = (struct line *)lines;
      node->hi = (struct line *)(lines - nlo);
      node->end_lo = (struct line *)(lines - nlo);
      node->end_hi = (struct line *)((lines - nlo) - nhi);
      queue_insert(queue, node);
      merge_loop(queue, total_lines, tfp, temp_output);
    }
                                       
           
  }
}
static void avoid_trashing_input(struct sortfile *files, size_t ntemps,
                                 size_t nfiles, char const *outfile) {
  size_t i;
  _Bool got_outstat;
  struct stat outstat;
  struct tempnode *tempcopy;
  _Bool is_stdin;
  int tmp;
  _Bool same;
  struct stat instat;
  int tmp___0;
  int tmp___1;
  int tmp___2;
  int tmp___3;
  int tmp___4;
  int tmp___5;
  int tmp___6;
  int tmp___7;
  FILE *tftp;

   
                           
                                              
               
               

                          
                         
       
                                           
                                   
                    
                                                     
                           
                          
                            
                  
                        
           
                
                      
         
              
             
                           
                        
                                              
                              
                  
                                         
                              
           
                             
                             
           
                                 
         
                       
                                      
                            
                
                                                     
                            
         
                           
                                                
                                                  
                          
                    
                          
             
                  
                        
           
                
                      
         
                              
       
                 
                        
                                        
                                                           
                                                     
         
                                                           
                                     
       
          
     
               
           
   
}
static void merge(struct sortfile *files, size_t ntemps, size_t nfiles,
                  char const *output_file) {
  size_t in;
  size_t out;
  size_t remainder;
  size_t cheap_slots;
  FILE *tfp;
  struct tempnode *temp;
  struct tempnode *tmp;
  size_t num_merged;
  size_t tmp___0;
  size_t tmp___1;
  size_t tmp___2;
  size_t nshortmerge;
  FILE *tfp___0;
  struct tempnode *temp___0;
  struct tempnode *tmp___3;
  size_t num_merged___0;
  size_t tmp___4;
  size_t tmp___5;
  size_t tmp___6;
  size_t tmp___7;
  FILE **fps;
  size_t nopened;
  size_t tmp___8;
  FILE *ofp;
  FILE *tmp___9;
  char *tmp___10;
  int *tmp___11;
  char *tmp___12;
  FILE *tfp___1;
  struct tempnode *temp___1;
  size_t tmp___13;
  size_t tmp___14;

   
               

                                       
                         
       
                     
               
                 

                                               
                               
         
                                
                   
                                      
                           
                
                                   
         
                                                                      
                                                         
                             
                                  
                           
                
                               
         
                          
                                                         
                                   
                         
              
       
                    
                              
                                                                        
                                    
                                                      
                                        
                           
                                   
                           
                
                                
         
                                                                       
                                                             
                                 
                                      
                           
                
                                   
         
                          
                                                             
                      
              
                                           
                             
       
                                                                
                                              
                    
                         
     
              
                                                             
               
                                                      
                        
                              
                                                
                      
                  
                                                                 
                               
         
                                      
                              
                                            
                                                   
                
                               
                                              
                                                     
           
         
              
                             
                                            
                                                               
         
       
                 
                  
                                                           
                                                                           
                           
                               
         
       
                     
                             
                          
              
                           
       
                                                     
                                                    
                             
                          
              
                           
       
                         
                                                         
                                   
                                                                   
                                                   
               
                              
     
                   
           
   
}
static void sort(char *const *files, size_t nfiles, char const *output_file,
                 size_t nthreads) {
  struct buffer buf___1;
  size_t ntemps;
  _Bool output_file_created;
  char const *temp_output;
  char const *file;
  FILE *fp;
  FILE *tmp;
  FILE *tfp;
  size_t bytes_per_line;
  size_t tmp___0;
  size_t mult;
  size_t tmp___1;
  struct line *line;
  struct tempnode *tmp___2;
  struct merge_node_queue queue;
  struct merge_node *merge_tree;
  struct merge_node *tmp___3;
  struct merge_node *root;
  _Bool tmp___4;
  size_t i;
  struct tempnode *node;
  struct sortfile *tempfiles;
  struct sortfile *tmp___5;

  {
    ntemps = (size_t)0;
                                   
    buf___1.alloc = (size_t)0;
    while (1) {

                    
                         
       
      file = (char const *)*files;
      tmp = xfopen(file, "r");
      fp = tmp;
                          {
                            
        mult = (size_t)1;
                   

                                      
                                 
           
                         
                 
         
      while_break___0:
        bytes_per_line = mult * sizeof(struct line);
      }       
                                                           
       
      if (!buf___1.alloc) {
        tmp___1 = sort_buffer_size((FILE *const *)(&fp), (size_t)1, files,
                                   nfiles, bytes_per_line);
        initbuf(&buf___1, bytes_per_line, tmp___1);
      }
                             
      files++;
      nfiles--;
                {
        tmp___4 = fillbuf___7(&buf___1, fp, file);
                       
                               
         
                         {
          if (nfiles) {
                                                                       
                                                                            {
              buf___1.left = buf___1.used;
              goto while_break___1;
            }
          }
        }
        line = buffer_linelim((struct buffer const *)(&buf___1));
                         {
                       {
                         {
                                 {
                                  
                tfp = xfopen(output_file, "w");
                temp_output = output_file;
                                               
              }       
                         
                                            
                                                            
               
            }       
                       
                                          
                                                          
             
          }       
                     
                                        
                                                        
           
        }       
                   
                                      
                                                      
         
                                  {
          queue_init(&queue, nthreads);
          tmp___3 = merge_tree_init(nthreads, buf___1.nlines, line);
          merge_tree = tmp___3;
          root = merge_tree + 1;
          sortlines(line, nthreads, buf___1.nlines, root, &queue, tfp,
                    temp_output);
                                
                                             
                                         
        }       
                                                                          
         
                                  
                                 {
          goto finish;
        }
      }
    while_break___1:
      xfclose(fp, file);
    }
  while_break:;
  finish:
    free((void *)buf___1.buf);
                               
                                         
                                                                        
                          
                    
                 

                    
                               
         
                                                           
                                     
                                             
            
       
                    
                                                    
                              
     
               
           
  }
}
static void insertkey(struct keyfield *key_arg) {
  struct keyfield **p;
  struct keyfield *key;
  struct keyfield *tmp;

  {
    tmp = (struct keyfield *)xmemdup((void const *)key_arg, sizeof(*key));
    key = tmp;
    p = &keylist;
               

                
                         
       
                  
           
                      
     
  while_break:
    *p = key;
                                               
           
  }
}
static __attribute__((__noreturn__)) void badfieldspec(char const *spec,
                                                       char const *msgid);
static __attribute__((__noreturn__)) void badfieldspec(char const *spec,
                                                       char const *msgid);
static void badfieldspec(char const *spec, char const *msgid) {
  char const *tmp;
  char *tmp___0;
  char *tmp___1;

   
                      
                             
                                                            
                                                     
            
   
}
static __attribute__((__noreturn__)) void
incompatible_options(char const *opts___1);
static __attribute__((__noreturn__)) void
incompatible_options(char const *opts___1);
static void incompatible_options(char const *opts___1) {
  char *tmp;

   
                                                      
                                             
            
   
}
static void check_ordering_compatibility(void) {
  struct keyfield *key;
  char opts___1[sizeof(short_options)];
  _Bool tmp;
  _Bool tmp___0;

   
                  
               

                 
                         
       
                                                                 
                                          
                                 
                                                                               
                           
                               
                      
                               
                               
                                                            
                                                       
       
                      
     
               
           
   
}
static char const *parse_field_count(char const *string, size_t *val,
                                     char const *msgid) {
  char *suffix;
  uintmax_t n;
  strtol_error tmp;
  char const *tmp___0;
  char *tmp___1;
  char *tmp___2;

  {
    tmp = xstrtoumax(string, &suffix, 10, &n, "");
                                  
                  
     
                                  
                  
     
                                  
                  
     
                                  
                  
     
                                  
                  
     
                      
  case_0:
    *val = n;
                    
                        
     
         
                                
                      
         
                
                              
                               
                                                            
                                                           
     
                                       
                
    return ((char const *)suffix);
  }
}
static char *set_ordering(char const *s, struct keyfield *key,
                          enum blanktype blanktype) {

   

              {

                
                         
       
                                  
                     
       
                                   
                      
       
                                   
                      
       
                                   
                      
       
                                   
                      
       
                                   
                      
       
                                  
                     
       
      if ((int const) * s == 110) {
        goto case_110;
      }
                                  
                     
       
      if ((int const) * s == 114) {
        goto case_114;
      }
                                  
                     
       
                          
            
                                          
                                    
              
                                            
                                      
         
       
                                          
                                    
              
                                            
                                      
         
       
                        
             
                                                   
                        
             
                                                    
                        
             
                                      
                        
             
                                    
                        
             
                         
                                                   
       
                        
    case_77:
      key->month = (_Bool)1;
      goto switch_break;
    case_110:
      key->numeric = (_Bool)1;
                        
            
                             
                        
    case_114:
      key->reverse = (_Bool)1;
                        
            
                              
                        
                   
                         
    switch_break:
      s++;
    }
  while_break:;
    return ((char *)s);
   

}
static struct keyfield *key_init(struct keyfield *key) {

   

    memset((void *)key, 0, sizeof(*key));
                                      
    return (key);
   

}
static void sighandler(int sig___0) {

   

              
                                      
                   
           
   

}
int main(int argc, char **argv);
static int const sig[11] = {(int const)14, (int const)1,  (int const)2,
                            (int const)13, (int const)3,  (int const)15,
                            (int const)29, (int const)27, (int const)26,
                            (int const)24, (int const)25};
static char opts[10] = {(char)'X', (char)' ',   (char)'-', (char)'-',
                        (char)'d', (char)'e',   (char)'b', (char)'u',
                        (char)'g', (char)'\000'};
static char *minus = (char *)"-";
static char opts___0[3] = {(char)0, (char)'o', (char)0};
int main(int argc, char **argv) {
  struct keyfield *key;
  struct keyfield key_buf;
  struct keyfield gkey;
  _Bool gkey_only;
  char const *s;
  int c;
  char checkonly;
  _Bool mergeonly;
  char *random_source;
  _Bool need_random;
  size_t nthreads;
  size_t nfiles;
  _Bool posixly_correct;
  char *tmp;
  _Bool obsolete_usage;
  int tmp___0;
  char **files;
  char *files_from;
  struct Tokens tok;
  char const *outfile;
  struct lconv const *locale;
  struct lconv const *tmp___1;
  size_t i;
  struct sigaction act;
  int tmp___2;
  int oi;
  size_t tmp___3;
  int tmp___4;
  _Bool minus_pos_usage;
  int tmp___5;
  int tmp___6;
  char const *optarg1;
  int tmp___7;
  char *tmp___8;
  char *tmp___9;
  size_t tmp___10;
  ptrdiff_t tmp___11;
  char str[2];
  ptrdiff_t tmp___12;
  char *tmp___13;
  int tmp___14;
  size_t tmp___15;
  size_t tmp___16;
  size_t tmp___17;
  char *tmp___18;
  int tmp___19;
  char *tmp___20;
  int tmp___21;
  char newtab;
  char *tmp___22;
  char const *tmp___23;
  char *tmp___24;
  int tmp___25;
  char *tmp___26;
  char const *p;
  FILE *stream;
  char const *tmp___27;
  char *tmp___28;
  char *tmp___29;
  char const *tmp___30;
  char *tmp___31;
  int *tmp___32;
  int tmp___33;
  char const *tmp___34;
  char *tmp___35;
  _Bool tmp___36;
  int tmp___37;
  size_t i___0;
  char const *tmp___38;
  char *tmp___39;
  unsigned long file_number;
  char *tmp___40;
  char *tmp___41;
  int tmp___42;
  char const *tmp___43;
  char *tmp___44;
  _Bool tmp___45;
  _Bool tmp___46;
  char *tmp___47;
  char const *tmp___48;
  char *tmp___49;
  char *tmp___50;
  char const *tmp_dir;
  char const *tmp___51;
  char const *tmp___52;
  char const *tmp___53;
  char *tmp___54;
  int tmp___56;
  _Bool tmp___57;
  struct sortfile *sortfiles;
  struct sortfile *tmp___58;
  size_t i___1;
  unsigned long np;
  unsigned long tmp___59;
  size_t nthreads_max;
  char *tmp___60;
  int tmp___61;

  {
                         
          
    checkonly = (char)0;
    mergeonly = (_Bool)0;
                                        
                           
                         
    nfiles = (size_t)0;
    tmp = getenv("POSIXLY_CORRECT");
    posixly_correct = (_Bool)((unsigned long)tmp != (unsigned long)((void *)0));
                               
                                               
                                     
    outfile = (char const *)((void *)0);
    set_program_name((char const *)*(argv + 0));
                     
                                                           
                            
                               
                                     
                                  
                                                 
                     
                                                                
                         
                          
            
                                         
                            
       
     
                                                            
                         
                         
            
                                         
                           
       
     
                               
    inittables();
                                 
                  
               

                        
                         
       
                                                                          
                                                              
                                              
                                                
       
          
     
              
                                                     
                                 
                     
                  
               

                        
                             
       
                                                                              
                    
                                                                
                                                   
       
          
     
                  
                                 
                          
                    
    gkey.sword = 0xffffffffffffffffUL;
    files = (char **)xnmalloc((size_t)argc, sizeof(*files));
    while (1) {
              
                    
                
             {
                              
                              
                                 
                               
                                     
                                                           
                                                              
                                                    
                                    
                              
                                                 
                                      
                                
                                  
                         
                       
                            
                              
                     
                          
                            
                   
                        
                          
                 
                      
                        
               
                    
                      
             
                  
                        
           
               {
        _L___1:
          c = getopt_long(argc, (char *const *)argv, short_options,
                          long_options___1, &oi);
          if (c == -1) {
          _L:
                                {
              goto while_break___1;
            }
                             
                     
                             
                     
                                                   
          } else {
                         
                          
             
                           
                            
             
                          
                           
             
                           
                           
             
                           
                           
             
                           
                           
             
                           
                           
             
                           
                           
             
            if (c == 77) {
              goto case_98;
            }
            if (c == 110) {
              goto case_98;
            }
            if (c == 114) {
              goto case_98;
            }
                          
                           
             
                          
                           
             
                           
                            
             
            if (c == 99) {
              goto case_99;
            }
                          
                           
             
                           
                            
             
                           
                            
             
                           
                            
             
            if (c == 107) {
              goto case_107;
            }
                           
                            
             
                           
                            
             
                           
                            
             
                           
                            
             
                           
                            
             
                          
                           
             
            if (c == 116) {
              goto case_116;
            }
                          
                           
             
                           
                            
             
            if (c == 117) {
              goto case_117;
            }
                           
                            
             
                           
                            
             
                            
                                
             
                            
                                
             
                                
          case_1:
            key = (struct keyfield *)((void *)0);
                                           
                                   
                                                         
                                                                          
                                
                          
                                
                   
                        
                              
                 
                      
                            
               
                                               
                                    
                                       
                              
                        
                              
                 
                      
                            
               
                                                                      
                                   
                                         
                                                                              
                                                                 
                        
                                              
                                                             
                                                                     
                   
                 
                                  
                                    
                                                      
                   
                 
                         
                                                       
                        
                                                                    
                                 
                                                         
                          
                                          
                                       
                               
                                                                
                                                                     
                                                                          
                                                  
                                                                 
                                                                            
                       
                                        
                                         
                                         
                         
                       
                                                                        
                                     
                                                                               
                       
                     
                                                  
                                   
                   
                 
               
             
                      {
              tmp___10 = nfiles;
              nfiles++;
              *(files + tmp___10) = optarg;
            }
            goto switch_break;
                   
                                            
                                                                      
                                                     
                                          
          case_98:
            str[0] = (char)c;
                                  
            set_ordering((char const *)(str), &gkey, (enum blanktype)2);
            goto switch_break;
                   
                         
                                              
                                                                           
                                                        
                                             
                    
                      
             
          case_99:
            if (checkonly) {
                                        
                                           
               
            }
            checkonly = (char)c;
            goto switch_break;
                   
                                   
                                                                        
                                     
                                                                           
                                                    
               
             
                                                    
                              
                   
                             
                              
                   
                                
                              
          case_107:
            key = key_init(&key_buf);
            s = parse_field_count((char const *)optarg, &key->sword,
                                  "invalid number at field start");
                                  
                           
                            
                                                                         
             
                                        
                                                       
                                                                  
                                    
                             
                              
                                                                               
               
             
                              
                                
                                                  
               
             
            s = (char const *)set_ordering(s, key, (enum blanktype)0);
                                        
                                                
                                     
                    
                                                       
                                                                  
                                    
                             
                              
                                                                           
               
                                          
                                                         
                                                                    
               
                                                                        
             
                     
                                                
                                                            
             
            insertkey(key);
                              
                   
                                 
                              
                   
                                                              
                              
                   
                          
                                                               
                                     
                                                                      
                                                    
               
             
                                           
                              
                   
                                
                        
                                                                            
                                     
                                                                        
                                                    
               
             
                                   
                              
                   
                              
            goto switch_break;
                  
                                                                 
                              
          case_116:
            newtab = *(optarg + 0);
                          
                                              
                                                  
             
                                
                                                             
                                  
                                      
                      
                                                       
                                                             
                                                              
               
             
                             
                                       
                                                        
                                                    
               
             
            tab = (int)newtab;
                              
                  
                                               
                              
                   
                                                                           
                              
          case_117:
            unique = (_Bool)1;
                              
                   
                                        
                                                        
                                       
                         

                                                      
                                       
                 
                            
                     
                    
               
                            
                                             
             
                              
                   
                              
                              
                       
                     
                              
                       
                                                                 
                                                                            
                    
                              
                         
                     
          switch_break:;
          }
        }
      }
    }
  while_break___1:;
                     
                   
                                                     
                                               
                                                      
                  
                                                                           
                                          
                 
       
                                                       
                          
                       
              
                                                            
                                                                  
                                                     
                                                           
                                        
                                                                
         
       
                             
                                           
                     
                                      
                            
                                                     
                                                               
                                                        
         
              
                                                   
                                                             
                                                      
       
                      
                            
                        
                           
                          
                   

                                  
                                 
           
                                                                 
                              
                                                             
                                                                             
                                                     
                                                          
                  
                                                    
                                        
                                                                  
                                                                          
                                                                         
             
           
                  
         
                       
              
                                                   
                                               
                                                      
       
     
                  
               

                 
                             
       
                                                                   
                     
                            
                                    
                                          
                                              
                                              
                                  
                                      
                                                      
                                                  
                                      
                                    
                                      
         
       
                                                                 
                      
     
                   
                  {
      tmp___46 = default_key_compare((struct keyfield const *)(&gkey));
      if (!tmp___46) {
                             
        insertkey(&gkey);
                                                                   
      }
    }
                                   
                
                      
                    
              
                      
               
                          
                                
                  
                                
           
                                                     
         
       
                            
                                                           
                                                 
                                                     
                                                      
              
                                                           
                                            
       
                                                                
     
    reverse = gkey.reverse;
                      
                                                         
     
                                
                                                
                         
                    
                           
              
                          
       
                             
     
                        
                         
                          
                     
     
                          
                                                                            
                              
              
                                                                        
       
     
    if (checkonly) {
                         
                                                     
                                                                    
                                                                      
       
                    
                                
                                                       
       
      tmp___57 = check((char const *)*(files + 0), checkonly);
                     
                     
             {
        tmp___56 = 1;
      }
      exit(tmp___56);
    }
                    
                                                                        
                           
                        
                 

                                
                               
         
                                                                   
                
       
                    
                                                   
           {
                     {
        tmp___59 = num_processors((enum nproc_query)2);
        np = tmp___59;
                      {
          nthreads = np;
        }       
                               
         
      }
                                                                              
                                    
                            
              
                                
       
      sort((char *const *)files, nfiles, outfile, nthreads);
    }
                          
                                   
                           
                                           
                                         
       
     
            
  }
}
