typedef unsigned long size_t;
typedef long __off_t;
typedef long __off64_t;
struct _IO_FILE;
struct _IO_FILE;
struct _IO_FILE;
typedef struct _IO_FILE FILE;
typedef void _IO_lock_t;
struct _IO_marker {
  struct _IO_marker *_next;
  struct _IO_FILE *_sbuf;
  int _pos;
};
struct _IO_FILE {
  int _flags;
  char *_IO_read_ptr;
  char *_IO_read_end;
  char *_IO_read_base;
  char *_IO_write_base;
  char *_IO_write_ptr;
  char *_IO_write_end;
  char *_IO_buf_base;
  char *_IO_buf_end;
  char *_IO_save_base;
  char *_IO_backup_base;
  char *_IO_save_end;
  struct _IO_marker *_markers;
  struct _IO_FILE *_chain;
  int _fileno;
  int _flags2;
  __off_t _old_offset;
  unsigned short _cur_column;
  signed char _vtable_offset;
  char _shortbuf[1];
  _IO_lock_t *_lock;
  __off64_t _offset;
  void *__pad1;
  void *__pad2;
  void *__pad3;
  void *__pad4;
  size_t __pad5;
  int _mode;
  char _unused2[(15UL * sizeof(int) - 4UL * sizeof(void *)) - sizeof(size_t)];
};
typedef long __time_t;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __syscall_slong_t;
typedef __mode_t mode_t;
struct timespec {
  __time_t tv_sec;
  __syscall_slong_t tv_nsec;
};
struct stat {
  __dev_t st_dev;
  __ino_t st_ino;
  __nlink_t st_nlink;
  __mode_t st_mode;
  __uid_t st_uid;
  __gid_t st_gid;
  int __pad0;
  __dev_t st_rdev;
  __off_t st_size;
  __blksize_t st_blksize;
  __blkcnt_t st_blocks;
  struct timespec st_atim;
  struct timespec st_mtim;
  struct timespec st_ctim;
  __syscall_slong_t __glibc_reserved[3];
};
typedef __ino_t ino_t;
typedef __dev_t dev_t;
struct hash_table;
struct hash_table;
struct hash_table;
typedef struct hash_table Hash_table;
struct F_triple {
  char *name;
  ino_t st_ino;
  dev_t st_dev;
};
typedef __off_t off_t;
typedef __builtin_va_list __gnuc_va_list;
typedef __gnuc_va_list va_list;
typedef int wchar_t;
union __anonunion___value_4 {
  unsigned int __wch;
  char __wchb[4];
};
struct __anonstruct___mbstate_t_3 {
  int __count;
  union __anonunion___value_4 __value;
};
typedef struct __anonstruct___mbstate_t_3 __mbstate_t;
typedef unsigned int wint_t;
struct hash_tuning {
  float shrink_threshold;
  float shrink_factor;
  float growth_threshold;
  float growth_factor;
  _Bool is_n_buckets;
};
typedef struct hash_tuning Hash_tuning;
typedef __mbstate_t mbstate_t;
struct mbchar {
  char const *ptr;
  size_t bytes;
  _Bool wc_valid;
  wchar_t wc;
  char buf[24];
};
struct mbuiter_multi {
  _Bool in_shift;
  mbstate_t state;
  _Bool next_done;
  struct mbchar cur;
};
typedef struct mbuiter_multi mbui_iterator_t;
typedef __gid_t gid_t;
typedef __uid_t uid_t;
typedef unsigned long uintmax_t;
struct dev_ino {
  ino_t st_ino;
  dev_t st_dev;
};
struct cycle_check_state {
  struct dev_ino dev_ino;
  uintmax_t chdir_counter;
  int magic;
};
typedef long ptrdiff_t;
struct dirent {
  __ino_t d_ino;
  __off_t d_off;
  unsigned short d_reclen;
  unsigned char d_type;
  char d_name[256];
};
struct __dirstream;
struct __dirstream;
struct __dirstream;
typedef struct __dirstream DIR;
typedef long __ssize_t;
typedef __ssize_t ssize_t;
enum quoting_style {
  literal_quoting_style = 0,
  shell_quoting_style = 1,
  shell_always_quoting_style = 2,
  c_quoting_style = 3,
  c_maybe_quoting_style = 4,
  escape_quoting_style = 5,
  locale_quoting_style = 6,
  clocale_quoting_style = 7,
  custom_quoting_style = 8
};
struct option {
  char const *name;
  int has_arg;
  int *flag;
  int val;
};
typedef __nlink_t nlink_t;
struct I_ring {
  int ir_data[4];
  int ir_default_val;
  unsigned int ir_front;
  unsigned int ir_back;
  _Bool ir_empty;
};
typedef struct I_ring I_ring;
struct _ftsent;
struct _ftsent;
struct _ftsent;
union __anonunion_fts_cycle_26 {
  struct hash_table *ht;
  struct cycle_check_state *state;
};
struct __anonstruct_FTS_25 {
  struct _ftsent *fts_cur;
  struct _ftsent *fts_child;
  struct _ftsent **fts_array;
  dev_t fts_dev;
  char *fts_path;
  int fts_rfd;
  int fts_cwd_fd;
  size_t fts_pathlen;
  size_t fts_nitems;
  int (*fts_compar)(struct _ftsent const **, struct _ftsent const **);
  int fts_options;
  struct hash_table *fts_leaf_optimization_works_ht;
  union __anonunion_fts_cycle_26 fts_cycle;
  I_ring fts_fd_ring;
};
typedef struct __anonstruct_FTS_25 FTS;
struct _ftsent {
  struct _ftsent *fts_cycle;
  struct _ftsent *fts_parent;
  struct _ftsent *fts_link;
  long fts_number;
  void *fts_pointer;
  char *fts_accpath;
  char *fts_path;
  int fts_errno;
  int fts_symfd;
  size_t fts_pathlen;
  FTS *fts_fts;
  ptrdiff_t fts_level;
  size_t fts_namelen;
  nlink_t fts_n_dirs_remaining;
  unsigned short fts_info;
  unsigned short fts_flags;
  unsigned short fts_instr;
  struct stat fts_statp[1];
  char fts_name[1];
};
typedef struct _ftsent FTSENT;
typedef unsigned long reg_syntax_t;
struct quoting_options;
struct quoting_options;
struct quoting_options;
struct quoting_options {
  enum quoting_style style;
  int flags;
  unsigned int quote_these_too[255UL / (sizeof(int) * 8UL) + 1UL];
  char const *left_quote;
  char const *right_quote;
};
struct slotvec {
  size_t size;
  char *val;
};
struct hash_entry {
  void *data;
  struct hash_entry *next;
};
struct hash_table {
  struct hash_entry *bucket;
  struct hash_entry const *bucket_limit;
  size_t n_buckets;
  size_t n_buckets_used;
  size_t n_entries;
  Hash_tuning const *tuning;
  size_t (*hasher)(void const *, size_t);
  _Bool (*comparator)(void const *, void const *);
  void (*data_freer)(void *);
  struct hash_entry *free_entry_list;
};
struct __anonstruct___fsid_t_1 {
  int __val[2];
};
typedef struct __anonstruct___fsid_t_1 __fsid_t;
typedef unsigned long __fsblkcnt_t;
typedef unsigned long __fsfilcnt_t;
typedef long __fsword_t;
struct Active_dir {
  dev_t dev;
  ino_t ino;
  FTSENT *fts_ent;
};
struct statfs {
  __fsword_t f_type;
  __fsword_t f_bsize;
  __fsblkcnt_t f_blocks;
  __fsblkcnt_t f_bfree;
  __fsblkcnt_t f_bavail;
  __fsfilcnt_t f_files;
  __fsfilcnt_t f_ffree;
  __fsid_t f_fsid;
  __fsword_t f_namelen;
  __fsword_t f_frsize;
  __fsword_t f_flags;
  __fsword_t f_spare[4];
};
struct LCO_ent {
  dev_t st_dev;
  _Bool opt_ok;
};
enum rm_interactive {
  RMI_ALWAYS = 3,
  RMI_SOMETIMES = 4,
  RMI_NEVER = 5
};
struct rm_options {
  _Bool ignore_missing_files;
  enum rm_interactive interactive;
  _Bool one_file_system;
  _Bool recursive;
  struct dev_ino *root_dev_ino;
  _Bool stdin_tty;
  _Bool verbose;
  _Bool require_restore_cwd;
};
enum RM_status {
  RM_OK = 2,
  RM_USER_DECLINED = 3,
  RM_ERROR = 4,
  RM_NONEMPTY_DIR = 5
};
enum Ternary {
  T_UNKNOWN = 2,
  T_NO = 3,
  T_YES = 4
};
typedef enum Ternary Ternary;
enum Prompt_action {
  PA_DESCEND_INTO_DIR = 2,
  PA_REMOVE_DIR = 3
};
enum interactive_type {
  interactive_never = 0,
  interactive_once = 1,
  interactive_always = 2
};
size_t freadahead(FILE *fp);
                             
           

   

                                                                               

                         
     

                           
                                                 
            
               
     
                                                                   
   

 
extern __attribute__((__nothrow__)) int(__attribute__((__leaf__))
                                        tolower)(int __c);
extern __attribute__((__nothrow__))
size_t(__attribute__((__nonnull__(1), __leaf__)) strlen)(char const *__s)
    __attribute__((__pure__));
extern int fclose(FILE *__stream);
extern __attribute__((__nothrow__)) int(__attribute__((__leaf__))
                                        fileno)(FILE *__stream);
extern __attribute__((__nothrow__)) int *(
    __attribute__((__leaf__)) __errno_location)(void)__attribute__((__const__));
extern int close(int __fd);
int dup_safer(int fd);
extern __attribute__((__nothrow__)) int(
    __attribute__((__nonnull__(1, 2), __leaf__))
    strcmp)(char const *__s1, char const *__s2) __attribute__((__pure__));
extern __attribute__((__nothrow__)) int(
    __attribute__((__nonnull__(1, 2), __leaf__))
    strncmp)(char const *__s1, char const *__s2, size_t __n)
    __attribute__((__pure__));
__attribute__((__noreturn__)) void xalloc_die(void);
extern __attribute__((__nothrow__)) void *(__attribute__((__leaf__))
                                           malloc)(size_t __size)
    __attribute__((__malloc__));
size_t base_len(char const *name);
char *last_component(char const *name);
char const *file_type(struct stat const *st);
extern __attribute__((__nothrow__)) char *(__attribute__((__leaf__))
                                           gettext)(char const *__msgid)
    __attribute__((__format_arg__(1)));
char const *file_type(struct stat const *st) {
            
  char *tmp___0;
  char *tmp___1;
  char const *tmp___2;
                      
                      
                      
                      
                      
                      
                      
                       
                       

  {
    if ((st->st_mode & 61440U) == 32768U) {
                              
                                            
                      
             {
        tmp___0 = gettext("regular file");
        tmp___1 = tmp___0;
      }
      return ((char const *)tmp___1);
    }
                                          {
      tmp___2 = (char const *)gettext("directory");
      return (tmp___2);
    }
                                           
                                                            
                       
     
                                          
                                                                
                       
     
                                          
                                              
                       
     
                                           
                                                       
                       
     
                                           
                                                
                       
     
                                    
                                                       
                       
     
                                    
                                                   
                       
     
                                    
                                                               
                        
     
                                                   
                      
  }
}
void *hash_lookup(Hash_table const *table___0, void const *entry);
void *(__attribute__((__warn_unused_result__))
       hash_insert)(Hash_table *table___0, void const *entry);
void triple_free(void *x);
void *xmalloc(size_t n) __attribute__((__malloc__));
extern int fflush(FILE *__stream);
int rpl_fflush(FILE *stream);
int(__attribute__((__nonnull__(1))) rpl_fseeko)(FILE *fp, off_t offset,
                                                int whence);
extern __attribute__((__nothrow__)) int(__attribute__((__leaf__))
                                        __freading)(FILE *__fp);
                                                                        

   

                           
                                  
     
           
   

 
int rpl_fflush(FILE *stream) {
          
              
  int tmp___1;

  {
                                                              
                           
                   
            
                                   
                            
                             
                     
       
     
                                                    
    tmp___1 = fflush(stream);
    return (tmp___1);
  }
}
int fd_safer(int fd);
int fd_safer(int fd) {
        
          
        
               
               

   

                  
                    
                            
                
                                     
                     
                  
                                     
                     
               
       
     
    return (fd);
   

}
extern int(__attribute__((__nonnull__(1))) open)(char const *__file,
                                                 int __oflag, ...);
extern int fcntl(int __fd, int __cmd, ...);
int rpl_fcntl(int fd, int action, ...);
static int have_dupfd_cloexec = 0;
int rpl_fcntl(int fd, int action, ...) {
  va_list arg;
  int result;
  int target;
  int tmp;
               
  int flags;
  int tmp___1;
  int saved_errno;
  int *tmp___2;
  int *tmp___3;
  int tmp___4;
  void *p;
  void *tmp___5;

  {
    result = -1;
    __builtin_va_start(arg, action);
                        {
      goto case_1030;
    }
    goto switch_default;
  case_1030:
    tmp = __builtin_va_arg(arg, int);
    target = tmp;
                                 {
      result = fcntl(fd, action, target);
                       {
        have_dupfd_cloexec = 1;
      }       
                                     
                             
                                 
                
                                            
                           
                              
           
                                  
         
       
    }       
                                        
     
                     {
                                     

        tmp___1 = fcntl(result, 1);
        flags = tmp___1;
                        
                  
               {
          tmp___4 = fcntl(result, 2, flags | 1);
                             {
             
             tmp___2 = __errno_location();
            saved_errno = *tmp___2;
            close(result);
            tmp___3 = __errno_location();
            *tmp___3 = saved_errno;
            result = -1;
          }
        }
       

    }
    goto switch_break;
  switch_default:
    tmp___5 = __builtin_va_arg(arg, void *);
    p = tmp___5;
    result = fcntl(fd, action, p);
    goto switch_break;
  switch_break:
    __builtin_va_end(arg);
    return (result);
  }
}
                          
int volatile exit_failure = (int volatile)1;
extern __attribute__((__nothrow__)) unsigned short const **(
    __attribute__((__leaf__)) __ctype_b_loc)(void)__attribute__((__const__));
extern struct _IO_FILE *stdin;
extern __attribute__((__nothrow__)) int(__attribute__((__leaf__))
                                        ferror_unlocked)(FILE *__stream);
extern __attribute__((__nothrow__))
size_t(__attribute__((__leaf__)) __ctype_get_mb_cur_max)(void);
extern __attribute__((__nothrow__)) void(__attribute__((__leaf__))
                                         free)(void *__ptr);
extern __attribute__((__nothrow__, __noreturn__)) void(__attribute__((__leaf__))
                                                       abort)(void);
extern
    __attribute__((__nothrow__)) void *(__attribute__((__nonnull__(1),
                                                       __leaf__))
                                        memset)(void *__s, int __c, size_t __n);
extern __attribute__((__nothrow__)) char *(__attribute__((__nonnull__(1),
                                                          __leaf__))
                                           strrchr)(char const *__s, int __c)
    __attribute__((__pure__));
int(__attribute__((__nonnull__(1, 2))) mbscasecmp)(char const *s1,
                                                   char const *s2);
extern __attribute__((__nothrow__))
wint_t(__attribute__((__leaf__)) towlower)(wint_t __wc);
size_t hash_string(char const *string, size_t n_buckets);
Hash_table *(__attribute__((__warn_unused_result__))
             hash_initialize)(size_t candidate, Hash_tuning const *tuning,
                              size_t (*hasher)(void const *, size_t),
                              _Bool (*comparator)(void const *, void const *),
                              void (*data_freer)(void *));
void hash_free(Hash_table *table___0);
extern __attribute__((__nothrow__, __noreturn__)) void(__attribute__((
    __leaf__)) __assert_fail)(char const *__assertion, char const *__file,
                              unsigned int __line, char const *__function);
extern __attribute__((__nothrow__)) int(__attribute__((__leaf__))
                                        mbsinit)(mbstate_t const *__ps)
    __attribute__((__pure__));
extern __attribute__((__nothrow__))
size_t(__attribute__((__leaf__))
       mbrtowc)(wchar_t *__restrict __pwc, char const *__restrict __s,
                size_t __n, mbstate_t *__restrict __p);
unsigned int const is_basic_table[8];
                                        

   

                                                                    
                                                      
                         
   

 
size_t strnlen1(char const *string, size_t maxlen);
                                                                     
          
                 
                 
              
                

   
                          
             
     
                         
                      
     
                                               
                  
                                  
                                                 
                                    
            
                                                       
                 
                      
                                     
                                                                              
                                  
       
                                
               
                                         
                                                 
                       
                                                                       
                                                    
                                    
                                      
              
                                                      
                                                  
                                        
                
                                       
                                        
                                                        
                            
                                              
                                                                              
                                              
             
                                       
                            
                                      
                                                                              
                                              
             
           
                                        
                                                               
                        
                                      
           
         
       
     
                               
           
   
 
void *xrealloc(void *p, size_t n);
_Bool euidaccess_stat(struct stat const *st, int mode);
extern __attribute__((__nothrow__))
__uid_t(__attribute__((__leaf__)) geteuid)(void);
extern __attribute__((__nothrow__))
__gid_t(__attribute__((__leaf__)) getegid)(void);
extern __attribute__((__nothrow__)) int(__attribute__((__leaf__))
                                        group_member)(__gid_t __gid);
_Bool euidaccess_stat(struct stat const *st, int mode) {
  uid_t euid;
                       
              
              
              
              
              
              
              
              
               
             
                 
               

  {
              
                    
                        
     
    euid = geteuid();
                    {
                            

        return ((_Bool)1);
               
                         
                                                                        
                            
         
       
    }
                        
                          
                           
                                   
                                     
                                      
                                           
                                             
                                              
                                                          
                            
                                  
                     
                          
                                
                   
                        
                              
                 
                      
                            
               
                    
                          
             
                  
                        
           
                
                      
         
              
                    
       
            
           
                               
                         
              
                    
       
                               
                         
              
                    
       
                              
                         
              
                    
       
                                                         
                         
              
                    
       
                                                         
                         
              
                    
       
                                                        
                         
              
                    
       
                                                                
                    
              
                    
       
                                                                
                    
              
                    
       
                                                               
                     
              
                     
       
                                                                                
                                             
                                            
                                           
                                          
                                         
     
                                    
                    
            
                           
                      
                                      
                      
              
                                                     
                       
                        
         
       
     
                                                
                        
     
                      
  }
}
                       
          

   
                              
                 
   
 
extern __attribute__((__nothrow__)) int(
    __attribute__((__nonnull__(1, 2), __leaf__))
    stat)(char const *__restrict __file, struct stat *__restrict __buf);
extern
    __attribute__((__nothrow__)) int(__attribute__((__nonnull__(2), __leaf__))
                                     fstat)(int __fd, struct stat *__buf);
                             
                             
                                
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                                     
                                                                       
                                                                       
                                                                       
                                                                       
                                                                       
                                                                     
                                                               
                                                                     
                                                                       
                                                                       
                                                                       
                                                                       
                                                                       
                                                                     
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                                       
                                                                       
                                                                       
                                                                       
                                                                     
                                                                     
                                                                       
                                                                   
                                                                       
                                                                       
                                                                       
                                                                       
                                                                     
                                                                     
                                                                       
                                                                      
                                
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              
                   
void cycle_check_init(struct cycle_check_state *state);
_Bool cycle_check(struct cycle_check_state *state, struct stat const *sb);
                                                            

   

                                             
   

 
                                                        

   

                                        
                           
           
   

 
                                                                           
            

   

                                     
                    
                                    
                                                                               
                         
     
                               

                                                               

                                                                 
                            
         
       

     

                             
                                                        
              
                                        
                          
       
                                                
                                                
     
                      
   

 
extern void error(int __status, int __errnum, char const *__format, ...);
char const *quote(char const *name);
void close_stdout(void);
extern struct _IO_FILE *stdout;
extern struct _IO_FILE *stderr;
extern __attribute__((__noreturn__)) void _exit(int __status);
int close_stream(FILE *stream);
char *quotearg_colon(char const *arg);
                             
                          
                         
                          
                  
                
               
               
              
               
              

   
                                   
                       
                         
                                     
                                
                  
         
              
         
                                                   
                          
                        
                                              
                                       
                                                             
                
                                       
                                                
         
                                 
       
     
                                   
                       
                               
     
           
   
 
void close_stdin(void);
                                 
void close_stdin(void) {
             
          
              
                 
              
                          
                      
                
               
               

   
                    
                                
                        
                                           
                     
                                    
                           
                          
         
       
     
                                  
                       
                      
     
               
                                                            
                            
                          
                                                
                                     
                                                           
              
                                     
                                              
       
     
                   
               
                               
     
           
   
}
extern __attribute__((__nothrow__))
size_t(__attribute__((__leaf__)) __fpending)(FILE *__fp);
                                
                     
             
                  
              
                    
              
               
               

   

                             
                                       
                                      
                                      
                             
                                        
                    
                  
            
                        
                           
                      
                
                                       
                              
                 
                               
                                           
                           
             
                        
           
         
       
     
               
   

 
int set_cloexec_flag(int desc, _Bool value);
                                             
            
          
               
              
              

   

                                
                
                     
                  
                            
              
                             
       
                         
                              
                   
              
                                               
                            
                     
         
       
     
                
   

 
extern __attribute__((__nothrow__)) int(__attribute__((__leaf__))
                                        fchdir)(int __fd);
extern int(__attribute__((__nonnull__(2))) openat)(int __fd, char const *__file,
                                                   int __oflag, ...);
extern __attribute__((__nothrow__)) void *(
    __attribute__((__nonnull__(1), __leaf__))
    memchr)(void const *__s, int __c, size_t __n) __attribute__((__pure__));
extern __attribute__((__nothrow__)) void *(
    __attribute__((__nonnull__(1, 2), __leaf__))
    memmove)(void *__dest, void const *__src, size_t __n);
extern __attribute__((__nothrow__)) int(
    __attribute__((__nonnull__(1, 2), __leaf__))
    lstat)(char const *__restrict __file, struct stat *__restrict __buf);
size_t triple_hash(void const *x, size_t table_size);
_Bool triple_compare_ino_str(void const *x, void const *y);
char *last_component(char const *name) {
  char const *base;
  char const *p;
  _Bool saw_slash;

  {
                    
                         
               

                                        
                         
       
             
     
              
             
              {

                
                             
       
                                 {
        saw_slash = (_Bool)1;
      }       
                        
                   
                               
         
       
      p++;
    }
                  ;
    return ((char *)base);
  }
}
size_t base_len(char const *name) {
  size_t len;
                    

  {
                           
    len = strlen(name);
              {

                      
                                                          
                           
         
              
                         
       
                  
           
       len--;
    }
              ;
    return (len);
  }
}
                                 
                           
ptrdiff_t __xargmatch_internal(char const *context, char const *arg,
                               char const *const *arglist, char const *vallist,
                               size_t valsize, void (*exit_fn)(void));
extern __attribute__((__nothrow__)) int(
    __attribute__((__nonnull__(1, 2), __leaf__))
    memcmp)(void const *__s1, void const *__s2, size_t __n)
    __attribute__((__pure__));
extern int(__attribute__((__nonnull__(1))) closedir)(DIR *__dirp);
extern struct dirent *(__attribute__((__nonnull__(1))) readdir)(DIR *__dirp);
DIR *opendir_safer(char const *name);
                                       
ptrdiff_t argmatch(char const *arg, char const *const *arglist,
                   char const *vallist, size_t valsize);
void argmatch_invalid(char const *context, char const *value,
                      ptrdiff_t problem);
void argmatch_valid(char const *const *arglist, char const *vallist,
                    size_t valsize);
extern int fprintf(FILE *__restrict __stream, char const *__restrict __format,
                   ...);
extern int putc_unlocked(int __c, FILE *__stream);
char *quotearg_n_style(int n, enum quoting_style s, char const *arg);
char const *quote_n(int n, char const *name);
__attribute__((__noreturn__)) void usage(int status);
static void __argmatch_die(void) {

   

             
           
   

}
void (*argmatch_die)(void) = &__argmatch_die;
                                                               
                                                         
           
                
                     
                  
          
                 
              

   
                             
                         
                         
                  
               

                            
                         
       
                                                                   
                     
                                                       
                                
                                
                
                                
                                    
                  
                                                                       
                                   
                    
                                                                                
                                                                           
                        
                                     
               
             
           
         
       
          
     
               
                    
                             
            
                        
     
   
 
                                                             
                                          
                     
            
                
                
                      
                

   
                         
                                                  
                    
            
                                                        
                        
     
                                   
                                  
                                                                
                                          
           
   
 
                                                                    
                                     
           
                       
            
              

   
                                         
                                          
                                       
                  
               

                            
                         
       
                     
                                                       
                                         
              
                                                
                                                                         
                      
                                                         
                                           
                
                                                     
         
       
          
     
              
                                
           
   
 
                                                                    
                                                                               
                                                                       
                
                

   

                                                   
              
                    
                   
     
                                        
                                              
                 
                           
   

 
extern
    __attribute__((__nothrow__)) void *(__attribute__((__warn_unused_result__,
                                                       __leaf__))
                                        realloc)(void *__ptr, size_t __size);
_Bool yesno(void);
extern
    __attribute__((__nothrow__)) int(__attribute__((__nonnull__(1), __leaf__))
                                     rpmatch)(char const *__response);
extern __ssize_t getline(char **__restrict __lineptr, size_t *__restrict __n,
                         FILE *__restrict __stream);
_Bool yesno(void) {
  _Bool yes;
  char *response;
  size_t response_size;
  ssize_t response_len;
  ssize_t tmp;
  int tmp___0;

  {
                                   
                              
    tmp = getline(&response, &response_size, stdin);
                       
                             
                     
           {
                                                       
      tmp___0 = rpmatch((char const *)response);
      yes = (_Bool)(0 < tmp___0);
    }
                           
    return (yes);
  }
}
__inline static void *xnmalloc(size_t n, size_t s) __attribute__((__malloc__));
__inline static void *xnmalloc(size_t n, size_t s) __attribute__((__malloc__));
                                                    
          
                

   
                                              
               
            
               
     
                              
                   
     
                             
                     
   
 
extern
    __attribute__((__nothrow__)) void *(__attribute__((__leaf__))
                                        calloc)(size_t __nmemb, size_t __size)
        __attribute__((__malloc__));
void *xmalloc(size_t n) __attribute__((__malloc__));
void *xmalloc(size_t n) {
  void *p;
  void *tmp;

  {
    tmp = malloc(n);
    p = tmp;
             
                     
                     
       
     
    return (p);
  }
}
                                   

   

                      
             
                     
                     
       
     
               
   

 
__attribute__((__nothrow__))
FTS *(__attribute__((__warn_unused_result__, __leaf__))
      fts_open)(char *const *argv, int options,
                int (*compar)(FTSENT const **, FTSENT const **));
FTS *xfts_open(char *const *argv, int options,
               int (*compar)(FTSENT const **, FTSENT const **));
FTS *xfts_open(char *const *argv, int options,
               int (*compar)(FTSENT const **, FTSENT const **)) {
  FTS *fts;
  FTS *tmp;
               

  {
    tmp = fts_open(argv, options | 512, compar);
    fts = tmp;
                                                           
                                   
                              
                                                     
                                                                               
                                        
       
                   
     
    return (fts);
  }
}
__attribute__((__noreturn__)) void xalloc_die(void);
                       
            

   
                                      
                                           
            
   
 
_Bool can_write_any_file(void);
                         
static _Bool can_write;
                                
            
              

   

                       
                     
                      
                               
                      
                             
     
                       
   

 
extern int printf(char const *__restrict __format, ...);
extern int fputs_unlocked(char const *__restrict __s,
                          FILE *__restrict __stream);
                                     
void version_etc_arn(FILE *stream, char const *command_name,
                     char const *package, char const *version,
                     char const *const *authors, size_t n_authors);
void version_etc_va(FILE *stream, char const *command_name, char const *package,
                    char const *version, va_list authors);
void version_etc(FILE *stream, char const *command_name, char const *package,
                 char const *version, ...) __attribute__((__sentinel__));
                                                            
                                                              
                                                                    
            
                
                
                
                
                
                
                
                
                
                
                 

   
                       
                                                                      
            
                                                   
     
                         
                                                      
             
                                                               
                                                                              
                                                                           
                                                                 
                                                  
                           
                  
     
                           
                  
     
                           
                  
     
                           
                  
     
                           
                  
     
                           
                  
     
                           
                  
     
                           
                  
     
                           
                  
     
                           
                  
     
                        
         
            
         
                                          
                                                           
                      
         
                                                 
                                                                           
                      
         
                                                      
                                                                          
                            
                      
         
                                                           
                                                                          
                                            
                      
         
                                                               
                                                                          
                                                            
                      
         
                                                                   
                                                                          
                                                                            
                      
         
                                                                       
                                                                          
                                                                           
                            
                      
         
                                                                            
                                                                          
                                                                           
                                            
                      
         
                                                                                
                                                                          
                                                                           
                                                            
                      
                 
                       
                                                                          
                                                                           
                                                                           
                                                            
                      
                
           
   
 
                                                                                
                                                           
                   
                          
                  

   
                          
               

                             
                                                      
                                 
                                                                  
                           
         
              
                         
       
                  
     
              
                                                           
                                                               
           
   
 
void version_etc(FILE *stream, char const *command_name, char const *package,
                 char const *version, ...) __attribute__((__sentinel__));
                                                                             
                                            
                  

   
                                         
                                                                    
                              
           
   
 
                                        
                                                                       
                                                                       
                                                                       
                                                                       
                                                                       
                                                                       
                                                                       
                                                                       
                                                                       
                                                                       
                                                                       
                                                          
#pragma weak pthread_key_create
#pragma weak pthread_getspecific
#pragma weak pthread_setspecific
#pragma weak pthread_key_delete
#pragma weak pthread_self
#pragma weak pthread_cancel
                                                    
                  
                  

   
                                                                     
              
                                                           
                                             
            
                      
     
   
 
_Bool strip_trailing_slashes(char *file);
_Bool strip_trailing_slashes(char *file) {
  char *base;
  char *tmp;
  char *base_lim;
  _Bool had_slash;
  size_t tmp___0;

  {
    tmp = last_component((char const *)file);
    base = tmp;
                 
                  
     
    tmp___0 = base_len((char const *)base);
    base_lim = base + tmp___0;
    had_slash = (_Bool)((int)*base_lim != 0);
    *base_lim = (char)'\000';
    return (had_slash);
  }
}
int open_safer(char const *file, int flags, ...);
extern DIR *fdopendir(int __fd);
struct dev_ino *get_root_dev_ino(struct dev_ino *root_d_i);
struct dev_ino *get_root_dev_ino(struct dev_ino *root_d_i) {
  struct stat statbuf;
  int tmp;

  {
    tmp = lstat("/", &statbuf);
             {
      return ((struct dev_ino *)((void *)0));
    }
                                      
                                      
                      
  }
}
                                   
                                        
                                               
int set_char_quoting(struct quoting_options *o, char c, int i);
char *quotearg_char(char const *arg, char ch);
char *quotearg_char_mem(char const *arg, size_t argsize, char ch);
__inline static char *xcharalloc(size_t n) __attribute__((__malloc__));
__inline static char *xcharalloc(size_t n) __attribute__((__malloc__));
                                            
            
                
                

   
                              
                       
                    
            
                                          
                        
     
                             
   
 
extern __attribute__((__nothrow__)) int(__attribute__((__leaf__))
                                        iswprint)(wint_t __wc);
                                           
                                                                 
                                          
                                                  
                                                             
                                                             
                                                             
                                                              
                                                      
                                                                
                   
                  
                              
            
        

   
                          
            
              
            
                                     
     
                                                                       
                                                           
                                  
                                                 
               
   
 
static struct quoting_options
quoting_options_from_style(enum quoting_style style) {
  struct quoting_options o;

  {
    o.style = style;
                
    memset((void *)(o.quote_these_too), 0, sizeof(o.quote_these_too));
    return (o);
  }
}
static char const *gettext_quote(char const *msgid, enum quoting_style s) {
  char const *translation;
  char const *tmp;

  {
    tmp = (char const *)gettext(msgid);
    translation = tmp;
                                                             
                                  
                           
       
     
    return (translation);
  }
}
static size_t
quotearg_buffer_restyled(char *buffer, size_t buffersize, char const *arg,
                         size_t argsize, enum quoting_style quoting_style,
                         int flags, unsigned int const *quote_these_too,
                         char const *left_quote, char const *right_quote) {
  size_t i;
  size_t len;
  char const *quote_string;
  size_t quote_string_len;
                          
                       
             
  _Bool elide_outer_quotes;
  unsigned char c;
                    
                       
              
              
           
                  
                                 
                    
            
               
                 
           
              
              
              
  int tmp___6;
  size_t tmp___7;

  {
    len = (size_t)0;
                                   
                                 
                                 
                                   
                                         
                                                   
                                            
                  
     
                                            
                  
     
                                            
                  
     
    if ((unsigned int)quoting_style == 6U) {
      goto case_6;
    }
                                            
                  
     
                                            
                  
     
                                            
                  
     
                                            
                  
     
                                            
                  
     
                        
         
                                          
                                  
         
                              
                 

                               
                                       
         
              
                         
       
                 
     
                                 
                        
                                 
                      
         
                                 
                                  
    goto switch_break;
  case_6:
                                           {
      left_quote = gettext_quote("`", quoting_style);
      right_quote = gettext_quote("\'", quoting_style);
    }
                             {
      quote_string = left_quote;
                {

                             
                               
         
                  {

                                {
            *(buffer + len) = (char)*quote_string;
          }
          len++;
                               
        }
                      
                        
      }
                    ;
    }
                                 
    quote_string = right_quote;
                                            
                      
         
                                          
                                  
         
                              
                 

                               
                                       
         
              
                             
       
                     
     
                        
                                 
                      
         
                                  
                      
                 
            
  switch_break:
    i = (size_t)0;
    while (1) {

                                           {
        tmp___6 = (int const) * (arg + i) == 0;
      }       
                               
       
      if (tmp___6) {
        goto while_break___3;
      }
                                
                              
                               
                                                
                                                     
                                                                           
                               
                                       
                                               
               
                                        
             
           
         
       
      c = (unsigned char)*(arg + i);
                        
                        
       
                         
                     
       
                        
                        
       
                        
                        
       
                         
                     
       
                         
                     
       
                         
                     
       
                        
                    
       
                         
                     
       
                         
                     
       
                          
                      
       
                          
                      
       
                         
                     
       
                          
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                          
                     
       
                         
                         
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                         
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                          
                     
       
                              
               
                              
                                 
                                         
         
                   

                                 
                                         
           
                
                               
         
                       
                                
                                              
                                                        
                         

                                       
                                              
                 
                      
                                     
               
                             
                         

                                       
                                              
                 
                      
                                     
               
                             
             
           
         
                               
              
                        
                      
         
       
                            
            
                                              
                        
       
                                              
                        
       
                              
               
                               
                                       
       
                            
               
                      
                                
                                                      
                                                        
                           
             
                                                        
                           
             
                                                        
                           
             
                                                        
                           
             
                                                        
                           
             
                                                        
                           
             
                                                        
                           
             
                                                        
                           
             
                                                        
                           
             
                                    
                  
                                     
                                             
             
                                                  
                     
                       

                                     
                                            
               
                    
                                   
             
                           
                       

                                     
                                             
               
                    
                                   
             
                           
                       

                                     
                                             
               
                    
                                   
             
                           
                       

                                     
                                            
               
                    
                                    
             
                            
                                  
                             
                                  
                            
           
         
       
                            
                       
                            
                      
                            
               
                               
                    
               
                               
                    
            
                               
                    
            
                               
                              
            
                               
                              
           
                               
                              
            
                               
                    
            
              
                              
                                 
                                 
                         
           
         
       
                       
                                              
                                 
                                         
         
       
             
                              
                
                          
       
                            
             
                                            
                                               
              
                                 
       
                     
                              
       
            
                     
                              
       
            
                                              
                                 
                                         
         
       
                            
                
                                              
                                 
                                         
         
                   

                                 
                                         
           
                
                                
         
                        
                   

                                 
                                         
           
                
                                
         
                        
                   

                                 
                                         
           
                
                                
         
                        
       
                            
            
                            
                       
                           
                      
                                  
                                                                              
              
                                                       
                      
                             
                                              
                                
         
                   
                                                                            
                          
                             
                                  
                  
                                                
                                   
                                    
                    
                                                  
                                     
                           

                                        
                                            
                                            
                     
                          
                                          
                   
                      
                 
                                
                                      
                      
                                         
                                                          
                                  
                               

                                         
                                              
                       
                                                                      
                                         
                       
                                                                      
                                         
                       
                                                                      
                                         
                       
                                                                      
                                         
                       
                                                                       
                                         
                       
                                              
                                
                                                     
                                       
                                            
                                     
                          
                     
                                    
                   
                 
                                              
                               
                                       
                 
                           
               
             
           
                                                           
                        
                                  
           
         
                        
       
                    
                    
              
                                
                           
                 
                         
                       

                                      
                                 
                                           
                                                   
                   
                             

                                           
                                                   
                     
                          
                                          
                   
                                  
                             

                                           
                                                                   
                     
                          
                                          
                   
                                  
                             

                                           
                                                                         
                     
                          
                                          
                   
                                 
                                                         
                        
                          
                 
                      
                 
                                     
                             

                                           
                                                   
                     
                          
                                          
                   
                                 
                                            
                 
               
                                    
                                      
               
                         

                                       
                                            
                 
                      
                                      
               
                             
                  
                                            
             
                            
                         
           
         
       
                      
                              
                    
              
                                 
               
                                
                                                                               
                                                              
                                                                        
                          
             
                  
                        
           
                
               
                                
                         
           
         
       
                 
                               
                                       
       
                 

                               
                                       
         
              
                              
       
                      
            
                 {

                              {
          *(buffer + len) = (char)c;
        }
        len++;
                              
      }
                     ;
           
       i++;
    }
  while_break___3:;
                     
                                              
                                 
                                         
         
       
     
                      {
                                

                  {

                               
                                  
           
                    {

                                  {
              *(buffer + len) = (char)*quote_string;
            }
            len++;
                                  
          }
                         
                          
        }
                       ;
       

    }
                          {
      *(buffer + len) = (char)'\000';
    }
    return (len);
                            
                                        
                                                                    
                                                                    
                     
  }
}
static char slot0[256];
                                
static struct slotvec slotvec0 = {sizeof(slot0), slot0};
static struct slotvec *slotvec = &slotvec0;
static char *quotearg_n_options(int n, char const *arg, size_t argsize,
                                struct quoting_options const *options) {
        
           
                  
  struct slotvec *sv;
            
                     
              
                          
  size_t size;
  char *val;
  int flags;
               
  size_t tmp___2;
               

  {
                             
             
                         
    sv = slotvec;
                
              
     
                       
                             
                                                                              
                                                
                     
              
                     
       
                                               
                     
       
                         
                                                
              
                     
       
                                                                         
                   
                         
                       
       
                                                                            
                                
     
    size = (sv + n)->size;
    val = (sv + n)->val;
    flags = (int)(options->flags | 1);
    tmp___2 = quotearg_buffer_restyled(
        val, size, arg, argsize, (enum quoting_style)options->style, flags,
        (unsigned int const *)(options->quote_these_too),
        (char const *)options->left_quote, (char const *)options->right_quote);
                    
                        
                         
                            
                                                         
                          
       
                             
                          
                                                       
                                                                         
                                                                                
                                                                 
                                                                   
     
                                 
                 
    return (val);
  }
}
char *quotearg_n_style(int n, enum quoting_style s, char const *arg) {
  struct quoting_options o;
  struct quoting_options tmp;
  char *tmp___0;

  {
    tmp = quoting_options_from_style(s);
    o = tmp;
    tmp___0 = quotearg_n_options(n, arg, (size_t)-1,
                                 (struct quoting_options const *)(&o));
    return (tmp___0);
  }
}
char *quotearg_char_mem(char const *arg, size_t argsize, char ch) {
  struct quoting_options options;
  char *tmp;

  {
                                      
                                      
    tmp = quotearg_n_options(0, arg, argsize,
                             (struct quoting_options const *)(&options));
    return (tmp);
  }
}
char *quotearg_char(char const *arg, char ch) {
  char *tmp;

  {
    tmp = quotearg_char_mem(arg, (size_t)-1, ch);
    return (tmp);
  }
}
                                       
            

   
                                        
                 
   
 
char const *quote_n(int n, char const *name) {
  char const *tmp;

  {
    tmp = (char const *)quotearg_n_style(n, (enum quoting_style)6, name);
    return (tmp);
  }
}
char const *quote(char const *name) {
  char const *tmp;

  {
    tmp = quote_n(0, name);
    return (tmp);
  }
}
                         
void set_program_name(char const *argv0);
                                     
extern char *program_invocation_short_name;
extern int fputs(char const *__restrict __s, FILE *__restrict __stream);
char const *program_name = (char const *)((void *)0);
void set_program_name(char const *argv0) {
  char const *slash;
  char const *base;
  int tmp;
  int tmp___0;

  {
    if ((unsigned long)argv0 == (unsigned long)((void *)0)) {
      fputs("A NULL argv[0] was passed through an exec system call.\n", stderr);
              
    }
    slash = (char const *)strrchr(argv0, '/');
                                                             
                       
            
                   
     
                             
                                                        
                         
                     
                                              
                       
                           
                                                        
         
       
     
    program_name = argv0;
                                            
           
  }
}
extern DIR *(__attribute__((__nonnull__(1))) opendir)(char const *__name);
extern
    __attribute__((__nothrow__)) int(__attribute__((__nonnull__(1), __leaf__))
                                     dirfd)(DIR *__dirp);
DIR *opendir_safer(char const *name) {
  DIR *dp;
  DIR *tmp;
         
              
             
        
        
              
               
               

  {
    tmp = opendir(name);
    dp = tmp;
             
                          
                   
                    
                      
                                  
                      
                               
                                       
                       
                       
                     
           
                       
                                       
                       
                     
         
       
     
    return (dp);
  }
}
int openat_safer(int fd, char const *file, int flags, ...);
int openat_safer(int fd, char const *file, int flags, ...) {
  mode_t mode;
             
  int tmp;
  int tmp___0;

  {
    mode = (mode_t)0;
                     
                                    
                                          
                           
     
    tmp = openat(fd, file, flags, mode);
    tmp___0 = fd_safer(tmp);
    return (tmp___0);
  }
}
int open_safer(char const *file, int flags, ...) {
  mode_t mode;
             
  int tmp;
  int tmp___0;

  {
    mode = (mode_t)0;
                     
                                    
                                          
                           
     
    tmp = open(file, flags, mode);
    tmp___0 = fd_safer(tmp);
    return (tmp___0);
  }
}
                                                                  
                                                                    
                        
                        
          
             
                 
              
              
              
              
              
              
              
               
               
               
               
               
               
               
                          
                          
                   
                   
               
                                  
               
                                  
                  

   
                                                 
                 
     
                                        
                         
                         
                                
                                                                
                                 
                         
                                
                                                                
                                 
                 
                                   
                                 
                                  
                         
                  
                         
           
                
                       
         
                       
                                     
                                   
                                    
                           
                    
                           
             
                  
                         
           
                          
                             
           
                
                           
         
                                 
                                   
                                                 
                                                     
                                              
                  
                         
           
                             
                
                                   
                         
                  
                                                     
                                                           
                                                                             
                                 
                    
                                                      
                                                             
                                                                               
                                  
                              
                        
                               
                 
                                  
                      
                                                             
                                                                               
                                   
                              
                        
                               
                 
                                  
               
                                 
             
                                
           
                              
         
                       
                       
                       
         
                                         
                                   
                                         
                                   
       
                
                                 
                               
                                
                       
                
                       
         
              
                     
       
                     
                   
       
                                 
                               
                                
                       
                
                       
         
              
                     
       
                     
                    
       
                 
            
                                     
                                     
                 
                                   
                                                         
                                       
                                       
                
                                  
         
                                   
                                                         
                                       
                                       
                
                                  
         
                           
                               
         
             
             
                                    
                               
         
       
                     
                                 
     
   
 
                                        
                                                              
                                                                     
extern __attribute__((__nothrow__, __noreturn__)) void(__attribute__((__leaf__))
                                                       exit)(int __status);
extern int optind;
extern __attribute__((__nothrow__)) int(__attribute__((__leaf__)) getopt_long)(
    int ___argc, char *const *___argv, char const *__shortopts,
    struct option const *__longopts, int *__longind);
#pragma weak pthread_mutex_init
#pragma weak pthread_mutex_lock
#pragma weak pthread_mutex_unlock
#pragma weak pthread_mutex_destroy
#pragma weak pthread_rwlock_init
#pragma weak pthread_rwlock_rdlock
#pragma weak pthread_rwlock_wrlock
#pragma weak pthread_rwlock_unlock
#pragma weak pthread_rwlock_destroy
#pragma weak pthread_once
#pragma weak pthread_cond_init
#pragma weak pthread_cond_wait
#pragma weak pthread_cond_signal
#pragma weak pthread_cond_broadcast
#pragma weak pthread_cond_destroy
#pragma weak pthread_mutexattr_init
#pragma weak pthread_mutexattr_settype
#pragma weak pthread_mutexattr_destroy
#pragma weak pthread_self
#pragma weak pthread_cancel
void i_ring_init(I_ring *ir, int default_val);
int i_ring_push(I_ring *ir, int val);
int i_ring_pop(I_ring *ir);
_Bool i_ring_empty(I_ring const *ir);
                                               
        

   
                            
                      
                     
          
               

                     
                         
       
                                   
          
     
              
                                     
           
   
 
_Bool i_ring_empty(I_ring const *ir) {

   

    return ((_Bool)ir->ir_empty);
   

}
                                      
                        
              

   
                                                                   
                                    
                                
                            
                                  
                                                                       
     
                            
                     
   
 
                            
              
            

   
                                           
              
              
     
                                        
                                                   
                                      
                              
            
                                                     
     
                     
   
 
_Bool(__attribute__((__warn_unused_result__))
      hash_rehash)(Hash_table *table___0, size_t candidate);
void *hash_delete(Hash_table *table___0, void const *entry);
__inline static size_t rotr_sz(size_t x, int n) {

   

    return (((x >> n) | (x << (8UL * sizeof(x) - (unsigned long)n))) &
            0xffffffffffffffffUL);
   

}
                                                  
                                                                
                                                                   
                                  
             
                                  
                

   
                                                                      
                                                                  
                                                                            
              
     
                                                                    
                         
     
                    
               

                    
                         
       
                                                                
                                      
              
                                                                                
                      
                                        
         
       
                                                       
     
               
                       
   
 
                                                          
               
                   

   
                      
               
                                  
                
                         
       
                                                      
               
     
               
                   
   
 
                                         
                 
                
          

   
                        
                               
               

                               
                                     
                           
         
              
                         
       
                
                              
                
     
               
                              
              
            
              
     
                        
   
 
                                            
            

   

                           
                             
     
                     
               

                                              
                                  
                  
                           
         
              
                         
       
                       
     
               
                       
   

 
                                                      
             
             

   
                                   
              
                     
   
 
                                                           

   

                                                           
   

 
                                                  
                            
                

   
                               
                                                                    
                        
     
                   
                                                    
                                                                         
                                                                
                                                           
                                                                 
                                        
                                                            
                                                                     
                                               
                                    
                 
               
             
           
         
       
     
                                        
                      
   
 
static size_t compute_bucket_size(size_t candidate, Hash_tuning const *tuning) {
  float new_candidate;
          

  {
                                

                     
                                                                     
                                                        {
        return ((size_t)0);
      }
                                        
     

                                      
                                              
               
            
               
     
                                                                
                         
     
                       
  }
}
Hash_table *(__attribute__((__warn_unused_result__))
             hash_initialize)(size_t candidate, Hash_tuning const *tuning,
                              size_t (*hasher)(void const *, size_t),
                              _Bool (*comparator)(void const *, void const *),
                              void (*data_freer)(void *)) {
  Hash_table *table___0;
            

  {
                                                              
                           
     
                                                                  
                                   
     
                                                         
                                                                 
                                         
     
                  
                               
     
                               
                                  
               
                
     
                                                                  
                                
                
     
                                                    
                                                            
                                                                         
                
     
                             
                                                                              
                                          
                                     
                               
                                       
                                       
    table___0->free_entry_list = (struct hash_entry *)((void *)0);
    return (table___0);
       
                            
                                       
  }
}
                                       
                            
                            
                          

   
                                
                                 
                                   
                   

                                       
                                                          
                             
           
                             
                            
                       

                            
                                     
               
                                                       
                                    
             
                           
           
                   
         
                   
       
     
                               
               

                                                                              
                             
       
                            
                 

                      
                               
         
                            
                             
                      
       
                    
               
     
                  
                                        
               

                    
                             
       
                          
                           
                    
     
                  
                                    
                            
           
   
 
                                                                 
                         

   
                                     
                                       
                                             
            
                                                      
     
                 
   
 
                                                                         

   

                            
                                             
                                       
           
   

 
static void *hash_find_entry(Hash_table *table___0, void const *entry,
                             struct hash_entry **bucket_head, _Bool delete) {
                            
             
                            
             
                          
                
                 
                              
                

   

                                                              
                                     
                                                                            
              
     
                          
                                                                    
                         
     
                                                              
              
            
                                                                              
                    
         
                            
                     
                             
                                
                            
                                        
                  
                                     
           
         
                      
       
     
                    
               

                          
                         
       
                                                                        
                    
              
                                             
                                                       
                      
               
                                          
                       
                                    
                                          
                                            
           
                            
         
       
                            
     
               
    return ((void *)0);
   

}
                                                                             
                            
                            
                          
             
                                
             
                 
                               
                             

   

                         
               

                                                                        
                         
       
                         
                              
                   

                        
                                 
           
                              
                                                                     
                                         
                                                                                
                    
           
                              
                                 
                                            
                                      
                  
                                    
                                    
                                    
           
                        
         
                      
                            
                                                        
                   
                      
         
                                                                       
                                           
                                                                              
                  
         
                               
                                        
                              
                                                                       
                              
           
                                 
                                             
                                       
                
                                  
                                  
         
                                 
                                
       
           
               
     
               
                      
   

 
_Bool(__attribute__((__warn_unused_result__))
      hash_rehash)(Hash_table *table___0, size_t candidate) {
                     
  Hash_table *new_table;
  size_t new_size;
  size_t tmp;
                
                
                

  {
    tmp = compute_bucket_size(candidate, table___0->tuning);
    new_size = tmp;
                    
                        
     
    if (new_size == table___0->n_buckets) {
      return ((_Bool)1);
    }
                         
    new_table->bucket =
        (struct hash_entry *)calloc(new_size, sizeof(*(new_table->bucket)));
                                                                         
                        
     
                                    
                             
                                                                  
                                          
                                     
                                          
                                          
                                                  
                                                  
                                                            
                                                               
                  
                                      
                                            
                                                        
                                                  
                                                            
                                                              
                        
     
                                                            
                                                               
                  
                                                                 
                     
                
       
            
              
     
                                    
                      
  }
}
                                              
                                                               
             
                            
                            
                  
            
                
                
                               
                             

   

                 
              
     
                                                                
                                                            
                    
     
                                                
                                               
                                                
                              
                                                  
                                                 
                                                  
                                   
                                   
                                                           
                                               
                
                                                            
                                                 
                                                  
         
                        
                                                       
                             
         
                                                            
                       
                             
         
                                                                       
                                                                   
                  
         
       
     
                       
                                          
                          
                                                                   
                           
       
                                      
                                     
                               
                               
                             
     
                                 
                             
                                  
                           
   

 
                                                             
             
                            
                            
                   
            
                            
                          
                

   
                                                                
                
                         
     
                             
                        
                                    
                                                  
                                                 
                                                  
                                
                                                    
                                                   
                                                    
                                     
                                     
                                                             
                                                 
                  
                                                              
                                                   
                                                    
           
                                  
                                                      
                         
                                                
                       

                            
                                 
               
                                  
                                   
                            
             
                      
                                                                          
           
         
       
     
                  
   
 
size_t hash_pjw(void const *x, size_t tablesize);
                                                      
                           
             
                 

   
                                   
                                                          
                  
                                                           
   
 
                                                            
                           
                           
              
              

   
                                   
                                   
                                 
                                   
                                                                       
                           
                      
                
                      
         
              
                    
       
            
                  
     
                            
   
 
                           
                     

   
                             
                          
                    
           
   
 
                                                  
                
           

   
                  
                        
               

                
                         
       
                                                                               
          
     
               
                           
   
 
extern __attribute__((__nothrow__)) char *(__attribute__((__leaf__))
                                           setlocale)(int __category,
                                                      char const *__locale);
extern __attribute__((__nothrow__)) int(
    __attribute__((__nonnull__(2, 3), __leaf__))
    fstatat)(int __fd, char const *__restrict __file,
             struct stat *__restrict __buf, int __flag);
__attribute__((__nothrow__)) int(__attribute__((__warn_unused_result__,
                                                __leaf__)) fts_close)(FTS *sp);
__attribute__((__nothrow__))
FTSENT *(__attribute__((__warn_unused_result__, __leaf__)) fts_read)(FTS *sp);
__attribute__((__nothrow__)) int(__attribute__((__leaf__))
                                 fts_set)(FTS *sp __attribute__((__unused__)),
                                          FTSENT *p, int instr);
extern void(__attribute__((__nonnull__(1, 4)))
            qsort)(void *__base, size_t __nmemb, size_t __size,
                   int (*__compar)(void const *, void const *));
static FTSENT *fts_alloc(FTS *sp, char const *name, size_t namelen);
static FTSENT *fts_build(FTS *sp, int type);
static void fts_lfree(FTSENT *head);
static void fts_load(FTS *sp, FTSENT *p);
static size_t fts_maxarglen(char *const *argv);
static void fts_padjust(FTS *sp, FTSENT *head);
static _Bool fts_palloc(FTS *sp, size_t more);
static FTSENT *fts_sort(FTS *sp, FTSENT *head, size_t nitems);
static unsigned short fts_stat(FTS *sp, FTSENT *p, _Bool follow);
static int fts_safe_changedir(FTS *sp, FTSENT *p, int fd, char const *dir);
                                                       
                              
                              
          

   
                                      
                                      
                             
                               

                
               
                
       
            
              
     
                        
   
 
                                                         
                              

   
                                      
                                             
   
 
static _Bool setup_dir(FTS *fts) {

   

                                 
                         
                                                                       
                                                                            
                               
                          
       
            
                            
                                                                              
                                  
                          
       
                                             
     
    return ((_Bool)1);
   

}
                                               
                        
                        
                         
                                   
                

   

                                 
                                                 
                                                     
               
                
                          
       
                                  
                                  
                        
                     
                                                                                
                                                              
                         
                             
                            
         
                                                
                                          
       
            
                                                 
                                                                   
                    
                             
                                          
       
     
                      
   

 
                                              
                        
                        
              
                 

   
                                               
                                 
                                  
                                  
                                                                   
                   
                
       
                  
            
                               
                                                                
                                      
                     

                                                               
                      
             
                                                                              
                                                                                
                                                        
                                                
                                                        
                                                
               
             
                             
           
                     
         
       
     
           
   
 
                               

   

                                
                             
                                    
       
            
                                        
     
           
   

 
                                            
         
          
                

   
               
                                                      
                    
                         
       
                                
               
                    
                  
       
     
               
           
   
 
                                                              

   

               

                                      
                
       
                       
     
               
                   
                                           
            

                                           
     

           
   

 
__inline static DIR *opendirat(int fd, char const *dir) {
  int new_fd;
  int tmp;
  DIR *dirp;
                  
               
               

  {
    tmp = openat_safer(fd, dir, 67840);
    new_fd = tmp;
                     
                                  
     
                                       
    dirp = fdopendir(new_fd);
                                                            
                                   
                             
                    
                                   
                             
     
    return (dirp);
  }
}
static void cwd_advance_fd(FTS *sp, int fd, _Bool chdir_down_one) {
          
                      
          

   

                         
               

                         
                             
                  
         
       
                       
     
               
                         
                                               
                            
                                 
                               
       
            
                                   
                       
                     
         
       
     
    sp->fts_cwd_fd = fd;
           
   

}
                                                             
                 
          
         
              
              
              

   
                               
                   
            
              
     
                             
                                
                                                                   
                        
            
                                            
                        
     
                 
                  
                                     
     
                
   
 
__attribute__((__nothrow__))
FTS *(__attribute__((__warn_unused_result__, __leaf__))
      fts_open)(char *const *argv, int options,
                int (*compar)(FTSENT const **, FTSENT const **));
FTS *(__attribute__((__warn_unused_result__, __leaf__))
      fts_open)(char *const *argv, int options,
                int (*compar)(FTSENT const **, FTSENT const **)) {
  register FTS *sp;
  register FTSENT *p;
  register FTSENT *root;
                         
  FTSENT *parent;
              
                   
               
               
               
  size_t maxarglen;
  size_t tmp___4;
  size_t tmp___5;
  _Bool tmp___6;
              
  size_t len;
  size_t tmp___8;
  struct _ftsent *tmp___9;
                 
               

  {
                                   
                                
                          
                                   
                    
                                  
     
                      
                          
                                     
                      
                                    
       
     
                          
                                   
                    
                                  
     
    sp = (FTS *)malloc(sizeof(FTS));
                                                          
                                  
     
                                       
                            
    sp->fts_options = options;
                              
                           
                              
     
    sp->fts_cwd_fd = -100;
    tmp___4 = fts_maxarglen(argv);
    maxarglen = tmp___4;
                            {
      tmp___5 = maxarglen;
    }       
                             
     
    tmp___6 = fts_palloc(sp, tmp___5);
                   
                
     
                                                            {
      parent = fts_alloc(sp, "", (size_t)0);
                                                                
                  
       
      parent->fts_level = (ptrdiff_t)-1;
    }
                                                              
                  
            
                                   
                    
              
                    
       
     
                                
    root = (FTSENT *)((void *)0);
                       
    while (1) {

      if (!((unsigned long)*argv != (unsigned long)((void *)0))) {
        goto while_break;
      }
      tmp___8 = strlen((char const *)*argv);
      len = tmp___8;
      p = fts_alloc(sp, (char const *)*argv, len);
                                                           
                  
       
                                  
      p->fts_parent = parent;
      p->fts_accpath = p->fts_name;
                      {
                                                                
                                           
                                             
                

          p->fts_info = fts_stat(sp, p, (_Bool)0);
         

      }       
                                                
       
                  {
        p->fts_link = root;
        root = p;
      }       
                                                    
                                                                
                   
                     
                
                            
                  
         
       
      argv++;
               
    }
  while_break:;
                 
                         
                                          
       
     
    tmp___9 = fts_alloc(sp, "", (size_t)0);
    sp->fts_cur = tmp___9;
                                                               
                
     
    (sp->fts_cur)->fts_link = root;
                                                
                             
                    
                
     
                                 
                                     
                                                 
                               
                           
                               
         
       
     
                                      
    return (sp);
       
                     
                         
       
                                
       
                     
                                
  }
}
static void fts_load(FTS *sp, FTSENT *p) {
  register size_t len;
                    
  size_t tmp;
  char *tmp___0;

  {
    tmp = p->fts_namelen;
                         
    len = tmp;
    memmove((void *)sp->fts_path, (void const *)(p->fts_name), len + 1UL);
                                                   
             
                                                              
             
                                       
                                                                    
                             
              
                        
               
                                         
                                                                      
                               
         
       
     
                           
                          
                             
           
  }
}
__attribute__((__nothrow__)) int(__attribute__((__warn_unused_result__,
                                                __leaf__)) fts_close)(FTS *sp);
int(__attribute__((__warn_unused_result__, __leaf__)) fts_close)(FTS *sp) {
                         
                     
  int saved_errno;
           
              
               
              
               
              
               

  {
                    
                      
                      
                 

                                    
                           
         
                  
                                                                       
                          
                
                            
         
                            
       
                
                      
     
                        
                               
     
                                
                               
                                
                                
                                        
                      
                                   
                             
         
       
            
                                   
                                      
                      
                                       
                                 
         
                                     
                      
                                 
                                         
                                   
           
         
       
     
                                    
                                             
                                                    
     
                 
                     
                     {
                                   
                             
      return (-1);
    }
               
  }
}
extern
    __attribute__((__nothrow__)) int(__attribute__((__nonnull__(2), __leaf__))
                                     fstatfs)(int __fildes,
                                              struct statfs *__buf);
                                                          
                       
          

   

                                   
                   
                        
     
                                     
                         
     
                                  
                         
     
                        
                
                      
                 
                      

                      
   

 
static _Bool leaf_optimization_applies(int dir_fd) {
  struct statfs fs_buf;
  int tmp;

  {
    tmp = fstatfs(dir_fd, &fs_buf);
                  {
      return ((_Bool)0);
    }
                                       
                           
     
                        
                  
                      
                 
                      

                      
  }
}
static size_t LCO_hash(void const *x, size_t table_size) {
  struct LCO_ent const *ax;

  {
    ax = (struct LCO_ent const *)x;
    return ((uintmax_t)ax->st_dev % table_size);
  }
}
static _Bool LCO_compare(void const *x, void const *y) {
  struct LCO_ent const *ax;
  struct LCO_ent const *ay;

  {
    ax = (struct LCO_ent const *)x;
    ay = (struct LCO_ent const *)y;
    return ((_Bool)(ax->st_dev == ay->st_dev));
  }
}
static _Bool link_count_optimize_ok(FTSENT const *p) {
  FTS *sp;
  Hash_table *h;
                     
                      
  _Bool opt_ok;
                     
  struct hash_table *tmp___0;

  {
                           
                                           
                                   
                        
     
                                                        {
      tmp___0 =
          hash_initialize((size_t)13, (Hash_tuning const *)((void *)0),
                          &LCO_hash, &LCO_compare, (void (*)(void *))(&free));
      sp->fts_leaf_optimization_works_ht = tmp___0;
      h = tmp___0;
      if ((unsigned long)h == (unsigned long)((void *)0)) {
        return ((_Bool)0);
      }
    }
                                               
                                                              
                                                              
              
                           
     
                                               
                                                          
                        
     
    opt_ok = leaf_optimization_applies(sp->fts_cwd_fd);
                        
                                               
                                                             
                                                           
                       
                        
     
               

                                                       
                
       
                       
     
               
                    
  }
}
__attribute__((__nothrow__))
FTSENT *(__attribute__((__warn_unused_result__, __leaf__)) fts_read)(FTS *sp);
FTSENT *(__attribute__((__warn_unused_result__, __leaf__)) fts_read)(FTS *sp) {
  register FTSENT *p;
  register FTSENT *tmp;
  register unsigned short instr;
  register char *t;
               
              
               
              
  struct _ftsent *tmp___4;
              
              
              
              
              
                
               
  size_t tmp___12;
  char *tmp___13;
  FTSENT *parent;
  _Bool tmp___14;
                
                 
  int *tmp___17;
  struct _ftsent *tmp___18;
                
  int tmp___20;
               
               
               
               
                  
                
                
                
               
               
                
               
  FTSENT *tmp___32;

  {
                                                                   
                                     
            
                                   
                                       
       
     
    p = sp->fts_cur;
                         
                                     
                          
                                              
                 
     
                          
                                   
                
              
                                     
           
                                                  
                                      
                                         
                                                      
                                     
                                
                                             
                                        
                                                
                      
                                                                       
               
             
           
                             
         
       
     
    if ((int)p->fts_info == 1) {
                            
                    
              
                                   
                                                      
                 
                                        
                                  
             
                                
                                       
                                                            
             
                                            
                       
                               
                               
             
                       
                       
           
         
       
                                                                       
                                     
                                   
                                   
                                                        
         
       
                                                                       
                                                                              
                      
                                       
                                  
                                                                 
                            
                     

                                                                    
                                   
             
                                                          
                            
           
                         
         
             {
        tmp___4 = fts_build(sp, 3);
        sp->fts_child = tmp___4;
        if ((unsigned long)tmp___4 == (unsigned long)((void *)0)) {
                                       
                                           
           
                             
                                        
                                              
             
           
                     
                             
                                 
           
                         
          return (p);
        }
      }
      p = sp->fts_child;
                                                    
      goto name;
    }
       
     tmp = p;
    p = p->fts_link;
    if ((unsigned long)p != (unsigned long)((void *)0)) {
                      
                        
      if (p->fts_level == 0L) {
                                        
                                     
                                      
                                        
                             
                    
                                    
             
                                                  
                        
                  
                                        
                             
                    
                                    
             
                                      
                              
           
                        
                        
                  
                        
           
                
                      
         
                      
                                  
                                         
         
                     
        fts_load(sp, p);
        setup_dir(sp);
        goto check_for_dir;
      }
                                   
                  
       
                                   
                                                
                                    
                                       
                                                     
                                    
                               
                                            
                                       
                                              
                    
                                                                     
             
           
         
                                         
       
    name:
                                            
                                                               
                                                      
             {
        tmp___12 = (p->fts_parent)->fts_pathlen;
      }
      t = sp->fts_path + tmp___12;
      tmp___13 = t;
      t++;
      *tmp___13 = (char)'/';
      memmove((void *)t, (void const *)(p->fts_name), p->fts_namelen + 1UL);
    check_for_dir:
      sp->fts_cur = p;
                                  {
        if (p->fts_statp[0].st_size == 2L) {
                                 
                                 {
                                                      

                                        

                                           

                  tmp___14 = link_count_optimize_ok((FTSENT const *)parent);
                                  
                                
                   
                         
                              
                 
                       
                            
               
                     
                          
             
          }       
                 
                                                     
                                                               
                                       

                                                   

                                                   
                 

               

             
           
        } else {
                     

                                                   
                      
             
                                 
           
                         
        }
      }
                                  
                                 
                                               
         
                                    
                        
                                        
                         
                                         
         
       
      return (p);
    }
    p = tmp->fts_parent;
    sp->fts_cur = p;
                      
    if (p->fts_level == -1L) {
                      
                                    
                    
      tmp___18 = (struct _ftsent *)((void *)0);
                             
      return (tmp___18);
    }
               

                                      
                
       
                           
     
                  
     *(sp->fts_path + p->fts_pathlen) = (char)'\000';
                            {
                                      
                                   

                                    

                                     {
            tmp___20 = -100;
          }       
                                   
           
          cwd_advance_fd(sp, tmp___20, (_Bool)1);
                       
                 
                                      
                            
                  
                                   
           
                                      
                              
         
                       
                       
                
                       
         
               
                     
       
                     
                                      
                                 
                                
       
    }       
                                  
                                     
                                      
                                                       
                         
                  
                                            
                                
           
                         
                                          
                                    
                                
                                          
                                    
                                          
                                     
                                    
           
         
                            
              
                                       
                                                                     
                         
                                          
                                     
                                    
           
         
       
     
                       
                                      
           {
      p->fts_info = (unsigned short)6;
    }
                            
                 
                         
                             
       
                     
     
                                 
                                       
           {
      tmp___32 = p;
    }
    return (tmp___32);
  }
}
__attribute__((__nothrow__)) int(__attribute__((__leaf__))
                                 fts_set)(FTS *sp __attribute__((__unused__)),
                                          FTSENT *p, int instr);
                                                                           
                                                              
           

   

                     
                       
                         
                           
                             
                                       
                        
                         
             
           
         
       
     
                                         
               
   

 
                                                                                
          
              

   
                                                                            
                   
            
                                                                              
                
              
                
       
                    
     
                     
   
 
                                                                
              

   
                      
                  
     
                      
                  
     
                      
                  
     
                      
                  
     
                       
                   
     
                      
                  
     
                       
                   
     
                        
         
                         
                      
         
                        
                      
         
                         
                      
         
                        
                      
          
                         
                      
         
                         
                      
          
                         
                      
                 
                     
               
                       
           
   
 
static FTSENT *fts_build(FTS *sp, int type) {
  register struct dirent *dp;
  register FTSENT *p;
  register FTSENT *head;
  register size_t nitems;
  FTSENT *cur;
  FTSENT *tail;
  DIR *dirp;
                
                  
                
                 
  ptrdiff_t level;
                 
  _Bool nostat;
  size_t len;
  size_t maxlen;
                 
           
           
  DIR *tmp___0;
               
               
                
              
  int dir_fd;
  int tmp___5;
               
  int tmp___7;
                
               
  size_t tmp___9;
                
                
                  
                 
  size_t tmp___14;
                  
                
                  
               
               
               
               
               
               
               
               
               
               
                 

  {
    cur = sp->fts_cur;
                                {
                                  

        tmp___0 = opendirat(sp->fts_cwd_fd, (char const *)cur->fts_accpath);
        dirp = tmp___0;
               
                                                                
                       
       
    }       
                                                              
                     
     
                                                            
                      
                                          
                                 
                              
       
                                     
     
                                   
                                                  
            
                                  
                   
                             
                           
         
                  
                                    
                                     
                       
                                       
                        
                                         
         
       
     
                    
                          
                        
            
                                
                                   
                                     
                        
                  
                        
           
                                                                   
                            
                
                               
                            
         
              
                             
                          
       
     
                 
                  
           {
                      

             
         tmp___5 = dirfd(dirp);
        dir_fd = tmp___5;
                                    
                            
                                       
                                               
           
         
                         
                  
               {
          tmp___7 =
              fts_safe_changedir(sp, cur, dir_fd, (char const *)((void *)0));
                        
             
                         
                              
                                             
                                          
               
             
                                                                       
                               
                           
                                        
                                
                              
               
             
                                      
                  
                               
           
        }
               
                           
       
    }
                                                                 
                                   
            
                             
     
                              
                              
                   
           
                           
            
                               
     
    len++;
                                   
    level = cur->fts_level + 1L;
                        
    tail = (FTSENT *)((void *)0);
    head = tail;
                       
    while (1) {

                {
        dp = readdir(dirp);
        if (!dp) {
          goto while_break___0;
        }
      }       
                             
       
                                   {
                                       

                               
                        
                 {
                                           

              if (!dp->d_name[2]) {
                goto __Cont;
              }
             

          }
         

      }
      tmp___9 = strlen((char const *)(dp->d_name));
      p = fts_alloc(sp, (char const *)(dp->d_name), tmp___9);
                                                           
                  
       
                                                    
                               
                                       
                                                      
                                                          
                        
             
                                        
                                  
                          
                          
                         
                                            
                                  
                                        
                                  
                                         
         
                                                                    
                              
                                    
                                    
           
         
                                       
       
                                                    
                               
                          
                        
                        
                       
                                          
                                
                                      
                       
                                       
       
      p->fts_level = level;
      p->fts_parent = sp->fts_cur;
                               
                                         
                                
                                     
                                                                               
             {
        p->fts_accpath = p->fts_name;
      }
                                                                        
                    
             {
                                     

               
                                     
                                      
                                         
                                              
                               
                        
                               
                 
                      
                             
               
                    
                           
             
                  
                         
           
                                      
          p->fts_info = (unsigned short)11;
                                                                
                                                        
                                     
                                       
                           
                    
                           
             
                  
                         
           
                                   
                 
                                                  
                                      
                         
                  
                                        
                           
                    
                                          
                             
                      
                             
               
             
           
                                   
         
      }
                         
                     
                                    
         
       
                                                  
      if ((unsigned long)head == (unsigned long)((void *)0)) {
        tail = p;
        head = tail;
      } else {
        tail->fts_link = p;
        tail = p;
      }
      nitems++;
    __Cont:;
    }
  while_break___0:;
               
                     
     
                   
                            
     
                              
                                   
             
              
                            
               
         
       
                         
     
                  
                      
                    
              
                      
               
                                     
                                            
                                         
                                          
                                            
                                  
                        
                                         
                 
                                                       
                             
                      
                                            
                                  
                        
                                         
                 
                                            
                                    
               
                             
                             
                      
                             
               
                    
                           
             
                                
                  
                                                                         
                                
           
                         
                                              
                                    
                            
                                           
           
         
       
     
                 {
                      

        cur->fts_info = (unsigned short)6;
       

                      
                                     
    }
                           
                            
                                    
                                                                     
                         
                                              
                                              
                                                                                                    
           
         
       
     
    if (sp->fts_compar) {
                         

        head = fts_sort(sp, head, nitems);
       

    }
    return (head);
  }
}
static unsigned short fts_stat(FTS *sp, FTSENT *p, _Bool follow) {
  struct stat *sbp;
                  
           
               
               
              
  int tmp___3;
               
  int tmp___5;
              
              

  {
    sbp = p->fts_statp;
                             
                                
                          
       
     
                              
              
           {
                  {
         
         tmp___3 = stat((char const *)p->fts_accpath, sbp);
                      
                                   
                             
                                       
                              
                                                               
                               
                                           
                           
                                          
             
           
                                     
                   
         
      }       
                 
                                                                            
                      
                                       
                                  
            
                                                      
                                      
         
       
    }
    if ((sbp->st_mode & 61440U) == 16384U) {
                                 
                    
              
                    
       
                                                                   
                                      
                              
                      
                
                                          
                                  
                   
                                       
                            
                      
                            
               
                                               
             
           
         
       
      return ((unsigned short)1);
    }
                                            
                                  
     
                                            
                                 
     
    return ((unsigned short)3);
  }
}
                                                     
                    
                    
          

   
                            
                            
                                                          
                 
   
 
static FTSENT *fts_sort(FTS *sp, FTSENT *head, size_t nitems) {
                       
                     
                    
  int (*compare)(void const *, void const *);
  int (*tmp)(void const *, void const *);
  FTSENT **a;
                   

  {
                                          {
                                                       

        tmp = (int (*)(void const *, void const *))sp->fts_compar;
               
                          
       
    }       
                        
     
    compare = tmp;
                                 {
                                     
                                                               
                                    
                                                       
                                   
                      
              

        a = (FTSENT **)realloc((void *)sp->fts_array,
                               sp->fts_nitems * sizeof(*a));
        if (!a) {
                                      
                                                         
                                     
          return (head);
        }
       

                        
    }
                       
             
               

               
                         
       
                   
           
                   
                      
     
              
     qsort((void *)sp->fts_array, nitems, sizeof(FTSENT *), compare);
                       
               
               
               
                    
                             
       
                                        
           
     
                  
                                                          
                  
  }
}
static FTSENT *fts_alloc(FTS *sp, char const *name, size_t namelen) {
  register FTSENT *p;
  size_t len;

  {
    len = sizeof(FTSENT) + namelen;
    p = (FTSENT *)malloc(len);
                                                         
                                     
     
    memmove((void *)(p->fts_name), (void const *)name, namelen);
                                        
    p->fts_namelen = namelen;
                    
    p->fts_path = sp->fts_path;
                     
                                     
                                     
                       
                               
    return (p);
  }
}
                                     
                     

   
               
               
               
                         
       
                            
                      
     
               
           
   
 
static _Bool fts_palloc(FTS *sp, size_t more) {
  char *p;
                 
           

  {
                                               
                                    
                                 
                                         
                               
                
                        
     
                              
    p = (char *)realloc((void *)sp->fts_path, sp->fts_pathlen);
                                                         
                                 
                                         
                        
     
    sp->fts_path = p;
    return ((_Bool)1);
  }
}
                                                
            
             

   
                        
                      
               

               
                         
       
                 

                                                                            
                                                                 
         
                           
                             
       
                    
                      
     
              
             
               

                                  
                             
       
                 

                                                                            
                                                                 
         
                           
                             
       
                     
                        
                        
              
                          
       
     
                   
           
   
 
static size_t fts_maxarglen(char *const *argv) {
  size_t len;
  size_t max;

  {
                    
              {

                   
                         
       
      len = strlen((char const *)*argv);
                     {
        max = len;
      }
             
    }
              ;
    return (max + 1UL);
  }
}
static int fts_safe_changedir(FTS *sp, FTSENT *p, int fd, char const *dir) {
  int ret;
  _Bool is_dotdot;
          
              
  int newfd;
                
  _Bool tmp___1;
  struct stat sb;
  int tmp___2;
               
  int tmp___4;
             
               
               

  {
              
                              
                     
                    
              
                    
       
            
                  
     
                               
                              
                                  
                      
                    
         
       
                 
     
                {
                      

                                    

                                                                     
                        {
                                                     
            is_dotdot = (_Bool)1;
                                 
                             
                                              
             
          }
         

       

    }
    newfd = fd;
                 
                                            
                      
                    
       
     
                              
              
           {
      if (dir) {
        tmp___4 = strcmp(dir, "..");
                          {
           
           tmp___2 = fstat(newfd, &sb);
                        
                     
                      
           
                                                    
                                         
                         
                     
                      
                  
                                                      
                                           
                           
                       
                        
             
           
        }
      }
    }
    if (sp->fts_options & 512) {
      cwd_advance_fd(sp, newfd, (_Bool)(!is_dotdot));
      return (0);
    }
    ret = fchdir(newfd);
       
                  
                                   
                        
                   
                                   
                        
     
                 
  }
}
extern int fseeko(FILE *__stream, __off_t __off, int __whence);
extern __attribute__((__nothrow__))
__off_t(__attribute__((__leaf__)) lseek)(int __fd, __off_t __offset,
                                         int __whence);
                                                                       
                                                             
            
            
              
                
              

   
                                                                             
                                             
                                              

                                                                             

                            
                              

                             
                     
                           
             
                  
                         
           
                               
                                                
                        
                           
                        
           
                            
                            
                                 
                         
             
                  
                       
           
         

       

     
                                         
                     
   
 
#pragma weak pthread_key_create
#pragma weak pthread_getspecific
#pragma weak pthread_setspecific
#pragma weak pthread_key_delete
#pragma weak pthread_self
#pragma weak pthread_cancel
#pragma weak pthread_mutex_init
#pragma weak pthread_mutex_lock
#pragma weak pthread_mutex_unlock
#pragma weak pthread_mutex_destroy
#pragma weak pthread_rwlock_init
#pragma weak pthread_rwlock_rdlock
#pragma weak pthread_rwlock_wrlock
#pragma weak pthread_rwlock_unlock
#pragma weak pthread_rwlock_destroy
#pragma weak pthread_once
#pragma weak pthread_cond_init
#pragma weak pthread_cond_wait
#pragma weak pthread_cond_signal
#pragma weak pthread_cond_broadcast
#pragma weak pthread_cond_destroy
#pragma weak pthread_mutexattr_init
#pragma weak pthread_mutexattr_settype
#pragma weak pthread_mutexattr_destroy
#pragma weak pthread_self
#pragma weak pthread_cancel
                            
extern
    __attribute__((__nothrow__)) int(__attribute__((__nonnull__(1), __leaf__))
                                     euidaccess)(char const *__name,
                                                 int __type);
extern
    __attribute__((__nothrow__)) int(__attribute__((__nonnull__(2), __leaf__))
                                     faccessat)(int __fd, char const *__file,
                                                int __type, int __flag);
extern
    __attribute__((__nothrow__)) int(__attribute__((__nonnull__(2), __leaf__))
                                     unlinkat)(int __fd, char const *__name,
                                               int __flag);
                                                                        
          

   
                                     
                 
   
 
                                                                
           
          

   
                                                  

                                   
                                                                     
                 
                
              
                             
                  
                

                  
         

       
                          
             
                        
     
   
 
                                     
                                            
                          
                           
                

   
               

                                                 
               
                                                            
                    
              
                                                            
                       
                      
         
       
     


                                      
   
 
__inline static _Bool is_empty_dir(int fd_cwd, char const *dir) {
  DIR *dirp;
                          
                  
  int fd;
  int tmp;
               
               
              

  {
    tmp = openat(fd_cwd, dir, 198912);
    fd = tmp;
    if (fd < 0) {
      return ((_Bool)0);
    }
    dirp = fdopendir(fd);
                                                            
                
                        
     
                                 
                 
                                               
                                 
                           
    closedir(dirp);
                                                          
                        
     
                           
                  
            
                  
     
                            
  }
}
enum RM_status rm(char *const *file, struct rm_options const *x);
static int cache_fstatat(int fd, char const *file, struct stat *st, int flag) {
           
  int tmp___0;
               

  {
                            {
      tmp___0 = fstatat(fd, file, st, flag);
                         
                                  
                                 
                                   
       
    }
                           {
      return (0);
    }
                                 
                               
                
  }
}
                                                               

   

                              
                
   

 
static int write_protected_non_symlink(int fd_cwd, char const *file,
                                       char const *full_name,
                                       struct stat *buf___1) {
            
              
  int tmp___1;
                       
                 
                
              
  int tmp___5;
               
               

  {
                               
              
                 
     
                                                        
                       
                  
     
                                                
                 
     
    tmp___1 = faccessat(fd_cwd, file, 2, 512);
    if (tmp___1 == 0) {
      return (0);
    }
                                
                            
                                  
                                                                 
                    
                    
              
                    
       
                       
     
    tmp___5 = euidaccess(full_name, 2);
                       
                 
     
                                 
                         
                                   
                   
                 
     
                
  }
}
static enum RM_status prompt(FTS const *fts, FTSENT const *ent, _Bool is_dir,
                             struct rm_options const *x,
                             enum Prompt_action mode, Ternary *is_empty_p) {
  int fd_cwd;
  char const *full_name;
  char const *filename;
  struct stat st;
  struct stat *sbuf;
  int dirent_type;
          
  int write_protected;
               
               
               
  int tmp___2;
  char const *quoted_name;
  char const *tmp___3;
                
  _Bool is_empty;
                
  char *tmp___6;
  char *tmp___7;
                
               
  int tmp___10;
  char const *tmp___11;
                 
  char *tmp___13;
  char *tmp___14;
  _Bool tmp___15;

  {
    fd_cwd = (int)fts->fts_cwd_fd;
    full_name = (char const *)ent->fts_path;
    filename = (char const *)ent->fts_accpath;
                     
                               
     
    sbuf = &st;
                          
                 
              
            
              
     
                      
                        
                          
                                 
     
                                                   
                                 
     
                 
                                  {
                                                     
                
              

                           

           
                                   

            write_protected =
                write_protected_non_symlink(fd_cwd, filename, full_name, sbuf);
                                         
                                
           

         

       

    }
                          
                  
           {
      if ((unsigned int const)x->interactive == 3U) {
             
                                   {
                                 

                                                                 
                              {
                                                       
                                 
                      

                                                         

                  dirent_type = 4;
                 

               

            }       
                                   
                                           
                                  
             
           

        }
                                   
                                  
                         
           
                                 
                        
           
                            
                
                                                         
                                       
           
                            
               
                              
                                 
                          
           
                            
                      
         
        tmp___3 = quote(full_name);
        quoted_name = tmp___3;
                                  
                                                
                                                                 
                                     
         
                        {
          is_empty = is_empty_dir(fd_cwd, filename);
                         
                                     
                  
                                     
           
        }       
                              
         
        if (dirent_type == 4) {
          if ((unsigned int)mode == 2U) {
                            

                                    
                         
                                                                               
                                  
                     {
                tmp___6 = gettext("%s: descend into directory %s? ");
                tmp___7 = tmp___6;
              }
              fprintf(stderr, (char const *)tmp___7, program_name, quoted_name);
                     
                          
             
          } else {
            goto _L___1;
          }
        } else {
        _L___1:
          tmp___10 = cache_fstatat(fd_cwd, filename, sbuf, 256);
                              
                                                  
                                         
                                                                   
                                       
           
          tmp___11 = file_type((struct stat const *)sbuf);
                                
                                                                     
                                
                 {
            tmp___13 = gettext("%s: remove %s %s? ");
            tmp___14 = tmp___13;
          }
          fprintf(stderr, (char const *)tmp___14, program_name, tmp___11,
                  quoted_name);
        }
        tmp___15 = yesno();
        if (!tmp___15) {
          return ((enum RM_status)3);
        }
      }
    }
    return ((enum RM_status)2);
  }
}
__inline static _Bool nonexistent_file_errno(int errnum) {

   

    if (errnum == 2) {
      goto case_2;
    }
                       
                  
     
    goto switch_default;
  case_2:
    return ((_Bool)1);
  switch_default:
    return ((_Bool)0);

                      
   

}
__inline static _Bool ignorable_missing(struct rm_options const *x,
                                        int errnum) {
  _Bool tmp;
  int tmp___0;

  {
                                 {
      tmp = nonexistent_file_errno(errnum);
      if (tmp) {
        tmp___0 = 1;
      } else {
                    
      }
    }       
                  
     
    return ((_Bool)tmp___0);
  }
}
                                                  

   

                         
                        
           
   

 
                                             
            

   
                        
               

                                  
                         
       
                          
                         
       
                         
                        
     
               
           
   
 
static enum RM_status excise(FTS *fts, FTSENT *ent, struct rm_options const *x,
                             _Bool is_dir) {
  int flag;
  int tmp;
                      
                
                
                
  int tmp___4;
                 
               
              
               
               
  int *tmp___9;
  _Bool tmp___10;
                
  char const *tmp___12;
  char *tmp___13;
  int *tmp___14;

  {
    if (is_dir) {
      tmp = 512;
    } else {
      tmp = 0;
    }
    flag = tmp;
    tmp___4 = unlinkat(fts->fts_cwd_fd, (char const *)ent->fts_accpath, flag);
    if (tmp___4 == 0) {
                       
                                                     
                     
                                                       
                            
                
                                            
                            
         
                                               
       
      return ((enum RM_status)2);
    }
                                 
                         
                                                                              
                    
                                     
                               
                                       
                        
         
              
                                     
                      
       
     
    tmp___9 = __errno_location();
    tmp___10 = ignorable_missing(x, *tmp___9);
    if (tmp___10) {
      return ((enum RM_status)2);
    }
                                  
                                    
                                 
     
    tmp___12 = quote((char const *)ent->fts_path);
    tmp___13 = gettext("cannot remove %s");
    tmp___14 = __errno_location();
    error(0, *tmp___14, (char const *)tmp___13, tmp___12);
                            
    return ((enum RM_status)4);
  }
}
static enum RM_status rm_fts(FTS *fts, FTSENT *ent,
                             struct rm_options const *x) {
  char const *tmp;
  char *tmp___0;
  _Bool tmp___1;
                      
                
                
                
                      
                
                      
                      
                 
               
                 
  Ternary is_empty_directory;
  enum RM_status s;
  enum RM_status tmp___13;
                       
                 
  _Bool is_dir;
  int tmp___16;
  enum RM_status s___0;
  enum RM_status tmp___17;
  enum RM_status tmp___18;
                       
                 
                       
                 
  char const *tmp___23;
                 

  {
    if ((int)ent->fts_info == 1) {
      goto case_1;
    }
                                  
                  
     
                                   
                  
     
                                   
                  
     
                                   
                  
     
    if ((int)ent->fts_info == 6) {
      goto case_8;
    }
                                  
                  
     
    if ((int)ent->fts_info == 11) {
      goto case_8;
    }
    if ((int)ent->fts_info == 3) {
      goto case_8;
    }
                                  
                  
     
                                  
                  
     
    goto switch_default;
  case_1:
    if (!x->recursive) {
      tmp = quote((char const *)ent->fts_path);
      tmp___0 = gettext("cannot remove %s");
      error(0, 21, (char const *)tmp___0, tmp);
                              
                              
      return ((enum RM_status)4);
    }
                              {
      tmp___1 = strip_trailing_slashes(ent->fts_path);
                   {
        ent->fts_pathlen = strlen((char const *)ent->fts_path);
      }
                                                               
                                                     
                    
                                                     
                                                         
                                                    
                                
                                   
       
                            
                                                                    
                                                                      
                       
                                                                  
                                  
                                                             
                         
                                                                            
                                                            
                      
                                          
                                                                  
                                                                               
                                                      
                                                                      
               
                        
                                                                              
                                                  
                               
             
                      
                                    
                                       
           
         
       
    }
    tmp___13 = prompt((FTS const *)fts, (FTSENT const *)ent, (_Bool)1, x,
                      (enum Prompt_action)2, &is_empty_directory);
    s = tmp___13;
                                
                                                   
                                          
                                
       
     
                                
                              
                              
     
    return (s);
  case_8:
                                 {
                               
                                  
                                                         
                                    
                                                          
                      
                                                                          
                                                          
                                       
           
         
       
    }
    if ((int)ent->fts_info == 6) {
      tmp___16 = 1;
    } else {
                                    
                     
              

        tmp___16 = 0;
       

    }
    is_dir = (_Bool)tmp___16;
    tmp___17 = prompt((FTS const *)fts, (FTSENT const *)ent, is_dir, x,
                      (enum Prompt_action)3, (Ternary *)((void *)0));
    s___0 = tmp___17;
    if ((unsigned int)s___0 != 2U) {
      return (s___0);
    }
    tmp___18 = excise(fts, ent, x, is_dir);
    return (tmp___18);
         
               
                                                    
                         
                                                                                
                                                                       
                                                                             
                                                    
                           
     
                  
                            
                               
         
                                                  
                                               
                                                               
                            
                               
  switch_default:
    tmp___23 = quote((char const *)ent->fts_path);
              
                                                                            
                                                                     
                                   
    abort();

                               
  }
}
enum RM_status rm(char *const *file, struct rm_options const *x) {
  enum RM_status rm_status;
  int bit_flags;
  FTS *fts;
  FTS *tmp;
  FTSENT *ent;
                
               
               
  enum RM_status s;
  enum RM_status tmp___3;
                
               
  int tmp___6;

  {
    rm_status = (enum RM_status)2;
               {
                      
                              {
        bit_flags |= 64;
      }
      tmp = xfts_open(file, bit_flags,
                      (int (*)(FTSENT const **, FTSENT const **))((void *)0));
      fts = tmp;
      while (1) {
        ent = fts_read(fts);
        if ((unsigned long)ent == (unsigned long)((void *)0)) {
                                       
                              
                                                 
                                         
                                                      
                                          
           
          goto while_break;
        }
        tmp___3 = rm_fts(fts, ent, x);
        s = tmp___3;
                                       
                                         
                                           
                            
                                                                       
                              
                                                                             
                              
             
           
         
                  {

          if ((unsigned int)s == 4U) {
            rm_status = s;
          } else {
                                        
                                                  
                              
               
             
          }
                               
        }
                      ;
      }
    while_break:
      tmp___6 = fts_close(fts);
                         
                                              
                                     
                                                  
                                      
       
    }
    return (rm_status);
  }
}
                    
extern __attribute__((__nothrow__)) int(__attribute__((__leaf__))
                                        isatty)(int __fd);
extern
    __attribute__((__nothrow__)) int(__attribute__((__nonnull__(1), __leaf__))
                                     atexit)(void (*__func)(void));
extern
    __attribute__((__nothrow__)) char *(__attribute__((__leaf__))
                                        textdomain)(char const *__domainname);
extern __attribute__((__nothrow__)) char *(__attribute__((
    __leaf__)) bindtextdomain)(char const *__domainname, char const *__dirname);
                                                
            
                
                
                
                          
                      
                
                
              
                
                

   
                                       
                                                  
                                                                
                                                                           
                                                                
                      
                                                                            
                                                  
                                                                    
                          
                      
                                                       
                    
                                               
                                                          
                                                                     
                                               
       
     
                                           
                      
                                                                               
                                           
           
   
 
                                                   

   

                
   

 
static struct option const long_opts[12] = {
    {"directory", 0, (int *)((void *)0), 'd'},
    {"force", 0, (int *)((void *)0), 'f'},
    {"interactive", 2, (int *)((void *)0), 128},
    {"one-file-system", 0, (int *)((void *)0), 129},
    {"no-preserve-root", 0, (int *)((void *)0), 130},
    {"preserve-root", 0, (int *)((void *)0), 131},
    {"-presume-input-tty", 0, (int *)((void *)0), 132},
    {"recursive", 0, (int *)((void *)0), 'r'},
    {"verbose", 0, (int *)((void *)0), 'v'},
    {"help", 0, (int *)((void *)0), -130},
    {"version", 0, (int *)((void *)0), -131},
    {(char const *)((void *)0), 0, (int *)((void *)0), 0}};
                                                
                                                                               
                                                           
                                                                   
                                                                   
                                                                    
static void diagnose_leading_hyphen(int argc, char **argv) {
  int i;
  char const *arg;
                 
  char const *tmp;
  char *tmp___0;
  char *tmp___1;
              

  {
    i = 1;
              {

                        
                         
       
      arg = (char const *)*(argv + i);
                                         {
                         

                                    
                             

            tmp = quote(arg);
            tmp___0 = quotearg_n_style(1, (enum quoting_style)1, arg);
            tmp___1 = gettext("Try `%s ./%s\' to remove the file %s.\n");
            fprintf(stderr, (char const *)tmp___1, *(argv + 0), tmp___0, tmp);
                             
           

         

      }
          
    }
              ;
           
  }
}
__attribute__((__noreturn__)) void usage(int status);
void usage(int status) {
  char *tmp;
  char *tmp___0;
  char *tmp___1;
                
                
                
                
                
                
                
                

  {
    if (status != 0) {
      tmp = gettext("Try `%s --help\' for more information.\n");
      fprintf(stderr, (char const *)tmp, program_name);
    } else {
      tmp___0 = gettext("Usage: %s [OPTION]... FILE...\n");
      printf((char const *)tmp___0, program_name);
      tmp___1 = gettext("Remove (unlink) the FILE(s).\n\n  -f, --force         "
                        "  ignore nonexistent files, never prompt\n  -i        "
                        "            prompt before every removal\n");
      fputs_unlocked((char const *)tmp___1, stdout);
                        
                                                                                
                                                                             
                                                                           
                                                                                
                                                                                
                                                                
                                                    
               
                                                                       
                                                                               
                                                                                
                                                                              
                                                    
                        
                                                                         
                                                                      
                                                                               
                                                                
                                                    
                                                                         
                                                    
               
                                                                            
                                                    
               
                                                                           
                                                                         
                                                                       
                                                    
                                                                                
                                                                                
                                                 
                                                                
                                                                            
                                                                            
                                                                              
                                                                             
                                                    
                            
    }
    exit(status);
  }
}
static void rm_option_init(struct rm_options *x) {

   

                                       
                                            
                                  
                            
                                                    
    x->stdin_tty = (_Bool)isatty(0);
                          
                                      
           
   

}
                                  
int main(int argc, char **argv) {
                      
  struct rm_options x;
  _Bool prompt_once;
  int c;
        
                
                
                      
                
               
                 
  char **file;
                
  char *tmp___5;
  char *tmp___6;
                
  enum RM_status status;
  enum RM_status tmp___8;
  int tmp___9;

  {
                             
                           
    set_program_name((char const *)*(argv + 0));
    setlocale(6, "");
    bindtextdomain("coreutils", "/usr/local/share/locale");
    textdomain("coreutils");
    atexit(&close_stdin);
    rm_option_init(&x);
                              
    while (1) {
      c = getopt_long(argc, (char *const *)argv, "dfirvIR", long_opts,
                      (int *)((void *)0));
      if (!(c != -1)) {
        goto while_break;
      }
      if (c == 100) {
        goto case_100;
      }
      if (c == 102) {
        goto case_102;
      }
      if (c == 105) {
        goto case_105;
      }
      if (c == 73) {
        goto case_73;
      }
      if (c == 114) {
        goto case_114;
      }
                    
                      
       
                     
                      
       
                     
                      
       
                     
                      
       
                     
                      
       
                     
                      
       
                     
                      
       
                      
                          
       
                      
                          
       
      goto switch_default;
    case_100:
      goto switch_break;
    case_102:
      x.interactive = (enum rm_interactive)5;
                                        
                             
      goto switch_break;
    case_105:
      x.interactive = (enum rm_interactive)3;
                                        
                             
      goto switch_break;
    case_73:
      x.interactive = (enum rm_interactive)5;
                                        
      prompt_once = (_Bool)1;
                        
    case_114:
      x.recursive = (_Bool)1;
                        
             
                   
                                                                         
                                                    
                                                                     
                                                                               
                                        
              
              
       
                   
                    
       
                   
                    
       
                   
                    
       
                            
           
                                             
                             
                            
           
                                             
                                        
                             
                            
           
                                             
                                        
                             
                            
                      
                        
             
                                   
                        
             
                               
                        
             
                               
                        
             
                             
                        
             
                           
                        
                 
               
      goto switch_break;
                 
                                                                       
                                                                           
                                       
              
                        
    switch_default:
      diagnose_leading_hyphen(argc, argv);
      usage(1);
    switch_break:;
    }
  while_break:;
                         
                                   
                
              
                                             
                                           
                 
       
     
                      
                          
                                                        
                                                                          
                               
                                                              
                                       
                                                             
         
       
     
                                      
    file = argv + optind;
    if (prompt_once) {
                        
                
              

                            

           
                            
                                                                        
                              
                  {
            tmp___5 = gettext("%s: remove all arguments? ");
            tmp___6 = tmp___5;
          }
          fprintf(stderr, (char const *)tmp___6, program_name);
                            
                         
                    
           
         

       

    }
    tmp___8 = rm((char *const *)file, (struct rm_options const *)(&x));
    status = tmp___8;
    if (!((unsigned int)status == 2U)) {
                                          

        if (!((unsigned int)status == 4U)) {
          __assert_fail("((status) == RM_OK || (status) == RM_USER_DECLINED || "
                        "(status) == RM_ERROR)",
                        "/home/khheo/project/benchmark/coreutils-8.4/src/rm.c",
                        352U, "main");
        }
       

    }
    if ((unsigned int)status == 4U) {
      tmp___9 = 1;
    } else {
                  
    }
    exit(tmp___9);
  }
}
