Command line used to find this crash:

/home/qxin6/afl-2.52b/afl-fuzz -i /home/qxin6/debaug_expt/debaug/benchmark/flex-2.5.4/aflfuzz/robust_testing/chiselnosan/fuzz_byfile/30m/n10train/5/afl-in -o /home/qxin6/debaug_expt/debaug/benchmark/flex-2.5.4/aflfuzz/robust_testing/chiselnosan/fuzz_byfile/30m/n10train/5/afl-out /home/qxin6/debaug_expt/debaug/benchmark/flex-2.5.4/aflfuzz/robust_testing/chiselnosan/fuzz_byfile/30m/n10train/5/flex-2.5.4 error -dl -Crem @@

If you can't reproduce a bug outside of afl-fuzz, be sure to set the same
memory limit. The limit used for this fuzzing session was 50.0 MB.

Need a tool to minimize test cases before investigating the crashes or sending
them to a vendor? Check out the afl-tmin that comes with the fuzzer!

Found any cool bugs in open-source tools using afl-fuzz? If yes, please drop
me a mail at <lcamtuf@coredump.cx> once the issues are fixed - I'd love to
add your finds to the gallery at:

  http://lcamtuf.coredump.cx/afl/

Thanks :-)
