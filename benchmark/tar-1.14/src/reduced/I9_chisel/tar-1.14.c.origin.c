typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __off_t;
typedef long __off64_t;
typedef int __pid_t;
typedef long __time_t;
typedef int __clockid_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __ssize_t;
typedef long __syscall_slong_t;
typedef __mode_t mode_t;
typedef __off_t off_t;
typedef __pid_t pid_t;
typedef __ssize_t ssize_t;
typedef __time_t time_t;
typedef __clockid_t clockid_t;
typedef unsigned long size_t;
struct timespec {
  __time_t tv_sec;
  __syscall_slong_t tv_nsec;
};
struct stat {
  __dev_t st_dev;
  __ino_t st_ino;
  __nlink_t st_nlink;
  __mode_t st_mode;
  __uid_t st_uid;
  __gid_t st_gid;
  int __pad0;
  __dev_t st_rdev;
  __off_t st_size;
  __blksize_t st_blksize;
  __blkcnt_t st_blocks;
  struct timespec st_atim;
  struct timespec st_mtim;
  struct timespec st_ctim;
  __syscall_slong_t __glibc_reserved[3];
};
struct mtop {
  short mt_op;
  int mt_count;
};
struct _IO_FILE;
struct _IO_FILE;
typedef struct _IO_FILE FILE;
typedef void _IO_lock_t;
struct _IO_marker {
  struct _IO_marker *_next;
  struct _IO_FILE *_sbuf;
  int _pos;
};
struct _IO_FILE {
  int _flags;
  char *_IO_read_ptr;
  char *_IO_read_end;
  char *_IO_read_base;
  char *_IO_write_base;
  char *_IO_write_ptr;
  char *_IO_write_end;
  char *_IO_buf_base;
  char *_IO_buf_end;
  char *_IO_save_base;
  char *_IO_backup_base;
  char *_IO_save_end;
  struct _IO_marker *_markers;
  struct _IO_FILE *_chain;
  int _fileno;
  int _flags2;
  __off_t _old_offset;
  unsigned short _cur_column;
  signed char _vtable_offset;
  char _shortbuf[1];
  _IO_lock_t *_lock;
  __off64_t _offset;
  void *__pad1;
  void *__pad2;
  void *__pad3;
  void *__pad4;
  size_t __pad5;
  int _mode;
  char _unused2[(15UL * sizeof(int) - 4UL * sizeof(void *)) - sizeof(size_t)];
};
typedef unsigned long uintmax_t;
struct posix_header {
  char name[100];
  char mode[8];
  char uid[8];
  char gid[8];
  char size[12];
  char mtime[12];
  char chksum[8];
  char typeflag;
  char linkname[100];
  char magic[6];
  char version[2];
  char uname[32];
  char gname[32];
  char devmajor[8];
  char devminor[8];
  char prefix[155];
};
struct sparse {
  char offset[12];
  char numbytes[12];
};
struct sparse_header {
  struct sparse sp[21];
  char isextended;
};
struct oldgnu_header {
  char unused_pad1[345];
  char atime[12];
  char ctime[12];
  char offset[12];
  char longnames[4];
  char unused_pad2;
  struct sparse sp[4];
  char isextended;
  char realsize[12];
};
struct star_header {
  char name[100];
  char mode[8];
  char uid[8];
  char gid[8];
  char size[12];
  char mtime[12];
  char chksum[8];
  char typeflag;
  char linkname[100];
  char magic[6];
  char version[2];
  char uname[32];
  char gname[32];
  char devmajor[8];
  char devminor[8];
  char prefix[131];
  char atime[12];
  char ctime[12];
};
struct star_in_header {
  char fill[345];
  char prefix[1];
  char fill2;
  char fill3[8];
  char isextended;
  struct sparse sp[4];
  char realsize[12];
  char offset[12];
  char atime[12];
  char ctime[12];
  char mfill[8];
  char xmagic[4];
};
struct star_ext_header {
  struct sparse sp[21];
  char isextended;
};
struct sp_array {
  off_t offset;
  size_t numbytes;
};
struct tar_stat_info {
  char *orig_file_name;
  char *file_name;
  int had_trailing_slash;
  char *link_name;
  unsigned int devminor;
  unsigned int devmajor;
  char *uname;
  char *gname;
  struct stat stat;
  unsigned long atime_nsec;
  unsigned long mtime_nsec;
  unsigned long ctime_nsec;
  off_t archive_file_size;
  _Bool is_sparse;
  size_t sparse_map_avail;
  size_t sparse_map_size;
  struct sp_array *sparse_map;
};
union block {
  char buffer[512];
  struct posix_header header;
  struct star_header star_header;
  struct oldgnu_header oldgnu_header;
  struct sparse_header sparse_header;
  struct star_in_header star_in_header;
  struct star_ext_header star_ext_header;
};
typedef double tarlong;
enum subcommand {
  UNKNOWN_SUBCOMMAND = 0,
  APPEND_SUBCOMMAND = 1,
  CAT_SUBCOMMAND = 2,
  CREATE_SUBCOMMAND = 3,
  DELETE_SUBCOMMAND = 4,
  DIFF_SUBCOMMAND = 5,
  EXTRACT_SUBCOMMAND = 6,
  LIST_SUBCOMMAND = 7,
  UPDATE_SUBCOMMAND = 8
};
enum access_mode {
  ACCESS_READ = 0, ACCESS_WRITE = 1, ACCESS_UPDATE = 2
};
typedef __dev_t dev_t;
typedef __builtin_va_list __gnuc_va_list;
typedef __gnuc_va_list va_list;
struct utimbuf {
  __time_t actime;
  __time_t modtime;
};
enum archive_format {
  DEFAULT_FORMAT = 0,
  V7_FORMAT = 1,
  OLDGNU_FORMAT = 2,
  USTAR_FORMAT = 3,
  POSIX_FORMAT = 4,
  STAR_FORMAT = 5,
  GNU_FORMAT = 6
};
enum read_header {
  HEADER_STILL_UNREAD = 0,
  HEADER_SUCCESS = 1,
  HEADER_SUCCESS_EXTENDED = 2,
  HEADER_ZERO_BLOCK = 3,
  HEADER_END_OF_FILE = 4,
  HEADER_FAILURE = 5
};
typedef __ino_t ino_t;
typedef __gid_t gid_t;
typedef __uid_t uid_t;
struct obstack;
struct obstack;
struct mode_change {
  char op;
  char flags;
  mode_t affected;
  mode_t value;
  struct mode_change *next;
};
struct name {
  struct name *next;
  size_t length;
  uintmax_t found_count;
  int isdir;
  char firstch;
  char regexp;
  int change_dir;
  char const *dir_contents;
  char fake;
  char name[1];
};
enum dump_status {
  dump_status_ok = 0,
  dump_status_short = 1,
  dump_status_fail = 2,
  dump_status_not_implemented = 3
};
struct xheader {
  struct obstack *stk;
  size_t size;
  char *buffer;
};
struct hash_tuning {
  float shrink_threshold;
  float shrink_factor;
  float growth_threshold;
  float growth_factor;
  _Bool is_n_buckets;
};
typedef struct hash_tuning Hash_tuning;
struct hash_table;
struct hash_table;
typedef struct hash_table Hash_table;
struct link {
  dev_t dev;
  ino_t ino;
  size_t nlink;
  char name[1];
};
enum old_files {
  DEFAULT_OLD_FILES = 0,
  NO_OVERWRITE_DIR_OLD_FILES = 1,
  OVERWRITE_OLD_FILES = 2,
  UNLINK_FIRST_OLD_FILES = 3,
  KEEP_OLD_FILES = 4,
  KEEP_NEWER_FILES = 5
};
enum remove_option {
  ORDINARY_REMOVE_OPTION = 0,
  RECURSIVE_REMOVE_OPTION = 1,
  WANT_DIRECTORY_REMOVE_OPTION = 2
};
enum permstatus {
  UNKNOWN_PERMSTATUS = 0,
  ARCHIVED_PERMSTATUS = 1,
  INTERDIR_PERMSTATUS = 2
};
struct delayed_set_stat {
  struct delayed_set_stat *next;
  struct stat stat_info;
  size_t file_name_len;
  mode_t invert_permissions;
  enum permstatus permstatus;
  _Bool after_symlinks;
  char file_name[1];
};
struct string_list;
struct string_list;
struct delayed_symlink {
  struct delayed_symlink *next;
  dev_t dev;
  ino_t ino;
  time_t mtime;
  uid_t uid;
  gid_t gid;
  struct string_list *sources;
  char target[1];
};
struct string_list {
  struct string_list *next;
  char string[1];
};
enum strtol_error {
  LONGINT_OK = 0,
  LONGINT_OVERFLOW = 1,
  LONGINT_INVALID_SUFFIX_CHAR = 2,
  LONGINT_INVALID_SUFFIX_CHAR_WITH_OVERFLOW = 3,
  LONGINT_INVALID = 4
};
typedef enum strtol_error strtol_error;
struct _obstack_chunk {
  char *limit;
  struct _obstack_chunk *prev;
  char contents[4];
};
struct obstack {
  long chunk_size;
  struct _obstack_chunk *chunk;
  char *object_base;
  char *next_free;
  char *chunk_limit;
  long temp;
  int alignment_mask;
  struct _obstack_chunk *(*chunkfun)(void *, long);
  void (*freefun)(void *, struct _obstack_chunk *);
  void *extra_arg;
  unsigned int use_extra_arg : 1;
  unsigned int maybe_empty_object : 1;
  unsigned int alloc_failed : 1;
};
struct keyword_list {
  struct keyword_list *next;
  char *pattern;
  char *value;
};
struct xhdr_tab {
  char const *keyword;
  void (*coder)(struct tar_stat_info const *, char const *, struct xheader *,
                void *data);
  void (*decoder)(struct tar_stat_info *, char const *);
  _Bool protect;
};
enum children {
  NO_CHILDREN = 0, CHANGED_CHILDREN = 1, ALL_CHILDREN = 2
};
struct directory {
  dev_t device_number;
  ino_t inode_number;
  enum children children;
  _Bool nfs;
  _Bool found;
  char name[1];
};
struct tm {
  int tm_sec;
  int tm_min;
  int tm_hour;
  int tm_mday;
  int tm_mon;
  int tm_year;
  int tm_wday;
  int tm_yday;
  int tm_isdst;
  long tm_gmtoff;
  char const *tm_zone;
};
enum quoting_style {
  literal_quoting_style = 0,
  shell_quoting_style = 1,
  shell_always_quoting_style = 2,
  c_quoting_style = 3,
  escape_quoting_style = 4,
  locale_quoting_style = 5,
  clocale_quoting_style = 6
};
struct quoting_options;
struct quoting_options;
enum backup_type {
  none = 0, simple = 1, numbered_existing = 2, numbered = 3
};
struct saved_cwd {
  int desc;
  char *name;
};
struct wd {
  char const *name;
  int saved;
  struct saved_cwd saved_cwd;
};
struct passwd {
  char *pw_name;
  char *pw_passwd;
  __uid_t pw_uid;
  __gid_t pw_gid;
  char *pw_gecos;
  char *pw_dir;
  char *pw_shell;
};
struct group {
  char *gr_name;
  char *gr_passwd;
  __gid_t gr_gid;
  char **gr_mem;
};
struct exclude;
struct exclude;
typedef int __daddr_t;
typedef void (*__sighandler_t)(int);
struct mtget {
  long mt_type;
  long mt_resid;
  long mt_dsreg;
  long mt_gstat;
  long mt_erreg;
  __daddr_t mt_fileno;
  __daddr_t mt_blkno;
};
struct tar_sparse_file;
struct tar_sparse_file;
enum sparse_scan_state {
  scan_begin = 0, scan_block = 1, scan_end = 2
};
struct tar_sparse_optab {
  _Bool (*init)(struct tar_sparse_file *);
  _Bool (*done)(struct tar_sparse_file *);
  _Bool (*sparse_member_p)(struct tar_sparse_file *);
  _Bool (*dump_header)(struct tar_sparse_file *);
  _Bool (*fixup_header)(struct tar_sparse_file *);
  _Bool (*decode_header)(struct tar_sparse_file *);
  _Bool (*scan_block)(struct tar_sparse_file *, enum sparse_scan_state, void *);
  _Bool (*dump_region)(struct tar_sparse_file *, size_t);
  _Bool (*extract_region)(struct tar_sparse_file *, size_t);
};
struct tar_sparse_file {
  int fd;
  size_t dumped_size;
  struct tar_stat_info *stat_info;
  struct tar_sparse_optab *optab;
  void *closure;
};
enum oldgnu_add_status {
  add_ok = 0, add_finish = 1, add_fail = 2
};
union __anonunion_56 {
  int __in;
  int __i;
};
union __anonunion_57 {
  int __in;
  int __i;
};
union __anonunion_58 {
  int __in;
  int __i;
};
union __anonunion_59 {
  int __in;
  int __i;
};
union __anonunion_60 {
  int __in;
  int __i;
};
union __anonunion_61 {
  int __in;
  int __i;
};
union __anonunion_62 {
  int __in;
  int __i;
};
union __anonunion_63 {
  int __in;
  int __i;
};
union __anonunion_64 {
  int __in;
  int __i;
};
union __anonunion_65 {
  int __in;
  int __i;
};
union __anonunion_66 {
  int __in;
  int __i;
};
union __anonunion_67 {
  int __in;
  int __i;
};
struct option {
  char const *name;
  int has_arg;
  int *flag;
  int val;
};
struct fmttab {
  char const *name;
  enum archive_format fmt;
};
typedef void *iconv_t;
struct dirent {
  __ino_t d_ino;
  __off_t d_off;
  unsigned short d_reclen;
  unsigned char d_type;
  char d_name[256];
};
struct __dirstream;
struct __dirstream;
typedef struct __dirstream DIR;
struct patopts {
  char const *pattern;
  int options;
};
struct exclude {
  struct patopts *exclude;
  size_t exclude_alloc;
  size_t exclude_count;
};
struct __anonstruct_textint_27 {
  long value;
  size_t digits;
};
typedef struct __anonstruct_textint_27 textint;
struct __anonstruct_table_28 {
  char const *name;
  int type;
  int value;
};
typedef struct __anonstruct_table_28 table;
struct __anonstruct_parser_control_31 {
  char const *input;
  long day_ordinal;
  int day_number;
  int local_isdst;
  long time_zone;
  int meridian;
  textint year;
  long month;
  long day;
  long hour;
  long minutes;
  struct timespec seconds;
  long rel_year;
  long rel_month;
  long rel_day;
  long rel_hour;
  long rel_minutes;
  long rel_seconds;
  long rel_ns;
  _Bool timespec_seen;
  size_t dates_seen;
  size_t days_seen;
  size_t local_zones_seen;
  size_t rels_seen;
  size_t times_seen;
  size_t zones_seen;
  table local_time_zone_table[3];
};
typedef struct __anonstruct_parser_control_31 parser_control;
union __anonunion_YYSTYPE_32 {
  long intval;
  textint textintval;
  struct timespec timespec;
};
typedef union __anonunion_YYSTYPE_32 YYSTYPE;
typedef long __suseconds_t;
struct timeval {
  __time_t tv_sec;
  __suseconds_t tv_usec;
};
struct timezone {
  int tz_minuteswest;
  int tz_dsttime;
};
typedef struct timezone *__restrict __timezone_ptr_t;
struct hash_entry {
  void *data;
  struct hash_entry *next;
};
typedef long ptrdiff_t;
struct hash_table {
  struct hash_entry *bucket;
  struct hash_entry const *bucket_limit;
  size_t n_buckets;
  size_t n_buckets_used;
  size_t n_entries;
  Hash_tuning const *tuning;
  size_t (*hasher)(void const *, size_t);
  _Bool (*comparator)(void const *, void const *);
  void (*data_freer)(void *);
  struct hash_entry *free_entry_list;
};
struct lconv {
  char *decimal_point;
  char *thousands_sep;
  char *grouping;
  char *int_curr_symbol;
  char *currency_symbol;
  char *mon_decimal_point;
  char *mon_thousands_sep;
  char *mon_grouping;
  char *positive_sign;
  char *negative_sign;
  char int_frac_digits;
  char frac_digits;
  char p_cs_precedes;
  char p_sep_by_space;
  char n_cs_precedes;
  char n_sep_by_space;
  char p_sign_posn;
  char n_sign_posn;
  char int_p_cs_precedes;
  char int_p_sep_by_space;
  char int_n_cs_precedes;
  char int_n_sep_by_space;
  char int_p_sign_posn;
  char int_n_sign_posn;
};
typedef int wchar_t;
union __anonunion___value_23 {
  unsigned int __wch;
  char __wchb[4];
};
struct __anonstruct___mbstate_t_22 {
  int __count;
  union __anonunion___value_23 __value;
};
typedef struct __anonstruct___mbstate_t_22 __mbstate_t;
typedef unsigned int wint_t;
typedef __mbstate_t mbstate_t;
struct quoting_options {
  enum quoting_style style;
  int quote_these_too[255UL / (sizeof(int) * 8UL) + 1UL];
};
struct slotvec {
  size_t size;
  char *val;
};
/* #pragma merger("0","000.buffer.o.i","") */
extern __attribute__((__nothrow__)) void *(
    __attribute__((__nonnull__(1, 2), __leaf__))
    memcpy)(void *__restrict __dest, void const *__restrict __src, size_t __n);
extern
    __attribute__((__nothrow__)) void *(__attribute__((__nonnull__(1),
                                                       __leaf__))
                                        memset)(void *__s, int __c, size_t __n);
extern __attribute__((__nothrow__)) void *(
    __attribute__((__nonnull__(1), __leaf__))
    memchr)(void const *__s, int __c, size_t __n) __attribute__((__pure__));
extern __attribute__((__nothrow__)) char *(
    __attribute__((__nonnull__(1, 2), __leaf__))
    strcpy)(char *__restrict __dest, char const *__restrict __src);
extern __attribute__((__nothrow__)) char *(
    __attribute__((__nonnull__(1, 2), __leaf__))
    strcat)(char *__restrict __dest, char const *__restrict __src);
extern __attribute__((__nothrow__)) int(
    __attribute__((__nonnull__(1, 2), __leaf__))
    strcmp)(char const *__s1, char const *__s2) __attribute__((__pure__));
extern __attribute__((__nothrow__)) char *(__attribute__((__nonnull__(1),
                                                          __leaf__))
                                           strchr)(char const *__s, int __c)
    __attribute__((__pure__));
extern __attribute__((__nothrow__))
size_t(__attribute__((__nonnull__(1), __leaf__)) strlen)(char const *__s)
    __attribute__((__pure__));
extern __attribute__((__nothrow__)) int *(
    __attribute__((__leaf__)) __errno_location)(void)__attribute__((__const__));
extern int(__attribute__((__nonnull__(1))) open)(char const *__file,
                                                 int __oflag, ...);
extern int(__attribute__((__nonnull__(1))) creat)(char const *__file,
                                                  mode_t __mode);
extern __attribute__((__nothrow__))
__off_t(__attribute__((__leaf__)) lseek)(int __fd, __off_t __offset,
                                         int __whence);
extern int close(int __fd);
extern __attribute__((__nothrow__)) int(__attribute__((__leaf__))
                                        ioctl)(int __fd,
                                               unsigned long __request, ...);
extern __attribute__((__nothrow__)) void(__attribute__((__leaf__))
                                         free)(void *__ptr);
extern __attribute__((__nothrow__)) void *(__attribute__((__leaf__))
                                           valloc)(size_t __size)
    __attribute__((__malloc__));
extern __attribute__((__nothrow__, __noreturn__)) void(__attribute__((__leaf__))
                                                       abort)(void);
extern int system(char const *__command);
extern struct _IO_FILE *stdin;
extern struct _IO_FILE *stdout;
extern struct _IO_FILE *stderr;
extern int fclose(FILE *__stream);
extern int fflush_unlocked(FILE *__stream);
extern FILE *fopen(char const *__restrict __filename,
                   char const *__restrict __modes);
extern int fprintf(FILE *__restrict __stream, char const *__restrict __format,
                   ...);
extern __attribute__((__nothrow__)) int
sprintf(char *__restrict __s, char const *__restrict __format, ...);
extern int fscanf(FILE *__restrict __stream, char const *__restrict __format,
                  ...);
extern int fputc_unlocked(int __c, FILE *__stream);
extern char *fgets_unlocked(char *__restrict __s, int __n,
                            FILE *__restrict __stream);
extern __attribute__((__nothrow__)) int(__attribute__((__leaf__))
                                        ferror_unlocked)(FILE *__stream);
extern __attribute__((__nothrow__))
time_t(__attribute__((__leaf__)) time)(time_t *__timer);
extern __attribute__((__nothrow__)) int(__attribute__((__leaf__))
                                        clock_gettime)(clockid_t __clock_id,
                                                       struct timespec *__tp);
int strip_trailing_slashes(char *path);
extern void error(int __status, int __errnum, char const *__format, ...);
void *xmalloc(size_t n);
char *xstrdup(char const *string);
extern __attribute__((__nothrow__)) char *(__attribute__((__leaf__))
                                           gettext)(char const *__msgid)
    __attribute__((__format_arg__(1)));
extern __attribute__((__nothrow__)) char *(__attribute__((__leaf__))
                                           ngettext)(char const *__msgid1,
                                                     char const *__msgid2,
                                                     unsigned long __n)
    __attribute__((__format_arg__(1), __format_arg__(2)));
extern int fnmatch(char const *__pattern, char const *__name, int __flags);
char *human_readable(uintmax_t n, char *buf, int opts,
                     uintmax_t from_block_size, uintmax_t to_block_size);
char *quotearg_colon(char const *arg);
char const *quote_n(int n, char const *name);
char const *quote(char const *name);
size_t safe_read(int fd, void *buf, size_t count);
enum subcommand subcommand_option;
int blocking_factor;
size_t record_size;
_Bool backup_option;
_Bool checkpoint_option;
char const *use_compress_program_option;
_Bool force_local_option;
char const *info_script_option;
_Bool multi_volume_option;
_Bool read_full_records_option;
char const *rsh_command_option;
tarlong tape_length_option;
_Bool to_stdout_option;
_Bool totals_option;
int verbose_option;
_Bool verify_option;
char const *volno_file_option;
char const *volume_label_option;
int archive;
_Bool dev_null_output;
struct timespec start_timespec;
struct tar_stat_info current_stat_info;
char const **archive_name_array;
int archive_names;
char const **archive_name_cursor;
char const *index_file_name;
enum access_mode access_mode;
FILE *stdlis;
char *save_name;
off_t save_sizeleft;
off_t save_totsize;
_Bool write_archive_to_stdout;
size_t available_space_after(union block *pointer);
off_t current_block_ordinal(void);
void close_archive(void);
void closeout_volume_number(void);
union block *find_next_block(void);
void flush_read(void);
void flush_write(void);
void flush_archive(void);
void init_volume_number(void);
void open_archive(enum access_mode wanted_access);
void print_total_written(void);
void reset_eof(void);
void set_next_block_after(union block *block);
void clear_read_error_count(void);
void xclose(int fd);
__attribute__((__noreturn__)) void archive_write_error(ssize_t status);
void archive_read_error(void);
void finish_header(struct tar_stat_info *st, union block *header,
                   off_t block_ordinal);
void off_to_chars(off_t v, char *p, size_t s);
void time_to_chars(time_t v, char *p, size_t s);
_Bool now_verifying;
void verify_volume(void);
__attribute__((__noreturn__)) void fatal_exit(void);
char *stringify_uintmax_t_backwards(uintmax_t o___0, char *buf);
off_t off_from_header(char const *p, size_t s);
uintmax_t uintmax_from_header(char const *p, size_t s);
void assign_string(char **string, char const *value);
_Bool maybe_backup_file(char const *path, int this_is_the_archive);
void undo_last_backup(void);
void close_error(char const *name);
void close_warn(char const *name);
void open_error(char const *name);
__attribute__((__noreturn__)) void open_fatal(char const *name);
void open_warn(char const *name);
void read_error(char const *name);
void write_error(char const *name);
__attribute__((__noreturn__)) void
write_fatal_details(char const *name, ssize_t status, size_t size);
char *safer_name_suffix(char const *file_name, _Bool link_target);
void tar_stat_destroy(struct tar_stat_info *st);
char *output_start;
void sys_detect_dev_null_output(void);
void sys_save_archive_dev_ino(void);
void sys_drain_input_pipe(void);
void sys_wait_for_child(pid_t child_pid___0);
void sys_spawn_shell(void);
pid_t sys_child_open_for_compress(void);
pid_t sys_child_open_for_uncompress(void);
size_t sys_write_archive_buffer(void);
_Bool sys_get_archive_stat(void);
char *rmt_path__;
int rmt_open__(char const *path, int open_mode, int bias,
               char const *remote_shell);
int rmt_close__(int handle);
size_t rmt_read__(int handle, char *buffer___2, size_t length);
off_t rmt_lseek__(int handle, off_t offset, int whence);
int rmt_ioctl__(int handle, int operation, char *argument);
static tarlong prev_written;
static tarlong bytes_written;
union block *record_start;
union block *record_end;
union block *current_block;
off_t records_read;
off_t records_written;
static off_t record_start_block;
static void backspace_output(void);
static _Bool new_volume(enum access_mode mode);
static pid_t child_pid;
static int read_error_count;
static int hit_eof;
static int checkpoint;
_Bool time_to_start_writing;
static int volno = 1;
static int global_volno = 1;
static char *real_s_name;
static off_t real_s_totsize;
static off_t real_s_sizeleft;
void clear_read_error_count(void) {

   

                         
           
   

}
void print_total_written(void) {
  tarlong written;
  char bytes[sizeof(tarlong) * 8UL];
  char abbr[((((((2UL * sizeof(uintmax_t)) * 8UL) * 302UL) / 1000UL + 1UL) *
                  17UL -
              16UL) +
             3UL) +
            1UL];
  char rate[((((((2UL * sizeof(uintmax_t)) * 8UL) * 302UL) / 1000UL + 1UL) *
                  17UL -
              16UL) +
             3UL) +
            1UL];
  double seconds;
  int human_opts;
  struct timespec now;
  time_t tmp;
  int tmp___0;
  char *tmp___1;
  char const *tmp___2;
  char *tmp___3;
  char *tmp___4;
                    
                    
                    
                    
                    
                    
                    

   
     
                                             
                       
                                       
     
                       
                                                              
                                                                     
            
       

                                
                                                        
       

     
     
                                                
                                                                
     
                              
                                                              
         

                                                                        
                                                                           
                                          
         

              
                      
       
            
                    
     
     
                                                                    
                                                           
                                                                
                                                
                                                                                
     
           
   
}
off_t current_block_ordinal(void) {

   

    return (record_start_block + (current_block - record_start));
   

}
void reset_eof(void) {

   

                  
                  
                                   
                                                  
                                        
     
           
   

}
union block *find_next_block(void) {

   

    if ((unsigned long)current_block == (unsigned long)record_end) {
                    
                                  
       
      {
	flush_archive();
      }
                                                                      
                    
                                  
       
    }
    return (current_block);
   

}
void set_next_block_after(union block *block) {

   

    {
      while (1) {
                                     ;
        if (!((unsigned long)block >= (unsigned long)current_block)) {
          goto while_break;
        }
        current_block++;
      }
    while_break: /* CIL Label */;
    }
                                                                   
       

                
       

     
           
   

}
size_t available_space_after(union block *pointer) {

   

    return ((size_t)(record_end->buffer - pointer->buffer));
   

}
void xclose(int fd) {
  char *tmp;
  int tmp___0;
                   

   
     
                          
     
                       
       

                                
                                       
       

     
           
   
}
static _Bool check_label_pattern(union block *label) {
  char *string;
  _Bool result;
  void *tmp;
  int tmp___0;
  size_t tmp___1;
  void *tmp___2;
  int tmp___3;
                   
                    

  {
     
                                                              
                                               
     
               
                        
     
     
               
                                                                              
     
                       
                        
     
                              {
      return ((_Bool)0);
    }
     
                                            
                                                                    
                               
                                              
                                                                  
                                              
                                                                
               
                                                                               
                                     
                           
     
                    
  }
}
void open_archive(enum access_mode wanted_access) {
  int backed_up_flag;
  char *tmp;
  char *tmp___0;
  void *tmp___1;
  void *tmp___2;
  char *tmp___3;
  int tmp___4;
  char *tmp___5;
  int tmp___7;
  int tmp___8;
  void *tmp___9;
  int tmp___11;
  int tmp___12;
  void *tmp___13;
  int tmp___15;
  int tmp___16;
  void *tmp___17;
  int tmp___19;
  int tmp___20;
  void *tmp___21;
  int tmp___22;
  int saved_errno;
  int *tmp___23;
  int *tmp___24;
  _Bool tmp___25;
  union block *label;
  union block *tmp___26;
  char const *tmp___27;
  char *tmp___28;
  char const *tmp___29;
  char const *tmp___30;
  char *tmp___31;
  _Bool tmp___32;
  unsigned int tmp___33;
                    
                    
                    
                    
                    
                    
                    

  {
                       
                          
       
                                                                       
                                                             
       
                    
         

                                      
         

       
           {
                            {
        stdlis = stderr;
      }       
                        
       
    }
                             
       

                                                       
                                       
                     
       

     
                             
       

                                                   
                                           
                     
       

     
     
                                           
                            
                              
     
                              
       
                                               
                                              
       
                         
                          
       
           {
       

        tmp___2 = valloc(record_size);
        record_start = (union block *)tmp___2;
       

    }
                        
       

                                                                           
                                                            
                     
       

     
    current_block = record_start;
    record_end = record_start + blocking_factor;
    if ((unsigned int)wanted_access == 2U) {
      tmp___33 = 0U;
    } else {
      tmp___33 = (unsigned int)wanted_access;
    }
    access_mode = (enum access_mode)tmp___33;
                                      
       
                                                
                      
         
                                                
                      
         
                                                
                      
         
                          
                              
       
                                                    
                                            
       
                          
                              
       
                                                  
       
                          
                              
       
                
       
                          
                                    
       
                                              
         
                                                           
         
                           
                          
         
       
           {
      {
	tmp___22 = strcmp(*(archive_name_array + 0), "-");
      }
                          
                                            
                            
           

                                                                    
                                               
                         
           

         
         
                                                  
                            
           
                                                  
                            
           
                                                  
                            
           
                                
                                   
                      
                                
                                   
                      
                          
                                
                                   
                      
                          
                                             
                                
                                          
         
             {
                           {
                                   {
             
                                                                  
             
                            {
                                             
                                                            {
                {
                  tmp___9 = memchr(
                      (void const *)*(archive_name_array + 0), '/',
                      (size_t)(rmt_path__ - (char *)*(archive_name_array + 0)));
                }
                             {
                   

                    tmp___8 =
                        open(*(archive_name_array + 0), 66,
                             ((128 | (128 >> 3)) | ((128 >> 3) >> 3)) |
                                 ((256 | (256 >> 3)) | ((256 >> 3) >> 3)));
                    archive = tmp___8;
                   

                }       
                   

                                                                                
                                                             
                                      
                   

                 
              }       
                 

                                                               
                                                                           
                                                                               
                                    
                 

               
            }       
               

                                                             
                                                                         
                                                                             
                                  
               

             
          }       
             

                                                           
                                                                       
                                                                           
                                
             

           
        }       
           

                                                    
                              
             
                                                    
                              
             
                                                    
                              
             
                                  
                                     
                                      
               
                                                                    
               
                               
                                               
                                                               
                   
                              
                                                                            
                                                    
                                                                            
                   
                                 
                     

                                
                                                            
                                                                         
                                                                             
                                         
                     

                          
                     

                                                                         
                                                                         
                                         
                     

                   
                        
                   

                              
                                                          
                                                                       
                                                                           
                                       
                   

                 
                      
                 

                                                               
                                                                            
                                                                                
                                     
                 

               
                    
               

                                                             
                                                                          
                                                                              
                                   
               

             
                                  
                                     
                                
               

                                                                
                                   
               

             
                                      
               
                                                                    
               
                               
                                               
                                                               
                   
                              
                                                                            
                                                    
                                                                            
                   
                                 
                     

                                       
                                                    
                                                                             
                                                                              
                                         
                     

                          
                     

                                                                          
                                                                         
                                         
                     

                   
                        
                   

                                     
                                                  
                                                                           
                                                                            
                                       
                   

                 
                      
                 

                            
                                                      
                                                                               
                                                                                
                                     
                 

               
                    
               

                          
                                                    
                                                                             
                                                                              
                                   
               

             
                                  
                                     
                                      
               
                                                                    
               
                               
                                               
                                                               
                   
                              
                                                                            
                                                    
                                                                            
                   
                                 
                     

                                
                                                             
                                                                         
                                                                             
                                         
                     

                          
                     

                                                                          
                                                                         
                                         
                     

                   
                        
                   

                              
                                                           
                                                                       
                                                                           
                                       
                   

                 
                      
                 

                                                                
                                                                            
                                                                                
                                     
                 

               
                    
               

                                                              
                                                                          
                                                                              
                                   
               

             
                                  
                                            
           

         
      }
    }
                      
              
            
                                  
         
                                            
         
                        
              
                                        
                                  
         
                               
             

                                 
             

           
           
                                          
                                    
                                                  
           
         
       
     
     
                                   
                                 
     
    {
      if ((unsigned int)wanted_access == 2U) {
        goto case_2___2;
      }
      if ((unsigned int)wanted_access == 0U) {
        goto case_0___2;
      }
                                             {
        goto case_1___2;
      }
                            
    case_2___2: /* CIL Label */
      records_written = (off_t)0;
    case_0___2 : /* CIL Label */
    {
                              
      record_end = record_start;
                        
    }
                                
         
                                       
                           
         
                     
           

                                                  
                                                                  
                                                          
                         
           

         
         
	                                        
	 
                        
           

                                                       
                                                                      
                                                              
                                                                    
                         
           

         
       
                            
    case_1___2: /* CIL Label */
      records_written = (off_t)0;
                                
         
                                                       
         
                                  
           

                                                                          
                                                                   
                                         
           

                
           

                                                                         
                                                                        
           

         
         
                                                     
                                                                   
                                                
                                                                  
                                                    
                                                                          
                                                            
                                                                     
         
       
                            
                                      
    }
           
  }
}
void flush_write(void) {
  int copy_back;
  ssize_t status;
  char *tmp;
  int *tmp___0;
  size_t tmp___1;
  char *tmp___2;
  int *tmp___3;
  int *tmp___4;
  int *tmp___5;
  _Bool tmp___6;
  int tmp___7;
  size_t tmp___8;
  char *tmp___9;
                    
                    

  {
                            
                   
                               
         

                                               
                                                     
         

       
     
                             
                                                
         

                                       
                        
                              
         

              
                    
       
            
                           
                            
                                      
              
         

                                               
                                    
         

       
     
                                        
                                 
         

                                      
         

       
     
                      
                        
                                       
     
                                        
                                
                        
           

                                                                           
                                                               
                                          
                                            
           

                
           

                                                         
                                      
                                       
           

         
       
             
     
                      
       
                                     
       
                           
         
                                       
         
                            
           
                                         
           
                              
             

                                          
             

           
         
       
     
     
                                                
     
                   
             
     
                        
                                    
     
                               
                              
                        
                      
                          
              
                    
       
            
                           
                                
                      
                       
              
                          
                        
                         
                
                        
         
       
     
                              
       

                                                     
                                                                      
                                                                
                                            
                                                                        
                                                          
                                                  
                                                                   
       

     
                      
                                
                       
       
       
                                                     
                                                                     
                                                            
                                                  
                                                                
                                                        
                                                      
                                                        
                                                                 
                                 
                           
                                                                   
                                 
       
                                
                       
       
     
    {
      tmp___8 = sys_write_archive_buffer();
                                
    }
                                        
       

                                    
       

     
                                     
                    
       
                                  
                                                       
                                                
                                                                 
                                          
                                   
       
                                                        
                                                    
              
                                                                         
           

                                                         
           

                
           

                                                                           
                                                               
                                            
                                          
           

         
       
                    
     
           
  }
}
__attribute__((__noreturn__)) void archive_write_error(ssize_t status);
void archive_write_error(ssize_t status) {
  int e;
  int *tmp;
  int *tmp___0;

   
                        
       

                                 
                 
                              
                                     
                     
       

     
     
                                                                     
     
   
}
void archive_read_error(void) {
  char *tmp;
  char *tmp___0;
  int tmp___1;
                   
                   

   
     
                                       
     
                                   
       

                                                            
                                       
                     
       

     
                               
                       
                       
       

                                                       
                                           
                     
       

     
           
   
}
static void short_read(size_t status) {
  size_t left;
  char *more;
  size_t tmp;
  size_t tmp___0;
  char buf[(((sizeof(uintmax_t) * 8UL) * 302UL) / 1000UL + 1UL) + 1UL];
  char *tmp___1;
  char *tmp___2;
  unsigned long rest;
  char *tmp___3;
  unsigned long rsize;
  char *tmp___4;
                    
                    

   
                                         
                                
     
                 
                                      
                                     
                     
                         
                                              
                                 
               
                    
                               
             
                  
                             
           
         
                     
           

                       
                                                
                                       
                 

                                                                    
                               
                 

                      
                 

                                                                   
                                   
                 

               
                                                      
                                     
               
               
		                     
	       
             
                                           
           

         
                            
           
                                                    
                                   
                                                                             
                             
                                                       
                                                                              
           
                           
         
                                        
           

                                      
                                                                       
                                                                               
                                                     
                         
           

         
                       
                       
       
                                 
     
                                    
                               
                                       
                              
             

                                                   
                                                           
                                                                    
                                                        
             

           
         
       
     
                                                             
                   
           
   
}
void flush_read(void) {
  size_t status;
  char *tmp;
  char *tmp___0;
  size_t tmp___1;
  size_t tmp___2;
  union block *cursor;
  _Bool tmp___3;
  _Bool tmp___4;
  size_t tmp___5;
  size_t tmp___6;
  char const *tmp___7;
  char const *tmp___8;
  char *tmp___9;
  _Bool tmp___10;
  char const *tmp___11;
  char *tmp___12;
  char *tmp___13;
  uintmax_t s1;
  uintmax_t s2;
  char const *tmp___14;
  char *tmp___15;
  int tmp___16;
  char totsizebuf[(((sizeof(uintmax_t) * 8UL) * 302UL) / 1000UL + 1UL) + 1UL];
  char s1buf[(((sizeof(uintmax_t) * 8UL) * 302UL) / 1000UL + 1UL) + 1UL];
  char s2buf[(((sizeof(uintmax_t) * 8UL) * 302UL) / 1000UL + 1UL) + 1UL];
  char *tmp___17;
  char *tmp___18;
  char *tmp___19;
  char const *tmp___20;
  char *tmp___21;
  char *tmp___22;
  off_t tmp___23;
  int *tmp___24;
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    

  {
                            
                   
                               
         

                                              
                                                     
         

       
     
                         
                                  
                                     
         
                      
                                              
                      
         
                                    
           

                                                 
           

         
       
     
                              
                      
         

                                                                         
                                                             
                                          
                                        
         

              
         

                                                       
                                    
                                     
         

       
     
  error_loop:
                             
       

                 
                                                                               
                         
       

           {
       

        tmp___2 =
            safe_read(archive, (void *)(record_start->buffer), record_size);
                         
       

    }
                               {
      records_read++;
             
    }
                        
                  
            
                                           
         
                                        
         
                              
               
                                    
                        
                                                        
                          
             
                                                        
                          
             
                                                        
                          
             
                                
                                  
                                  
                                   
            
                                                      
           
                           
                     
             
                              
                                          
           
                                                      
           
                           
                     
             
                              
                                        
           
             
                         
                                              
                                         
                   

                                                             
                                                                            
                                     
                   

                        
                   

                                                                                
                                                     
                                     
                   

                 
                                                        
                                   
                 
                 
		                       
		 
               
                                         
             
                                        
               

                                   
               

             
                                  
                                                     
                                        
                 
                                                         
                 
                                
                   
                                                              
                                                                              
                                                                     
                                                                         
                            
                                   
                   
                                  
                 
               
                                   
                 

                                                                        
                                                     
                                                            
                                                                              
                 

               
                       
                    
                                        
                 

                                                                  
                                                      
                 

               
             
                              
                                                       
                        
                      
                 
                                                                        
                                                               
                 
                               
                      
                                                              
                                                                           
                                                                
                          
                                 
                 
                                  
                 
               
               
                                                                             
                                                                      
                                         
                                                                 
                                                          
               
                                                         
                            
                      
                                   
                                        
                 
                                                           
                         
                                                                              
                                      
                                     
                                                           
                         
                                                                              
                                      
                                     
                                                           
                                              
                                  
                                                                          
                                  
                                 
                                                                        
                                                                             
                                                                         
                                            
                          
                                 
                 
                                  
                 
               
               
                                           
                                                                 
                                                          
               
                                                                 
                 
                                                                       
                                                      
                          
                                 
                 
                                
               
                       
             
                                   
                           
                   
                  
                        
           
                
                      
         
              
             
                                             
           
                                 
           
                          
         
       
     
     
                         
     
           
  }
}
void flush_archive(void) {

   

                                                    
    current_block = record_start;
    record_end = record_start + blocking_factor;
                                         {
      if (time_to_start_writing) {
         

          access_mode = (enum access_mode)1;
                                           
          backspace_output();
         

      }
    }
    {
                                            
                    
       
      if ((unsigned int)access_mode == 1U) {
        goto case_1;
      }
                                            
                    
       
                        
    case_0 : /* CIL Label */
    {
      flush_read();
    }
      goto switch_break;
    case_1 : /* CIL Label */
    {
      flush_write();
    }
                        
                            
     
              
     
    switch_break: /* CIL Label */;
    }
           
   

}
static void backspace_output(void) {
  struct mtop operation;
  int tmp;
  int tmp___0;
  int tmp___1;
  int *tmp___2;
  int tmp___3;
  int tmp___4;
  int tmp___5;
  off_t position;
  off_t tmp___6;
  __off_t tmp___7;
  off_t tmp___8;
  char *tmp___9;
  off_t tmp___10;
  __off_t tmp___11;
  off_t tmp___12;
                    

  {
                               
                           
                             
       

                          
                                
                                                                           
                                       
                                               
                                  
                      
       

            
       

                        
                    
                                                                           
                                            
                                  
                          
       

     
                       
             
     
     
                                   
     
                        
                               
         

                                
                                  
                                                                             
                                         
                                                 
                                    
                            
         

              
         

                          
                      
                                                                             
                                              
                                    
                            
         

       
                         
               
       
     
                             
       

                                                                
                          
       

           {
       

        tmp___7 = lseek(archive, (off_t)0, 1);
        tmp___8 = tmp___7;
       

    }
    position = tmp___8;
    position = (off_t)((size_t)position - record_size);
                        
                          
     
                             
       

                                                                 
                            
       

           {
       

        tmp___11 = lseek(archive, position, 0);
                            
       

    }
                               
       
                          
                                                                              
                                           
       
                                                  
                                        
         

                                                   
                                                                
         

       
     
           
  }
}
void close_archive(void) {
  int tmp;
  int tmp___0;
  int tmp___1;
  union block *tmp___2;

  {
                               {
       

        flush_archive();
       

    }       
                                            
         

                          
         

       
     
     
                             
     
                        
       

                        
       

     
                             
       

                                               
                      
       

            
       

                                 
                          
       

     
                       
       

                                         
       

     
     
                                    
                                           
     
                    
       

                                
       

     
                      
       

                                  
       

     
                              
                                 
            
                             
     
     
                            
     
           
  }
}
void init_volume_number(void) {
  FILE *file;
  FILE *tmp;
  char *tmp___0;
  char *tmp___1;
  int tmp___2;
  int tmp___3;
  int tmp___4;
  int *tmp___5;
                   
                    

   
     
                                                                    
                                                        
                 
     
               
       
                                                         
                                                                               
       
                         
         

                                                      
                                                                  
                                                      
                       
         

              
                               
           

                                                        
                                                                    
                                                        
                         
           

         
       
       
	                                
       
                    
         

                                        
         

       
       
	                       
       
                         
         

                                         
         

       
            
       
	                             
       
                          
         

                                        
         

       
     
           
   
}
void closeout_volume_number(void) {
  FILE *file;
  FILE *tmp;
  int tmp___0;
  int tmp___1;

   
     
                                                                    
                                                        
                 
     
               
       
                                                
                                                                       
                                        
       
                    
         

                                         
         

       
       
	                       
       
                         
         

                                         
         

       
            
       

	                              
       

     
           
   
}
static FILE *read_file;
static int looped;
static _Bool new_volume(enum access_mode mode) {
  FILE *tmp;
  int tmp___0;
  int tmp___1;
  int tmp___2;
  char *tmp___3;
  char *tmp___4;
  int tmp___5;
  char input_buffer[80];
  char const *tmp___6;
  char *tmp___7;
  char *tmp___8;
  char *tmp___9;
  char *tmp___10;
  char *tmp___11;
  char *tmp___12;
  char *tmp___13;
  char *name;
  char *cursor;
  char *tmp___14;
  int tmp___16;
  int tmp___17;
  void *tmp___18;
  int tmp___20;
  int tmp___21;
  void *tmp___22;
  int tmp___24;
  int tmp___25;
  void *tmp___26;
  int tmp___28;
  int tmp___29;
  void *tmp___30;
  int tmp___31;
                    
                    
                    
                    
                    
                    
                    
                    
                    

  {
                     
                                
                           
           

                                                                    
                                                              
                            
           

                
                            
         
       
     
                        
                        
     
                        
       

                        
       

     
                             
       

                                                   
                          
       

            
       

                                 
                          
       

     
                       
       

                                         
       

     
                   
                           
       

                                                    
                                           
                     
       

     
            
                          
                                             
                                                              
                                               
                 
     
           
                 
                               
                                
           

                                     
           

         
         
	                                       
	 
                           
           

                                                      
                                                                   
                         
           

         
              
         

                     
                                          
             
                                           
                                                    
                                                                              
                                                        
                                                                            
                               
                                      
                        
                                                                          
                                                           
                                                                       
             
                                                                        
               
                                                                       
                                                   
               
                                                          
                                                            
                                                              
                     

                                                                          
                                                         
                     

                   
                 
               
               
		             
	       
             
                                             
                               
                    
                                                
                                 
                      
                                                 
                                   
                 
               
             
             
                                               
                             
               
                                                
                              
               
                                                
                              
               
                                               
                             
               
                                
                                     
             
                                 
                                                                       
                                                                              
                                                                     
                                                        
                                                                
             
                                
                                      
             
                                                              
                                                  
             
                                                          
                                                            
                                                              
                     

                                                                           
                                                          
                     

                   
                 
               
               
		             
	       
                                     
                                      
                                      
               
                           
                                                    
                                            
                                             
                                           
                     
                   
                         
                 
                                               
               
                            
               
                           
                                                    
                                
                                                
                                           
                     
                          
                                         
                   
                           
                 
                                               
               
               
                                       
                                                       
                                                              
               
                                
                                     
             
                                
             
                                
                                          
             
           
                                     
         

       
     
     
                                                         
     
                        
                                          
                  
            
                          
                                  
           
                                                           
           
                           
                                           
                                                      
               
                          
                                                                   
                                                                                
               
                             
                 

                                                           
                                                                            
                                                                                
                                     
                 

                      
                 

                                                                          
                                                            
                                     
                 

               
                    
               

                                                         
                                                                          
                                                                              
                                   
               

             
                  
             

                                                       
                                                                        
                                                                            
                                 
             

           
                
           

                                                     
                                                                      
                                                                          
                               
           

         
              
         

                                         
                        
           
                                         
                        
           
                                         
                        
           
                                
                               
                                    
             
                                                             
             
                             
                                             
                                                        
                 
                                    
                                                              
                                                                           
                 
                               
                   

                              
                                                     
                                                                       
                                                                           
                                       
                   

                        
                   

                                                                           
                                                              
                                       
                   

                 
                      
                 

                                                          
                                                                            
                                                                                
                                     
                 

               
                    
               

                                                        
                                                                          
                                                                              
                                   
               

             
                  
             

                                                      
                                                                        
                                                                            
                                 
             

           
                                
                               
                              
             

                                                         
             

           
                                    
             
                                                             
             
                             
                                             
                                                        
                 
                                    
                                                              
                                                                           
                 
                               
                   

                                     
                                             
                                                                           
                                                                            
                                       
                   

                        
                   

                                                                            
                                                              
                                       
                   

                 
                      
                 

                            
                                                 
                                                                               
                                                                                
                                     
                 

               
                    
               

                          
                                               
                                                                             
                                                                              
                                   
               

             
                  
             

                        
                                             
                                                                           
                                                                            
                                 
             

           
                                
                               
                                    
             
                                                             
             
                             
                                             
                                                        
                 
                                    
                                                              
                                                                           
                 
                               
                   

                              
                                                      
                                                                       
                                                                           
                                       
                   

                        
                   

                                                                            
                                                              
                                       
                   

                 
                      
                 

                                                           
                                                                            
                                                                                
                                     
                 

               
                    
               

                                                         
                                                                          
                                                                              
                                   
               

             
                  
             

                                                       
                                                                        
                                                                            
                                 
             

           
                                
                                          
         

       
     
                      
       
                                        
       
                           
                                       
                              
             

                                 
             

           
         
       
                    
     
    return ((_Bool)1);
  }
}
/* #pragma merger("0","001.compare.o.i","") */
extern __attribute__((__nothrow__)) int(
    __attribute__((__nonnull__(1, 2), __leaf__))
    memcmp)(void const *__s1, void const *__s2, size_t __n)
    __attribute__((__pure__));
extern __attribute__((__nothrow__)) int(
    __attribute__((__nonnull__(1, 2), __leaf__))
    strncmp)(char const *__s1, char const *__s2, size_t __n)
    __attribute__((__pure__));
extern __attribute__((__nothrow__))
ssize_t(__attribute__((__nonnull__(1, 2), __leaf__))
        readlink)(char const *__restrict __path, char *__restrict __buf,
                  size_t __len);
extern int fsync(int __fd);
extern int vfprintf(FILE *__restrict __s, char const *__restrict __format,
                    __gnuc_va_list __arg);
extern
    __attribute__((__nothrow__)) int(__attribute__((__nonnull__(1), __leaf__))
                                     utime)(char const *__file,
                                            struct utimbuf const *__file_times);
__attribute__((__noreturn__)) void xalloc_die(void);
int exit_status;
_Bool atime_preserve_option;
_Bool dereference_option;
void diff_archive(void);
void diff_init(void);
char *get_directory_contents(char *path, dev_t device);
union block *current_header;
enum archive_format current_format;
void decode_header(union block *header, struct tar_stat_info *stat_info,
                   enum archive_format *format_pointer, int do_user_group);
void print_header(struct tar_stat_info *st, off_t block_ordinal);
enum read_header read_header(_Bool raw_extended_headers);
void skip_member(void);
int deref_stat(_Bool deref, char const *name, struct stat *buf);
void readlink_error(char const *name);
void readlink_warn(char const *name);
void seek_error_details(char const *name, off_t offset);
void seek_warn(char const *name);
void stat_error(char const *name);
void stat_warn(char const *name);
_Bool sys_compare_uid(struct stat *a, struct stat *b);
_Bool sys_compare_gid(struct stat *a, struct stat *b);
_Bool sys_compare_links(struct stat *link_data, struct stat *stat_data);
void report_difference(struct tar_stat_info *st __attribute__((__unused__)),
                       char const *fmt, ...);
_Bool sparse_diff_file(int fd, struct tar_stat_info *st);
static int diff_handle;
static char *diff_buffer;
void diff_init(void) {
  void *tmp;

   
     
                                
                                
     
                       
       

                     
       

     
           
   
}
void report_difference(struct tar_stat_info *st __attribute__((__unused__)),
                       char const *fmt, ...) {
  va_list ap;
  char *tmp;

   
              
       

                                                                        
                                                  
                                                              
                                    
                                                   
                                                          
                             
                                                  
                                                       
       

     
                           
                      
     
           
   
}
static int process_noop(size_t size __attribute__((__unused__)),
                        char *data __attribute__((__unused__))) {

   

    return (1);
   

}
static int process_rawdata(size_t bytes, char *buffer___2) {
  size_t status;
  size_t tmp;
  char *tmp___0;
  char *tmp___1;
  int tmp___2;
                   

  {
     
                                                               
                   
     
                          
                                           
         

                                                                
                                                                           
         

              
         

                                                               
                                                                        
                                                                              
                                   
         

       
                 
     
     
               
                                                                             
     
                  
       
                                             
                                                                     
       
                 
     
    return (1);
  }
}
static char *dumpdir_cursor;
static int process_dumpdir(size_t bytes, char *buffer___2) {
  char *tmp;
  int tmp___0;
                   

  {
     
               
                                                                                
     
                  
       
                                         
                                                                 
       
                 
     
                            
    return (1);
  }
}
static void read_and_process(off_t size, int (*processor)(size_t, char *)) {
  union block *data_block;
  size_t data_size;
  char *tmp;
  int tmp___0;
                   

   
                              
                           
     
     
                 
                                      
                    
                           
         
         
	                                 
	 
                          
           
                                                       
                                           
                            
           
                 
         
         
	                                                
	 
                                       
                                   
         
         
	                                                        
	 
                       
                                    
         
         
                               
                                                                     
                                                   
         
                                  
                                                                     
         
       
                                 
     
           
   
}
static int get_stat_data(char const *file_name, struct stat *stat_data) {
  int status;
  int tmp;
  int *tmp___0;

  {
     
                                                                 
                   
     
                      
       
                                     
       
                          
         

                               
         

              
         

	                        
	 

       
       
	                                                                 
       
                 
     
    return (1);
  }
}
void diff_archive(void) {
  struct stat stat_data;
  int status;
  struct utimbuf restore_times;
  char *tmp;
  char *tmp___0;
  char *tmp___1;
  int tmp___2;
  char *tmp___3;
  char *tmp___4;
  char *tmp___5;
  _Bool tmp___6;
  char *tmp___7;
  _Bool tmp___8;
  char *tmp___9;
  char *tmp___10;
  struct stat file_data;
  struct stat link_data;
  int tmp___11;
  int tmp___12;
  char const *tmp___13;
  char *tmp___14;
  _Bool tmp___15;
  size_t len;
  size_t tmp___16;
  char *linkbuf;
  void *tmp___17;
  ssize_t tmp___18;
  int *tmp___19;
  char *tmp___20;
  int tmp___21;
  int tmp___22;
  char *tmp___23;
  char *tmp___24;
  char *tmp___25;
  char *dumpdir_buffer;
  char *tmp___26;
  int tmp___27;
  char *tmp___28;
  char *tmp___29;
  off_t offset;
  int tmp___30;
  char *tmp___31;
  char *tmp___32;
  __off_t tmp___33;
  int tmp___34;
  int tmp___35;
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    

   
     
                                           
                                                                            
     
                         
                          
         

                                   
                                                    
                                                       
         

       
       
	                                            
       
     
     
                                                      
                    
       
                                                       
                    
       
                                                       
                    
       
                                                       
                    
       
                                                       
                     
       
                                                       
                     
       
                                                       
                     
       
                                                       
                     
       
                                                       
                     
       
                                                       
                     
       
                                                       
                        
       
                                                       
                     
       
                                                       
                     
       
                          
                                    
     
                                                                          
                                                                               
                                                 
                                                  
                      
     
                            
                            
                             
                             
                                                  
                        
       
       
                                                                          
                                            
       
                     
         
                        
         
                  
       
                                                      
         
                                                 
                                                                       
                        
         
                  
       
                                           
                                
                                                                  
                                                                      
                                                                           
                              
                                
                                                                  
                                                                      
                                                                           
         

                                            
                                                                       
         

       
       
	                                                               
       
                     
         

                                           
                                                                       
         

       
       
	                                                               
       
                     
         

                                           
                                                                       
         

       
                                                                              
         

                                                
                                                                       
         

       
                                                       
                                                                  
           
                                               
                                                                          
                          
           
                    
         
       
       
	                                                                 
       
                            
         
                                                                
                        
                                                                           
         
                  
       
                                                      
                                                       
                                        
         

                                                            
         

              
                                  
           

                                     
                                                                     
                                                          
           

         
         
	                                                                     
	 
                                  
           

                                                       
           

         
       
       
	                            
       
                        
         

                                                                 
         

       
                                  
         

                                                          
                                                          
         

       
         
                        
                             
     
                
                                                                               
     
                      
                          
       
       
                                                                           
                                             
       
                      
                          
       
       
	                                                     
       
                      
         

                                                                      
                                                 
                                                                       
                                      
         

       
                        
                             
     
                                                                   
                     
                                             
                                 
                
                                                                               
                                                                 
                             
     
                       
         
                                        
         
                             
           

                                                                     
           

                
           

	                                                              
	   

         
         
	                                                                   
	 
              
                                    
           

                                                  
                                                                          
           

                
           
                                                                         
                                                           
           
                              
             

                                                    
                                                                            
             

           
         
       
                        
                             
                             
                              
      
                
                                                                               
     
                      
                          
       
                                                       
                                                            
              
                                                         
                                                               
                
                                                              
         
                            
       
                     
         
                                                  
                                                                        
         
                          
       
                                                       
                    
              
                                                         
                               
                                                                    
             
                                                          
                                                                            
             
                              
           
         
       
                                           
                                
                                                                  
                                                                      
                                                                           
                              
                                
                                                                  
                                                                      
                                                                           
         
                                             
                                                                        
         
                          
       
                        
                             
     
                                                                               
                                
     
                                
         

                                                                               
                                                        
         

       
                           
         

                                          
                                                                             
                                       
         

              
         

	                                                                  
	 

       
                                
         

                                                     
         

       
               
                             
      
                
                                                                               
     
                      
                          
       
                                                      
         
                                                  
                                                                        
         
                          
       
                                           
                                
                                                                  
                                                                      
                                                                           
                              
                                
                                                                  
                                                                      
                                                                           
         
                                             
                                                                        
         
                          
       
                        
                            
                        
                            
                                                 
                        
       
       
                                                                           
                                             
       
                      
                          
       
                                                      
         
                                                  
                                                                        
                        
         
                          
       
       
                                 
                                                                 
                                                          
       
                                                                         
         
                                             
                                                                        
                        
         
                          
       
       
	                                                                 
       
                            
         
                                                                
                                                                           
                        
         
                          
       
       
	                                         
       
                          
         
                                                                                
                                                                           
         
                          
       
                                
         

                                                                               
                                           
         

       
       
	                                                                   
       
                                
         

                                                     
         

       
       
	                            
       
                        
         

                                                                 
         

       
                        
                                  
     
           
   
}
void verify_volume(void) {
  struct mtop operation;
  int status;
  off_t tmp;
  __off_t tmp___0;
  off_t tmp___1;
  int *tmp___2;
  int tmp___3;
  int tmp___4;
  int tmp___5;
  int tmp___6;
  enum read_header status___0;
  enum read_header tmp___7;
  int counter;
  char *tmp___8;

   
                       
       

                    
       

     
     
                     
                                                                    
                                 
                             
     
                             
       

                              
                                
                                                                           
                                       
                                               
                                  
                         
       

            
       

                        
                    
                                                                           
                                            
                                  
                         
       

     
                     
       
                                     
       
                          
                
              
                                 
           

                                  
                                    
                                                                               
                                           
                                                   
                                      
                             
           

                
           

                            
                        
                                                                               
                                                
                                      
                             
           

         
                         
           
                                   
             

                                                                  
                            
             

                  
             

                                                    
                                
             

           
                              
             
                                                   
             
                   
           
         
       
     
     
                                        
                               
                   
     
     
                 
                                      
         
                                          
                               
         
                                             
                      
           
                       
                                                
               
                          
                                                   
               
                                                      
                                     
               
             
                                           
           
           
                                                                            
                                                                             
                                                       
                                                        
                            
           
         
                                             
                           
                
                                               
                             
           
         
         
	                 
	 
       
                                 
     
                                      
                             
           
   
}
/* #pragma merger("0","002.create.o.i","") */
extern __attribute__((__nothrow__)) unsigned int(
    __attribute__((__leaf__)) gnu_dev_major)(unsigned long long __dev)
    __attribute__((__const__));
extern __attribute__((__nothrow__)) unsigned int(
    __attribute__((__leaf__)) gnu_dev_minor)(unsigned long long __dev)
    __attribute__((__const__));
extern __attribute__((__nothrow__)) char *(
    __attribute__((__nonnull__(1, 2), __leaf__))
    strncpy)(char *__restrict __dest, char const *__restrict __src, size_t __n);
extern __attribute__((__nothrow__)) char *(__attribute__((__nonnull__(1),
                                                          __leaf__))
                                           strdup)(char const *__s)
    __attribute__((__malloc__));
extern
    __attribute__((__nothrow__)) int(__attribute__((__nonnull__(2), __leaf__))
                                     fstat)(int __fd, struct stat *__buf);
extern __attribute__((__nothrow__))
__uid_t(__attribute__((__leaf__)) getuid)(void);
extern __attribute__((__nothrow__))
__gid_t(__attribute__((__leaf__)) getgid)(void);
extern
    __attribute__((__nothrow__)) int(__attribute__((__nonnull__(1), __leaf__))
                                     unlink)(char const *__name);
char *savedir(char const *dir);
void *xrealloc(void *p, size_t n);
mode_t mode_adjust(mode_t oldmode, struct mode_change const *changes);
enum archive_format archive_format;
int after_date_option;
gid_t group_option;
_Bool ignore_failed_read_option;
_Bool incremental_option;
_Bool interactive_option;
char const *listed_incremental_option;
struct mode_change *mode_option;
struct timespec newer_mtime_option;
int recursion_option;
_Bool numeric_owner_option;
_Bool one_file_system_option;
uid_t owner_option;
_Bool remove_files_option;
_Bool sparse_option;
_Bool file_dumpable_p(struct tar_stat_info *st);
void create_archive(void);
void pad_archive(off_t size_left);
void dump_file(char *p, int top_level, dev_t parent_device);
union block *start_header(struct tar_stat_info *st);
void simple_finish_header(union block *header);
union block *start_private_header(char const *name, size_t size);
void write_eot(void);
void check_links(void);
void gid_to_chars(gid_t v, char *p, size_t s);
void major_to_chars(int v, char *p, size_t s);
void minor_to_chars(int v, char *p, size_t s);
void mode_to_chars(mode_t v, char *p, size_t s);
void size_to_chars(size_t v, char *p, size_t s);
void uid_to_chars(uid_t v, char *p, size_t s);
void uintmax_to_chars(uintmax_t v, char *p, size_t s);
void string_to_chars(char *str, char *p, size_t s);
void write_directory_file(void);
struct xheader extended_header;
void close_diag(char const *name);
void open_diag(char const *name);
void read_diag_details(char const *name, off_t offset, size_t size);
void readlink_diag(char const *name);
void savedir_diag(char const *name);
void stat_diag(char const *name);
void unlink_error(char const *name);
struct name *gnu_list_name;
void gid_to_gname(gid_t gid, char **gname);
int gname_to_gid(char const *gname, gid_t *gidp);
void uid_to_uname(uid_t uid, char **uname);
int uname_to_uid(char const *uname, uid_t *uidp);
char *name_next(int change_dirs);
void collect_and_sort_names(void);
char *name_from_list(void);
void blank_name_list(void);
_Bool excluded_name(char const *name);
_Bool is_avoided_name(char const *name);
int confirm(char const *message_action, char const *message_name);
void tar_stat_init(struct tar_stat_info *st);
void xheader_store(char const *keyword, struct tar_stat_info const *st,
                   void *data);
void xheader_write(char type, char *name, struct xheader *xhdr);
void xheader_write_global(void);
void xheader_finish(struct xheader *xhdr);
char *xheader_xhdr_name(struct tar_stat_info *st);
void sys_stat_nanoseconds(struct tar_stat_info *st);
_Bool sys_file_is_archive(struct tar_stat_info *p);
_Bool sparse_file_p(struct tar_stat_info *st);
enum dump_status sparse_dump_file(int fd, struct tar_stat_info *st);
_Bool string_ascii_p(char const *str);
void *hash_lookup(Hash_table const *table___0, void const *entry);
void *hash_get_first(Hash_table const *table___0);
void *hash_get_next(Hash_table const *table___0, void const *entry);
Hash_table *hash_initialize(size_t candidate, Hash_tuning const *tuning,
                            size_t (*hasher)(void const *, size_t),
                            _Bool (*comparator)(void const *, void const *),
                            void (*data_freer)(void *));
void *hash_insert(Hash_table *table___0, void const *entry);
static void to_octal(uintmax_t value, char *where, size_t size) {
  uintmax_t v;
  size_t i;

  {
    v = value;
    i = size;
    {
      while (1) {
                                     ;
        i--;
        *(where + i) = (char)(48UL + (v & (unsigned long)((1 << 3) - 1)));
        v >>= 3;
        if (!i) {
          goto while_break;
        }
      }
    while_break: /* CIL Label */;
    }
           
  }
}
static void to_base256(int negative, uintmax_t value, char *where,
                       size_t size) {
  uintmax_t v;
  uintmax_t propagated_sign_bits;
  size_t i;

   
              
                                                                             
             
     
                 
                                      
            
                                                                 
                                            
                 
                           
         
       
                                 
     
           
   
}
static int warned_once;
static void to_chars(int negative, uintmax_t value, size_t valsize,
                     uintmax_t (*substitute)(int *), char *where, size_t size,
                     char const *type) {
  int base256_allowed;
  char *tmp;
  uintmax_t maxval;
  char valbuf[((((sizeof(uintmax_t) * 8UL) * 302UL) / 1000UL + 1UL) + 1UL) +
              1UL];
  char maxbuf[(((sizeof(uintmax_t) * 8UL) * 302UL) / 1000UL + 1UL) + 1UL];
  char minbuf[((((sizeof(uintmax_t) * 8UL) * 302UL) / 1000UL + 1UL) + 1UL) +
              1UL];
  char const *minval_string;
  char const *maxval_string;
  char *tmp___0;
  char const *value_string;
  uintmax_t m;
  char *p;
  char *tmp___1;
  char *p___0;
  char *tmp___2;
  char *tmp___3;
  int negsub;
  uintmax_t sub;
  uintmax_t tmp___4;
  uintmax_t s;
  char subbuf[((((sizeof(uintmax_t) * 8UL) * 302UL) / 1000UL + 1UL) + 1UL) +
              1UL];
  char *sub_string;
  char *tmp___5;
  char *tmp___6;
  char *tmp___7;
  int tmp___8;
  int tmp___9;
  unsigned long tmp___10;
  unsigned long tmp___11;
  unsigned long tmp___12;
  unsigned long tmp___13;
  unsigned long tmp___14;
  unsigned long tmp___15;
                    
                    
                    
                    
                    
                    
                    
                    

  {
                                             
                  
            
                                               
                    
              
                    
       
     
                              
    if (!negative) {
                                                        {
                                                     
      }       
                                 
       
                             {
         

                                                 
          to_octal(value, where, size - 1UL);
         

      }       
                    
       
    } else {
    _L___5: /* CIL Label */
                    {
                                                
      }       
                         
       
                                                         
                                                     
              
                                 
       
                                 
                              
                         
                         
                  
                             
           
           
                                         
                                                               
           
                
                      
         
              
                             
                       
                                                    
                               
               

                                
                                                                   
                                               
               

             
                                                   
                                                          
                                                      
                    
                                       
             
             
	                                                    
	     
                  
                        
           
                
                               
                                
                                                               
                                                           
                    
                                       
             
                              
                  
                                                               
                                                           
                    
                                       
             
                              
           
           
                                                    
                       
                                                                                
                                
                                                  
           
                                
                               
                               
                    
                                     
             
             
                                                      
                                   
                                                                                
                                
                          
                  
                             
                                              
             
                  
                                
           
                         
             

                                                      
                         
                                
                                                                             
                             
                              
                      
                                 
                                                 
             

                  
             

                                                      
                        
                          
                                                                             
                             
                                                   
             

           
                           
             
                                               
                                     
                                                           
             
                         
                       
                    
                      
             
             
                                                      
                                   
                                                                                
                                
                                   
             
                         
                           
                                      
             
             
                       
                                                                              
                                                                    
                                                              
                                                                               
                             
             
                  
             

                                                                   
                                                                    
                                                  
                              
             

           
         
       
    }
           
  }
}
static gid_t gid_nobody;
static uintmax_t gid_substitute(int *negative) {
  gid_t r;
  int tmp;
                   

  {
                      
       
                                                  
       
                 
                               
       
     
    r = gid_nobody;
                       
    return ((uintmax_t)r);
  }
}
void gid_to_chars(gid_t v, char *p, size_t s) {
                   

   

     
                                                                                
     
           
   

}
void major_to_chars(int v, char *p, size_t s) {
                   

   

     
                                                                            
                          
     
           
   

}
void minor_to_chars(int v, char *p, size_t s) {
                   

   

     
                                                                            
                          
     
           
   

}
void mode_to_chars(mode_t v, char *p, size_t s) {
  int negative;
  uintmax_t u;
  int tmp;
  int tmp___0;
  int tmp___1;
  int tmp___2;
  int tmp___3;
  int tmp___4;
  int tmp___5;
  int tmp___6;
  int tmp___7;
  int tmp___8;
  int tmp___9;
  int tmp___10;
                    

  {
                         
                           
                           
                                     
                                       
                                        
                                                         
                                                           
                                                             
                                        
                                       
                            
                                   
                     
                          
                                 
                   
                        
                               
                 
                      
                             
               
                    
                           
             
                  
                         
           
                
                       
         
              
                     
       
           {
    _L___10: /* CIL Label */
      negative = 0;
                     {
        tmp = 2048;
      }       
                
       
                     {
        tmp___0 = 1024;
      }       
                    
       
                    {
        tmp___1 = 512;
      }       
                    
       
                    {
        tmp___2 = 256;
      }       
                    
       
                    {
        tmp___3 = 128;
      }       
                    
       
                   {
        tmp___4 = 64;
      }       
                    
       
                                        {
        tmp___5 = 32;
      }       
                    
       
                                        {
        tmp___6 = 16;
      }       
                    
       
                                       {
        tmp___7 = 8;
      }       
                    
       
                                               {
        tmp___8 = 4;
      }       
                    
       
                                               {
        tmp___9 = 2;
      }       
                    
       
                                              {
        tmp___10 = 1;
      }       
                     
       
      u = (uintmax_t)(
          ((((((((((tmp | tmp___0) | tmp___1) | tmp___2) | tmp___3) | tmp___4) |
               tmp___5) |
              tmp___6) |
             tmp___7) |
            tmp___8) |
           tmp___9) |
          tmp___10);
    }
    {
      to_chars(negative, u, sizeof(v), (uintmax_t(*)(int *))0, p, s, "mode_t");
    }
           
  }
}
void off_to_chars(off_t v, char *p, size_t s) {
                   

   

    {
      to_chars(v < 0L, (uintmax_t)v, sizeof(v), (uintmax_t(*)(int *))0, p, s,
               "off_t");
    }
           
   

}
void size_to_chars(size_t v, char *p, size_t s) {
                   

   

     
                                                                        
     
           
   

}
void time_to_chars(time_t v, char *p, size_t s) {
                   

   

     
                                                                             
                         
     
           
   

}
static uid_t uid_nobody;
static uintmax_t uid_substitute(int *negative) {
  uid_t r;
  int tmp;
                   

  {
                      
       
                                                  
       
                 
                               
       
     
    r = uid_nobody;
                       
    return ((uintmax_t)r);
  }
}
void uid_to_chars(uid_t v, char *p, size_t s) {
                   

   

     
                                                                                
     
           
   

}
void uintmax_to_chars(uintmax_t v, char *p, size_t s) {
                   

   

    {
      to_chars(0, v, sizeof(v), (uintmax_t(*)(int *))0, p, s, "uintmax_t");
    }
           
   

}
void string_to_chars(char *str, char *p, size_t s) {

   

     
                                                                               
                 
                                 
     
           
   

}
_Bool file_dumpable_p(struct tar_stat_info *st) {
  int tmp;

  {
                          
              
           {
                                        
                               
                                                                       
                                                                     
                  
                
                  
         
             {
        tmp = 1;
      }
    }
    return ((_Bool)tmp);
  }
}
void write_eot(void) {
  union block *pointer;
  union block *tmp;
  size_t tmp___0;

  {
    {
                              
                    
                                                        
                                    
      pointer = find_next_block();
      tmp___0 = available_space_after(pointer);
      memset((void *)(pointer->buffer), 0, tmp___0);
                                    
    }
           
  }
}
static void tar_copy_str(char *dst, char const *src, size_t len) {

   

    {
                                     
      strncpy((char * /* __restrict  */)dst,
              (char const * /* __restrict  */)src, len);
    }
           
   

}
union block *start_private_header(char const *name, size_t size) {
  time_t t;
  union block *header;
  union block *tmp;
  __uid_t tmp___0;
  __gid_t tmp___1;
                   
                   

  {
    {
      tmp = find_next_block();
      header = tmp;
                                                               
                                                           
                                                    
                                                
               
                                                                           
                                                                       
                                                                      
                         
                                                                            
                         
                                                                            
                                                
                                                      
                                                
                                                      
                                                               
                                                                   
                                                                 
                                                                
    }
    return (header);
  }
}
static union block *write_short_name(struct tar_stat_info *st) {
  union block *header;
  union block *tmp;

  {
    {
      tmp = find_next_block();
      header = tmp;
      memset((void *)(header->buffer), 0, sizeof(union block));
      tar_copy_str(header->header.name, (char const *)st->file_name,
                   (size_t)100);
    }
    return (header);
  }
}
static void write_gnu_long_link(struct tar_stat_info *st, char const *p,
                                char type) {
  size_t size;
  size_t tmp;
  size_t bufsize;
  union block *header;
                   
                   

   
     
                      
                       
                                                           
                                                              
                                                         
                                     
                                           
                                 
                                              
     
     
                 
                                      
                                
                           
         
         
                                                            
                                                             
                       
                          
                                                                 
                                     
                                                  
         
       
                                 
     
     
                                                        
                                                      
                                                                 
                                                          
     
           
   
}
static size_t split_long_name(char const *name, size_t length) {
  size_t i;

  {
                         
                           
     
                     
    {
                {
                                     ;
                         
                           
         
                                             
                           
         
        i--;
      }
    while_break: /* CIL Label */;
    }
    return (i);
  }
}
static union block *write_ustar_long_name(char const *name) {
  size_t length;
  size_t tmp;
  size_t i;
  union block *header;
  char *tmp___0;
  char *tmp___1;
  char *tmp___2;
  char *tmp___3;
                    
                    
                    

  {
    {
      tmp = strlen(name);
      length = tmp;
    }
                        {
       
                                       
                                                                            
                                                         
                        
       
      return ((union block *)((void *)0));
    }
     
                                        
     
                   
       
                                       
                 
                                                                               
                                                    
                        
       
                                          
            
                                       
         
                                         
                            
                                                                         
                                                      
                          
         
                                            
       
     
     
                                 
                                                                  
                                                               
                                                      
                                                             
                                                              
                                 
     
                    
  }
}
static void write_long_link(struct tar_stat_info *st) {
  char *tmp;
  char *tmp___0;
                   
                   

   
     
                                               
                    
       
                                               
                    
       
                                               
                    
       
                                               
                    
       
                                               
                    
       
                                               
                    
       
                          
                            
     
                                                                             
     
                        
                            
                            
                             
      
                                                        
                                                                 
                                              
                      
     
                        
                            
                            
      
                                                                      
     
                        
                                    
     
              
     
                                  
     
           
   
}
static union block *write_long_name(struct tar_stat_info *st) {
  char *tmp;
  char *tmp___0;
  size_t tmp___1;
  union block *tmp___2;
  union block *tmp___3;
                   
                   

  {
    {
                                               
                    
       
                                               
                    
       
                                               
                    
       
                                               
                    
       
                                               
                    
       
                                               
                    
       
                          
                            
     
                                                                         
     
                        
                            
     
                                                    
     
                           
         
                                                            
                                                                              
                                                      
                          
         
                                            
       
                        
    case_3:  /* CIL Label */
                            
     {
      tmp___2 = write_ustar_long_name((char const *)st->file_name);
    }
      return (tmp___2);
                            
                            
      
                                                                      
     
                        
                                    
     
              
     
                                  
    }
     
                                     
     
                     
  }
}
static union block *write_extended(struct tar_stat_info *st,
                                   union block *old_header) {
  union block *header;
  union block hp;
  char *p;
                   
                   
                   
                   
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                  
                    
                    
                    
                    
                  
                    
                    
                    
                    
                    
                    
                  
                    
                    
                    
                    
                    
                    
                    
                    
                    
                  
                    
                    

  {
                                {
      return (old_header);
    }       
                                                                             
                            
       
     
     
                                       
                                                   
                                                                     
                                
                                                    
                      
                                 
                                              
                                                                              
     
                    
  }
}
static union block *write_header_name(struct tar_stat_info *st) {
  union block *tmp;
  union block *tmp___0;
  union block *tmp___1;
  size_t tmp___2;
  _Bool tmp___3;
                   

  {
                                             
       
                                                              
       
                    
                
              
         
                                                                             
                                     
         
                     
       
           {
    _L : {
	                                              
      }
                            
         
                                        
         
                         
             {
        {
	  tmp___1 = write_short_name(st);
	}
        return (tmp___1);
      }
    }
  }
}
union block *start_header(struct tar_stat_info *st) {
  union block *header;
  mode_t tmp;
  size_t tmp___0;
  _Bool tmp___1;
  size_t tmp___2;
  _Bool tmp___3;
  int tmp___4;
                   
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    

  {
    {
      header = write_header_name(st);
    }
                  
                                          
     
                                      
                                     
     
                                      
                                     
     
                      
       

                                           
                                                                   
                          
                               
                                    
                                                                      
                                                                          
                                                                               
                
       

     
                                            {
       

        mode_to_chars(
            st->stat.st_mode &
                (unsigned int)(3584 |
                               (((64 | (64 >> 3)) | ((64 >> 3) >> 3)) |
                                (((128 | (128 >> 3)) | ((128 >> 3) >> 3)) |
                                 ((256 | (256 >> 3)) | ((256 >> 3) >> 3))))),
            header->header.mode, sizeof(header->header.mode));
       

    }       
                                               
         

                        
                                
                                       
                                                                         
                                                                             
                                                                               
                                                                
         

              
         

                                                              
                                                     
         

       
     
                                     
                                               
         

                                                                            
         

              
         

                                                           
                                                   
         

       
            
       

                                                         
                                                 
       

     
                                     
                                               
         

                                                                            
         

              
         

                                                           
                                                   
         

       
            
       

                                                         
                                                 
       

     
                                        {
                                               
         

                                                                             
         

             {
         

          off_to_chars(st->stat.st_size, header->header.size,
                       sizeof(header->header.size));
         

      }
    }       
       

                                                           
                                                  
       

     
     
                                                                  
                                                  
     
                                               
                  
            
                                                  
                              
       
                                                                           
                                                                           
       
                                      
                                                   
             

                                                                         
                                       
             

                  
             

                                                                        
                                                              
             

           
                
           

                                                                      
                                                            
           

         
                                      
                                                   
             

                                                                         
                                       
             

                  
             

                                                                        
                                                              
             

           
                
           

                                                                      
                                                            
           

         
              
         

                                                    
                                                          
                                                    
                                                          
         

       
     
                                             
       

                                                                            
                                                                            
       

            
                               
                                                 
           

                                                                               
                                                               
                                                                               
                                                               
           

                
                                                   
             

                                                    
                                                        
                                                                 
                                                    
                                                        
                                                                 
             

           
         
       
     
                                             
                       
            
                    
     
                                            
     
                                               
                    
       
                                               
                    
       
                                               
                    
       
                                               
                    
       
                                               
                    
       
                          
                           
                        
                            
                            
      
                                                              
                                                         
     
                        
                            
                            
      
                                                               
                                                                   
                                                                 
                                                                
     
                        
                                    
     
              
     
                                  
     
                                                
                                  
         
                                                    
                                                    
         
                                                 
           
                                                      
           
                               
             

                                                                      
                                       
             

                  
             
	                                                        
	     
                          
               

                                                                
                                                              
               

                    
               

                                                                        
                                         
               

             
           
                
           

                                                            
                                                          
           

         
                                                 
           
                                                      
           
                               
             

                                                                      
                                       
             

                  
             
	                                                        
	     
                          
               

                                                                
                                                              
               

                    
               

                                                                        
                                         
               

             
           
                
           

                                                            
                                                          
           

         
       
     
    return (header);
  }
}
void simple_finish_header(union block *header) {
  size_t i;
  int sum;
  char *p;
  char *tmp;
  size_t tmp___0;
                   

  {
    {
      memcpy((void * /* __restrict  */)(header->header.chksum),
             (void const * /* __restrict  */) "        ",
             sizeof(header->header.chksum));
              
      p = header->buffer;
      i = sizeof(*header);
    }
    {
      while (1) {
                                     ;
        tmp___0 = i;
        i--;
        if (!(tmp___0 != 0UL)) {
          goto while_break;
        }
        tmp = p;
        p++;
        sum += 255 & (int)*tmp;
      }
    while_break: /* CIL Label */;
    }
    {
      uintmax_to_chars((uintmax_t)sum, header->header.chksum, (size_t)7);
      set_next_block_after(header);
    }
           
  }
}
void finish_header(struct tar_stat_info *st, union block *header,
                   off_t block_ordinal) {

   

    if (verbose_option) {
                                              {
                                                {
                                                   {
                                                     {
               

                                        
                                                
                print_header(st, block_ordinal);
               

            }
          }
        }
      }
    }
    {
                                          
      simple_finish_header(header);
    }
           
   

}
void pad_archive(off_t size_left) {
  union block *blk;

   
     
                 
                                      
                                
                           
         
         
                                    
                                  
                                                        
                                    
                            
         
       
                                 
     
           
   
}
static enum dump_status dump_regular_file(int fd, struct tar_stat_info *st) {
  off_t size_left;
  off_t block_ordinal;
  union block *blk;
  size_t bufsize;
  size_t count;
  size_t tmp;
  char buf[(((sizeof(uintmax_t) * 8UL) * 302UL) / 1000UL + 1UL) + 1UL];
  char *tmp___0;
  char *tmp___1;
  char *tmp___2;
                    

  {
    {
      size_left = st->stat.st_size;
      block_ordinal = current_block_ordinal();
      blk = start_header(st);
    }
               
                                   
     
                                             
     
    {
      finish_header(st, blk, block_ordinal);
    }
    {
      while (1) {
                                     ;
        if (!(size_left > 0L)) {
          goto while_break;
        }
                                  
           

                                                                   
                                      
                                            
           

         
        {
          blk = find_next_block();
          bufsize = available_space_after(blk);
        }
        if ((size_t)size_left < bufsize) {
          bufsize = (size_t)size_left;
                                  
                      
             

                                                                          
             

           
        }
                     
                          
               {
           

            tmp = safe_read(fd, (void *)(blk->buffer), bufsize);
            count = tmp;
           

        }
                                            
           
                                                               
                                                                     
                                   
           
                                       
         
        {
          size_left = (off_t)((size_t)size_left - count);
          set_next_block_after(blk + (bufsize - 1UL) / 512UL);
        }
        if (count != bufsize) {
           
                                                                      
                                                    
                                     
                                                                             
                             
                                                                       
                     
                                                                          
                                                                           
                                                   
                                                                 
           
                                           
                            
           
           
	                           
	   
          return ((enum dump_status)1);
        }
      }
    while_break: /* CIL Label */;
    }
                                 
  }
}
static void dump_regular_finish(int fd, struct tar_stat_info *st,
                                time_t original_ctime) {
  struct stat final_stat;
  char *tmp;
  char *tmp___0;
  int tmp___1;
  int tmp___2;
  int tmp___3;
                    
                    

   
                  
       
                                         
       
                         
         

                                                      
         

              
                                                          
           

                                                                   
                                                                
                                                    
           

         
       
       
	                    
       
                         
         

                                                       
         

       
     
                              
       
                                                           
       
                          
         

                                                         
         

       
     
           
   
}
static void dump_dir0(char *directory, struct tar_stat_info *st, int top_level,
                      dev_t parent_device) {
  dev_t our_device;
  union block *blk;
  off_t block_ordinal;
  off_t tmp;
  off_t size_left;
  off_t totsize;
  size_t bufsize;
  ssize_t count;
  char const *buffer___2;
  char const *p_buffer;
  size_t size;
  size_t tmp___0;
  _Bool tmp___1;
  char *tmp___2;
  char *tmp___3;
  char const *entry;
  size_t entry_len;
  char *name_buf;
  char *tmp___4;
  size_t name_size;
  size_t tmp___5;
  size_t name_len;
  void *tmp___6;
  _Bool tmp___7;
                    

  {
    {
      our_device = st->stat.st_dev;
      tmp___1 = is_avoided_name((char const *)st->orig_file_name);
    }
                  {
      {
                                         
                                      
                            
                                      
        blk = start_header(st);
      }
                 
               
       
                               
                                         
              
                                         
       
                               {
         

          finish_header(st, blk, block_ordinal);
         

      }       
                                          
           
                                                    
                                                     
                               
           
                           
                                  
             
                         
                                              
                                 
                                   
                 
                 
                                             
                                       
                                                            
                                   
                 
               
                                         
             
           
           
                      
                                                                              
                                                  
                                  
                                
           
           
                       
                                                
                                      
                                     
               
                                        
                 

                                                                              
                                            
                                         
                 

               
               
                                        
                                                     
               
                                                
                                            
                                                   
                            
                   

                                                                
                                                   
                   

                 
               
               
                                                               
                                                                          
                                                                 
                                    
                                                                    
               
             
                                           
           
                                    
             

                                                         
             

           
                 
         
       
    }
                            
             
     
                                 
                       
                                               
                               
             

                                                                         
                       
                                                                               
                                                          
             

           
                 
         
       
     
    {
      tmp___4 = strdup((char const *)st->orig_file_name);
      name_buf = tmp___4;
      tmp___5 = strlen((char const *)name_buf);
      name_size = tmp___5;
      name_len = name_size;
      entry = (char const *)directory;
    }
    {
                {
                                         ;
        {
	  entry_len = strlen(entry);
	}
        if (!(entry_len != 0UL)) {
          goto while_break___1;
        }
                                              {
           

            name_size = name_len + entry_len;
            tmp___6 = xrealloc((void *)name_buf, name_size + 1UL);
            name_buf = (char *)tmp___6;
           

        }
        {
          strcpy((char * /* __restrict  */)(name_buf + name_len),
                 (char const * /* __restrict  */)entry);
          tmp___7 = excluded_name((char const *)name_buf);
        }
                      {
           

            dump_file(name_buf, 0, our_device);
           

        }
        entry += entry_len + 1UL;
      }
    while_break___1: /* CIL Label */;
    }
     
                             
     
           
  }
}
static void ensure_slash(char **pstr) {
  size_t len;
  size_t tmp;
  void *tmp___0;
  size_t tmp___1;

  {
    {
      tmp = strlen((char const *)*pstr);
      len = tmp;
    }
     
                 
                                      
                         
                                                     
                             
           
                
                           
         
              
       
                                 
     
                                       
       

                                                     
                                
       

     
    tmp___1 = len;
    len++;
    *(*pstr + tmp___1) = (char)'/';
    *(*pstr + len) = (char)'\000';
           
  }
}
static _Bool dump_dir(struct tar_stat_info *st, int top_level,
                      dev_t parent_device) {
  char *directory;

  {
    {
      directory = savedir((char const *)st->orig_file_name);
    }
                     
       
                                                       
       
                        
     
    {
      ensure_slash(&st->orig_file_name);
      ensure_slash(&st->file_name);
      dump_dir0(directory, st, top_level, parent_device);
                              
    }
    return ((_Bool)1);
  }
}
void create_archive(void) {
  char *p;
  size_t buffer_size;
  char *buffer___2;
  void *tmp;
  char const *q;
  _Bool tmp___0;
  size_t plen;
  size_t tmp___1;
  void *tmp___2;
  size_t tmp___3;
  size_t qlen;
  size_t tmp___4;
  void *tmp___5;
  _Bool tmp___6;
  _Bool tmp___7;

  {
    {
      open_archive((enum access_mode)1);
                             
    }
                             
       
                                   
                                   
                                 
                                 
       
       
                   
                                        
           
	                         
	   
                                                                  
                             
           
           
	                                             
	   
                         
             

                                         
             

           
         
                                   
       
       
	                  
       
       
                   
                                            
           
	                         
	   
                                                                  
                                 
           
           
	                                             
	   
                         
             
                                                
                             
             
                                      
               
                           
                                                    
                                     
                                               
                                         
                   
                                          
                 
                                               
               
               
                                                                    
                                             
               
             
             
                                                          
                                                              
             
                                                             
                             
                     
                                                  
             
                                            
                    
               

                           
                                                    
                            
                                         
                   
                   
                                        
                                   
                   
                                              
                                                    
                       
                                   
                                                            
                                             
                                                             
                                                 
                           
                                                  
                         
                                                       
                       
                       
                                                                            
                                                     
                       
                     
                     
                                                                           
                                                                      
                                                          
                     
                   
                                  
                 
                                               
               

             
           
         
                                       
       
       
	                         
       
           {
       

        while (1) {
                                           ;
          {
	    p = name_next(1);
	  }
          if (!((unsigned long)p != (unsigned long)((void *)0))) {
            goto while_break___4;
          }
          {
	    tmp___7 = excluded_name((char const *)p);
	  }
                        {
             

              dump_file(p, 1, (dev_t)0);
             

          }
        }
      while_break___4: /* CIL Label */;
       

    }
    {
      write_eot();
      close_archive();
    }
                                    
       

                               
       

     
           
  }
}
static unsigned int hash_link(void const *entry, unsigned int n_buckets) {
  struct link const *l;
  uintmax_t num;

  {
    l = (struct link const *)entry;
    num = (uintmax_t)(l->dev ^ l->ino);
    return ((unsigned int)(num % (unsigned long)n_buckets));
  }
}
static _Bool compare_links(void const *entry1, void const *entry2) {
  struct link const *link1;
  struct link const *link2;

  {
    link1 = (struct link const *)entry1;
    link2 = (struct link const *)entry2;
    return ((_Bool)(((link1->dev ^ link2->dev) | (link1->ino ^ link2->ino)) ==
                    0UL));
  }
}
static void unknown_file_error(char *p) {
  char *tmp;
  char *tmp___0;
                   

   
     
                                            
                                                               
                                              
     
                                     
                      
     
           
   
}
static Hash_table *link_table;
static _Bool dump_hard_link(struct tar_stat_info *st) {
  struct link lp;
  struct link *duplicate;
  off_t block_ordinal;
  union block *blk;
  char const *link_name;
  char *tmp;
  size_t tmp___0;
  int tmp___1;
  void *tmp___2;
                    

  {
                     
                                    
         
                                   
                                   
                   
                                                                               
                                             
         
                        
           
                                                                               
                                          
                                 
                                                    
                                                     
                                        
           
                                
             

                                  
             

           
           
                                          
                                   
           
                     
                              
           
           
                                                                       
                                             
                                                  
           
                                    
             
                                                                 
             
                               
               

                                                               
               

             
           
                            
         
       
     
    return ((_Bool)0);
  }
}
static void file_count_links(struct tar_stat_info *st) {
  struct link *duplicate;
  struct link *lp;
  size_t tmp;
  void *tmp___0;
  void *tmp___1;

   
                                  
       
                                                       
                 
                                                                              
                                    
                                  
                                  
                                      
                                                    
                                                                   
       
                       
                
              
         
                      
                                                                
                                                                            
                                                                   
         
                         
              
                                                              
                                             
         
                           
             

                           
             

           
                
           

	                 
	   

         
       
                                                          
         

                  
         

       
                    
     
           
   
}
void check_links(void) {
  struct link *lp;
  void *tmp;
  void *tmp___0;
  char *tmp___1;
                   

   
                      
             
     
     
                                                           
                              
     
     
                 
                                      
                  
                           
         
                        
           

                                                            
                                                         
           

         
         
                   
                                                                              
                                      
         
       
                                 
     
           
   
}
static void dump_file0(struct tar_stat_info *st, char *p, int top_level,
                       dev_t parent_device) {
  union block *header;
  char type;
  time_t original_ctime;
  struct utimbuf restore_times;
  off_t block_ordinal;
  int tmp;
  char *tmp___0;
  int tmp___1;
  char *tmp___2;
  char *tmp___3;
  char *tmp___4;
  char *tmp___5;
  _Bool tmp___6;
  _Bool tmp___7;
  int fd;
  enum dump_status status;
  char *tmp___8;
  char *tmp___9;
  int *tmp___10;
  _Bool tmp___11;
  _Bool tmp___12;
  char *buffer___2;
  int size;
  size_t linklen;
  void *tmp___13;
  ssize_t tmp___14;
  int tmp___15;
  char *tmp___16;
  char *tmp___17;
  _Bool tmp___20;
  unsigned int tmp___21;
  unsigned int tmp___22;
  int tmp___23;
                    
                    
                    
                    
                    

  {
                              
                             
       
                                              
       
                 
               
       
     
    {
      assign_string(&st->orig_file_name, (char const *)p);
      tmp___0 = safer_name_suffix((char const *)p, (_Bool)0);
      assign_string(&st->file_name, (char const *)tmp___0);
      tmp___1 = deref_stat(dereference_option, (char const *)p, &st->stat);
    }
                       
       
                                   
       
             
     
    {
                                               
                               
      original_ctime = st->stat.st_ctim.tv_sec;
                                                     
                                                      
    }
                                                   
                                                                
                    
              
                                                                   
                                                                      
                                 
                                     
                          
                    
                                                                        
                            
                      
                                                                           
                                                                              
                                         
                                        
                       

                                                                  
                                                                               
                                                                    
                       

                     
                           
                   
                 
               
             
           
         
       
     
     
                                        
     
                  
       
                                                  
                                                                 
                                                    
       
             
     
    if ((st->stat.st_mode & 61440U) == 16384U) {
      {
        dump_dir(st, top_level, parent_device);
      }
                                  
         

                                                                           
         

       
             
    } else {
       
	                                            
       
                     
               
             {
         
	                               
	 
                      
                 
         
        if ((st->stat.st_mode & 61440U) == 32768U) {
          goto _L___6;
        } else {
                 {
          _L___6 : /* CIL Label */
          {
                                           
          }
                          {
              {
                fd = open((char const *)st->orig_file_name, 0);
              }
                           
                                 
                   
                                                  
                   
                                       
                     

                               
                                                                           
                                                                              
                                                                  
                     

                          
                     

		                                                  
		     

                   
                        
                   

		                                                
		   

                 
                       
               
            }       
                      
             
                               {
               
                                             
               
                             
                 
                                                    
                 
                                                 
                   

                                                       
                   

                 
                     {
                 

		  status = dump_regular_file(fd, st);
		 

              }
            }       
               

		                                   
	       

             
             
                                               
                            
               
                                               
                            
               
                                               
                            
               
                                               
                            
               
                                
                                   
                                        
                 

                                                             
                 

               
               
		                                            
	       
                                
                                   
                                        
                 

                                                             
                 

               
               
		          
	       
                                
                                    
             
                        
             
                     
                                    
             
                      
             
                                          
             
                                        
               

                                                       
                                                                
               

             
             
	                           
	     
                   
          }       
                                                        
                                                 
                                                        
                 

                               
                 

                      
                                           
                   

                                 
                   

                 
               
               
                                                           
                                              
                                                                      
                                                                         
                                                   
                                     
               
                             
                 
                                                 
                 
                       
               
               
                                                    
                                                                        
               
                               
                 

                                      
                 

               
               
                                                        
                                              
                                          
               
                            
                       
               
               
                                                                               
                                          
                                                    
                                                         
               
                                        
                 
                                                     
                 
                                     
                   

                                                  
                   

                 
               
               
		                     
	       
                     
                    
                                                         
                                 
                      
                                                            
                                   
                        
                                                             
                                     
                          
                                                                
                       
                                                                   
                                                                 
                                                                      
                       
                             
                            
                       
			                      
		       
                             
                     
                   
                 
               
             
           
        }
      }
    }
                                             
       
                              
       
             
     
     
                                              
                                    
                                
     
                  
             
     
                                   
                          
       

                                                                       
                                                              
                                                        
                                                                       
                                                              
                                                        
       

     
     
                                               
     
                              
       
                                           
       
                           
         

                                        
         

       
     
           
  }
}
void dump_file(char *p, int top_level, dev_t parent_device) {
  struct tar_stat_info st;
                   

  {
    {
      tar_stat_init(&st);
      dump_file0(&st, p, top_level, parent_device);
                            
    }
           
  }
}
/* #pragma merger("0","003.delete.o.i","") */
_Bool ignore_zeros_option;
uintmax_t occurrence_option;
void delete_archive_members(void);
size_t recent_long_name_blocks;
size_t recent_long_link_blocks;
void truncate_warn(char const *name);
void name_gather(void);
void names_notfound(void);
struct name *name_scan(char const *path);
void xheader_decode(struct tar_stat_info *st);
int sys_truncate(int fd);
static union block *new_record;
static int new_blocks;
static _Bool acting_as_filter;
union block *recent_long_name;
union block *recent_long_link;
static off_t records_skipped;
static void move_archive(off_t count) {
  struct mtop operation;
  int tmp;
  int tmp___0;
  int tmp___1;
  int *tmp___2;
  int tmp___3;
  int tmp___4;
  int tmp___5;
  int tmp___6;
  off_t position0;
  off_t tmp___7;
  __off_t tmp___8;
  off_t tmp___9;
  off_t increment;
  off_t position;
  off_t tmp___10;
  __off_t tmp___11;
  off_t tmp___12;

  {
                      
             
     
                     
                                 
                                         
                                                    
            
                                 
                                      
                                                   
     
                  
                               
         

                            
                                  
                                                                             
                                         
                                                 
                                    
                        
         

              
         

                          
                      
                                                                             
                                              
                                    
                            
         

       
                         
               
       
       
	                             
       
                          
                                 
           

                                  
                                    
                                                                               
                                           
                                                   
                                      
                              
           

                
           

                            
                        
                                                                               
                                                
                                      
                              
           

         
                           
                 
         
       
     
                             
       

                                                                
                          
       

           {
       

        tmp___8 = lseek(archive, (off_t)0, 1);
        tmp___9 = tmp___8;
       

    }
    position0 = tmp___9;
    increment = (off_t)(record_size * (size_t)count);
    position = position0 + increment;
                                                     
       

                                                                
       

           {
                                                       
         

                                                                  
         

             {
                            
                              
                
                              
         
                                 
           

                                                                     
                                
           

               {
           

            tmp___11 = lseek(archive, position, 0);
                                
           

        }
                                   
           

                                                                    
           

         
      }
    }
           
  }
}
static void write_record(int move_back_flag) {
  union block *save_record;

  {
                               
    record_start = new_record;
                           
       

                    
                      
                    
       

           {
       

        move_archive((records_written + records_skipped) - records_read);
        flush_write();
       

    }
                               
                         
                              
         

                                                                           
         

       
     
    new_blocks = 0;
           
  }
}
static void write_recent_blocks(union block *h, size_t blocks) {
  size_t i;
  int tmp;

   
                  
     
                 
                                      
                            
                           
         
                         
                     
                                       
                                            
           

                            
           

         
            
       
                                 
     
           
   
}
static void write_recent_bytes(char *data, size_t bytes) {
  size_t blocks;
  size_t rest;

   
     
                             
                                    
                                                       
                                                                           
                                                                            
     
                       
       

                                                                     
                             
       

     
                 
                                        
       

                        
       

     
           
   
}
void delete_archive_members(void) {
  enum read_header logical_status;
  enum read_header previous_status;
  struct name *name;
  off_t blocks_to_skip;
  off_t blocks_to_keep;
  int kept_blocks_in_record;
  int tmp;
  enum read_header status;
  enum read_header tmp___0;
  char *tmp___1;
  char *tmp___2;
  void *tmp___3;
  enum read_header status___0;
  char *tmp___4;
  int count;
  int total_zero_blocks;
  int zero_blocks;
  int tmp___5;
  uintmax_t tmp___6;
  uintmax_t tmp___7;
                    
                    
                    

  {
    {
                                           
                                            
                                
                                
      name_gather();
      open_archive((enum access_mode)2);
                                                   
                                           
    }
    {
                {
                                     ;
        {
          tmp___0 = read_header((_Bool)1);
          status = tmp___0;
        }
        {
                                           
                        
           
                                           
                        
           
                                           
                        
           
                                           
                        
           
                                           
                        
           
                                           
                        
           
                            
                                
         
                  
         
                                
         
                                                                      
         
                                                                  
             
                            
             
                              
           
                                
                                         
                                        
                  
                                                                          
           
                         
             
                            
             
                              
           
                               
                                  
                            
                               
                                    
             
                                                   
             
                              
           
                               
                                               
                            
                                
         
                                               
         
           
                                                      
                              
             
                                                      
                              
             
                                                      
                              
             
                                                      
                              
             
                                                      
                              
             
                                                      
                              
             
                                  
                                      
           
                                                                       
                                               
           
                                      
                                      
                                       
            
                                                         
                                               
                            
           
                                     
                                  
                                      
           
                    
           
                                            
           
                            
                                      
        }
        previous_status = status;
                                                    
                           
         
      }
    while_break: /* CIL Label */;
    }
    {
                                          
      tmp___3 = xmalloc(record_size);
      new_record = (union block *)tmp___3;
    }
    if ((unsigned int)logical_status == 1U) {
      goto _L___3;
    } else {
                                              {
      _L___3: /* CIL Label */
        write_archive_to_stdout = (_Bool)0;
                                                         
                         
           

                                                        
                                                                
                                               
           

         
                                                 
                                               
                          
         
        {
          while (1) {
          while_continue___0: /* CIL Label */;
                                                                            
               

                                
               

             
            {
              status___0 = read_header((_Bool)0);
                                                 
            }
                                                 
                                        
                 
                                                       
                 
                                        
               
             
                                                 
                                                   
                                   
                   {
              if ((unsigned int)status___0 == 3U) {
                logical_status = (enum read_header)4;
                goto while_break___0;
              }
            }
                                                 
               
                                                                      
                                                   
                                
                                                     
               
                                      
             
            {
	      name = name_scan((char const *)current_stat_info.file_name);
	    }
            if ((unsigned long)name != (unsigned long)((void *)0)) {
              (name->found_count)++;
                                            {
                                            
              }       
                                                                              
               
                           {
              flush_file : {
                set_next_block_after(current_header);
                                
                                                                          
              }
                 
                             
                                                      
                                                                          
                                           
                     
                     
                                                                   
                                      
                     
                   
                                                 
                 
                                                
                                          
                goto while_continue___0;
              }
            }
                                       
               

                                                          
                                                         
               

                    
               

                                                                               
                                                                               
               

             
            {
                                                           
                           
              blocks_to_keep =
                  ((current_stat_info.stat.st_size + 512L) - 1L) / 512L;
                                                   
            }
                                                
               

                                
               

             
            kept_blocks_in_record = (int)(record_end - current_block);
            if ((off_t)kept_blocks_in_record > blocks_to_keep) {
              kept_blocks_in_record = (int)blocks_to_keep;
            }
            {
                        {
                                                 ;
                                      
                                       
                 
                                                                                
                   
                                 
                                                 
                                                            
                   
                                                                      
                                                                
                   
                 
                count = kept_blocks_in_record;
                if (blocking_factor - new_blocks < count) {
                  count = blocking_factor - new_blocks;
                }
                             
                   

                            
                   

                 
                {
                  memcpy((void * /* __restrict  */)(new_record + new_blocks),
                         (void const * /* __restrict  */)current_block,
                         (size_t)(count * 512));
                  new_blocks += count;
                  current_block += count;
                                                 
                                                 
                }
                if (new_blocks == blocking_factor) {
                   

                    write_record(1);
                   

                }
              }
            while_break___2: /* CIL Label */;
            }
          }
        while_break___0: /* CIL Label */;
        }
      }
    }
                                             
                            
       
                   
                                            
           
                                                       
                                                        
                                                
                                             
                                                
           
                                         
                                 
           
         
                                       
       
     
     
                               
     
                            
                                  
         
                                          
         
                      
           

                                                     
           

         
       
     
     
                      
                       
     
           
  }
}
/* #pragma merger("0","004.extract.o.i","") */
extern __attribute__((__nothrow__)) int(
    __attribute__((__nonnull__(1, 2), __leaf__))
    stat)(char const *__restrict __file, struct stat *__restrict __buf);
extern __attribute__((__nothrow__)) int(
    __attribute__((__nonnull__(1, 2), __leaf__))
    lstat)(char const *__restrict __file, struct stat *__restrict __buf);
extern
    __attribute__((__nothrow__)) int(__attribute__((__nonnull__(1), __leaf__))
                                     chmod)(char const *__file,
                                            __mode_t __mode);
extern __attribute__((__nothrow__))
__mode_t(__attribute__((__leaf__)) umask)(__mode_t __mask);
extern
    __attribute__((__nothrow__)) int(__attribute__((__nonnull__(1), __leaf__))
                                     mkdir)(char const *__path,
                                            __mode_t __mode);
extern
    __attribute__((__nothrow__)) int(__attribute__((__nonnull__(1), __leaf__))
                                     mknod)(char const *__path, __mode_t __mode,
                                            __dev_t __dev);
extern
    __attribute__((__nothrow__)) int(__attribute__((__nonnull__(1), __leaf__))
                                     mkfifo)(char const *__path,
                                             __mode_t __mode);
extern
    __attribute__((__nothrow__)) int(__attribute__((__nonnull__(1), __leaf__))
                                     access)(char const *__name, int __type);
extern
    __attribute__((__nothrow__)) int(__attribute__((__nonnull__(1), __leaf__))
                                     chown)(char const *__file, __uid_t __owner,
                                            __gid_t __group);
extern
    __attribute__((__nothrow__)) int(__attribute__((__nonnull__(1), __leaf__))
                                     lchown)(char const *__file,
                                             __uid_t __owner, __gid_t __group);
extern __attribute__((__nothrow__))
__uid_t(__attribute__((__leaf__)) geteuid)(void);
extern __attribute__((__nothrow__)) int(__attribute__((
    __nonnull__(1, 2), __leaf__)) link)(char const *__from, char const *__to);
extern __attribute__((__nothrow__)) int(
    __attribute__((__nonnull__(1, 2), __leaf__))
    symlink)(char const *__from, char const *__to);
char *base_name(char const *name);
void (*xalloc_fail_func)(void);
size_t full_write(int fd, void const *buf, size_t count);
_Bool absolute_names_option;
enum old_files old_files_option;
_Bool recursive_unlink_option;
int same_owner_option;
int same_permissions_option;
size_t strip_path_elements;
_Bool touch_option;
_Bool we_are_root;
void extr_init(void);
void extract_archive(void);
void extract_finish(void);
void gnu_restore(char const *directory_name);
char const *tartime(time_t t);
void print_for_mkdir(char *pathname, int length, mode_t mode);
void skip_file(off_t size);
void extract_mangle(void);
int remove_any_file(char const *path, enum remove_option option);
void chmod_error_details(char const *name, mode_t mode);
void chown_error_details(char const *name, uid_t uid, gid_t gid);
void link_error(char const *target, char const *source);
void mkdir_error(char const *name);
void mkfifo_error(char const *name);
void mknod_error(char const *name);
void symlink_error(char const *contents, char const *name);
void utime_error(char const *name);
void write_error_details(char const *name, size_t status, size_t size);
size_t stripped_prefix_len(char const *file_name, size_t num);
_Bool contains_dot_dot(char const *name);
_Bool sparse_member_p(struct tar_stat_info *st);
enum dump_status sparse_extract_file(int fd, struct tar_stat_info *st,
                                     off_t *size);
static mode_t newdir_umask;
static mode_t current_umask;
static struct delayed_set_stat *delayed_set_stat_head;
static struct delayed_symlink *delayed_symlink_head;
void extr_init(void) {
  __uid_t tmp;

   
     
                      
                                       
                                                  
                                            
                                         
                                        
     
                                      
                                
            
       

                            
                                     
       

     
           
   
}
static void set_mode(char const *file_name, struct stat const *stat_info,
                     struct stat const *cur_info, mode_t invert_permissions,
                     enum permstatus permstatus, char typeflag) {
  mode_t mode;
  struct stat st;
  int tmp;
  int tmp___0;
                    

   
                                      
                                           
                                          
                                             
                                        
                                                                  
                                                                      
                                                                           
                                      
                                        
                       
               
             
           
         
              
                    
       
            
                           
                                
               
              
                        
           
                                                                 
                                                               
           
                         
             
                                    
             
                   
           
                                                
         
                                           
                                                                
       
     
     
                                       
     
                       
       

                                             
       

     
           
   
}
static void check_time(char const *file_name, time_t t) {
  time_t now;
  char const *tmp;
  char *tmp___0;
  char const *tmp___1;
  char *tmp___2;
                   
                   

   
                  
       

                         
                                                               
                                                           
       

            
                                      
         
                                  
         
                      
           

                                 
                                                                          
                                                                  
                                            
           

         
       
     
           
   
}
static void set_stat(char const *file_name, struct stat const *stat_info,
                     struct stat const *cur_info, mode_t invert_permissions,
                     enum permstatus permstatus, char typeflag) {
  struct utimbuf utimbuf;
  int tmp;
  int tmp___0;
  int tmp___1;

   
                              
                          
                                             
                                   
                                                                 
                  
                                                   
           
           
                                                                  
                                                                       
           
                        
             

                                     
             

                  
             

                                                    
                                                     
             

           
         
       
       
                                                                                
                           
       
     
                                
                                           
                                  
           
                                                                   
                                                         
           
                            
             

                                                                      
                                                            
             

           
                
           
                                                                  
                                                        
           
                            
             

                                                                      
                                                            
             

           
                                           
             

                                                                    
                                                                 
             

           
         
       
     
           
   
}
static void delay_set_stat(char const *file_name, struct stat const *stat_info,
                           mode_t invert_permissions,
                           enum permstatus permstatus) {
  size_t file_name_len;
  size_t tmp;
  struct delayed_set_stat *data;
  void *tmp___0;

   
     
                              
                          
               
                                                                                
                                   
                       
                                                
                                          
                                                         
                                                        
                                                    
                                    
                                      
                                                  
                                         
                                   
     
           
   
}
static void repair_delayed_set_stat(char const *dir,
                                    struct stat const *dir_stat_info) {
  struct delayed_set_stat *data;
  struct stat st;
  int tmp;
  char *tmp___0;
  char *tmp___1;
                   
                   

   
                                 
     
                 
                                      
                    
                           
         
         
                                                                       
                                                             
         
                       
           
                                                        
           
                 
         
                                                          
                                                            
                                                     
                                      
                                                                      
                                                                          
                                                                            
                                                              
                                                  
                   
           
         
                          
       
                                 
     
     
                                    
                                                                              
                                                  
                      
     
           
   
}
static int make_directories(char *file_name) {
  char *cursor0;
  char *cursor;
  int did_something;
  int mode;
  int invert_permissions;
  int status;
  int *tmp;
  int tmp___0;
  int *tmp___1;

  {
                            
    did_something = 0;
                     
     
                 
                                      
                       
                           
         
                                    
                      
         
                                                              
                      
                
                                          
                        
           
         
                                        
                                                                      
                        
                  
                                            
                          
                    
                                              
                                                                            
                              
                        
                                                  
                                
                   
                 
               
             
           
         
                               
              
                                                                        
                                                                            
                                                                              
                                 
                          
                                 
                
                                           
         
         
                                                 
                                                                
         
                          
           
                                                   
                                                                          
                                                                           
                                                                                
                              
                                
           
                      
         
         
                              
                                       
         
                             
                      
                
           
	                             
	   
                           
                        
                  
                    
                                    
             
                                                           
             
                                 
                            
               
             
           
         
                         
             
                 
       
                                 
     
    return (did_something);
  }
}
static _Bool file_newer_p(char const *file_name,
                          struct tar_stat_info *tar_stat) {
  struct stat st;
  int tmp;
                   

  {
     
                                                           
                                                         
     
             {
       
                             
       
      return ((_Bool)1);
    }
                                             
                                                               
                          
       
     
                      
  }
}
static int prepare_to_extract(char const *file_name) {
  int tmp;
  int *tmp___0;
  int *tmp___1;
  char *tmp___2;
  _Bool tmp___3;
                   

  {
                           
                 
     
     
                                                 
                    
       
                                                 
                    
       
                          
                            
     
                                      
                                                                         
     
                 
         
                                       
         
                       
           
                                         
           
                              
             
                                      
             
                       
           
         
       
                        
                            
     
                                                            
     
                    
         
                                                      
                                                        
         
                   
       
                        
                                   
                        
                                  
     
    return (1);
  }
}
static int maybe_recoverable(char *file_name, int *interdir_made) {
  int e;
  int *tmp;
  int *tmp___0;
  int *tmp___1;
  _Bool tmp___2;
  int r;
  int tmp___3;
  int *tmp___4;
  int *tmp___5;
  int tmp___6;

  {
     
                               
               
     
                        {
      return (0);
    }
     
                                   
     
     
                           
                     
       
                          
                        
       
                          
                             
     
                                                 
                    
       
                                                 
                    
       
                                                 
                    
       
                                                 
                    
       
                                                 
                    
       
                                                 
                    
       
                            
                           
                 
                            
     
                                                                          
     
                    
         
                                       
                       
         
                   
       
                            
                            
                             
      
                                                                                
                  
                                   
                    
     
                 
                           
                            
                                      
     
                                
     
                                            
     
                     
         
                                       
                       
         
                   
       
                         
                 
                                   
                 
                                  
     
  }
}
static void apply_nonancestor_delayed_set_stat(char const *file_name,
                                               _Bool after_symlinks) {
  size_t file_name_len;
  size_t tmp;
  _Bool check_for_renamed_directories;
  struct delayed_set_stat *data;
  _Bool skip_this_one;
  struct stat st;
  struct stat const *cur_info;
  int tmp___0;
  char *tmp___1;
  char *tmp___2;
  int tmp___3;
                    
                    
                    

   
     
                              
                          
                                               
     
     
                 
                                      
                                     
                           
         
                                     
                                 
                                          
                                                
                                                                            
                                                              
                           
                
                                                    
                                                     
                                                                          
                            
                      
                                                                              
                         
                                        
                 
                                                           
                                                                   
                                                        
                 
                                     
                                     
                   
                 
               
             
           
         
                                            
           
                                                  
                                                                             
                                                                   
           
                             
             

                                                          
                                       
             

                  
                                                      
                                                           
                 

                                                                            
                                                                              
                                                          
                                                              
                                  
                                           
                 

               
                    
               

                                                                          
                                                                            
                                                        
                                                            
                                
                                         
               

             
           
         
                             
           

                                                     
                                                                       
                                                                            
           

         
         
                                             
                             
         
       
                                 
     
           
   
}
static int conttype_diagnosed;
void extract_archive(void) {
  union block *data_block;
  int fd;
  int status;
  size_t count;
  size_t written;
  int openflag;
  mode_t mode;
  off_t size;
  int interdir_made;
  char typeflag;
  char *file_name;
  int tmp;
  size_t prefix_len;
  size_t tmp___0;
  int e;
  int *tmp___1;
  char *tmp___2;
  char *tmp___3;
  _Bool tmp___4;
  _Bool tmp___5;
  int tmp___6;
  char *tmp___7;
  int tmp___8;
  char *tmp___9;
  int *tmp___10;
  int tmp___11;
  int tmp___12;
  struct stat st;
  int tmp___13;
  struct delayed_set_stat *h;
  struct delayed_symlink *p;
  size_t tmp___14;
  void *tmp___15;
  size_t tmp___16;
  void *tmp___17;
  int tmp___18;
  int tmp___19;
  char *tmp___20;
  int tmp___21;
  int tmp___22;
  _Bool tmp___23;
  int tmp___24;
  char const *link_name;
  char *tmp___25;
  struct stat st1;
  struct stat st2;
  int e___0;
  struct delayed_symlink *ds;
  struct string_list *p___0;
  size_t tmp___26;
  void *tmp___27;
  int tmp___28;
  int tmp___29;
  int *tmp___30;
  int *tmp___31;
  int tmp___32;
  int tmp___33;
  int tmp___34;
  int tmp___35;
  int tmp___36;
  int tmp___37;
  struct stat st___0;
  int tmp___38;
  int *tmp___39;
  int *tmp___40;
  int tmp___41;
  int *tmp___42;
  char const *tmp___43;
  char *tmp___44;
  char *tmp___45;
  char *tmp___46;
  char *tmp___47;
  char *tmp___48;
  char *tmp___49;
  int tmp___50;
  int tmp___51;
  int tmp___52;
  int tmp___53;
  int tmp___54;
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    

  {
    {
                        
      set_next_block_after(current_header);
      decode_header(current_header, &current_stat_info, &current_format, 1);
    }
                             
       
                                                                            
       
                 
         
                        
         
               
       
     
                        {
       

        print_header(&current_stat_info, (off_t)-1);
       

    }
    {
      file_name = safer_name_suffix((char const *)current_stat_info.file_name,
                                    (_Bool)0);
    }
                              
       
                 
                                                                              
                             
       
                                               
         
                        
         
               
       
                              
     
     
                                                                            
     
                        
                              
         
                                                                  
         
                       
           
                                         
                         
                                                              
                                                                    
                                                        
                            
                          
           
                 
         
       
     
     
                                                    
     
                 {
      tmp___50 = 'S';
    }       
                                                      
     
    typeflag = (char)tmp___50;
    {
                                
                     
       
                               
                     
       
                                
                     
       
                                
                     
       
                                
                     
       
                                
                     
       
                                
                     
       
                                
                     
       
                                
                     
       
                                
                        
       
                                
                        
       
                                
                     
       
                                
                     
       
                                
                     
       
                                
                     
       
                                
                     
       
                          
                            
                            
                             
                             
                                                  
                        
       
    again_file:
                                                {
        tmp___51 = 512;
      }       
                       
       
      openflag = 65 | tmp___51;
      mode = (current_stat_info.stat.st_mode &
              (unsigned int)(((64 | (64 >> 3)) | ((64 >> 3) >> 3)) |
                             (((128 | (128 >> 3)) | ((128 >> 3) >> 3)) |
                              ((256 | (256 >> 3)) | ((256 >> 3) >> 3))))) &
             ~current_umask;
                             
               
                          
       
       
	                                                      
       
                     
         
                        
         
                            
           

                               
           

         
                          
       
                                
                                  
           

                                   
                                                                              
                                               
           

         
       
      {
	fd = open((char const *)file_name, openflag, mode);
      }
                   
         
                                                                 
         
                      
                          
         
         
                                              
                        
         
                            
           

                               
           

         
                          
       
    extract_file:
                                        
         

                                                             
         

             {
        size = current_stat_info.stat.st_size;
        {
          while (1) {
                                         ;
            if (!(size > 0L)) {
              goto while_break;
            }
                                      
               

                                         
                                                                         
                                                              
                                     
               

             
            {
	      data_block = find_next_block();
	    }
                              
               
                                                               
                                                   
                                
               
                               
             
            {
	      written = available_space_after(data_block);
	    }
            if (written > (size_t)size) {
              written = (size_t)size;
            }
            {
                                            
                            
              count =
                  full_write(fd, (void const *)(data_block->buffer), written);
              size = (off_t)((size_t)size - count);
              set_next_block_after(
                  (union block *)((data_block->buffer + written) - 1));
            }
                                   
               
                                                                             
               
                               
             
          }
        while_break: /* CIL Label */;
        }
      }
       
	                
       
                                
         

                                                     
         

       
                             
                          
       
       
	                   
       
                       
         
                                               
         
                            
           

                               
           

         
       
                                                 
                     
              
                     
       
       
                                         
                                                                
                                                                              
                           
       
                        
                             
     
                                                             
     
                      
                          
       
                                  
                
              
                                                            
                      
                
           
                      
                                                                            
           
                         
                    
                       
                                                
               
		                                           
	       
                              
                                     
               
               
		                                                        
	       
                              
                                     
               
             
                                           
           
                        
                         
               

                                                    
               

                    
               
		                          
	       
                                  
                 

                                                      
                            
                 

                      
                 
		                       
		 
                                    
                   

                                                         
                   

                        
                   
                              
                                                                          
                              
                                                                               
                                                                 
                                            
                                     
                                                           
                                                   
                                             
                                       
                                       
                                                 
                                                           
                                                           
                                                               
                                       
                                                                              
                                    
                             
                                                                
                                                                 
                                                                            
                                                                      
                                                                 
                                                           
                                                            
                                              
                   
                          
                                             
                       
                                                                   
                                                                        
                                                             
                       
                                          
                                                                         
                           
                                                                          
                           
                                                        
                                                                              
                                                   
                             

                                         
                                                                  
                                 
                                                               
                                                  
                                                                       
                                                        
                                                                              
                                 
                                                    
                                   

                                                                             
                                   

                                        
                                                                  
                                                                  
                                 
                                            
                                        
                                                              
                                                         
                                   
                                        
                                                       
                                 
                               
                                                             
                             

                           
                         
                       
                     
                   
                             
                 
               
             
                  
                
                       
                                                
               
                                                                           
                                                          
               
                                   
                                     
               
               
		                                                        
	       
                              
                                     
               
             
                                           
           
                              
               

                                                 
                                                                        
                                                                               
                                    
               

                    
               

                                                                        
                                                       
               

             
           
         
       
                        
                            
           

                               
           

         
       
                        
                             
     
                                                             
     
                      
                          
       
                  
                                                                             
                                             
                                         
                                                        
     
                        
                                  
                 
           
                                                                      
                                                                     
           
                              
             

                         
                                                  
                          
                                       
                 
                                            
                                              
                                                          
                       
                                                                   
                                  
                                                                               
                                                                     
                                                
                                         
                                                               
                                                                         
                                                                          
                                                  
                                            
                       
                                           
                     
                   
                 
                              
               
                                             
             

           
         
                          
       
       
	                                                        
       
                     
                        
       
                               
         
                                        
         
                              
                            
         
       
       
                                      
                          
                                                                  
                                                                 
       
                          
         
                                                                    
                                                                   
         
                            
                                         
                                           
                                
             
           
         
       
       
	                                               
       
                          
         

                             
         

       
                        
                            
                                              
                     
                            
                                               
                 
	                                                       
       
                      
                          
       
       
                                                                               
                                                       
       
                        
         
                                                                  
         
                       
                         
         
         
	                                       
	 
                            
           

                               
           

         
                          
       
       
                                         
                                                                
                                                                       
                           
       
                        
                             
     
                                                             
     
                      
                          
       
       
                   
                                            
           
                    
                                                                                
           
                               
                                 
           
           
	                                                            
	   
                          
                                 
           
         
                                       
       
                        
         

                                           
                                                                  
                                                               
                                                 
         

              
         
	                                        
	 
                            
           

                               
           

         
       
                        
               
                            
                             
                                
         

                                               
         

              
                                  
           

                          
           

         
       
                        
                     
              
                       
       
       
                                                                          
                                                                     
                                                                         
                                                                          
                                                             
       
                        
                          
       
                       
                              
       
                 
	                                              
       
                        
         
                                        
         
                              
                              
                        
                  
                                                       
                          
                    
                                                         
                                      
               
                                                                          
                                                                            
               
                                    
                                      
                     
                                                                      
                                                                              
                     
                                      
                   
                                                            
                                                           
                                          
                   
                 
                 
                                                
                                 
                 
               
             
           
         
         
	                                                          
	 
                       
                         
         
         
	                                
	 
                              
           
                                                 
           
                              
             

                                 
             

           
                            
         
       
                     
                        
                    
              
                                                   
                      
                
                                                     
                                 
                              
                           
                    
                           
             
             
                             
                                          
                                                                 
                                                                        
                                                                            
                                                                              
                                                              
                                             
             
           
         
       
                        
                            
                           
         

                                                                      
                                             
                                                    
                                                                      
         

       
                        
                             
     
                       
     
                        
                             
     
                                                                           
                         
                                                                         
                                                    
                      
                    
     
                          
         

                             
         

       
                        
                             
                             
      
                                                    
                                          
                      
                    
     
                          
         

                             
         

       
                        
                                    
     
                                                         
                
                                                                            
                                                                   
     
                      
                                  
    }
           
  }
}
static void apply_delayed_symlinks(void) {
  struct delayed_symlink *ds;
  struct string_list *sources;
  char const *valid_source;
  char const *source;
  struct stat st;
  int tmp;
  int tmp___0;
  int tmp___1;
  int tmp___2;
  struct string_list *next;
  struct delayed_symlink *next___0;
                    

   
                              
     
                 
                                      
                  
                           
         
                              
                                       
                              
         
                     
                                              
                           
                                   
             
             
                                                       
                                                                     
                                                                      
             
                               
                                         
                                           
                                                       
                     
                                               
                     
                                       
                       

                                             
                       

                            
                                         
                         
                                                               
                         
                                              
                                  
                         
                              
                            
                                                                          
                       
                                       
                           

                                                                              
                           

                                
                           

                                                  
                                                
                                                
                                                                        
                                                                       
                                                                    
                           

                         
                       
                     
                   
                 
               
             
                                    
           
                                         
         
                              
         
                     
                                              
                           
                                   
             
             
                                   
                                    
                             
             
           
                                         
         
         
                              
                           
                        
         
       
                                 
     
                                                       
           
   
}
void extract_finish(void) {
                   
                   

   

     
                                                       
                               
                                                       
     
           
   

}
__attribute__((__noreturn__)) void fatal_exit(void);
void fatal_exit(void) {
  char *tmp;
                   

   
     

                       
                                                             
                                     
              
     

   
}
/* #pragma merger("0","005.xheader.o.i","") */
extern __attribute__((__nothrow__)) unsigned short const **(
    __attribute__((__leaf__)) __ctype_b_loc)(void)__attribute__((__const__));
extern __attribute__((__nothrow__)) char *(
    __attribute__((__nonnull__(2), __leaf__))
    strtok)(char *__restrict __s, char const *__restrict __delim);
extern __attribute__((__nothrow__)) char *(
    __attribute__((__nonnull__(1, 2), __leaf__))
    stpcpy)(char *__restrict __dest, char const *__restrict __src);
extern __attribute__((__nothrow__))
__pid_t(__attribute__((__leaf__)) getpid)(void);
extern __attribute__((__nothrow__)) unsigned long(__attribute__((
    __nonnull__(1), __leaf__)) strtoul)(char const *__restrict __nptr,
                                        char **__restrict __endptr, int __base);
extern
    __attribute__((__nothrow__)) void *(__attribute__((__leaf__))
                                        calloc)(size_t __nmemb, size_t __size)
        __attribute__((__malloc__));
extern __attribute__((__nothrow__)) char *(__attribute__((__nonnull__(1),
                                                          __leaf__))
                                           getenv)(char const *__name);
char *dir_name(char const *path);
strtol_error xstrtoumax(char const *s, char **ptr, int strtol_base,
                        uintmax_t *val, char const *valid_suffixes);
__attribute__((__noreturn__)) void usage(int status);
void xheader_decode_global(void);
void xheader_read(union block *p, size_t size);
void xheader_destroy(struct xheader *xhdr);
char *xheader_ghdr_name(void);
void xheader_set_option(char *string);
_Bool utf8_convert(_Bool to_utf, char const *input, char **output);
extern void _obstack_newchunk(struct obstack *, int);
extern int _obstack_begin(struct obstack *, int, int, void *(*)(long),
                          void (*)(void *));
extern void obstack_free(struct obstack *obstack, void *block);
static _Bool xheader_protected_pattern_p(char const *pattern);
static _Bool xheader_protected_keyword_p(char const *keyword);
static __attribute__((__noreturn__)) void xheader_set_single_keyword(char *kw);
static void code_string(char const *string, char const *keyword,
                        struct xheader *xhdr);
static void extended_header_init(void);
static size_t global_header_count;
static struct keyword_list *keyword_pattern_list;
static struct keyword_list *keyword_global_override_list;
static struct keyword_list *keyword_override_list;
static struct keyword_list *global_header_override_list;
static char *exthdr_name;
static char *globexthdr_name;
static _Bool xheader_keyword_deleted_p(char const *kw) {
  struct keyword_list *kp;
  int tmp;

  {
                              
     
                 
                                      
                  
                           
         
         
	                                                  
	 
                       
                            
         
                      
       
                                 
     
    return ((_Bool)0);
  }
}
static _Bool xheader_keyword_override_p(char const *keyword) {
  struct keyword_list *kp;
  int tmp;

  {
                               
     
                 
                                      
                  
                           
         
         
	                                                   
	 
                       
                            
         
                      
       
                                 
     
    return ((_Bool)0);
  }
}
static void xheader_list_append(struct keyword_list **root, char const *kw,
                                char const *value) {
  struct keyword_list *kp;
  void *tmp;
  char *tmp___0;

   
     
                                 
                                      
                                
     
                
       

                                 
                            
       

            
                                      
     
                     
               
           
   
}
static void xheader_list_destroy(struct keyword_list **root) {
  struct keyword_list *kw;
  struct keyword_list *next;

   
               
                 
       
                   
                                        
                    
                             
           
           
                            
                                      
                                    
                             
                      
           
         
                                   
       
                                                 
     
           
   
}
static __attribute__((__noreturn__)) void xheader_set_single_keyword(char *kw);
static void xheader_set_single_keyword(char *kw) {
  char *tmp;
                   

   
     

                                                                   
                                         
               
     

   
}
static void xheader_set_keyword_equal(char *kw, char *eq) {
  _Bool global;
  char *p;
  unsigned short const **tmp;
  unsigned short const **tmp___0;
  char *tmp___1;
  _Bool tmp___2;
  char *tmp___3;
  _Bool tmp___4;
  int tmp___5;
  int tmp___6;
  int tmp___7;
                    
                    

   
                      
           
                                
          
                        
     
     
                 
                                      
                                                   
           
                                  
           
                                                         
                             
           
                
                           
         
            
       
                                 
     
                 
               
     
                 
                                          
                 
           
                                      
           
                                                             
                                 
           
                
                               
         
            
       
                                     
     
     
                                                   
     
                       
       
                                                               
       
                    
         

                                                         
                                                
                   
         

       
       
                                                                   
                                                       
       
            
       
	                                                  
       
                         
         

                                                       
         

              
         
	                                                        
	 
                           
           

                                                             
           

                
           
	                                                            
	   
                        
             

                                                                   
                                                     
                       
             

           
                       
             

                                                                
                                                                     
             

                  
             

                                                                           
                                                   
             

           
         
       
     
           
   
}
void xheader_set_option(char *string) {
  char *token;
  char *p;
  char *tmp;

   
     
                                                      
                                                           
     
     
                 
                                      
                     
                           
         
         
                                                 
                  
         
                 
           

                                              
           

                
           

	                                        
	   

         
         
                                                               
                                                               
         
       
                                 
     
           
   
}
static void to_decimal(uintmax_t value, char *where, size_t size) {
  size_t i;
  size_t j;
  size_t tmp;
  size_t tmp___0;
  char c;

   
                  
            
        
                             
     
                 
                                      
                    
            
                                                         
                      
                       
                       
                             
           
                
                           
         
       
                                 
     
                  
        
     
                 
                                          
                       
                               
         
                         
                                    
                         
            
            
       
                                     
     
           
   
}
static char *xheader_format_name(struct tar_stat_info *st, char const *fmt,
                                 _Bool allow_n) {
  char *buf;
  size_t len;
  size_t tmp;
  char *q;
  char const *p;
  char *dir;
  char *base;
  char pidbuf[64];
  char nbuf[64];
  char *tmp___0;
  size_t tmp___1;
  size_t tmp___2;
  __pid_t tmp___3;
  size_t tmp___4;
  size_t tmp___5;
  char *tmp___6;
  void *tmp___7;
  char *tmp___8;
  char const *tmp___9;
  char *tmp___10;
  char const *tmp___11;
  char *tmp___12;
  char const *tmp___13;
  char *tmp___14;
  char const *tmp___15;
                    
                    

  {
     
                        
                
                                
                                 
              
     
    {
                {
                                     ;
                {
           
                                     
                                      
           
                   
                             
           
        }       
                           
         
        {
                                            
                         
           
                                             
                          
           
                                             
                          
           
                                             
                          
           
                                             
                          
           
                            
                                
                
                            
                                 
                   
             

                                                                   
                                                                       
                                                  
                                   
             

           
                            
                                 
                   
             

                                                                 
                                                   
                                   
             

           
                            
                                  
         
                             
                                                                 
                                                   
                               
         
                            
        case_110: /* CIL Label */
                       {
             

                                                                            
              tmp___5 = strlen((char const *)(nbuf));
              len += tmp___5 - 1UL;
             

          }
                            
                                      
        }
            
      }
    while_break: /* CIL Label */;
    }
    {
      tmp___7 = xmalloc(len + 1UL);
      buf = (char *)tmp___7;
              
              
    }
     
                 
                                          
                  
                               
         
                                    
           

                                              
                               
             
                                               
                                
             
                                               
                                
             
                                               
                                
             
                                               
                                
             
                                
                                      
                        
                
                        
                
                                      
                
                                  
                                       
                      
               

                                                       
                                                                
               

             
                   
                                  
                                       
                       
               

                                                       
                                                                 
               

             
                   
                                  
                                        
           
                                                   
                                                                 
                   
           
                                  
                                       
                          
               

                                                       
                                                                   
                       
               

             
                                         
                         
                
                         
                
                                        
                     
                           
                  
                           
                  
                                          
             
                                            
           

                
                       
              
                       
              
                                      
         
       
                                     
     
     
                 
                                          
                                                    
                                        
                                 
           
                
                               
         
            
       
                                     
     
                 
    return (buf);
  }
}
char *xheader_xhdr_name(struct tar_stat_info *st) {
  char *tmp;
                   

  {
                       
       

                                                           
       

     
    {
      tmp = xheader_format_name(st, (char const *)exthdr_name, (_Bool)0);
    }
    return (tmp);
  }
}
char *xheader_ghdr_name(void) {
  size_t len;
  char const *tmp;
  char *tmp___0;
  size_t tmp___1;
  void *tmp___2;
  char *tmp___3;
                   
                   
                   
                    

  {
                           
       
                                   
                                    
       
                 
                     
       
       
                              
                                                    
                               
                                          
                                                         
                                                    
                                                         
                                                                     
       
     
    {
      tmp___3 = xheader_format_name((struct tar_stat_info *)((void *)0),
                                    (char const *)globexthdr_name, (_Bool)1);
    }
    return (tmp___3);
  }
}
void xheader_write(char type, char *name, struct xheader *xhdr) {
  union block *header;
  size_t size;
  char *p;
  size_t len;

   
     
                        
                                                              
                                     
                                   
                       
     
     
                 
                                      
         
                                     
                            
         
                         
                     
         
         
                                                            
                                                         
         
                          
           

                                                                   
           

         
         
                   
                      
                                       
         
                            
                           
         
       
                                 
     
     
                            
     
           
   
}
void xheader_write_global(void) {
  char *name;
  struct keyword_list *kp;

   
                                        
             
     
     
                             
                                        
     
     
                 
                                      
                  
                           
         
         
                                                                         
                                        
                        
         
       
                                 
     
     
                                       
                                 
                                                       
                         
                            
     
           
   
}
struct xhdr_tab const xhdr_tab[17];
static struct xhdr_tab const *locate_handler(char const *keyword) {
  struct xhdr_tab const *p;
  int tmp;

  {
                 
     
                 
                                      
                          
                           
         
         
	                                                  
	 
                       
                     
         
            
       
                                 
     
    return ((struct xhdr_tab const *)((void *)0));
  }
}
static _Bool xheader_protected_pattern_p(char const *pattern) {
  struct xhdr_tab const *p;
  int tmp;

  {
                 
     
                 
                                      
                          
                           
         
                         
           
                                                                
           
                         
                              
           
         
            
       
                                 
     
    return ((_Bool)0);
  }
}
static _Bool xheader_protected_keyword_p(char const *keyword) {
  struct xhdr_tab const *p;
  int tmp;

  {
                 
     
                 
                                      
                          
                           
         
                         
           
                                                            
           
                         
                              
           
         
            
       
                                 
     
    return ((_Bool)0);
  }
}
static _Bool decode_record(char **p,
                           void (*handler)(void *, char const *, char const *),
                           void *data) {
  size_t len;
  char const *keyword;
  char *start;
  char endc;
  char *tmp;
  char *tmp___0;
                    
                    

  {
               
                          
                        
     
     
                                                         
                                                      
     
                           
       
                      
                                                                              
                                       
                        
       
                        
     
           
                               
     
                 
                                      
                                                                  
                           
         
                               
                           
         
               
       
                                 
     
                          {
       
                                                                           
                                           
                        
       
      return ((_Bool)0);
    }
     
                      
                                    
                                       
                                                        
                                    
                        
                       
     
                      
  }
}
static void run_override_list(struct keyword_list *kp,
                              struct tar_stat_info *st) {
  struct xhdr_tab const *t;
  struct xhdr_tab const *tmp;

   
     
                 
                                      
                  
                           
         
         
                                                          
                  
         
                
           

                                                         
           

         
                      
       
                                 
     
           
   
}
static void decx(void *data, char const *keyword, char const *value) {
  struct xhdr_tab const *t;
  struct tar_stat_info *st;
  _Bool tmp;
  _Bool tmp___0;

   
     
                                        
                                               
     
              
             
            
       
	                                              
       
                    
               
       
     
     
                                  
     
            
       

                                   
       

     
           
   
}
void xheader_decode(struct tar_stat_info *st) {
  char *p;
  char *endp;
  _Bool tmp;

   
     
                                                          
                                                         
     
                               
                                       
                                                                   
       
                   
                                        
                                                          
                             
           
           
	                                               
	   
                     
                             
           
         
                                   
       
     
     
                                                   
     
           
   
}
static void decg(void *data, char const *keyword, char const *value) {
  struct keyword_list **kwl;

   
     
                                         
                                               
     
           
   
}
void xheader_decode_global(void) {
  char *p;
  char *endp;
  _Bool tmp;

   
                               
       
                                         
                                                                     
                                                           
       
       
                   
                                        
                                                          
                             
           
           
                                          
                                                                        
           
                     
                             
           
         
                                   
       
     
           
   
}
static void extended_header_init(void) {
  void *tmp;

   
                               
       

                                                      
                                                    
                                                                              
                                                  
       

     
           
   
}
void xheader_store(char const *keyword, struct tar_stat_info const *st,
                   void *data) {
  struct xhdr_tab const *t;
  _Bool tmp;
  _Bool tmp___0;

   
                                 
             
     
     
                                  
     
             
             
     
     
                                               
     
              
             
            
       
	                                              
       
                    
               
       
     
     
                             
                                                         
     
           
   
}
void xheader_read(union block *p, size_t size) {
  size_t j;
  size_t nblocks;
  void *tmp;
  size_t len;

   
     
                    
                                           
                    
                                  
                                               
                                
                                           
     
     
                 
                                      
                   
                          
                            
         
         
                                                                        
                                                                   
                                  
                                
                   
                      
         
                            
                           
         
       
                                 
     
           
   
}
static size_t format_uintmax(uintmax_t val, char *buf, size_t s) {
  char *p;
  char *tmp;
  char *tmp___0;

  {
               
                    
       
                   
                                        
              
                      
                              
                             
           
         
                                   
       
            
                        
       
                   
                                            
                  
              
                                           
                      
                              
                                 
           
         
                                       
       
       
                   
                                            
                                                          
                                 
           
                      
              
                               
         
                                       
       
     
    return (s);
  }
}
static void xheader_print(struct xheader *xhdr, char const *keyword,
                          char const *value) {
  size_t len;
  size_t tmp;
  size_t tmp___0;
  size_t p;
  size_t n;
  char nbuf[100];
  struct obstack *__o;
  int __len;
  struct obstack *__o___0;
  char *tmp___1;
  struct obstack *__o___1;
  int __len___0;
  size_t tmp___2;
  struct obstack *__o___2;
  char *tmp___3;
  struct obstack *__o___3;
  int __len___1;
  size_t tmp___4;
  struct obstack *__o___4;
  char *tmp___5;
                    

   
     
                            
                              
                                  
                    
     
     
                 
                                      
         
                
                                                                      
         
                        
                           
         
       
                                 
     
     
                                       
                      
                     
     
                                                 
                                          
       

                                      
       

     
     
                                                      
                                                                    
                              
                          
     
                                                 
                                              
       

                                      
       

     
     
                                   
                             
                           
                          
                                
                               
     
                                                         
                                              
       

                                              
       

     
     
                                                          
                                                                         
                                      
                          
     
                                                 
                                              
       

                                      
       

     
     
                                   
                             
                           
                          
                              
                               
     
                                                         
                                              
       

                                              
       

     
     
                                                          
                                                                       
                                      
                          
     
                                                 
                                              
       

                                      
       

     
                                 
                           
                          
           
   
}
void xheader_finish(struct xheader *xhdr) {
  struct keyword_list *kp;
  struct obstack *__o;
  char *tmp;
  struct obstack *__o1;
  void *value;

   
                               
     
                 
                                      
                  
                           
         
         
                                                                                
                        
         
       
                                 
     
                    
                                                                                
       

                                  
       

     
                         
                       
                   
                     
                                      
                                                                 
                                    
     
                     
                   
                                                                       
                                        
                                               
                                                  
                                          
     
     
                                          
                                   
                                                      
     
           
   
}
void xheader_destroy(struct xheader *xhdr) {
  struct obstack *__o;
  void *__obj;
  char *tmp;

   
                    
                      
                        
                                                                       
                                                                               
                              
                                 
                               
                
           

	                             
	   

         
              
         

	                           
	 

       
       
                                
                                                  
       
            
       

	                           
       

     
                             
                           
           
   
}
static void code_string(char const *string, char const *keyword,
                        struct xheader *xhdr) {
  char *outstr;
  _Bool tmp;

   
     
                                                    
     
               
       

                                 
       

     
     
                                                         
                           
     
           
   
}
static void decode_string(char **string, char const *arg) {
  _Bool tmp;

   
                  
       

                              
                                      
       

     
     
                                                
     
               
       

                                   
       

     
           
   
}
static void code_time(time_t t, unsigned long nano, char const *keyword,
                      struct xheader *xhdr) {
  char sbuf[200];
  size_t s;
  size_t tmp;
  size_t tmp___0;
  size_t tmp___1;
                    

   
     
                                                                         
              
     
                                   
             
     
     
                                            
                  
          
                                
                                                          
                   
                        
                                                         
     
           
   
}
static void decode_time(char const *arg, time_t *secs, unsigned long *nsecs) {
  uintmax_t u;
  char *p;
  strtol_error tmp;
  strtol_error tmp___0;
                   
                   

   
     
                                                
     
                                      
                        
                          
         
                                                                               
                               
         
                                      
                     
         
       
     
           
   
}
static void code_num(uintmax_t value, char const *keyword,
                     struct xheader *xhdr) {
  char sbuf[100];
  size_t s;
  size_t tmp;
                   

   
     
                                                                  
              
                                     
                        
                                                         
     
           
   
}
static void dummy_coder(struct tar_stat_info const *st
                        __attribute__((__unused__)),
                        char const *keyword __attribute__((__unused__)),
                        struct xheader *xhdr __attribute__((__unused__)),
                        void *data __attribute__((__unused__))) {

   

           
   

}
static void dummy_decoder(struct tar_stat_info *st __attribute__((__unused__)),
                          char const *arg __attribute__((__unused__))) {

   

           
   

}
static void atime_coder(struct tar_stat_info const *st, char const *keyword,
                        struct xheader *xhdr,
                        void *data __attribute__((__unused__))) {

   

     
                                                                               
                               
     
           
   

}
static void atime_decoder(struct tar_stat_info *st, char const *arg) {

   

     
                                                                  
     
           
   

}
static void gid_coder(struct tar_stat_info const *st, char const *keyword,
                      struct xheader *xhdr,
                      void *data __attribute__((__unused__))) {

   

     
                                                          
     
           
   

}
static void gid_decoder(struct tar_stat_info *st, char const *arg) {
  uintmax_t u;
  strtol_error tmp;
                   

   
     
                                                              
     
                                  
                                   
     
           
   
}
static void gname_coder(struct tar_stat_info const *st, char const *keyword,
                        struct xheader *xhdr,
                        void *data __attribute__((__unused__))) {

   

     
                                                          
     
           
   

}
static void gname_decoder(struct tar_stat_info *st, char const *arg) {

   

     
                                     
     
           
   

}
static void linkpath_coder(struct tar_stat_info const *st, char const *keyword,
                           struct xheader *xhdr,
                           void *data __attribute__((__unused__))) {

   

     
                                                              
     
           
   

}
static void linkpath_decoder(struct tar_stat_info *st, char const *arg) {

   

     
                                         
     
           
   

}
static void ctime_coder(struct tar_stat_info const *st, char const *keyword,
                        struct xheader *xhdr,
                        void *data __attribute__((__unused__))) {

   

     
                                                                               
                               
     
           
   

}
static void ctime_decoder(struct tar_stat_info *st, char const *arg) {

   

     
                                                                  
     
           
   

}
static void mtime_coder(struct tar_stat_info const *st, char const *keyword,
                        struct xheader *xhdr,
                        void *data __attribute__((__unused__))) {

   

     
                                                                               
                               
     
           
   

}
static void mtime_decoder(struct tar_stat_info *st, char const *arg) {

   

     
                                                                  
     
           
   

}
static void path_coder(struct tar_stat_info const *st, char const *keyword,
                       struct xheader *xhdr,
                       void *data __attribute__((__unused__))) {

   

     
                                                              
     
           
   

}
static void path_decoder(struct tar_stat_info *st, char const *arg) {

   

     
                                              
                                         
                                                                     
     
           
   

}
static void size_coder(struct tar_stat_info const *st, char const *keyword,
                       struct xheader *xhdr,
                       void *data __attribute__((__unused__))) {

   

     
                                                           
     
           
   

}
static void size_decoder(struct tar_stat_info *st, char const *arg) {
  uintmax_t u;
  __off_t tmp;
  strtol_error tmp___0;
                   

   
     
                                                                  
     
                                      
                       
                             
                                  
     
           
   
}
static void uid_coder(struct tar_stat_info const *st, char const *keyword,
                      struct xheader *xhdr,
                      void *data __attribute__((__unused__))) {

   

     
                                                          
     
           
   

}
static void uid_decoder(struct tar_stat_info *st, char const *arg) {
  uintmax_t u;
  strtol_error tmp;
                   

   
     
                                                              
     
                                  
                                   
     
           
   
}
static void uname_coder(struct tar_stat_info const *st, char const *keyword,
                        struct xheader *xhdr,
                        void *data __attribute__((__unused__))) {

   

     
                                                          
     
           
   

}
static void uname_decoder(struct tar_stat_info *st, char const *arg) {

   

     
                                     
     
           
   

}
static void sparse_size_coder(struct tar_stat_info const *st,
                              char const *keyword, struct xheader *xhdr,
                              void *data) {

   

     
                                          
     
           
   

}
static void sparse_size_decoder(struct tar_stat_info *st, char const *arg) {
  uintmax_t u;
  strtol_error tmp;
                   

   
     
                                                              
     
                                  
                                    
     
           
   
}
static void sparse_numblocks_coder(struct tar_stat_info const *st,
                                   char const *keyword, struct xheader *xhdr,
                                   void *data __attribute__((__unused__))) {

   

     
                                                               
     
           
   

}
static void sparse_numblocks_decoder(struct tar_stat_info *st,
                                     char const *arg) {
  uintmax_t u;
  void *tmp;
  strtol_error tmp___0;
                   

   
     
                                                                  
     
                                      
       

                                
                                                                         
                                                
                                         
       

     
           
   
}
static void sparse_offset_coder(struct tar_stat_info const *st,
                                char const *keyword, struct xheader *xhdr,
                                void *data) {
  size_t i;

   
     
                            
                                                                       
     
           
   
}
static void sparse_offset_decoder(struct tar_stat_info *st, char const *arg) {
  uintmax_t u;
  strtol_error tmp;
                   

   
     
                                                              
     
                                  
                                                                 
     
           
   
}
static void sparse_numbytes_coder(struct tar_stat_info const *st,
                                  char const *keyword, struct xheader *xhdr,
                                  void *data) {
  size_t i;

   
     
                            
                                                              
     
           
   
}
static void sparse_numbytes_decoder(struct tar_stat_info *st, char const *arg) {
  uintmax_t u;
  void *tmp;
  size_t tmp___0;
  strtol_error tmp___1;
                   

   
     
                                                                  
     
                                      
                                                        
         

                                     
                                                
                                                                              
                                                  
         

       
                                     
                               
                                               
     
           
   
}
                                      
                                                      
                                                        
                                                        
                                                      
                                                
                                                      
                                                               
                                                      
                                                   
                                                   
                                                
                                                      
                                                                            
                                                                                
               
                                                                       
               
                                                                             
               
                               
                                                                            
                                       
                                                                             
/* #pragma merger("0","006.incremen.o.i","") */
extern void(__attribute__((__nonnull__(1, 4)))
            qsort)(void *__base, size_t __nmemb, size_t __size,
                   int (*__compar)(void const *, void const *));
extern __attribute__((__nothrow__))
FILE *(__attribute__((__leaf__)) fdopen)(int __fd, char const *__modes);
extern __ssize_t getline(char **__restrict __lineptr, size_t *__restrict __n,
                         FILE *__restrict __stream);
extern int fseek(FILE *__stream, long __off, int __whence);
extern __attribute__((__nothrow__)) int(__attribute__((__leaf__))
                                        fileno)(FILE *__stream);
size_t hash_do_for_each(Hash_table const *table___0,
                        _Bool (*processor)(void *, void *),
                        void *processor_data);
size_t hash_string(char const *string, size_t n_buckets);
char const *program_name;
void read_directory_file(void);
char *quote_copy_string(char const *string);
int unquote_string(char *string);
void savedir_error(char const *name);
void seek_error(char const *name);
void truncate_error(char const *name);
char *new_name(char const *path, char const *name);
static Hash_table *directory_table;
static unsigned int hash_directory(void const *entry, unsigned int n_buckets) {
  struct directory const *directory;
  size_t tmp;

  {
    {
      directory = (struct directory const *)entry;
      tmp = hash_string((char const *)(directory->name), (size_t)n_buckets);
    }
    return ((unsigned int)tmp);
  }
}
static _Bool compare_directories(void const *entry1, void const *entry2) {
  struct directory const *directory1;
  struct directory const *directory2;
  int tmp;

  {
    {
      directory1 = (struct directory const *)entry1;
      directory2 = (struct directory const *)entry2;
      tmp = strcmp((char const *)(directory1->name),
                   (char const *)(directory2->name));
    }
    return ((_Bool)(tmp == 0));
  }
}
static struct directory *note_directory(char const *name, dev_t dev, ino_t ino,
                                        _Bool nfs, _Bool found) {
  size_t size;
  size_t tmp;
  struct directory *directory;
  void *tmp___0;
  void *tmp___1;

  {
    {
                         
                                                                           
                              
                                              
                                     
                                    
                                             
                           
      directory->found = found;
                                                         
                                                   
    }
                          
              
            
       
                         
                                                              
                                                                               
                                                                       
       
                            
            
	                                                                  
	 
                       
           

                         
           

         
              
         

	               
	 

       
     
    return (directory);
  }
}
static struct directory *find_directory(char *name) {
  size_t size;
  size_t tmp;
  struct directory *dir;
  void *tmp___0;
  void *tmp___1;

  {
                          {
      return ((struct directory *)0);
    }       
       
                                         
                                                                             
                                         
                                          
                                                     
                                                     
                 
                                                                                
       
                                           
     
  }
}
static int compare_dirents(void const *first, void const *second) {
  int tmp;

  {
    {
      tmp = strcmp((char const *)(*((char *const *)first) + 1),
                   (char const *)(*((char *const *)second) + 1));
    }
    return (tmp);
  }
}
static void scan_path(struct obstack *stk, char *path, dev_t device) {
  char *dirp;
  char *tmp;
  char const *entry;
  size_t entrylen;
  char *name_buffer___0;
  size_t name_buffer_size;
  size_t name_length;
  struct directory *directory;
  enum children children;
  int *tmp___0;
  size_t tmp___1;
  void *tmp___2;
  size_t tmp___3;
  void *tmp___4;
  struct obstack *__o;
  char *tmp___5;
  struct stat stat_data;
  int tmp___6;
  _Bool nfs;
  char *tmp___7;
  char *tmp___8;
  char *tmp___9;
  char *tmp___10;
  struct obstack *__o___0;
  char *tmp___11;
  struct obstack *__o___1;
  char *tmp___12;
  struct obstack *__o___2;
  char *tmp___13;
  struct obstack *__o___3;
  char *tmp___14;
  _Bool tmp___15;
  struct obstack *__o___4;
  int __len;
  struct obstack *__o___5;
  int __len___0;
  unsigned int tmp___16;
  int tmp___17;
                    
                    
                    
                    
                    

   
     
                                        
                 
     
                
       

                                          
       

     
     
                                   
                   
                                           
                                         
                                                
                                        
                                                       
                                                   
                                           
     
                                                  
       

                                                         
                                                     
       

     
     
                                                          
                                       
     
                    
                                                   
            
                    
     
                                       
               
                                         
                                   
         
                     
                                          
             
	                               
	     
                                     
                               
             
                                                             
               
                           
                                                    
                                            
                                                                      
                                         
                   
                 
                                               
               
               
                         
                                                                              
                                                  
               
             
             
                                                                               
                                                            
                                                                      
             
                           
                        
                                                       
                                                    
                 

                                            
                 

               
                                       
                                 
                                   
                    
               
                                                        
                                                                                
               
                            
                 
                                                           
                 
                            
               
                                                           
                 
                                                   
                                                      
                                                                                
                                     
                                                              
                 
                                                                             
                                                       
                                
                          
                                                                       
                                           
                                                                           
                                    
                       
                            
                                           
                                           
                         

                                   
                                                                            
                                                                              
                                                                      
                         

                       
                                                             
                                           
                                                                  
                                                                 
                     
                   
                                              
                        
                                       
                     

                                                                              
                                                                 
                                                                   
                     

                   
                   
                                                                             
                                                                
                                                                                
                   
                                                  
                                 
                          
                                                                               
                                   
                            
                                                     
                                                      
                                                       
                                                         
                                       
                                
                                      
                         
                              
                                             
                                                
                                                        
                                                          
                                         
                                  
                                                           
                                                            
                                                             
                                                               
                                             
                                      
                                             
                               
                                    
                                           
                             
                           
                                
                                       
                         
                       
                     
                   
                                                                
                 
                                             
                                                   
                                                           
                          
                                
                   
                        
                                       
                                                     
                                                           
                   
                 
                              
                                                             
                                                          
                   

                                                  
                   

                 
                                              
                                       
                                      
                      
                                             
                                                   
                                  
                                                                 
                                                              
                       

                                                      
                       

                     
                                                  
                                           
                                          
                          
                                 
                   
                        
                                        
                                                     
                                                                               
                                   
                            
                                                     
                                                      
                                                       
                                                         
                                                
                                                   
                                        
                                  
                                                          
                                                            
                                          
                                    
                                                             
                                                              
                                                               
                                                                 
                                                       
                                                
                                                                               
                                                                            
                                     

                                                                    
                                     

                                   
                                                                
                                                         
                                                        
                                        
                                               
                                 
                                      
                                             
                               
                             
                           
                                
                                       
                         
                              
                                     
                       
                     
                          
                                          
                                  
                                                                 
                                                              
                       

                                                      
                       

                     
                                                  
                                           
                                          
                   
                 
               
             
                          
                                          
                                                             
                                                      
               

                                                  
               

             
             
                                                                  
                                                                           
                                          
             
                 
                                    
           
                                     
         
       
     
                  
                  
                                                         
                                              
       

                                              
       

     
     
                                                          
                                                                             
                                      
                                    
     
               
       

                           
       

     
           
   
}
static char *sort_obstack(struct obstack *stk) {
  char *pointer;
  struct obstack *__o1;
  void *value;
  size_t counter;
  char *cursor;
  char *buffer___2;
  char **array;
  char **array_cursor;
  size_t tmp;
  struct obstack *__h;
  struct obstack *__o;
  int __len;
  struct obstack *__o1___0;
  void *value___0;
  size_t tmp___0;
  char **tmp___1;
  void *tmp___2;
  char *string;
  char *tmp___3;
  char tmp___4;
  char *tmp___5;

  {
               
                                      
                                                                 
                                    
     
                     
                   
                                                                       
                                        
                                               
                                                  
                                          
     
                                        
                            
                        
                     
    {
                {
                                     ;
                       
                           
         
         
                    
                                             
                              
         
      }
    while_break: /* CIL Label */;
    }
                  {
      return ((char *)((void *)0));
    }
              
              
                                                    
                                                          
       

                                      
       

     
                            
                   
                                              
                                                                         
                                        
     
                         
                   
                                                                               
                                            
                                                       
                                                          
                                                  
     
                                                
                               
                         
                     
     
                 
                                          
                       
                               
         
         
                                 
                         
                            
                                                 
                                  
         
       
                                     
     
     
                                
                                                                      
                                                           
                                   
                          
                           
     
     
                 
                                          
                             
                               
         
                               
         
                     
                                              
                             
                     
                             
                     
                               
                               
                           
                                   
             
                                    
           
                                         
         
                       
       
                                     
     
                           
                        
  }
}
char *get_directory_contents(char *path, dev_t device) {
  struct obstack stk;
  char *buffer___2;
  struct obstack *__o;
  void *__obj;
  char *tmp;

  {
    {
                                                             
                                                
                                    
      buffer___2 = sort_obstack(&stk);
                 
                        
    }
                                                                     
                                                                             
                            
                               
                             
              
         

	                           
	 

       
            
       

	                         
       

     
    return (buffer___2);
  }
}
static FILE *listed_incremental_stream;
void read_directory_file(void) {
  int fd;
  FILE *fp;
  char *buf;
  size_t bufsize;
  char *ebuf;
  int n;
  long lineno;
  unsigned long u;
  int *tmp;
  unsigned long tmp___0;
  time_t t;
  char *tmp___1;
  char *tmp___2;
  char *tmp___3;
  char *tmp___4;
  int *tmp___5;
  int *tmp___6;
  dev_t dev;
  ino_t ino;
  _Bool nfs;
  char *strp;
  int *tmp___7;
  char *tmp___8;
  char *tmp___9;
  char *tmp___10;
  char *tmp___11;
  int *tmp___12;
  int *tmp___13;
  int *tmp___14;
  char *tmp___15;
  char *tmp___16;
  char *tmp___17;
  char *tmp___18;
  int *tmp___19;
  int *tmp___20;
  __ssize_t tmp___21;
  __ssize_t tmp___22;
  int tmp___23;
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    

   
     
                      
                                              
                                                          
                                                              
     
                 
       
                                              
       
             
     
     
                            
     
              
       
                                              
                  
       
             
     
     
                                     
                                                           
                                                                
                                                        
     
                        
       
                    
                                 
                 
                                                              
                                                                  
                    
                      
       
                                                      
         

                                                  
                                                              
                                                    
                          
         

              
                       
           
                                         
           
                               
             

                                                      
                                                                  
                                                        
                              
             

                  
                    
           
                
           
                                      
             

                                                           
                                                                  
                                                        
                              
             

                  
                                            
               
                                             
               
                                   
                 

                                                               
                                                                      
                                                            
                                  
                 

                      
                                              
                                                                  
               
                    
                                            
                                                                
             
           
         
       
       
                   
                                        
           
                                                                 
                                                                      
                                                              
                              
           
                         
                             
           
                                               
                                
                   
                                            
                                            
           
           
                                         
                         
                                                             
                                                                
                    
           
                                                           
             

                                                         
                                                                  
                                                                  
                              
             

                  
                           
               
                                              
               
                                    
                 

                                                             
                                                                      
                                                                      
                                  
                 

                      
                            
               
                    
                   
                             
                 

                                                                   
                                                                       
                                                                        
                                  
                 

                      
                                                
                   
                                                  
                   
                                        
                     

                                                                       
                                                                           
                                                                            
                                      
                     

                   
                 
               
             
           
           
                        
                                          
                          
                                                             
                                                                
                    
           
                                                           
             

                                                         
                                                                   
                                                                    
                              
             

                  
                           
               
                                              
               
                                    
                 

                                                             
                                                                       
                                                                        
                                  
                 

                      
                            
               
                    
                   
                             
                 

                                                                  
                                                                       
                                                                        
                                  
                 

                      
                                                
                   
                                                  
                   
                                        
                     

                                                                      
                                                                           
                                                                            
                                      
                     

                   
                 
               
             
           
           
                        
                   
                                 
                                                                        
           
         
                                   
       
     
     
                                     
     
                   
       

                                              
       

     
              
       

                          
       

     
           
   
}
static _Bool write_directory_file_entry(void *entry, void *data) {
  struct directory const *directory;
  FILE *fp;
  int e;
  char *str;
  char *tmp;
  int *tmp___0;
  int *tmp___1;
  int tmp___2;
  int tmp___3;
  char *tmp___4;

  {
                                                
                      
                           
       
                                                                 
                  
       
                
                                           
              
                                  
       
       
                
                                          
                                                                                
                                                    
                                                             
                                     
                     
       
                
         

                            
         

       
       
                                     
                     
       
     
     
                                    
     
                 {
      tmp___3 = 0;
    }       
                  
     
    return ((_Bool)tmp___3);
  }
}
void write_directory_file(void) {
  FILE *fp;
  int tmp;
  int tmp___0;
  int tmp___1;
  int tmp___2;
  int tmp___3;
  int tmp___4;

   
                                   
              
             
     
     
                             
     
                   
       

                                              
       

     
     
                           
                                      
     
                       
       

                                                  
       

     
     
                                            
                                                       
                                                    
                                    
     
                   
                            
         

                                                               
                                                                    
         

       
     
     
                                    
     
                  
       

                                               
       

     
     
                           
     
                       
       

                                               
       

     
           
   
}
void gnu_restore(char const *directory_name) {
  char *archive_dir;
  char *current_dir;
  char *cur;
  char *arc;
  size_t size;
  size_t copied;
  union block *data_block;
  char *to;
  void *tmp;
  char *tmp___0;
  size_t tmp___1;
  size_t tmp___2;
  int tmp___3;
  char *p;
  char *tmp___4;
  char const *tmp___5;
  char *tmp___6;
  int e;
  int *tmp___7;
  char *tmp___8;
  char *tmp___9;
  int tmp___10;
  int tmp___11;
                    
                    
                    
                    

   
     
                                            
     
                       
       
                      
       
             
     
                                                  
                                                         
       

                     
       

     
     
                          
                                
                       
     
     
                 
                                      
                            
                           
         
         
	                                 
	 
                          
           
                                                           
                                               
                            
           
                           
         
         
	                                             
	 
                            
                        
         
         
                                              
                                                                               
                       
                               
                                                                  
                         
         
       
                                 
     
                      
     
                 
                                          
                    
                               
         
                          
         
                     
                                              
                        
                                   
             
             
                    
                                                                     
             
                           
                                   
             
             
                                                  
                                   
             
           
                                         
         
                             
           
                                                                  
                        
           
                                    
                    
                  
             
	                                                    
	     
                           
               
                                   
                 

                                                   
                                                         
                                                            
                                                                                
                                   
                 

               
               
                          
                                                                            
               
                              
                 

                                               
                               
                                                            
                                                         
                                                              
                                  
                 

               
             
           
           
	                    
	   
         
         
                                              
                               
         
       
                                     
     
     
                                
                                
     
           
   
}
/* #pragma merger("0","007.list.o.i","") */
extern __attribute__((__nothrow__)) unsigned long long(__attribute__((__leaf__))
                                                       gnu_dev_makedev)(
    unsigned int __major, unsigned int __minor) __attribute__((__const__));
extern int putc_unlocked(int __c, FILE *__stream);
extern size_t fwrite_unlocked(void const *__restrict __ptr, size_t __size,
                              size_t __n, FILE *__restrict __stream);
extern __attribute__((__nothrow__)) struct tm *(__attribute__((__leaf__))
                                                gmtime)(time_t const *__timer);
extern
    __attribute__((__nothrow__)) struct tm *(__attribute__((__leaf__))
                                             localtime)(time_t const *__timer);
struct quoting_options *clone_quoting_options(struct quoting_options *o___0);
void set_quoting_style(struct quoting_options *o___0, enum quoting_style s);
size_t quotearg_buffer(char *buffer___2, size_t buffersize, char const *arg,
                       size_t argsize, struct quoting_options const *o___0);
char *quotearg(char const *arg);
_Bool utc_option;
_Bool block_number_option;
_Bool show_omitted_dirs_option;
gid_t gid_from_header(char const *p, size_t s);
int major_from_header(char const *p, size_t s);
int minor_from_header(char const *p, size_t s);
mode_t mode_from_header(char const *p, size_t s);
size_t size_from_header(char const *p, size_t s);
time_t time_from_header(char const *p, size_t s);
uid_t uid_from_header(char const *p, size_t s);
void list_archive(void);
void read_and(void (*do_something)(void));
void decode_mode(mode_t mode, char *string);
int name_match(char const *path);
_Bool all_names_found(struct tar_stat_info *p);
_Bool sparse_fixup_header(struct tar_stat_info *st);
enum dump_status sparse_skip_file(struct tar_stat_info *st);
static uintmax_t from_header(char const *where0, size_t digs, char const *type,
                             uintmax_t minus_minval, uintmax_t maxval);
static char const base_64_digits[64] = {
    (char const)'A', (char const)'B', (char const)'C', (char const)'D',
    (char const)'E', (char const)'F', (char const)'G', (char const)'H',
    (char const)'I', (char const)'J', (char const)'K', (char const)'L',
    (char const)'M', (char const)'N', (char const)'O', (char const)'P',
    (char const)'Q', (char const)'R', (char const)'S', (char const)'T',
    (char const)'U', (char const)'V', (char const)'W', (char const)'X',
    (char const)'Y', (char const)'Z', (char const)'a', (char const)'b',
    (char const)'c', (char const)'d', (char const)'e', (char const)'f',
    (char const)'g', (char const)'h', (char const)'i', (char const)'j',
    (char const)'k', (char const)'l', (char const)'m', (char const)'n',
    (char const)'o', (char const)'p', (char const)'q', (char const)'r',
    (char const)'s', (char const)'t', (char const)'u', (char const)'v',
    (char const)'w', (char const)'x', (char const)'y', (char const)'z',
    (char const)'0', (char const)'1', (char const)'2', (char const)'3',
    (char const)'4', (char const)'5', (char const)'6', (char const)'7',
    (char const)'8', (char const)'9', (char const)'+', (char const)'/'};
static char base64_map[256];
static void base64_init(void) {
  int i;

   
     
                                                           
            
     
     
                 
                                      
                        
                           
         
                                                     
            
       
                                 
     
           
   
}
void read_and(void (*do_something)(void)) {
  enum read_header status;
  enum read_header prev_status;
  char *tmp;
  char *tmp___0;
  int tmp___1;
  _Bool tmp___2;
  char buf[(((sizeof(uintmax_t) * 8UL) * 302UL) / 1000UL + 1UL) + 1UL];
  off_t tmp___3;
  char *tmp___4;
  char *tmp___5;
  char buf___0[(((sizeof(uintmax_t) * 8UL) * 302UL) / 1000UL + 1UL) + 1UL];
  off_t tmp___6;
  char *tmp___7;
  char *tmp___8;
  char buf___1[(((sizeof(uintmax_t) * 8UL) * 302UL) / 1000UL + 1UL) + 1UL];
  off_t tmp___9;
  char *tmp___10;
  char *tmp___11;
  char *tmp___12;
  char buf___2[(((sizeof(uintmax_t) * 8UL) * 302UL) / 1000UL + 1UL) + 1UL];
  off_t block_ordinal;
  off_t tmp___13;
  char *tmp___14;
  char *tmp___15;
  char *tmp___16;
  _Bool tmp___17;
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    

  {
    {
                                   
                    
                    
      open_archive((enum access_mode)0);
    }
    {
      while (1) {
                                     ;
        {
          prev_status = status;
                                               
                                            
          status = read_header((_Bool)0);
        }
        {
                                           
                        
           
                                           
                        
           
          if ((unsigned int)status == 1U) {
            goto case_1;
          }
                                           
                        
           
                                           
                        
           
                                           
                        
           
          goto switch_break;
                                
                                
          
                  
         
        case_1 : /* CIL Label */
        {
                                                                          
        }
                        
                                                   
               
                                                                         
                                                                 
                                                          
                                                                              
               
                                                         
                                              
                        
                      
                                                            
                                                
                                                              
                                                   
                            
                          
                                
                   
                        
                              
                 
               
                    
                      
                       
                                                                           
             
                            
                    
                                                                 
                               
                 
                                                                 
                               
                 
                                                                 
                               
                 
                                                                 
                               
                 
                                    
                                      
                                      
                                       
                                       
                                      
                                               
                   

                                         
                                                                   
                                                      
                                                            
                   

                 
                                              
               
                              
               
                            
                                                
               
               
             
                  
                    
           
          {
	    (*do_something)();
	  }
          goto __Cont;
                               
                                    
             

                                                
                                                      
                                     
                                                                               
                               
                                                                   
                                                        
                                                                        
             

           
           
	                                         
	   
                                     
             
                                             
             
                                             
                                
             
             
                                                
                                                      
                                     
                           
                                                                             
                             
                                                           
                                                          
             
                              
           
                               
                      
                               
                                    
             

                                                
                                                       
                                     
                           
                                                                             
                             
                                                                  
                                                        
                                                                          
             

           
                            
                                
         
                                               
         
           
                                                  
                              
             
                                                  
                              
             
                                                  
                              
             
                                                  
                              
             
                                                  
                              
             
                                                  
                              
             
                                  
                                      
           
                                                                        
                                                
                            
           
                                     
                                     
                                       
               

                                                   
                                         
                               
                                                                             
                               
                                                                             
                                                         
                                             
                             
                                                                               
                               
                                                 
                                                          
                                                                            
               

             
             
                                                            
                                                  
                              
             
                                  
                                     
                                     
                                   
                                      
           
                    
           
                                            
           
                      
        switch_break: /* CIL Label */;
        }
        goto while_break;
      __Cont : {
	                                                 
	}
                       
                           
         
      }
    while_break: /* CIL Label */;
    }
     
                      
                       
     
           
  }
}
void list_archive(void) {
  off_t size;
  size_t written;
  size_t check;
  union block *data_block;
  char *tmp;
  int *tmp___0;
                   

  {
     
                                                                            
     
                        {
       

        print_header(&current_stat_info, (off_t)-1);
       

    }
                             
                                                       
         
                                               
         
                                  
           

                                     
                                                                     
                                                          
           

         
                                              
         
                     
                                          
                               
                               
             
                                      
                                   
             
             
	                                     
	     
                              
               
                                                           
                                               
                                
               
                               
             
             
	                                                  
	     
                                         
                                     
             
             
                                           
                           
                                      
                                                                       
                                                                            
                                   
                                                                       
             
                                   
               
                                                                              
                                                    
                                                           
               
                               
             
                                                   
           
                                     
         
                                  
           

                                                       
           

         
         
                                       
                                  
         
               
       
     
                              
       

                                                                             
       

     
    {
      skip_member();
    }
                              
       

                                                   
       

     
           
  }
}
enum read_header read_header(_Bool raw_extended_headers) {
  size_t i;
  int unsigned_sum;
  int signed_sum;
  int recorded_sum;
  uintmax_t parsed_sum;
  char *p;
  union block *header;
  union block *header_copy;
  char *bp;
  union block *data_block;
  size_t size;
  size_t written;
  union block *next_long_name;
  union block *next_long_link;
  size_t next_long_name_blocks;
  size_t next_long_link_blocks;
  char *tmp;
  size_t tmp___0;
  size_t tmp___1;
  size_t name_size;
  void *tmp___2;
  char *tmp___3;
  off_t tmp___4;
  off_t tmp___5;
  char const *name;
  struct posix_header const *h;
  char namebuf[((sizeof(h->prefix) + 1UL) + 100UL) + 1UL];
  char *np;
  size_t tmp___6;
  char *tmp___7;
  int tmp___8;
                    
                    

  {
    next_long_name = (union block *)0;
    next_long_link = (union block *)0;
    {
                {
                                     ;
        {
          header = find_next_block();
          current_header = header;
        }
                      
                                       
         
        unsigned_sum = 0;
                       
        p = header->buffer;
        i = sizeof(*header);
        {
          while (1) {
                                             ;
            tmp___0 = i;
            i--;
            if (!(tmp___0 != 0UL)) {
              goto while_break___0;
            }
            unsigned_sum += (int)((unsigned char)*p);
                    
            p++;
                                                   
          }
        while_break___0: /* CIL Label */;
        }
        if (unsigned_sum == 0) {
          return ((enum read_header)3);
        }
        i = sizeof(header->header.chksum);
        {
          while (1) {
                                             ;
            tmp___1 = i;
            i--;
            if (!(tmp___1 != 0UL)) {
              goto while_break___1;
            }
            unsigned_sum -= (int)((unsigned char)header->header.chksum[i]);
            signed_sum -= (int)((signed char)header->header.chksum[i]);
          }
        while_break___1: /* CIL Label */;
        }
        {
          unsigned_sum = (int)((unsigned long)unsigned_sum +
                               32UL * sizeof(header->header.chksum));
                                                        
                                                                   
          parsed_sum = from_header(
              (char const *)(header->header.chksum),
              sizeof(header->header.chksum), (char const *)0, (uintmax_t)0,
              (uintmax_t)(-1 - (-1 << (sizeof(int) * 8UL - 1UL))));
        }
                                                 
                                       
         
        recorded_sum = (int)parsed_sum;
        if (unsigned_sum != recorded_sum) {
                                          {
            return ((enum read_header)5);
          }
        }
                                                 
                                                      
               {
           

            current_stat_info.stat.st_size =
                off_from_header((char const *)(header->header.size),
                                sizeof(header->header.size));
           

        }
                                                 
                      
               {
                                                   
                        
                 {
                                                      
                          
                   {
                                                        
                                     
                                           
                                               
                        
                                                           
                                
                          
                                                             
                                           
                                                                         
                                                                      
                                                                                
                         

                                       
                         

                              
                                               
                           

                                         
                           

                         
                       
                       
                                                      
                                                             
                       
                                                               
                                             
                           

                                                         
                           

                         
                                                     
                                                             
                              
                                             
                           

                                                         
                           

                         
                                                     
                                                             
                       
                       
                                                     
                                               
                                                       
                                      
                       
                       
                                   
                                                            
                                              
                                                 
                           
                           
			                                   
			   
                                            
                             
                                                                             
                                                                 
                                              
                             
                                                 
                           
                           
			                                                
			   
                                               
                                           
                           
                           
                                                                
                                                                    
                                                           
                                            
                                          
                                                 
                                                                                
                                                    
                                            
                           
                         
                                                       
                       
                                         
                            
                                                                
                         

                                                    
                                                                  
                                                           
                                                                
                         

                              
                                                                  
                           

                                                      
                                                                    
                                                             
                                                                  
                                                    
                           

                         
                       
                     
                   
                 
                     {
                h = (struct posix_header const *)(&current_header->header);
                                      {
                   

                                                   
                   

                }
                                     
                                                                      
                                                    
                                                                  
                       {
                  np = namebuf;
                                     
                     
                                                                          
                     
                                       
                       

                                                            
                                                                           
                                                  
                                                                 
                                                           
                                      
                                     
                             
                                             
                                                                           
                       

                     
                   
                  {
                    memcpy((void * /* __restrict  */)np,
                           (void const * /* __restrict  */)(h->name),
                           sizeof(h->name));
                                                           
                    name = (char const *)(namebuf);
                                                        
                                                        
                  }
                }
                {
                  assign_string(&current_stat_info.orig_file_name, name);
                  assign_string(&current_stat_info.file_name, name);
                  current_stat_info.had_trailing_slash =
                      strip_trailing_slashes(current_stat_info.file_name);
                }
                                      {
                   

                                                   
                   

                }
                                     
                                                                      
                                                    
                                                                  
                       {
                   

                                                               
                                                                         
                                                
                                                                
                                                   
                                                        
                                                        
                   

                }
                {
		                                                    
		}
                return ((enum read_header)1);
              }
            }
          }
        }
      }
                                ;
    }
  }
}
void decode_header(union block *header, struct tar_stat_info *stat_info,
                   enum archive_format *format_pointer, int do_user_group) {
  enum archive_format format;
  int tmp;
  int tmp___0;
  int tmp___1;
  int tmp___2;
  int tmp___3;
  int tmp___4;
  unsigned long long tmp___5;
  _Bool tmp___6;

  {
    {
      tmp___0 = strcmp((char const *)(header->header.magic), "ustar");
    }
                      {
                                                      
                                                      
                                                        
                                                           
                                                            
                                                              
                                                                 
                                                    
                          
                                
                   
                        
                              
                 
                      
                            
               
                    
                          
             
                  
                        
           
                
                      
         
              
                             
                                   
                                          
                
                                          
         
       
    }       
       
	                                                              
       
                     
                                        
              
                                        
       
     
    {
                               
      stat_info->stat.st_mode = mode_from_header(
          (char const *)(header->header.mode), sizeof(header->header.mode));
                                                        
                                                                              
                                                                             
                                                                             
                                                                          
                                                                   
                                                  
                                                                          
                                                                   
                                                  
                                                             
                                                             
    }
                                     
                               
         

                                          
                                                                           
                                                                    
                                          
                                                                           
                                                                    
         

       
     
                                     
       

                                                 
                                                                            
                                                 
                                                                            
                                             
       

            
                                       
         

                                          
                                                                         
                                                                  
                                          
                                                                         
                                                                  
         

       
                          
                                   
           

                                                     
                                                                                
           

                
                                         
             

                                      
                                                                     
                                                              
             

                  
             
                                                                          
                                                              
             
                           
               

                                        
                                                                       
                                                                
               

             
           
         
                                   
           

                                                     
                                                                                
           

                
                                         
             

                                      
                                                                     
                                                              
             

                  
             
                                                                          
                                                              
             
                           
               

                                        
                                                                       
                                                                
               

             
           
         
       
       
                                                 
                       
         
                                                 
                       
         
                            
                               
                               
        
                                                                            
                                                   
       
                          
                                     
                                             
                                    
       
     
     
                                                             
                                
                                           
     
                  
       

                                       
                                        
       

     
           
  }
}
static int warned_once___0;
static struct quoting_options *o;
static uintmax_t from_header(char const *where0, size_t digs, char const *type,
                             uintmax_t minus_minval, uintmax_t maxval) {
  uintmax_t value;
  char const *where;
  char const *lim;
  int negative;
  char *tmp;
  unsigned short const **tmp___0;
  char const *where1;
  uintmax_t overflow;
  char const *tmp___1;
  int digit;
  char *tmp___2;
  char *tmp___3;
  int dig;
  char *tmp___4;
  char const *tmp___5;
  char *string;
  void *tmp___6;
  char const *tmp___7;
  char *tmp___8;
  int signbit;
  uintmax_t topbits;
  char const *tmp___9;
  char const *tmp___10;
  char *tmp___11;
  char buf[1000];
  char *tmp___12;
  unsigned short const **tmp___13;
  char minval_buf[((((sizeof(uintmax_t) * 8UL) * 302UL) / 1000UL + 1UL) + 1UL) +
                  1UL];
  char maxval_buf[(((sizeof(uintmax_t) * 8UL) * 302UL) / 1000UL + 1UL) + 1UL];
  char value_buf[((((sizeof(uintmax_t) * 8UL) * 302UL) / 1000UL + 1UL) + 1UL) +
                 1UL];
  char *minval_string;
  char *tmp___14;
  char *value_string;
  char *tmp___15;
  char *tmp___16;
  char *tmp___17;
  uintmax_t tmp___18;
  uintmax_t tmp___19;
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    

  {
    where = where0;
    lim = where + digs;
    negative = 0;
                     
     
                 
                                      
                                                         
                     
             

                                                                                
                                                   
                              
             

           
                                 
         
         
	                            
	 
                                                                                
                           
         
                
       
                                 
     
    value = (uintmax_t)0;
                                          {
                     
                              
      {
        while (1) {
                                           ;
          tmp___1 = where;
          where++;
          value += (uintmax_t)((int const) * tmp___1 - 48);
                                                           
                                 
                 {
            if (!((unsigned int)*where - 48U <= 7U)) {
              goto while_break___0;
            }
          }
                                                  
          value <<= 3;
        }
      while_break___0: /* CIL Label */;
      }
                     
                    
              
                             
                               
                                   
                       
                                                             
                                      
                                   
                             
               
                           
                                                    
                                                  
                          
                                                                   
                                         
                          
                                                              
                                           
                     
                   
                                                          
                                                          
                              
                 
                                               
               
                      
                                                  
                              
                                            
                   

                                                                              
                                                                           
                                                                             
                                        
                                 
                   

                 
               
             
           
         
       
                     
                   
           

                                                                             
                                                                             
                        
                            
           

         
                               
       
    }       
                                      
                    
              
                                        
                               
                                 
             

                                  
                                                                                
                                                 
             

           
                          
                  
                                                 
           
                       
                                                
                                                               
                                                             
                                  
                                       
                 
                      
                                     
               
                                               
                 
                                                         
                                           
                                                          
                                                                       
                                                  
                 
                           
                   

                                                          
                                      
                                                                               
                                                                      
                                    
                   

                 
                                       
               
                                                        
                      
             
                                           
           
                
                                            
                        
                  
                                            
                                   
                                                                         
                                             
                                                                   
                              
                      
                                  
                                                                       
                                      
               
                           
                                                    
                                   
                          
                                                                               
                                                                   
                                         
                   
                                                                 
                               
                       

                                           
                                                                         
                                                                  
                                        
                       

                     
                                           
                   
                 
                                               
               
                                 
                             
                               
               
             
           
         
       
     
                                                     
                   
         
                                     
         
                                                                        
                      
                     
                     
               

                                                                       
                                                            
               

             
             
                         
                                                  
                                                                  
                                        
                                         
                   
                        
                                       
                 
                      
               
                                             
             
             
                                                                              
                                                                 
                                 
                                                                           
                                                                               
                              
             
           
                                 
         
       
     
                   
                              
           {
                        
    }
                           {
                     
                          
             {
        tmp___18 = value;
      }
      return (tmp___18);
    }
               
       
                                                 
                         
                              
                                                                              
                                 
                                                 
                  
                             
                                                                              
                                
       
                     
                       
                                  
       
                         
                        
                                   
       
       
                                                 
                   
                        
                                                                              
                                                                        
                                                                              
                        
                        
       
     
                           
  }
}
gid_t gid_from_header(char const *p, size_t s) {
  uintmax_t tmp;
                   

  {
    {
      tmp = from_header(p, s, "gid_t", -((uintmax_t)((gid_t)0)),
                        (uintmax_t)4294967295U);
    }
    return ((gid_t)tmp);
  }
}
int major_from_header(char const *p, size_t s) {
  uintmax_t tmp;
                   

  {
    {
      tmp = from_header(p, s, "major_t",
                        -((uintmax_t)(-1 << (sizeof(int) * 8UL - 1UL))),
                        (uintmax_t)(-1 - (-1 << (sizeof(int) * 8UL - 1UL))));
    }
    return ((int)tmp);
  }
}
int minor_from_header(char const *p, size_t s) {
  uintmax_t tmp;
                   

  {
    {
      tmp = from_header(p, s, "minor_t",
                        -((uintmax_t)(-1 << (sizeof(int) * 8UL - 1UL))),
                        (uintmax_t)(-1 - (-1 << (sizeof(int) * 8UL - 1UL))));
    }
    return ((int)tmp);
  }
}
mode_t mode_from_header(char const *p, size_t s) {
  unsigned int u;
  uintmax_t tmp;
  int tmp___0;
  int tmp___1;
  int tmp___2;
  int tmp___3;
  int tmp___4;
  int tmp___5;
  int tmp___6;
  int tmp___7;
  int tmp___8;
  int tmp___9;
  int tmp___10;
  int tmp___11;
                    

  {
    {
      tmp = from_header(p, s, "mode_t", -((uintmax_t)((mode_t)0)),
                        0xffffffffffffffffUL);
      u = (unsigned int)tmp;
    }
                   {
      tmp___0 = 2048;
    }       
                  
     
                   {
      tmp___1 = 1024;
    }       
                  
     
                  {
      tmp___2 = 512;
    }       
                  
     
                  {
      tmp___3 = 256;
    }       
                  
     
                  {
      tmp___4 = 128;
    }       
                  
     
                 {
      tmp___5 = 64;
    }       
                  
     
                 {
      tmp___6 = 256 >> 3;
    }       
                  
     
                 {
      tmp___7 = 128 >> 3;
    }       
                  
     
                {
      tmp___8 = 64 >> 3;
    }       
                  
     
                {
      tmp___9 = (256 >> 3) >> 3;
    }       
                  
     
                {
      tmp___10 = (128 >> 3) >> 3;
    }       
                   
     
                {
      tmp___11 = (64 >> 3) >> 3;
    }       
                   
     
    return (
        (mode_t)(((((((((((tmp___0 | tmp___1) | tmp___2) | tmp___3) | tmp___4) |
                       tmp___5) |
                      tmp___6) |
                     tmp___7) |
                    tmp___8) |
                   tmp___9) |
                  tmp___10) |
                 tmp___11));
  }
}
off_t off_from_header(char const *p, size_t s) {
  uintmax_t tmp;
                   

  {
    {
      tmp =
          from_header(p, s, "off_t", (uintmax_t)0,
                      (uintmax_t)(-1L - (-1L << (sizeof(off_t) * 8UL - 1UL))));
    }
    return ((off_t)tmp);
  }
}
size_t size_from_header(char const *p, size_t s) {
  uintmax_t tmp;
                   

  {
    {
      tmp = from_header(p, s, "size_t", (uintmax_t)0, 0xffffffffffffffffUL);
    }
    return (tmp);
  }
}
time_t time_from_header(char const *p, size_t s) {
  uintmax_t tmp;
                   

  {
    {
      tmp = from_header(
          p, s, "time_t", -((uintmax_t)(-1L << (sizeof(time_t) * 8UL - 1UL))),
          (uintmax_t)(-1L - (-1L << (sizeof(time_t) * 8UL - 1UL))));
    }
    return ((time_t)tmp);
  }
}
uid_t uid_from_header(char const *p, size_t s) {
  uintmax_t tmp;
                   

  {
    {
      tmp = from_header(p, s, "uid_t", -((uintmax_t)((uid_t)0)),
                        (uintmax_t)4294967295U);
    }
    return ((uid_t)tmp);
  }
}
uintmax_t uintmax_from_header(char const *p, size_t s) {
  uintmax_t tmp;
                   

  {
    {
      tmp = from_header(p, s, "uintmax_t", (uintmax_t)0, 0xffffffffffffffffUL);
    }
    return (tmp);
  }
}
char *stringify_uintmax_t_backwards(uintmax_t o___0, char *buf) {

   

          
                        
     
                 
                                      
              
                                                
                      
                              
                           
         
       
                                 
     
    return (buf);
   

}
static char buffer[27UL];
char const *tartime(time_t t) {
  char *p;
  struct tm *tm;
  struct tm *tmp;
  struct tm *tmp___0;
  struct tm *tmp___1;
  uintmax_t tmp___2;
                   

  {
                    {
       

        tmp = gmtime((time_t const *)(&t));
        tmp___1 = tmp;
       

    }       
       

                                                  
                          
       

     
    tm = tmp___1;
            {
       
                
                                               
                                                                              
                                                                                
                                    
       
      return ((char const *)(buffer));
    }
                 
                                
            
                             
     
     
                                                                          
     
                 
          
                     
     
     
                 
                                      
                                                                     
                                  
                           
         
            
                       
       
                                 
     
                             
  }
}
static int ugswidth = 18;
void print_header(struct tar_stat_info *st, off_t block_ordinal) {
  char modes[11];
  char const *time_stamp;
  char *temp_name;
  char uform[(((sizeof(uintmax_t) * 8UL) * 302UL) / 1000UL + 1UL) + 1UL];
  char gform[(((sizeof(uintmax_t) * 8UL) * 302UL) / 1000UL + 1UL) + 1UL];
  char *user;
  char *group;
  char size[2UL * ((((sizeof(uintmax_t) * 8UL) * 302UL) / 1000UL + 1UL) + 1UL)];
  char uintbuf[(((sizeof(uintmax_t) * 8UL) * 302UL) / 1000UL + 1UL) + 1UL];
  int pad;
  char buf[(((sizeof(uintmax_t) * 8UL) * 302UL) / 1000UL + 1UL) + 1UL];
  char *tmp;
  char *tmp___0;
  char *tmp___1;
  char *tmp___2;
  size_t tmp___3;
  uintmax_t u;
  uintmax_t tmp___4;
  uid_t tmp___5;
  uintmax_t g;
  uintmax_t tmp___6;
  gid_t tmp___7;
  unsigned int tmp___8;
  char *tmp___9;
  unsigned int tmp___10;
  char *tmp___11;
  char *tmp___12;
  size_t tmp___13;
  size_t tmp___14;
  size_t tmp___15;
  char *tmp___16;
  char *tmp___17;
  char *tmp___18;
  char *tmp___19;
  char type_string[2];
  char const *tmp___20;
  char *tmp___21;
  char *tmp___22;
  char *tmp___23;
  char *tmp___24;
  uintmax_t tmp___25;
  char *tmp___26;
  char *tmp___27;
  char *tmp___28;
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    

  {
                            {
      temp_name = st->orig_file_name;
    }       
                                
     
                              
                               
         

                                                  
         

       
       
                       
                                                                     
                       
                                                                     
                                            
                                     
                                                                                
                                        
                                                  
                                                              
       
     
                             {
       

        tmp___1 = quotearg((char const *)temp_name);
        fprintf((FILE * /* __restrict  */) stdlis,
                (char const * /* __restrict  */) "%s\n", tmp___1);
       

    }       
                           
       
                                                         
                       
         
                                                         
                       
         
                                                         
                       
         
                                                         
                       
         
                                                         
                       
         
                                                         
                       
         
                                                         
                       
         
                                                        
                       
         
                                                         
                       
         
                                                         
                       
         
                                                         
                       
         
                                                         
                       
         
                                                         
                       
         
                                                         
                       
         
                                                         
                       
         
                                                         
                       
         
                          
                              
                             
                          
                              
                             
                          
                              
                             
                          
                               
                               
        
                             
                                                    
                                           
                        
       
                          
                              
                              
                               
        
                             
                                                  
       
                                                        
                               
         
                          
                              
                             
                          
                              
                             
                          
                              
                             
                          
                              
                             
                          
                              
                             
                          
                              
                             
                          
                              
                             
                          
                              
                             
                          
                                    
       
       
                                                 
                                                      
       
                      
                                                 
                                      
                             
                  
                        
           
                
                      
         
              
                              
       
                 
                                                                   
                                                                            
                                                            
                    
       
                                        
           

                                                 
                  
                                                                               
                               
           

                
           

                     
                                                                           
                                                                    
                                                      
                                                                           
                         
           

         
       
                      
                                                 
                                      
                              
                  
                        
           
                
                      
         
              
                              
       
                 
                                                                   
                                                                            
                                                            
                    
       
                                        
           

                                                  
                  
                                                                               
                               
           

                
           

                     
                                                                           
                                                                    
                                                      
                                                                           
                          
           

         
       
       
                                                         
                           
         
                                                         
                           
         
                            
                                   
                                   
        
                                                                      
                                                
                               
                     
                                                                              
                                                
                                                        
                                                
                                                     
                                                                       
                                                 
                                
                     
                                                                              
                                                
                                                         
       
                              
                                      
       
                                                 
                                        
                     
                                                                              
                                                
                                                         
       
                              
                                        
       
       
                                              
                                               
                                                
                                                              
       
                           
                       
       
       
                                                  
                                                                            
                                                                   
                                                     
                                                  
                                                                  
       
       
                                                         
                           
         
                                                         
                           
         
                                                        
                          
         
                                                         
                          
         
                                                         
                          
         
                                                         
                          
         
                                                         
                          
         
                                                         
                          
         
                                                         
                          
         
                                                         
                          
         
                                                         
                          
         
                                                         
                           
         
                                                         
                           
         
                                                         
                           
         
                                                         
                           
         
                                                         
                           
         
                                
                                   
       
                                                         
                                                  
                                                                       
       
                              
                                   
       
                                                         
                                            
                                                  
                                                                    
       
                              
                                          
       
                                                         
                                      
                                                      
                                                      
                                                  
                                                                    
       
                              
                                   
                                   
                                    
                                    
                                    
                                    
                                    
                                    
                                    
        
                                    
       
                              
                                   
       
                                              
                                                  
                                                          
       
                              
                                   
       
                                              
                                                  
                                                          
       
                              
                                   
       
                                                  
                                                  
                                                          
       
                              
                                   
       
                                       
                                                                 
                                                          
                                                 
                     
                     
                                                                              
                                                
                                                         
                                                         
                                                  
                                                                
       
                              
                                   
       
                                                       
                                                  
                                                          
       
                              
                                        
       
     
     
                              
     
           
  }
}
void print_for_mkdir(char *pathname, int length, mode_t mode) {
  char modes[11];
  char buf[(((sizeof(uintmax_t) * 8UL) * 302UL) / 1000UL + 1UL) + 1UL];
  off_t tmp;
  char *tmp___0;
  char *tmp___1;
  char *tmp___2;
  char *tmp___3;
                    
                    
                    
                    

   
                             
       
                             
                                     
       
                                
         

                                        
                                                  
                             
                   
                                                                                
                                          
                                                    
                                                                    
         

       
       
                                                   
                                                 
                                                  
                                                                        
                                                         
       
     
           
   
}
void skip_file(off_t size) {
  union block *x;
  char *tmp;
                   

  {
                              
                          
                           
     
    {
      while (1) {
                                     ;
        if (!(size > 0L)) {
          goto while_break;
        }
        {
	  x = find_next_block();
	}
                 
           

                                                       
                                           
                         
           

         
        {
          set_next_block_after(x);
          size -= 512L;
        }
                                  
                                
         
      }
    while_break: /* CIL Label */;
    }
           
  }
}
void skip_member(void) {
  char save_typeflag;
  _Bool tmp;

  {
    {
      save_typeflag = current_header->header.typeflag;
      set_next_block_after(current_header);
                                                                           
                                                
    }
              
       

                                             
       

           {
                                    {
         

          skip_file(current_stat_info.stat.st_size);
         

      }
    }
           
  }
}
/* #pragma merger("0","008.mangle.o.i","") */
extern __attribute__((__nothrow__)) int(__attribute__((__leaf__))
                                        rename)(char const *__old,
                                                char const *__new);
void extract_mangle(void) {
  off_t size;
  char *buffer___2;
  void *tmp;
  char *copy;
  char *cursor;
  union block *block;
  union block *tmp___0;
  size_t available;
  char *tmp___1;
  char *next_cursor;
  char *name;
  char *name_end;
  char *tmp___2;
  int tmp___3;
  char const *tmp___4;
  char *tmp___5;
  char *tmp___6;
  int *tmp___7;
  char *tmp___8;
  int tmp___9;
  int tmp___10;
  char const *tmp___11;
  char *tmp___12;
  char *tmp___13;
  int *tmp___14;
  char *tmp___15;
  int tmp___16;
  int tmp___17;
  int tmp___18;
  char *tmp___19;
  int tmp___20;
  int tmp___21;
                    
                    
                    
                    
                    
                    

   
     
                                            
                                         
                               
                        
                          
     
                                       
       

                     
       

            
                                                 
         

                       
         

       
     
                                        
     
                 
                                      
                           
                           
         
         
                                      
                          
         
                     
           
                                                                 
                                               
                            
           
                 
         
         
	                                           
	 
                                       
                                   
         
         
                                                
                                                                             
                            
                                                   
                               
                                                                
         
       
                                 
     
     
                 
                                          
                       
                               
         
         
                                                           
                                
                        
                                  
                                                                         
         
                       
           
                                                                            
           
                         
             

                                                                  
                                                          
                              
             

                  
             
                                
                                                         
             
             
                         
                                                  
                 
                                                                                
                 
                                
                                       
                 
                 
                             
                                                                 
                 
               
                                             
             
             
                                       
                                   
                                           
                        
                                                                            
             
                           
               
                                                                
               
                             
                            
                      
                 
                            
                                                                                
                 
                               
                          
                                                                      
                                                                
                                                                 
                                                
                                                                       
                                  
                                  
                 
                        
                          
                 
               
                    
               
                                   
                 

                                                           
                                                                          
                 

               
             
           
                
           
                              
                                                       
           
           
                       
                                                
               
		                                                             
	       
                             
                                     
               
               
                           
                                                               
               
             
                                           
           
                                   
                                               
                                               
           
           
                                         
                                                                               
           
                        
             

                                                                 
                                                           
                                                           
                                           
                                                                          
                              
             

                  
                                 
               

                                                      
                                                                       
               

             
           
         
                             
       
                                     
     
           
   
}
/* #pragma merger("0","009.misc.o.i","") */
extern __attribute__((__nothrow__)) int(__attribute__((__leaf__))
                                        pipe)(int *__pipedes);
extern
    __attribute__((__nothrow__)) int(__attribute__((__nonnull__(1), __leaf__))
                                     chdir)(char const *__path);
extern __attribute__((__nothrow__)) __pid_t fork(void);
extern
    __attribute__((__nothrow__)) int(__attribute__((__nonnull__(1), __leaf__))
                                     rmdir)(char const *__path);
char *find_backup_file_name(char const *file, enum backup_type backup_type___0);
enum backup_type backup_type;
int chdir_arg(char const *dir);
void chdir_do(int i);
__attribute__((__noreturn__)) void chdir_fatal(char const *name);
__attribute__((__noreturn__)) void exec_fatal(char const *name);
void read_error_details(char const *name, off_t offset, size_t size);
__attribute__((__noreturn__)) void read_fatal(char const *name);
__attribute__((__noreturn__)) void
read_fatal_details(char const *name, off_t offset, size_t size);
void read_warn_details(char const *name, off_t offset, size_t size);
void savedir_warn(char const *name);
void seek_warn_details(char const *name, off_t offset);
void seek_diag_details(char const *name, off_t offset);
void waitpid_error(char const *name);
pid_t xfork(void);
void xpipe(int *fd);
char *quotearg_n_style(int n, enum quoting_style s, char const *arg);
int save_cwd(struct saved_cwd *cwd);
int restore_cwd(struct saved_cwd const *cwd);
static __attribute__((__noreturn__)) void call_arg_fatal(char const *call,
                                                         char const *name);
void assign_string(char **string, char const *value) {
  char *tmp;

  {
                  
       

                              
       

     
               {
       

        tmp = xstrdup(value);
        *string = tmp;
       

    }       
                          
     
           
  }
}
char *quote_copy_string(char const *string) {
  char const *source;
  char *destination;
  char *buffer___2;
  int copying;
  int character;
  char const *tmp;
  size_t length;
  size_t tmp___0;
  void *tmp___1;
  char *tmp___2;
  char *tmp___3;
  char *tmp___4;
  int tmp___5;

  {
                    
                            
                           
                
     
                 
                                      
                       
                           
         
                     
                 
                              
         
                                
                         
           
                                
                         
           
                              
                                
                                
                          
             

                                                        
                          
                                       
                                                                        
                                           
                                                          
                                                                     
                                                
             

           
                                
                        
                                
                                
                        
                                
                           
                  
                          
           
                                   
                            
                                       
                        
                                  
                          
                                       
           
                            
                                      
         
       
                                 
     
                  
                                  
                          
     
    return ((char *)0);
  }
}
int unquote_string(char *string) {
  int result;
  char *source;
  char *destination;
  char *tmp;
  char *tmp___0;
  char *tmp___1;
  char *tmp___2;
  char *tmp___3;
  char *tmp___4;
  char *tmp___5;
  int value;
  char *tmp___6;
  char *tmp___7;
  char *tmp___8;
  char *tmp___9;
  char *tmp___10;
  char *tmp___11;
  char *tmp___12;
  char *tmp___13;
  char *tmp___14;
  char *tmp___15;
  char *tmp___16;

  {
               
    source = string;
    destination = string;
    {
                {
                                     ;
                       
                           
         
                                 
                   
           
                                     
                           
             
                                      
                            
             
                                      
                            
             
                                      
                            
             
                                     
                           
             
                                      
                            
             
                                     
                           
             
                                     
                           
             
                                     
                           
             
                                     
                           
             
                                     
                           
             
                                     
                           
             
                                     
                           
             
                                     
                           
             
                                     
                           
             
                                
                                  
                              
                          
                              
                     
                              
                                   
                                  
                          
                                  
                     
                              
                                   
                                  
                          
                                  
                     
                              
                                   
                                  
                          
                                  
                     
                              
                                  
                                  
                          
                                  
                     
                              
                                   
                                  
                          
                                  
                     
                              
                                  
                                  
                          
                                 
                     
                              
                                  
                                  
                                   
                                   
                                   
                                   
                                   
                                   
                              
                     
                                       
                                    
                                    
                            
                                     
                                
                    
                                      
                                      
                              
                                       
                                  
               
             
                             
                     
                                                     
                                    
                                    
                            
                                     
                                
                    
                                      
                                      
                              
                                       
                                  
               
             
                              
                     
                                                      
                                   
                          
                                    
                              
                                         
                       
                                   
                          
                                   
                          
                                     
                            
                                
                       
                                    
             
                              
                                        
           
               {
                                                                   {
            tmp___15 = destination;
            destination++;
            tmp___16 = source;
            source++;
            *tmp___15 = *tmp___16;
          }       
                     
                          
           
        }
      }
    while_break: /* CIL Label */;
    }
                                                              
                                  
     
    return (result);
  }
}
static char *before_backup_name;
static char *after_backup_name;
static _Bool must_be_dot_or_slash(char const *path) {
  int tmp;

  {
              
                                        {
       

                  {
                                       ;
                                              {
                   
          }       
                                                 
                               
                                                                        
                       
                                                             
                      
                                               
               
                    
                                             
             
           
        }
                                  ;
       

    }       
       
                   
                                            
                                               
                                                    
                                   
             
                  
                                 
           
                    
           
                       
                                                
                                                
                                     
               
                     
             
                                           
           
         
                                       
       
                         
                
              
                                             
                             
                    
                  
                    
           
                
                  
         
       
                          
     
  }
}
static int safer_rmdir(char const *path) {
  int *tmp;
  _Bool tmp___0;
  int tmp___1;

  {
     
                                           
     
                  
       
                                 
                 
       
                  
     
    {
      tmp___1 = rmdir(path);
    }
    return (tmp___1);
  }
}
int remove_any_file(char const *path, enum remove_option option) {
  int tmp;
  int *tmp___0;
  int *tmp___1;
  int tmp___2;
  int *tmp___3;
  int tmp___4;
  int tmp___5;
  char *directory;
  char *tmp___6;
  char const *entry;
  size_t entrylen;
  char *path_buffer;
  char *tmp___7;
  int r;
  int tmp___8;
  int e;
  int *tmp___9;
  int *tmp___10;
  int tmp___11;

  {
                       
       
                           
       
                     
                   
       
       
	                             
       
                          
         
                                       
         
                             
                     
         
       
     
     
                                  
     
                       
                 
     
     
                                   
     
     
                           
                     
       
                          
                    
       
                           
                    
       
                           
                    
       
                        
                            
                        
         
                                 
         
                           
                      
                
                      
         
              
                    
       
                       
                             
                             
                              
      
                                       
                        
       
                                       
                    
       
                                       
                    
       
                            
                               
                            
                           
                  
                            
     
                              
                          
     
                       
                   
       
                                      
       
                   
                                        
           
	                             
	   
                                   
                             
           
           
                                            
                                  
                                                                
                                                             
                        
                                         
                         
                                      
           
                   
             
                                      
                                            
                            
             
                       
           
                                  
         
                                   
       
       
                                
                                     
       
                             
                                      
     
                        
                                  
     
    return (0);
  }
}
_Bool maybe_backup_file(char const *path, int this_is_the_archive) {
  struct stat file_stat;
  void *tmp;
  int *tmp___0;
  int tmp___1;
  char const *tmp___2;
  char const *tmp___3;
  char *tmp___4;
  int e;
  int *tmp___5;
  char const *tmp___6;
  char *tmp___7;
  char *tmp___8;
  int tmp___9;
                    
                    
                    

  {
                              
                                
         
                                         
         
                         
                                                                
             
                                                   
                                                                
             
                       
                                
             
           
         
       
     
     
                                                          
                                                                    
     
                  
       
                                     
       
                          
                          
       
       
	                 
       
                        
     
                                                 
                        
     
                              
                                                   
                          
              
                                                    
                            
         
       
     
     
                                               
                                                         
                                                                   
     
                             
       

                     
       

     
    {
      tmp___9 = rename((char const *)before_backup_name,
                       (char const *)after_backup_name);
    }
                      {
                           
         

                                                                
                                                                 
                                                   
                                                    
                                                                             
         

       
      return ((_Bool)1);
    }       
       
                                     
                     
                                                              
                                                                   
                                                     
                                                             
                        
                                                           
       
                        
     
  }
}
void undo_last_backup(void) {
  int e;
  int *tmp;
  char const *tmp___0;
  char *tmp___1;
  char *tmp___2;
  int tmp___3;
  char const *tmp___4;
  char const *tmp___5;
  char *tmp___6;
                    
                    

   
                            
       
                                                         
                                                           
       
                         
         

                                   
                   
                                                                 
                                                                    
                                                       
                                                               
                          
         

       
                           
         

                                                                 
                                                                
                                                        
                                                    
                                                                             
         

       
       
	                                                   
       
     
           
   
}
int deref_stat(_Bool deref, char const *name, struct stat *buf) {
  int tmp;
  int tmp___0;
  int tmp___1;

  {
               {
       

        tmp = stat((char const * /* __restrict  */)name,
                   (struct stat * /* __restrict  */) buf);
        tmp___1 = tmp;
       

    }       
       

                                                             
                                                               
                          
       

     
    return (tmp___1);
  }
}
static struct wd *wd;
static size_t wds;
static size_t wd_alloc;
int chdir_arg(char const *dir) {
  void *tmp;
  size_t tmp___0;
                   

  {
                          
       
                                          
                                                           
                              
       
                 
                               
                              
              
       
     
                     
       
                   
                                        
                                              
                                                   
                               
             
                  
                             
           
                   
           
                       
                                                
                                               
                                     
               
                          
                   
                    
             
                                           
           
         
                                   
       
                                                      
                                  
       
     
                           
                          
    tmp___0 = wds;
          
    return ((int)tmp___0);
  }
}
static int previous;
void chdir_do(int i) {
  struct wd *prev;
  struct wd *curr;
  char *tmp;
  int tmp___0;
  char *tmp___1;
  int tmp___2;
  int tmp___3;
                   
                    

   
                        
                           
                    
                         
         
                          
                                               
         
                           
           

                                                           
                                           
                         
           

         
       
                        
         
                                                                              
         
                      
           

                                                                 
                                               
                         
           

         
              
                
                                                        
             

                              
             

           
         
         
	                              
	 
                           
           

                                    
           

         
       
                   
     
           
   
}
void decode_mode(mode_t mode, char *string) {
  char *tmp;
  char *tmp___0;
  char *tmp___1;
  char *tmp___2;
  char *tmp___3;
  char *tmp___4;
  char *tmp___5;
  char *tmp___6;
  char *tmp___7;
  int tmp___8;
  int tmp___9;
  int tmp___10;
  int tmp___11;
  int tmp___12;
  int tmp___13;
  int tmp___14;
  int tmp___15;
  int tmp___16;
  int tmp___17;
  int tmp___18;
  int tmp___19;
  int tmp___20;
  int tmp___21;
  int tmp___22;

   
                 
             
                      
                    
            
                    
     
                         
                     
             
                      
                    
            
                    
     
                             
                     
             
                       
                       
                       
              
                       
       
                          
            
                       
                       
              
                       
       
                          
     
                              
                     
             
                                          
                     
            
                     
     
                              
                     
             
                                          
                     
            
                     
     
                              
                     
             
                       
                                           
                       
              
                       
       
                          
            
                                           
                       
              
                       
       
                          
     
                              
                     
             
                                                 
                     
            
                     
     
                              
                     
             
                                                 
                     
            
                     
     
                              
                     
             
                      
                                                  
                       
              
                       
       
                          
            
                                                  
                       
              
                       
       
                          
     
                              
                           
           
   
}
static void call_arg_error(char const *call, char const *name) {
  int e;
  int *tmp;
  char *tmp___0;
  char *tmp___1;
                   

   
     
                               
               
                                     
                                         
                                                        
                      
     
           
   
}
static __attribute__((__noreturn__)) void call_arg_fatal(char const *call,
                                                         char const *name);
static void call_arg_fatal(char const *call, char const *name) {
  int e;
  int *tmp;
  char *tmp___0;
  char *tmp___1;
                   

   
     

                               
               
                                     
                                         
                                                        
                   
     

   
}
static void call_arg_warn(char const *call, char const *name) {
  int e;
  int *tmp;
  char *tmp___0;
  char *tmp___1;
                   

   
     
                               
               
                                     
                                                  
                                                        
     
           
   
}
__attribute__((__noreturn__)) void chdir_fatal(char const *name);
void chdir_fatal(char const *name) {
                   

   

     

                                    
     

   

}
void chmod_error_details(char const *name, mode_t mode) {
  int e;
  int *tmp;
  char buf[10];
  char *tmp___0;
  char *tmp___1;
                   
                   

   
     
                               
               
                             
                                     
                                                        
                                                       
                      
     
           
   
}
void chown_error_details(char const *name, uid_t uid, gid_t gid) {
  int e;
  int *tmp;
  char *tmp___0;
  char *tmp___1;
                   

   
     
                               
               
                                     
                                                                           
                                                                     
                                
                      
     
           
   
}
void close_error(char const *name) {
                   

   

     
                                    
     
           
   

}
void close_warn(char const *name) {
                   

   

     
                                   
     
           
   

}
void close_diag(char const *name) {

   

                                    
       

                         
       

            
       

	                  
       

     
           
   

}
__attribute__((__noreturn__)) void exec_fatal(char const *name);
void exec_fatal(char const *name) {
                   

   

     

                                   
     

   

}
void link_error(char const *target, char const *source) {
  int e;
  int *tmp;
  char const *tmp___0;
  char *tmp___1;
  char *tmp___2;
                   

   
     
                               
               
                                   
                                       
                                                      
                                                           
                      
     
           
   
}
void mkdir_error(char const *name) {
                   

   

     
                                    
     
           
   

}
void mkfifo_error(char const *name) {
                   

   

     
                                     
     
           
   

}
void mknod_error(char const *name) {
                   

   

     
                                    
     
           
   

}
void open_error(char const *name) {
                   

   

     
                                   
     
           
   

}
__attribute__((__noreturn__)) void open_fatal(char const *name);
void open_fatal(char const *name) {
                   

   

     

                                   
     

   

}
void open_warn(char const *name) {
                   

   

     
                                  
     
           
   

}
void open_diag(char const *name) {

   

                                    
       

                        
       

            
       

	                 
       

     
           
   

}
void read_error(char const *name) {
                   

   

     
                                   
     
           
   

}
void read_error_details(char const *name, off_t offset, size_t size) {
  char buf[(((sizeof(uintmax_t) * 8UL) * 302UL) / 1000UL + 1UL) + 1UL];
  int e;
  int *tmp;
  char *tmp___0;
  char *tmp___1;
  char *tmp___2;
                    

   
     
                               
               
                                              
                            
                                                                              
                                     
                                                                       
                                                                               
                                                                 
                      
     
           
   
}
void read_warn_details(char const *name, off_t offset, size_t size) {
  char buf[(((sizeof(uintmax_t) * 8UL) * 302UL) / 1000UL + 1UL) + 1UL];
  int e;
  int *tmp;
  char *tmp___0;
  char *tmp___1;
  char *tmp___2;
                    

   
     
                               
               
                                              
                            
                                                                              
                                     
                         
                                                                 
                                                                         
                                                                 
     
           
   
}
void read_diag_details(char const *name, off_t offset, size_t size) {

   

                                    
       

                                              
       

            
       

	                                       
       

     
           
   

}
__attribute__((__noreturn__)) void read_fatal(char const *name);
void read_fatal(char const *name) {
                   

   

     

                                   
     

   

}
__attribute__((__noreturn__)) void
read_fatal_details(char const *name, off_t offset, size_t size);
void read_fatal_details(char const *name, off_t offset, size_t size) {
  char buf[(((sizeof(uintmax_t) * 8UL) * 302UL) / 1000UL + 1UL) + 1UL];
  int e;
  int *tmp;
  char *tmp___0;
  char *tmp___1;
  char *tmp___2;
                    

   
     

                               
               
                                              
                            
                                                                              
                                     
                                                                       
                                                                               
                                                                 
                   
     

   
}
void readlink_error(char const *name) {
                   

   

     
                                       
     
           
   

}
void readlink_warn(char const *name) {
                   

   

     
                                      
     
           
   

}
void readlink_diag(char const *name) {

   

                                    
       

                            
       

            
       

	                     
       

     
           
   

}
void savedir_error(char const *name) {
                   

   

     
                                      
     
           
   

}
void savedir_warn(char const *name) {
                   

   

     
                                     
     
           
   

}
void savedir_diag(char const *name) {

   

                                    
       

                           
       

            
       

	                    
       

     
           
   

}
void seek_error(char const *name) {
                   

   

     
                                   
     
           
   

}
void seek_error_details(char const *name, off_t offset) {
  char buf[(((sizeof(uintmax_t) * 8UL) * 302UL) / 1000UL + 1UL) + 1UL];
  int e;
  int *tmp;
  char *tmp___0;
  char *tmp___1;
  char *tmp___2;
                   
                    

   
     
                               
               
                                              
                            
                                                                              
                                     
                                                 
                                                           
                      
     
           
   
}
void seek_warn(char const *name) {
                   

   

     
                                  
     
           
   

}
void seek_warn_details(char const *name, off_t offset) {
  char buf[(((sizeof(uintmax_t) * 8UL) * 302UL) / 1000UL + 1UL) + 1UL];
  int e;
  int *tmp;
  char *tmp___0;
  char *tmp___1;
  char *tmp___2;
                   
                    

   
     
                               
               
                                              
                            
                                                                              
                                     
                                                          
                                                           
     
           
   
}
void seek_diag_details(char const *name, off_t offset) {

   

                                    
       

                                        
       

            
       

	                                 
       

     
           
   

}
void symlink_error(char const *contents, char const *name) {
  int e;
  int *tmp;
  char const *tmp___0;
  char *tmp___1;
  char *tmp___2;
                   

   
     
                               
               
                                     
                                     
                                                           
                                                           
                      
     
           
   
}
void stat_error(char const *name) {
                   

   

     
                                   
     
           
   

}
void stat_warn(char const *name) {
                   

   

     
                                  
     
           
   

}
void stat_diag(char const *name) {

   

                                    
       

                        
       

            
       

	                 
       

     
           
   

}
void truncate_error(char const *name) {
                   

   

     
                                       
     
           
   

}
void truncate_warn(char const *name) {
                   

   

     
                                      
     
           
   

}
void unlink_error(char const *name) {
                   

   

     
                                     
     
           
   

}
void utime_error(char const *name) {
                   

   

     
                                    
     
           
   

}
void waitpid_error(char const *name) {
                   

   

     
                                      
     
           
   

}
void write_error(char const *name) {
                   

   

     
                                    
     
           
   

}
void write_error_details(char const *name, size_t status, size_t size) {
  char *tmp;

   
                        
       

                          
       

            
       

                                                        
                                                                
                                                           
                        
       

     
           
   
}
__attribute__((__noreturn__)) void
write_fatal_details(char const *name, ssize_t status, size_t size);
void write_fatal_details(char const *name, ssize_t status, size_t size) {

   

     

                                                      
                   
     

   

}
pid_t xfork(void) {
  pid_t p;
  __pid_t tmp;
  char *tmp___0;
                   
                   

  {
    {
      tmp = fork();
      p = tmp;
    }
                  
       

                                           
                                                      
       

     
    return (p);
  }
}
void xpipe(int *fd) {
  char *tmp;
  int tmp___0;
                   
                   

   
     
                         
     
                      
       

                                              
                                                  
       

     
           
   
}
char const *quote_n(int n, char const *name) {
  char *tmp;

  {
    {
      tmp = quotearg_n_style(n, (enum quoting_style)5, name);
    }
    return ((char const *)tmp);
  }
}
char const *quote(char const *name) {
  char const *tmp;

  {
    {
      tmp = quote_n(0, name);
    }
    return (tmp);
  }
}
/* #pragma merger("0","00a.names.o.i","") */
extern int getc_unlocked(FILE *__stream);
extern struct passwd *getpwuid(__uid_t __uid);
extern struct passwd *getpwnam(char const *__name);
extern struct group *getgrgid(__gid_t __gid);
extern struct group *getgrnam(char const *__name);
_Bool excluded_filename(struct exclude const *ex, char const *f);
char filename_terminator;
struct exclude *excluded;
char const *files_from_option;
_Bool same_order_option;
_Bool starting_file_option;
void init_names(void);
void name_add(char const *name);
void name_init(void);
void name_term(void);
void name_close(void);
struct name *addname(char const *string, int change_dir___0);
void add_avoided_name(char const *name);
void request_stdin(char const *option);
static char *cached_uname;
static char *cached_gname;
static uid_t cached_uid;
static gid_t cached_gid;
static char *cached_no_such_uname;
static char *cached_no_such_gname;
static uid_t cached_no_such_uid;
static gid_t cached_no_such_gid;
void uid_to_uname(uid_t uid, char **uname) {
  struct passwd *passwd;
                   
                   

   
                    
                                      
         
                              
         
               
       
     
                        
                  
            
                              
                              
       
                               
       
                     
           

                             
                                                                        
           

                
           
                                     
                                
           
                 
         
       
     
     
                                                  
     
           
   
}
void gid_to_gname(gid_t gid, char **gname) {
  struct group *group;
                   
                   

   
                    
                                      
         
                              
         
               
       
     
                        
                  
            
                              
                              
       
                              
       
                    
           

                             
                                                                       
           

                
           
                                     
                                
           
                 
         
       
     
     
                                                  
     
           
   
}
int uname_to_uid(char const *uname, uid_t *uidp) {
  struct passwd *passwd;
  int tmp;
  int tmp___0;

  {
                               
       
                                                                
       
                     
                   
       
     
                        
              
            
                                                                          
                
              
         
	                                                      
	 
                           
              
	                             
	   
                       
             

                                          
                                                                          
             

                  
             
	                                                  
	     
                       
           
         
       
     
                       
    return (1);
  }
}
int gname_to_gid(char const *gname, gid_t *gidp) {
  struct group *group;
  int tmp;
  int tmp___0;

  {
                               
       
                                                                
       
                     
                   
       
     
                        
              
            
                                                                          
                
              
         
	                                                      
	 
                           
              
	                            
	   
                      
             

                                         
                                                  
             

                  
             
	                                                  
	     
                       
           
         
       
     
                       
    return (1);
  }
}
static struct name *namelist;
static struct name **nametail = &namelist;
static char const **name_array;
static int allocated_names;
static int names;
static int name_index;
void init_names(void) {
  void *tmp;

  {
    {
      allocated_names = 10;
      tmp = xmalloc(sizeof(char const *) * (unsigned long)allocated_names);
      name_array = (char const **)tmp;
                
    }
           
  }
}
void name_add(char const *name) {
  void *tmp;
  int tmp___0;

  {
                                   
       

                             
                                          
                                                                              
                                        
       

     
    tmp___0 = names;
    names++;
    *(name_array + tmp___0) = name;
           
  }
}
static FILE *name_file;
static char *name_buffer;
static size_t name_buffer_length;
__inline static int is_pattern(char const *string) {
  char *tmp;
  char *tmp___0;
  char *tmp___1;
  int tmp___2;

  {
     
                                
     
             {
      tmp___2 = 1;
    }       
       
	                              
       
                    
                    
              
         
	                                
	 
                      
                      
                
                      
         
       
     
    return (tmp___2);
  }
}
void name_init(void) {
  void *tmp;
  int tmp___0;
                   

  {
    {
      tmp = xmalloc((size_t)102);
      name_buffer = (char *)tmp;
      name_buffer_length = (size_t)100;
    }
                            
       
                                                 
       
                    
         
                                                                              
                                                                  
         
                         
           

                                          
           

         
              
         

                              
                            
         

       
     
           
  }
}
void name_term(void) {

   

     
                                
                               
     
           
   

}
static int read_name_from_file(void) {
  int character;
  size_t counter;
  void *tmp;
  size_t tmp___0;
  void *tmp___1;

  {
                        
     
                 
                                      
         
	                                       
	 
                              
                                                         
                             
           
                
                           
         
                                            
                                                              
             

                           
             

           
           
                                      
                                                                          
                                      
           
         
                          
                  
                                                   
       
                                 
     
                         
                            
                   
       
     
                                        
                                                          
         

                       
         

       
       
                                  
                                                                          
                                      
       
     
                                            
    return (1);
  }
}
char *name_next(int change_dirs) {
  char const *source;
  char *cursor;
  int chdir_flag;
  int tmp;
  size_t source_len;
  int tmp___0;
  void *tmp___1;
  size_t tmp___2;
  char *tmp___3;
  int tmp___4;
  int tmp___5;
  char *tmp___6;
                    

  {
    chdir_flag = 0;
                                        
                      
     
    {
                {
                                     ;
        if (name_index == names) {
                          {
            goto while_break;
          }
           
	                                
	   
                     
                             
           
        } else {
          {
            tmp___0 = name_index;
            name_index++;
            source = *(name_array + tmp___0);
                                        
          }
                                                
             
                         
                                                  
                                          
                                          
                   

                                 
                   

                 
                                                         
                                       
                 
               
                                             
             
             
                                        
                                                          
                                            
             
           
          {
            strcpy((char * /* __restrict  */)name_buffer,
                   (char const * /* __restrict  */)source);
          }
        }
        {
                                                      
                                               
        }
         
                     
                                              
                                                                     
                                          
                                     
               
                    
                                   
             
                             
                     
                                    
           
                                         
         
                         
           
                                                       
           
                            
             

                                                     
             

           
                         
               {
                           {
            {
              tmp___5 = strcmp((char const *)name_buffer, "-C");
            }
                               
                             
                   {
               
		                            
	       
              return (name_buffer);
            }
          }       
             
	                                  
	     
                                 
           
        }
      }
    while_break: /* CIL Label */;
    }
                    
                       
         

                                                          
                                             
                       
         

       
     
    return ((char *)0);
  }
}
void name_close(void) {
  int tmp;

   
                    
                                                             
         
                                  
         
                       
           

                                                   
           

         
       
     
           
   
}
static struct name *buffer___0;
static size_t allocated_size;
static int change_dir;
void name_gather(void) {
  char const *name;
  void *tmp;
  char const *dir;
  char *tmp___0;
  char *tmp___1;
  char *tmp___2;
  char *tmp___3;
  int tmp___4;
  size_t needed_size;
  void *tmp___5;
  int change_dir___0;
  int change_dir0;
  char const *dir___0;
  char *tmp___6;
  char *tmp___7;
  char *tmp___8;
  char *tmp___9;
  int tmp___10;
                    
                    

  {
                            
                                  
         

                          
                                                                         
                                        
                                          
                                                        
         

       
       
                   
                                        
           
                                   
                                         
           
                     
             
                                           
             
                                  
                               
             
                  
                             
           
           
                                   
                                        
           
                     
             

                                                              
                                                 
                           
             

           
           
                                   
                                                          
           
         
                                   
       
                 
         
                                            
                                                                     
                                              
                            
         
                                           
           
                       
                                                
                                    
                                    
                 

                               
                 

               
                                                    
                                     
               
             
                                           
           
           
                                                                   
                                                
           
         
         
                                              
                                                              
                                                       
                                              
                                                 
                                
                                     
         
              
                         
           

                                                 
           

         
       
           {
                         
      {
        while (1) {
                                           ;
          change_dir0 = change_dir___0;
          {
                      {
                                               ;
              {
                tmp___9 = name_next(0);
                name = (char const *)tmp___9;
              }
                         
                 
                                                
                 
                                       
                                       
                 
                     {
                goto while_break___2;
              }
              {
                tmp___6 = name_next(0);
                dir___0 = (char const *)tmp___6;
              }
                             
                 

                                                                  
                                                     
                               
                 

               
              {
                tmp___8 = xstrdup(dir___0);
                change_dir___0 = chdir_arg((char const *)tmp___8);
              }
            }
          while_break___2: /* CIL Label */;
          }
          if (name) {
             

              addname(name, change_dir___0);
             

          } else {
                                                
               

                                                         
               

             
            goto while_break___1;
          }
        }
      while_break___1: /* CIL Label */;
      }
    }
           
  }
}
struct name *addname(char const *string, int change_dir___0) {
  size_t length;
  size_t tmp;
  size_t tmp___0;
  struct name *name;
  void *tmp___1;
  int tmp___2;

  {
                {
       

        tmp = strlen(string);
        tmp___0 = tmp;
       

    }       
                          
     
    {
      length = tmp___0;
      tmp___1 =
          xmalloc(((unsigned long)(&((struct name *)0)->name) + length) + 1UL);
      name = (struct name *)tmp___1;
    }
                {
       

                             
        strcpy((char * /* __restrict  */)(name->name),
               (char const * /* __restrict  */)string);
       

    }       
                           
                              
     
                                  
                          
                                     
                           
                            
                                      
                                         
                {
      {
        tmp___2 = is_pattern(string);
      }
                   {
        name->regexp = (char)1;
                                               
                                  
                
                                                 
                                    
                  
                                                   
                                      
             
           
         
      }
    }
    *nametail = name;
    nametail = &name->next;
    return (name);
  }
}
static struct name *namelist_match(char const *path, size_t length) {
  struct name *p;
  int tmp;
  int tmp___0;
  int tmp___1;
  int tmp___2;

  {
    p = namelist;
    {
      while (1) {
                                     ;
        if (!p) {
          goto while_break;
        }
                         
                                                    
                        
           
         
                       {
           

            tmp = fnmatch((char const *)(p->name), path, recursion_option);
            tmp___2 = tmp == 0;
           

        }       
                                    
                                                        
                          
                    
                                                           
                                       
                                        
                 
                                                                               
                                              
                 
                                     
                                
                          
                                
                   
                        
                              
                 
                      
                            
               
             
                  
                        
           
                            
         
        if (tmp___2) {
          return (p);
        }
      __Cont:
        p = p->next;
      }
    while_break: /* CIL Label */;
    }
    return ((struct name *)0);
  }
}
int name_match(char const *path) {
  size_t length;
  size_t tmp;
  struct name *cursor;
  uintmax_t tmp___0;

  {
    {
      tmp = strlen(path);
      length = tmp;
    }
    {
                {
                                     ;
                          
                      
                                      
         
                           
           
                                         
                                        
                                 
           
                                      
         
         
	                                        
	 
                     
                                                            
                                   
                                               
                                        
               
                    
                                      
             
                  
                                    
           
                                     
             

                                     
                                          
                                   
             

           
           
	                                 
	   
                                         
                                          
                  
                                                                            
           
                                
         
                               {
                                     {
             
                            
             
                                       {
              return (0);
            }
          }       
                       
           
        }       
                     
         
      }
                                ;
    }
  }
}
_Bool all_names_found(struct tar_stat_info *p) {
  struct name const *cursor;
  size_t len;
  uintmax_t tmp;

  {
                       {
      return ((_Bool)0);
    }       
                                     
                          
              
                                    
                            
         
       
     
     
                                               
                                             
     
     
                 
                                      
                      
                           
         
                             
                            
                
                                         
                                      
                  
                                                          
                                                                        
           
                    
                                 
                                                
                                                                
                                  
               
             
                  
                                
                                
                    
                          
             
           
         
                                                   
       
                                 
     
                      
  }
}
void names_notfound(void) {
  struct name const *cursor;
  char *tmp;
  char *tmp___0;
  char *tmp___1;
  char *tmp___2;
  char *name;
  char *tmp___3;
  char *tmp___4;
  uintmax_t tmp___5;
                    
                    
                    

   
                                           
     
                 
                                      
                      
                           
         
                                       
                                        
                
                                                            
                                                                          
         
                       
                              
                                             
               

                                                                   
                                                              
                                                        
                                
               

                    
               

                                                                       
                         
                                                                            
                                                            
                                
               

             
           
         
                                                   
       
                                 
     
                                
                         
                            
       

                   
                                            
           
	                        
	   
                                                                     
                                 
           
           
                                                         
                                                          
                                                        
                            
           
         
                                       
       

     
           
   
}
static struct name *merge_sort(struct name *list, int length,
                               int (*compare)(struct name const *,
                                              struct name const *)) {
  struct name *first_list;
  struct name *second_list;
  int first_length;
  int second_length;
  struct name *result;
  struct name **merge_point;
  struct name *cursor;
  int counter;
  int tmp;
  int tmp___0;

  {
                      
                    
     
                     {
       
                                                   
                                                          
       
                    
                            
                            
                                      
                        
       
      return (list);
    }
                      
                                    
                               
                  
                               
     
                 
                                      
                       
                           
         
                    
             
                              
                  
       
                                 
     
     
                                 
                                      
                                                                 
                                                                    
                            
     
     
                 
                                          
                         
                             
                                 
           
                
                               
         
         
                                                               
                                                                 
         
                          
                                    
                                    
                                          
                              
                
                                     
                                     
                                           
                               
         
       
                                     
     
                     
                                
            
                                 
     
                    
  }
}
static int compare_names(struct name const *n1, struct name const *n2) {
  int found_diff;
  int tmp;
  int tmp___0;
  uintmax_t tmp___1;
  uintmax_t tmp___2;

  {
                                  {
      tmp___1 = n2->found_count;
    }       
                                                    
                                                                      
     
                                  {
      tmp___2 = n1->found_count;
    }       
                                                    
                                                                      
     
    found_diff = (int)(tmp___1 - tmp___2);
                    {
      tmp___0 = found_diff;
    }       
       

                                                                         
                      
       

     
    return (tmp___0);
  }
}
static void add_hierarchy_to_namelist(struct name *name, dev_t device) {
  char *path;
  char *buffer___2;
  char *tmp;
  size_t name_length;
  size_t allocated_length;
  char *namebuf;
  void *tmp___0;
  char *string;
  size_t string_length;
  int change_dir___0;
  size_t tmp___1;
  void *tmp___2;
  struct name *tmp___3;
                    

   
     
                        
                                                 
                       
     
                      
                                              
            
                                 
                                 
                                               
              
                                       
       
       
                                                  
                                  
                                          
                                                      
                                                 
                                                     
       
                                                           
                              
                      
                                         
                                                
       
                          
       
                   
                                        
                         
                             
           
           
	                                                 
	   
                                   
                                                                  
               
                           
                                                    
                                          
                                          
                     

                                   
                     

                   
                                                                           
                                         
                   
                 
                                               
               
               
                                                                            
                                          
               
             
             
                                                                       
                                                                   
                                                                       
                                                         
             
           
                                        
         
                                   
       
       
	                      
       
     
           
   
}
void collect_and_sort_names(void) {
  struct name *name;
  struct name *next_name;
  int num_names;
  struct stat statbuf;
  int tmp;
                   
                   

   
     
                    
     
                                    
       

                              
       

     
                    
       

                        
       

     
                    
     
                 
                                      
                    
                           
         
                               
                                
                      
                
                                   
                        
           
         
                           
                      
         
         
	                             
	 
                         
                      
         
         
                                                                          
                                     
         
                       
           
                                                  
           
                      
         
                                                   
           

                                  
                                                            
           

         
             
                         
       
                                 
     
                  
                    
     
                 
                                          
                    
                               
         
                    
                          
       
                                     
     
     
                                                                 
                      
     
     
                 
                                          
                    
                               
         
                                         
                          
       
                                     
     
           
   
}
struct name *name_scan(char const *path) {
  size_t length;
  size_t tmp;
  struct name *cursor;
  struct name *tmp___0;

  {
    {
      tmp = strlen(path);
      length = tmp;
    }
    {
                {
                                     ;
        {
          tmp___0 = namelist_match(path, length);
          cursor = tmp___0;
        }
                    {
          return (cursor);
        }
                               {
                         
                                        
               
                              
               
                                          
                                          
               
                    
                                        
             
                  
                                      
           
        }       
                                    
         
      }
                                ;
    }
  }
}
char *name_from_list(void) {

   

                        {
      gnu_list_name = namelist;
    }
    {
                {
                                     ;
                           {
          if (!gnu_list_name->found_count) {
                                      {
              goto while_break;
            }
          }
        }       
                           
         
        gnu_list_name = gnu_list_name->next;
      }
    while_break: /* CIL Label */;
    }
    if (gnu_list_name) {
      {
        (gnu_list_name->found_count)++;
                                            
      }
      return (gnu_list_name->name);
    }
    return ((char *)0);
   

}
void blank_name_list(void) {
  struct name *name;

   
                                     
                    
     
                 
                                      
                    
                           
         
                                         
                          
       
                                 
     
           
   
}
char *new_name(char const *path, char const *name) {
  size_t pathlen;
  size_t tmp;
  size_t namesize;
  size_t tmp___0;
  int slash;
  char *buffer___2;
  void *tmp___1;
  int tmp___2;

  {
    {
                         
                    
      tmp___0 = strlen(name);
      namesize = tmp___0 + 1UL;
    }
                 {
                                                            
                    
              
                    
       
    }       
                  
     
    {
                      
                                                              
                                   
                                                  
                                                            
      *(buffer___2 + pathlen) = (char)'/';
                                                                        
                                                             
    }
    return (buffer___2);
  }
}
_Bool excluded_name(char const *name) {
  _Bool tmp;

  {
    {
      tmp = excluded_filename((struct exclude const *)excluded, name + 0);
    }
    return (tmp);
  }
}
static unsigned int hash_string_hasher(void const *name,
                                       unsigned int n_buckets) {
  size_t tmp;

  {
    {
      tmp = hash_string((char const *)name, (size_t)n_buckets);
    }
    return ((unsigned int)tmp);
  }
}
static _Bool hash_string_compare(void const *name1, void const *name2) {
  int tmp;

  {
    {
      tmp = strcmp((char const *)name1, (char const *)name2);
    }
    return ((_Bool)(tmp == 0));
  }
}
static _Bool hash_string_insert(Hash_table **table___0, char const *string) {
  Hash_table *t;
  char *s;
  char *tmp;
  char *e;
  Hash_table *tmp___0;
  void *tmp___1;

  {
    {
                     
      tmp = xstrdup(string);
      s = tmp;
    }
            
              
           {
       
                            
                                              
                                                                   
                                                       
                    
                             
       
                    
            
                                                  
                            
       
                 
           

                         
           

         
              
         

	               
	 

       
    }
                                              {
      return ((_Bool)1);
    }       
       
	                
       
                        
     
  }
}
static _Bool hash_string_lookup(Hash_table const *table___0,
                                char const *string) {
  void *tmp;
  int tmp___0;

  {
                    
       
                                                           
       
                
                    
              
                    
       
           {
      tmp___0 = 0;
    }
    return ((_Bool)tmp___0);
  }
}
static Hash_table *avoided_name_table;
void add_avoided_name(char const *name) {

   

     
                                                    
     
           
   

}
_Bool is_avoided_name(char const *name) {
  _Bool tmp;

  {
    {
      tmp = hash_string_lookup((Hash_table const *)avoided_name_table, name);
    }
    return (tmp);
  }
}
static Hash_table *prefix_table[2];
static char const *const diagnostic[2] = {
    (char const * /* const  */) "Removing leading `%s\' from member names",
    (char const
         * /* const  */) "Removing leading `%s\' from hard link targets"};
static char const *const diagnostic___0[2] = {
    (char const * /* const  */) "Substituting `.\' for empty member name",
    (char const * /* const  */) "Substituting `.\' for empty hard link target"};
char *safer_name_suffix(char const *file_name, _Bool link_target) {
  char const *p;
  size_t prefix_len;
  char c;
  char const *tmp;
  char *prefix;
  void *tmp___0;
  char *tmp___1;
  _Bool tmp___2;
  char *tmp___3;
                    

  {
                               {
      p = file_name;
    }       
                             
                                 
       
                   
                                        
                    
                             
           
                                            
                                              
                                                
                                                           
                      
                                
                                                             
                 
               
             
           
           
                       
                                                
                      
                  
                             
                                 
                                     
               
                        
                                     
               
             
                                           
           
         
                                   
       
                                 
       
                   
                                            
                                         
                                 
           
                      
               
              
         
                                       
       
                                           
                       
         
                                                       
                                   
                                                  
                                                                        
                                                
                                                                  
                                                             
         
                      
           

                                                                     
                                                       
           

         
       
     
              
                                                         
         

                                                                       
                                             
         

       
              
     
    return ((char *)p);
  }
}
size_t stripped_prefix_len(char const *file_name, size_t num) {
  char const *p;
  _Bool slash;

  {
                      
     
                 
                                      
                                       
                           
         
            
       
                                 
     
     
                 
                                          
                  
                               
         
                                               
            
                    
                
                           
                                             
           
           
                       
                                                
                                             
                                     
               
                  
             
                                           
           
         
       
                                     
     
    return ((size_t)-1);
  }
}
_Bool contains_dot_dot(char const *name) {
  char const *p;
  char const *tmp;

  {
                 
    {
                {
                                     ;
                                          
                                            
                                              
                                
                    
                              
                                  
               
             
           
         
        {
                    {
                                             ;
            tmp = p;
            p++;
                       {
              return ((_Bool)0);
            }
                                              
                                   
             
          }
        while_break___0: /* CIL Label */;
        }
            
      }
                                ;
    }
  }
}
/* #pragma merger("0","00b.rtapelib.o.i","") */
extern
    __attribute__((__nothrow__)) int(__attribute__((__leaf__)) dup)(int __fd);
extern __attribute__((__nothrow__)) int(
    __attribute__((__nonnull__(1, 2), __leaf__))
    execl)(char const *__path, char const *__arg, ...);
extern __attribute__((__nothrow__))
__sighandler_t(__attribute__((__leaf__)) signal)(int __sig,
                                                 void (*__handler)(int));
extern
    __attribute__((__nothrow__)) int(__attribute__((__nonnull__(1), __leaf__))
                                     atoi)(char const *__nptr)
        __attribute__((__pure__));
extern
    __attribute__((__nothrow__)) long(__attribute__((__nonnull__(1), __leaf__))
                                      atol)(char const *__nptr)
        __attribute__((__pure__));
char const *rmt_command_option;
void sys_reset_uid_gid(void);
size_t rmt_write__(int handle, char *buffer___2, size_t length);
static int from_remote[4][2] = {{-1, -1}, {-1, -1}, {-1, -1}, {-1, -1}};
static int to_remote[4][2] = {{-1, -1}, {-1, -1}, {-1, -1}, {-1, -1}};
static void _rmt_shutdown(int handle, int errno_value) {
  int *tmp;

   
     
                                    
                                  
                                  
                                
                               
                         
     
           
   
}
static int do_command(int handle, char const *buffer___2) {
  size_t length;
  size_t tmp;
  void (*pipe_handler)();
  void (*tmp___0)(int);
  ssize_t written;
  size_t tmp___1;

  {
     
                               
                   
                                             
                                         
               
                                                                             
                                 
                                              
     
                                    
                 
     
     
                               
     
    return (-1);
  }
}
static char *get_status_string(int handle, char *command_buffer) {
  char *cursor;
  int counter;
  size_t tmp;
  char character;
  size_t tmp___0;
  int *tmp___1;
  int *tmp___2;

  {
                
                            
     
                 
                                      
                              
                           
         
         
	                                                                     
	 
                         
           
                                     
           
                             
         
                                 
                                 
                           
         
                  
                 
       
                                 
     
                        
       
                                 
       
                         
     
                            
    {
                {
                                         ;
                       
                               
         
                                 
                               
         
        cursor++;
      }
    while_break___0: /* CIL Label */;
    }
                             
                  
           {
                              {
                              
       
                   
                                            
           
                                                                             
                                           
           
                                  
                                 
           
                                     
                                 
           
         
                                       
       
         
                                       
                                                      
         
                                 
           

                                         
                                            
           

         
        return ((char *)0);
      }
    }
                             
       
                                 
       
                         
     
                        
  }
}
static long get_status(int handle) {
  char command_buffer[64];
  char const *status;
  char *tmp;
  long result;
  long tmp___0;
  int *tmp___1;
                   

  {
     
                                                      
                                 
     
                 
       
                               
                         
       
                         
                        
       
       
                                     
                     
       
     
    return (-1L);
  }
}
static off_t get_status_off(int handle) {
  char command_buffer[64];
  char const *status;
  char *tmp;
  off_t count;
  int negative;
  int digit;
  char const *tmp___0;
  off_t c10;
  off_t nc;
  int tmp___1;
  int tmp___2;
                    

  {
     
                                                      
                                 
     
                 {
      return ((off_t)-1);
    }       
                       
       
                   
                                        
                                              
                                               
                               
             
           
                      
               
                   
         
                                   
       
                                            
                     
                    
              
                                         
                      
                
                      
         
       
                        
       
                   
                                            
                           
                   
                                                    
                                         
                                 
                  
                              
                           
                                      
                    
                                      
             
                                     
                                 
                    
                             
                                   
                      
                                   
               
                            
                                   
               
             
                       
           
         
                                       
       
                     
     
  }
}
static void encode_oflag(char *buf, int oflag) {
                   
                   
                   
                   
                   
                   
                   
                    
                    
                    
                    
                    
                    

   

     
                                            
                                                             
     
     
                             
                    
       
                             
                    
       
                             
                    
       
                          
                            
     
                                           
                                                          
     
                        
                            
     
                                           
                                                        
     
                        
                            
     
                                           
                                                          
     
                        
                                    
     
              
     
                                  
     
                       
       

                                             
                                                             
       

     
                     
       

                                             
                                                            
       

     
                       
       

                                             
                                                            
       

     
                      
       

                                             
                                                           
       

     
                      
       

                                             
                                                             
       

     
                       
       

                                             
                                                               
       

     
                          
       

                                             
                                                            
       

     
                          
       

                                             
                                                           
       

     
                      
       

                                             
                                                            
       

     
           
   

}
int rmt_open__(char const *path, int open_mode, int bias,
               char const *remote_shell) {
  int remote_pipe_number;
  char *path_copy;
  char *remote_host;
  char *remote_file;
  char *remote_user;
  int *tmp;
  char *cursor;
  int *tmp___0;
  char const *remote_shell_basename;
  pid_t status;
  char *tmp___1;
  int e;
  int *tmp___2;
  int *tmp___3;
  int tmp___4;
  int tmp___5;
  int e___0;
  int *tmp___6;
  int *tmp___7;
  char *tmp___8;
  int *tmp___9;
  size_t remote_file_len;
  size_t tmp___10;
  char *command_buffer;
  void *tmp___11;
  int e___1;
  int *tmp___12;
  int tmp___13;
  long tmp___14;
  char const *tmp___15;
  char const *tmp___16;
                    
                    
                    
                    
                    
                    

  {
                           
    {
                {
                                     ;
                                        
                           
         
                                                       
                                                       
                             
           
         
                             
      }
    while_break: /* CIL Label */;
    }
                                 {
       
                                 
                  
       
      return (-1);
    }
     
                                
                              
                              
                              
                         
     
     
                 
                                          
                       
                               
         
         
                                   
                         
           
                                   
                         
           
                                   
                         
           
                              
                                       
                            
                                 
         
                                  
                                       
                       
         
                      
                                
                             
                                      
                                   
                                     
           
                            
                                
                             
                                   
                                     
           
                            
                                      
         
                 
       
                                     
     
                      
                                   
                                
       
     
                        
                                    
     
     
                                        
                                                    
                                                    
     
                        
              
            
       
	                                                
       
                          
            
                                     
                     
                                
                                     
                     
       
                    
       
     
     
                      
     
                       
       
                                     
                         
                                
                                     
                         
       
                  
     
                      
       
                 
                                              
                                                
                                                
                 
                                                
                                                  
                                                  
                            
       
                        
                                 
                                        
                
                                
         
         
                                                                       
                                                  
         
              
                                 
                                        
                
                                
         
         
                                                                           
                           
         
       
       
                                                         
                                     
                                                    
       
     
     
                                                
                                              
                                                   
                                 
                                                   
                                        
                                                       
                                                                     
                                                                      
                                                      
                                                    
                                                                              
     
                         
                  
            
       
	                                          
       
                            
                
                                      
                          
                                     
                                
                                                 
       
                    
       
     
     
                                   
                              
     
                                       
  }
}
int rmt_close__(int handle) {
  long status;
  int tmp;
  int *tmp___0;
                   

  {
     
                                      
     
                    
                  
     
    {
      status = get_status(handle);
                                   
                                      
    }
    return ((int)status);
  }
}
size_t rmt_read__(int handle, char *buffer___2, size_t length) {
  char command_buffer[64];
  size_t status;
  size_t rlen;
  size_t counter;
  int tmp;
  long tmp___0;
                    
                    

  {
    {
                                                         
                                                                 
      tmp = do_command(handle, (char const *)(command_buffer));
    }
                   {
      return ((size_t)-1);
    }       
       
                                     
                                 
       
                                           
                            
       
     
                        
     
                 
                                      
                                  
                           
         
         
                                                                      
                                             
         
                                           
           
                                     
           
                              
                
                            
             
                                       
             
                                
           
         
                        
                           
       
                                 
     
                    
  }
}
size_t rmt_write__(int handle, char *buffer___2, size_t length) {
  char command_buffer[64];
  void (*pipe_handler)();
  size_t written;
  int tmp;
  void (*tmp___0)(int);
  long r;
  long tmp___1;
                    
                    

  {
     
                                                         
                                                                 
                                                               
     
                    
                         
     
     
                                             
                                         
               
                                                                             
                                              
     
                           {
      {
        tmp___1 = get_status(handle);
        r = tmp___1;
      }
                  {
        return ((size_t)0);
      }
                                
                        
       
                          
    }
     
                               
     
                     
  }
}
off_t rmt_lseek__(int handle, off_t offset, int whence) {
  char command_buffer[64];
  char operand_buffer[(((sizeof(uintmax_t) * 8UL) * 302UL) / 1000UL + 1UL) +
                      1UL];
  uintmax_t u;
  char *p;
  int tmp;
  off_t tmp___0;
                    
                    
                    

  {
                      
                               
            
                            
     
                                                
        
                 
     
                 
                                      
            
                                          
                  
                          
                           
         
       
                                 
     
                      
          
                     
     
     
                        
                    
       
                        
                    
       
                        
                    
       
                          
                           
                 
                        
                           
                 
                        
                           
                 
                        
                                    
     
              
     
                                  
     
     
                                                         
                                                                       
                                                               
     
                    
                         
     
    {
      tmp___0 = get_status_off(handle);
    }
    return (tmp___0);
  }
}
int rmt_ioctl__(int handle, int operation, char *argument) {
  int *tmp;
  char command_buffer[64];
  char operand_buffer[(((sizeof(uintmax_t) * 8UL) * 302UL) / 1000UL + 1UL) +
                      1UL];
  uintmax_t u;
  char *p;
  int tmp___0;
  long tmp___1;
  ssize_t status;
  size_t counter;
  int tmp___2;
  char copy;
                    
                    
                    
                    

  {
     

                      
                                                                               
                                               
                      
       
                      
                                                                               
                                                
                          
       
                          
                                    
     
                               
                
     
                  
                             
                                                    
                                                              
              
                                                           
       
                                                  
          
                   
       
                   
                                        
              
                                            
                    
                            
                             
           
         
                                   
       
                                                   {
            
                       
      }
      {
                                                           
                                                             
                                                          
        tmp___0 = do_command(handle, (char const *)(command_buffer));
      }
                         {
        return (-1);
      }
       
	                             
       
                            
                                  
     
                                        
     
                          
                    
              
         
	                              
	 
                            
                      
         
       
       
                   
                                            
                               
                                 
           
           
                                                                         
                                                
           
                                                
             
                                       
             
                        
                  
                                 
               
                                         
               
                          
             
           
                                                       
                              
         
                                       
       
                                                       
                   
       
                          
       
                   
                                            
                                            
                                 
           
                                       
                                                                
                                               
                         
         
                                       
       
                 
                                 ;
     

  }
}
/* #pragma merger("0","00c.sparse.o.i","") */
static _Bool tar_sparse_member_p(struct tar_sparse_file *file) {
  _Bool tmp;

  {
                                         
       
                                                        
       
                   
     
    return ((_Bool)0);
  }
}
static _Bool tar_sparse_init(struct tar_sparse_file *file) {
  _Bool tmp;

  {
                                  
                              
       
                                             
       
                   
     
    return ((_Bool)1);
  }
}
static _Bool tar_sparse_done(struct tar_sparse_file *file) {
  _Bool tmp;

  {
                              
       
                                             
       
                   
     
    return ((_Bool)1);
  }
}
static _Bool tar_sparse_scan(struct tar_sparse_file *file,
                             enum sparse_scan_state state, void *block) {
  _Bool tmp;

  {
                                    
       
                                                                 
       
                   
     
    return ((_Bool)1);
  }
}
static _Bool tar_sparse_dump_region(struct tar_sparse_file *file, size_t i) {
  _Bool tmp;

  {
                                     
       
                                                       
       
                   
     
    return ((_Bool)0);
  }
}
static _Bool tar_sparse_extract_region(struct tar_sparse_file *file, size_t i) {
  _Bool tmp;

  {
                                        
       
                                                          
       
                   
     
    return ((_Bool)0);
  }
}
static _Bool tar_sparse_dump_header(struct tar_sparse_file *file) {
  _Bool tmp;

  {
                                     
       
                                                    
       
                   
     
    return ((_Bool)0);
  }
}
static _Bool tar_sparse_decode_header(struct tar_sparse_file *file) {
  _Bool tmp;

  {
                                       
       
                                                      
       
                   
     
    return ((_Bool)1);
  }
}
static _Bool tar_sparse_fixup_header(struct tar_sparse_file *file) {
  _Bool tmp;

  {
                                      
       
                                                     
       
                   
     
    return ((_Bool)1);
  }
}
static _Bool lseek_or_error(struct tar_sparse_file *file, off_t offset,
                            int whence) {
  __off_t tmp;

  {
     
                                            
     
                   
       
                                                                          
                                  
       
                        
     
    return ((_Bool)1);
  }
}
static _Bool zero_block_p(char *buffer___2, size_t size) {
  char *tmp;
  size_t tmp___0;

  {
     
                 
                                      
                       
               
                       
                           
         
                         
                     
                   
                            
         
       
                                 
     
    return ((_Bool)1);
  }
}
static void sparse_add_map(struct tar_sparse_file *file, struct sp_array *sp) {
  void *tmp;
  void *tmp___0;
  size_t tmp___1;

   
                                                       
                                     
       

                                                                           
                                                               
                                                        
       

            
                                                
                                               
         

                                                    
                                                                   
                                                                 
                                                                               
                                                                     
         

       
     
                                                  
                                            
                                                     
           
   
}
static char buffer___1[512];
static _Bool sparse_scan_file(struct tar_sparse_file *file) {
  size_t count;
  size_t offset;
  struct sp_array sp;
  _Bool tmp;
  _Bool tmp___0;
  _Bool tmp___1;
  _Bool tmp___2;
  _Bool tmp___3;
  _Bool tmp___4;

  {
     
                         
                           
                              
                                              
     
               
                        
     
     
                                                   
                                                     
                                                      
                                                                            
     
                   
                        
     
    {
                {
                                     ;
        {
          count = safe_read(file->fd, (void *)(buffer___1), sizeof(buffer___1));
        }
                           
                                                 
                             
           
                
                           
         
         
	                                            
	 
                     {
                           {
            {
                                        
                                      
              tmp___1 =
                  tar_sparse_scan(file, (enum sparse_scan_state)1, (void *)0);
            }
                          {
              return ((_Bool)0);
            }
          }
        }       
                                   
                                      
           
           
                                 
                                                  
                                                                              
                                                                      
                                                            
           
                         
                              
           
         
        {
                          
                                                       
        }
      }
    while_break: /* CIL Label */;
    }
                             
                                
     
     
                                
                                            
                                                                        
                                                                            
     
                     
  }
}
static struct tar_sparse_optab oldgnu_optab;
static struct tar_sparse_optab star_optab;
static struct tar_sparse_optab pax_optab;
static _Bool sparse_select_optab(struct tar_sparse_file *file) {
  unsigned int tmp;

  {
                                             
                                         
            
                                         
     
     
                      
                    
       
                      
                    
       
                      
                    
       
                      
                    
       
                      
                    
       
                      
                    
       
                          
                           
                           
                         
                           
                           
                                   
                        
                           
                               
                        
                           
                                
                        
                                   
                        
                                  
     
    return ((_Bool)1);
  }
}
static _Bool sparse_dump_region(struct tar_sparse_file *file, size_t i) {
  union block *blk;
  off_t bytes_left;
  _Bool tmp;
  size_t bufsize;
  size_t bytes_read;
  off_t tmp___0;

  {
     
                                                                        
           
                                                                               
     
               
                        
     
     
                 
                                      
                                 
                           
         
                                
                               
                
                               
         
         
                                    
                                  
                                                        
                                                                           
         
                                                 
           
                              
                                                                
                                                                              
                                                                         
                                            
                         
           
                            
         
         
                                                                
                                          
                                    
         
       
                                 
     
    return ((_Bool)1);
  }
}
static _Bool sparse_extract_region(struct tar_sparse_file *file, size_t i) {
  size_t write_size;
  _Bool tmp;
  int tmp___0;
  size_t count;
  size_t wrbytes;
  union block *blk;
  union block *tmp___1;
  char *tmp___2;
                    

  {
     
           
                                                                               
     
               
                        
     
                                                               
                            
       
                                         
       
                    
         

                                                                         
         

       
            
       

                   
                                        
                                    
                             
           
                                   
                                  
                  
                                 
           
           
                                        
                          
           
                     
             
                                                             
                                                 
                              
             
                              
           
           
                                      
                                                                               
                                
                                       
           
                                 
             
                                  
                                                                         
                           
             
                              
           
         
                                   
       

     
    return ((_Bool)1);
  }
}
enum dump_status sparse_dump_file(int fd, struct tar_stat_info *st) {
  _Bool rc___1;
  struct tar_sparse_file file;
  _Bool tmp;
  _Bool tmp___0;
  size_t i;
  _Bool tmp___1;
  int tmp___2;

  {
     
                          
                   
                                       
     
              
       
                                         
       
                     
                                     
       
            
                                   
     
     
                                       
     
                 
                                      
         
                                        
         
                      
                        
           
                       
                                            
                           
                                                                
                                   
                 
                      
                                 
               
               
                                                          
                    
               
             
                                       
           
         
       
     
     
                                                                       
                                             
                                       
     
                 {
                  {
        tmp___2 = 0;
      }       
                    
       
    }       
                  
     
    return ((enum dump_status)tmp___2);
  }
}
_Bool sparse_file_p(struct tar_stat_info *st) {

   

    return ((_Bool)(st->stat.st_blocks <
                    st->stat.st_size / 512L +
                        (__off_t)(st->stat.st_size % 512L != 0L)));
   

}
_Bool sparse_member_p(struct tar_stat_info *st) {
  struct tar_sparse_file file;
  _Bool tmp;
  _Bool tmp___0;

  {
     
                                       
     
               
                        
     
    {
                          
      tmp___0 = tar_sparse_member_p(&file);
    }
    return (tmp___0);
  }
}
_Bool sparse_fixup_header(struct tar_stat_info *st) {
  struct tar_sparse_file file;
  _Bool tmp;
  _Bool tmp___0;

  {
     
                                       
     
               
                        
     
    {
                          
      tmp___0 = tar_sparse_fixup_header(&file);
    }
    return (tmp___0);
  }
}
enum dump_status sparse_extract_file(int fd, struct tar_stat_info *st,
                                     off_t *size) {
  _Bool rc___1;
  struct tar_sparse_file file;
  size_t i;
  _Bool tmp;
  _Bool tmp___0;
  _Bool tmp___1;
  int tmp___2;

  {
     
                        
                          
                   
                                       
     
              
       
                                         
       
                     
                                     
       
            
                                   
     
     
                                               
                    
     
     
                 
                                      
                     
                                                          
                             
           
                
                           
         
         
                                                       
              
         
       
                                 
     
     
                                                                   
                                        
                                       
     
                 {
                  {
        tmp___2 = 0;
      }       
                    
       
    }       
                  
     
    return ((enum dump_status)tmp___2);
  }
}
enum dump_status sparse_skip_file(struct tar_stat_info *st) {
  _Bool rc___1;
  struct tar_sparse_file file;
  _Bool tmp;
  _Bool tmp___0;
  _Bool tmp___1;
  int tmp___2;

  {
     
                        
                          
                   
                                       
     
              
       
                                         
       
                     
                                     
       
            
                                   
     
     
                                               
                                                     
                                       
     
                 {
                  {
        tmp___2 = 0;
      }       
                    
       
    }       
                  
     
    return ((enum dump_status)tmp___2);
  }
}
static char diff_buffer___0[512];
static _Bool check_sparse_region(struct tar_sparse_file *file, off_t beg,
                                 off_t end) {
  _Bool tmp;
  size_t bytes_read;
  size_t rdsize;
  char *tmp___0;
  _Bool tmp___1;
                   

  {
     
                                         
     
               
                        
     
     
                 
                                      
                           
                           
         
                                     
                             
                               
         
         
                                                            
                                                                              
         
                                                 
           
                                                                              
                                           
           
                            
         
         
	                                                      
	 
                       
           
                                                                    
                                                                           
           
                            
         
                                                
       
                                 
     
    return ((_Bool)1);
  }
}
static _Bool check_data_region(struct tar_sparse_file *file, size_t i) {
  size_t size_left;
  _Bool tmp;
  size_t bytes_read;
  size_t rdsize;
  union block *blk;
  union block *tmp___0;
  char *tmp___1;
  char *tmp___2;
  int tmp___3;
                    
                    

  {
     
           
                                                                               
     
               
                        
     
                                                              
     
                 
                                      
                                 
                           
         
                                
                               
                
                             
         
         
                                      
                        
         
                   
           
                                                           
                                               
                            
           
                            
         
         
                                    
                                                                              
         
                                                 
           
                              
                                                                
                                                                              
                                                                         
                                   
                        
           
                            
         
         
                                          
                                  
                                                       
                                                                    
         
                      
           
                                                 
                                                                      
           
                            
         
       
                                 
     
    return ((_Bool)1);
  }
}
_Bool sparse_diff_file(int fd, struct tar_stat_info *st) {
  _Bool rc___1;
  struct tar_sparse_file file;
  size_t i;
  off_t offset;
  _Bool tmp;
  _Bool tmp___0;
  _Bool tmp___1;
  _Bool tmp___2;
  int tmp___3;

  {
     
                        
                        
                          
                   
                                       
     
             {
      {
        tmp___0 = tar_sparse_init(&file);
      }
                    {
        return ((_Bool)1);
      }
    }       
                        
     
     
                                               
                    
     
     
                 
                                      
                     
                                                          
                             
           
                
                           
         
         
                                        
                                                                         
         
                      
           
                                                  
           
                        
                        
                  
                        
           
                
                      
         
                                
                                                                             
                                                                       
            
       
                                 
     
                  
       

                                                                       
                                             
       

     
     
                             
     
                    
  }
}
static _Bool oldgnu_sparse_member_p(struct tar_sparse_file *file
                                    __attribute__((__unused__))) {

   

    return ((_Bool)((int)current_header->header.typeflag == 83));
   

}
static enum oldgnu_add_status oldgnu_add_sparse(struct tar_sparse_file *file,
                                                struct sparse *s) {
  struct sp_array sp;

  {
                                   
                                         
     
     
                                                                                
                   
                                                                             
     
                         
                                         
            
                                                   
                                            
                                           
              
                                                        
                                             
         
       
     
     
                                
     
    return ((enum oldgnu_add_status)0);
  }
}
static _Bool oldgnu_fixup_header(struct tar_sparse_file *file) {

   

     
                                                                             
                                                        
                                                                 
                                                          
     
    return ((_Bool)1);
   

}
static enum oldgnu_add_status rc;
static _Bool oldgnu_get_sparse_info(struct tar_sparse_file *file) {
  size_t i;
  union block *h;
  int ext_p;
  char *tmp;
  char *tmp___0;
                   
                   

  {
                       
                                                   
                  
     
                 
                                      
                         
                           
         
         
	                                                        
	 
                                     
                           
         
            
       
                                 
     
                                             
     
                 
                                          
                                     
                       
                                 
           
                
                               
         
         
	                        
	 
                 
           
                                                       
                                           
                            
           
                            
         
         
                                  
                        
         
         
                     
                                              
                           
                                              
                                     
               
                    
                                   
             
             
                                                                    
                  
             
           
                                         
         
                                                 
       
                                     
     
                                 
       
                                                               
                                                                              
                        
       
                        
     
    return ((_Bool)1);
  }
}
static void oldgnu_store_sparse_info(struct tar_sparse_file *file,
                                     size_t *pindex, struct sparse *sp,
                                     size_t sparse_size) {

   

     
                 
                                      
                                                            
                                     
                             
           
                
                           
         
         
                                                                         
                                                       
                                                                            
                                                            
                        
               
                      
         
       
                                 
     
           
   

}
static _Bool oldgnu_dump_header(struct tar_sparse_file *file) {
  off_t block_ordinal;
  off_t tmp;
  union block *blk;
  size_t i;

  {
     
                                    
                          
                                          
                                       
     
                                                    
                                              
     
     
                                                                                
                                                        
                                                                          
                                             
                    
                                                                           
                                     
                                                          
                                                         
     
     
                 
                                      
                                                         
                           
         
         
                                  
                                                        
                                                                                
                                    
         
                                                      
                                                  
                
                           
         
       
                                 
     
    return ((_Bool)1);
  }
}
                                               
                                                    
                                                    
                            
                        
                         
                            
                                                                         
                   
                        
                            
static _Bool star_sparse_member_p(struct tar_sparse_file *file
                                  __attribute__((__unused__))) {

   

    return ((_Bool)((int)current_header->header.typeflag == 83));
   

}
static _Bool star_fixup_header(struct tar_sparse_file *file) {

   

     
                                                                             
                                                        
                                                                  
                                                           
     
    return ((_Bool)1);
   

}
static enum oldgnu_add_status rc___0;
static _Bool star_get_sparse_info(struct tar_sparse_file *file) {
  size_t i;
  union block *h;
  int ext_p;
  char *tmp;
  char *tmp___0;
                   
                   

  {
                       
                                                   
                                                
                                                         
                      
         
                     
                                          
                             
                               
             
             
	                                                                 
	     
                                             
                               
             
                
           
                                     
         
                                                  
              
                  
       
            
                
     
     
                 
                                          
                                         
                       
                                 
           
                
                               
         
         
	                        
	 
                 
           
                                                       
                                           
                            
           
                            
         
         
                                  
                        
         
         
                     
                                              
                           
                                                  
                                     
               
                    
                                   
             
             
                                                                          
                  
             
           
                                         
         
                                                   
       
                                     
     
                                     
       
                                                               
                                                                              
                        
       
                        
     
    return ((_Bool)1);
  }
}
                                             
                                                    
                                                    
                          
                                                    
                       
                          
                                                                         
                   
                                                            
                            
static _Bool pax_sparse_member_p(struct tar_sparse_file *file) {

   

    return ((_Bool)((file->stat_info)->archive_file_size !=
                    (file->stat_info)->stat.st_size));
   

}
static _Bool pax_dump_header(struct tar_sparse_file *file) {
  off_t block_ordinal;
  off_t tmp;
  union block *blk;
  size_t i;
                   
                   
                   
                   

  {
     
                                    
                          
                                      
                                                                              
                                           
                                                                              
                    
     
     
                 
                                      
                                                         
                           
         
         
                                            
                                                                      
                                      
                                              
                                                                      
                                      
              
         
       
                                 
     
     
                                          
                                                                          
                                             
                                                         
     
    return ((_Bool)1);
  }
}
                                            
                                                    
                                                    
                         
                     
                                                    
                                                    
                                                                         
                   
                        
                            
/* #pragma merger("0","00d.system.o.i","") */
extern __attribute__((__nothrow__)) int(
    __attribute__((__nonnull__(1, 2), __leaf__))
    execlp)(char const *__file, char const *__arg, ...);
extern __attribute__((__nothrow__)) int(__attribute__((__leaf__))
                                        setuid)(__uid_t __uid);
extern __attribute__((__nothrow__)) int(__attribute__((__leaf__))
                                        setgid)(__gid_t __gid);
extern __attribute__((__nothrow__)) int(__attribute__((__leaf__))
                                        ftruncate)(int __fd, __off_t __length);
extern __attribute__((__nothrow__)) int(__attribute__((__leaf__))
                                        kill)(__pid_t __pid, int __sig);
extern __pid_t waitpid(__pid_t __pid, int *__stat_loc, int __options);
extern __attribute__((__nothrow__, __noreturn__)) void(__attribute__((__leaf__))
                                                       exit)(int __status);
dev_t ar_dev;
ino_t ar_ino;
void sys_stat_nanoseconds(struct tar_stat_info *st) {

   

                                                             
                                                             
                                                             
           
   

}
static struct stat archive_stat;
_Bool sys_get_archive_stat(void) {
  int tmp;

  {
    {
      tmp = fstat(archive, &archive_stat);
    }
    return ((_Bool)(tmp == 0));
  }
}
_Bool sys_file_is_archive(struct tar_stat_info *p) {
  int tmp;

  {
                {
                                    {
                                      {
          tmp = 1;
        }       
                  
         
      }       
                
       
    }       
              
     
    return ((_Bool)tmp);
  }
}
void sys_save_archive_dev_ino(void) {

   

                                
                                                      
                                     
                                     
              
                          
       
            
                        
     
           
   

}
static char const dev_null[10] = {
    (char const)'/', (char const)'d',   (char const)'e', (char const)'v',
    (char const)'/', (char const)'n',   (char const)'u', (char const)'l',
    (char const)'l', (char const)'\000'};
void sys_detect_dev_null_output(void) {
  struct stat dev_null_stat;
  int tmp;
  int tmp___0;
  int tmp___1;
                   

   
     
                                                        
     
                   
                  
            
                                  
                                                       
           
                                                                      
                                                                              
           
                             
                                                              
                                                                
                            
                      
                            
               
                    
                          
             
                  
                        
           
                
                      
         
              
                    
       
     
                                     
           
   
}
void sys_drain_input_pipe(void) {
  size_t r;
  size_t tmp;
  size_t tmp___0;

   
                                          
                                  
                                                       
                      
                
                                                          
                                  
           
                       
                                            
                                       
                 

                                                                             
                                                
                          
                 

                      
                 

                                                                              
                                                   
                              
                 

               
                             
                                                   
                                   
                 
                      
                                 
               
                                  
             
                                       
           
           
         
       
     
           
   
}
void sys_wait_for_child(pid_t child_pid___0) {
  int wait_status;
  int *tmp;
  __pid_t tmp___0;
  union __anonunion_56 __constr_expr_0;
  char *tmp___1;
  union __anonunion_57 __constr_expr_1;
  char *tmp___2;
  union __anonunion_58 __constr_expr_2;
  union __anonunion_59 __constr_expr_3;
                    
                    

   
                        
       
                   
                                        
           
	                                                      
	   
                                 
                             
           
           
	                             
	   
                          
             
                                                         
             
                             
           
         
                                   
       
                                         
                                                                           
         

                                             
                                                         
                                                                        
                          
         

              
                                           
                                                      
           

                                               
                                                          
                                              
                                                      
                            
           

         
       
     
           
   
}
void sys_spawn_shell(void) {
  pid_t child;
  char const *shell;
  char *tmp;
  int wait_status;
  int *tmp___0;
  __pid_t tmp___1;
                   
                   

   
     
                            
                                
     
                 
                        
     
     
                      
     
                     
       

                                              
                          
       

            
       

                   
                                        
           
	                                              
	   
                                 
                             
           
           
	                                 
	   
                              
             
                                   
             
                             
           
         
                                   
       

     
           
   
}
_Bool sys_compare_uid(struct stat *a, struct stat *b) {

   

    return ((_Bool)(a->st_uid == b->st_uid));
   

}
_Bool sys_compare_gid(struct stat *a, struct stat *b) {

   

    return ((_Bool)(a->st_gid == b->st_gid));
   

}
_Bool sys_compare_links(struct stat *link_data, struct stat *stat_data) {
  int tmp;

  {
                                                {
                                                  {
        tmp = 1;
      }       
                
       
    }       
              
     
    return ((_Bool)tmp);
  }
}
int sys_truncate(int fd) {
  off_t pos;
  __off_t tmp;
  int tmp___0;
  int tmp___1;

  {
    {
      tmp = lseek(fd, (off_t)0, 1);
      pos = tmp;
    }
                  {
      tmp___1 = -1;
    }       
       

                                     
                          
       

     
    return (tmp___1);
  }
}
void sys_reset_uid_gid(void) {
  __uid_t tmp;
  __gid_t tmp___0;

   
     
                     
                  
                         
                      
     
           
   
}
static int is_regular_file(char const *name) {
  struct stat stbuf;
  int *tmp;
  int tmp___0;
                   

  {
    {
      tmp___0 = stat((char const * /* __restrict  */)name,
                     (struct stat * /* __restrict  */)(&stbuf));
    }
                      {
      return ((stbuf.st_mode & 61440U) == 32768U);
    }       
       
	                         
       
                         
     
  }
}
size_t sys_write_archive_buffer(void) {
  size_t tmp;
  size_t tmp___0;
  size_t tmp___1;

  {
                             
       

             
                                                                                
                      
       

           {
       

        tmp___0 = full_write(archive, (void const *)(record_start->buffer),
                             record_size);
        tmp___1 = tmp___0;
       

    }
    return (tmp___1);
  }
}
static void xdup2(int from, int into) {
  int status;
  int tmp;
  int e;
  int *tmp___0;
  char *tmp___1;
  int *tmp___2;
  int e___0;
  int *tmp___3;
  char *tmp___4;
                    
                    

   
                       
       
                          
                     
       
                        
         
                                       
         
                            
           

                                         
                         
                                              
                                               
                         
           

         
       
       
	                   
       
                           
                         
           

                                         
                             
                                            
                                                   
                         
           

         
         
	          
	 
       
       
	             
       
     
           
   
}
pid_t sys_child_open_for_compress(void) {
  int parent_pipe[2];
  int child_pipe[2];
  pid_t grandchild_pid;
  pid_t child_pid___0;
  int wait_status;
  char *tmp;
  int saved_errno;
  int *tmp___0;
  int *tmp___1;
  int tmp___2;
  void *tmp___3;
  int tmp___4;
  char *tmp___5;
  int tmp___7;
  int tmp___8;
  void *tmp___9;
  int tmp___10;
  size_t status;
  char *cursor;
  size_t length;
  size_t size;
  int *tmp___11;
  __pid_t tmp___12;
  union __anonunion_60 __constr_expr_4;
  union __anonunion_61 __constr_expr_5;
  union __anonunion_62 __constr_expr_6;
  union __anonunion_63 __constr_expr_7;
                    
                    
                    
                    

  {
    {
                         
      child_pid___0 = xfork();
    }
                           {
       
                                 
                               
       
      return (child_pid___0);
    }
     
                                   
                                       
                               
                             
                                                       
     
                       
                                
         
                                                              
         
                         
                                         
                                                         
             
                               
                                                               
                                                                            
             
                          
                          
             
                  
                        
           
                
                      
         
              
                
	                                                       
	 
                      
                              
             

                                                              
             

           
           
                                                      
                                                                               
                                                                                
           
                            
             
                                           
                                     
             
                                
               

                                   
               

             
             
                                           
                                     
                                                    
             
           
           
                              
                                                                            
                              
                                                    
           
         
       
     
     
                        
                               
     
                              
       

                                              
                                             
                                
                              
                                                                        
                          
                                                
       

     
     
                              
                            
                                                        
     
                        
                  
            
                                
         
                                                              
         
                         
                                         
                                                         
             
                               
                                                               
                                                                            
             
                          
               

                         
                                                    
                                                                             
                                                                              
                                  
               

                    
               

                                                                            
                                                         
                                  
               

             
                  
             

                       
                                                  
                                                                           
                                                                            
                                
             

           
                
           

                                                      
                                                                               
                                                                                
                              
           

         
              
         

                                                    
                                                                             
                                                                              
                            
         

       
                        
         

                                                
         

       
     
     
                 
                                      
                           
                           
                                      
         
                     
                                              
                                          
                                   
             
             
                                          
                                                          
             
                                                 
               

                                                        
               

             
                                
                                   
             
                             
                             
           
                                         
         
                            
                             
             
                                                                
                                           
                                                  
             
                                        
               

                                                     
               

             
           
                           
         
         
	                                      
	 
                                    
           

                                                 
           

         
       
                                 
     
     
                 
                                          
         
	                                                      
	 
                                
                               
         
         
	                                
	 
                             
           
                                                       
           
                               
         
       
                                     
     
                                       
                                                                         
       

                                           
                                                       
                        
       

            
                                         
                                                    
                                           
                                                         
       
     
     
                        
     
  }
}
pid_t sys_child_open_for_uncompress(void) {
  int parent_pipe[2];
  int child_pipe[2];
  pid_t grandchild_pid;
  pid_t child_pid___0;
  int wait_status;
  char *tmp;
  int tmp___0;
  void *tmp___1;
  int tmp___2;
  char *tmp___3;
  int tmp___5;
  int tmp___6;
  void *tmp___7;
  int tmp___8;
  char *cursor;
  size_t maximum;
  size_t count;
  size_t status;
  size_t tmp___9;
  size_t tmp___10;
  size_t tmp___11;
  int *tmp___12;
  __pid_t tmp___13;
  union __anonunion_64 __constr_expr_8;
  union __anonunion_65 __constr_expr_9;
  union __anonunion_66 __constr_expr_10;
  union __anonunion_67 __constr_expr_11;
                    
                    
                    
                    

  {
    {
                         
      child_pid___0 = xfork();
    }
                           {
       
                                            
                                 
                               
       
      return (child_pid___0);
    }
     
                                   
                                       
                               
                             
                                                       
     
                       
                                
         
                                                              
         
                         
                                         
                                                         
             
                               
                                                               
                                                                            
             
                          
                          
             
                  
                        
           
                
                      
         
              
                
	                                                       
	 
                      
           
                                                        
                                                                     
                                                                         
           
                            
             

                                                    
             

           
           
                              
                                                                            
                                    
                                                    
           
         
       
     
     
                        
                               
     
                              
       

                                              
                                             
                                
                              
                                                                              
                          
                                                
       

     
     
                              
                            
                                                       
     
                       
                  
            
                                
         
                                                              
         
                         
                                         
                                                         
             
                               
                                                               
                                                                            
             
                          
               

                                                            
                                                                         
                                                                             
                                  
               

                    
               

                                                                           
                                                         
                                  
               

             
                  
             

                                                          
                                                                       
                                                                           
                                
             

           
                
           

                                                        
                                                                     
                                                                         
                              
           

         
              
         

                                                      
                                                                   
                                                                       
                            
         

       
     
                      
       

                                              
       

     
     
                 
                                      
         
	                           
	 
                 
                                 
           

                                                                           
                                              
                             
           

                
           

                      
                                                                                
                              
           

         
                                             
           
                                 
           
                          
         
                            
                           
         
                                      
                         
         
                     
                                              
                           
                                   
             
                                  
                              
                    
                                  
             
             
	                                                            
	     
                                    
               

                                                         
               

             
                            
                             
           
                                         
         
       
                                 
     
     
                
     
     
                 
                                          
         
	                                                      
	 
                                
                               
         
         
	                                
	 
                             
           
                                                       
           
                               
         
       
                                     
     
                                        
                                                                          
       

                                           
                                                       
                        
       

            
                                          
                                                     
                                           
                                                         
       
     
     
                        
     
  }
}
/* #pragma merger("0","00e.tar.o.i","") */
extern char *optarg;
extern int optind;
extern int printf(char const *__restrict __format, ...);
extern int puts(char const *__s);
extern int fputs_unlocked(char const *__restrict __s,
                          FILE *__restrict __stream);
extern __attribute__((__nothrow__)) char *(__attribute__((__leaf__))
                                           setlocale)(int __category,
                                                      char const *__locale);
extern
    __attribute__((__nothrow__)) char *(__attribute__((__leaf__))
                                        textdomain)(char const *__domainname);
extern __attribute__((__nothrow__)) char *(__attribute__((
    __leaf__)) bindtextdomain)(char const *__domainname, char const *__dirname);
extern __attribute__((__nothrow__)) int(__attribute__((__leaf__)) getopt_long)(
    int ___argc, char *const *___argv, char const *__shortopts,
    struct option const *__longopts, int *__longind);
char const *simple_backup_suffix;
enum backup_type xget_version(char const *context, char const *version);
struct exclude *new_exclude(void);
void add_exclude(struct exclude *ex, char const *pattern, int options);
int add_exclude_file(void (*add_func)(struct exclude *, char const *, int),
                     struct exclude *ex, char const *filename, int options,
                     char line_end);
struct mode_change *mode_compile(char const *mode_string,
                                 unsigned int masked_ops);
int check_links_option;
int allocated_archive_names;
void update_archive(void);
_Bool get_date(struct timespec *result, char const *p,
               struct timespec const *now);
void prepend_default_options(char const *options, int *pargc, char ***pargv);
static char const *stdin_used_by;
void request_stdin(char const *option) {
  char *tmp;
                   

   
                        
       

                                                                            
                                                              
                 
       

     
                           
           
   
}
static FILE *confirm_file;
static int confirm_file_EOF;
int confirm(char const *message_action, char const *message_name) {
  char const *tmp;
  int reply;
  int tmp___0;
  int tmp___1;
  int character;
  int tmp___2;
                   
                    

  {
                        
                         
                    
              
                            
                                
         
                                                                           
                                                                     
         
                              
             

                                     
             

           
                
           

                                
                                 
           

         
       
     
     
                                
                                                
                                                                              
                              
     
                          {
      tmp___1 = -1;
    }       
       

                                              
                          
       

     
    reply = tmp___1;
                      
     
                 
                                      
                                 
                           
         
                              
           
                                 
                                         
                                    
           
                           
         
         
	                                          
	 
       
                                 
     
                      {
      tmp___2 = 1;
    }       
                        
                    
              
                    
       
     
    return (tmp___2);
  }
}
static struct fmttab const fmttab[6] = {
    {"v7", (enum archive_format)1},
    {"oldgnu", (enum archive_format)2},
    {"ustar", (enum archive_format)3},
    {"posix", (enum archive_format)4},
    {"gnu", (enum archive_format)6},
    {(char const *)((void *)0), (enum archive_format)0}};
static void set_archive_format(char const *name) {
  struct fmttab const *p;
  char *tmp;
  char *tmp___0;
  int tmp___1;
                   

   
               
     
                 
                                      
         
	                                                
	 
                              
                           
         
            
                       
           

                                       
                                                            
                                                    
                     
           

         
       
                                 
     
                                                 
           
   
}
static char const *archive_format_string(enum archive_format fmt) {
  struct fmttab const *p;
                   

  {
               
     
                 
                                      
                       
                           
         
                                                                    
                                         
         
            
       
                                 
     
    return ("unknown?");
  }
}
static void assert_format(unsigned int fmt_mask) {
  char *tmp;
                   

   
                                                                               
       

                                                                            
                                       
                 
       

     
           
   
}
static int show_help;
static int show_version;
static struct option long_options[104] = {
    {"absolute-names", 0, (int *)0, 'P'},
    {"after-date", 1, (int *)0, 'N'},
    {"anchored", 0, (int *)0, 128},
    {"append", 0, (int *)0, 'r'},
    {"atime-preserve", 0, (int *)0, 129},
    {"backup", 2, (int *)0, 130},
    {"block-number", 0, (int *)0, 'R'},
    {"blocking-factor", 1, (int *)0, 'b'},
    {"bzip2", 0, (int *)0, 'j'},
    {"catenate", 0, (int *)0, 'A'},
    {"checkpoint", 0, (int *)0, 131},
    {"check-links", 0, &check_links_option, 1},
    {"compare", 0, (int *)0, 'd'},
    {"compress", 0, (int *)0, 'Z'},
    {"concatenate", 0, (int *)0, 'A'},
    {"confirmation", 0, (int *)0, 'w'},
    {"create", 0, (int *)0, 'c'},
    {"delete", 0, (int *)0, 132},
    {"dereference", 0, (int *)0, 'h'},
    {"diff", 0, (int *)0, 'd'},
    {"directory", 1, (int *)0, 'C'},
    {"exclude", 1, (int *)0, 133},
    {"exclude-from", 1, (int *)0, 'X'},
    {"extract", 0, (int *)0, 'x'},
    {"file", 1, (int *)0, 'f'},
    {"files-from", 1, (int *)0, 'T'},
    {"force-local", 0, (int *)0, 134},
    {"format", 1, (int *)0, 135},
    {"get", 0, (int *)0, 'x'},
    {"group", 1, (int *)0, 136},
    {"gunzip", 0, (int *)0, 'z'},
    {"gzip", 0, (int *)0, 'z'},
    {"help", 0, &show_help, 1},
    {"ignore-case", 0, (int *)0, 137},
    {"ignore-failed-read", 0, (int *)0, 138},
    {"ignore-zeros", 0, (int *)0, 'i'},
    {"incremental", 0, (int *)0, 'G'},
    {"index-file", 1, (int *)0, 139},
    {"info-script", 1, (int *)0, 'F'},
    {"interactive", 0, (int *)0, 'w'},
    {"keep-newer-files", 0, (int *)0, 140},
    {"keep-old-files", 0, (int *)0, 'k'},
    {"label", 1, (int *)0, 'V'},
    {"list", 0, (int *)0, 't'},
    {"listed-incremental", 1, (int *)0, 'g'},
    {"mode", 1, (int *)0, 141},
    {"multi-volume", 0, (int *)0, 'M'},
    {"new-volume-script", 1, (int *)0, 'F'},
    {"newer", 1, (int *)0, 'N'},
    {"newer-mtime", 1, (int *)0, 142},
    {"null", 0, (int *)0, 148},
    {"no-anchored", 0, (int *)0, 143},
    {"no-ignore-case", 0, (int *)0, 144},
    {"no-overwrite-dir", 0, (int *)0, 145},
    {"no-wildcards", 0, (int *)0, 146},
    {"no-wildcards-match-slash", 0, (int *)0, 147},
    {"no-recursion", 0, &recursion_option, 0},
    {"no-same-owner", 0, &same_owner_option, -1},
    {"no-same-permissions", 0, &same_permissions_option, -1},
    {"numeric-owner", 0, (int *)0, 149},
    {"occurrence", 2, (int *)0, 150},
    {"old-archive", 0, (int *)0, 'o'},
    {"one-file-system", 0, (int *)0, 'l'},
    {"overwrite", 0, (int *)0, 151},
    {"owner", 1, (int *)0, 152},
    {"pax-option", 1, (int *)0, 153},
    {"portability", 0, (int *)0, 'o'},
    {"posix", 0, (int *)0, 154},
    {"preserve", 0, (int *)0, 155},
    {"preserve-order", 0, (int *)0, 's'},
    {"preserve-permissions", 0, (int *)0, 'p'},
    {"recursion", 0, &recursion_option, 1 << 3},
    {"recursive-unlink", 0, (int *)0, 157},
    {"read-full-records", 0, (int *)0, 'B'},
    {"record-size", 1, (int *)0, 156},
    {"remove-files", 0, (int *)0, 158},
    {"rmt-command", 1, (int *)0, 159},
    {"rsh-command", 1, (int *)0, 160},
    {"same-order", 0, (int *)0, 's'},
    {"same-owner", 0, &same_owner_option, 1},
    {"same-permissions", 0, (int *)0, 'p'},
    {"show-defaults", 0, (int *)0, 161},
    {"show-omitted-dirs", 0, (int *)0, 162},
    {"sparse", 0, (int *)0, 'S'},
    {"starting-file", 1, (int *)0, 'K'},
    {"strip-path", 1, (int *)0, 163},
    {"suffix", 1, (int *)0, 164},
    {"tape-length", 1, (int *)0, 'L'},
    {"to-stdout", 0, (int *)0, 'O'},
    {"totals", 0, (int *)0, 165},
    {"touch", 0, (int *)0, 'm'},
    {"uncompress", 0, (int *)0, 'Z'},
    {"ungzip", 0, (int *)0, 'z'},
    {"unlink-first", 0, (int *)0, 'U'},
    {"update", 0, (int *)0, 'u'},
    {"utc", 0, (int *)0, 167},
    {"use-compress-program", 1, (int *)0, 166},
    {"verbose", 0, (int *)0, 'v'},
    {"verify", 0, (int *)0, 'W'},
    {"version", 0, &show_version, 1},
    {"volno-file", 1, (int *)0, 168},
    {"wildcards", 0, (int *)0, 169},
    {"wildcards-match-slash", 0, (int *)0, 170},
    {(char const *)0, 0, (int *)0, 0}};
__attribute__((__noreturn__)) void usage(int status);
void usage(int status) {
  char *tmp;
  char *tmp___0;
  char *tmp___1;
  char *tmp___2;
  char *tmp___3;
  char *tmp___4;
  char *tmp___5;
  char *tmp___6;
  char *tmp___7;
  char *tmp___8;
  char *tmp___9;
  char *tmp___10;
  char *tmp___11;
  char *tmp___12;
  char *tmp___13;
  char *tmp___14;
  char const *tmp___15;
  char *tmp___16;
  char *tmp___17;
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    

   
                      
       

                                                                  
                                                  
                                                                   
       

            
       

                          
                                                                              
                                                                              
                                                               
                                                          
                 
                                                                               
                                                                               
                                                                               
                                                                               
                                                               
                                                                     
                                                         
                                                                               
                                                                           
                                                                               
                                                               
                                                          
                          
                                                                         
                                                                              
                                                                                
                                                                            
                                                                                
                                                                            
                                                                               
                                                                         
                                                                      
                         
                                                               
                                                          
                          
                                                                              
                                                                                
                                                                   
                                                                        
                                                                               
                                                                            
                                                                              
                                                                           
                                                                             
                                                                  
                                                                           
                                                                          
                                                                         
                                                                       
                                                                                
                                                                           
                                                                               
                                                                              
                                                                             
                                                                                
                                                                                
                                                                              
                                                                            
                                                                                
                                                             
                                                               
                                                          
                          
                                                                             
                                                                                
                                                                                
                                                                       
                                                                            
                                                                        
                                                                           
                                                                            
                                                                               
                                                                               
                                                                              
                                                            
                                                                                
                                                                                
                                                                                
                        
                                                               
                                                          
                          
                                                                                
                                                                                
                                                                    
                                                                          
                                                                            
                                                                             
                                                                                
                                                                            
                                                                                
                                                                     
                                                                
                                                                         
                      
                                                               
                                                          
                          
                                                                                
                                                                                
                                                                               
                                                                                
                                                            
                                                               
                                                          
                          
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                             
                                                                           
                                                                             
                                                                                
                                             
                                                                                
                                                                                
                                                                            
                                                                                
                                                                             
                                                                             
                                                                            
                                                                             
                                                      
                                                               
                                                          
                          
                                                                                
                                                                         
                                                                               
                                                                
                                                                                
                                                                              
                                                                              
                                                                               
                                                                            
                                                                               
                                                                             
                                                                          
                                                                         
                                                                     
                                                                                
                                                                               
                                                                               
                                                                            
                                                                                
                                                                           
                                                                                
                                                                              
                                                                         
                                                               
                                                          
                                                                             
                                                                                
                                                                          
                                                                                
                                                                
                                                          
                  
                                                                            
                                                                             
                                                                      
                                                                
                                                          
                           
                                                                              
                                                                           
                                                                              
                                                                             
                                                                              
                                                                            
                                                                            
                                                                              
                                                                            
                                                                               
                                                                             
                            
                                                                
                                                          
                           
                                                                             
                                                                                
                                                                   
                                                                
                                                          
                  
                                                                               
                                                                            
                                                                            
                                                                            
                                                                              
                                                                    
                                                                
                                                          
                                                                 
                  
                                                                               
                                                                               
                                                                               
                                                                      
                                                                            
                                                       
                                                                            
       

     
     
                   
     
   
}
static void set_subcommand_option(enum subcommand subcommand) {
  char *tmp;
                   

  {
                                                
                                                                        
         

                                                                                
                                         
                   
         

       
     
    subcommand_option = subcommand;
           
  }
}
static void set_use_compress_program_option(char const *string) {
  char *tmp;
  int tmp___0;
                   

   
                                      
       
                                                              
       
                         
         

                                                           
                                         
                   
         

       
     
                                         
           
   
}
static void decode_options(int argc, char **argv) {
  int optchar;
  int input_files;
  char const *textual_date_option;
  char const *backup_suffix_string;
  char const *version_control_string;
  int exclude_options;
  _Bool o_option;
  int pax_option;
  char *tmp;
  int new_argc;
  char **new_argv;
  char *const *in;
  char **out;
  char const *letter;
  char buffer___2[3];
  char const *cursor;
  size_t tmp___0;
  void *tmp___1;
  char **tmp___2;
  char *const *tmp___3;
  char *const *tmp___4;
  char **tmp___5;
  char *tmp___6;
  char **tmp___7;
  char *const *tmp___8;
  char *tmp___9;
  char **tmp___10;
  char *const *tmp___11;
  char *tmp___12;
  uintmax_t u;
  char *tmp___13;
  char *tmp___14;
  strtol_error tmp___15;
  void *tmp___16;
  int tmp___17;
  char *tmp___18;
  uintmax_t u___0;
  char *tmp___19;
  char *tmp___20;
  strtol_error tmp___21;
  char *tmp___22;
  struct stat st;
  char *tmp___23;
  int tmp___24;
  char const *tmp___25;
  char const *tmp___26;
  char *tmp___27;
  _Bool tmp___28;
  int e;
  int *tmp___29;
  char *tmp___30;
  int tmp___31;
  char *tmp___32;
  uintmax_t g;
  char *tmp___33;
  char *tmp___34;
  strtol_error tmp___35;
  size_t tmp___36;
  int tmp___37;
  char *tmp___38;
  uintmax_t u___1;
  char *tmp___39;
  char *tmp___40;
  strtol_error tmp___41;
  uintmax_t u___2;
  char *tmp___42;
  char *tmp___43;
  strtol_error tmp___44;
  size_t tmp___45;
  int tmp___46;
  uintmax_t u___3;
  char *tmp___47;
  char *tmp___48;
  strtol_error tmp___49;
  char *tmp___50;
  char const *tmp___51;
  uintmax_t u___4;
  char *tmp___52;
  char *tmp___53;
  strtol_error tmp___54;
  char *tmp___55;
  char *tmp___56;
  char *tmp___57;
  char *tmp___58;
  char *tmp___59;
  char *tmp___60;
  char *tmp___61;
  char *tmp___62;
  size_t volume_label_max_len;
  char *tmp___63;
  char *tmp___64;
  size_t tmp___65;
  char *tmp___66;
  char *tmp___67;
  char *tmp___68;
  char *tmp___69;
  char *tmp___70;
  char *tmp___71;
  int tmp___72;
  char *tmp___73;
  int tmp___74;
  char *tmp___75;
  char const *treated_as;
  char const *tmp___76;
  char *tmp___77;
  int tmp___78;
  unsigned long tmp___79;
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     

  {
    {
                                            
                                               
                                
                          
                     
                                             
                                              
      blocking_factor = 20;
      record_size = (size_t)10240;
      excluded = new_exclude();
                                                                      
                                                         
                                
                               
                               
                                           
                                               
    }
                   
                                          
         
                                    
                                       
                                                      
                                                         
                                                                            
                                      
                                   
                         
                        
                
                       
               
                                      
                       
               
                                          
         
         
                     
                                          
                           
                               
             
             
                                            
                            
                    
                                                             
                                                                              
                                                   
                                             
                                             
             
                         
                                                     
                                                                       
                                
                        
                               
                       
                                              
                        
                   

                                                                                
                                                                             
                             
                   

                 
               
             
                     
           
                                     
         
         
                     
                                              
                                                                      
                                   
             
                           
                  
                          
                 
                                          
           
                                         
         
                         
                        
                        
       
     
     
                      
                                       
                                                                    
     
    {
      while (1) {
                                         ;
        {
          optchar = getopt_long(
              argc, (char *const *)argv,
              "-01234567ABC:F:GIK:L:MN:OPRST:UV:WX:Zb:cdf:g:hijklmoprstuvwxyz",
              (struct option const *)(long_options), (int *)0);
        }
        if (!(optchar != -1)) {
          goto while_break___1;
        }
        {
          if (optchar == 63) {
            goto case_63;
          }
          if (optchar == 0) {
            goto case_0;
          }
          if (optchar == 1) {
            goto case_1;
          }
          if (optchar == 65) {
            goto case_65;
          }
          if (optchar == 98) {
            goto case_98;
          }
          if (optchar == 66) {
            goto case_66;
          }
          if (optchar == 99) {
            goto case_99;
          }
          if (optchar == 67) {
            goto case_67;
          }
          if (optchar == 100) {
            goto case_100;
          }
          if (optchar == 102) {
            goto case_102;
          }
          if (optchar == 70) {
            goto case_70;
          }
          if (optchar == 103) {
            goto case_103;
          }
          if (optchar == 71) {
            goto case_71;
          }
          if (optchar == 104) {
            goto case_104;
          }
                               
                          
           
                              
                         
           
                               
                          
           
                               
                          
           
                              
                         
           
                               
                          
           
                              
                         
           
                               
                          
           
                              
                         
           
                              
                         
           
                               
                          
           
                               
                          
           
                              
                         
           
                               
                          
           
          if (optchar == 80) {
            goto case_80;
          }
          if (optchar == 114) {
            goto case_114;
          }
          if (optchar == 82) {
            goto case_82;
          }
          if (optchar == 115) {
            goto case_115;
          }
          if (optchar == 83) {
            goto case_83;
          }
          if (optchar == 116) {
            goto case_116;
          }
          if (optchar == 84) {
            goto case_84;
          }
          if (optchar == 117) {
            goto case_117;
          }
          if (optchar == 85) {
            goto case_85;
          }
          if (optchar == 167) {
            goto case_167;
          }
          if (optchar == 118) {
            goto case_118;
          }
          if (optchar == 86) {
            goto case_86;
          }
          if (optchar == 119) {
            goto case_119;
          }
          if (optchar == 87) {
            goto case_87;
          }
          if (optchar == 120) {
            goto case_120;
          }
          if (optchar == 88) {
            goto case_88;
          }
          if (optchar == 121) {
            goto case_121;
          }
          if (optchar == 122) {
            goto case_122;
          }
          if (optchar == 90) {
            goto case_90;
          }
          if (optchar == 128) {
            goto case_128;
          }
          if (optchar == 129) {
            goto case_129;
          }
          if (optchar == 131) {
            goto case_131;
          }
          if (optchar == 130) {
            goto case_130;
          }
          if (optchar == 132) {
            goto case_132;
          }
          if (optchar == 133) {
            goto case_133;
          }
          if (optchar == 134) {
            goto case_134;
          }
          if (optchar == 135) {
            goto case_135;
          }
          if (optchar == 139) {
            goto case_139;
          }
          if (optchar == 137) {
            goto case_137;
          }
                               
                          
           
                               
                          
           
                               
                          
           
                               
                          
           
                               
                          
           
                               
                          
           
                               
                          
           
                               
                          
           
                               
                          
           
                               
                          
           
                               
                          
           
                               
                          
           
                               
                          
           
                               
                          
           
                               
                          
           
                               
                          
           
                               
                          
           
                               
                          
           
                               
                          
           
          if (optchar == 158) {
            goto case_158;
          }
          if (optchar == 159) {
            goto case_159;
          }
          if (optchar == 160) {
            goto case_160;
          }
                               
                          
           
                               
                          
           
                               
                          
           
                               
                          
           
                               
                          
           
                               
                          
           
                               
                          
           
                               
                          
           
                              
                         
           
                              
                         
           
                              
                         
           
                              
                         
           
                              
                         
           
                              
                         
           
                              
                         
           
          if (optchar == 55) {
            goto case_48;
          }
          goto switch_break;
        case_63 : /* CIL Label */
        {
          usage(2);
        }
        case_0: /* CIL Label */
          goto switch_break;
        case_1 : /* CIL Label */
        {
          name_add((char const *)optarg);
          input_files++;
        }
          goto switch_break;
        case_65 : /* CIL Label */
        {
          set_subcommand_option((enum subcommand)2);
        }
          goto switch_break;
        case_98 : /* CIL Label */
        {
          tmp___15 = xstrtoumax((char const *)optarg, (char **)0, 10, &u, "");
        }
          if ((unsigned int)tmp___15 == 0U) {
            blocking_factor = (int)u;
            if (u == (uintmax_t)blocking_factor) {
              if (0 < blocking_factor) {
                record_size = u * 512UL;
                if (!(u == record_size / 512UL)) {
                   

                    tmp___13 = gettext("Invalid blocking factor");
                    tmp___14 = quotearg_colon((char const *)optarg);
                    error(0, 0, "%s: %s", tmp___14, tmp___13);
                    usage(2);
                   

                }
              } else {
                 

                  tmp___13 = gettext("Invalid blocking factor");
                  tmp___14 = quotearg_colon((char const *)optarg);
                  error(0, 0, "%s: %s", tmp___14, tmp___13);
                  usage(2);
                 

              }
            } else {
               

                tmp___13 = gettext("Invalid blocking factor");
                tmp___14 = quotearg_colon((char const *)optarg);
                error(0, 0, "%s: %s", tmp___14, tmp___13);
                usage(2);
               

            }
          } else {
             

              tmp___13 = gettext("Invalid blocking factor");
              tmp___14 = quotearg_colon((char const *)optarg);
              error(0, 0, "%s: %s", tmp___14, tmp___13);
              usage(2);
             

          }
          goto switch_break;
        case_66: /* CIL Label */
          read_full_records_option = (_Bool)1;
          goto switch_break;
        case_99 : /* CIL Label */
        {
          set_subcommand_option((enum subcommand)3);
        }
          goto switch_break;
        case_67 : /* CIL Label */
        {
          name_add("-C");
          name_add((char const *)optarg);
        }
          goto switch_break;
        case_100 : /* CIL Label */
        {
          set_subcommand_option((enum subcommand)5);
        }
          goto switch_break;
        case_102: /* CIL Label */
          if (archive_names == allocated_archive_names) {
             

              allocated_archive_names *= 2;
              tmp___16 = xrealloc((void *)archive_name_array,
                                  sizeof(char const *) *
                                      (unsigned long)allocated_archive_names);
              archive_name_array = (char const **)tmp___16;
             

          }
          tmp___17 = archive_names;
          archive_names++;
          *(archive_name_array + tmp___17) = (char const *)optarg;
          goto switch_break;
        case_70: /* CIL Label */
          info_script_option = (char const *)optarg;
          multi_volume_option = (_Bool)1;
          goto switch_break;
        case_103: /* CIL Label */
          listed_incremental_option = (char const *)optarg;
          after_date_option = 1;
        case_71: /* CIL Label */
          incremental_option = (_Bool)1;
          goto switch_break;
        case_104: /* CIL Label */
          dereference_option = (_Bool)1;
          goto switch_break;
        case_105: /* CIL Label */
          ignore_zeros_option = (_Bool)1;
          goto switch_break;
                                 
         
                                                                                
                                                    
                                              
                   
         
                            
                                  
         
                                                   
         
                            
                                 
                                               
                            
                                 
         
                                          
                                           
         
                            
                                 
                                            
                            
                                 
         
                    
                                                                           
         
                                             
             

                                                        
                                                              
                                                        
                       
             

           
                                                              
                                         
                            
                                 
                                  
                            
                                
                                         
                            
                                
                                
                                 
                                                 
             

                                                                 
                                                  
                       
             

           
                                   
                        
                  
                                     
                                    
             
                        
                                                                            
             
                                  
                 

                                                   
                                                            
                                                      
                           
                 

               
                                                            
                                                              
                    
               
                                                                              
                                                                          
               
                             
                                                           
                      
                 

                                                         
                                                                
                            
                                                                            
                                                                          
                                                                    
                 

               
             
           
                            
                                 
                              
                            
                                
                                      
                            
        case_112: /* CIL Label */
          same_permissions_option = 1;
          goto switch_break;
        case_80: /* CIL Label */
          absolute_names_option = (_Bool)1;
          goto switch_break;
        case_114 : /* CIL Label */
        {
          set_subcommand_option((enum subcommand)1);
        }
          goto switch_break;
        case_82: /* CIL Label */
          block_number_option = (_Bool)1;
          goto switch_break;
        case_115: /* CIL Label */
          same_order_option = (_Bool)1;
          goto switch_break;
        case_83: /* CIL Label */
          sparse_option = (_Bool)1;
          goto switch_break;
        case_116 : /* CIL Label */
        {
          set_subcommand_option((enum subcommand)7);
          verbose_option++;
        }
          goto switch_break;
        case_84: /* CIL Label */
          files_from_option = (char const *)optarg;
          goto switch_break;
        case_117 : /* CIL Label */
        {
          set_subcommand_option((enum subcommand)8);
        }
          goto switch_break;
        case_85: /* CIL Label */
          old_files_option = (enum old_files)3;
          goto switch_break;
        case_167: /* CIL Label */
          utc_option = (_Bool)1;
          goto switch_break;
        case_118: /* CIL Label */
          verbose_option++;
          goto switch_break;
        case_86: /* CIL Label */
          volume_label_option = (char const *)optarg;
          goto switch_break;
        case_119: /* CIL Label */
          interactive_option = (_Bool)1;
          goto switch_break;
        case_87: /* CIL Label */
          verify_option = (_Bool)1;
          goto switch_break;
        case_120 : /* CIL Label */
        {
          set_subcommand_option((enum subcommand)6);
        }
          goto switch_break;
        case_88 : /* CIL Label */
        {
          tmp___31 =
              add_exclude_file(&add_exclude, excluded, (char const *)optarg,
                               exclude_options | recursion_option, (char)'\n');
        }
          if (tmp___31 != 0) {
             

              tmp___29 = __errno_location();
              e = *tmp___29;
              tmp___30 = quotearg_colon((char const *)optarg);
              error(0, e, "%s", tmp___30);
              fatal_exit();
             

          }
          goto switch_break;
        case_121 : /* CIL Label */
        {
          tmp___32 = gettext(
              "Warning: the -y option is not supported; perhaps you meant -j?");
          error(0, 0, (char const *)tmp___32);
          usage(2);
        }
          goto switch_break;
        case_122 : /* CIL Label */
        {
          set_use_compress_program_option("gzip");
        }
          goto switch_break;
        case_90 : /* CIL Label */
        {
          set_use_compress_program_option("compress");
        }
          goto switch_break;
        case_128: /* CIL Label */
          exclude_options |= 1 << 30;
          goto switch_break;
        case_129: /* CIL Label */
          atime_preserve_option = (_Bool)1;
          goto switch_break;
        case_131: /* CIL Label */
          checkpoint_option = (_Bool)1;
          goto switch_break;
        case_130: /* CIL Label */
          backup_option = (_Bool)1;
          if (optarg) {
            version_control_string = (char const *)optarg;
          }
          goto switch_break;
        case_132 : /* CIL Label */
        {
          set_subcommand_option((enum subcommand)4);
        }
          goto switch_break;
        case_133 : /* CIL Label */
        {
          add_exclude(excluded, (char const *)optarg,
                      exclude_options | recursion_option);
        }
          goto switch_break;
        case_134: /* CIL Label */
          force_local_option = (_Bool)1;
          goto switch_break;
        case_135 : /* CIL Label */
        {
          set_archive_format((char const *)optarg);
        }
          goto switch_break;
        case_139: /* CIL Label */
          index_file_name = (char const *)optarg;
          goto switch_break;
        case_137: /* CIL Label */
          exclude_options |= 1 << 4;
          goto switch_break;
                                 
                                               
                            
                                 
                                               
                            
                                  
         
                                                  
         
                                
             
                                                                           
             
                            
                      
             
                  
                
                                                                                
           
                                               
                                               
                                        
                      
                 

                                                          
                                                                  
                                                            
                               
                 

               
                    
               

                                                        
                                                                
                                                          
                             
               

             
           
                            
                                  
         
                                                               
         
                                           
                                                         
             

                                                                 
                                                  
                           
             

           
                                           
                                                         
             

                           
             

           
                            
                                 
                                        
                            
                                 
                                       
                            
                                 
                                               
                            
                                 
                                        
                            
                                 
                               
                            
                                 
                                             
                            
                                 
                                          
                            
                                 
                        
                                             
                  
             
                        
                                                                               
             
                                               
                                        
                    
               

                                                     
                                                                
                                                          
                             
               

             
           
                            
                                 
                                               
                            
                                  
         
                                                  
         
                                
             
                                                                           
             
                            
                          
             
                  
                    
                      
                                                                             
           
                                               
                                                       
                                            
                      
                 

                                                      
                                                                  
                                                            
                               
                 

               
                    
               

                                                    
                                                                
                                                          
                             
               

             
           
                            
                                  
         
                       
                                     
         
                            
                                  
         
                                      
         
                            
                                 
                                      
                                       
                            
                                  
         
                    
                                                                           
         
                                             
                                    
               

                                                          
                                                                
                                                          
                         
               

             
                  
             

                                                        
                                                              
                                                        
                       
             

           
                              
                                           
             

                                                                          
                                                       
                       
             

           
                                                       
                            
        case_157: /* CIL Label */
          recursive_unlink_option = (_Bool)1;
          goto switch_break;
        case_158: /* CIL Label */
          remove_files_option = (_Bool)1;
          goto switch_break;
        case_159: /* CIL Label */
          rmt_command_option = (char const *)optarg;
          goto switch_break;
        case_160: /* CIL Label */
          rsh_command_option = (char const *)optarg;
          goto switch_break;
        case_161 : /* CIL Label */
        {
          tmp___51 = archive_format_string((enum archive_format)6);
          printf((char const * /* __restrict  */) "--format=%s -f%s -b%d\n",
                 tmp___51, "-", 20);
          exit(0);
        }
        case_163 : /* CIL Label */
        {
          tmp___54 =
              xstrtoumax((char const *)optarg, (char **)0, 10, &u___4, "");
        }
          if ((unsigned int)tmp___54 == 0U) {
            if (!(u___4 == u___4)) {
               

                tmp___52 = gettext("Invalid number of elements");
                tmp___53 = quotearg_colon((char const *)optarg);
                error(0, 0, "%s: %s", tmp___53, tmp___52);
                usage(2);
               

            }
          } else {
             

              tmp___52 = gettext("Invalid number of elements");
              tmp___53 = quotearg_colon((char const *)optarg);
              error(0, 0, "%s: %s", tmp___53, tmp___52);
              usage(2);
             

          }
          strip_path_elements = u___4;
          goto switch_break;
        case_164: /* CIL Label */
          backup_option = (_Bool)1;
                                                      
                            
                                 
                                   
                            
                                  
         
                                                                
         
                            
                                 
                                                   
                            
                                 
                                     
                            
                                 
                                
                            
        case_48:  /* CIL Label */
                                 
                                  
                                  
                                  
                                  
                                  
                                  
         {
          tmp___55 =
              gettext("Options `-[0-7][lmh]\' not supported by *this* tar");
          error(0, 0, (char const *)tmp___55);
          usage(2);
        }
        switch_break: /* CIL Label */;
        }
      }
    while_break___1: /* CIL Label */;
    }
                   
                                                  
         

                                   
         

              
                              
       
     
     
                 
                                          
                               
                               
         
         
                                                   
                        
                   
         
       
                                     
     
                       
       

                                                                               
                                                                            
                           
                                                                              
                                                                               
                                                                        
                                     
                                                                        
                                     
                
       

     
                    
       

                 
       

     
                                             
                       
                                                
              
                                                
       
     
                              
                                                  
         

                                                             
         

       
     
                             
       

                                                           
       

            
                                
         

                                                             
         

       
     
                        
       

                                                                        
       

     
                            
                         
                                 
           

                      
                                                                           
                                                
                     
           

         
       
                                                  
                                                    
                                                      
                                                        
               

                                                                        
                                                               
                                                    
                         
               

             
           
         
       
     
                             
       
                          
                                  
                                                           
       
                                       
                                        
       
     
                            
                                 
         

                                                                            
                                              
                   
         

       
     
                                    
                                             
         

                    
                                                                          
                                              
                   
         

       
     
                              
                                
                  
                                         
                                                                            
                
              
                       
       
       
                              
                                                                   
                                               
       
                                            
         

                                                         
                    
                                                                          
                                                                           
                                             
                                                                              
                   
         

       
     
                        
                                
         

                                                                    
                                              
                   
         

       
                                        
         

                                                                  
                                              
                   
         

       
     
                                      
                                
         

                                                                            
                                              
                   
         

       
                                                  
         

                                                                  
                                              
                   
         

       
     
                     
                                               
                                                    
           

                      
                                                                           
                                                
                     
           

                
                                                      
             

                        
                                                                             
                                                  
                       
             

                  
                                                        
               

                          
                                                                               
                                                    
                         
               

             
           
         
       
     
                                  
                                           
     
                     
                         
     
                              
                                                    
     
     
                                                  
                    
       
                                                  
                    
       
                                                  
                    
       
                                                  
                    
       
                                                  
                    
       
                                                  
                    
       
                                                  
                    
       
                          
                           
                             
                                 
           

                                                                               
                                                
                     
           

         
       
                            
                           
                           
                            
                                                
       
                   
                                            
                                                    
                                                                       
                                 
           
           
	                                                 
	   
                          
             

                                  
             

           
                                
         
                                       
       
                            
                               
                               
                                
                                                
       
                   
                                            
                                                    
                                                                       
                                 
           
           
	                                                 
	   
                          
             

                        
                                                                           
                                                  
                       
             

           
                                
         
                                       
       
                                   
                            
                                      
     
                                             
                               
       

                                                 
                                                      
       

     
                        
       

                                                                       
       

     
                         
                                
         
                                                        
                                
                                                             
         
                            
           

                                                                              
                                                                                
                                              
           

         
       
     
           
  }
}
int main(int argc, char **argv) {
  int tmp;
  void *tmp___0;
  char *tmp___1;
  char *tmp___2;
  int tmp___3;
  int tmp___4;
  char *tmp___5;
  int tmp___6;
  int tmp___7;
                    
                    
                    
                    

  {
    {
      tmp = clock_gettime(0, &start_timespec);
    }
    if (tmp != 0) {
       

        start_timespec.tv_sec = time((time_t *)0);
       

    }
    {
      program_name = (char const *)*(argv + 0);
      setlocale(6, "");
      bindtextdomain("tar", "/usr/local/share/locale");
      textdomain("tar");
      exit_status = 0;
      filename_terminator = (char)'\n';
      set_quoting_style((struct quoting_options *)0, (enum quoting_style)4);
      allocated_archive_names = 10;
      tmp___0 = xmalloc(sizeof(char const *) *
                        (unsigned long)allocated_archive_names);
      archive_name_array = (char const **)tmp___0;
      archive_names = 0;
      signal(17, (void (*)(int))0);
      init_names();
      decode_options(argc, argv);
      name_init();
    }
    if (volno_file_option) {
       

        init_volume_number();
       

    }
    {
      if ((unsigned int)subcommand_option == 0U) {
        goto case_0;
      }
      if ((unsigned int)subcommand_option == 2U) {
        goto case_2;
      }
      if ((unsigned int)subcommand_option == 8U) {
        goto case_2;
      }
      if ((unsigned int)subcommand_option == 1U) {
        goto case_2;
      }
      if ((unsigned int)subcommand_option == 4U) {
        goto case_4;
      }
      if ((unsigned int)subcommand_option == 3U) {
        goto case_3;
      }
      if ((unsigned int)subcommand_option == 6U) {
        goto case_6;
      }
      if ((unsigned int)subcommand_option == 7U) {
        goto case_7;
      }
      if ((unsigned int)subcommand_option == 5U) {
        goto case_5;
      }
      goto switch_break;
    case_0 : /* CIL Label */
    {
      tmp___1 = gettext("You must specify one of the `-Acdtrux\' options");
      error(0, 0, (char const *)tmp___1);
      usage(2);
    }
    case_2:  /* CIL Label */
                            
                             
     {
      update_archive();
    }
      goto switch_break;
    case_4 : /* CIL Label */
    {
      delete_archive_members();
    }
      goto switch_break;
    case_3 : /* CIL Label */
    {
      create_archive();
      name_close();
    }
      if (totals_option) {
         

          print_total_written();
         

      }
      goto switch_break;
    case_6 : /* CIL Label */
    {
      extr_init();
      read_and(&extract_archive);
      extract_finish();
    }
      goto switch_break;
    case_7 : /* CIL Label */
    {
      read_and(&list_archive);
    }
      goto switch_break;
    case_5 : /* CIL Label */
    {
      diff_init();
      read_and(&diff_archive);
    }
      goto switch_break;
    switch_break: /* CIL Label */;
    }
    if (check_links_option) {
       

        check_links();
       

    }
    if (volno_file_option) {
       

        closeout_volume_number();
       

    }
    {
      free((void *)archive_name_array);
      name_term();
    }
    if ((unsigned long)stdlis != (unsigned long)stderr) {
      {
        tmp___3 = ferror_unlocked(stdlis);
      }
      if (tmp___3) {
         

          tmp___2 = gettext("Error in writing to standard output");
          error(0, 0, (char const *)tmp___2);
          fatal_exit();
         

      } else {
        {
	  tmp___4 = fclose(stdlis);
	}
        if (tmp___4 != 0) {
           

            tmp___2 = gettext("Error in writing to standard output");
            error(0, 0, (char const *)tmp___2);
            fatal_exit();
           

        }
      }
    }
    if (exit_status == 2) {
       

        tmp___5 = gettext("Error exit delayed from previous errors");
        error(0, 0, (char const *)tmp___5);
       

    }
    {
      tmp___6 = ferror_unlocked(stderr);
    }
    if (tmp___6) {
      exit_status = 2;
    } else {
      {
	tmp___7 = fclose(stderr);
      }
      if (tmp___7 != 0) {
        exit_status = 2;
      }
    }
    return (exit_status);
  }
}
void tar_stat_init(struct tar_stat_info *st) {

   

    {
      memset((void *)st, 0, sizeof(*st));
    }
    return;
   

}
void tar_stat_destroy(struct tar_stat_info *st) {

   

    {
      free((void *)st->orig_file_name);
      free((void *)st->file_name);
      free((void *)st->link_name);
      free((void *)st->uname);
      free((void *)st->gname);
      free((void *)st->sparse_map);
      memset((void *)st, 0, sizeof(*st));
    }
    return;
   

}
/* #pragma merger("0","00f.update.o.i","") */
static void append_file(char *path) {
  int handle;
  int tmp;
  struct stat stat_data;
  off_t bytes_left;
  union block *start;
  union block *tmp___0;
  size_t buffer_size;
  size_t tmp___1;
  size_t status;
  char buf[(((sizeof(uintmax_t) * 8UL) * 302UL) / 1000UL + 1UL) + 1UL];
  char *tmp___2;
  char *tmp___3;
  char *tmp___4;
  int tmp___5;
  int tmp___6;
                    
                    

  {
    {
      tmp = open((char const *)path, 0);
      handle = tmp;
    }
    if (handle < 0) {
      {
        open_error((char const *)path);
      }
      return;
    }
    {
      tmp___5 = fstat(handle, &stat_data);
    }
    if (tmp___5 != 0) {
       

        stat_error((char const *)path);
       

    } else {
      bytes_left = stat_data.st_size;
      {
        while (1) {
                                       ;
          if (!(bytes_left > 0L)) {
            goto while_break;
          }
          {
            tmp___0 = find_next_block();
            start = tmp___0;
            tmp___1 = available_space_after(start);
            buffer_size = tmp___1;
          }
          if ((size_t)bytes_left < buffer_size) {
            buffer_size = (size_t)bytes_left;
            status = buffer_size % 512UL;
            if (status) {
               

                memset((void *)(start->buffer + bytes_left), 0, 512UL - status);
               

            }
          }
          {
	    status = safe_read(handle, (void *)(start->buffer), buffer_size);
	  }
          if (status == 0xffffffffffffffffUL) {
             

              read_fatal_details((char const *)path,
                                 stat_data.st_size - bytes_left, buffer_size);
             

          }
          if (status == 0UL) {
             

              tmp___2 = stringify_uintmax_t_backwards(
                  (uintmax_t)bytes_left,
                  buf + ((((sizeof(uintmax_t) * 8UL) * 302UL) / 1000UL + 1UL) +
                         1UL));
              tmp___3 = quotearg_colon((char const *)path);
              tmp___4 = ngettext("%s: File shrank by %s byte",
                                 "%s: File shrank by %s bytes",
                                 (unsigned long)bytes_left);
              error(0, 0, (char const *)tmp___4, tmp___3, tmp___2);
              fatal_exit();
             

          }
          {
            bytes_left = (off_t)((size_t)bytes_left - status);
            set_next_block_after(start + (status - 1UL) / 512UL);
          }
        }
      while_break: /* CIL Label */;
      }
    }
    {
      tmp___6 = close(handle);
    }
    if (tmp___6 != 0) {
       

        close_error((char const *)path);
       

    }
    return;
  }
}
void update_archive(void) {
  enum read_header previous_status;
  int found_end;
  enum read_header status;
  enum read_header tmp;
  struct name *name;
  struct stat s;
  enum archive_format unused;
  int tmp___0;
  char *tmp___1;
  char *tmp___2;
  char *path;
  _Bool tmp___3;
  int tmp___4;
                    
                    
                    
                    

  {
    {
      previous_status = (enum read_header)0;
      found_end = 0;
      name_gather();
      open_archive((enum access_mode)2);
      xheader_write_global();
    }
    {
      while (1) {
                                     ;
        if (!(!found_end)) {
          goto while_break;
        }
        {
          tmp = read_header((_Bool)0);
          status = tmp;
        }
        {
          if ((unsigned int)status == 0U) {
            goto case_0;
          }
          if ((unsigned int)status == 2U) {
            goto case_0;
          }
          if ((unsigned int)status == 1U) {
            goto case_1;
          }
          if ((unsigned int)status == 3U) {
            goto case_3;
          }
          if ((unsigned int)status == 4U) {
            goto case_4;
          }
          if ((unsigned int)status == 5U) {
            goto case_5;
          }
          goto switch_break;
        case_0:  /* CIL Label */
                                
         {
          abort();
        }
        case_1: /* CIL Label */
          if ((unsigned int)subcommand_option == 8U) {
            {
              name = name_scan((char const *)current_stat_info.file_name);
            }
            if ((unsigned long)name != (unsigned long)((void *)0)) {
              {
                decode_header(current_header, &current_stat_info, &unused, 0);
                chdir_do(name->change_dir);
                tmp___0 =
                    deref_stat(dereference_option,
                               (char const *)current_stat_info.file_name, &s);
              }
              if (tmp___0 == 0) {
                if (s.st_mtim.tv_sec <= current_stat_info.stat.st_mtim.tv_sec) {
                   

                    add_avoided_name((char const *)current_stat_info.file_name);
                   

                }
              }
            }
          }
          {
	    skip_member();
	  }
          goto switch_break;
        case_3: /* CIL Label */
          current_block = current_header;
          found_end = 1;
          goto switch_break;
        case_4: /* CIL Label */
          found_end = 1;
          goto switch_break;
        case_5 : /* CIL Label */
        {
          set_next_block_after(current_header);
        }
          {
            if ((unsigned int)previous_status == 0U) {
              goto case_0___0;
            }
            if ((unsigned int)previous_status == 1U) {
              goto case_1___0;
            }
            if ((unsigned int)previous_status == 3U) {
              goto case_1___0;
            }
            if ((unsigned int)previous_status == 5U) {
              goto case_5___0;
            }
            if ((unsigned int)previous_status == 4U) {
              goto case_4___0;
            }
            if ((unsigned int)previous_status == 2U) {
              goto case_4___0;
            }
            goto switch_break___0;
          case_0___0 : /* CIL Label */
          {
            tmp___1 = gettext("This does not look like a tar archive");
            error(0, 0, (char const *)tmp___1);
          }
          case_1___0:  /* CIL Label */
                                      
           {
            tmp___2 = gettext("Skipping to next header");
            error(0, 0, (char const *)tmp___2);
            exit_status = 2;
          }
          case_5___0: /* CIL Label */
            goto switch_break___0;
          case_4___0:  /* CIL Label */
                                      
           {
            abort();
          }
          switch_break___0: /* CIL Label */;
          }
          goto switch_break;
        switch_break: /* CIL Label */;
        }
        previous_status = status;
      }
    while_break: /* CIL Label */;
    }
    {
      reset_eof();
      time_to_start_writing = (_Bool)1;
      output_start = current_block->buffer;
    }
    {
      while (1) {
      while_continue___0: /* CIL Label */;
        {
	  path = name_from_list();
	}
        if (!((unsigned long)path != (unsigned long)((void *)0))) {
          goto while_break___0;
        }
        {
	  tmp___3 = excluded_name((char const *)path);
	}
        if (tmp___3) {
          goto while_continue___0;
        }
        if (interactive_option) {
          {
            tmp___4 = confirm("add", (char const *)path);
          }
          if (!tmp___4) {
            goto while_continue___0;
          }
        }
        if ((unsigned int)subcommand_option == 2U) {
           

            append_file(path);
           

        } else {
           

	    dump_file(path, 1, (dev_t)0);
	   

        }
      }
    while_break___0: /* CIL Label */;
    }
    {
      write_eot();
      close_archive();
      names_notfound();
    }
    return;
  }
}
/* #pragma merger("0","010.utf8.o.i","") */
static iconv_t conv_desc[2] = {(iconv_t)-1, (iconv_t)-1};
static iconv_t utf8_init(_Bool to_utf) {

   

    if ((unsigned long)conv_desc[(int)to_utf] == (unsigned long)((iconv_t)-1)) {
      if (to_utf) {
        conv_desc[(int)to_utf] = (iconv_t)-1;
      } else {
        conv_desc[(int)to_utf] = (iconv_t)-1;
      }
    }
    return (conv_desc[(int)to_utf]);
   

}
_Bool utf8_convert(_Bool to_utf, char const *input, char **output) {
  char *ib;
  char *ob;
  size_t inlen;
  size_t outlen;
  size_t rc___1;
  iconv_t cd;
  iconv_t tmp;
  size_t tmp___0;
  char *tmp___1;
  void *tmp___2;

  {
    {
      tmp = utf8_init(to_utf);
      cd = tmp;
    }
    if ((unsigned long)cd == (unsigned long)((iconv_t)0)) {
      {
        *output = xstrdup(input);
      }
      return ((_Bool)1);
    } else {
      if ((unsigned long)cd == (unsigned long)((iconv_t)-1)) {
        return ((_Bool)0);
      }
    }
    {
      tmp___0 = strlen(input);
      inlen = tmp___0 + 1UL;
      outlen = inlen * 16UL + 1UL;
      tmp___2 = xmalloc(outlen);
      tmp___1 = (char *)tmp___2;
      *output = tmp___1;
      ob = tmp___1;
      ib = (char *)input;
      rc___1 = (size_t)0;
      *ob = (char)0;
    }
    return ((_Bool)(rc___1 != 0xffffffffffffffffUL));
  }
}
_Bool string_ascii_p(char const *str) {
  unsigned char const *p;

  {
    p = (unsigned char const *)str;
    {
      while (1) {
                                     ;
        if (!*p) {
          goto while_break;
        }
        if ((int const) * p > 127) {
          return ((_Bool)0);
        }
        p++;
      }
    while_break: /* CIL Label */;
    }
    return ((_Bool)1);
  }
}
/* #pragma merger("0","011.addext.o.i","") */
extern
    __attribute__((__nothrow__)) long(__attribute__((__nonnull__(1), __leaf__))
                                      pathconf)(char const *__path, int __name);
void addext(char *filename, char const *ext, int e);
size_t base_len(char const *name);
void addext(char *filename, char const *ext, int e) {
  char *s;
  char *tmp;
  size_t slen;
  size_t tmp___0;
  size_t extlen;
  size_t tmp___1;
  size_t slen_max;
  long name_max;
  int *tmp___2;
  char c;
  long size;
  int *tmp___3;

  {
    {
      tmp = base_name((char const *)filename);
      s = tmp;
      tmp___0 = base_len((char const *)s);
      slen = tmp___0;
      tmp___1 = strlen(ext);
      extlen = tmp___1;
      slen_max = (size_t)255;
    }
    if (14UL < slen + extlen) {
      goto _L___3;
    } else {
      if (0) {
      _L___3 : /* CIL Label */
      {
        tmp___2 = __errno_location();
        *tmp___2 = 0;
      }
        if ((unsigned long)s == (unsigned long)filename) {
           

            name_max = pathconf(".", 3);
           

        } else {
          c = *s;
          if (!((int)c == 47)) {
            *s = (char)0;
          }
          {
            name_max = pathconf((char const *)filename, 3);
            *s = c;
          }
        }
        if (0L <= name_max) {
          goto _L;
        } else {
          {
	    tmp___3 = __errno_location();
	  }
          if (*tmp___3 == 0) {
          _L:
            slen_max = (size_t)name_max;
            size = (long)slen_max;
            if (name_max != size) {
              slen_max = (size_t)-1;
            }
          }
        }
      }
    }
    if (slen + extlen <= slen_max) {
       

        strcpy((char * /* __restrict  */)(s + slen),
               (char const * /* __restrict  */)ext);
       

    } else {
      if (slen_max <= slen) {
        slen = slen_max - 1UL;
      }
      *(s + slen) = (char)e;
      *(s + (slen + 1UL)) = (char)0;
    }
    return;
  }
}
/* #pragma merger("0","012.argmatch.o.i","") */
int argmatch(char const *arg, char const *const *arglist, char const *vallist,
             size_t valsize);
                           
void argmatch_invalid(char const *context, char const *value, int problem);
void argmatch_valid(char const *const *arglist, char const *vallist,
                    size_t valsize);
int __xargmatch_internal(char const *context, char const *arg,
                         char const *const *arglist, char const *vallist,
                         size_t valsize, void (*exit_fn)(void));
int volatile exit_failure;
static void __argmatch_die(void) {

   

     

      exit((int)exit_failure);
     

   

}
void (*argmatch_die)(void) = &__argmatch_die;
int argmatch(char const *arg, char const *const *arglist, char const *vallist,
             size_t valsize) {
  int i;
  size_t arglen;
  int matchind;
  int ambiguous;
  int tmp;
  size_t tmp___0;
  int tmp___1;

  {
    {
      matchind = -1;
      ambiguous = 0;
      arglen = strlen(arg);
      i = 0;
    }
    {
      while (1) {
                                     ;
        if (!*(arglist + i)) {
          goto while_break;
        }
        {
	  tmp___1 = strncmp((char const *)*(arglist + i), arg, arglen);
	}
        if (!tmp___1) {
          {
            tmp___0 = strlen((char const *)*(arglist + i));
          }
          if (tmp___0 == arglen) {
            return (i);
          } else {
            if (matchind == -1) {
              matchind = i;
            } else {
              if ((unsigned long)vallist == (unsigned long)((void *)0)) {
                ambiguous = 1;
              } else {
                {
                  tmp = memcmp(
                      (void const *)(vallist + valsize * (size_t)matchind),
                      (void const *)(vallist + valsize * (size_t)i), valsize);
                }
                if (tmp) {
                  ambiguous = 1;
                }
              }
            }
          }
        }
        i++;
      }
    while_break: /* CIL Label */;
    }
    if (ambiguous) {
      return (-2);
    } else {
      return (matchind);
    }
  }
}
void argmatch_invalid(char const *context, char const *value, int problem) {
  char const *format;
  char *tmp;
  char *tmp___0;
  char *tmp___1;
  char const *tmp___2;
  char *tmp___3;
                    
                    

  {
    if (problem == -1) {
       

        tmp = gettext("invalid argument %s for %s");
        tmp___1 = tmp;
       

    } else {
       

        tmp___0 = gettext("ambiguous argument %s for %s");
        tmp___1 = tmp___0;
       

    }
    {
      format = (char const *)tmp___1;
      tmp___2 = quote_n(1, context);
      tmp___3 = quotearg_n_style(0, (enum quoting_style)5, value);
      error(0, 0, format, tmp___3, tmp___2);
    }
    return;
  }
}
void argmatch_valid(char const *const *arglist, char const *vallist,
                    size_t valsize) {
  int i;
  char const *last_val;
  char *tmp;
  int tmp___0;
                   

  {
    {
      last_val = (char const *)((void *)0);
      tmp = gettext("Valid arguments are:");
      fprintf((FILE * /* __restrict  */) stderr,
              (char const * /* __restrict  */)tmp);
      i = 0;
    }
    {
      while (1) {
                                     ;
        if (!*(arglist + i)) {
          goto while_break;
        }
        if (i == 0) {
           

            fprintf((FILE * /* __restrict  */) stderr,
                    (char const * /* __restrict  */) "\n  - `%s\'",
                    *(arglist + i));
            last_val = vallist + valsize * (size_t)i;
           

        } else {
          {
            tmp___0 =
                memcmp((void const *)last_val,
                       (void const *)(vallist + valsize * (size_t)i), valsize);
          }
          if (tmp___0) {
             

              fprintf((FILE * /* __restrict  */) stderr,
                      (char const * /* __restrict  */) "\n  - `%s\'",
                      *(arglist + i));
              last_val = vallist + valsize * (size_t)i;
             

          } else {
             

              fprintf((FILE * /* __restrict  */) stderr,
                      (char const * /* __restrict  */) ", `%s\'",
                      *(arglist + i));
             

          }
        }
        i++;
      }
    while_break: /* CIL Label */;
    }
    {
      putc_unlocked('\n', stderr);
    }
    return;
  }
}
int __xargmatch_internal(char const *context, char const *arg,
                         char const *const *arglist, char const *vallist,
                         size_t valsize, void (*exit_fn)(void)) {
  int res;
  int tmp;

  {
    {
      tmp = argmatch(arg, arglist, vallist, valsize);
      res = tmp;
    }
    if (res >= 0) {
      return (res);
    }
    {
      argmatch_invalid(context, arg, res);
      argmatch_valid(arglist, vallist, valsize);
      (*exit_fn)();
    }
    return (-1);
  }
}
/* #pragma merger("0","013.backupfile.o.i","") */
extern DIR *(__attribute__((__nonnull__(1))) opendir)(char const *__name);
extern int(__attribute__((__nonnull__(1))) closedir)(DIR *__dirp);
extern struct dirent *(__attribute__((__nonnull__(1))) readdir)(DIR *__dirp);
extern __attribute__((__nothrow__)) void *(__attribute__((__leaf__))
                                           malloc)(size_t __size)
    __attribute__((__malloc__));
enum backup_type get_version(char const *context, char const *version);
size_t dir_len(char const *path);
char const *simple_backup_suffix = "~";
static int max_backup_version(char const *file, char const *dir);
static int version_number(char const *base, char const *backup,
                          size_t base_length);
char *find_backup_file_name(char const *file,
                            enum backup_type backup_type___0) {
  size_t backup_suffix_size_max;
  size_t file_len;
  size_t tmp;
  size_t numbered_suffix_size_max;
  char *s;
  char const *suffix;
  size_t tmp___0;
  void *tmp___1;
  int highest_backup;
  size_t dirlen;
  size_t tmp___2;
  size_t tmp___3;
  char *tmp___4;
  char *numbered_suffix;
                    
                    

  {
    {
      tmp = strlen(file);
      file_len = tmp;
      numbered_suffix_size_max =
          (((sizeof(int) * 8UL - 1UL) * 302UL) / 1000UL + 2UL) + 4UL;
      suffix = simple_backup_suffix;
      tmp___0 = strlen(simple_backup_suffix);
      backup_suffix_size_max = tmp___0 + 1UL;
    }
    if (backup_suffix_size_max < numbered_suffix_size_max) {
      backup_suffix_size_max = numbered_suffix_size_max;
    }
    {
      tmp___1 = malloc(((file_len + 1UL) + backup_suffix_size_max) +
                       numbered_suffix_size_max);
      s = (char *)tmp___1;
    }
    if (s) {
      if ((unsigned int)backup_type___0 != 1U) {
        {
          tmp___2 = dir_len(file);
          dirlen = tmp___2;
          memcpy((void * /* __restrict  */)s,
                 (void const * /* __restrict  */)file, dirlen);
        }
        if (dirlen == 0UL) {
          tmp___3 = dirlen;
          dirlen++;
          *(s + tmp___3) = (char)'.';
        }
        {
          *(s + dirlen) = (char)'\000';
          tmp___4 = base_name(file);
          highest_backup =
              max_backup_version((char const *)tmp___4, (char const *)s);
        }
        if ((unsigned int)backup_type___0 == 2U) {
          if (!(highest_backup == 0)) {
             

              numbered_suffix = s + (file_len + backup_suffix_size_max);
              sprintf((char * /* __restrict  */)numbered_suffix,
                      (char const * /* __restrict  */) ".~%d~",
                      highest_backup + 1);
              suffix = (char const *)numbered_suffix;
             

          }
        } else {
           

            numbered_suffix = s + (file_len + backup_suffix_size_max);
            sprintf((char * /* __restrict  */)numbered_suffix,
                    (char const * /* __restrict  */) ".~%d~",
                    highest_backup + 1);
            suffix = (char const *)numbered_suffix;
           

        }
      }
      {
        strcpy((char * /* __restrict  */)s,
               (char const * /* __restrict  */)file);
        addext(s, suffix, '~');
      }
    }
    return (s);
  }
}
static int max_backup_version(char const *file, char const *dir) {
  DIR *dirp;
  struct dirent *dp;
  int highest_version;
  int this_version;
  size_t file_name_length;
  size_t tmp;
  int tmp___0;

  {
    {
      dirp = opendir(dir);
    }
    if (!dirp) {
      return (0);
    }
    {
      highest_version = 0;
      file_name_length = base_len(file);
    }
    {
      while (1) {
      while_continue: /* CIL Label */;
        {
	  dp = readdir(dirp);
	}
        if (!((unsigned long)dp != (unsigned long)((struct dirent *)0))) {
          goto while_break;
        }
        if (!(dp->d_ino != 0UL)) {
          goto while_continue;
        } else {
          {
	    tmp = strlen((char const *)(dp->d_name));
	  }
          if (tmp < file_name_length + 4UL) {
            goto while_continue;
          }
        }
        {
          this_version = version_number(file, (char const *)(dp->d_name),
                                        file_name_length);
        }
        if (this_version > highest_version) {
          highest_version = this_version;
        }
      }
    while_break: /* CIL Label */;
    }
    {
      tmp___0 = closedir(dirp);
    }
    if (tmp___0) {
      return (0);
    }
    return (highest_version);
  }
}
static int version_number(char const *base, char const *backup,
                          size_t base_length) {
  int version;
  char const *p;
  int tmp;

  {
    {
      version = 0;
      tmp = strncmp(base, backup, base_length);
    }
    if (tmp == 0) {
      if ((int const) * (backup + base_length) == 46) {
        if ((int const) * (backup + (base_length + 1UL)) == 126) {
          p = backup + (base_length + 2UL);
          {
            while (1) {
                                           ;
              if (!((unsigned int)*p - 48U <= 9U)) {
                goto while_break;
              }
              version = (version * 10 + (int)*p) - 48;
              p++;
            }
          while_break: /* CIL Label */;
          }
          if ((int const) * (p + 0) != 126) {
            version = 0;
          } else {
            if (*(p + 1)) {
              version = 0;
            }
          }
        }
      }
    }
    return (version);
  }
}
static char const *const backup_args[9] = {
    (char const * /* const  */) "none",
    (char const * /* const  */) "off",
    (char const * /* const  */) "simple",
    (char const * /* const  */) "never",
    (char const * /* const  */) "existing",
    (char const * /* const  */) "nil",
    (char const * /* const  */) "numbered",
    (char const * /* const  */) "t",
    (char const * /* const  */)0};
static enum backup_type const backup_types[8] = {
    (enum backup_type const)0, (enum backup_type const)0,
    (enum backup_type const)1, (enum backup_type const)1,
    (enum backup_type const)2, (enum backup_type const)2,
    (enum backup_type const)3, (enum backup_type const)3};
enum backup_type get_version(char const *context, char const *version) {
  int tmp;

  {
    if ((unsigned long)version == (unsigned long)((char const *)0)) {
      return ((enum backup_type)2);
    } else {
      if ((int const) * version == 0) {
        return ((enum backup_type)2);
      } else {
        {
          tmp = __xargmatch_internal(context, version, backup_args,
                                     (char const *)(backup_types),
                                     sizeof(backup_types[0]), argmatch_die);
        }
        return ((enum backup_type)backup_types[tmp]);
      }
    }
  }
}
enum backup_type xget_version(char const *context, char const *version) {
  enum backup_type tmp;
  char *tmp___0;
  enum backup_type tmp___1;
                   
                   
                   
                   

  {
    if (version) {
      if (*version) {
        {
          tmp = get_version(context, version);
        }
        return (tmp);
      } else {
        {
          tmp___0 = getenv("VERSION_CONTROL");
          tmp___1 = get_version("$VERSION_CONTROL", (char const *)tmp___0);
        }
        return (tmp___1);
      }
    } else {
      {
        tmp___0 = getenv("VERSION_CONTROL");
        tmp___1 = get_version("$VERSION_CONTROL", (char const *)tmp___0);
      }
      return (tmp___1);
    }
  }
}
/* #pragma merger("0","014.basename.o.i","") */
char *base_name(char const *name) {
  char const *base;
  char const *p;

  {
    base = name + 0;
    p = base;
    {
      while (1) {
                                     ;
        if (!*p) {
          goto while_break;
        }
        if ((int const) * p == 47) {
          {
            while (1) {
                                               ;
              p++;
              if (!((int const) * p == 47)) {
                goto while_break___0;
              }
            }
          while_break___0: /* CIL Label */;
          }
          if (!*p) {
            if ((int const) * base == 47) {
              base = p - 1;
            }
            goto while_break;
          }
          base = p;
        }
        p++;
      }
    while_break: /* CIL Label */;
    }
    return ((char *)base);
  }
}
size_t base_len(char const *name) {
  size_t len;

  {
    {
      len = strlen(name);
    }
    {
      while (1) {
                                     ;
        if (1UL < len) {
          if (!((int const) * (name + (len - 1UL)) == 47)) {
            goto while_break;
          }
        } else {
          goto while_break;
        }
        goto __Cont;
      __Cont:
        len--;
      }
    while_break: /* CIL Label */;
    }
    return (len);
  }
}
/* #pragma merger("0","015.dirname.o.i","") */
size_t dir_len(char const *path) {
  size_t prefix_length;
  size_t length;
  char *tmp;

  {
    {
      prefix_length = (size_t)0;
      tmp = base_name(path);
      length = (size_t)(tmp - (char *)path);
    }
    {
      while (1) {
                                     ;
        if (!(prefix_length < length)) {
          goto while_break;
        }
        if (!((int const) * (path + (length - 1UL)) == 47)) {
          return (length);
        }
        length--;
      }
    while_break: /* CIL Label */;
    }
    return (prefix_length +
            (size_t)((int const) * (path + prefix_length) == 47));
  }
}
char *dir_name(char const *path) {
  size_t length;
  size_t tmp;
  int append_dot;
  char *newpath;
  void *tmp___0;
  size_t tmp___1;

  {
    {
      tmp = dir_len(path);
      length = tmp;
      append_dot = length == 0UL;
      tmp___0 = xmalloc((length + (size_t)append_dot) + 1UL);
      newpath = (char *)tmp___0;
      memcpy((void * /* __restrict  */)newpath,
             (void const * /* __restrict  */)path, length);
    }
    if (append_dot) {
      tmp___1 = length;
      length++;
      *(newpath + tmp___1) = (char)'.';
    }
    *(newpath + length) = (char)0;
    return (newpath);
  }
}
/* #pragma merger("0","016.exclude.o.i","") */
extern __attribute__((__nothrow__)) int(
    __attribute__((__nonnull__(1, 2), __leaf__))
    strcasecmp)(char const *__s1, char const *__s2) __attribute__((__pure__));
extern __attribute__((__nothrow__)) int(
    __attribute__((__nonnull__(1, 2), __leaf__))
    strncasecmp)(char const *__s1, char const *__s2, size_t __n)
    __attribute__((__pure__));
void *xzalloc(size_t s);
void *x2realloc(void *p, size_t *pn);
void *x2nrealloc(void *p, size_t *pn, size_t s);
__inline static _Bool is_space(unsigned char c) {
  unsigned short const **tmp;

  {
    {
      tmp = __ctype_b_loc();
    }
    return ((_Bool)(((int const) * (*tmp + (int)c) & 8192) != 0));
  }
}
struct exclude *new_exclude(void) {
  struct exclude *tmp;
  void *tmp___0;

  {
    {
      tmp___0 = xzalloc(sizeof(*tmp));
    }
    return ((struct exclude *)tmp___0);
  }
}
static int fnmatch_no_wildcards(char const *pattern, char const *f,
                                int options) {
  int tmp;
  int tmp___0;
  int tmp___1;
  size_t patlen;
  size_t tmp___2;
  int r;
  int tmp___3;
  int tmp___4;
  int tmp___5;

  {
    if (!(options & (1 << 3))) {
      if (options & (1 << 4)) {
         

          tmp = strcasecmp(pattern, f);
          tmp___1 = tmp;
         

      } else {
         

          tmp___0 = strcmp(pattern, f);
          tmp___1 = tmp___0;
         

      }
      return (tmp___1);
    } else {
      {
        tmp___2 = strlen(pattern);
        patlen = tmp___2;
      }
      if (options & (1 << 4)) {
         

          tmp___3 = strncasecmp(pattern, f, patlen);
          tmp___5 = tmp___3;
         

      } else {
         

          tmp___4 = strncmp(pattern, f, patlen);
          tmp___5 = tmp___4;
         

      }
      r = tmp___5;
      if (!r) {
        r = (int)*(f + patlen);
        if (r == 47) {
          r = 0;
        }
      }
      return (r);
    }
  }
}
_Bool excluded_filename(struct exclude const *ex, char const *f) {
  size_t exclude_count;
  struct patopts const *exclude;
  size_t i;
  _Bool excluded___0;
  char const *pattern;
  int options;
  int (*matcher)(char const *, char const *, int);
  _Bool matched;
  int tmp;
  char const *p;
  int tmp___0;

  {
    exclude_count = (size_t)ex->exclude_count;
    if (exclude_count == 0UL) {
      return ((_Bool)0);
    } else {
      exclude = (struct patopts const *)ex->exclude;
      excluded___0 =
          (_Bool)(!(!((exclude + 0)->options & (int const)(1 << 29))));
      i = (size_t)0;
      {
        while (1) {
                                       ;
          if (!(i < exclude_count)) {
            goto while_break;
          }
          pattern = (char const *)(exclude + i)->pattern;
          options = (int)(exclude + i)->options;
          if ((int)excluded___0 == !(!(options & (1 << 29)))) {
            if (options & (1 << 28)) {
              matcher = &fnmatch;
            } else {
              matcher = &fnmatch_no_wildcards;
            }
            {
              tmp = (*matcher)(pattern, f, options);
              matched = (_Bool)(tmp == 0);
            }
            if (!(options & (1 << 30))) {
              p = f;
              {
                while (1) {
                                                   ;
                  if (*p) {
                    if (!(!matched)) {
                      goto while_break___0;
                    }
                  } else {
                    goto while_break___0;
                  }
                  if ((int const) * p == 47) {
                    if ((int const) * (p + 1) != 47) {
                       

                        tmp___0 = (*matcher)(pattern, p + 1, options);
                        matched = (_Bool)(tmp___0 == 0);
                       

                    }
                  }
                  p++;
                }
              while_break___0: /* CIL Label */;
              }
            }
            excluded___0 = (_Bool)((int)excluded___0 ^ (int)matched);
          }
          i++;
        }
      while_break: /* CIL Label */;
      }
      return (excluded___0);
    }
  }
}
void add_exclude(struct exclude *ex, char const *pattern, int options) {
  struct patopts *patopts;
  void *tmp;
  size_t tmp___0;

  {
    if (ex->exclude_count == ex->exclude_alloc) {
       

        tmp = x2nrealloc((void *)ex->exclude, &ex->exclude_alloc,
                         sizeof(*(ex->exclude)));
        ex->exclude = (struct patopts *)tmp;
       

    }
    tmp___0 = ex->exclude_count;
    (ex->exclude_count)++;
    patopts = ex->exclude + tmp___0;
    patopts->pattern = pattern;
    patopts->options = options;
    return;
  }
}
int add_exclude_file(void (*add_func)(struct exclude *, char const *, int),
                     struct exclude *ex, char const *filename, int options,
                     char line_end) {
  _Bool use_stdin;
  FILE *in;
  char *buf;
  char *p;
  char const *pattern;
  char const *lim;
  size_t buf_alloc;
  size_t buf_count;
  int c;
  int e;
  void *tmp;
  size_t tmp___0;
  int *tmp___1;
  int tmp___2;
  int *tmp___3;
  int tmp___4;
  void *tmp___5;
  char *pattern_end;
  _Bool tmp___6;
  _Bool tmp___7;
  int *tmp___8;
  int tmp___9;
  int tmp___10;
  int tmp___11;

  {
    if ((int const) * (filename + 0) == 45) {
      if (!*(filename + 1)) {
        tmp___9 = 1;
      } else {
        tmp___9 = 0;
      }
    } else {
      tmp___9 = 0;
    }
    use_stdin = (_Bool)tmp___9;
    buf = (char *)((void *)0);
    buf_alloc = (size_t)0;
    buf_count = (size_t)0;
    e = 0;
    if (use_stdin) {
      in = stdin;
    } else {
      {
        in = fopen((char const * /* __restrict  */)filename,
                   (char const * /* __restrict  */) "r");
      }
      if (!in) {
        return (-1);
      }
    }
    {
      while (1) {
                                     ;
        {
	  c = getc_unlocked(in);
	}
        if (!(c != -1)) {
          goto while_break;
        }
        if (buf_count == buf_alloc) {
           

            tmp = x2realloc((void *)buf, &buf_alloc);
            buf = (char *)tmp;
           

        }
        tmp___0 = buf_count;
        buf_count++;
        *(buf + tmp___0) = (char)c;
      }
    while_break: /* CIL Label */;
    }
    {
      tmp___2 = ferror_unlocked(in);
    }
    if (tmp___2) {
       

        tmp___1 = __errno_location();
        e = *tmp___1;
       

    }
    if (!use_stdin) {
      {
        tmp___4 = fclose(in);
      }
      if (tmp___4 != 0) {
         

          tmp___3 = __errno_location();
          e = *tmp___3;
         

      }
    }
    {
      tmp___5 = xrealloc((void *)buf, buf_count + 1UL);
      buf = (char *)tmp___5;
      *(buf + buf_count) = line_end;
    }
    if (buf_count == 0UL) {
      tmp___10 = 0;
    } else {
      if ((int)*(buf + (buf_count - 1UL)) == (int)line_end) {
        tmp___10 = 0;
      } else {
        tmp___10 = 1;
      }
    }
    lim = (char const *)((buf + buf_count) + tmp___10);
    pattern = (char const *)buf;
    p = buf;
    {
      while (1) {
                                         ;
        if (!((unsigned long)p < (unsigned long)lim)) {
          goto while_break___0;
        }
        if ((int)*p == (int)line_end) {
          {
            pattern_end = p;
            tmp___7 = is_space((unsigned char)line_end);
          }
          if (tmp___7) {
             

              while (1) {
                                                 ;
                if ((unsigned long)pattern_end == (unsigned long)pattern) {
                  goto next_pattern;
                } else {
                  {
		    tmp___6 = is_space((unsigned char)*(pattern_end + -1));
		  }
                  if (!tmp___6) {
                    goto while_break___1;
                  }
                }
                pattern_end--;
              }
            while_break___1: /* CIL Label */;
             

          }
          {
            *pattern_end = (char)'\000';
            (*add_func)(ex, pattern, options);
          }
        next_pattern:
          pattern = (char const *)(p + 1);
        }
        p++;
      }
    while_break___0: /* CIL Label */;
    }
    {
      tmp___8 = __errno_location();
      *tmp___8 = e;
    }
    if (e) {
      tmp___11 = -1;
    } else {
      tmp___11 = 0;
    }
    return (tmp___11);
  }
}
/* #pragma merger("0","017.exitfail.o.i","") */
int volatile exit_failure = (int volatile)1;
/* #pragma merger("0","018.full-write.o.i","") */
size_t safe_write(int fd, void const *buf, size_t count);
size_t full_write(int fd, void const *buf, size_t count) {
  size_t total;
  char const *ptr;
  size_t n_rw;
  size_t tmp;
  int *tmp___0;

  {
    total = (size_t)0;
    ptr = (char const *)buf;
    {
      while (1) {
                                     ;
        if (!(count > 0UL)) {
          goto while_break;
        }
        {
          tmp = safe_write(fd, (void const *)ptr, count);
          n_rw = tmp;
        }
        if (n_rw == 0xffffffffffffffffUL) {
          goto while_break;
        }
        if (n_rw == 0UL) {
          {
            tmp___0 = __errno_location();
            *tmp___0 = 28;
          }
          goto while_break;
        }
        total += n_rw;
        ptr += n_rw;
        count -= n_rw;
      }
    while_break: /* CIL Label */;
    }
    return (total);
  }
}
/* #pragma merger("0","019.getdate.o.i","") */
__attribute__((__nothrow__))
time_t(__attribute__((__leaf__)) rpl_mktime)(struct tm *tp);
int gettime(struct timespec *ts);
extern __attribute__((__nothrow__)) int(__attribute__((__leaf__))
                                        toupper)(int __c);
static int yyerror(char *s __attribute__((__unused__)));
static int yylex(YYSTYPE *lvalp, parser_control *pc);
static char const yytranslate[275] = {
    (char const)0,  (char const)2,  (char const)2,  (char const)2,
    (char const)2,  (char const)2,  (char const)2,  (char const)2,
    (char const)2,  (char const)2,  (char const)2,  (char const)2,
    (char const)2,  (char const)2,  (char const)2,  (char const)2,
    (char const)2,  (char const)2,  (char const)2,  (char const)2,
    (char const)2,  (char const)2,  (char const)2,  (char const)2,
    (char const)2,  (char const)2,  (char const)2,  (char const)2,
    (char const)2,  (char const)2,  (char const)2,  (char const)2,
    (char const)2,  (char const)2,  (char const)2,  (char const)2,
    (char const)2,  (char const)2,  (char const)2,  (char const)2,
    (char const)2,  (char const)2,  (char const)2,  (char const)2,
    (char const)23, (char const)2,  (char const)2,  (char const)24,
    (char const)2,  (char const)2,  (char const)2,  (char const)2,
    (char const)2,  (char const)2,  (char const)2,  (char const)2,
    (char const)2,  (char const)2,  (char const)22, (char const)2,
    (char const)2,  (char const)2,  (char const)2,  (char const)2,
    (char const)21, (char const)2,  (char const)2,  (char const)2,
    (char const)2,  (char const)2,  (char const)2,  (char const)2,
    (char const)2,  (char const)2,  (char const)2,  (char const)2,
    (char const)2,  (char const)2,  (char const)2,  (char const)2,
    (char const)2,  (char const)2,  (char const)2,  (char const)2,
    (char const)2,  (char const)2,  (char const)2,  (char const)2,
    (char const)2,  (char const)2,  (char const)2,  (char const)2,
    (char const)2,  (char const)2,  (char const)2,  (char const)2,
    (char const)2,  (char const)2,  (char const)2,  (char const)2,
    (char const)2,  (char const)2,  (char const)2,  (char const)2,
    (char const)2,  (char const)2,  (char const)2,  (char const)2,
    (char const)2,  (char const)2,  (char const)2,  (char const)2,
    (char const)2,  (char const)2,  (char const)2,  (char const)2,
    (char const)2,  (char const)2,  (char const)2,  (char const)2,
    (char const)2,  (char const)2,  (char const)2,  (char const)2,
    (char const)2,  (char const)2,  (char const)2,  (char const)2,
    (char const)2,  (char const)2,  (char const)2,  (char const)2,
    (char const)2,  (char const)2,  (char const)2,  (char const)2,
    (char const)2,  (char const)2,  (char const)2,  (char const)2,
    (char const)2,  (char const)2,  (char const)2,  (char const)2,
    (char const)2,  (char const)2,  (char const)2,  (char const)2,
    (char const)2,  (char const)2,  (char const)2,  (char const)2,
    (char const)2,  (char const)2,  (char const)2,  (char const)2,
    (char const)2,  (char const)2,  (char const)2,  (char const)2,
    (char const)2,  (char const)2,  (char const)2,  (char const)2,
    (char const)2,  (char const)2,  (char const)2,  (char const)2,
    (char const)2,  (char const)2,  (char const)2,  (char const)2,
    (char const)2,  (char const)2,  (char const)2,  (char const)2,
    (char const)2,  (char const)2,  (char const)2,  (char const)2,
    (char const)2,  (char const)2,  (char const)2,  (char const)2,
    (char const)2,  (char const)2,  (char const)2,  (char const)2,
    (char const)2,  (char const)2,  (char const)2,  (char const)2,
    (char const)2,  (char const)2,  (char const)2,  (char const)2,
    (char const)2,  (char const)2,  (char const)2,  (char const)2,
    (char const)2,  (char const)2,  (char const)2,  (char const)2,
    (char const)2,  (char const)2,  (char const)2,  (char const)2,
    (char const)2,  (char const)2,  (char const)2,  (char const)2,
    (char const)2,  (char const)2,  (char const)2,  (char const)2,
    (char const)2,  (char const)2,  (char const)2,  (char const)2,
    (char const)2,  (char const)2,  (char const)2,  (char const)2,
    (char const)2,  (char const)2,  (char const)2,  (char const)2,
    (char const)2,  (char const)2,  (char const)2,  (char const)2,
    (char const)2,  (char const)2,  (char const)2,  (char const)2,
    (char const)2,  (char const)2,  (char const)2,  (char const)2,
    (char const)2,  (char const)2,  (char const)2,  (char const)2,
    (char const)2,  (char const)2,  (char const)2,  (char const)2,
    (char const)2,  (char const)2,  (char const)2,  (char const)2,
    (char const)2,  (char const)2,  (char const)2,  (char const)2,
    (char const)1,  (char const)3,  (char const)4,  (char const)5,
    (char const)6,  (char const)7,  (char const)8,  (char const)9,
    (char const)10, (char const)11, (char const)12, (char const)13,
    (char const)14, (char const)15, (char const)16, (char const)17,
    (char const)18, (char const)19, (char const)20};
static short const yyr1[66] = {
    (short const)0,  (short const)25, (short const)25, (short const)26,
    (short const)27, (short const)27, (short const)28, (short const)28,
    (short const)28, (short const)28, (short const)28, (short const)28,
    (short const)28, (short const)29, (short const)29, (short const)29,
    (short const)29, (short const)29, (short const)30, (short const)30,
    (short const)31, (short const)31, (short const)31, (short const)32,
    (short const)32, (short const)32, (short const)33, (short const)33,
    (short const)33, (short const)33, (short const)33, (short const)33,
    (short const)33, (short const)33, (short const)33, (short const)34,
    (short const)34, (short const)35, (short const)35, (short const)35,
    (short const)35, (short const)35, (short const)35, (short const)35,
    (short const)35, (short const)35, (short const)35, (short const)35,
    (short const)35, (short const)35, (short const)35, (short const)35,
    (short const)35, (short const)35, (short const)35, (short const)35,
    (short const)35, (short const)36, (short const)36, (short const)37,
    (short const)37, (short const)38, (short const)38, (short const)39,
    (short const)40, (short const)40};
static short const yyr2[66] = {
    (short const)0, (short const)1, (short const)1, (short const)2,
    (short const)0, (short const)2, (short const)1, (short const)1,
    (short const)1, (short const)1, (short const)1, (short const)1,
    (short const)1, (short const)2, (short const)4, (short const)4,
    (short const)6, (short const)6, (short const)1, (short const)2,
    (short const)1, (short const)1, (short const)2, (short const)1,
    (short const)2, (short const)2, (short const)3, (short const)5,
    (short const)3, (short const)3, (short const)3, (short const)2,
    (short const)4, (short const)2, (short const)3, (short const)2,
    (short const)1, (short const)2, (short const)2, (short const)1,
    (short const)2, (short const)2, (short const)1, (short const)2,
    (short const)2, (short const)1, (short const)2, (short const)2,
    (short const)1, (short const)2, (short const)2, (short const)1,
    (short const)2, (short const)2, (short const)2, (short const)2,
    (short const)1, (short const)1, (short const)1, (short const)1,
    (short const)1, (short const)1, (short const)1, (short const)1,
    (short const)0, (short const)1};
static short const yydefact[81] = {
    (short const)4,  (short const)0,  (short const)1,  (short const)2,
    (short const)60, (short const)62, (short const)59, (short const)61,
    (short const)3,  (short const)57, (short const)58, (short const)23,
    (short const)45, (short const)21, (short const)48, (short const)18,
    (short const)51, (short const)0,  (short const)42, (short const)56,
    (short const)39, (short const)20, (short const)0,  (short const)63,
    (short const)0,  (short const)0,  (short const)5,  (short const)6,
    (short const)7,  (short const)8,  (short const)10, (short const)9,
    (short const)11, (short const)36, (short const)12, (short const)24,
    (short const)19, (short const)0,  (short const)31, (short const)22,
    (short const)44, (short const)47, (short const)50, (short const)41,
    (short const)53, (short const)38, (short const)25, (short const)43,
    (short const)46, (short const)13, (short const)49, (short const)33,
    (short const)40, (short const)52, (short const)37, (short const)0,
    (short const)0,  (short const)0,  (short const)54, (short const)55,
    (short const)35, (short const)30, (short const)0,  (short const)29,
    (short const)34, (short const)28, (short const)64, (short const)26,
    (short const)32, (short const)65, (short const)15, (short const)0,
    (short const)14, (short const)0,  (short const)64, (short const)27,
    (short const)17, (short const)16, (short const)0,  (short const)0,
    (short const)0};
static short const yydefgoto[16] = {
    (short const)78, (short const)2,  (short const)3,  (short const)26,
    (short const)27, (short const)28, (short const)29, (short const)30,
    (short const)31, (short const)32, (short const)33, (short const)8,
    (short const)9,  (short const)10, (short const)34, (short const)72};
static short const yypact[81] = {
    (short const) - 19,    (short const)29,       (short const) - 32768,
    (short const)15,       (short const) - 32768, (short const) - 32768,
    (short const) - 32768, (short const) - 32768, (short const) - 32768,
    (short const) - 32768, (short const) - 32768, (short const) - 8,
    (short const) - 32768, (short const) - 32768, (short const) - 32768,
    (short const)36,       (short const) - 32768, (short const) - 4,
    (short const) - 32768, (short const) - 32768, (short const) - 32768,
    (short const)38,       (short const)30,       (short const) - 5,
    (short const)39,       (short const)40,       (short const) - 32768,
    (short const) - 32768, (short const) - 32768, (short const) - 32768,
    (short const) - 32768, (short const) - 32768, (short const) - 32768,
    (short const)49,       (short const) - 32768, (short const) - 32768,
    (short const) - 32768, (short const)41,       (short const)32,
    (short const) - 32768, (short const) - 32768, (short const) - 32768,
    (short const) - 32768, (short const) - 32768, (short const) - 32768,
    (short const) - 32768, (short const) - 32768, (short const) - 32768,
    (short const) - 32768, (short const) - 32768, (short const) - 32768,
    (short const)33,       (short const) - 32768, (short const) - 32768,
    (short const) - 32768, (short const)42,       (short const)43,
    (short const)44,       (short const) - 32768, (short const) - 32768,
    (short const) - 32768, (short const) - 32768, (short const)45,
    (short const) - 32768, (short const) - 32768, (short const) - 32768,
    (short const) - 6,     (short const)46,       (short const) - 32768,
    (short const) - 32768, (short const) - 32768, (short const)19,
    (short const) - 32768, (short const)47,       (short const)8,
    (short const) - 32768, (short const) - 32768, (short const) - 32768,
    (short const)56,       (short const)57,       (short const) - 32768};
static short const yypgoto[16] = {
    (short const) - 32768, (short const) - 32768, (short const) - 32768,
    (short const) - 32768, (short const) - 32768, (short const) - 32768,
    (short const) - 32768, (short const) - 32768, (short const) - 32768,
    (short const) - 32768, (short const) - 32768, (short const) - 32768,
    (short const) - 32768, (short const) - 11,    (short const) - 32768,
    (short const) - 10};
static short const yytable[71] = {
    (short const)46, (short const)47, (short const)1,  (short const)48,
    (short const)69, (short const)49, (short const)50, (short const)51,
    (short const)52, (short const)53, (short const)54, (short const)70,
    (short const)55, (short const)37, (short const)38, (short const)35,
    (short const)71, (short const)56, (short const)69, (short const)57,
    (short const)11, (short const)12, (short const)13, (short const)14,
    (short const)15, (short const)76, (short const)16, (short const)17,
    (short const)18, (short const)19, (short const)20, (short const)21,
    (short const)22, (short const)23, (short const)24, (short const)25,
    (short const)40, (short const)5,  (short const)41, (short const)7,
    (short const)36, (short const)42, (short const)39, (short const)43,
    (short const)44, (short const)45, (short const)4,  (short const)5,
    (short const)6,  (short const)7,  (short const)63, (short const)64,
    (short const)60, (short const)58, (short const)59, (short const)62,
    (short const)79, (short const)80, (short const)61, (short const)65,
    (short const)74, (short const)66, (short const)67, (short const)68,
    (short const)77, (short const)75, (short const)0,  (short const)0,
    (short const)0,  (short const)0,  (short const)73};
static short const yycheck[71] = {
    (short const)5,    (short const)6,    (short const)21,   (short const)8,
    (short const)10,   (short const)10,   (short const)11,   (short const)12,
    (short const)13,   (short const)14,   (short const)15,   (short const)17,
    (short const)17,   (short const)17,   (short const)18,   (short const)23,
    (short const)22,   (short const)22,   (short const)10,   (short const)24,
    (short const)5,    (short const)6,    (short const)7,    (short const)8,
    (short const)9,    (short const)17,   (short const)11,   (short const)12,
    (short const)13,   (short const)14,   (short const)15,   (short const)16,
    (short const)17,   (short const)18,   (short const)19,   (short const)20,
    (short const)6,    (short const)18,   (short const)8,    (short const)20,
    (short const)4,    (short const)11,   (short const)4,    (short const)13,
    (short const)14,   (short const)15,   (short const)17,   (short const)18,
    (short const)19,   (short const)20,   (short const)17,   (short const)18,
    (short const)3,    (short const)14,   (short const)14,   (short const)23,
    (short const)0,    (short const)0,    (short const)17,   (short const)17,
    (short const)71,   (short const)18,   (short const)18,   (short const)18,
    (short const)74,   (short const)18,   (short const) - 1, (short const) - 1,
    (short const) - 1, (short const) - 1, (short const)24};
int yyparse(void *parm);
int yyparse(void *parm) {
  register int yystate;
  register int yyn;
  register short *yyssp;
  register YYSTYPE *yyvsp;
  int yyerrstatus;
  int yychar1;
  short yyssa[200];
  YYSTYPE yyvsa[200];
  short *yyss;
  YYSTYPE *yyvs;
  int yystacksize;
  int yyfree_stacks;
  int yychar;
  YYSTYPE yylval;
  int yynerrs;
  YYSTYPE yyval;
  int yylen;
  YYSTYPE *yyvs1;
  short *yyss1;
  int size;
  void *tmp;
  void *tmp___0;
  int tmp___1;
  long tmp___2;
                    
                    
                  
                    
                    

  {
    yychar1 = 0;
    yyss = yyssa;
    yyvs = yyvsa;
    yystacksize = 200;
    yyfree_stacks = 0;
    yystate = 0;
    yyerrstatus = 0;
    yynerrs = 0;
    yychar = -2;
    yyssp = yyss - 1;
    yyvsp = yyvs;
  yynewstate:
    yyssp++;
    *yyssp = (short)yystate;
    if ((unsigned long)yyssp >= (unsigned long)((yyss + yystacksize) - 1)) {
      yyvs1 = yyvs;
      yyss1 = yyss;
      size = (int)((yyssp - yyss) + 1L);
      if (yystacksize >= 10000) {
        {
          yyerror((char *)"parser stack overflow");
        }
        if (yyfree_stacks) {
           

            free((void *)yyss);
            free((void *)yyvs);
           

        }
        return (2);
      }
      yystacksize *= 2;
      if (yystacksize > 10000) {
        yystacksize = 10000;
      }
      {
        tmp = __builtin_alloca((unsigned long)yystacksize * sizeof(*yyssp));
        yyss = (short *)tmp;
        __builtin_memcpy(
            (void *)((char *)yyss), (void const *)((char *)yyss1),
            (unsigned long)((unsigned int)size * (unsigned int)sizeof(*yyssp)));
        tmp___0 = __builtin_alloca((unsigned long)yystacksize * sizeof(*yyvsp));
        yyvs = (YYSTYPE *)tmp___0;
        __builtin_memcpy(
            (void *)((char *)yyvs), (void const *)((char *)yyvs1),
            (unsigned long)((unsigned int)size * (unsigned int)sizeof(*yyvsp)));
        yyssp = (yyss + size) - 1;
        yyvsp = (yyvs + size) - 1;
      }
      if ((unsigned long)yyssp >= (unsigned long)((yyss + yystacksize) - 1)) {
        goto yyabortlab;
      }
    }
    goto yybackup;
  yybackup:
    yyn = (int)yypact[yystate];
    if (yyn == -32768) {
      goto yydefault;
    }
    if (yychar == -2) {
       

        yychar = yylex(&yylval, (parser_control *)parm);
       

    }
    if (yychar <= 0) {
      yychar1 = 0;
      yychar = 0;
    } else {
      if ((unsigned int)yychar <= 274U) {
        tmp___1 = (int const)yytranslate[yychar];
      } else {
        tmp___1 = (int const)41;
      }
      yychar1 = (int)tmp___1;
    }
    yyn += yychar1;
    if (yyn < 0) {
      goto yydefault;
    } else {
      if (yyn > 70) {
        goto yydefault;
      } else {
        if ((int const)yycheck[yyn] != (int const)yychar1) {
          goto yydefault;
        }
      }
    }
    yyn = (int)yytable[yyn];
    if (yyn < 0) {
      if (yyn == -32768) {
        goto yyerrlab;
      }
      yyn = -yyn;
      goto yyreduce;
    } else {
      if (yyn == 0) {
        goto yyerrlab;
      }
    }
    if (yyn == 80) {
      goto yyacceptlab;
    }
    if (yychar != 0) {
      yychar = -2;
    }
    yyvsp++;
    *yyvsp = yylval;
    if (yyerrstatus) {
      yyerrstatus--;
    }
    yystate = yyn;
    goto yynewstate;
  yydefault:
    yyn = (int)yydefact[yystate];
    if (yyn == 0) {
      goto yyerrlab;
    }
  yyreduce:
    yylen = (int)yyr2[yyn];
    if (yylen > 0) {
      yyval = *(yyvsp + (1 - yylen));
    }
    {
      if (yyn == 3) {
        goto case_3;
      }
      if (yyn == 6) {
        goto case_6;
      }
      if (yyn == 7) {
        goto case_7;
      }
      if (yyn == 8) {
        goto case_8;
      }
      if (yyn == 9) {
        goto case_9;
      }
      if (yyn == 10) {
        goto case_10;
      }
      if (yyn == 11) {
        goto case_11;
      }
      if (yyn == 13) {
        goto case_13;
      }
      if (yyn == 14) {
        goto case_14;
      }
      if (yyn == 15) {
        goto case_15;
      }
      if (yyn == 16) {
        goto case_16;
      }
      if (yyn == 17) {
        goto case_17;
      }
      if (yyn == 18) {
        goto case_18;
      }
      if (yyn == 19) {
        goto case_19;
      }
      if (yyn == 20) {
        goto case_20;
      }
      if (yyn == 21) {
        goto case_21;
      }
      if (yyn == 22) {
        goto case_22;
      }
      if (yyn == 23) {
        goto case_23;
      }
      if (yyn == 24) {
        goto case_24;
      }
      if (yyn == 25) {
        goto case_25;
      }
      if (yyn == 26) {
        goto case_26;
      }
      if (yyn == 27) {
        goto case_27;
      }
      if (yyn == 28) {
        goto case_28;
      }
      if (yyn == 29) {
        goto case_29;
      }
      if (yyn == 30) {
        goto case_30;
      }
      if (yyn == 31) {
        goto case_31;
      }
      if (yyn == 32) {
        goto case_32;
      }
      if (yyn == 33) {
        goto case_33;
      }
      if (yyn == 34) {
        goto case_34;
      }
      if (yyn == 35) {
        goto case_35;
      }
      if (yyn == 37) {
        goto case_37;
      }
      if (yyn == 38) {
        goto case_38;
      }
      if (yyn == 39) {
        goto case_39;
      }
      if (yyn == 40) {
        goto case_40;
      }
      if (yyn == 41) {
        goto case_41;
      }
      if (yyn == 42) {
        goto case_42;
      }
      if (yyn == 43) {
        goto case_43;
      }
      if (yyn == 44) {
        goto case_44;
      }
      if (yyn == 45) {
        goto case_45;
      }
      if (yyn == 46) {
        goto case_46;
      }
      if (yyn == 47) {
        goto case_47;
      }
      if (yyn == 48) {
        goto case_48;
      }
      if (yyn == 49) {
        goto case_49;
      }
      if (yyn == 50) {
        goto case_50;
      }
      if (yyn == 51) {
        goto case_51;
      }
      if (yyn == 52) {
        goto case_52;
      }
      if (yyn == 53) {
        goto case_53;
      }
      if (yyn == 54) {
        goto case_54;
      }
      if (yyn == 55) {
        goto case_55;
      }
      if (yyn == 56) {
        goto case_56;
      }
      if (yyn == 60) {
        goto case_60;
      }
      if (yyn == 62) {
        goto case_62;
      }
      if (yyn == 63) {
        goto case_63;
      }
      if (yyn == 64) {
        goto case_64;
      }
      if (yyn == 65) {
        goto case_65;
      }
      goto switch_break;
    case_3: /* CIL Label */
      ((parser_control *)parm)->seconds = (yyvsp + 0)->timespec;
      ((parser_control *)parm)->timespec_seen = (_Bool)1;
      goto switch_break;
    case_6: /* CIL Label */
      (((parser_control *)parm)->times_seen)++;
      goto switch_break;
    case_7: /* CIL Label */
      (((parser_control *)parm)->local_zones_seen)++;
      goto switch_break;
    case_8: /* CIL Label */
      (((parser_control *)parm)->zones_seen)++;
      goto switch_break;
    case_9: /* CIL Label */
      (((parser_control *)parm)->dates_seen)++;
      goto switch_break;
    case_10: /* CIL Label */
      (((parser_control *)parm)->days_seen)++;
      goto switch_break;
    case_11: /* CIL Label */
      (((parser_control *)parm)->rels_seen)++;
      goto switch_break;
    case_13: /* CIL Label */
      ((parser_control *)parm)->hour = (yyvsp + -1)->textintval.value;
      ((parser_control *)parm)->minutes = 0L;
      ((parser_control *)parm)->seconds.tv_sec = (__time_t)0;
      ((parser_control *)parm)->seconds.tv_nsec = (__syscall_slong_t)0;
      ((parser_control *)parm)->meridian = (int)(yyvsp + 0)->intval;
      goto switch_break;
    case_14: /* CIL Label */
      ((parser_control *)parm)->hour = (yyvsp + -3)->textintval.value;
      ((parser_control *)parm)->minutes = (yyvsp + -1)->textintval.value;
      ((parser_control *)parm)->seconds.tv_sec = (__time_t)0;
      ((parser_control *)parm)->seconds.tv_nsec = (__syscall_slong_t)0;
      ((parser_control *)parm)->meridian = (int)(yyvsp + 0)->intval;
      goto switch_break;
    case_15: /* CIL Label */
      ((parser_control *)parm)->hour = (yyvsp + -3)->textintval.value;
      ((parser_control *)parm)->minutes = (yyvsp + -1)->textintval.value;
      ((parser_control *)parm)->seconds.tv_sec = (__time_t)0;
      ((parser_control *)parm)->seconds.tv_nsec = (__syscall_slong_t)0;
      ((parser_control *)parm)->meridian = 2;
      (((parser_control *)parm)->zones_seen)++;
      ((parser_control *)parm)->time_zone =
          (yyvsp + 0)->textintval.value % 100L +
          ((yyvsp + 0)->textintval.value / 100L) * 60L;
      goto switch_break;
    case_16: /* CIL Label */
      ((parser_control *)parm)->hour = (yyvsp + -5)->textintval.value;
      ((parser_control *)parm)->minutes = (yyvsp + -3)->textintval.value;
      ((parser_control *)parm)->seconds = (yyvsp + -1)->timespec;
      ((parser_control *)parm)->meridian = (int)(yyvsp + 0)->intval;
      goto switch_break;
    case_17: /* CIL Label */
      ((parser_control *)parm)->hour = (yyvsp + -5)->textintval.value;
      ((parser_control *)parm)->minutes = (yyvsp + -3)->textintval.value;
      ((parser_control *)parm)->seconds = (yyvsp + -1)->timespec;
      ((parser_control *)parm)->meridian = 2;
      (((parser_control *)parm)->zones_seen)++;
      ((parser_control *)parm)->time_zone =
          (yyvsp + 0)->textintval.value % 100L +
          ((yyvsp + 0)->textintval.value / 100L) * 60L;
      goto switch_break;
    case_18: /* CIL Label */
      ((parser_control *)parm)->local_isdst = (int)(yyvsp + 0)->intval;
      goto switch_break;
    case_19: /* CIL Label */
      if ((yyvsp + -1)->intval < 0L) {
        tmp___2 = 1L;
      } else {
        tmp___2 = (yyvsp + -1)->intval + 1L;
      }
      ((parser_control *)parm)->local_isdst = (int)tmp___2;
      goto switch_break;
    case_20: /* CIL Label */
      ((parser_control *)parm)->time_zone = (yyvsp + 0)->intval;
      goto switch_break;
    case_21: /* CIL Label */
      ((parser_control *)parm)->time_zone = (yyvsp + 0)->intval + 60L;
      goto switch_break;
    case_22: /* CIL Label */
      ((parser_control *)parm)->time_zone = (yyvsp + -1)->intval + 60L;
      goto switch_break;
    case_23: /* CIL Label */
      ((parser_control *)parm)->day_ordinal = 1L;
      ((parser_control *)parm)->day_number = (int)(yyvsp + 0)->intval;
      goto switch_break;
    case_24: /* CIL Label */
      ((parser_control *)parm)->day_ordinal = 1L;
      ((parser_control *)parm)->day_number = (int)(yyvsp + -1)->intval;
      goto switch_break;
    case_25: /* CIL Label */
      ((parser_control *)parm)->day_ordinal = (yyvsp + -1)->textintval.value;
      ((parser_control *)parm)->day_number = (int)(yyvsp + 0)->intval;
      goto switch_break;
    case_26: /* CIL Label */
      ((parser_control *)parm)->month = (yyvsp + -2)->textintval.value;
      ((parser_control *)parm)->day = (yyvsp + 0)->textintval.value;
      goto switch_break;
    case_27: /* CIL Label */
      if (4UL <= (yyvsp + -4)->textintval.digits) {
        ((parser_control *)parm)->year = (yyvsp + -4)->textintval;
        ((parser_control *)parm)->month = (yyvsp + -2)->textintval.value;
        ((parser_control *)parm)->day = (yyvsp + 0)->textintval.value;
      } else {
        ((parser_control *)parm)->month = (yyvsp + -4)->textintval.value;
        ((parser_control *)parm)->day = (yyvsp + -2)->textintval.value;
        ((parser_control *)parm)->year = (yyvsp + 0)->textintval;
      }
      goto switch_break;
    case_28: /* CIL Label */
      ((parser_control *)parm)->year = (yyvsp + -2)->textintval;
      ((parser_control *)parm)->month = -(yyvsp + -1)->textintval.value;
      ((parser_control *)parm)->day = -(yyvsp + 0)->textintval.value;
      goto switch_break;
    case_29: /* CIL Label */
      ((parser_control *)parm)->day = (yyvsp + -2)->textintval.value;
      ((parser_control *)parm)->month = (yyvsp + -1)->intval;
      ((parser_control *)parm)->year.value = -(yyvsp + 0)->textintval.value;
      ((parser_control *)parm)->year.digits = (yyvsp + 0)->textintval.digits;
      goto switch_break;
    case_30: /* CIL Label */
      ((parser_control *)parm)->month = (yyvsp + -2)->intval;
      ((parser_control *)parm)->day = -(yyvsp + -1)->textintval.value;
      ((parser_control *)parm)->year.value = -(yyvsp + 0)->textintval.value;
      ((parser_control *)parm)->year.digits = (yyvsp + 0)->textintval.digits;
      goto switch_break;
    case_31: /* CIL Label */
      ((parser_control *)parm)->month = (yyvsp + -1)->intval;
      ((parser_control *)parm)->day = (yyvsp + 0)->textintval.value;
      goto switch_break;
    case_32: /* CIL Label */
      ((parser_control *)parm)->month = (yyvsp + -3)->intval;
      ((parser_control *)parm)->day = (yyvsp + -2)->textintval.value;
      ((parser_control *)parm)->year = (yyvsp + 0)->textintval;
      goto switch_break;
    case_33: /* CIL Label */
      ((parser_control *)parm)->day = (yyvsp + -1)->textintval.value;
      ((parser_control *)parm)->month = (yyvsp + 0)->intval;
      goto switch_break;
    case_34: /* CIL Label */
      ((parser_control *)parm)->day = (yyvsp + -2)->textintval.value;
      ((parser_control *)parm)->month = (yyvsp + -1)->intval;
      ((parser_control *)parm)->year = (yyvsp + 0)->textintval;
      goto switch_break;
    case_35: /* CIL Label */
      ((parser_control *)parm)->rel_ns = -((parser_control *)parm)->rel_ns;
      ((parser_control *)parm)->rel_seconds =
          -((parser_control *)parm)->rel_seconds;
      ((parser_control *)parm)->rel_minutes =
          -((parser_control *)parm)->rel_minutes;
      ((parser_control *)parm)->rel_hour = -((parser_control *)parm)->rel_hour;
      ((parser_control *)parm)->rel_day = -((parser_control *)parm)->rel_day;
      ((parser_control *)parm)->rel_month =
          -((parser_control *)parm)->rel_month;
      ((parser_control *)parm)->rel_year = -((parser_control *)parm)->rel_year;
      goto switch_break;
    case_37: /* CIL Label */
      ((parser_control *)parm)->rel_year +=
          (yyvsp + -1)->textintval.value * (yyvsp + 0)->intval;
      goto switch_break;
    case_38: /* CIL Label */
      ((parser_control *)parm)->rel_year +=
          (yyvsp + -1)->textintval.value * (yyvsp + 0)->intval;
      goto switch_break;
    case_39: /* CIL Label */
      ((parser_control *)parm)->rel_year += (yyvsp + 0)->intval;
      goto switch_break;
    case_40: /* CIL Label */
      ((parser_control *)parm)->rel_month +=
          (yyvsp + -1)->textintval.value * (yyvsp + 0)->intval;
      goto switch_break;
    case_41: /* CIL Label */
      ((parser_control *)parm)->rel_month +=
          (yyvsp + -1)->textintval.value * (yyvsp + 0)->intval;
      goto switch_break;
    case_42: /* CIL Label */
      ((parser_control *)parm)->rel_month += (yyvsp + 0)->intval;
      goto switch_break;
    case_43: /* CIL Label */
      ((parser_control *)parm)->rel_day +=
          (yyvsp + -1)->textintval.value * (yyvsp + 0)->intval;
      goto switch_break;
    case_44: /* CIL Label */
      ((parser_control *)parm)->rel_day +=
          (yyvsp + -1)->textintval.value * (yyvsp + 0)->intval;
      goto switch_break;
    case_45: /* CIL Label */
      ((parser_control *)parm)->rel_day += (yyvsp + 0)->intval;
      goto switch_break;
    case_46: /* CIL Label */
      ((parser_control *)parm)->rel_hour +=
          (yyvsp + -1)->textintval.value * (yyvsp + 0)->intval;
      goto switch_break;
    case_47: /* CIL Label */
      ((parser_control *)parm)->rel_hour +=
          (yyvsp + -1)->textintval.value * (yyvsp + 0)->intval;
      goto switch_break;
    case_48: /* CIL Label */
      ((parser_control *)parm)->rel_hour += (yyvsp + 0)->intval;
      goto switch_break;
    case_49: /* CIL Label */
      ((parser_control *)parm)->rel_minutes +=
          (yyvsp + -1)->textintval.value * (yyvsp + 0)->intval;
      goto switch_break;
    case_50: /* CIL Label */
      ((parser_control *)parm)->rel_minutes +=
          (yyvsp + -1)->textintval.value * (yyvsp + 0)->intval;
      goto switch_break;
    case_51: /* CIL Label */
      ((parser_control *)parm)->rel_minutes += (yyvsp + 0)->intval;
      goto switch_break;
    case_52: /* CIL Label */
      ((parser_control *)parm)->rel_seconds +=
          (yyvsp + -1)->textintval.value * (yyvsp + 0)->intval;
      goto switch_break;
    case_53: /* CIL Label */
      ((parser_control *)parm)->rel_seconds +=
          (yyvsp + -1)->textintval.value * (yyvsp + 0)->intval;
      goto switch_break;
    case_54: /* CIL Label */
      ((parser_control *)parm)->rel_seconds +=
          (yyvsp + -1)->timespec.tv_sec * (yyvsp + 0)->intval;
      ((parser_control *)parm)->rel_ns +=
          (yyvsp + -1)->timespec.tv_nsec * (yyvsp + 0)->intval;
      goto switch_break;
    case_55: /* CIL Label */
      ((parser_control *)parm)->rel_seconds +=
          (yyvsp + -1)->timespec.tv_sec * (yyvsp + 0)->intval;
      ((parser_control *)parm)->rel_ns +=
          (yyvsp + -1)->timespec.tv_nsec * (yyvsp + 0)->intval;
      goto switch_break;
    case_56: /* CIL Label */
      ((parser_control *)parm)->rel_seconds += (yyvsp + 0)->intval;
      goto switch_break;
    case_60: /* CIL Label */
      yyval.timespec.tv_sec = (yyvsp + 0)->textintval.value;
      yyval.timespec.tv_nsec = (__syscall_slong_t)0;
      goto switch_break;
    case_62: /* CIL Label */
      yyval.timespec.tv_sec = (yyvsp + 0)->textintval.value;
      yyval.timespec.tv_nsec = (__syscall_slong_t)0;
      goto switch_break;
    case_63: /* CIL Label */
      if (((parser_control *)parm)->dates_seen) {
        if (!((parser_control *)parm)->rels_seen) {
          if (((parser_control *)parm)->times_seen) {
            ((parser_control *)parm)->year = (yyvsp + 0)->textintval;
          } else {
            if (2UL < (yyvsp + 0)->textintval.digits) {
              ((parser_control *)parm)->year = (yyvsp + 0)->textintval;
            } else {
              goto _L___4;
            }
          }
        } else {
          goto _L___4;
        }
      } else {
      _L___4: /* CIL Label */
        if (4UL < (yyvsp + 0)->textintval.digits) {
          (((parser_control *)parm)->dates_seen)++;
          ((parser_control *)parm)->day = (yyvsp + 0)->textintval.value % 100L;
          ((parser_control *)parm)->month =
              ((yyvsp + 0)->textintval.value / 100L) % 100L;
          ((parser_control *)parm)->year.value =
              (yyvsp + 0)->textintval.value / 10000L;
          ((parser_control *)parm)->year.digits =
              (yyvsp + 0)->textintval.digits - 4UL;
        } else {
          (((parser_control *)parm)->times_seen)++;
          if ((yyvsp + 0)->textintval.digits <= 2UL) {
            ((parser_control *)parm)->hour = (yyvsp + 0)->textintval.value;
            ((parser_control *)parm)->minutes = 0L;
          } else {
            ((parser_control *)parm)->hour =
                (yyvsp + 0)->textintval.value / 100L;
            ((parser_control *)parm)->minutes =
                (yyvsp + 0)->textintval.value % 100L;
          }
          ((parser_control *)parm)->seconds.tv_sec = (__time_t)0;
          ((parser_control *)parm)->seconds.tv_nsec = (__syscall_slong_t)0;
          ((parser_control *)parm)->meridian = 2;
        }
      }
      goto switch_break;
    case_64: /* CIL Label */
      yyval.intval = 2L;
      goto switch_break;
    case_65: /* CIL Label */
      yyval.intval = (yyvsp + 0)->intval;
      goto switch_break;
    switch_break: /* CIL Label */;
    }
    yyvsp -= yylen;
    yyssp -= yylen;
    yyvsp++;
    *yyvsp = yyval;
    yyn = (int)yyr1[yyn];
    yystate = (int)((int const)yypgoto[yyn - 25] + (int const) * yyssp);
    if (yystate >= 0) {
      if (yystate <= 70) {
        if ((int const)yycheck[yystate] == (int const) * yyssp) {
          yystate = (int)yytable[yystate];
        } else {
          yystate = (int)yydefgoto[yyn - 25];
        }
      } else {
        yystate = (int)yydefgoto[yyn - 25];
      }
    } else {
      yystate = (int)yydefgoto[yyn - 25];
    }
    goto yynewstate;
  yyerrlab:
    if (!yyerrstatus) {
       

        yynerrs++;
        yyerror((char *)"parse error");
       

    }
    goto yyerrlab1;
  yyerrlab1:
    if (yyerrstatus == 3) {
      if (yychar == 0) {
        goto yyabortlab;
      }
      yychar = -2;
    }
    yyerrstatus = 3;
    goto yyerrhandle;
  yyerrdefault:
    if ((unsigned long)yyssp == (unsigned long)yyss) {
      goto yyabortlab;
    }
    yyvsp--;
    yyssp--;
    yystate = (int)*yyssp;
  yyerrhandle:
    yyn = (int)yypact[yystate];
    if (yyn == -32768) {
      goto yyerrdefault;
    }
    yyn++;
    if (yyn < 0) {
      goto yyerrdefault;
    } else {
      if (yyn > 70) {
        goto yyerrdefault;
      } else {
        if ((int const)yycheck[yyn] != 1) {
          goto yyerrdefault;
        }
      }
    }
    yyn = (int)yytable[yyn];
    if (yyn < 0) {
      if (yyn == -32768) {
        goto yyerrdefault;
      }
      yyn = -yyn;
      goto yyreduce;
    } else {
      if (yyn == 0) {
        goto yyerrdefault;
      }
    }
    if (yyn == 80) {
      goto yyacceptlab;
    }
    yyvsp++;
    *yyvsp = yylval;
    yystate = yyn;
    goto yynewstate;
  yyacceptlab:
    if (yyfree_stacks) {
       

        free((void *)yyss);
        free((void *)yyvs);
       

    }
    return (0);
  yyabortlab:
    if (yyfree_stacks) {
       

        free((void *)yyss);
        free((void *)yyvs);
       

    }
    return (1);
  }
}
static table const meridian_table[5] = {{"AM", 264, 0},
                                        {"A.M.", 264, 0},
                                        {"PM", 264, 1},
                                        {"P.M.", 264, 1},
                                        {(char const *)0, 0, 0}};
static table const dst_table[1] = {{"DST", 258, 0}};
static table const month_and_day_table[25] = {
    {"JANUARY", 266, 1},    {"FEBRUARY", 266, 2}, {"MARCH", 266, 3},
    {"APRIL", 266, 4},      {"MAY", 266, 5},      {"JUNE", 266, 6},
    {"JULY", 266, 7},       {"AUGUST", 266, 8},   {"SEPTEMBER", 266, 9},
    {"SEPT", 266, 9},       {"OCTOBER", 266, 10}, {"NOVEMBER", 266, 11},
    {"DECEMBER", 266, 12},  {"SUNDAY", 259, 0},   {"MONDAY", 259, 1},
    {"TUESDAY", 259, 2},    {"TUES", 259, 2},     {"WEDNESDAY", 259, 3},
    {"WEDNES", 259, 3},     {"THURSDAY", 259, 4}, {"THUR", 259, 4},
    {"THURS", 259, 4},      {"FRIDAY", 259, 5},   {"SATURDAY", 259, 6},
    {(char const *)0, 0, 0}};
static table const time_units_table[11] = {
    {"YEAR", 269, 1},   {"MONTH", 267, 1},      {"FORTNIGHT", 260, 14},
    {"WEEK", 260, 7},   {"DAY", 260, 1},        {"HOUR", 262, 1},
    {"MINUTE", 265, 1}, {"MIN", 265, 1},        {"SECOND", 268, 1},
    {"SEC", 268, 1},    {(char const *)0, 0, 0}};
static table const relative_time_table[20] = {
    {"TOMORROW", 260, 1}, {"YESTERDAY", 260, -1}, {"TODAY", 260, 0},
    {"NOW", 260, 0},      {"LAST", 272, -1},      {"THIS", 272, 0},
    {"NEXT", 272, 1},     {"FIRST", 272, 1},      {"THIRD", 272, 3},
    {"FOURTH", 272, 4},   {"FIFTH", 272, 5},      {"SIXTH", 272, 6},
    {"SEVENTH", 272, 7},  {"EIGHTH", 272, 8},     {"NINTH", 272, 9},
    {"TENTH", 272, 10},   {"ELEVENTH", 272, 11},  {"TWELFTH", 272, 12},
    {"AGO", 257, 1},      {(char const *)0, 0, 0}};
static table const time_zone_table[51] = {
    {"GMT", 270, 0},     {"UT", 270, 0},     {"UTC", 270, 0},
    {"WET", 270, 0},     {"WEST", 261, 0},   {"BST", 261, 0},
    {"ART", 270, -180},  {"BRT", 270, -180}, {"BRST", 261, -180},
    {"NST", 270, -210},  {"NDT", 261, -210}, {"AST", 270, -240},
    {"ADT", 261, -240},  {"CLT", 270, -240}, {"CLST", 261, -240},
    {"EST", 270, -300},  {"EDT", 261, -300}, {"CST", 270, -360},
    {"CDT", 261, -360},  {"MST", 270, -420}, {"MDT", 261, -420},
    {"PST", 270, -480},  {"PDT", 261, -480}, {"AKST", 270, -540},
    {"AKDT", 261, -540}, {"HST", 270, -600}, {"HAST", 270, -600},
    {"HADT", 261, -600}, {"SST", 270, -720}, {"WAT", 270, 60},
    {"CET", 270, 60},    {"CEST", 261, 60},  {"MET", 270, 60},
    {"MEZ", 270, 60},    {"MEST", 261, 60},  {"MESZ", 261, 60},
    {"EET", 270, 120},   {"EEST", 261, 120}, {"CAT", 270, 120},
    {"SAST", 270, 120},  {"EAT", 270, 180},  {"MSK", 270, 180},
    {"MSD", 261, 180},   {"IST", 270, 330},  {"SGT", 270, 480},
    {"KST", 270, 540},   {"JST", 270, 540},  {"GST", 270, 600},
    {"NZST", 270, 720},  {"NZDT", 261, 720}, {(char const *)0, 0, 0}};
static table const military_table[26] = {
    {"A", 270, -60},  {"B", 270, -120},       {"C", 270, -180},
    {"D", 270, -240}, {"E", 270, -300},       {"F", 270, -360},
    {"G", 270, -420}, {"H", 270, -480},       {"I", 270, -540},
    {"K", 270, -600}, {"L", 270, -660},       {"M", 270, -720},
    {"N", 270, 60},   {"O", 270, 120},        {"P", 270, 180},
    {"Q", 270, 240},  {"R", 270, 300},        {"S", 270, 360},
    {"T", 270, 420},  {"U", 270, 480},        {"V", 270, 540},
    {"W", 270, 600},  {"X", 270, 660},        {"Y", 270, 720},
    {"Z", 270, 0},    {(char const *)0, 0, 0}};
static int to_hour(long hours, int meridian) {
  long tmp;
  int tmp___0;
  long tmp___1;
  int tmp___2;
  long tmp___3;

  {
     

      if (meridian == 2) {
        goto case_2;
      }
      if (meridian == 0) {
        goto case_0;
      }
      if (meridian == 1) {
        goto case_1;
      }
      goto switch_default;
    case_2: /* CIL Label */
      if (0L <= hours) {
        if (hours < 24L) {
          tmp = hours;
        } else {
          tmp = -1L;
        }
      } else {
        tmp = -1L;
      }
      return ((int)tmp);
    case_0: /* CIL Label */
      if (0L < hours) {
        if (hours < 12L) {
          tmp___1 = hours;
        } else {
          goto _L___3;
        }
      } else {
      _L___3: /* CIL Label */
        if (hours == 12L) {
          tmp___0 = 0;
        } else {
          tmp___0 = -1;
        }
        tmp___1 = (long)tmp___0;
      }
      return ((int)tmp___1);
    case_1: /* CIL Label */
      if (0L < hours) {
        if (hours < 12L) {
          tmp___3 = hours + 12L;
        } else {
          goto _L___4;
        }
      } else {
      _L___4: /* CIL Label */
        if (hours == 12L) {
          tmp___2 = 12;
        } else {
          tmp___2 = -1;
        }
        tmp___3 = (long)tmp___2;
      }
      return ((int)tmp___3);
    switch_default : /* CIL Label */
    {
      abort();
    }
                                 ;
     

  }
}
static long to_year(textint textyear) {
  long year;
  int tmp;

  {
    year = textyear.value;
    if (year < 0L) {
      year = -year;
    } else {
      if (textyear.digits == 2UL) {
        if (year < 69L) {
          tmp = 2000;
        } else {
          tmp = 1900;
        }
        year += (long)tmp;
      }
    }
    return (year);
  }
}
static table const *lookup_zone(parser_control const *pc, char const *name) {
  table const *tp;
  int tmp;
  int tmp___0;

  {
    tp = (table const *)(pc->local_time_zone_table);
    {
      while (1) {
                                     ;
        if (!tp->name) {
          goto while_break;
        }
        {
	  tmp = strcmp(name, (char const *)tp->name);
	}
        if (tmp == 0) {
          return (tp);
        }
        tp++;
      }
    while_break: /* CIL Label */;
    }
    tp = time_zone_table;
    {
      while (1) {
                                         ;
        if (!tp->name) {
          goto while_break___0;
        }
        {
	  tmp___0 = strcmp(name, (char const *)tp->name);
	}
        if (tmp___0 == 0) {
          return (tp);
        }
        tp++;
      }
    while_break___0: /* CIL Label */;
    }
    return ((table const *)0);
  }
}
static table const *lookup_word(parser_control const *pc, char *word) {
  char *p;
  char *q;
  size_t wordlen;
  table const *tp;
  _Bool period_found;
  _Bool abbrev;
  int tmp;
  unsigned short const **tmp___0;
  int tmp___1;
  int tmp___2;
  int tmp___3;
  int tmp___4;
  int tmp___5;
  int tmp___6;
  int tmp___7;
  int tmp___8;
  char tmp___9;
  int tmp___10;

  {
    p = word;
    {
      while (1) {
                                     ;
        if (!*p) {
          goto while_break;
        }
        {
	  tmp___0 = __ctype_b_loc();
	}
        if ((int const) * (*tmp___0 + (int)((unsigned char)*p)) & 512) {
           

            tmp = toupper((int)((unsigned char)*p));
            *p = (char)tmp;
           

        }
        p++;
      }
    while_break: /* CIL Label */;
    }
    tp = meridian_table;
    {
      while (1) {
                                         ;
        if (!tp->name) {
          goto while_break___0;
        }
        {
	  tmp___1 = strcmp((char const *)word, (char const *)tp->name);
	}
        if (tmp___1 == 0) {
          return (tp);
        }
        tp++;
      }
    while_break___0: /* CIL Label */;
    }
    {
      wordlen = strlen((char const *)word);
    }
    if (wordlen == 3UL) {
      tmp___10 = 1;
    } else {
      if (wordlen == 4UL) {
        if ((int)*(word + 3) == 46) {
          tmp___10 = 1;
        } else {
          tmp___10 = 0;
        }
      } else {
        tmp___10 = 0;
      }
    }
    abbrev = (_Bool)tmp___10;
    tp = month_and_day_table;
    {
      while (1) {
                                         ;
        if (!tp->name) {
          goto while_break___1;
        }
        if (abbrev) {
           

            tmp___2 =
                strncmp((char const *)word, (char const *)tp->name, (size_t)3);
            tmp___4 = tmp___2;
           

        } else {
           

            tmp___3 = strcmp((char const *)word, (char const *)tp->name);
            tmp___4 = tmp___3;
           

        }
        if (tmp___4 == 0) {
          return (tp);
        }
        tp++;
      }
    while_break___1: /* CIL Label */;
    }
    {
      tp = lookup_zone(pc, (char const *)word);
    }
    if (tp) {
      return (tp);
    }
    {
      tmp___5 = strcmp((char const *)word, (char const *)dst_table[0].name);
    }
    if (tmp___5 == 0) {
      return (dst_table);
    }
    tp = time_units_table;
    {
      while (1) {
                                         ;
        if (!tp->name) {
          goto while_break___2;
        }
        {
	  tmp___6 = strcmp((char const *)word, (char const *)tp->name);
	}
        if (tmp___6 == 0) {
          return (tp);
        }
        tp++;
      }
    while_break___2: /* CIL Label */;
    }
    if ((int)*(word + (wordlen - 1UL)) == 83) {
      *(word + (wordlen - 1UL)) = (char)'\000';
      tp = time_units_table;
      {
        while (1) {
                                           ;
          if (!tp->name) {
            goto while_break___3;
          }
          {
	    tmp___7 = strcmp((char const *)word, (char const *)tp->name);
	  }
          if (tmp___7 == 0) {
            return (tp);
          }
          tp++;
        }
      while_break___3: /* CIL Label */;
      }
      *(word + (wordlen - 1UL)) = (char)'S';
    }
    tp = relative_time_table;
    {
      while (1) {
                                         ;
        if (!tp->name) {
          goto while_break___4;
        }
        {
	  tmp___8 = strcmp((char const *)word, (char const *)tp->name);
	}
        if (tmp___8 == 0) {
          return (tp);
        }
        tp++;
      }
    while_break___4: /* CIL Label */;
    }
    if (wordlen == 1UL) {
      tp = military_table;
      {
        while (1) {
                                           ;
          if (!tp->name) {
            goto while_break___5;
          }
          if ((int)*(word + 0) == (int)*(tp->name + 0)) {
            return (tp);
          }
          tp++;
        }
      while_break___5: /* CIL Label */;
      }
    }
    period_found = (_Bool)0;
    q = word;
    p = q;
    {
      while (1) {
                                         ;
        tmp___9 = *q;
        *p = tmp___9;
        if (!tmp___9) {
          goto while_break___6;
        }
        if ((int)*q == 46) {
          period_found = (_Bool)1;
        } else {
          p++;
        }
        q++;
      }
    while_break___6: /* CIL Label */;
    }
    if (period_found) {
      {
        tp = lookup_zone(pc, (char const *)word);
      }
      if (tp) {
        return (tp);
      }
    }
    return ((table const *)0);
  }
}
static int yylex(YYSTYPE *lvalp, parser_control *pc) {
  unsigned char c;
  size_t count;
  unsigned short const **tmp;
  char const *p;
  int sign;
  unsigned long value;
  unsigned long value1;
  time_t s;
  int ns;
  int digits;
  unsigned long value1___0;
  char const *tmp___0;
  char const *tmp___1;
  char buff[20];
  char *p___0;
  table const *tp;
  char *tmp___2;
  unsigned short const **tmp___3;
  unsigned short const **tmp___4;
  char const *tmp___5;
  char const *tmp___6;
  int tmp___7;
  int tmp___8;
                    

  {
     

      while (1) {
                                     ;
        {
          while (1) {
                                             ;
            {
              c = (unsigned char)*(pc->input);
              tmp = __ctype_b_loc();
            }
            if (!(((int const) * (*tmp + (int)c) & 8192) != 0)) {
              goto while_break___0;
            }
            (pc->input)++;
          }
        while_break___0: /* CIL Label */;
        }
        if ((unsigned int)c - 48U <= 9U) {
          goto _L___6;
        } else {
          if ((int)c == 45) {
            goto _L___6;
          } else {
            if ((int)c == 43) {
            _L___6: /* CIL Label */
              if ((int)c == 45) {
                goto _L___3;
              } else {
                if ((int)c == 43) {
                _L___3: /* CIL Label */
                  if ((int)c == 45) {
                    sign = -1;
                  } else {
                    sign = 1;
                  }
                  (pc->input)++;
                  c = (unsigned char)*(pc->input);
                  if (!((unsigned int)c - 48U <= 9U)) {
                    goto __Cont;
                  }
                } else {
                  sign = 0;
                }
              }
              p = pc->input;
              value = 0UL;
              {
                while (1) {
                                                   ;
                  value1 = value + (unsigned long)((int)c - 48);
                  if (value1 < value) {
                    return ('?');
                  }
                  value = value1;
                  p++;
                  c = (unsigned char)*p;
                  if (!((unsigned int)c - 48U <= 9U)) {
                    goto while_break___1;
                  }
                  if (1844674407370955161UL < value) {
                    return ('?');
                  }
                  value *= 10UL;
                }
              while_break___1: /* CIL Label */;
              }
              if ((int)c == 46) {
                goto _L___5;
              } else {
                if ((int)c == 44) {
                _L___5: /* CIL Label */
                  if ((unsigned int)*(p + 1) - 48U <= 9U) {
                    if (sign < 0) {
                      s = (time_t)(-value);
                      if (0L < s) {
                        return ('?');
                      }
                      value1___0 = (unsigned long)(-s);
                    } else {
                      s = (time_t)value;
                      if (s < 0L) {
                        return ('?');
                      }
                      value1___0 = (unsigned long)s;
                    }
                    if (value != value1___0) {
                      return ('?');
                    }
                    p++;
                    tmp___0 = p;
                    p++;
                    ns = (int)((int const) * tmp___0 - 48);
                    digits = 2;
                    {
                      while (1) {
                                                         ;
                        if (!(digits <= 9)) {
                          goto while_break___2;
                        }
                        ns *= 10;
                        if ((unsigned int)*p - 48U <= 9U) {
                          tmp___1 = p;
                          p++;
                          ns += (int)((int const) * tmp___1 - 48);
                        }
                        digits++;
                      }
                    while_break___2: /* CIL Label */;
                    }
                    if (sign < 0) {
                       

                        while (1) {
                                                           ;
                          if (!((unsigned int)*p - 48U <= 9U)) {
                            goto while_break___3;
                          }
                          if ((int const) * p != 48) {
                            ns++;
                            goto while_break___3;
                          }
                          p++;
                        }
                      while_break___3: /* CIL Label */;
                       

                    }
                    {
                      while (1) {
                                                         ;
                        if (!((unsigned int)*p - 48U <= 9U)) {
                          goto while_break___4;
                        }
                        p++;
                      }
                    while_break___4: /* CIL Label */;
                    }
                    if (sign < 0) {
                      if (ns) {
                        s--;
                        if (!(s < 0L)) {
                          return ('?');
                        }
                        ns = 1000000000 - ns;
                      }
                    }
                    lvalp->timespec.tv_sec = s;
                    lvalp->timespec.tv_nsec = (__syscall_slong_t)ns;
                    pc->input = p;
                    if (sign) {
                      tmp___7 = 273;
                    } else {
                      tmp___7 = 274;
                    }
                    return (tmp___7);
                  } else {
                    goto _L___4;
                  }
                } else {
                _L___4: /* CIL Label */
                  if (sign < 0) {
                    lvalp->textintval.value = (long)(-value);
                    if (0L < lvalp->textintval.value) {
                      return ('?');
                    }
                  } else {
                    lvalp->textintval.value = (long)value;
                    if (lvalp->textintval.value < 0L) {
                      return ('?');
                    }
                  }
                  lvalp->textintval.digits = (size_t)(p - pc->input);
                  pc->input = p;
                  if (sign) {
                    tmp___8 = 271;
                  } else {
                    tmp___8 = 272;
                  }
                  return (tmp___8);
                }
              }
            }
          }
        }
        {
	  tmp___4 = __ctype_b_loc();
	}
        if ((int const) * (*tmp___4 + (int)c) & 1024) {
          p___0 = buff;
          {
            while (1) {
                                               ;
              if ((unsigned long)p___0 <
                  (unsigned long)((buff + sizeof(buff)) - 1)) {
                tmp___2 = p___0;
                p___0++;
                *tmp___2 = (char)c;
              }
              {
                (pc->input)++;
                c = (unsigned char)*(pc->input);
                tmp___3 = __ctype_b_loc();
              }
              if (!((int const) * (*tmp___3 + (int)c) & 1024)) {
                if (!((int)c == 46)) {
                  goto while_break___5;
                }
              }
            }
          while_break___5: /* CIL Label */;
          }
          {
            *p___0 = (char)'\000';
            tp = lookup_word((parser_control const *)pc, buff);
          }
          if (!tp) {
            return ('?');
          }
          lvalp->intval = (long)tp->value;
          return ((int)tp->type);
        }
        if ((int)c != 40) {
          tmp___5 = pc->input;
          (pc->input)++;
          return ((int)*tmp___5);
        }
        count = (size_t)0;
        {
          while (1) {
                                             ;
            tmp___6 = pc->input;
            (pc->input)++;
            c = (unsigned char)*tmp___6;
            if ((int)c == 0) {
              return ((int)c);
            }
            if ((int)c == 40) {
              count++;
            } else {
              if ((int)c == 41) {
                count--;
              }
            }
            if (!(count != 0UL)) {
              goto while_break___6;
            }
          }
        while_break___6: /* CIL Label */;
        }
      __Cont:;
      }
                                ;
     

  }
}
static int yyerror(char *s __attribute__((__unused__))) {

   

    return (0);
   

}
_Bool get_date(struct timespec *result, char const *p,
               struct timespec const *now) {
  time_t Start;
  long Start_ns;
  struct tm const *tmp;
  struct tm tm;
  struct tm tm0;
  parser_control pc;
  struct timespec gettime_buffer;
  int tmp___0;
  struct tm *tmp___1;
  int quarter;
  time_t probe;
  struct tm const *probe_tm;
  struct tm *tmp___2;
  int tmp___3;
  int tmp___4;
  long tmp___5;
  long delta;
  time_t t1;
  long sum_ns;
  long normalized_ns;
  time_t t0;
  long d1;
  time_t t1___0;
  long d2;
  time_t t2;
  long d3;
  time_t t3;
  long d4;
  time_t t4;
                    
                  

  {
    if (!now) {
      {
        tmp___0 = gettime(&gettime_buffer);
      }
      if (tmp___0 != 0) {
        return ((_Bool)0);
      }
      now = (struct timespec const *)(&gettime_buffer);
    }
    {
      Start = (time_t)now->tv_sec;
      Start_ns = (long)now->tv_nsec;
      tmp___1 = localtime(&now->tv_sec);
      tmp = (struct tm const *)tmp___1;
    }
    if (!tmp) {
      return ((_Bool)0);
    }
    pc.input = p;
    pc.year.value = (long)tmp->tm_year;
    pc.year.value += 1900L;
    pc.year.digits = (size_t)4;
    pc.month = (long)(tmp->tm_mon + 1);
    pc.day = (long)tmp->tm_mday;
    pc.hour = (long)tmp->tm_hour;
    pc.minutes = (long)tmp->tm_min;
    pc.seconds.tv_sec = (__time_t)tmp->tm_sec;
    pc.seconds.tv_nsec = Start_ns;
    tm.tm_isdst = (int)tmp->tm_isdst;
    pc.meridian = 2;
    pc.rel_ns = 0L;
    pc.rel_seconds = 0L;
    pc.rel_minutes = 0L;
    pc.rel_hour = 0L;
    pc.rel_day = 0L;
    pc.rel_month = 0L;
    pc.rel_year = 0L;
    pc.timespec_seen = (_Bool)0;
    pc.dates_seen = (size_t)0;
    pc.days_seen = (size_t)0;
    pc.rels_seen = (size_t)0;
    pc.times_seen = (size_t)0;
    pc.local_zones_seen = (size_t)0;
    pc.zones_seen = (size_t)0;
    pc.local_time_zone_table[0].name = (char const *)tmp->tm_zone;
    pc.local_time_zone_table[0].type = 263;
    pc.local_time_zone_table[0].value = (int)tmp->tm_isdst;
    pc.local_time_zone_table[1].name = (char const *)0;
    quarter = 1;
    {
      while (1) {
                                     ;
        if (!(quarter <= 3)) {
          goto while_break;
        }
        {
          probe = Start + (time_t)(quarter * 7776000);
          tmp___2 = localtime((time_t const *)(&probe));
          probe_tm = (struct tm const *)tmp___2;
        }
        if (probe_tm) {
          if (probe_tm->tm_zone) {
            if (probe_tm->tm_isdst !=
                (int const)pc.local_time_zone_table[0].value) {
              pc.local_time_zone_table[1].name =
                  (char const *)probe_tm->tm_zone;
              pc.local_time_zone_table[1].type = 263;
              pc.local_time_zone_table[1].value = (int)probe_tm->tm_isdst;
              pc.local_time_zone_table[2].name = (char const *)0;
              goto while_break;
            }
          }
        }
        quarter++;
      }
    while_break: /* CIL Label */;
    }
    if (pc.local_time_zone_table[0].name) {
      if (pc.local_time_zone_table[1].name) {
        {
          tmp___3 = strcmp(pc.local_time_zone_table[0].name,
                           pc.local_time_zone_table[1].name);
        }
        if (!tmp___3) {
          pc.local_time_zone_table[0].value = -1;
          pc.local_time_zone_table[1].name = (char const *)0;
        }
      }
    }
    {
      tmp___4 = yyparse((void *)(&pc));
    }
    if (tmp___4 != 0) {
      return ((_Bool)0);
    }
    if (pc.timespec_seen) {
      *result = pc.seconds;
      return ((_Bool)1);
    }
    if (1UL < pc.times_seen) {
      return ((_Bool)0);
    } else {
      if (1UL < pc.dates_seen) {
        return ((_Bool)0);
      } else {
        if (1UL < pc.days_seen) {
          return ((_Bool)0);
        } else {
          if (1UL < pc.local_zones_seen + pc.zones_seen) {
            return ((_Bool)0);
          } else {
            if (pc.local_zones_seen) {
              if (1 < pc.local_isdst) {
                return ((_Bool)0);
              }
            }
          }
        }
      }
    }
    {
      tmp___5 = to_year(pc.year);
      tm.tm_year = (int)((tmp___5 - 1900L) + pc.rel_year);
      tm.tm_mon = (int)((pc.month - 1L) + pc.rel_month);
      tm.tm_mday = (int)(pc.day + pc.rel_day);
    }
    if (pc.times_seen) {
      goto _L___3;
    } else {
      if (pc.rels_seen) {
        if (!pc.dates_seen) {
          if (!pc.days_seen) {
          _L___3 : /* CIL Label */
          {
            tm.tm_hour = to_hour(pc.hour, pc.meridian);
          }
            if (tm.tm_hour < 0) {
              return ((_Bool)0);
            }
            tm.tm_min = (int)pc.minutes;
            tm.tm_sec = (int)pc.seconds.tv_sec;
          } else {
            tm.tm_sec = 0;
            tm.tm_min = tm.tm_sec;
            tm.tm_hour = tm.tm_min;
            pc.seconds.tv_nsec = (__syscall_slong_t)0;
          }
        } else {
          tm.tm_sec = 0;
          tm.tm_min = tm.tm_sec;
          tm.tm_hour = tm.tm_min;
          pc.seconds.tv_nsec = (__syscall_slong_t)0;
        }
      } else {
        tm.tm_sec = 0;
        tm.tm_min = tm.tm_sec;
        tm.tm_hour = tm.tm_min;
        pc.seconds.tv_nsec = (__syscall_slong_t)0;
      }
    }
    if (((((pc.dates_seen | pc.days_seen) | pc.times_seen) |
          (unsigned long)pc.rel_day) |
         (unsigned long)pc.rel_month) |
        (unsigned long)pc.rel_year) {
      tm.tm_isdst = -1;
    }
    if (pc.local_zones_seen) {
      tm.tm_isdst = pc.local_isdst;
    }
    {
      tm0 = tm;
      Start = rpl_mktime(&tm);
    }
    if (Start == -1L) {
      if (pc.zones_seen) {
        tm = tm0;
        if (tm.tm_year <= 70) {
          (tm.tm_mday)++;
          pc.time_zone += 1440L;
        } else {
          (tm.tm_mday)--;
          pc.time_zone -= 1440L;
        }
        {
	  Start = rpl_mktime(&tm);
	}
      }
      if (Start == -1L) {
        return ((_Bool)0);
      }
    }
    if (pc.days_seen) {
      if (!pc.dates_seen) {
        {
          tm.tm_mday =
              (int)((long)tm.tm_mday +
                    ((long)(((pc.day_number - tm.tm_wday) + 7) % 7) +
                     7L * (pc.day_ordinal - (long)(0L < pc.day_ordinal))));
          tm.tm_isdst = -1;
          Start = rpl_mktime(&tm);
        }
        if (Start == -1L) {
          return ((_Bool)0);
        }
      }
    }
    if (pc.zones_seen) {
      delta = pc.time_zone * 60L;
      delta -= tm.tm_gmtoff;
      t1 = Start - delta;
      if ((Start < t1) != (delta < 0L)) {
        return ((_Bool)0);
      }
      Start = t1;
    }
    sum_ns = pc.seconds.tv_nsec + pc.rel_ns;
    normalized_ns = (sum_ns % 1000000000L + 1000000000L) % 1000000000L;
    t0 = Start;
    d1 = 3600L * pc.rel_hour;
    t1___0 = t0 + d1;
    d2 = 60L * pc.rel_minutes;
    t2 = t1___0 + d2;
    d3 = pc.rel_seconds;
    t3 = t2 + d3;
    d4 = (sum_ns - normalized_ns) / 1000000000L;
    t4 = t3 + d4;
    if ((((((d1 / 3600L ^ pc.rel_hour) | (d2 / 60L ^ pc.rel_minutes)) |
           (long)((t1___0 < t0) ^ (d1 < 0L))) |
          (long)((t2 < t1___0) ^ (d2 < 0L))) |
         (long)((t3 < t2) ^ (d3 < 0L))) |
        (long)((t4 < t3) ^ (d4 < 0L))) {
      return ((_Bool)0);
    }
    result->tv_sec = t4;
    result->tv_nsec = normalized_ns;
    return ((_Bool)1);
  }
}
/* #pragma merger("0","01a.getopt1.o.i","") */
/* #pragma merger("0","01b.getopt.o.i","") */
/* #pragma merger("0","01c.gettime.o.i","") */
extern __attribute__((__nothrow__)) int(
    __attribute__((__nonnull__(1), __leaf__))
    gettimeofday)(struct timeval *__restrict __tv, __timezone_ptr_t __tz);
int gettime(struct timespec *ts) {
  int tmp;
  struct timeval tv;
  int r;
  int tmp___0;

  {
    {
      tmp = clock_gettime(0, ts);
    }
    if (tmp == 0) {
      return (0);
    }
    {
      tmp___0 = gettimeofday((struct timeval * /* __restrict  */)(&tv),
                             (__timezone_ptr_t)0);
      r = tmp___0;
    }
    if (r == 0) {
      ts->tv_sec = tv.tv_sec;
      ts->tv_nsec = tv.tv_usec * 1000L;
    }
    return (r);
  }
}
/* #pragma merger("0","01d.hash.o.i","") */
_Bool hash_rehash(Hash_table *table___0, size_t candidate);
static struct hash_tuning const default_tuning = {
    (float)0.0, (float)1.0, (float)0.8, (float)1.414, (_Bool)0};
void *hash_lookup(Hash_table const *table___0, void const *entry) {
  struct hash_entry const *bucket;
  size_t tmp;
  struct hash_entry const *cursor;
  _Bool tmp___0;

  {
    {
      tmp = (*(table___0->hasher))(entry, (size_t)table___0->n_buckets);
      bucket = (struct hash_entry const *)(table___0->bucket + tmp);
    }
    if (!((unsigned long)bucket < (unsigned long)table___0->bucket_limit)) {
       

        abort();
       

    }
    if ((unsigned long)bucket->data == (unsigned long)((void *)0)) {
      return ((void *)0);
    }
    cursor = bucket;
    {
      while (1) {
                                     ;
        if (!cursor) {
          goto while_break;
        }
        {
          tmp___0 =
              (*(table___0->comparator))(entry, (void const *)cursor->data);
        }
        if (tmp___0) {
          return ((void *)cursor->data);
        }
        cursor = (struct hash_entry const *)cursor->next;
      }
    while_break: /* CIL Label */;
    }
    return ((void *)0);
  }
}
void *hash_get_first(Hash_table const *table___0) {
  struct hash_entry const *bucket;

  {
    if (table___0->n_entries == 0UL) {
      return ((void *)0);
    }
    bucket = (struct hash_entry const *)table___0->bucket;
    {
      while (1) {
                                     ;
        if (!((unsigned long)bucket < (unsigned long)table___0->bucket_limit)) {
           

            abort();
           

        } else {
          if (bucket->data) {
            return ((void *)bucket->data);
          }
        }
        bucket++;
      }
                                ;
    }
  }
}
void *hash_get_next(Hash_table const *table___0, void const *entry) {
  struct hash_entry const *bucket;
  size_t tmp;
  struct hash_entry const *cursor;

  {
    {
      tmp = (*(table___0->hasher))(entry, (size_t)table___0->n_buckets);
      bucket = (struct hash_entry const *)(table___0->bucket + tmp);
    }
    if (!((unsigned long)bucket < (unsigned long)table___0->bucket_limit)) {
       

        abort();
       

    }
    cursor = bucket;
    {
      while (1) {
                                     ;
        if (!cursor) {
          goto while_break;
        }
        if ((unsigned long)cursor->data == (unsigned long)entry) {
          if (cursor->next) {
            return ((cursor->next)->data);
          }
        }
        cursor = (struct hash_entry const *)cursor->next;
      }
    while_break: /* CIL Label */;
    }
    {
      while (1) {
                                         ;
        bucket++;
        if (!((unsigned long)bucket < (unsigned long)table___0->bucket_limit)) {
          goto while_break___0;
        }
        if (bucket->data) {
          return ((void *)bucket->data);
        }
      }
    while_break___0: /* CIL Label */;
    }
    return ((void *)0);
  }
}
size_t hash_do_for_each(Hash_table const *table___0,
                        _Bool (*processor)(void *, void *),
                        void *processor_data) {
  size_t counter;
  struct hash_entry const *bucket;
  struct hash_entry const *cursor;
  _Bool tmp;

  {
    counter = (size_t)0;
    bucket = (struct hash_entry const *)table___0->bucket;
    {
      while (1) {
                                     ;
        if (!((unsigned long)bucket < (unsigned long)table___0->bucket_limit)) {
          goto while_break;
        }
        if (bucket->data) {
          cursor = bucket;
          {
            while (1) {
                                               ;
              if (!cursor) {
                goto while_break___0;
              }
              {
		tmp = (*processor)((void *)cursor->data, processor_data);
	      }
              if (!tmp) {
                return (counter);
              }
              counter++;
              cursor = (struct hash_entry const *)cursor->next;
            }
          while_break___0: /* CIL Label */;
          }
        }
        bucket++;
      }
    while_break: /* CIL Label */;
    }
    return (counter);
  }
}
size_t hash_string(char const *string, size_t n_buckets) {
  size_t value;
  char const *tmp;

  {
    value = (size_t)0;
    {
      while (1) {
                                     ;
        if (!*string) {
          goto while_break;
        }
        tmp = string;
        string++;
        value = (value * 31UL + (size_t)((unsigned char)*tmp)) % n_buckets;
      }
    while_break: /* CIL Label */;
    }
    return (value);
  }
}
static _Bool is_prime(size_t candidate) {
  size_t divisor;
  size_t square;
  int tmp;

  {
    divisor = (size_t)3;
    square = divisor * divisor;
    {
      while (1) {
                                     ;
        if (square < candidate) {
          if (!(candidate % divisor)) {
            goto while_break;
          }
        } else {
          goto while_break;
        }
        divisor++;
        square += 4UL * divisor;
        divisor++;
      }
    while_break: /* CIL Label */;
    }
    if (candidate % divisor) {
      tmp = 1;
    } else {
      tmp = 0;
    }
    return ((_Bool)tmp);
  }
}
static size_t next_prime(size_t candidate) {
  _Bool tmp;

  {
    if (candidate < 10UL) {
      candidate = (size_t)10;
    }
    candidate |= 1UL;
    {
      while (1) {
                                     ;
        {
	  tmp = is_prime(candidate);
	}
        if (tmp) {
          goto while_break;
        }
        candidate += 2UL;
      }
    while_break: /* CIL Label */;
    }
    return (candidate);
  }
}
static _Bool check_tuning(Hash_table *table___0) {
  Hash_tuning const *tuning;
  float epsilon;

  {
    tuning = table___0->tuning;
    epsilon = 0.1f;
    if (epsilon < (float)tuning->growth_threshold) {
      if (tuning->growth_threshold < (float const)((float)1 - epsilon)) {
        if ((float)1 + epsilon < (float)tuning->growth_factor) {
          if ((float const)0 <= tuning->shrink_threshold) {
            if (tuning->shrink_threshold + (float const)epsilon <
                tuning->shrink_factor) {
              if (tuning->shrink_factor <= (float const)1) {
                if (tuning->shrink_threshold + (float const)epsilon <
                    tuning->growth_threshold) {
                  return ((_Bool)1);
                }
              }
            }
          }
        }
      }
    }
    table___0->tuning = &default_tuning;
    return ((_Bool)0);
  }
}
Hash_table *hash_initialize(size_t candidate, Hash_tuning const *tuning,
                            size_t (*hasher)(void const *, size_t),
                            _Bool (*comparator)(void const *, void const *),
                            void (*data_freer)(void *)) {
  Hash_table *table___0;
  void *tmp;
  _Bool tmp___0;
  float new_candidate;
  void *tmp___1;
  int tmp___2;
  int tmp___3;

  {
    if ((unsigned long)hasher == (unsigned long)((void *)0)) {
      return ((Hash_table *)((void *)0));
    } else {
      if ((unsigned long)comparator == (unsigned long)((void *)0)) {
        return ((Hash_table *)((void *)0));
      }
    }
    {
      tmp = malloc(sizeof(*table___0));
      table___0 = (Hash_table *)tmp;
    }
    if ((unsigned long)table___0 == (unsigned long)((void *)0)) {
      return ((Hash_table *)((void *)0));
    }
    if (!tuning) {
      tuning = &default_tuning;
    }
    {
      table___0->tuning = tuning;
      tmp___0 = check_tuning(table___0);
    }
    if (!tmp___0) {
      goto fail;
    }
    if (!tuning->is_n_buckets) {
      new_candidate =
          (float)((float const)candidate / tuning->growth_threshold);
      if ((float)0xffffffffffffffffUL <= new_candidate) {
        goto fail;
      }
      candidate = (size_t)new_candidate;
    }
    if (sizeof(ptrdiff_t) <= sizeof(size_t)) {
      tmp___2 = -1;
    } else {
      tmp___2 = -2;
    }
    if ((size_t)tmp___2 / sizeof(*(table___0->bucket)) < candidate) {
      goto fail;
    }
    {
      table___0->n_buckets = next_prime(candidate);
    }
    if (sizeof(ptrdiff_t) <= sizeof(size_t)) {
      tmp___3 = -1;
    } else {
      tmp___3 = -2;
    }
    if ((size_t)tmp___3 / sizeof(*(table___0->bucket)) < table___0->n_buckets) {
      goto fail;
    }
    {
      tmp___1 = calloc(table___0->n_buckets, sizeof(*(table___0->bucket)));
      table___0->bucket = (struct hash_entry *)tmp___1;
      table___0->bucket_limit =
          (struct hash_entry const *)(table___0->bucket + table___0->n_buckets);
      table___0->n_buckets_used = (size_t)0;
      table___0->n_entries = (size_t)0;
      table___0->hasher = hasher;
      table___0->comparator = comparator;
      table___0->data_freer = data_freer;
      table___0->free_entry_list = (struct hash_entry *)((void *)0);
    }
    return (table___0);
  fail : {
      free((void *)table___0);
    }
    return ((Hash_table *)((void *)0));
  }
}
static struct hash_entry *allocate_entry(Hash_table *table___0) {
  struct hash_entry *new;
  void *tmp;

  {
    if (table___0->free_entry_list) {
      new = table___0->free_entry_list;
      table___0->free_entry_list = new->next;
    } else {
       

        tmp = malloc(sizeof(*new));
        new = (struct hash_entry *)tmp;
       

    }
    return (new);
  }
}
static void free_entry(Hash_table *table___0, struct hash_entry *entry) {

   

    entry->data = (void *)0;
    entry->next = table___0->free_entry_list;
    table___0->free_entry_list = entry;
    return;
   

}
static void *hash_find_entry(Hash_table *table___0, void const *entry,
                             struct hash_entry **bucket_head, _Bool delete) {
  struct hash_entry *bucket;
  size_t tmp;
  struct hash_entry *cursor;
  void *data;
  struct hash_entry *next;
  _Bool tmp___0;
  void *data___0;
  struct hash_entry *next___0;
  _Bool tmp___1;

  {
    {
      tmp = (*(table___0->hasher))(entry, table___0->n_buckets);
      bucket = table___0->bucket + tmp;
    }
    if (!((unsigned long)bucket < (unsigned long)table___0->bucket_limit)) {
       

        abort();
       

    }
    *bucket_head = bucket;
    if ((unsigned long)bucket->data == (unsigned long)((void *)0)) {
      return ((void *)0);
    }
    {
      tmp___0 = (*(table___0->comparator))(entry, (void const *)bucket->data);
    }
    if (tmp___0) {
      data = bucket->data;
      if (delete) {
        if (bucket->next) {
           

            next = bucket->next;
            *bucket = *next;
            free_entry(table___0, next);
           

        } else {
          bucket->data = (void *)0;
        }
      }
      return (data);
    }
    cursor = bucket;
    {
      while (1) {
                                     ;
        if (!cursor->next) {
          goto while_break;
        }
        {
          tmp___1 = (*(table___0->comparator))(
              entry, (void const *)(cursor->next)->data);
        }
        if (tmp___1) {
          data___0 = (cursor->next)->data;
          if (delete) {
             

              next___0 = cursor->next;
              cursor->next = next___0->next;
              free_entry(table___0, next___0);
             

          }
          return (data___0);
        }
        cursor = cursor->next;
      }
    while_break: /* CIL Label */;
    }
    return ((void *)0);
  }
}
_Bool hash_rehash(Hash_table *table___0, size_t candidate) {
  Hash_table *new_table;
  struct hash_entry *bucket;
  struct hash_entry *cursor;
  struct hash_entry *next;
  void *data;
  struct hash_entry *new_bucket;
  size_t tmp;
  struct hash_entry *new_entry;
  struct hash_entry *tmp___0;

  {
    {
      new_table =
          hash_initialize(candidate, table___0->tuning, table___0->hasher,
                          table___0->comparator, table___0->data_freer);
    }
    if ((unsigned long)new_table == (unsigned long)((void *)0)) {
      return ((_Bool)0);
    }
    new_table->free_entry_list = table___0->free_entry_list;
    bucket = table___0->bucket;
    {
      while (1) {
                                     ;
        if (!((unsigned long)bucket < (unsigned long)table___0->bucket_limit)) {
          goto while_break;
        }
        if (bucket->data) {
          cursor = bucket;
          {
            while (1) {
                                               ;
              if (!cursor) {
                goto while_break___0;
              }
              {
                data = cursor->data;
                tmp = (*(new_table->hasher))((void const *)data,
                                             new_table->n_buckets);
                new_bucket = new_table->bucket + tmp;
              }
              if (!((unsigned long)new_bucket <
                    (unsigned long)new_table->bucket_limit)) {
                 

                  abort();
                 

              }
              next = cursor->next;
              if (new_bucket->data) {
                if ((unsigned long)cursor == (unsigned long)bucket) {
                  {
                    tmp___0 = allocate_entry(new_table);
                    new_entry = tmp___0;
                  }
                  if ((unsigned long)new_entry == (unsigned long)((void *)0)) {
                    return ((_Bool)0);
                  }
                  new_entry->data = data;
                  new_entry->next = new_bucket->next;
                  new_bucket->next = new_entry;
                } else {
                  cursor->next = new_bucket->next;
                  new_bucket->next = cursor;
                }
              } else {
                new_bucket->data = data;
                (new_table->n_buckets_used)++;
                if ((unsigned long)cursor != (unsigned long)bucket) {
                   

                    free_entry(new_table, cursor);
                   

                }
              }
              cursor = next;
            }
          while_break___0: /* CIL Label */;
          }
        }
        bucket++;
      }
    while_break: /* CIL Label */;
    }
    {
      free((void *)table___0->bucket);
      table___0->bucket = new_table->bucket;
      table___0->bucket_limit = new_table->bucket_limit;
      table___0->n_buckets = new_table->n_buckets;
      table___0->n_buckets_used = new_table->n_buckets_used;
      table___0->free_entry_list = new_table->free_entry_list;
      free((void *)new_table);
    }
    return ((_Bool)1);
  }
}
void *hash_insert(Hash_table *table___0, void const *entry) {
  void *data;
  struct hash_entry *bucket;
  struct hash_entry *new_entry;
  struct hash_entry *tmp;
  Hash_tuning const *tuning;
  float candidate;
  _Bool tmp___0;
  float tmp___1;

  {
    if (!entry) {
       

        abort();
       

    }
    {
      data = hash_find_entry(table___0, entry, &bucket, (_Bool)0);
    }
    if ((unsigned long)data != (unsigned long)((void *)0)) {
      return (data);
    }
    if (bucket->data) {
      {
        tmp = allocate_entry(table___0);
        new_entry = tmp;
      }
      if ((unsigned long)new_entry == (unsigned long)((void *)0)) {
        return ((void *)0);
      }
      new_entry->data = (void *)entry;
      new_entry->next = bucket->next;
      bucket->next = new_entry;
      (table___0->n_entries)++;
      return ((void *)entry);
    }
    bucket->data = (void *)entry;
    (table___0->n_entries)++;
    (table___0->n_buckets_used)++;
    if ((float const)table___0->n_buckets_used >
        (table___0->tuning)->growth_threshold *
            (float const)table___0->n_buckets) {
      {
        check_tuning(table___0);
      }
      if ((float const)table___0->n_buckets_used >
          (table___0->tuning)->growth_threshold *
              (float const)table___0->n_buckets) {
        tuning = table___0->tuning;
        if (tuning->is_n_buckets) {
          tmp___1 = (float const)table___0->n_buckets * tuning->growth_factor;
        } else {
          tmp___1 =
              ((float const)table___0->n_buckets * tuning->growth_factor) *
              tuning->growth_threshold;
        }
        candidate = (float)tmp___1;
        if ((float)0xffffffffffffffffUL <= candidate) {
          return ((void *)0);
        }
        {
	  tmp___0 = hash_rehash(table___0, (size_t)candidate);
	}
        if (!tmp___0) {
          entry = (void const *)((void *)0);
        }
      }
    }
    return ((void *)entry);
  }
}
/* #pragma merger("0","01e.human.o.i","") */
extern __attribute__((__nothrow__)) struct lconv *(__attribute__((__leaf__))
                                                   localeconv)(void);
extern __attribute__((__nothrow__)) void *(
    __attribute__((__nonnull__(1, 2), __leaf__))
    memmove)(void *__dest, void const *__src, size_t __n);
static char const power_letter[9] = {
    (char const)0,   (char const)'K', (char const)'M',
    (char const)'G', (char const)'T', (char const)'P',
    (char const)'E', (char const)'Z', (char const)'Y'};
static long double adjust_value(int inexact_style, long double value) {
  uintmax_t u;
  int tmp;

  {
    if (inexact_style != 1) {
      if (value < (long double)0xffffffffffffffffUL) {
        u = (uintmax_t)value;
        if (inexact_style == 0) {
          if ((long double)u != value) {
            tmp = 1;
          } else {
            tmp = 0;
          }
        } else {
          tmp = 0;
        }
        value = (long double)(u + (uintmax_t)tmp);
      }
    }
    return (value);
  }
}
static char *group_number(char *number, size_t numberlen, char const *grouping,
                          char const *thousands_sep) {
  register char *d;
  size_t grouplen;
  size_t thousands_seplen;
  size_t tmp;
  size_t i;
  char buf[(((2UL * sizeof(uintmax_t)) * 8UL) * 302UL) / 1000UL + 1UL];
  unsigned char g;
                    

  {
    {
      grouplen = 0xffffffffffffffffUL;
      tmp = strlen(thousands_sep);
      thousands_seplen = tmp;
      i = numberlen;
      memcpy((void * /* __restrict  */)(buf),
             (void const * /* __restrict  */)number, numberlen);
      d = number + numberlen;
    }
    {
      while (1) {
                                     ;
        g = (unsigned char)*grouping;
        if (g) {
          if ((int)g < 127) {
            grouplen = (size_t)g;
          } else {
            grouplen = i;
          }
          grouping++;
        }
        if (i < grouplen) {
          grouplen = i;
        }
        {
          d -= grouplen;
          i -= grouplen;
          memcpy((void * /* __restrict  */)d,
                 (void const * /* __restrict  */)(buf + i), grouplen);
        }
        if (i == 0UL) {
          return (d);
        }
        {
          d -= thousands_seplen;
          memcpy((void * /* __restrict  */)d,
                 (void const * /* __restrict  */)thousands_sep,
                 thousands_seplen);
        }
      }
                                ;
    }
  }
}
char *human_readable(uintmax_t n, char *buf, int opts,
                     uintmax_t from_block_size, uintmax_t to_block_size) {
  int inexact_style;
  unsigned int base;
  uintmax_t amt;
  int tenths;
  int exponent;
  int exponent_max;
  char *p;
  char *psuffix;
  char const *integerlim;
  int rounding;
  char const *decimal_point;
  size_t decimal_pointlen;
  char const *grouping;
  char const *thousands_sep;
  struct lconv const *l;
  struct lconv *tmp;
  size_t pointlen;
  size_t tmp___0;
  size_t tmp___1;
  uintmax_t multiplier;
  uintmax_t divisor;
  uintmax_t r10;
  uintmax_t r2;
  long double dto_block_size;
  long double damt;
  size_t buflen;
  size_t nonintegerlen;
  long double tmp___2;
  long double e;
  long double tmp___3;
  long double tmp___4;
  unsigned int r10___0;
  unsigned int r2___0;
  int digit;
  uintmax_t power;
  char *tmp___5;
  char *tmp___6;
  char *tmp___7;
  int tmp___8;
  int tmp___9;
  int tmp___10;
  int tmp___11;
  int tmp___12;
  int tmp___13;
                    
                    
                    
                    
                    
                    
                    

  {
    inexact_style = opts & 3;
    if (opts & 32) {
      tmp___8 = 1024;
    } else {
      tmp___8 = 1000;
    }
    {
      base = (unsigned int)tmp___8;
      exponent = -1;
      exponent_max = (int)(sizeof(power_letter) - 1UL);
      decimal_point = ".";
      decimal_pointlen = (size_t)1;
      grouping = "";
      thousands_sep = "";
      tmp = localeconv();
      l = (struct lconv const *)tmp;
      tmp___0 = strlen((char const *)l->decimal_point);
      pointlen = tmp___0;
    }
    if (0UL < pointlen) {
      if (pointlen <= 16UL) {
        decimal_point = (char const *)l->decimal_point;
        decimal_pointlen = pointlen;
      }
    }
    {
      grouping = (char const *)l->grouping;
      tmp___1 = strlen((char const *)l->thousands_sep);
    }
    if (tmp___1 <= 16UL) {
      thousands_sep = (char const *)l->thousands_sep;
    }
    psuffix =
        (buf +
         ((((((2UL * sizeof(uintmax_t)) * 8UL) * 302UL) / 1000UL + 1UL) * 17UL -
           16UL) +
          3UL)) -
        3;
    p = psuffix;
    if (to_block_size <= from_block_size) {
      if (from_block_size % to_block_size == 0UL) {
        multiplier = from_block_size / to_block_size;
        amt = n * multiplier;
        if (amt / multiplier == n) {
          tenths = 0;
          rounding = 0;
          goto use_integer_arithmetic;
        }
      }
    } else {
      if (from_block_size != 0UL) {
        if (to_block_size % from_block_size == 0UL) {
          divisor = to_block_size / from_block_size;
          r10 = (n % divisor) * 10UL;
          r2 = (r10 % divisor) * 2UL;
          amt = n / divisor;
          tenths = (int)(r10 / divisor);
          if (r2 < divisor) {
            rounding = 0UL < r2;
          } else {
            rounding = 2 + (divisor < r2);
          }
          goto use_integer_arithmetic;
        }
      }
    }
    dto_block_size = (long double)to_block_size;
    damt = (long double)n * ((long double)from_block_size / dto_block_size);
    if (!(opts & 16)) {
       

        tmp___2 = adjust_value(inexact_style, damt);
        sprintf((char * /* __restrict  */)buf,
                (char const * /* __restrict  */) "%.0Lf", tmp___2);
        buflen = strlen((char const *)buf);
        nonintegerlen = (size_t)0;
       

    } else {
      e = (long double)1;
      exponent = 0;
      {
        while (1) {
                                       ;
          e *= (long double)base;
          exponent++;
          if (e * (long double)base <= damt) {
            if (!(exponent < exponent_max)) {
              goto while_break;
            }
          } else {
            goto while_break;
          }
        }
      while_break: /* CIL Label */;
      }
      {
        damt /= e;
        tmp___3 = adjust_value(inexact_style, damt);
        sprintf((char * /* __restrict  */)buf,
                (char const * /* __restrict  */) "%.1Lf", tmp___3);
        buflen = strlen((char const *)buf);
        nonintegerlen = decimal_pointlen + 1UL;
      }
      if ((1UL + nonintegerlen) + (size_t)(!(opts & 32)) < buflen) {
         

          tmp___4 = adjust_value(inexact_style, damt * (long double)10);
          sprintf((char * /* __restrict  */)buf,
                  (char const * /* __restrict  */) "%.0Lf",
                  tmp___4 / (long double)10);
          buflen = strlen((char const *)buf);
          nonintegerlen = (size_t)0;
         

      } else {
        if (opts & 8) {
          if ((int)*(buf + (buflen - 1UL)) == 48) {
             

              tmp___4 = adjust_value(inexact_style, damt * (long double)10);
              sprintf((char * /* __restrict  */)buf,
                      (char const * /* __restrict  */) "%.0Lf",
                      tmp___4 / (long double)10);
              buflen = strlen((char const *)buf);
              nonintegerlen = (size_t)0;
             

          }
        }
      }
    }
    {
      p = psuffix - buflen;
      memmove((void *)p, (void const *)buf, buflen);
      integerlim = (char const *)((p + buflen) - nonintegerlen);
    }
    goto do_grouping;
  use_integer_arithmetic:
    if (opts & 16) {
      exponent = 0;
      if ((uintmax_t)base <= amt) {
        {
          while (1) {
                                             ;
            r10___0 = (unsigned int)((amt % (unsigned long)base) * 10UL +
                                     (unsigned long)tenths);
            r2___0 = (r10___0 % base) * 2U + (unsigned int)(rounding >> 1);
            amt /= (uintmax_t)base;
            tenths = (int)(r10___0 / base);
            if (r2___0 < base) {
              rounding = r2___0 + (unsigned int)rounding != 0U;
            } else {
              rounding = 2 + (base < r2___0 + (unsigned int)rounding);
            }
            exponent++;
            if ((uintmax_t)base <= amt) {
              if (!(exponent < exponent_max)) {
                goto while_break___0;
              }
            } else {
              goto while_break___0;
            }
          }
        while_break___0: /* CIL Label */;
        }
        if (amt < 10UL) {
          if (inexact_style == 1) {
            tmp___10 = 2 < rounding + (tenths & 1);
          } else {
            if (inexact_style == 0) {
              if (0 < rounding) {
                tmp___9 = 1;
              } else {
                tmp___9 = 0;
              }
            } else {
              tmp___9 = 0;
            }
            tmp___10 = tmp___9;
          }
          if (tmp___10) {
            tenths++;
            rounding = 0;
            if (tenths == 10) {
              amt++;
              tenths = 0;
            }
          }
          if (amt < 10UL) {
            if (tenths) {
              goto _L___3;
            } else {
              if (!(opts & 8)) {
              _L___3 : /* CIL Label */
              {
                p--;
                *p = (char)(48 + tenths);
                p -= decimal_pointlen;
                memcpy((void * /* __restrict  */)p,
                       (void const * /* __restrict  */)decimal_point,
                       decimal_pointlen);
                rounding = 0;
                tenths = rounding;
              }
              }
            }
          }
        }
      }
    }
    if (inexact_style == 1) {
      tmp___12 = 5 < tenths + (0UL < (unsigned long)rounding + (amt & 1UL));
    } else {
      if (inexact_style == 0) {
        if (0 < tenths + rounding) {
          tmp___11 = 1;
        } else {
          tmp___11 = 0;
        }
      } else {
        tmp___11 = 0;
      }
      tmp___12 = tmp___11;
    }
    if (tmp___12) {
      amt++;
      if (opts & 16) {
        if (amt == (uintmax_t)base) {
          if (exponent < exponent_max) {
            exponent++;
            if (!(opts & 8)) {
               

                p--;
                *p = (char)'0';
                p -= decimal_pointlen;
                memcpy((void * /* __restrict  */)p,
                       (void const * /* __restrict  */)decimal_point,
                       decimal_pointlen);
               

            }
            amt = (uintmax_t)1;
          }
        }
      }
    }
    integerlim = (char const *)p;
    {
      while (1) {
                                         ;
        digit = (int)(amt % 10UL);
        p--;
        *p = (char)(digit + 48);
        amt /= 10UL;
        if (!(amt != 0UL)) {
          goto while_break___1;
        }
      }
    while_break___1: /* CIL Label */;
    }
  do_grouping:
    if (opts & 4) {
       

        p = group_number(p, (size_t)(integerlim - (char const *)p), grouping,
                         thousands_sep);
       

    }
    if (opts & 64) {
      if (exponent < 0) {
        exponent = 0;
        power = (uintmax_t)1;
        {
          while (1) {
                                             ;
            if (!(power < to_block_size)) {
              goto while_break___2;
            }
            exponent++;
            if (exponent == exponent_max) {
              goto while_break___2;
            }
            power *= (uintmax_t)base;
          }
        while_break___2: /* CIL Label */;
        }
      }
      if (exponent) {
        tmp___5 = psuffix;
        psuffix++;
        if (!(opts & 32)) {
          if (exponent == 1) {
            tmp___13 = 'k';
          } else {
            tmp___13 = (int)power_letter[exponent];
          }
        } else {
          tmp___13 = (int)power_letter[exponent];
        }
        *tmp___5 = (char)tmp___13;
      }
      if (opts & 128) {
        if (opts & 32) {
          if (exponent) {
            tmp___6 = psuffix;
            psuffix++;
            *tmp___6 = (char)'i';
          }
        }
        tmp___7 = psuffix;
        psuffix++;
        *tmp___7 = (char)'B';
      }
    }
    *psuffix = (char)'\000';
    return (p);
  }
}
                                               
                                                 
                                                                    
                                                                      
/* #pragma merger("0","01f.mktime.o.i","") */
extern __attribute__((__nothrow__)) struct tm *(
    __attribute__((__leaf__))
    localtime_r)(time_t const *__restrict __timer, struct tm *__restrict __tp);
__inline static int leapyear(int year) {
  int tmp;

  {
    if ((year & 3) == 0) {
      if (year % 100 != 0) {
        tmp = 1;
      } else {
        if ((year / 100 & 3) == 1) {
          tmp = 1;
        } else {
          tmp = 0;
        }
      }
    } else {
      tmp = 0;
    }
    return (tmp);
  }
}
static unsigned short const __mon_yday[2][13] = {
    {(unsigned short const)0, (unsigned short const)31,
     (unsigned short const)59, (unsigned short const)90,
     (unsigned short const)120, (unsigned short const)151,
     (unsigned short const)181, (unsigned short const)212,
     (unsigned short const)243, (unsigned short const)273,
     (unsigned short const)304, (unsigned short const)334,
     (unsigned short const)365},
    {(unsigned short const)0, (unsigned short const)31,
     (unsigned short const)60, (unsigned short const)91,
     (unsigned short const)121, (unsigned short const)152,
     (unsigned short const)182, (unsigned short const)213,
     (unsigned short const)244, (unsigned short const)274,
     (unsigned short const)305, (unsigned short const)335,
     (unsigned short const)366}};
__inline static time_t ydhms_diff(long year1, long yday1, int hour1, int min1,
                                  int sec1, int year0, int yday0, int hour0,
                                  int min0, int sec0) {
  int a4;
  int b4;
  int a100;
  int b100;
  int a400;
  int b400;
  int intervening_leap_days;
  time_t tyear1;
  time_t years;
  time_t days;
  time_t hours;
  time_t minutes;
  time_t seconds;

  {
    a4 = (int)(((year1 >> 2) + (long)(1900 >> 2)) - (long)(!(year1 & 3L)));
    b4 = ((year0 >> 2) + (1900 >> 2)) - !(year0 & 3);
    a100 = a4 / 25 - (a4 % 25 < 0);
    b100 = b4 / 25 - (b4 % 25 < 0);
    a400 = a100 >> 2;
    b400 = b100 >> 2;
    intervening_leap_days = ((a4 - b4) - (a100 - b100)) + (a400 - b400);
    tyear1 = year1;
    years = tyear1 - (time_t)year0;
    days = ((365L * years + yday1) - (time_t)yday0) +
           (time_t)intervening_leap_days;
    hours = (24L * days + (time_t)hour1) - (time_t)hour0;
    minutes = (60L * hours + (time_t)min1) - (time_t)min0;
    seconds = (60L * minutes + (time_t)sec1) - (time_t)sec0;
    return (seconds);
  }
}
static time_t guess_time_tm(long year, long yday, int hour, int min, int sec,
                            time_t const *t, struct tm const *tp) {
  time_t d;
  time_t tmp;
  time_t t1;
  long tmp___0;

  {
    if (tp) {
      {
        tmp = ydhms_diff(year, yday, hour, min, sec, (int)tp->tm_year,
                         (int)tp->tm_yday, (int)tp->tm_hour, (int)tp->tm_min,
                         (int)tp->tm_sec);
        d = tmp;
        t1 = (time_t)(*t + (time_t const)d);
      }
      if ((t1 < (time_t)*t) == (d < 0L)) {
        return (t1);
      }
    }
    if (*t < (time_t const)((((-1L << (sizeof(time_t) * 8UL - 1UL)) +
                              (-1L - (-1L << (sizeof(time_t) * 8UL - 1UL)))) >>
                             1) +
                            1L)) {
      tmp___0 =
          (-1L << (sizeof(time_t) * 8UL - 1UL)) +
          (time_t)(*t == (time_t const)(-1L << (sizeof(time_t) * 8UL - 1UL)));
    } else {
      tmp___0 =
          (-1L - (-1L << (sizeof(time_t) * 8UL - 1UL))) -
          (time_t)(*t ==
                   (time_t const)(-1L - (-1L << (sizeof(time_t) * 8UL - 1UL))));
    }
    return (tmp___0);
  }
}
static struct tm *ranged_convert(struct tm *(*convert)(time_t const *,
                                                       struct tm *),
                                 time_t *t, struct tm *tp) {
  struct tm *r;
  time_t bad;
  time_t ok;
  struct tm tm;
  time_t mid;
  time_t tmp;
  int tmp___0;

  {
    {
      r = (*convert)((time_t const *)t, tp);
    }
    if (!r) {
      if (*t) {
        bad = *t;
        ok = (time_t)0;
        {
          while (1) {
                                         ;
            if (bad < 0L) {
              tmp___0 = -1;
            } else {
              tmp___0 = 1;
            }
            if (!(bad != ok + (time_t)tmp___0)) {
              goto while_break;
            }
            if (bad < 0L) {
              tmp = bad + ((ok - bad) >> 1);
            } else {
              tmp = ok + ((bad - ok) >> 1);
            }
            {
              *t = tmp;
              mid = tmp;
              r = (*convert)((time_t const *)t, tp);
            }
            if (r) {
              tm = *r;
              ok = mid;
            } else {
              bad = mid;
            }
          }
        while_break: /* CIL Label */;
        }
        if (!r) {
          if (ok) {
            *t = ok;
            *tp = tm;
            r = tp;
          }
        }
      }
    }
    return (r);
  }
}
time_t mktime_internal(struct tm *tp,
                       struct tm *(*convert)(time_t const *, struct tm *),
                       time_t *offset) {
  time_t t;
  time_t gt;
  time_t t0;
  time_t t1;
  time_t t2;
  struct tm tm;
  int remaining_probes;
  int sec;
  int min;
  int hour;
  int mday;
  int mon;
  int year_requested;
  int isdst;
  int dst2;
  int mon_remainder;
  int negative_mon_remainder;
  int mon_years;
  long lyear_requested;
  long year;
  int mon_yday;
  int tmp;
  long lmday;
  long yday;
  time_t guessed_offset;
  int sec_requested;
  int ALOG2_SECONDS_PER_BIENNIUM;
  int ALOG2_MINUTES_PER_BIENNIUM;
  int ALOG2_HOURS_PER_BIENNIUM;
  int ALOG2_DAYS_PER_BIENNIUM;
  int LOG2_YEARS_PER_BIENNIUM;
  int approx_requested_biennia;
  int approx_biennia;
  int diff;
  int abs_diff;
  time_t time_t_max;
  time_t time_t_min;
  time_t overflow_threshold;
  time_t repaired_t0;
  struct tm *tmp___0;
  int stride;
  int duration_max;
  int delta_bound;
  int delta;
  int direction;
  time_t ot;
  struct tm otm;
  int sec_adjustment;
  struct tm *tmp___1;
  int tmp___2;
  int tmp___3;
  int tmp___4;

  {
    {
      remaining_probes = 6;
      sec = tp->tm_sec;
      min = tp->tm_min;
      hour = tp->tm_hour;
      mday = tp->tm_mday;
      mon = tp->tm_mon;
      year_requested = tp->tm_year;
      isdst = tp->tm_isdst;
      mon_remainder = mon % 12;
      negative_mon_remainder = mon_remainder < 0;
      mon_years = mon / 12 - negative_mon_remainder;
      lyear_requested = (long)year_requested;
      year = lyear_requested + (long)mon_years;
      tmp = leapyear((int)year);
      mon_yday = (int)((int const)__mon_yday[tmp][mon_remainder +
                                                  12 * negative_mon_remainder] -
                       1);
      lmday = (long)mday;
      yday = (long)mon_yday + lmday;
      guessed_offset = *offset;
      sec_requested = sec;
    }
    if (sec < 0) {
      sec = 0;
    }
    if (59 < sec) {
      sec = 59;
    }
    {
      t0 = ydhms_diff(year, yday, hour, min, sec, 70, 0, 0, 0,
                      (int)(-guessed_offset));
    }
    if ((((((-1L - (-1L << (sizeof(time_t) * 8UL - 1UL))) / 2147483647L) /
           366L) /
          24L) /
         60L) /
            60L <
        3L) {
      ALOG2_SECONDS_PER_BIENNIUM = 26;
      ALOG2_MINUTES_PER_BIENNIUM = 20;
      ALOG2_HOURS_PER_BIENNIUM = 14;
      ALOG2_DAYS_PER_BIENNIUM = 10;
      LOG2_YEARS_PER_BIENNIUM = 1;
      approx_requested_biennia =
          ((((year_requested >> LOG2_YEARS_PER_BIENNIUM) -
             (70 >> LOG2_YEARS_PER_BIENNIUM)) +
            (mday >> ALOG2_DAYS_PER_BIENNIUM)) +
           (hour >> ALOG2_HOURS_PER_BIENNIUM)) +
          (min >> ALOG2_MINUTES_PER_BIENNIUM);
      approx_biennia = (int)(t0 >> ALOG2_SECONDS_PER_BIENNIUM);
      diff = approx_biennia - approx_requested_biennia;
      if (diff < 0) {
        abs_diff = -diff;
      } else {
        abs_diff = diff;
      }
      time_t_max = -1L - (-1L << (sizeof(time_t) * 8UL - 1UL));
      time_t_min = -1L << (sizeof(time_t) * 8UL - 1UL);
      overflow_threshold =
          (time_t_max / 3L - time_t_min / 3L) >> ALOG2_SECONDS_PER_BIENNIUM;
      if (overflow_threshold < (time_t)abs_diff) {
        repaired_t0 = -1L - t0;
        approx_biennia = (int)(repaired_t0 >> ALOG2_SECONDS_PER_BIENNIUM);
        diff = approx_biennia - approx_requested_biennia;
        if (diff < 0) {
          abs_diff = -diff;
        } else {
          abs_diff = diff;
        }
        if (overflow_threshold < (time_t)abs_diff) {
          return ((time_t)-1);
        }
        guessed_offset += repaired_t0 - t0;
        t0 = repaired_t0;
      }
    }
    t2 = t0;
    t1 = t2;
    t = t1;
    dst2 = 0;
    {
      while (1) {
                                     ;
        {
          tmp___0 = ranged_convert(convert, &t, &tm);
          gt = guess_time_tm(year, yday, hour, min, sec, (time_t const *)(&t),
                             (struct tm const *)tmp___0);
        }
        if (!(t != gt)) {
          goto while_break;
        }
        if (t == t1) {
          if (t != t2) {
            if (tm.tm_isdst < 0) {
              goto offset_found;
            } else {
              if (isdst < 0) {
                tmp___3 = dst2 <= (tm.tm_isdst != 0);
              } else {
                tmp___3 = (isdst != 0) != (tm.tm_isdst != 0);
              }
              if (tmp___3) {
                goto offset_found;
              } else {
                goto _L___4;
              }
            }
          } else {
            goto _L___4;
          }
        } else {
        _L___4: /* CIL Label */
          remaining_probes--;
          if (remaining_probes == 0) {
            return ((time_t)-1);
          }
        }
        t1 = t2;
        t2 = t;
        t = gt;
        dst2 = tm.tm_isdst != 0;
      }
    while_break: /* CIL Label */;
    }
    if (isdst != tm.tm_isdst) {
      if (0 <= isdst) {
        if (0 <= tm.tm_isdst) {
          stride = 601200;
          duration_max = 536454000;
          delta_bound = duration_max / 2 + stride;
          delta = stride;
          {
            while (1) {
                                               ;
              if (!(delta < delta_bound)) {
                goto while_break___0;
              }
              direction = -1;
              {
                while (1) {
                                                   ;
                  if (!(direction <= 1)) {
                    goto while_break___1;
                  }
                  ot = t + (time_t)(delta * direction);
                  if ((ot < t) == (direction < 0)) {
                    {
                      ranged_convert(convert, &ot, &otm);
                    }
                    if (otm.tm_isdst == isdst) {
                      {
                        t = guess_time_tm(year, yday, hour, min, sec,
                                          (time_t const *)(&ot),
                                          (struct tm const *)(&otm));
                        ranged_convert(convert, &t, &tm);
                      }
                      goto offset_found;
                    }
                  }
                  direction += 2;
                }
              while_break___1: /* CIL Label */;
              }
              delta += stride;
            }
          while_break___0: /* CIL Label */;
          }
        }
      }
    }
  offset_found:
    *offset = (guessed_offset + t) - t0;
    if (sec_requested != tm.tm_sec) {
      if (sec == 0) {
        if (tm.tm_sec == 60) {
          tmp___4 = 1;
        } else {
          tmp___4 = 0;
        }
      } else {
        tmp___4 = 0;
      }
      {
        sec_adjustment = tmp___4 - sec;
        t1 = t + (time_t)sec_requested;
        t2 = t1 + (time_t)sec_adjustment;
        tmp___1 = (*convert)((time_t const *)(&t), &tm);
      }
      if (tmp___1) {
        tmp___2 = 0;
      } else {
        tmp___2 = 1;
      }
      if ((((t1 < t) != (sec_requested < 0)) |
           ((t2 < t1) != (sec_adjustment < 0))) |
          tmp___2) {
        return ((time_t)-1);
      }
    }
    *tp = tm;
    return (t);
  }
}
static time_t localtime_offset;
__attribute__((__nothrow__))
time_t(__attribute__((__leaf__)) rpl_mktime)(struct tm *tp);
time_t(__attribute__((__leaf__)) rpl_mktime)(struct tm *tp) {
  time_t tmp;

  {
    {
      tmp = mktime_internal(
          tp, (struct tm * (*)(time_t const *, struct tm *))(&localtime_r),
          &localtime_offset);
    }
    return (tmp);
  }
}
/* #pragma merger("0","020.modechange.o.i","") */
void mode_free(struct mode_change *changes);
strtol_error xstrtoul(char const *s, char **ptr, int strtol_base,
                      unsigned long *val, char const *valid_suffixes);
static struct mode_change *make_node_op_equals(mode_t new_mode) {
  struct mode_change *p;
  void *tmp;

  {
    {
      tmp = malloc(sizeof(struct mode_change));
      p = (struct mode_change *)tmp;
    }
    if ((unsigned long)p == (unsigned long)((void *)0)) {
      return (p);
    }
    p->next = (struct mode_change *)((void *)0);
    p->op = (char)'=';
    p->flags = (char)0;
    p->value = new_mode;
    p->affected = (mode_t)((4032 | (448 >> 3)) | ((448 >> 3) >> 3));
    return (p);
  }
}
static void mode_append_entry(struct mode_change **head,
                              struct mode_change **tail,
                              struct mode_change *e) {
  struct mode_change *tmp;

  {
    if ((unsigned long)*head == (unsigned long)((void *)0)) {
      tmp = e;
      *tail = tmp;
      *head = tmp;
    } else {
      (*tail)->next = e;
      *tail = e;
    }
    return;
  }
}
struct mode_change *mode_compile(char const *mode_string,
                                 unsigned int masked_ops) {
  struct mode_change *head;
  struct mode_change *tail;
  unsigned long octal_value;
  mode_t umask_value;
  struct mode_change *p;
  mode_t mode;
  strtol_error tmp;
  mode_t affected_bits;
  mode_t affected_masked;
  unsigned int ops_to_mask;
  int who_specified_p;
  struct mode_change *change;
  void *tmp___0;
  struct mode_change *p___0;
  struct mode_change *tmp___1;
  int tmp___2;
  int tmp___3;
  int tmp___4;
  int tmp___5;
  int tmp___6;
  int tmp___7;
  int tmp___8;
  int tmp___9;
  int tmp___10;
  int tmp___11;
  int tmp___12;
  int tmp___13;
  unsigned long tmp___14;
  int tmp___15;
  int tmp___16;
  int tmp___17;
                    

  {
    {
      head = (struct mode_change *)((void *)0);
      tmp = xstrtoul(mode_string, (char **)((void *)0), 8, &octal_value, "");
    }
    if ((unsigned int)tmp == 0U) {
      if (octal_value != (octal_value & 4095UL)) {
        return ((struct mode_change *)0);
      }
      if (256 >> 3 == 32) {
        if (128 >> 3 == 16) {
          if (64 >> 3 == 8) {
            if ((256 >> 3) >> 3 == 4) {
              if ((128 >> 3) >> 3 == 2) {
                if ((64 >> 3) >> 3 == 1) {
                  tmp___14 = octal_value;
                } else {
                  goto _L___7;
                }
              } else {
                goto _L___7;
              }
            } else {
              goto _L___7;
            }
          } else {
            goto _L___7;
          }
        } else {
          goto _L___7;
        }
      } else {
      _L___7: /* CIL Label */
        if (octal_value & 2048UL) {
          tmp___2 = 2048;
        } else {
          tmp___2 = 0;
        }
        if (octal_value & 1024UL) {
          tmp___3 = 1024;
        } else {
          tmp___3 = 0;
        }
        if (octal_value & 512UL) {
          tmp___4 = 512;
        } else {
          tmp___4 = 0;
        }
        if (octal_value & 256UL) {
          tmp___5 = 256;
        } else {
          tmp___5 = 0;
        }
        if (octal_value & 128UL) {
          tmp___6 = 128;
        } else {
          tmp___6 = 0;
        }
        if (octal_value & 64UL) {
          tmp___7 = 64;
        } else {
          tmp___7 = 0;
        }
        if (octal_value & 32UL) {
          tmp___8 = 256 >> 3;
        } else {
          tmp___8 = 0;
        }
        if (octal_value & 16UL) {
          tmp___9 = 128 >> 3;
        } else {
          tmp___9 = 0;
        }
        if (octal_value & 8UL) {
          tmp___10 = 64 >> 3;
        } else {
          tmp___10 = 0;
        }
        if (octal_value & 4UL) {
          tmp___11 = (256 >> 3) >> 3;
        } else {
          tmp___11 = 0;
        }
        if (octal_value & 2UL) {
          tmp___12 = (128 >> 3) >> 3;
        } else {
          tmp___12 = 0;
        }
        if (octal_value & 1UL) {
          tmp___13 = (64 >> 3) >> 3;
        } else {
          tmp___13 = 0;
        }
        tmp___14 = (unsigned long)((mode_t)(
            ((((((((((tmp___2 | tmp___3) | tmp___4) | tmp___5) | tmp___6) |
                  tmp___7) |
                 tmp___8) |
                tmp___9) |
               tmp___10) |
              tmp___11) |
             tmp___12) |
            tmp___13));
      }
      {
        mode = (mode_t)tmp___14;
        p = make_node_op_equals(mode);
      }
      if ((unsigned long)p == (unsigned long)((void *)0)) {
        return ((struct mode_change *)1);
      }
      {
	mode_append_entry(&head, &tail, p);
      }
      return (head);
    }
    {
      umask_value = umask((__mode_t)0);
      umask(umask_value);
      mode_string--;
    }
    {
      while (1) {
                                     ;
        affected_bits = (mode_t)0;
        ops_to_mask = 0U;
        affected_bits = (mode_t)0;
        ops_to_mask = 0U;
        mode_string++;
        {
          while (1) {
                                             ;
            {
              if ((int const) * mode_string == 117) {
                goto case_117;
              }
              if ((int const) * mode_string == 103) {
                goto case_103;
              }
              if ((int const) * mode_string == 111) {
                goto case_111;
              }
              if ((int const) * mode_string == 97) {
                goto case_97;
              }
              goto switch_default;
            case_117: /* CIL Label */
              affected_bits |= 2496U;
              goto switch_break;
            case_103: /* CIL Label */
              affected_bits |= (unsigned int)(1024 | (448 >> 3));
              goto switch_break;
            case_111: /* CIL Label */
              affected_bits |= (unsigned int)(512 | ((448 >> 3) >> 3));
              goto switch_break;
            case_97: /* CIL Label */
              affected_bits |=
                  (unsigned int)((4032 | (448 >> 3)) | ((448 >> 3) >> 3));
              goto switch_break;
            switch_default: /* CIL Label */
              goto no_more_affected;
            switch_break: /* CIL Label */;
            }
            mode_string++;
          }
                                        ;
        }
      no_more_affected:
        if (affected_bits) {
          who_specified_p = 1;
        } else {
          who_specified_p = 0;
          affected_bits = (mode_t)((4032 | (448 >> 3)) | ((448 >> 3) >> 3));
          ops_to_mask = masked_ops;
        }
        {
          while (1) {
                                             ;
            if (!((int const) * mode_string == 61)) {
              if (!((int const) * mode_string == 43)) {
                if (!((int const) * mode_string == 45)) {
                  goto while_break___1;
                }
              }
            }
            {
              tmp___0 = malloc(sizeof(struct mode_change));
              change = (struct mode_change *)tmp___0;
            }
            if ((unsigned long)change == (unsigned long)((void *)0)) {
              {
                mode_free(head);
              }
              return ((struct mode_change *)1);
            }
            change->next = (struct mode_change *)((void *)0);
            change->op = (char)*mode_string;
            affected_masked = affected_bits;
            if (!who_specified_p) {
              if ((int const) * mode_string == 61) {
                tmp___15 = 1;
              } else {
                tmp___15 = 0;
              }
              if (ops_to_mask & (unsigned int)tmp___15) {
                {
                  tmp___1 = make_node_op_equals((mode_t)0);
                  p___0 = tmp___1;
                }
                if ((unsigned long)p___0 == (unsigned long)((void *)0)) {
                  return ((struct mode_change *)1);
                }
                {
		  mode_append_entry(&head, &tail, p___0);
		}
              }
            }
            if ((int const) * mode_string == 61) {
              tmp___17 = 1;
            } else {
              if ((int const) * mode_string == 43) {
                tmp___16 = 2;
              } else {
                tmp___16 = 4;
              }
              tmp___17 = tmp___16;
            }
            if (ops_to_mask & (unsigned int)tmp___17) {
              affected_masked &= ~umask_value;
            }
            {
              change->affected = affected_masked;
              change->value = (mode_t)0;
              change->flags = (char)0;
              mode_append_entry(&head, &tail, change);
              mode_string++;
            }
            {
              while (1) {
                                                 ;
                {
                  if ((int const) * mode_string == 114) {
                    goto case_114;
                  }
                  if ((int const) * mode_string == 119) {
                    goto case_119;
                  }
                  if ((int const) * mode_string == 88) {
                    goto case_88;
                  }
                  if ((int const) * mode_string == 120) {
                    goto case_120;
                  }
                  if ((int const) * mode_string == 115) {
                    goto case_115;
                  }
                  if ((int const) * mode_string == 116) {
                    goto case_116;
                  }
                  if ((int const) * mode_string == 117) {
                    goto case_117___0;
                  }
                  if ((int const) * mode_string == 103) {
                    goto case_103___0;
                  }
                  if ((int const) * mode_string == 111) {
                    goto case_111___0;
                  }
                  goto switch_default___0;
                case_114: /* CIL Label */
                  change->value |=
                      (unsigned int)((256 | (256 >> 3)) | ((256 >> 3) >> 3)) &
                      affected_masked;
                  goto switch_break___0;
                case_119: /* CIL Label */
                  change->value |=
                      (unsigned int)((128 | (128 >> 3)) | ((128 >> 3) >> 3)) &
                      affected_masked;
                  goto switch_break___0;
                case_88: /* CIL Label */
                  change->flags = (char)((int)change->flags | 1);
                case_120: /* CIL Label */
                  change->value |=
                      (unsigned int)((64 | (64 >> 3)) | ((64 >> 3) >> 3)) &
                      affected_masked;
                  goto switch_break___0;
                case_115: /* CIL Label */
                  change->value |= 3072U & affected_masked;
                  goto switch_break___0;
                case_116: /* CIL Label */
                  change->value |= 512U & affected_masked;
                  goto switch_break___0;
                case_117___0: /* CIL Label */
                  if (change->value) {
                    goto invalid;
                  }
                  change->value = (mode_t)448;
                  change->flags = (char)((int)change->flags | 2);
                  goto switch_break___0;
                case_103___0: /* CIL Label */
                  if (change->value) {
                    goto invalid;
                  }
                  change->value = (mode_t)(448 >> 3);
                  change->flags = (char)((int)change->flags | 2);
                  goto switch_break___0;
                case_111___0: /* CIL Label */
                  if (change->value) {
                    goto invalid;
                  }
                  change->value = (mode_t)((448 >> 3) >> 3);
                  change->flags = (char)((int)change->flags | 2);
                  goto switch_break___0;
                switch_default___0: /* CIL Label */
                  goto no_more_values;
                switch_break___0: /* CIL Label */;
                }
                mode_string++;
              }
                                            ;
            }
          no_more_values:;
          }
        while_break___1: /* CIL Label */;
        }
        if (!((int const) * mode_string == 44)) {
          goto while_break;
        }
      }
    while_break: /* CIL Label */;
    }
    if ((int const) * mode_string == 0) {
      return (head);
    }
  invalid : {
      mode_free(head);
    }
    return ((struct mode_change *)0);
  }
}
mode_t mode_adjust(mode_t oldmode, struct mode_change const *changes) {
  mode_t newmode;
  mode_t value;
  int tmp;
  int tmp___0;
  int tmp___1;
  int tmp___2;
  int tmp___3;
  int tmp___4;
  int tmp___5;
  int tmp___6;
  int tmp___7;

  {
    newmode = oldmode & (unsigned int)((4032 | (448 >> 3)) | ((448 >> 3) >> 3));
    {
      while (1) {
                                     ;
        if (!changes) {
          goto while_break;
        }
        if ((int const)changes->flags & 2) {
          value = newmode & (unsigned int)changes->value;
          if (changes->value & 448U) {
            if (value & 256U) {
              tmp = (256 >> 3) | ((256 >> 3) >> 3);
            } else {
              tmp = 0;
            }
            if (value & 128U) {
              tmp___0 = (128 >> 3) | ((128 >> 3) >> 3);
            } else {
              tmp___0 = 0;
            }
            if (value & 64U) {
              tmp___1 = (64 >> 3) | ((64 >> 3) >> 3);
            } else {
              tmp___1 = 0;
            }
            value |= (unsigned int)((tmp | tmp___0) | tmp___1);
          } else {
            if (changes->value & (unsigned int const)(448 >> 3)) {
              if (value & (unsigned int)(256 >> 3)) {
                tmp___2 = 256 | ((256 >> 3) >> 3);
              } else {
                tmp___2 = 0;
              }
              if (value & (unsigned int)(128 >> 3)) {
                tmp___3 = 128 | ((128 >> 3) >> 3);
              } else {
                tmp___3 = 0;
              }
              if (value & (unsigned int)(64 >> 3)) {
                tmp___4 = 64 | ((64 >> 3) >> 3);
              } else {
                tmp___4 = 0;
              }
              value |= (unsigned int)((tmp___2 | tmp___3) | tmp___4);
            } else {
              if (value & (unsigned int)((256 >> 3) >> 3)) {
                tmp___5 = 256 | (256 >> 3);
              } else {
                tmp___5 = 0;
              }
              if (value & (unsigned int)((128 >> 3) >> 3)) {
                tmp___6 = 128 | (128 >> 3);
              } else {
                tmp___6 = 0;
              }
              if (value & (unsigned int)((64 >> 3) >> 3)) {
                tmp___7 = 64 | (64 >> 3);
              } else {
                tmp___7 = 0;
              }
              value |= (unsigned int)((tmp___5 | tmp___6) | tmp___7);
            }
          }
          value &= (unsigned int)changes->affected;
        } else {
          value = (mode_t)changes->value;
          if ((int const)changes->flags & 1) {
            if (!((oldmode & 61440U) == 16384U)) {
              if ((newmode &
                   (unsigned int)((64 | (64 >> 3)) | ((64 >> 3) >> 3))) == 0U) {
                value &= (unsigned int)(~((64 | (64 >> 3)) | ((64 >> 3) >> 3)));
              }
            }
          }
        }
        {
          if ((int const)changes->op == 61) {
            goto case_61;
          }
          if ((int const)changes->op == 43) {
            goto case_43;
          }
          if ((int const)changes->op == 45) {
            goto case_45;
          }
          goto switch_break;
        case_61: /* CIL Label */
          newmode = (newmode & (unsigned int)(~changes->affected)) | value;
          goto switch_break;
        case_43: /* CIL Label */
          newmode |= value;
          goto switch_break;
        case_45: /* CIL Label */
          newmode &= ~value;
          goto switch_break;
        switch_break: /* CIL Label */;
        }
        changes = (struct mode_change const *)changes->next;
      }
    while_break: /* CIL Label */;
    }
    return (newmode);
  }
}
void mode_free(struct mode_change *changes) {
  register struct mode_change *next;

  {
    {
      while (1) {
                                     ;
        if (!changes) {
          goto while_break;
        }
        {
          next = changes->next;
          free((void *)changes);
          changes = next;
        }
      }
    while_break: /* CIL Label */;
    }
    return;
  }
}
/* #pragma merger("0","021.prepargs.o.i","") */
static int prepend_args(char const *options, char *buf, char **argv) {
  char const *o___0;
  char *b;
  int n;
  unsigned short const **tmp;
  char const *tmp___0;
  char *tmp___1;
  char tmp___2;
  char const *tmp___3;
  unsigned short const **tmp___4;
  char *tmp___5;

  {
    o___0 = options;
    b = buf;
    n = 0;
    {
      while (1) {
                                     ;
        {
          while (1) {
                                             ;
            {
	      tmp = __ctype_b_loc();
	    }
            if (!((int const) * (*tmp + (int)((unsigned char)*o___0)) & 8192)) {
              goto while_break___0;
            }
            o___0++;
          }
        while_break___0: /* CIL Label */;
        }
        if (!*o___0) {
          return (n);
        }
        if (argv) {
          *(argv + n) = b;
        }
        n++;
        {
          while (1) {
                                             ;
            tmp___1 = b;
            b++;
            tmp___3 = o___0;
            o___0++;
            tmp___2 = (char)*tmp___3;
            *tmp___1 = tmp___2;
            if ((int)tmp___2 == 92) {
              if (*o___0) {
                tmp___0 = o___0;
                o___0++;
                *(b + -1) = (char)*tmp___0;
              }
            }
            if (*o___0) {
              {
                tmp___4 = __ctype_b_loc();
              }
              if ((int const) * (*tmp___4 + (int)((unsigned char)*o___0)) &
                  8192) {
                goto while_break___1;
              }
            } else {
              goto while_break___1;
            }
          }
        while_break___1: /* CIL Label */;
        }
        tmp___5 = b;
        b++;
        *tmp___5 = (char)'\000';
      }
                                ;
    }
  }
}
void prepend_default_options(char const *options, int *pargc, char ***pargv) {
  char *buf;
  size_t tmp;
  void *tmp___0;
  int prepended;
  int tmp___1;
  int argc;
  char *const *argv;
  char **pp;
  void *tmp___2;
  char **tmp___3;
  char *const *tmp___4;
  int tmp___5;
  char **tmp___6;
  char *tmp___7;
  char *const *tmp___8;

  {
    if (options) {
      {
        tmp = strlen(options);
        tmp___0 = xmalloc(tmp + 1UL);
        buf = (char *)tmp___0;
        tmp___1 = prepend_args(options, buf, (char **)0);
        prepended = tmp___1;
        argc = *pargc;
        argv = (char *const *)*pargv;
        tmp___2 =
            xmalloc((unsigned long)((prepended + argc) + 1) * sizeof(*pp));
        pp = (char **)tmp___2;
        *pargc = prepended + argc;
        *pargv = pp;
        tmp___3 = pp;
        pp++;
        tmp___4 = argv;
        argv++;
        *tmp___3 = (char *)*tmp___4;
        tmp___5 = prepend_args(options, buf, pp);
        pp += tmp___5;
      }
      {
        while (1) {
        while_continue: /* CIL Label */;
          tmp___6 = pp;
          pp++;
          tmp___8 = argv;
          argv++;
          tmp___7 = (char *)*tmp___8;
          *tmp___6 = tmp___7;
          if (!tmp___7) {
            goto while_break;
          }
          goto while_continue;
        }
      while_break: /* CIL Label */;
      }
    }
    return;
  }
}
/* #pragma merger("0","022.quotearg.o.i","") */
                                        
                                               
int set_char_quoting(struct quoting_options *o___0, char c, int i);
char *quotearg_n(int n, char const *arg);
char *quotearg_char(char const *arg, char ch);
extern __attribute__((__nothrow__))
size_t(__attribute__((__leaf__)) __ctype_get_mb_cur_max)(void);
extern __attribute__((__nothrow__)) int(__attribute__((__leaf__))
                                        mbsinit)(mbstate_t const *__ps)
    __attribute__((__pure__));
extern __attribute__((__nothrow__))
size_t(__attribute__((__leaf__))
       mbrtowc)(wchar_t *__restrict __pwc, char const *__restrict __s,
                size_t __n, mbstate_t *__restrict __p);
extern __attribute__((__nothrow__)) int(__attribute__((__leaf__))
                                        iswprint)(wint_t __wc);
                                           
                                          
                                        
                                               
                                    
                                         
                                         
                                          
                                  
                                                  
                                                             
                                                             
                                                             
                                 
static struct quoting_options default_quoting_options;
struct quoting_options *clone_quoting_options(struct quoting_options *o___0) {
  int e;
  int *tmp;
  struct quoting_options *p;
  void *tmp___0;
  int *tmp___1;
  struct quoting_options *tmp___2;

  {
    {
      tmp = __errno_location();
      e = *tmp;
      tmp___0 = xmalloc(sizeof(*p));
      p = (struct quoting_options *)tmp___0;
    }
    if (o___0) {
      tmp___2 = o___0;
    } else {
      tmp___2 = &default_quoting_options;
    }
    {
      *p = *tmp___2;
      tmp___1 = __errno_location();
      *tmp___1 = e;
    }
    return (p);
  }
}
void set_quoting_style(struct quoting_options *o___0, enum quoting_style s) {
  struct quoting_options *tmp;

  {
    if (o___0) {
      tmp = o___0;
    } else {
      tmp = &default_quoting_options;
    }
    tmp->style = s;
    return;
  }
}
int set_char_quoting(struct quoting_options *o___0, char c, int i) {
  unsigned char uc;
  int *p;
  int shift;
  int r;
  struct quoting_options *tmp;

  {
    uc = (unsigned char)c;
    if (o___0) {
      tmp = o___0;
    } else {
      tmp = &default_quoting_options;
    }
    p = tmp->quote_these_too + (unsigned long)uc / (sizeof(int) * 8UL);
    shift = (int)((unsigned long)uc % (sizeof(int) * 8UL));
    r = (*p >> shift) & 1;
    *p ^= ((i & 1) ^ r) << shift;
    return (r);
  }
}
static char const *gettext_quote(char const *msgid, enum quoting_style s) {
  char const *translation;
  char *tmp;
                   

  {
    {
      tmp = gettext(msgid);
      translation = (char const *)tmp;
    }
    if ((unsigned long)translation == (unsigned long)msgid) {
      if ((unsigned int)s == 6U) {
        translation = "\"";
      }
    }
    return (translation);
  }
}
static size_t quotearg_buffer_restyled(char *buffer___2, size_t buffersize,
                                       char const *arg, size_t argsize,
                                       enum quoting_style quoting_style,
                                       struct quoting_options const *o___0) {
  size_t i;
  size_t len;
  char const *quote_string;
  size_t quote_string_len;
  int backslash_escapes;
  int unibyte_locale;
  size_t tmp;
  char const *left;
  char const *tmp___0;
  char const *right;
  char const *tmp___1;
  unsigned char c;
  unsigned char esc;
  int tmp___2;
  size_t m;
  int printable;
  unsigned short const **tmp___3;
  mbstate_t mbstate;
  wchar_t w;
  size_t bytes;
  size_t tmp___4;
  size_t j;
  int tmp___5;
  int tmp___6;
  size_t ilim;
  size_t tmp___7;
  int tmp___8;
  int tmp___9;
                    
                    
                    
                    
                    

  {
    {
      len = (size_t)0;
      quote_string = (char const *)0;
      quote_string_len = (size_t)0;
      backslash_escapes = 0;
      tmp = __ctype_get_mb_cur_max();
      unibyte_locale = tmp == 1UL;
    }
    {
      if ((unsigned int)quoting_style == 3U) {
        goto case_3;
      }
      if ((unsigned int)quoting_style == 4U) {
        goto case_4;
      }
      if ((unsigned int)quoting_style == 5U) {
        goto case_5;
      }
      if ((unsigned int)quoting_style == 6U) {
        goto case_5;
      }
      if ((unsigned int)quoting_style == 2U) {
        goto case_2;
      }
      goto switch_default;
    case_3 : /* CIL Label */
    {
      while (1) {
                                     ;
        if (len < buffersize) {
          *(buffer___2 + len) = (char)'\"';
        }
        len++;
        goto while_break;
      }
    while_break: /* CIL Label */;
    }
      backslash_escapes = 1;
      quote_string = "\"";
      quote_string_len = (size_t)1;
      goto switch_break;
    case_4: /* CIL Label */
      backslash_escapes = 1;
      goto switch_break;
    case_5:  /* CIL Label */
                            
     {
      tmp___0 = gettext_quote("`", quoting_style);
      left = tmp___0;
      tmp___1 = gettext_quote("\'", quoting_style);
      right = tmp___1;
      quote_string = left;
    }
      {
        while (1) {
                                           ;
          if (!*quote_string) {
            goto while_break___0;
          }
          {
            while (1) {
                                               ;
              if (len < buffersize) {
                *(buffer___2 + len) = (char)*quote_string;
              }
              len++;
              goto while_break___1;
            }
          while_break___1: /* CIL Label */;
          }
          quote_string++;
        }
      while_break___0: /* CIL Label */;
      }
      {
        backslash_escapes = 1;
        quote_string = right;
        quote_string_len = strlen(quote_string);
      }
      goto switch_break;
    case_2 : /* CIL Label */
    {
      while (1) {
                                         ;
        if (len < buffersize) {
          *(buffer___2 + len) = (char)'\'';
        }
        len++;
        goto while_break___2;
      }
    while_break___2: /* CIL Label */;
    }
      quote_string = "\'";
      quote_string_len = (size_t)1;
      goto switch_break;
    switch_default: /* CIL Label */
      goto switch_break;
    switch_break: /* CIL Label */;
    }
    i = (size_t)0;
    {
      while (1) {
                                         ;
        if (argsize == 0xffffffffffffffffUL) {
          tmp___9 = (int const) * (arg + i) == 0;
        } else {
          tmp___9 = i == argsize;
        }
        if (tmp___9) {
          goto while_break___3;
        }
        if (backslash_escapes) {
          if (quote_string_len) {
            if (i + quote_string_len <= argsize) {
              {
                tmp___2 = memcmp((void const *)(arg + i),
                                 (void const *)quote_string, quote_string_len);
              }
              if (tmp___2 == 0) {
                 

                  while (1) {
                                                     ;
                    if (len < buffersize) {
                      *(buffer___2 + len) = (char)'\\';
                    }
                    len++;
                    goto while_break___4;
                  }
                while_break___4: /* CIL Label */;
                 

              }
            }
          }
        }
        c = (unsigned char)*(arg + i);
        {
          if ((int)c == 0) {
            goto case_0;
          }
          if ((int)c == 63) {
            goto case_63;
          }
          if ((int)c == 7) {
            goto case_7;
          }
          if ((int)c == 8) {
            goto case_8;
          }
          if ((int)c == 12) {
            goto case_12;
          }
          if ((int)c == 10) {
            goto case_10;
          }
          if ((int)c == 13) {
            goto case_13;
          }
          if ((int)c == 9) {
            goto case_9;
          }
          if ((int)c == 11) {
            goto case_11;
          }
          if ((int)c == 92) {
            goto case_92;
          }
          if ((int)c == 123) {
            goto case_123;
          }
          if ((int)c == 125) {
            goto case_123;
          }
          if ((int)c == 35) {
            goto case_35;
          }
          if ((int)c == 126) {
            goto case_35;
          }
          if ((int)c == 32) {
            goto case_32;
          }
          if ((int)c == 33) {
            goto case_32;
          }
          if ((int)c == 34) {
            goto case_32;
          }
          if ((int)c == 36) {
            goto case_32;
          }
          if ((int)c == 38) {
            goto case_32;
          }
          if ((int)c == 40) {
            goto case_32;
          }
          if ((int)c == 41) {
            goto case_32;
          }
          if ((int)c == 42) {
            goto case_32;
          }
          if ((int)c == 59) {
            goto case_32;
          }
          if ((int)c == 60) {
            goto case_32;
          }
          if ((int)c == 61) {
            goto case_32;
          }
          if ((int)c == 62) {
            goto case_32;
          }
          if ((int)c == 91) {
            goto case_32;
          }
          if ((int)c == 94) {
            goto case_32;
          }
          if ((int)c == 96) {
            goto case_32;
          }
          if ((int)c == 124) {
            goto case_32;
          }
          if ((int)c == 39) {
            goto case_39___0;
          }
          if ((int)c == 37) {
            goto case_37;
          }
          if ((int)c == 43) {
            goto case_37;
          }
          if ((int)c == 44) {
            goto case_37;
          }
          if ((int)c == 45) {
            goto case_37;
          }
          if ((int)c == 46) {
            goto case_37;
          }
          if ((int)c == 47) {
            goto case_37;
          }
          if ((int)c == 48) {
            goto case_37;
          }
          if ((int)c == 49) {
            goto case_37;
          }
          if ((int)c == 50) {
            goto case_37;
          }
          if ((int)c == 51) {
            goto case_37;
          }
          if ((int)c == 52) {
            goto case_37;
          }
          if ((int)c == 53) {
            goto case_37;
          }
          if ((int)c == 54) {
            goto case_37;
          }
          if ((int)c == 55) {
            goto case_37;
          }
          if ((int)c == 56) {
            goto case_37;
          }
          if ((int)c == 57) {
            goto case_37;
          }
          if ((int)c == 58) {
            goto case_37;
          }
          if ((int)c == 65) {
            goto case_37;
          }
          if ((int)c == 66) {
            goto case_37;
          }
          if ((int)c == 67) {
            goto case_37;
          }
          if ((int)c == 68) {
            goto case_37;
          }
          if ((int)c == 69) {
            goto case_37;
          }
          if ((int)c == 70) {
            goto case_37;
          }
          if ((int)c == 71) {
            goto case_37;
          }
          if ((int)c == 72) {
            goto case_37;
          }
          if ((int)c == 73) {
            goto case_37;
          }
          if ((int)c == 74) {
            goto case_37;
          }
          if ((int)c == 75) {
            goto case_37;
          }
          if ((int)c == 76) {
            goto case_37;
          }
          if ((int)c == 77) {
            goto case_37;
          }
          if ((int)c == 78) {
            goto case_37;
          }
          if ((int)c == 79) {
            goto case_37;
          }
          if ((int)c == 80) {
            goto case_37;
          }
          if ((int)c == 81) {
            goto case_37;
          }
          if ((int)c == 82) {
            goto case_37;
          }
          if ((int)c == 83) {
            goto case_37;
          }
          if ((int)c == 84) {
            goto case_37;
          }
          if ((int)c == 85) {
            goto case_37;
          }
          if ((int)c == 86) {
            goto case_37;
          }
          if ((int)c == 87) {
            goto case_37;
          }
          if ((int)c == 88) {
            goto case_37;
          }
          if ((int)c == 89) {
            goto case_37;
          }
          if ((int)c == 90) {
            goto case_37;
          }
          if ((int)c == 93) {
            goto case_37;
          }
          if ((int)c == 95) {
            goto case_37;
          }
          if ((int)c == 97) {
            goto case_37;
          }
          if ((int)c == 98) {
            goto case_37;
          }
          if ((int)c == 99) {
            goto case_37;
          }
          if ((int)c == 100) {
            goto case_37;
          }
          if ((int)c == 101) {
            goto case_37;
          }
          if ((int)c == 102) {
            goto case_37;
          }
          if ((int)c == 103) {
            goto case_37;
          }
          if ((int)c == 104) {
            goto case_37;
          }
          if ((int)c == 105) {
            goto case_37;
          }
          if ((int)c == 106) {
            goto case_37;
          }
          if ((int)c == 107) {
            goto case_37;
          }
          if ((int)c == 108) {
            goto case_37;
          }
          if ((int)c == 109) {
            goto case_37;
          }
          if ((int)c == 110) {
            goto case_37;
          }
          if ((int)c == 111) {
            goto case_37;
          }
          if ((int)c == 112) {
            goto case_37;
          }
          if ((int)c == 113) {
            goto case_37;
          }
          if ((int)c == 114) {
            goto case_37;
          }
          if ((int)c == 115) {
            goto case_37;
          }
          if ((int)c == 116) {
            goto case_37;
          }
          if ((int)c == 117) {
            goto case_37;
          }
          if ((int)c == 118) {
            goto case_37;
          }
          if ((int)c == 119) {
            goto case_37;
          }
          if ((int)c == 120) {
            goto case_37;
          }
          if ((int)c == 121) {
            goto case_37;
          }
          if ((int)c == 122) {
            goto case_37;
          }
          goto switch_default___2;
        case_0: /* CIL Label */
          if (backslash_escapes) {
            {
              while (1) {
                                                 ;
                if (len < buffersize) {
                  *(buffer___2 + len) = (char)'\\';
                }
                len++;
                goto while_break___5;
              }
            while_break___5: /* CIL Label */;
            }
            {
              while (1) {
                                                 ;
                if (len < buffersize) {
                  *(buffer___2 + len) = (char)'0';
                }
                len++;
                goto while_break___6;
              }
            while_break___6: /* CIL Label */;
            }
            {
              while (1) {
                                                 ;
                if (len < buffersize) {
                  *(buffer___2 + len) = (char)'0';
                }
                len++;
                goto while_break___7;
              }
            while_break___7: /* CIL Label */;
            }
            c = (unsigned char)'0';
          }
          goto switch_break___0;
        case_63 : /* CIL Label */
        {
          if ((unsigned int)quoting_style == 1U) {
            goto case_1;
          }
          if ((unsigned int)quoting_style == 3U) {
            goto case_3___0;
          }
          goto switch_default___0;
        case_1: /* CIL Label */
          goto use_shell_always_quoting_style;
        case_3___0: /* CIL Label */
          if (i + 2UL < argsize) {
            if ((int const) * (arg + (i + 1UL)) == 63) {
               

                if ((int const) * (arg + (i + 2UL)) == 33) {
                  goto case_33;
                }
                if ((int const) * (arg + (i + 2UL)) == 39) {
                  goto case_33;
                }
                if ((int const) * (arg + (i + 2UL)) == 40) {
                  goto case_33;
                }
                if ((int const) * (arg + (i + 2UL)) == 41) {
                  goto case_33;
                }
                if ((int const) * (arg + (i + 2UL)) == 45) {
                  goto case_33;
                }
                if ((int const) * (arg + (i + 2UL)) == 47) {
                  goto case_33;
                }
                if ((int const) * (arg + (i + 2UL)) == 60) {
                  goto case_33;
                }
                if ((int const) * (arg + (i + 2UL)) == 61) {
                  goto case_33;
                }
                if ((int const) * (arg + (i + 2UL)) == 62) {
                  goto case_33;
                }
                goto switch_break___2;
              case_33: /* CIL Label */
                                      
                                       
                                       
                                       
                                       
                                       
                                       
                                       
                 c = (unsigned char)*(arg + (i + 2UL));
                i += 2UL;
                {
                  while (1) {
                                                     ;
                    if (len < buffersize) {
                      *(buffer___2 + len) = (char)'?';
                    }
                    len++;
                    goto while_break___8;
                  }
                while_break___8: /* CIL Label */;
                }
                {
                  while (1) {
                                                     ;
                    if (len < buffersize) {
                      *(buffer___2 + len) = (char)'\\';
                    }
                    len++;
                    goto while_break___9;
                  }
                while_break___9: /* CIL Label */;
                }
                {
                  while (1) {
                                                      ;
                    if (len < buffersize) {
                      *(buffer___2 + len) = (char)'?';
                    }
                    len++;
                    goto while_break___10;
                  }
                while_break___10: /* CIL Label */;
                }
                goto switch_break___2;
              switch_break___2: /* CIL Label */;
               

            }
          }
          goto switch_break___1;
        switch_default___0: /* CIL Label */
          goto switch_break___1;
        switch_break___1: /* CIL Label */;
        }
          goto switch_break___0;
        case_7: /* CIL Label */
          esc = (unsigned char)'a';
          goto c_escape;
        case_8: /* CIL Label */
          esc = (unsigned char)'b';
          goto c_escape;
        case_12: /* CIL Label */
          esc = (unsigned char)'f';
          goto c_escape;
        case_10: /* CIL Label */
          esc = (unsigned char)'n';
          goto c_and_shell_escape;
        case_13: /* CIL Label */
          esc = (unsigned char)'r';
          goto c_and_shell_escape;
        case_9: /* CIL Label */
          esc = (unsigned char)'t';
          goto c_and_shell_escape;
        case_11: /* CIL Label */
          esc = (unsigned char)'v';
          goto c_escape;
        case_92: /* CIL Label */
          esc = c;
          goto c_and_shell_escape;
        c_and_shell_escape:
          if ((unsigned int)quoting_style == 1U) {
            goto use_shell_always_quoting_style;
          }
        c_escape:
          if (backslash_escapes) {
            c = esc;
            goto store_escape;
          }
          goto switch_break___0;
        case_123: /* CIL Label */
                                 
           if (argsize == 0xffffffffffffffffUL) {
            tmp___8 = (int const) * (arg + 1) == 0;
          } else {
            tmp___8 = argsize == 1UL;
          }
          if (!tmp___8) {
            goto switch_break___0;
          }
        case_35:  /* CIL Label */
                                 
           if (i != 0UL) {
            goto switch_break___0;
          }
        case_32:     /* CIL Label */
                                    
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
           if ((unsigned int)quoting_style == 1U) {
            goto use_shell_always_quoting_style;
          }
          goto switch_break___0;
        case_39___0 : /* CIL Label */
        {
          if ((unsigned int)quoting_style == 1U) {
            goto case_1___0;
          }
          if ((unsigned int)quoting_style == 2U) {
            goto case_2___0;
          }
          goto switch_default___1;
        case_1___0: /* CIL Label */
          goto use_shell_always_quoting_style;
        case_2___0 : /* CIL Label */
        {
          while (1) {
                                              ;
            if (len < buffersize) {
              *(buffer___2 + len) = (char)'\'';
            }
            len++;
            goto while_break___11;
          }
        while_break___11: /* CIL Label */;
        }
          {
            while (1) {
                                                ;
              if (len < buffersize) {
                *(buffer___2 + len) = (char)'\\';
              }
              len++;
              goto while_break___12;
            }
          while_break___12: /* CIL Label */;
          }
          {
            while (1) {
                                                ;
              if (len < buffersize) {
                *(buffer___2 + len) = (char)'\'';
              }
              len++;
              goto while_break___13;
            }
          while_break___13: /* CIL Label */;
          }
          goto switch_break___3;
        switch_default___1: /* CIL Label */
          goto switch_break___3;
        switch_break___3: /* CIL Label */;
        }
          goto switch_break___0;
        case_37:     /* CIL Label */
                                    
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
                                     
           goto switch_break___0;
        switch_default___2: /* CIL Label */
          if (unibyte_locale) {
             

              m = (size_t)1;
              tmp___3 = __ctype_b_loc();
              printable = (int)((int const) * (*tmp___3 + (int)c) & 16384);
             

          } else {
            {
              memset((void *)(&mbstate), 0, sizeof(mbstate));
              m = (size_t)0;
              printable = 1;
            }
            if (argsize == 0xffffffffffffffffUL) {
               

                argsize = strlen(arg);
               

            }
            {
              while (1) {
                                                  ;
                {
                  tmp___4 =
                      mbrtowc((wchar_t * /* __restrict  */)(&w),
                              (char const * /* __restrict  */)(arg + (i + m)),
                              argsize - (i + m),
                              (mbstate_t * /* __restrict  */)(&mbstate));
                  bytes = tmp___4;
                }
                if (bytes == 0UL) {
                  goto while_break___14;
                } else {
                  if (bytes == 0xffffffffffffffffUL) {
                    printable = 0;
                    goto while_break___14;
                  } else {
                    if (bytes == 0xfffffffffffffffeUL) {
                      printable = 0;
                      {
                        while (1) {
                                                            ;
                          if (i + m < argsize) {
                            if (!*(arg + (i + m))) {
                              goto while_break___15;
                            }
                          } else {
                            goto while_break___15;
                          }
                          m++;
                        }
                      while_break___15: /* CIL Label */;
                      }
                      goto while_break___14;
                    } else {
                      if ((unsigned int)quoting_style == 1U) {
                        j = (size_t)1;
                        {
                          while (1) {
                                                              ;
                            if (!(j < bytes)) {
                              goto while_break___16;
                            }
                            {
                              if ((int const) * (arg + ((i + m) + j)) == 91) {
                                goto case_91___0;
                              }
                              if ((int const) * (arg + ((i + m) + j)) == 92) {
                                goto case_91___0;
                              }
                              if ((int const) * (arg + ((i + m) + j)) == 94) {
                                goto case_91___0;
                              }
                              if ((int const) * (arg + ((i + m) + j)) == 96) {
                                goto case_91___0;
                              }
                              if ((int const) * (arg + ((i + m) + j)) == 124) {
                                goto case_91___0;
                              }
                              goto switch_break___4;
                            case_91___0:  /* CIL Label */
                                                         
                                                          
                                                          
                                                          
                               goto use_shell_always_quoting_style;
                            switch_break___4: /* CIL Label */;
                            }
                            j++;
                          }
                        while_break___16: /* CIL Label */;
                        }
                      }
                      {
			tmp___5 = iswprint((wint_t)w);
		      }
                      if (!tmp___5) {
                        printable = 0;
                      }
                      m += bytes;
                    }
                  }
                }
                {
		  tmp___6 = mbsinit((mbstate_t const *)(&mbstate));
		}
                if (tmp___6) {
                  goto while_break___14;
                }
              }
            while_break___14: /* CIL Label */;
            }
          }
          if (1UL < m) {
            goto _L___3;
          } else {
            if (backslash_escapes) {
              if (!printable) {
              _L___3: /* CIL Label */
                ilim = i + m;
                {
                  while (1) {
                                                      ;
                    if (backslash_escapes) {
                      if (!printable) {
                        {
                          while (1) {
                                                              ;
                            if (len < buffersize) {
                              *(buffer___2 + len) = (char)'\\';
                            }
                            len++;
                            goto while_break___18;
                          }
                        while_break___18: /* CIL Label */;
                        }
                        {
                          while (1) {
                                                              ;
                            if (len < buffersize) {
                              *(buffer___2 + len) = (char)(48 + ((int)c >> 6));
                            }
                            len++;
                            goto while_break___19;
                          }
                        while_break___19: /* CIL Label */;
                        }
                        {
                          while (1) {
                                                              ;
                            if (len < buffersize) {
                              *(buffer___2 + len) =
                                  (char)(48 + (((int)c >> 3) & 7));
                            }
                            len++;
                            goto while_break___20;
                          }
                        while_break___20: /* CIL Label */;
                        }
                        c = (unsigned char)(48 + ((int)c & 7));
                      }
                    }
                    if (ilim <= i + 1UL) {
                      goto while_break___17;
                    }
                    {
                      while (1) {
                                                          ;
                        if (len < buffersize) {
                          *(buffer___2 + len) = (char)c;
                        }
                        len++;
                        goto while_break___21;
                      }
                    while_break___21: /* CIL Label */;
                    }
                    i++;
                    c = (unsigned char)*(arg + i);
                  }
                while_break___17: /* CIL Label */;
                }
                goto store_c;
              }
            }
          }
        switch_break___0: /* CIL Label */;
        }
        if (backslash_escapes) {
          if (!(o___0->quote_these_too[(unsigned long)c / (sizeof(int) * 8UL)] &
                (1 << (unsigned long)c % (sizeof(int) * 8UL)))) {
            goto store_c;
          }
        } else {
          goto store_c;
        }
      store_escape : {
        while (1) {
                                            ;
          if (len < buffersize) {
            *(buffer___2 + len) = (char)'\\';
          }
          len++;
          goto while_break___22;
        }
      while_break___22: /* CIL Label */;
      }
      store_c : {
        while (1) {
                                            ;
          if (len < buffersize) {
            *(buffer___2 + len) = (char)c;
          }
          len++;
          goto while_break___23;
        }
      while_break___23: /* CIL Label */;
      }
        i++;
      }
    while_break___3: /* CIL Label */;
    }
    if (i == 0UL) {
      if ((unsigned int)quoting_style == 1U) {
        goto use_shell_always_quoting_style;
      }
    }
    if (quote_string) {
       

        while (1) {
                                            ;
          if (!*quote_string) {
            goto while_break___24;
          }
          {
            while (1) {
                                                ;
              if (len < buffersize) {
                *(buffer___2 + len) = (char)*quote_string;
              }
              len++;
              goto while_break___25;
            }
          while_break___25: /* CIL Label */;
          }
          quote_string++;
        }
      while_break___24: /* CIL Label */;
       

    }
    if (len < buffersize) {
      *(buffer___2 + len) = (char)'\000';
    }
    return (len);
  use_shell_always_quoting_style : {
    tmp___7 = quotearg_buffer_restyled(buffer___2, buffersize, arg, argsize,
                                       (enum quoting_style)2, o___0);
  }
    return (tmp___7);
  }
}
size_t quotearg_buffer(char *buffer___2, size_t buffersize, char const *arg,
                       size_t argsize, struct quoting_options const *o___0) {
  struct quoting_options const *p;
  int e;
  int *tmp;
  size_t r;
  size_t tmp___0;
  int *tmp___1;

  {
    if (o___0) {
      p = o___0;
    } else {
      p = (struct quoting_options const *)(&default_quoting_options);
    }
    {
      tmp = __errno_location();
      e = *tmp;
      tmp___0 = quotearg_buffer_restyled(buffer___2, buffersize, arg, argsize,
                                         (enum quoting_style)p->style, p);
      r = tmp___0;
      tmp___1 = __errno_location();
      *tmp___1 = e;
    }
    return (r);
  }
}
static char slot0[256];
static char *quotearg_n_options(int n, char const *arg, size_t argsize,
                                struct quoting_options const *options);
static unsigned int nslots = 1U;
static struct slotvec slotvec0 = {sizeof(slot0), slot0};
static struct slotvec *slotvec = &slotvec0;
static char *quotearg_n_options(int n, char const *arg, size_t argsize,
                                struct quoting_options const *options) {
  int e;
  int *tmp;
  unsigned int n0;
  unsigned int n1;
  void *tmp___0;
  void *tmp___1;
  size_t size;
  char *val;
  size_t qsize;
  size_t tmp___2;
  void *tmp___3;
  int *tmp___4;
  int tmp___5;

  {
    {
      tmp = __errno_location();
      e = *tmp;
      n0 = (unsigned int)n;
    }
    if (n < 0) {
       

        abort();
       

    }
    if (nslots <= n0) {
      n1 = n0 + 1U;
      if (sizeof(ptrdiff_t) <= sizeof(size_t)) {
        tmp___5 = -1;
      } else {
        tmp___5 = -2;
      }
      if ((size_t)tmp___5 / sizeof(*slotvec) < (size_t)n1) {
         

          xalloc_die();
         

      }
      if ((unsigned long)slotvec == (unsigned long)(&slotvec0)) {
         

          tmp___0 = xmalloc(sizeof(*slotvec));
          slotvec = (struct slotvec *)tmp___0;
          *slotvec = slotvec0;
         

      }
      {
        tmp___1 =
            xrealloc((void *)slotvec, (unsigned long)n1 * sizeof(*slotvec));
        slotvec = (struct slotvec *)tmp___1;
        memset((void *)(slotvec + nslots), 0,
               (unsigned long)(n1 - nslots) * sizeof(*slotvec));
        nslots = n1;
      }
    }
    {
      size = (slotvec + n)->size;
      val = (slotvec + n)->val;
      tmp___2 = quotearg_buffer(val, size, arg, argsize, options);
      qsize = tmp___2;
    }
    if (size <= qsize) {
      size = qsize + 1UL;
      (slotvec + n)->size = size;
      if ((unsigned long)val != (unsigned long)(slot0)) {
         

          free((void *)val);
         

      }
      {
        tmp___3 = xmalloc(size);
        val = (char *)tmp___3;
        (slotvec + n)->val = val;
        quotearg_buffer(val, size, arg, argsize, options);
      }
    }
    {
      tmp___4 = __errno_location();
      *tmp___4 = e;
    }
    return (val);
  }
}
char *quotearg_n(int n, char const *arg) {
  char *tmp;

  {
    {
      tmp = quotearg_n_options(
          n, arg, (size_t)-1,
          (struct quoting_options const *)(&default_quoting_options));
    }
    return (tmp);
  }
}
char *quotearg(char const *arg) {
  char *tmp;

  {
    {
      tmp = quotearg_n(0, arg);
    }
    return (tmp);
  }
}
static struct quoting_options
quoting_options_from_style(enum quoting_style style) {
  struct quoting_options o___0;
                   

  {
    {
      o___0.style = style;
      memset((void *)(o___0.quote_these_too), 0, sizeof(o___0.quote_these_too));
    }
    return (o___0);
  }
}
char *quotearg_n_style(int n, enum quoting_style s, char const *arg) {
  struct quoting_options o___0;
  struct quoting_options tmp;
  char *tmp___0;
                   
                   

  {
    {
      tmp = quoting_options_from_style(s);
      o___0 = tmp;
      tmp___0 = quotearg_n_options(n, arg, (size_t)-1,
                                   (struct quoting_options const *)(&o___0));
    }
    return (tmp___0);
  }
}
char *quotearg_char(char const *arg, char ch) {
  struct quoting_options options;
  char *tmp;
                   

  {
    {
      options = default_quoting_options;
      set_char_quoting(&options, ch, 1);
      tmp = quotearg_n_options(0, arg, (size_t)-1,
                               (struct quoting_options const *)(&options));
    }
    return (tmp);
  }
}
char *quotearg_colon(char const *arg) {
  char *tmp;

  {
    {
      tmp = quotearg_char(arg, (char)':');
    }
    return (tmp);
  }
}
/* #pragma merger("0","023.quote.o.i","") */
/* #pragma merger("0","024.safe-read.o.i","") */
extern ssize_t read(int __fd, void *__buf, size_t __nbytes);
size_t safe_read(int fd, void *buf, size_t count) {
  ssize_t result;
  int *tmp;

  {
    if (count > 2147483647UL) {
      count = (size_t)2147475456;
    }
    {
      while (1) {
                                     ;
        {
	  result = read(fd, buf, count);
	}
        if (result < 0L) {
          {
            tmp = __errno_location();
          }
          if (!(*tmp == 4)) {
            goto while_break;
          }
        } else {
          goto while_break;
        }
      }
    while_break: /* CIL Label */;
    }
    return ((size_t)result);
  }
}
/* #pragma merger("0","025.safe-write.o.i","") */
extern ssize_t write(int __fd, void const *__buf, size_t __n);
size_t safe_write(int fd, void const *buf, size_t count) {
  ssize_t result;
  int *tmp;

  {
    if (count > 2147483647UL) {
      count = (size_t)2147475456;
    }
    {
      while (1) {
                                     ;
        {
	  result = write(fd, buf, count);
	}
        if (result < 0L) {
          {
            tmp = __errno_location();
          }
          if (!(*tmp == 4)) {
            goto while_break;
          }
        } else {
          goto while_break;
        }
      }
    while_break: /* CIL Label */;
    }
    return ((size_t)result);
  }
}
/* #pragma merger("0","026.save-cwd.o.i","") */
extern __attribute__((__nothrow__)) int(__attribute__((__leaf__))
                                        fchdir)(int __fd);
char *xgetcwd(void);
static int have_working_fchdir = 1;
int save_cwd(struct saved_cwd *cwd) {

   

    cwd->desc = -1;
    cwd->name = (char *)((void *)0);
    if (have_working_fchdir) {
      {
        cwd->desc = open(".", 65536);
      }
      if (cwd->desc < 0) {
        return (1);
      }
    }
    if (!have_working_fchdir) {
      {
        cwd->name = xgetcwd();
      }
      if ((unsigned long)cwd->name == (unsigned long)((void *)0)) {
        return (1);
      }
    }
    return (0);
   

}
int restore_cwd(struct saved_cwd const *cwd) {
  int tmp;
  int tmp___0;

  {
    if (0 <= (int)cwd->desc) {
      {
        tmp = fchdir((int)cwd->desc);
      }
      return (tmp < 0);
    } else {
      {
	tmp___0 = chdir((char const *)cwd->name);
      }
      return (tmp___0 < 0);
    }
  }
}
/* #pragma merger("0","027.savedir.o.i","") */
char *savedir(char const *dir) {
  DIR *dirp;
  struct dirent *dp;
  char *name_space;
  size_t allocated;
  size_t used;
  int save_errno;
  void *tmp;
  int *tmp___0;
  char const *entry;
  size_t entry_size;
  size_t tmp___1;
  void *tmp___2;
  int *tmp___3;
  int *tmp___4;
  int tmp___5;
  int *tmp___6;
  int tmp___7;
  int tmp___8;

  {
    {
      allocated = (size_t)512;
      used = (size_t)0;
      dirp = opendir(dir);
    }
    if ((unsigned long)dirp == (unsigned long)((void *)0)) {
      return ((char *)((void *)0));
    }
    {
      tmp = xmalloc(allocated);
      name_space = (char *)tmp;
      tmp___0 = __errno_location();
      *tmp___0 = 0;
    }
    {
      while (1) {
                                     ;
        {
	  dp = readdir(dirp);
	}
        if (!((unsigned long)dp != (unsigned long)((void *)0))) {
          goto while_break;
        }
        entry = (char const *)(dp->d_name);
        if ((int const) * (entry + 0) != 46) {
          tmp___8 = 0;
        } else {
          if ((int const) * (entry + 1) != 46) {
            tmp___7 = 1;
          } else {
            tmp___7 = 2;
          }
          tmp___8 = tmp___7;
        }
        if ((int const) * (entry + tmp___8) != 0) {
          {
            tmp___1 = strlen(entry);
            entry_size = tmp___1 + 1UL;
          }
          if (used + entry_size < used) {
             

              xalloc_die();
             

          }
          if (allocated <= used + entry_size) {
            {
              while (1) {
                                                 ;
                if (2UL * allocated < allocated) {
                   

                    xalloc_die();
                   

                }
                allocated *= 2UL;
                if (!(allocated <= used + entry_size)) {
                  goto while_break___0;
                }
              }
            while_break___0: /* CIL Label */;
            }
            {
              tmp___2 = xrealloc((void *)name_space, allocated);
              name_space = (char *)tmp___2;
            }
          }
          {
            memcpy((void * /* __restrict  */)(name_space + used),
                   (void const * /* __restrict  */)entry, entry_size);
            used += entry_size;
          }
        }
      }
    while_break: /* CIL Label */;
    }
    {
      *(name_space + used) = (char)'\000';
      tmp___3 = __errno_location();
      save_errno = *tmp___3;
      tmp___5 = closedir(dirp);
    }
    if (tmp___5 != 0) {
       

        tmp___4 = __errno_location();
        save_errno = *tmp___4;
       

    }
    if (save_errno != 0) {
      {
        free((void *)name_space);
        tmp___6 = __errno_location();
        *tmp___6 = save_errno;
      }
      return ((char *)((void *)0));
    }
    return (name_space);
  }
}
/* #pragma merger("0","028.stripslash.o.i","") */
int strip_trailing_slashes(char *path) {
  char *base;
  char *tmp;
  char *base_lim;
  size_t tmp___0;
  int had_slash;

  {
    {
      tmp = base_name((char const *)path);
      base = tmp;
      tmp___0 = base_len((char const *)base);
      base_lim = base + tmp___0;
      had_slash = (int)*base_lim;
      *base_lim = (char)'\000';
    }
    return (had_slash);
  }
}
/* #pragma merger("0","029.xgetcwd.o.i","") */
extern __attribute__((__nothrow__)) char *(__attribute__((__leaf__))
                                           getcwd)(char *__buf, size_t __size);
char *xgetcwd(void) {
  char *cwd;
  char *tmp;
  int *tmp___0;

  {
    {
      tmp = getcwd((char *)((void *)0), (size_t)0);
      cwd = tmp;
    }
    if (!cwd) {
      {
        tmp___0 = __errno_location();
      }
      if (*tmp___0 == 12) {
         

          xalloc_die();
         

      }
    }
    return (cwd);
  }
}
/* #pragma merger("0","02a.xmalloc.o.i","") */
                                           
void *xclone(void const *p, size_t s);
extern
    __attribute__((__nothrow__)) void *(__attribute__((__warn_unused_result__,
                                                       __leaf__))
                                        realloc)(void *__ptr, size_t __size);
void (*xalloc_fail_func)(void) = (void (*)(void))0;
char const xalloc_msg_memory_exhausted[17] = {
    (char const)'m',   (char const)'e', (char const)'m', (char const)'o',
    (char const)'r',   (char const)'y', (char const)' ', (char const)'e',
    (char const)'x',   (char const)'h', (char const)'a', (char const)'u',
    (char const)'s',   (char const)'t', (char const)'e', (char const)'d',
    (char const)'\000'};
__attribute__((__noreturn__)) void xalloc_die(void);
void xalloc_die(void) {
  char *tmp;

  {
    if (xalloc_fail_func) {
       

        (*xalloc_fail_func)();
       

    }
    {
      tmp = gettext(xalloc_msg_memory_exhausted);
      error((int)exit_failure, 0, "%s", tmp);
      abort();
    }
  }
}
__inline static void *xnmalloc_inline(size_t n, size_t s) {
  void *p;
  int tmp;

  {
    if (sizeof(ptrdiff_t) <= sizeof(size_t)) {
      tmp = -1;
    } else {
      tmp = -2;
    }
    if ((size_t)tmp / s < n) {
       

        xalloc_die();
       

    } else {
      {
	p = malloc(n * s);
      }
      if (!p) {
         

          xalloc_die();
         

      }
    }
    return (p);
  }
}
void *xmalloc(size_t n) {
  void *tmp;

  {
    {
      tmp = xnmalloc_inline(n, (size_t)1);
    }
    return (tmp);
  }
}
__inline static void *xnrealloc_inline(void *p, size_t n, size_t s) {
  int tmp;

  {
    if (sizeof(ptrdiff_t) <= sizeof(size_t)) {
      tmp = -1;
    } else {
      tmp = -2;
    }
    if ((size_t)tmp / s < n) {
       

        xalloc_die();
       

    } else {
      {
	p = realloc(p, n * s);
      }
      if (!p) {
         

          xalloc_die();
         

      }
    }
    return (p);
  }
}
void *xrealloc(void *p, size_t n) {
  void *tmp;

  {
    {
      tmp = xnrealloc_inline(p, n, (size_t)1);
    }
    return (tmp);
  }
}
__inline static void *x2nrealloc_inline(void *p, size_t *pn, size_t s) {
  size_t n;
  void *tmp;

  {
    n = *pn;
    if (!p) {
      if (!n) {
        n = 64UL / s;
        n += (size_t)(!n);
      }
    } else {
      if (9223372036854775807UL / s < n) {
         

          xalloc_die();
         

      }
      n *= 2UL;
    }
    {
      *pn = n;
      tmp = xrealloc(p, n * s);
    }
    return (tmp);
  }
}
void *x2nrealloc(void *p, size_t *pn, size_t s) {
  void *tmp;

  {
    {
      tmp = x2nrealloc_inline(p, pn, s);
    }
    return (tmp);
  }
}
void *x2realloc(void *p, size_t *pn) {
  void *tmp;

  {
    {
      tmp = x2nrealloc_inline(p, pn, (size_t)1);
    }
    return (tmp);
  }
}
void *xzalloc(size_t s) {
  void *tmp;
  void *tmp___0;

  {
    {
      tmp = xmalloc(s);
      tmp___0 = memset(tmp, 0, s);
    }
    return (tmp___0);
  }
}
void *xclone(void const *p, size_t s) {
  void *tmp;
  void *tmp___0;

  {
    {
      tmp = xmalloc(s);
      tmp___0 = memcpy((void * /* __restrict  */)tmp,
                       (void const * /* __restrict  */)p, s);
    }
    return (tmp___0);
  }
}
/* #pragma merger("0","02b.xstrdup.o.i","") */
char *xstrdup(char const *string) {
  size_t tmp;
  void *tmp___0;

  {
    {
      tmp = strlen(string);
      tmp___0 = xclone((void const *)string, tmp + 1UL);
    }
    return ((char *)tmp___0);
  }
}
/* #pragma merger("0","02c.xstrtol.o.i","") */
extern __attribute__((__nothrow__, __noreturn__)) void(__attribute__((
    __leaf__)) __assert_fail)(char const *__assertion, char const *__file,
                              unsigned int __line, char const *__function);
extern
    __attribute__((__nothrow__)) long(__attribute__((__nonnull__(1), __leaf__))
                                      strtol)(char const *__restrict __nptr,
                                              char **__restrict __endptr,
                                              int __base);
/* #pragma merger("0","02d.xstrtoul.o.i","") */
static strtol_error bkm_scale___0(unsigned long *x, int scale_factor) {

   

    if (0xffffffffffffffffUL / (unsigned long)scale_factor < *x) {
      *x = 0xffffffffffffffffUL;
      return ((strtol_error)1);
    }
    *x *= (unsigned long)scale_factor;
    return ((strtol_error)0);
   

}
static strtol_error bkm_scale_by_power___0(unsigned long *x, int base,
                                           int power) {
  strtol_error err;
  strtol_error tmp;
  int tmp___0;

  {
    err = (strtol_error)0;
    {
      while (1) {
                                     ;
        tmp___0 = power;
        power--;
        if (!tmp___0) {
          goto while_break;
        }
        {
          tmp = bkm_scale___0(x, base);
          err = (strtol_error)((unsigned int)err | (unsigned int)tmp);
        }
      }
    while_break: /* CIL Label */;
    }
    return (err);
  }
}
strtol_error xstrtoul(char const *s, char **ptr, int strtol_base,
                      unsigned long *val, char const *valid_suffixes) {
  char *t_ptr;
  char **p;
  unsigned long tmp;
  strtol_error err;
  char const *q;
  unsigned short const **tmp___0;
  int *tmp___1;
  char *tmp___2;
  int *tmp___3;
  int *tmp___4;
  int base;
  int suffixes;
  strtol_error overflow;
  char *tmp___5;
  char *tmp___6;

  {
    err = (strtol_error)0;
    if (0 <= strtol_base) {
      if (!(strtol_base <= 36)) {
         

          __assert_fail(
              "0 <= strtol_base && strtol_base <= 36",
              "/home/wslee/project/cbenchmarks/tar-1.14/lib/xstrtol.c", 117U,
              "xstrtoul");
         

      }
    } else {
       

        __assert_fail("0 <= strtol_base && strtol_base <= 36",
                      "/home/wslee/project/cbenchmarks/tar-1.14/lib/xstrtol.c",
                      117U, "xstrtoul");
       

    }
    if (ptr) {
      p = ptr;
    } else {
      p = &t_ptr;
    }
    q = s;
    {
      while (1) {
                                     ;
        {
	  tmp___0 = __ctype_b_loc();
	}
        if (!((int const) * (*tmp___0 + (int)((unsigned char)*q)) & 8192)) {
          goto while_break;
        }
        q++;
      }
    while_break: /* CIL Label */;
    }
    if ((int const) * q == 45) {
      return ((strtol_error)4);
    }
    {
      tmp___1 = __errno_location();
      *tmp___1 = 0;
      tmp = strtoul((char const * /* __restrict  */)s,
                    (char ** /* __restrict  */)p, strtol_base);
    }
    if ((unsigned long)*p == (unsigned long)s) {
      if (valid_suffixes) {
        if (*(*p)) {
          {
            tmp___2 = strchr(valid_suffixes, (int)*(*p));
          }
          if (tmp___2) {
            tmp = 1UL;
          } else {
            return ((strtol_error)4);
          }
        } else {
          return ((strtol_error)4);
        }
      } else {
        return ((strtol_error)4);
      }
    } else {
      {
	tmp___4 = __errno_location();
      }
      if (*tmp___4 != 0) {
        {
          tmp___3 = __errno_location();
        }
        if (*tmp___3 != 34) {
          return ((strtol_error)4);
        }
        err = (strtol_error)1;
      }
    }
    if (!valid_suffixes) {
      *val = tmp;
      return (err);
    }
    if ((int)*(*p) != 0) {
      {
        base = 1024;
        suffixes = 1;
        tmp___5 = strchr(valid_suffixes, (int)*(*p));
      }
      if (!tmp___5) {
        *val = tmp;
        return ((strtol_error)((unsigned int)err | 2U));
      }
      {
	tmp___6 = strchr(valid_suffixes, '0');
      }
      if (tmp___6) {
         

          if ((int)*(*(p + 0) + 1) == 105) {
            goto case_105;
          }
          if ((int)*(*(p + 0) + 1) == 66) {
            goto case_66;
          }
          if ((int)*(*(p + 0) + 1) == 68) {
            goto case_66;
          }
          goto switch_break;
        case_105: /* CIL Label */
          if ((int)*(*(p + 0) + 2) == 66) {
            suffixes += 2;
          }
          goto switch_break;
        case_66: /* CIL Label */
                                
           base = 1000;
          suffixes++;
          goto switch_break;
        switch_break: /* CIL Label */;
         

      }
      {
        if ((int)*(*p) == 98) {
          goto case_98;
        }
        if ((int)*(*p) == 66) {
          goto case_66___0;
        }
        if ((int)*(*p) == 99) {
          goto case_99;
        }
        if ((int)*(*p) == 69) {
          goto case_69;
        }
        if ((int)*(*p) == 71) {
          goto case_71;
        }
        if ((int)*(*p) == 103) {
          goto case_71;
        }
        if ((int)*(*p) == 107) {
          goto case_107;
        }
        if ((int)*(*p) == 75) {
          goto case_107;
        }
        if ((int)*(*p) == 77) {
          goto case_77;
        }
        if ((int)*(*p) == 109) {
          goto case_77;
        }
        if ((int)*(*p) == 80) {
          goto case_80;
        }
        if ((int)*(*p) == 84) {
          goto case_84;
        }
        if ((int)*(*p) == 116) {
          goto case_84;
        }
        if ((int)*(*p) == 119) {
          goto case_119;
        }
        if ((int)*(*p) == 89) {
          goto case_89;
        }
        if ((int)*(*p) == 90) {
          goto case_90;
        }
        goto switch_default;
      case_98 : /* CIL Label */
      {
        overflow = bkm_scale___0(&tmp, 512);
      }
        goto switch_break___0;
      case_66___0 : /* CIL Label */
      {
        overflow = bkm_scale___0(&tmp, 1024);
      }
        goto switch_break___0;
      case_99: /* CIL Label */
        overflow = (strtol_error)0;
        goto switch_break___0;
      case_69 : /* CIL Label */
      {
        overflow = bkm_scale_by_power___0(&tmp, base, 6);
      }
        goto switch_break___0;
      case_71:   /* CIL Label */
                                
       {
        overflow = bkm_scale_by_power___0(&tmp, base, 3);
      }
        goto switch_break___0;
      case_107: /* CIL Label */
                               
       {
        overflow = bkm_scale_by_power___0(&tmp, base, 1);
      }
        goto switch_break___0;
      case_77:   /* CIL Label */
                                
       {
        overflow = bkm_scale_by_power___0(&tmp, base, 2);
      }
        goto switch_break___0;
      case_80 : /* CIL Label */
      {
        overflow = bkm_scale_by_power___0(&tmp, base, 5);
      }
        goto switch_break___0;
      case_84:   /* CIL Label */
                                
       {
        overflow = bkm_scale_by_power___0(&tmp, base, 4);
      }
        goto switch_break___0;
      case_119 : /* CIL Label */
      {
        overflow = bkm_scale___0(&tmp, 2);
      }
        goto switch_break___0;
      case_89 : /* CIL Label */
      {
        overflow = bkm_scale_by_power___0(&tmp, base, 8);
      }
        goto switch_break___0;
      case_90 : /* CIL Label */
      {
        overflow = bkm_scale_by_power___0(&tmp, base, 7);
      }
        goto switch_break___0;
      switch_default: /* CIL Label */
        *val = tmp;
        return ((strtol_error)((unsigned int)err | 2U));
      switch_break___0: /* CIL Label */;
      }
      err = (strtol_error)((unsigned int)err | (unsigned int)overflow);
      *p += suffixes;
      if (*(*p)) {
        err = (strtol_error)((unsigned int)err | 2U);
      }
    }
    *val = tmp;
    return (err);
  }
}
/* #pragma merger("0","02e.xstrtoumax.o.i","") */
extern __attribute__((__nothrow__))
uintmax_t(__attribute__((__leaf__)) strtoumax)(char const *__restrict __nptr,
                                               char **__restrict __endptr,
                                               int __base);
static strtol_error bkm_scale___1(uintmax_t *x, int scale_factor) {

   

    if (0xffffffffffffffffUL / (unsigned long)scale_factor < *x) {
      *x = 0xffffffffffffffffUL;
      return ((strtol_error)1);
    }
    *x *= (uintmax_t)scale_factor;
    return ((strtol_error)0);
   

}
static strtol_error bkm_scale_by_power___1(uintmax_t *x, int base, int power) {
  strtol_error err;
  strtol_error tmp;
  int tmp___0;

  {
    err = (strtol_error)0;
    {
      while (1) {
                                     ;
        tmp___0 = power;
        power--;
        if (!tmp___0) {
          goto while_break;
        }
        {
          tmp = bkm_scale___1(x, base);
          err = (strtol_error)((unsigned int)err | (unsigned int)tmp);
        }
      }
    while_break: /* CIL Label */;
    }
    return (err);
  }
}
strtol_error xstrtoumax(char const *s, char **ptr, int strtol_base,
                        uintmax_t *val, char const *valid_suffixes) {
  char *t_ptr;
  char **p;
  uintmax_t tmp;
  strtol_error err;
  char const *q;
  unsigned short const **tmp___0;
  int *tmp___1;
  char *tmp___2;
  int *tmp___3;
  int *tmp___4;
  int base;
  int suffixes;
  strtol_error overflow;
  char *tmp___5;
  char *tmp___6;

  {
    err = (strtol_error)0;
    if (0 <= strtol_base) {
      if (!(strtol_base <= 36)) {
         

          __assert_fail(
              "0 <= strtol_base && strtol_base <= 36",
              "/home/wslee/project/cbenchmarks/tar-1.14/lib/xstrtol.c", 117U,
              "xstrtoumax");
         

      }
    } else {
       

        __assert_fail("0 <= strtol_base && strtol_base <= 36",
                      "/home/wslee/project/cbenchmarks/tar-1.14/lib/xstrtol.c",
                      117U, "xstrtoumax");
       

    }
    if (ptr) {
      p = ptr;
    } else {
      p = &t_ptr;
    }
    q = s;
    {
      while (1) {
                                     ;
        {
	  tmp___0 = __ctype_b_loc();
	}
        if (!((int const) * (*tmp___0 + (int)((unsigned char)*q)) & 8192)) {
          goto while_break;
        }
        q++;
      }
    while_break: /* CIL Label */;
    }
    if ((int const) * q == 45) {
      return ((strtol_error)4);
    }
    {
      tmp___1 = __errno_location();
      *tmp___1 = 0;
      tmp = strtoumax((char const * /* __restrict  */)s,
                      (char ** /* __restrict  */)p, strtol_base);
    }
    if ((unsigned long)*p == (unsigned long)s) {
      if (valid_suffixes) {
        if (*(*p)) {
          {
            tmp___2 = strchr(valid_suffixes, (int)*(*p));
          }
          if (tmp___2) {
            tmp = (uintmax_t)1;
          } else {
            return ((strtol_error)4);
          }
        } else {
          return ((strtol_error)4);
        }
      } else {
        return ((strtol_error)4);
      }
    } else {
      {
	tmp___4 = __errno_location();
      }
      if (*tmp___4 != 0) {
        {
          tmp___3 = __errno_location();
        }
        if (*tmp___3 != 34) {
          return ((strtol_error)4);
        }
        err = (strtol_error)1;
      }
    }
    if (!valid_suffixes) {
      *val = tmp;
      return (err);
    }
    if ((int)*(*p) != 0) {
      {
        base = 1024;
        suffixes = 1;
        tmp___5 = strchr(valid_suffixes, (int)*(*p));
      }
      if (!tmp___5) {
        *val = tmp;
        return ((strtol_error)((unsigned int)err | 2U));
      }
      {
	tmp___6 = strchr(valid_suffixes, '0');
      }
      if (tmp___6) {
         

          if ((int)*(*(p + 0) + 1) == 105) {
            goto case_105;
          }
          if ((int)*(*(p + 0) + 1) == 66) {
            goto case_66;
          }
          if ((int)*(*(p + 0) + 1) == 68) {
            goto case_66;
          }
          goto switch_break;
        case_105: /* CIL Label */
          if ((int)*(*(p + 0) + 2) == 66) {
            suffixes += 2;
          }
          goto switch_break;
        case_66: /* CIL Label */
                                
           base = 1000;
          suffixes++;
          goto switch_break;
        switch_break: /* CIL Label */;
         

      }
      {
        if ((int)*(*p) == 98) {
          goto case_98;
        }
        if ((int)*(*p) == 66) {
          goto case_66___0;
        }
        if ((int)*(*p) == 99) {
          goto case_99;
        }
        if ((int)*(*p) == 69) {
          goto case_69;
        }
        if ((int)*(*p) == 71) {
          goto case_71;
        }
        if ((int)*(*p) == 103) {
          goto case_71;
        }
        if ((int)*(*p) == 107) {
          goto case_107;
        }
        if ((int)*(*p) == 75) {
          goto case_107;
        }
        if ((int)*(*p) == 77) {
          goto case_77;
        }
        if ((int)*(*p) == 109) {
          goto case_77;
        }
        if ((int)*(*p) == 80) {
          goto case_80;
        }
        if ((int)*(*p) == 84) {
          goto case_84;
        }
        if ((int)*(*p) == 116) {
          goto case_84;
        }
        if ((int)*(*p) == 119) {
          goto case_119;
        }
        if ((int)*(*p) == 89) {
          goto case_89;
        }
        if ((int)*(*p) == 90) {
          goto case_90;
        }
        goto switch_default;
      case_98 : /* CIL Label */
      {
        overflow = bkm_scale___1(&tmp, 512);
      }
        goto switch_break___0;
      case_66___0 : /* CIL Label */
      {
        overflow = bkm_scale___1(&tmp, 1024);
      }
        goto switch_break___0;
      case_99: /* CIL Label */
        overflow = (strtol_error)0;
        goto switch_break___0;
      case_69 : /* CIL Label */
      {
        overflow = bkm_scale_by_power___1(&tmp, base, 6);
      }
        goto switch_break___0;
      case_71:   /* CIL Label */
                                
       {
        overflow = bkm_scale_by_power___1(&tmp, base, 3);
      }
        goto switch_break___0;
      case_107: /* CIL Label */
                               
       {
        overflow = bkm_scale_by_power___1(&tmp, base, 1);
      }
        goto switch_break___0;
      case_77:   /* CIL Label */
                                
       {
        overflow = bkm_scale_by_power___1(&tmp, base, 2);
      }
        goto switch_break___0;
      case_80 : /* CIL Label */
      {
        overflow = bkm_scale_by_power___1(&tmp, base, 5);
      }
        goto switch_break___0;
      case_84:   /* CIL Label */
                                
       {
        overflow = bkm_scale_by_power___1(&tmp, base, 4);
      }
        goto switch_break___0;
      case_119 : /* CIL Label */
      {
        overflow = bkm_scale___1(&tmp, 2);
      }
        goto switch_break___0;
      case_89 : /* CIL Label */
      {
        overflow = bkm_scale_by_power___1(&tmp, base, 8);
      }
        goto switch_break___0;
      case_90 : /* CIL Label */
      {
        overflow = bkm_scale_by_power___1(&tmp, base, 7);
      }
        goto switch_break___0;
      switch_default: /* CIL Label */
        *val = tmp;
        return ((strtol_error)((unsigned int)err | 2U));
      switch_break___0: /* CIL Label */;
      }
      err = (strtol_error)((unsigned int)err | (unsigned int)overflow);
      *p += suffixes;
      if (*(*p)) {
        err = (strtol_error)((unsigned int)err | 2U);
      }
    }
    *val = tmp;
    return (err);
  }
}
/* #pragma merger("0","../../lib/addext.o.i","") */
/* #pragma merger("0","../../lib/argmatch.o.i","") */
/* #pragma merger("0","../../lib/backupfile.o.i","") */
/* #pragma merger("0","../../lib/basename.o.i","") */
/* #pragma merger("0","../../lib/dirname.o.i","") */
/* #pragma merger("0","../../lib/exclude.o.i","") */
/* #pragma merger("0","../../lib/exitfail.o.i","") */
/* #pragma merger("0","../../lib/full-write.o.i","") */
/* #pragma merger("0","../../lib/getdate.o.i","") */
/* #pragma merger("0","../../lib/getopt1.o.i","") */
/* #pragma merger("0","../../lib/getopt.o.i","") */
/* #pragma merger("0","../../lib/gettime.o.i","") */
/* #pragma merger("0","../../lib/hash.o.i","") */
/* #pragma merger("0","../../lib/human.o.i","") */
/* #pragma merger("0","../../lib/mktime.o.i","") */
/* #pragma merger("0","../../lib/modechange.o.i","") */
/* #pragma merger("0","../../lib/prepargs.o.i","") */
/* #pragma merger("0","../../lib/quotearg.o.i","") */
/* #pragma merger("0","../../lib/quote.o.i","") */
/* #pragma merger("0","../../lib/safe-read.o.i","") */
/* #pragma merger("0","../../lib/safe-write.o.i","") */
/* #pragma merger("0","../../lib/save-cwd.o.i","") */
/* #pragma merger("0","../../lib/savedir.o.i","") */
/* #pragma merger("0","../../lib/stripslash.o.i","") */
/* #pragma merger("0","../../lib/xgetcwd.o.i","") */
/* #pragma merger("0","../../lib/xmalloc.o.i","") */
/* #pragma merger("0","../../lib/xstrdup.o.i","") */
/* #pragma merger("0","../../lib/xstrtol.o.i","") */
/* #pragma merger("0","../../lib/xstrtoul.o.i","") */
/* #pragma merger("0","../../lib/xstrtoumax.o.i","") */
