typedef void *voidp;
typedef unsigned char uch;
typedef unsigned short ush;
typedef unsigned long ulg;
typedef long __off_t;
typedef long __off64_t;
typedef long __ssize_t;
typedef __ssize_t ssize_t;
typedef unsigned long size_t;
struct _IO_FILE;
struct _IO_FILE;
typedef struct _IO_FILE FILE;
typedef void _IO_lock_t;
struct _IO_marker {
  struct _IO_marker *_next;
  struct _IO_FILE *_sbuf;
  int _pos;
};
struct _IO_FILE {
  int _flags;
  char *_IO_read_ptr;
  char *_IO_read_end;
  char *_IO_read_base;
  char *_IO_write_base;
  char *_IO_write_ptr;
  char *_IO_write_end;
  char *_IO_buf_base;
  char *_IO_buf_end;
  char *_IO_save_base;
  char *_IO_backup_base;
  char *_IO_save_end;
  struct _IO_marker *_markers;
  struct _IO_FILE *_chain;
  int _fileno;
  int _flags2;
  __off_t _old_offset;
  unsigned short _cur_column;
  signed char _vtable_offset;
  char _shortbuf[1];
  _IO_lock_t *_lock;
  __off64_t _offset;
  void *__pad1;
  void *__pad2;
  void *__pad3;
  void *__pad4;
  size_t __pad5;
  int _mode;
  char _unused2[(15UL * sizeof(int) - 4UL * sizeof(void *)) - sizeof(size_t)];
};
typedef unsigned char char_type;
typedef long code_int;
typedef unsigned long cmp_code_int;
typedef struct _IO_FILE _IO_FILE;
struct huft;
union __anonunion_v_25 {
  ush n;
  struct huft *t;
};
struct huft {
  uch e;
  uch b;
  union __anonunion_v_25 v;
};
typedef int file_t;
union __anonunion_fc_7 {
  ush freq;
  ush code;
};
union __anonunion_dl_8 {
  ush dad;
  ush len;
};
struct ct_data {
  union __anonunion_fc_7 fc;
  union __anonunion_dl_8 dl;
};
typedef struct ct_data ct_data;
struct tree_desc {
  ct_data *dyn_tree;
  ct_data *static_tree;
  int *extra_bits;
  int extra_base;
  int elems;
  int max_length;
  int max_code;
};
typedef struct tree_desc tree_desc;
typedef ush Pos;
typedef unsigned int IPos;
struct config {
  ush good_length;
  ush max_lazy;
  ush nice_length;
  ush max_chain;
};
typedef struct config config;
typedef unsigned long __dev_t;
typedef unsigned int __uid_t;
typedef unsigned int __gid_t;
typedef unsigned long __ino_t;
typedef unsigned int __mode_t;
typedef unsigned long __nlink_t;
typedef long __time_t;
typedef long __blksize_t;
typedef long __blkcnt_t;
typedef long __syscall_slong_t;
typedef __off_t off_t;
typedef __time_t time_t;
struct timespec {
  __time_t tv_sec;
  __syscall_slong_t tv_nsec;
};
typedef void (*__sighandler_t)(int);
struct stat {
  __dev_t st_dev;
  __ino_t st_ino;
  __nlink_t st_nlink;
  __mode_t st_mode;
  __uid_t st_uid;
  __gid_t st_gid;
  int __pad0;
  __dev_t st_rdev;
  __off_t st_size;
  __blksize_t st_blksize;
  __blkcnt_t st_blocks;
  struct timespec st_atim;
  struct timespec st_mtim;
  struct timespec st_ctim;
  __syscall_slong_t __glibc_reserved[3];
};
struct option {
  char const *name;
  int has_arg;
  int *flag;
  int val;
};
struct dirent {
  __ino_t d_ino;
  __off_t d_off;
  unsigned short d_reclen;
  unsigned char d_type;
  char d_name[256];
};
struct __dirstream;
struct __dirstream;
typedef struct __dirstream DIR;
typedef struct dirent dir_type;
struct utimbuf {
  __time_t actime;
  __time_t modtime;
};
uch inbuf[32832];
uch outbuf[18432];
ush d_buf[32768];
uch window[65536L];
ush prev[1L << 16];
unsigned int insize;
unsigned int inptr;
int ifd;
int ofd;
int test;
int unlzh(int in, int out);
int fill_inbuf(int eof_ok);
void write_buf(int fd, voidp buf, unsigned int cnt);
void error(char *m);
static unsigned int decode(unsigned int count, uch *buffer);
static void decode_start(void);
static void huf_decode_start(void);
static unsigned int decode_c(void);
static unsigned int decode_p(void);
static void read_pt_len(int nn, int nbit, int i_special);
static void read_c_len(void);
static void fillbuf(int n);
static unsigned int getbits(int n);
static void init_getbits(void);
static void make_table(int nchar, uch *bitlen, int tablebits, ush *table);
static uch pt_len[19];
static unsigned int blocksize;
static ush pt_table[256];
static ush bitbuf;
static unsigned int subbitbuf;
static int bitcount;
static void fillbuf(int n) {
  unsigned int tmp;
  int tmp___0;
  int tmp___1;

   
                                     
     
                 
                                      
                              
                           
         
                      
                                                                
                             
                      
                  
                                    
                
           

                                    
                              
           

         
                                          
                                   
                         
         
                     
       
                                 
     
                  
                                                                   
           
   
}
static unsigned int getbits(int n) {
  unsigned int x;

  {
    {
      x = (unsigned int)((int)bitbuf >>
                         (16UL * sizeof(char) - (unsigned long)n));
                 
    }
    return (x);
  }
}
static void init_getbits(void) {

   

     
                      
                     
                   
                                          
     
           
   

}
static void make_table(int nchar, uch *bitlen, int tablebits, ush *table) {
  ush count[17];
  ush weight[17];
  ush start[18];
  ush *p;
  unsigned int i___0;
  unsigned int k;
  unsigned int len;
  unsigned int ch;
  unsigned int jutbits;
  unsigned int avail;
  unsigned int nextcode;
  unsigned int mask;
  unsigned int tmp;
  ush tmp___0;
  unsigned int tmp___1;
                    
                    
                    
                    

   
               
     
                 
                                      
                              
                           
         
                              
                
       
                                 
     
               
     
                 
                                          
                                             
                               
         
                                                                            
                
       
                                     
     
                      
               
     
                 
                                          
                              
                               
         
                           
                                                                            
                
       
                                     
     
                                        
       

                                     
       

     
                                             
               
     
                 
                                          
                                                  
                               
         
                                                           
                                                                       
                
       
                                     
     
     
                 
                                          
                              
                               
         
                                                   
                
       
                                     
     
                                                                 
                      
                                         
       
                   
                                            
                              
                                 
           
                      
                  
                                  
         
                                       
       
     
                                
                                  
            
     
                 
                                          
                                          
                               
         
                                           
                        
                      
         
                                                                      
                                             
                                           
           
                       
                                                
                                        
                                     
               
                                         
                      
             
                                           
           
                
                                       
                                     
                                                
           
                       
                                                
                                   
                                     
               
                                 
                                 
                                      
                                                    
                                
                        
                                  
               
                             
                                             
                      
                              
               
                      
                      
             
                                           
           
                       
         
                                   
             
             
       
                                     
     
           
   
}
static void read_pt_len(int nn, int nbit, int i_special) {
  int i___0;
  int c;
  int n;
  unsigned int mask;
  int tmp;
  int tmp___0;
  int tmp___1;
  int tmp___2;

   
     
                             
     
                 
       
                               
                  
       
       
                   
                                        
                              
                             
           
                                 
                  
         
                                   
       
                
       
                   
                                            
                               
                                 
           
                                   
                  
         
                                       
       
            
                
       
                   
                                            
                             
                                 
           
                                                         
                       
                                                             
             
                         
                                                  
                                                     
                                       
                 
                           
                    
               
                                             
             
           
                      
                    
                  
                        
           
           
                         
                            
                    
                                     
           
                                   
             
                                  
             
             
                         
                                                  
                    
                                
                                       
                 
                                
                        
                                         
               
                                             
             
           
         
                                       
       
       
                   
                                            
                              
                                 
           
                          
                  
                                   
         
                                       
       
       
	                                    
       
     
           
   
}
static void read_c_len(void) {
  int i___0;
  int c;
  int n;
  unsigned int mask;
  unsigned int tmp;
  unsigned int tmp___0;
  int tmp___1;
  int tmp___2;
  int tmp___3;

   
     
                          
     
                 
       
                            
                  
       
       
                   
                                        
                               
                             
           
                                 
                  
         
                                   
       
                
       
                   
                                            
                                
                                 
           
                                
                  
         
                                       
       
            
                
       
                   
                                            
                             
                                 
           
                                                                        
                        
                                                             
             
                         
                                                  
                                                  
                                                 
                        
                                   
                 
                           
                                 
                                       
                 
               
                                             
             
           
           
	                            
	   
                       
                         
                    
                    
                           
                 

                                   
                                      
                 

                      
                 

                                       
                                           
                 

               
             
             
                         
                                                  
                    
                                
                                       
                 
                                
                        
                                         
               
                                             
             
                  
                            
                    
                                           
           
         
                                       
       
       
                   
                                            
                               
                                 
           
                          
                  
                                   
         
                                       
       
       
	                                   
       
     
           
   
}
static unsigned int decode_c(void) {
  unsigned int j___0;
  unsigned int mask;

  {
                          
       
                                
       
                            
                      
       
       
                              
                     
                               
       
     
                
                                                                             
                       {
                                                        
      {
                  {
                                       ;
                                           {
            j___0 = (unsigned int)*((prev + 32768) + j___0);
          }       
                                              
           
          mask >>= 1;
                                 
                             
           
        }
      while_break: /* CIL Label */;
      }
    }
     
                                  
     
    return (j___0);
  }
}
static unsigned int decode_p(void) {
  unsigned int j___0;
  unsigned int mask;
  unsigned int tmp;

  {
                                                                               
                       
                                                       
       
                   
                                        
                                            
                                                            
                  
                                              
           
                     
                                
                             
           
         
                                   
       
     
     
                                  
     
                     {
       

        tmp = getbits((int)(j___0 - 1U));
        j___0 = (1U << (j___0 - 1U)) + tmp;
       

    }
    return (j___0);
  }
}
static void huf_decode_start(void) {

   

     
                     
                     
     
           
   

}
static int j;
static int done;
static void decode_start(void) {

   

     
                         
            
               
     
           
   

}
static unsigned int i;
static unsigned int decode(unsigned int count, uch *buffer) {
  unsigned int r;
  unsigned int c;
  unsigned int tmp;

  {
           
     
                 
                                      
            
                        
                           
         
                                      
                                         
            
                         
                     
         
       
                                 
     
    {
                {
                                         ;
        {
	  c = decode_c();
	}
                        
                   
                     
         
                       {
                                 
          r++;
                          {
            return (r);
          }
        }       
           
                                
                             
                                                     
           
           
                       
                                                
                  
                              
                                     
               
                                            
                                               
                  
                               
                           
               
             
                                           
           
         
      }
                                    ;
    }
  }
}
int unlzh(int in, int out) {
  unsigned int n;

  {
     
               
                
                     
     
     
                 
                                      
                       
                           
         
         
	                               
	 
                    
                       
             

                                                           
             

           
         
       
                                 
     
    return (0);
  }
}
unsigned int outcnt;
long bytes_out;
int unpack(int in, int out);
void flush_window(void);
static ulg orig_len;
static int max_len;
static uch literal[256];
static int lit_base[26];
static int leaves[26];
static int parents[26];
static int peek_bits;
static ulg bitbuf___0;
static int valid;
static void read_tree(void);
static void build_tree(void);
static void read_tree(void) {
  int len;
  int base;
  int n;
  unsigned int tmp;
  int tmp___0;
  int tmp___1;
  unsigned int tmp___2;
  int tmp___3;
  int tmp___4;
  unsigned int tmp___5;
  int tmp___6;
  int tmp___7;
  int tmp___8;
  unsigned int tmp___9;
  int tmp___10;
  int tmp___11;
                    
                    

   
                      
          
     
                 
                                      
                        
                           
         
                             
                      
                  
                                    
                
           

                                    
                              
           

         
                                                  
            
       
                                 
     
                         
                      
              
                                    
            
       

                                
                          
       

     
                      
                       
       

                                                                           
       

     
          
            
     
                 
                                          
                                
                               
         
                             
                          
                  
                                        
                
           

                                    
                              
           

         
                              
                         
              
       
                                     
     
                  
       

                                                         
       

     
                        
             
            
     
                 
                                          
                                
                               
         
                             
                        
         
                     
                                              
                           
                                   
             
                           
                   
                                 
                              
                      
                                             
                    
               

                                         
                                    
               

             
                                             
                
           
                                         
         
              
       
                                     
     
                        
           
   
}
static void build_tree(void) {
  int nodes;
  int len;
  uch *prefixp;
  int prefixes;
  int tmp;

   
              
                  
     
                 
                                      
                          
                           
         
                    
                             
                               
                             
              
       
                                 
     
                        
                          
            
                     
     
                                      
            
     
                 
                                          
                                  
                               
         
                                                    
         
                     
                                              
                           
                       
                       
                                   
             
                      
                                
           
                                         
         
              
       
                                     
     
     
                 
                                          
                                                                  
                               
         
                  
                          
       
                                     
     
           
   
}
int unpack(int in, int out) {
  int len;
  unsigned int eob;
  register unsigned int peek;
  unsigned int peek_mask;
  unsigned int tmp;
  int tmp___0;
  int tmp___1;
  ulg mask;
  unsigned int tmp___2;
  int tmp___3;
  int tmp___4;
  unsigned int tmp___5;
                    

  {
     
               
                
                  
                   
                
                          
                                                       
                                                
     
     
                 
                                      
         
                     
                                              
                                       
                                   
             
                                 
                          
                      
                                        
                    
               

                                        
                                  
               

             
                                                          
                       
           
                                         
         
                                                                   
                                                        
                                
                      
                                   
                
                                
                          
           
                       
                                                
                    
                                       
               
                           
                                                    
                                       
                                         
                   
                                       
                                    
                            
                                                  
                          
                     

                                              
                                        
                     

                   
                                                                
                             
                 
                                               
               
                                                                          
                                                         
                                     
               
             
                                           
           
         
                          
                               
                             
           
         
                         
                 
                                                                      
                               
           

                           
           

         
                     
       
                                 
     
     
                     
     
                                     
       

                                                               
       

     
    return (0);
  }
}
extern ssize_t read(int __fd, void *__buf, size_t __nbytes);
extern struct _IO_FILE *stderr;
extern int fprintf(FILE *__restrict __stream, char const *__restrict __format,
                   ...);
extern __attribute__((__nothrow__)) void *(
    __attribute__((__nonnull__(1, 2), __leaf__))
    memcpy)(void *__restrict __dest, void const *__restrict __src, size_t __n);
extern
    __attribute__((__nothrow__)) void *(__attribute__((__nonnull__(1),
                                                       __leaf__))
                                        memset)(void *__s, int __c, size_t __n);
long bytes_in;
char ifname[1024];
char *progname;
int exit_code;
int quiet;
int to_stdout;
void read_error(void);
int maxbits;
               
int unlzw(int in, int out);
int block_mode = 128;
int unlzw(int in, int out) {
  register char_type *stackp;
  code_int code;
  int finchar;
  code_int oldcode;
  code_int incode;
  long inbits;
  long posbits;
  int outpos;
  unsigned int bitmask;
  code_int free_ent;
  code_int maxcode;
  code_int maxmaxcode;
  int n_bits;
  int rsize;
  unsigned int tmp;
  int tmp___0;
  register int i___0;
  int e;
  int o;
  register char_type *p;
  int tmp___1;
  char const *tmp___2;
  register int i___1;
                    
                    
                    

  {
                         
                  
              
                                
            
       

                                
                          
       

     
                               
                              
                   
         

                                                    
                                                                                
                                                                  
                                                  
         

       
                           
                      
       
     
                  
                               
                      {
       
                
                                              
                                                                            
                                                                               
                                           
                      
       
      return (1);
    }
                        
               
                                  
                                                
                           
                
               
                                 
                     
                               
            
                               
     
     
                                            
                           
     
     
                 
                                      
                            
                           
         
                                       
               
       
                                 
     
     
                 
                                          
               
                                
                                            
                  
         
                     
                                              
                               
                                   
             
                                            
                    
           
                                         
         
                                 
                     
                           
           
                                                                     
                                             
           
                            
             

                           
             

           
                                        
                                                        
         
                         
                                                                               
                
                                                            
         
         
                     
                                              
                                      
                                   
             
                                     
                       
                                                         
                                                                            
                                                             
                       
                                      
                                     
                      
                                              
               
                                                          
                            
             
                                     
                                                               
                                               
                                     
                                 
                                    
                                 
                                 
                 

                                                  
                 

               
                               
                       
                             
                                     
                                                   
                                      
             
                               
                               
                 
                                                        
                                           
                           
                                                             
                                                                                
                                                                 
                             
                                                
                                                              
                 
                              
               
             
                          
                                                  
                                   
                                    
                            
                                   
                     

                                                               
                                                      
                                                                       
                     

                   
                 
                                
                                             
                        
                                                                            
                 
                 
		                         
		 
               
                       
                                           
                             
             
             
                         
                                                  
                                                     
                                       
                 
                         
                                       
                                            
               
                                             
             
                     
                                        
                                         
                                                                 
                                          
               

                           
                                                    
                                               
                                           
                   
                                  
                     

                                                                         
                                                                    
                                            
                                      
                     

                   
                                        
                                
                       

                                                                 
                                                        
                                                                         
                       

                     
                               
                   
                                  
                                                                       
                                     
                                         
                   
                 
                                               
               

                    
               

                                                                   
                                                                              
                                
               

             
                            
                                    
                                                   
                                                
                                   
             
                             
           
                                         
         
                            
                               
         
       
                                     
     
                
                       
         

                                                                          
                                                           
         

       
     
               
  }
}
int lzw(int in, int out);
static int msg_done = 0;
int lzw(int in, int out) {

   

                   
                 
     
     
                   
                                                
                                                                              
                                                                  
     
                    
                    
     
    return (1);
   

}
extern __attribute__((__nothrow__)) unsigned short const **(
    __attribute__((__leaf__)) __ctype_b_loc)(void)__attribute__((__const__));
extern __attribute__((__nothrow__)) int *(
    __attribute__((__leaf__)) __errno_location)(void)__attribute__((__const__));
extern ssize_t write(int __fd, void const *__buf, size_t __n);
extern __attribute__((__nothrow__)) void *(__attribute__((__leaf__))
                                           malloc)(size_t __size)
    __attribute__((__malloc__));
extern
    __attribute__((__nothrow__)) void *(__attribute__((__leaf__))
                                        calloc)(size_t __nmemb, size_t __size)
        __attribute__((__malloc__));
extern __attribute__((__nothrow__)) void(__attribute__((__leaf__))
                                         free)(void *__ptr);
extern __attribute__((__nothrow__)) char *(__attribute__((__nonnull__(1),
                                                          __leaf__))
                                           getenv)(char const *__name);
extern int _IO_putc(int __c, _IO_FILE *__fp);
extern void perror(char const *__s);
extern __attribute__((__nothrow__)) char *(
    __attribute__((__nonnull__(1, 2), __leaf__))
    strcpy)(char *__restrict __dest, char const *__restrict __src);
extern __attribute__((__nothrow__)) char *(__attribute__((__nonnull__(1),
                                                          __leaf__))
                                           strrchr)(char const *__s, int __c)
    __attribute__((__pure__));
extern __attribute__((__nothrow__))
size_t(__attribute__((__nonnull__(1, 2), __leaf__))
       strcspn)(char const *__s, char const *__reject)
    __attribute__((__pure__));
extern __attribute__((__nothrow__))
size_t(__attribute__((__nonnull__(1, 2), __leaf__))
       strspn)(char const *__s, char const *__accept) __attribute__((__pure__));
extern __attribute__((__nothrow__))
size_t(__attribute__((__nonnull__(1), __leaf__)) strlen)(char const *__s)
    __attribute__((__pure__));
char ofname[1024];
void abort_gzip(void);
int copy(int in, int out);
ulg updcrc(uch *s, unsigned int n);
void clear_bufs(void);
void flush_outbuf(void);
char *strlwr(char *s);
char *basename(char *fname);
char *add_envopt(int *argcp, char ***argvp, char *env___0);
void write_error(void);
void display_ratio(long num, long den, FILE *file);
voidp xmalloc(unsigned int size);
ulg crc_32_tab[256];
int copy(int in, int out) {
  int *tmp;
  int *tmp___0;

  {
     
                               
               
     
     
                 
                                      
                           
                                     
                             
           
                
                           
         
         
                                                           
                                    
                  
                                                                               
         
       
                                 
     
                            
       
                                     
       
                          
         

                       
         

       
     
                         
    return (0);
  }
}
static ulg crc = (ulg)4294967295L;
ulg updcrc(uch *s, unsigned int n) {
  register ulg c;
  uch *tmp;

  {
                                                        {
      c = (ulg)4294967295L;
    }       
              
              
         

                     
                                          
                    
                
                                                                  
                
                     
                               
             
           
                                     
         

       
     
            
    return (c ^ 4294967295UL);
  }
}
void clear_bufs(void) {

   

                
               
                   
                   
                         
           
   

}
int fill_inbuf(int eof_ok) {
  int len;
  int *tmp;

  {
     
                  
                               
               
     
    {
                {
                                     ;
        {
          len = (int)read(ifd, (void *)((char *)(inbuf) + insize),
                          (size_t)(32768U - insize));
        }
                       
                           
                
                          
                             
           
         
        insize += (unsigned int)len;
                                 
                           
         
      }
    while_break: /* CIL Label */;
    }
                       
                   
                    
       
       
	             
       
     
                                                   
    inptr = 1U;
    return ((int)inbuf[0]);
  }
}
void flush_outbuf(void) {

   

                       
             
     
    {
      write_buf(ofd, (voidp)((char *)(outbuf)), outcnt);
                                                       
                  
    }
           
   

}
void flush_window(void) {

   

                       
             
     
     
                             
     
                
       

                                                          
       

     
                                                     
                
           
   

}
void write_buf(int fd, voidp buf, unsigned int cnt) {
  unsigned int n;

  {
    {
                {
                                     ;
        {
	  n = (unsigned int)write(fd, (void const *)buf, (size_t)cnt);
	}
                          
                           
         
                               
           

                          
           

         
                 
                                       
      }
    while_break: /* CIL Label */;
    }
           
  }
}
char *strlwr(char *s) {
  char *t;
  unsigned short const **tmp___0;

  {
          
     
                 
                                      
                  
                           
         
         
	                            
	 
                                                       
                                           
                
                  
         
            
       
                                 
     
    return (s);
  }
}
char *basename(char *fname) {
  char *p;

  {
     
                                            
     
                                                         
                    
     
    return (fname);
  }
}
char *add_envopt(int *argcp, char ***argvp, char *env___0) {
  char *p;
  char **oargv;
  char **nargv;
  int oargc;
  int nargc;
  char *tmp;
  size_t tmp___0;
  size_t tmp___2;
  size_t tmp___3;
  char *tmp___4;
  int tmp___6;
  char **tmp___7;
  char **tmp___8;
  size_t tmp___9;
  char **tmp___10;
  char *tmp___11;
  char **tmp___12;
  char **tmp___13;
  int tmp___14;
                    
                    

  {
    {
                     
                
      tmp = getenv((char const *)env___0);
      env___0 = tmp;
    }
                                                              {
      return ((char *)((void *)0));
    }
     
                                              
                                                         
                                                   
                                                                
                  
     
     
                 
                                      
                  
                           
         
         
                                                   
                       
         
                           
                           
         
         
                                                    
                       
         
                 
                      
              
                                  
         
                
       
                                 
     
                     
       
                              
       
                                   
     
     
                      
                                                                    
     
                                                             
       

                                       
       

     
                   
                   
                    
            
                      
       

                                 
       

     
                    
            
                    
            
                        
                
     
                 
                                          
                           
                               
         
         
                                                   
                       
                           
                  
                        
         
         
                     
                                              
                         
                
                             
                                   
             
           
                                         
         
                
       
                                     
     
     
                 
                                          
                         
                
                        
                               
         
                         
                
                         
                
                              
       
                                     
     
                                 
                     
  }
}
void error(char *m) {

   

     
                                                
                                                                          
                         
                   
     
           
   

}
void read_error(void) {
  int *tmp;

   
     
                                                
                                                                   
                               
     
                    
       

                                       
       

            
       

                                                  
                                                                                
                        
       

     
     
                   
     
           
   
}
void write_error(void) {

   

     
                                                
                                                                   
                                     
                   
     
           
   

}
void display_ratio(long num, long den, FILE *file) {
  long ratio;

  {
                    
                 
           {
                          {
        ratio = (1000L * num) / den;
      }       
                                    
       
    }
                     
       

                            
                       
       

           {
       

	_IO_putc(' ', file);
       

    }
    {
      fprintf((FILE * /* __restrict  */) file,
              (char const * /* __restrict  */) "%2ld.%1ld%%", ratio / 10L,
              ratio % 10L);
    }
           
  }
}
voidp xmalloc(unsigned int size) {
  voidp cp;
  voidp tmp;
                   

  {
    {
      tmp = malloc((size_t)size);
      cp = tmp;
    }
                                                          
       

                                       
       

     
    return (cp);
  }
}
                       
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           
                                                                           
                                                                         
                                                                         
                                                                          
                                                                          
                                                                          
                                                                          
                                                                          
                                                                          
                                                                          
                                                                           
                                                                          
                                                                          
                                                                          
                                                                          
                                                                          
                                                                           
int inflate(void);
int huft_build(unsigned int *b, unsigned int n, unsigned int s, ush *d, ush *e,
               struct huft **t, int *m);
int huft_free(struct huft *t);
int inflate_codes(struct huft *tl, struct huft *td, int bl, int bd);
int inflate_stored(void);
int inflate_fixed(void);
int inflate_dynamic(void);
int inflate_block(int *e);
static unsigned int border[19] = {16U, 17U, 18U, 0U, 8U,  7U, 9U,  6U, 10U, 5U,
                                  11U, 4U,  12U, 3U, 13U, 2U, 14U, 1U, 15U};
static ush cplens[31] = {
    (ush)3,   (ush)4,  (ush)5,   (ush)6,   (ush)7,   (ush)8,   (ush)9,
    (ush)10,  (ush)11, (ush)13,  (ush)15,  (ush)17,  (ush)19,  (ush)23,
    (ush)27,  (ush)31, (ush)35,  (ush)43,  (ush)51,  (ush)59,  (ush)67,
    (ush)83,  (ush)99, (ush)115, (ush)131, (ush)163, (ush)195, (ush)227,
    (ush)258, (ush)0,  (ush)0};
static ush cplext[31] = {
    (ush)0, (ush)0, (ush)0, (ush)0, (ush)0, (ush)0,  (ush)0, (ush)0,
    (ush)1, (ush)1, (ush)1, (ush)1, (ush)2, (ush)2,  (ush)2, (ush)2,
    (ush)3, (ush)3, (ush)3, (ush)3, (ush)4, (ush)4,  (ush)4, (ush)4,
    (ush)5, (ush)5, (ush)5, (ush)5, (ush)0, (ush)99, (ush)99};
static ush cpdist[30] = {
    (ush)1,    (ush)2,    (ush)3,    (ush)4,     (ush)5,     (ush)7,
    (ush)9,    (ush)13,   (ush)17,   (ush)25,    (ush)33,    (ush)49,
    (ush)65,   (ush)97,   (ush)129,  (ush)193,   (ush)257,   (ush)385,
    (ush)513,  (ush)769,  (ush)1025, (ush)1537,  (ush)2049,  (ush)3073,
    (ush)4097, (ush)6145, (ush)8193, (ush)12289, (ush)16385, (ush)24577};
static ush cpdext[30] = {(ush)0,  (ush)0,  (ush)0,  (ush)0,  (ush)1,  (ush)1,
                         (ush)2,  (ush)2,  (ush)3,  (ush)3,  (ush)4,  (ush)4,
                         (ush)5,  (ush)5,  (ush)6,  (ush)6,  (ush)7,  (ush)7,
                         (ush)8,  (ush)8,  (ush)9,  (ush)9,  (ush)10, (ush)10,
                         (ush)11, (ush)11, (ush)12, (ush)12, (ush)13, (ush)13};
ulg bb;
unsigned int bk;
ush mask_bits[17] = {(ush)0,     (ush)1,    (ush)3,    (ush)7,    (ush)15,
                     (ush)31,    (ush)63,   (ush)127,  (ush)255,  (ush)511,
                     (ush)1023,  (ush)2047, (ush)4095, (ush)8191, (ush)16383,
                     (ush)32767, (ush)65535};
int lbits = 9;
int dbits = 6;
unsigned int hufts;
int huft_build(unsigned int *b, unsigned int n, unsigned int s, ush *d, ush *e,
               struct huft **t, int *m) {
  unsigned int a;
  unsigned int c[17];
  unsigned int f;
  int g;
  int h;
  register unsigned int i___0;
  register unsigned int j___0;
  register int k;
  int l;
  register unsigned int *p;
  register struct huft *q;
  struct huft r;
  struct huft *u[16];
  unsigned int v[288];
  register int w;
  unsigned int x[17];
  unsigned int *xp;
  int y;
  unsigned int z;
  unsigned int *tmp;
  unsigned int *tmp___0;
  unsigned int tmp___1;
  unsigned int *tmp___2;
  int tmp___4;
  unsigned int *tmp___5;
  unsigned int tmp___6;
  int tmp___7;
                    
                    
                    
                    
                  

  {
     
                                       
            
                
     
     
                 
                                      
                  
            
                
                     
                           
         
       
                                 
     
                    
                                      
             
                 
     
           
               
     
                 
                                          
                              
                               
         
                       
                               
         
                
       
                                     
     
                   
                                  
                     
     
                
     
                 
                                          
                     
                               
         
                       
                               
         
                
       
                                     
     
                   
                                  
                     
     
           
                   
     
                 
                                          
                               
                               
         
                                              
                    
                     
         
                
                
       
                                     
     
    y = (int)((unsigned int)y - c[i___0]);
               {
      return (2);
    }
    c[i___0] += (unsigned int)y;
               
                 
              
               
     
                 
                                          
                
                     
                               
         
                 
             
                    
            
                          
                     
       
                                     
     
          
               
     
                 
                                          
                    
            
                         
                          
                             
                       
                             
         
                
                           
                               
         
       
                                     
     
               
                 
          
           
           
                                      
                                   
           
     
                 
                                          
                        
                               
         
                 
         
                     
                                              
                        
                
                           
                                   
             
             
                         
                                                  
                                   
                                       
                 
                    
                       
                                          
                                          
                                      
                        
                        
                 
                                              
                                               
                                 
                              
                             
                   
                               
                                                        
                              
                                         
                                             
                       
                              
                           
                                     
                                             
                       
                               
                     
                                                   
                   
                 
                 
                                                 
                                                                     
                                                                 
                 
                                       
                                                                  
                          
                     

                                      
                     

                   
                             
                 
                                
                           
                            
                                                
                    
                         
                        
                               
                               
                                           
                            
                                           
                                          
                 
               
                                             
             
                               
                                                             
                            
                    
                           
                                
                               
                        
                               
                 
                                   
                                
                    
                      
                                             
                            
                    
                                              
               
             
                                             
                               
             
                         
                                                  
                                   
                                       
                 
                                 
                           
               
                                             
             
                                                 
             
                         
                                                   
                                       
                                        
                 
                               
                            
               
                                              
             
                           
             
                         
                                                   
                                                                        
                                        
                 
                    
                       
               
                                              
             
           
                                         
         
            
       
                                     
     
                 
                   
                    
              
                    
       
            
                  
     
                     
  }
}
int huft_free(struct huft *t) {
  register struct huft *p;
  register struct huft *q;

  {
          
     
                 
                                      
                                 
                                                             
                           
         
         
              
                     
                                    
                
         
       
                                 
     
    return (0);
  }
}
int inflate_codes(struct huft *tl, struct huft *td, int bl, int bd) {
  register unsigned int e;
  unsigned int n;
  unsigned int d;
  unsigned int w;
  struct huft *t;
  unsigned int ml;
  unsigned int md;
  register ulg b;
  register unsigned int k;
  unsigned int tmp;
  int tmp___0;
  int tmp___1;
  unsigned int tmp___2;
  int tmp___3;
  int tmp___4;
  unsigned int tmp___5;
  unsigned int tmp___6;
  int tmp___7;
  int tmp___8;
  unsigned int tmp___9;
  int tmp___10;
  int tmp___11;
  unsigned int tmp___12;
  int tmp___13;
  int tmp___14;
  unsigned int tmp___15;
  int tmp___16;
  int tmp___17;
  unsigned int tmp___19;
  unsigned int tmp___20;
  unsigned int tmp___21;

  {
           
           
               
    ml = (unsigned int)mask_bits[bl];
    md = (unsigned int)mask_bits[bd];
    {
                {
                                     ;
         
                     
                                              
                                          
                                   
             
                                 
                          
                      
                                        
                    
               

                                        
                                  
               

             
                                          
                    
           
                                         
         
                                        
                               
                      
           

                       
                                                
                             
                           
               
                              
                                      
                       
               
                           
                                                    
                                 
                                         
                   
                                       
                                    
                            
                                                  
                          
                     

                                              
                                        
                     

                   
                                                
                          
                 
                                               
               
                                                                          
                                     
                               
                                     
               
             
                                           
           

         
                        
                                
                      {
                      
              
                                        
                            
             

                         
                             
                     
             

           
        }       
                         
                             
           
           
                       
                                                
                             
                                     
               
                                   
                                
                        
                                              
                      
                 

                                          
                                    
                 

               
                                            
                      
             
                                           
           
                                    
                                                             
                  
                 
           
                       
                                                
                                            
                                     
               
                                   
                                
                        
                                               
                      
                 

                                           
                                      
                 

               
                                             
                      
             
                                           
           
                                          
                                 
                        
             

                         
                                                  
                               
                             
                 
                                
                                        
                         
                 
                             
                                                      
                                   
                                           
                     
                                         
                                       
                              
                                                      
                            
                       

                                                 
                                            
                       

                     
                                                   
                            
                   
                                                 
                 
                                                                            
                                       
                                 
                                       
                 
               
                                             
             

           
                          
                                  
           
                       
                                                
                             
                                     
               
                                   
                                 
                        
                                                
                      
                 

                                           
                                      
                 

               
                                             
                      
             
                                           
           
                                          
                                                             
                  
                 
           
                       
                                                
                          
                          
                             
                      
                             
               
                                    
                          
                      
                      
                      
               
                     
                               
                 

                                                                
                                                                      
                                    
                         
                         
                 

                      
                 

                             
                                                      
                                 
                        
                                 
                        
                                                        
                        
                             
                                           
                     
                   
                                                 
                 

               
                                
                 

                             
                                 
                         
                 

               
                       
                                     
               
             
                                           
           
         
      }
    while_break: /* CIL Label */;
    }
               
           
           
               
  }
}
int inflate_stored(void) {
  unsigned int n;
  unsigned int w;
  register ulg b;
  register unsigned int k;
  unsigned int tmp;
  int tmp___0;
  int tmp___1;
  unsigned int tmp___2;
  int tmp___3;
  int tmp___4;
  unsigned int tmp___5;
  int tmp___6;
  int tmp___7;
  unsigned int tmp___8;
  unsigned int tmp___9;

  {
           
           
               
               
            
           
     
                 
                                      
                         
                           
         
                             
                      
                  
                                    
                
           

                                    
                              
           

         
                                      
                
       
                                 
     
                                 
             
             
     
                 
                                          
                         
                               
         
                             
                          
                  
                                        
                
           

                                    
                              
           

         
                                      
                
       
                                     
     
                                           {
      return (1);
    }
             
             
    {
                 
                                          
                    
            
                       
                               
         
         
                     
                                              
                            
                                   
             
                                 
                              
                      
                                            
                    
               

                                        
                                  
               

             
                                          
                    
           
                                         
         
                    
            
                                 
                          
           

                       
                           
                   
           

         
                
                
       
                                     
    }
               
           
           
               
  }
}
int inflate_fixed(void) {
  int i___0;
  struct huft *tl;
  struct huft *td;
  int bl;
  int bd;
  unsigned int l[288];
  int tmp;
                   

  {
              
     
                 
                                      
                             
                           
         
                      
                
       
                                 
     
     
                 
                                          
                             
                               
         
                      
                
       
                                     
     
     
                 
                                          
                             
                               
         
                      
                
       
                                     
     
     
                 
                                          
                             
                               
         
                      
                
       
                                     
     
     
             
                                                                  
     
                    {
      return (i___0);
    }
    i___0 = 0;
     
                 
                                          
                            
                               
         
                      
                
       
                                     
     
     
             
                                                               
     
                    
       
                      
       
                     
     
     
                                          
     
              
                 
     
     
                    
                    
     
               
  }
}
int inflate_dynamic(void) {
  int i___0;
  unsigned int j___0;
  unsigned int l;
  unsigned int m;
  unsigned int n;
  struct huft *tl;
  struct huft *td;
  int bl;
  int bd;
  unsigned int nb;
  unsigned int nl;
  unsigned int nd;
  unsigned int ll[316];
  register ulg b;
  register unsigned int k;
  unsigned int tmp;
  int tmp___0;
  int tmp___1;
  unsigned int tmp___2;
  int tmp___3;
  int tmp___4;
  unsigned int tmp___5;
  int tmp___6;
  int tmp___7;
  unsigned int tmp___8;
  int tmp___9;
  int tmp___10;
  unsigned int tmp___11;
  int tmp___12;
  int tmp___13;
  int tmp___14;
  unsigned int tmp___15;
  int tmp___16;
  int tmp___17;
  int tmp___18;
  unsigned int tmp___19;
  unsigned int tmp___20;
  int tmp___21;
  int tmp___22;
  int tmp___23;
  unsigned int tmp___24;
  unsigned int tmp___25;
  int tmp___26;
  int tmp___27;
  int tmp___28;
  unsigned int tmp___29;
  int tmp___30;
                    

  {
           
           
     
                 
                                      
                        
                           
         
                             
                      
                  
                                    
                
           

                                    
                              
           

         
                                      
                
       
                                 
     
    nl = 257U + ((unsigned int)b & 31U);
            
            
     
                 
                                          
                        
                               
         
                             
                          
                  
                                        
                
           

                                    
                              
           

         
                                      
                
       
                                     
     
    nd = 1U + ((unsigned int)b & 31U);
            
            
     
                 
                                          
                        
                               
         
                             
                          
                  
                                        
                
           

                                    
                              
           

         
                                      
                
       
                                     
     
    nb = 4U + ((unsigned int)b & 15U);
            
            
                   {
      return (1);
    }       
                     
                   
       
     
               
    {
                {
                                         ;
                            
                               
         
         
                     
                                              
                            
                                   
             
                                 
                              
                      
                                             
                    
               

                                        
                                   
               

             
                                           
                    
           
                                         
         
                                                 
        b >>= 3;
                
                
      }
    while_break___2: /* CIL Label */;
    }
     
                 
                                          
                             
                               
         
                               
                
       
                                     
     
     
             
                                                                              
                                   
     
                     
                       
         

                        
         

       
                     
     
                
                                    
           
                   
     
                 
                                          
                                         
                               
         
         
                     
                                              
                                          
                                   
             
                                 
                               
                      
                                              
                    
               

                                         
                                    
               

             
                                           
                    
           
                                         
         
                                        
                                    
                    
                   
                                      
                          
                           
                  
                    
                           
                
                             
             
                         
                                                  
                                
                                       
                 
                                     
                                   
                          
                                                  
                        
                   

                                             
                                        
                   

                 
                                               
                        
               
                                             
             
                                                
                    
                    
                                                  
                         
             
             
                         
                                                  
                                 
                        
                                
                                       
                 
                                 
                        
                                 
               
                                             
             
                  
                               
               
                           
                                                    
                                  
                                         
                   
                                       
                                     
                            
                                                    
                          
                     

                                               
                                          
                     

                   
                                                 
                          
                 
                                               
               
                                                  
                      
                      
                                                    
                           
               
               
                           
                                                     
                                   
                          
                                  
                                          
                   
                                   
                          
                                    
                 
                                                
               
                     
                    
               
                           
                                                     
                                  
                                          
                   
                                       
                                     
                            
                                                    
                          
                     

                                               
                                          
                     

                   
                                                 
                          
                 
                                                
               
                                                     
                      
                      
                                                    
                           
               
               
                           
                                                     
                                   
                          
                                  
                                          
                   
                                   
                          
                                    
                 
                                                
               
                     
             
           
         
       
                                     
     
     
                    
             
             
                 
                                                                 
     
                     
                       
         

                  
                                                
                                                                             
                        
         

       
                     
     
     
                 
                                                                    
     
                     
                       
         

                  
                                                
                                                                              
                        
         

       
       
	              
       
                     
     
     
                                               
     
                   
                 
     
     
                    
                    
     
               
  }
}
int inflate_block(int *e) {
  unsigned int t;
  register ulg b;
  register unsigned int k;
  unsigned int tmp;
  int tmp___0;
  int tmp___1;
  unsigned int tmp___2;
  int tmp___3;
  int tmp___4;
  int tmp___5;
  int tmp___6;
  int tmp___7;

  {
           
           
     
                 
                                      
                        
                           
         
                             
                      
                  
                                    
                
           

                                    
                              
           

         
                                      
                
       
                                 
     
                    
            
        
     
                 
                                          
                        
                               
         
                             
                          
                  
                                        
                
           

                                    
                              
           

         
                                      
                
       
                                     
     
                             
            
            
           
           
                  
       
                                    
       
                       
     
                  
       
                                   
       
                       
     
                  
       
                                  
       
                       
     
    return (2);
  }
}
int inflate(void) {
  int e;
  int r;
  unsigned int h;

  {
                
            
                
           
     
                 
                                      
         
                     
                                
         
                     
                     
         
                        
                    
         
                    
                           
         
       
                                 
     
     
                 
                                          
                          
                               
         
                 
                
       
                                     
     
     
                      
                     
     
    return (0);
  }
}
int method;
int decrypt;
int unzip(int in, int out);
int check_zipfile(int in);
          
int pkzip = 0;
int ext_header = 0;
int check_zipfile(int in) {
  uch *h;

  {
                      
             
                                                                  
                                                                           
                                                            
                                                                    
                         
       
                
                                              
                                                                                
                              
                      
       
                 
            
                                                                          
                                                
                                                      
                                    
         
                                                    
                             
                                                                               
                                    
                        
         
                   
       
     
                           
                     {
                       {
         
                                                    
                             
                                                                        
                                                                                
                                    
                        
         
        return (1);
      }
    }
                                
                       
       
                
                                              
                       
                                                                                
                              
                      
       
                 
     
                                          
              
               
  }
}
int unzip(int in, int out) {
  ulg orig_crc;
  ulg orig_len___0;
  int n;
  uch buf[16];
  int res;
  int tmp;
  register ulg n___0;
  int tmp___0;
  uch c;
  unsigned int tmp___1;
  int tmp___2;
  int tmp___3;
  unsigned int tmp___4;
  ulg tmp___5;
  unsigned int tmp___6;
  int tmp___7;
  int tmp___8;
  unsigned int tmp___9;
  int tmp___10;
  int tmp___11;
  ulg tmp___12;
                    
                    
                    
                    
                    
                    
                    
                    

  {
     
                        
                            
               
                
                                     
     
                
                        
                                                            
                                                                    
                                                                   
                                                                         
                           
                                                                
                                                                        
                                                                       
                                                                             
                               
       
     
                      
       
                        
                  
       
                     
         

                                         
         

              
                       
           

                                                                      
           

         
       
            
                  
                          
                                                           
                                                                   
                                                                  
                                                                        
                          
                        
                         
                  
                        
           
                                                                 
                                                                         
                                                                        
                                                                              
                                  
                                                    
             

                                                        
                                                                            
                            
                                                               
                                                                       
                                                                          
                                                                                
                                   
                                                                        
             

           
           
                       
                                            
                              
                      
                             
                                 
               
                                   
                                
                        
                                              
                      
                 

                                          
                                    
                 

               
                               
                               
                       
                                  
                                     
                 

                                 
                 

               
             
                                       
           
           
	                   
	   
                
           

	                                                    
	   

         
              
         

	                                                  
	 

       
     
                 
            
       
                   
                                            
                         
                                 
           
                               
                            
                    
                                          
                  
             

                                      
                                
             

           
                                
              
         
                                       
       
                                                                        
                                                        
                                                              
                         
                                                           
                                                                   
                                                                  
                                                                        
                             
            
                       
              
         
                     
                                              
                            
                                   
             
                                 
                              
                      
                                             
                    
               

                                         
                                    
               

             
                                   
                
           
                                         
         
                                                         
                                                                 
                                                                
                                                                      
                           
                                                              
                                                                      
                                                                     
                                                                           
                               
       
     
     
                                    
     
                               
       

                                                            
       

     
                                         
       

                                                               
       

     
                
                                
                                                         
                                                                 
                                                                
                                                                      
                                      
                          
                         
               

                        
                                                      
                                                                                
                                                                             
                                      
               

             
                                 
                            
             
                  
             
                      
                                                    
                                                                              
                                                                          
                                    
                            
                        
                                 
             
                       
           
         
       
     
              
                       
    return (0);
  }
}
int file_read(char *buf, unsigned int size);
void bi_init(file_t zipfile);
void send_bits(int value, int length);
unsigned int bi_reverse(unsigned int code, int len);
void bi_windup(void);
void copy_block(char *buf, unsigned int len, int header);
int (*read_buf)(char *buf, unsigned int size);
static file_t zfile;
static unsigned short bi_buf;
static int bi_valid;
void bi_init(file_t zipfile) {

   

                    
                               
                 
                     {
      read_buf = &file_read;
    }
           
   

}
void send_bits(int value, int length) {
  unsigned int tmp;
  unsigned int tmp___0;
  unsigned int tmp___1;
  unsigned int tmp___2;

  {
    if (bi_valid > (int)(16UL * sizeof(char)) - length) {
      bi_buf = (unsigned short)((int)bi_buf | (value << bi_valid));
                           {
        tmp = outcnt;
        outcnt++;
        outbuf[tmp] = (uch)((int)bi_buf & 255);
        tmp___0 = outcnt;
        outcnt++;
        outbuf[tmp___0] = (uch)((int)bi_buf >> 8);
      }       
                         
                 
                                                   
                               
           

                           
           

         
                         
                 
                                                  
                               
           

                           
           

         
       
      bi_buf = (unsigned short)((int)((ush)value) >> (16UL * sizeof(char) -
                                                      (unsigned long)bi_valid));
      bi_valid = (int)((unsigned long)bi_valid +
                       ((unsigned long)length - 16UL * sizeof(char)));
    } else {
      bi_buf = (unsigned short)((int)bi_buf | (value << bi_valid));
      bi_valid += length;
    }
           
  }
}
unsigned int bi_reverse(unsigned int code, int len) {
  register unsigned int res;

  {
    res = 0U;
    {
      while (1) {
                                     ;
        res |= code & 1U;
        code >>= 1;
        res <<= 1;
        len--;
        if (!(len > 0)) {
          goto while_break;
        }
      }
    while_break: /* CIL Label */;
    }
    return (res >> 1);
  }
}
void bi_windup(void) {
  unsigned int tmp;
  unsigned int tmp___0;
  unsigned int tmp___1;
  unsigned int tmp___2;
  unsigned int tmp___3;

  {
                      {
                           {
        tmp = outcnt;
        outcnt++;
        outbuf[tmp] = (uch)((int)bi_buf & 255);
        tmp___0 = outcnt;
        outcnt++;
        outbuf[tmp___0] = (uch)((int)bi_buf >> 8);
      }       
                         
                 
                                                   
                               
           

                           
           

         
                         
                 
                                                  
                               
           

                           
           

         
       
    }       
                         
                         
                 
                                      
                               
           

                           
           

         
       
     
    bi_buf = (unsigned short)0;
    bi_valid = 0;
           
  }
}
void copy_block(char *buf, unsigned int len, int header) {
  unsigned int tmp;
  unsigned int tmp___0;
  unsigned int tmp___1;
  unsigned int tmp___2;
  unsigned int tmp___3;
  unsigned int tmp___4;
  unsigned int tmp___5;
  unsigned int tmp___6;
  unsigned int tmp___7;
  char *tmp___8;
  unsigned int tmp___9;

   
     
                  
     
                 
                            
                     
                 
                                                   
                         
                 
                                                      
              
                         
                 
                                                       
                               
           

                           
           

         
                         
                 
                                                      
                               
           

                           
           

         
       
                            
                         
                 
                                                          
                         
                 
                                                         
              
                         
                 
                                                          
                               
           

                           
           

         
                         
                 
                                                         
                               
           

                           
           

         
       
     
     
                 
                                      
                      
              
                       
                           
         
                         
                 
                      
              
                                        
                               
           

                           
           

         
       
                                 
     
           
   
}
int level;
void ct_init(ush *attr, int *methodp);
int ct_tally(int dist, int lc);
ulg flush_block(char *buf, ulg stored_len, int eof);
static int extra_lbits[29] = {0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2,
                              2, 3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 0};
static int extra_dbits[30] = {0, 0, 0,  0,  1,  1,  2,  2,  3,  3,
                              4, 4, 5,  5,  6,  6,  7,  7,  8,  8,
                              9, 9, 10, 10, 11, 11, 12, 12, 13, 13};
static int extra_blbits[19] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                               0, 0, 0, 0, 0, 0, 2, 3, 7};
static ct_data dyn_ltree[573];
static ct_data dyn_dtree[61];
static ct_data static_ltree[288];
static ct_data static_dtree[30];
static ct_data bl_tree[39];
static tree_desc l_desc = {dyn_ltree, static_ltree, extra_lbits, 257, 286, 15,
                           0};
static tree_desc d_desc = {dyn_dtree, static_dtree, extra_dbits, 0, 30, 15, 0};
static tree_desc bl_desc = {bl_tree, (ct_data *)0, extra_blbits, 0, 19, 7, 0};
static ush bl_count[16];
static uch bl_order[19] = {(uch)16, (uch)17, (uch)18, (uch)0,  (uch)8,
                           (uch)7,  (uch)9,  (uch)6,  (uch)10, (uch)5,
                           (uch)11, (uch)4,  (uch)12, (uch)3,  (uch)13,
                           (uch)2,  (uch)14, (uch)1,  (uch)15};
static int heap[573];
static int heap_len;
static int heap_max;
static uch depth[573];
static uch length_code[256];
static uch dist_code[512];
static int base_length[29];
static int base_dist[30];
static uch flag_buf[4096];
static unsigned int last_lit;
static unsigned int last_dist;
static unsigned int last_flags;
static uch flags;
static uch flag_bit;
static ulg opt_len;
static ulg static_len;
static ulg compressed_len;
static ulg input_len;
ush *file_type;
int *file_method;
long block_start;
unsigned int strstart;
static void init_block(void);
static void pqdownheap(ct_data *tree, int k);
static void gen_bitlen(tree_desc *desc);
static void gen_codes(ct_data *tree, int max_code);
static void build_tree___0(tree_desc *desc);
static void scan_tree(ct_data *tree, int max_code);
static void send_tree(ct_data *tree, int max_code);
static int build_bl_tree(void);
static void send_all_trees(int lcodes, int dcodes, int blcodes);
static void compress_block(ct_data *ltree, ct_data *dtree);
static void set_file_type(void);
void ct_init(ush *attr, int *methodp) {
  int n;
  int bits;
  int length;
  int code;
  int dist;
  int tmp;
  int tmp___0;
  int tmp___1;
  int tmp___2;
  int tmp___3;
  int tmp___4;
  int tmp___5;

  {
    file_type = attr;
                          
                        
                               
                                           
             
     
               
    code = 0;
    {
      while (1) {
                                     ;
        if (!(code < 28)) {
          goto while_break;
        }
        base_length[code] = length;
        n = 0;
        {
          while (1) {
                                             ;
            if (!(n < 1 << extra_lbits[code])) {
              goto while_break___0;
            }
            tmp = length;
            length++;
            length_code[tmp] = (uch)code;
            n++;
          }
        while_break___0: /* CIL Label */;
        }
        code++;
      }
    while_break: /* CIL Label */;
    }
                                        
             
    code = 0;
    {
      while (1) {
                                         ;
        if (!(code < 16)) {
          goto while_break___1;
        }
        base_dist[code] = dist;
        n = 0;
        {
          while (1) {
                                             ;
            if (!(n < 1 << extra_dbits[code])) {
              goto while_break___2;
            }
            tmp___0 = dist;
            dist++;
            dist_code[tmp___0] = (uch)code;
            n++;
          }
        while_break___2: /* CIL Label */;
        }
        code++;
      }
    while_break___1: /* CIL Label */;
    }
    dist >>= 7;
    {
      while (1) {
                                         ;
        if (!(code < 30)) {
          goto while_break___3;
        }
        base_dist[code] = dist << 7;
        n = 0;
        {
          while (1) {
                                             ;
            if (!(n < 1 << (extra_dbits[code] - 7))) {
              goto while_break___4;
            }
            tmp___1 = dist;
            dist++;
            dist_code[256 + tmp___1] = (uch)code;
            n++;
          }
        while_break___4: /* CIL Label */;
        }
        code++;
      }
    while_break___3: /* CIL Label */;
    }
             
     
                 
                                          
                            
                               
         
                                
               
       
                                     
     
    n = 0;
    {
      while (1) {
                                         ;
        if (!(n <= 143)) {
          goto while_break___6;
        }
        tmp___2 = n;
        n++;
        static_ltree[tmp___2].dl.len = (ush)8;
                                                  
      }
    while_break___6: /* CIL Label */;
    }
     
                 
                                          
                          
                               
         
                    
            
                                              
                                                  
       
                                     
     
     
                 
                                          
                          
                               
         
                    
            
                                              
                                                  
       
                                     
     
     
                 
                                          
                          
                               
         
                    
            
                                              
                                                  
       
                                     
     
     
                                   
            
     
     
                 
                                           
                        
                                
         
         
                                          
                                                                        
              
         
       
                                      
     
    {
      init_block();
    }
           
  }
}
static void init_block(void) {
  int n;

  {
          
    {
      while (1) {
                                     ;
        if (!(n < 286)) {
          goto while_break;
        }
        dyn_ltree[n].fc.freq = (ush)0;
        n++;
      }
    while_break: /* CIL Label */;
    }
          
     
                 
                                          
                        
                               
         
                                      
            
       
                                     
     
          
     
                 
                                          
                        
                               
         
                                    
            
       
                                     
     
    dyn_ltree[256].fc.freq = (ush)1;
    static_len = (ulg)0L;
    opt_len = static_len;
    last_flags = 0U;
    last_dist = last_flags;
    last_lit = last_dist;
                   
    flag_bit = (uch)1;
           
  }
}
static void pqdownheap(ct_data *tree, int k) {
  int v;
  int j___0;

  {
    v = heap[k];
    j___0 = k << 1;
    {
      while (1) {
                                     ;
        if (!(j___0 <= heap_len)) {
          goto while_break;
        }
        if (j___0 < heap_len) {
          if ((int)(tree + heap[j___0 + 1])->fc.freq <
              (int)(tree + heap[j___0])->fc.freq) {
            j___0++;
          } else {
                                                         
                                                     
                                                                           
                        
               
             
          }
        }
                                                                            
                           
                
                                                                               
                                                           
                               
             
           
         
        heap[k] = heap[j___0];
        k = j___0;
        j___0 <<= 1;
      }
    while_break: /* CIL Label */;
    }
    heap[k] = v;
           
  }
}
static void gen_bitlen(tree_desc *desc) {
  ct_data *tree;
  int *extra;
  int base;
  int max_code;
  int max_length;
  ct_data *stree;
  int h;
  int n;
  int m;
  int bits;
  int xbits;
  ush f;
  int overflow;

  {
    tree = desc->dyn_tree;
    extra = desc->extra_bits;
    base = desc->extra_base;
    max_code = desc->max_code;
    max_length = desc->max_length;
    stree = desc->static_tree;
                 
    bits = 0;
    {
      while (1) {
                                     ;
        if (!(bits <= 15)) {
          goto while_break;
        }
        bl_count[bits] = (ush)0;
        bits++;
      }
    while_break: /* CIL Label */;
    }
                                             
    h = heap_max + 1;
    {
      while (1) {
                                         ;
        if (!(h < 573)) {
          goto while_break___0;
        }
        n = heap[h];
        bits = (int)(tree + (int)(tree + n)->dl.dad)->dl.len + 1;
                                
                            
                     
         
        (tree + n)->dl.len = (ush)bits;
        if (n > max_code) {
          goto __Cont;
        }
        bl_count[bits] = (ush)((int)bl_count[bits] + 1);
        xbits = 0;
                        
                                        
         
        f = (tree + n)->fc.freq;
                                                
        if (stree) {
          static_len += (ulg)f * (ulg)((int)(stree + n)->dl.len + xbits);
        }
      __Cont:
        h++;
      }
    while_break___0: /* CIL Label */;
    }
                        
             
     
     
                 
                                          
                              
         
                     
                                              
                                              
                                   
             
                   
           
                                         
         
                                                        
                                                                
                                                                    
                      
                              
                               
         
       
                                     
     
                      
     
                 
                                          
                           
                               
         
                                
         
                     
                                              
                            
                                   
             
                
                        
                               
                                      
             
                                                                         
                                                                        
                                                          
                                             
             
                
           
                                         
         
               
       
                                     
     
           
  }
}
static void gen_codes(ct_data *tree, int max_code) {
  ush next_code[16];
  ush code;
  int bits;
  int n;
  int len;
  ush tmp;
                    

  {
                  
    bits = 1;
    {
      while (1) {
                                     ;
        if (!(bits <= 15)) {
          goto while_break;
        }
        code = (ush)(((int)code + (int)bl_count[bits - 1]) << 1);
        next_code[bits] = code;
        bits++;
      }
    while_break: /* CIL Label */;
    }
    n = 0;
    {
      while (1) {
                                         ;
        if (!(n <= max_code)) {
          goto while_break___0;
        }
        len = (int)(tree + n)->dl.len;
                       
                      
         
        {
          tmp = next_code[len];
          next_code[len] = (ush)((int)next_code[len] + 1);
          (tree + n)->fc.code = (ush)bi_reverse((unsigned int)tmp, len);
        }
      __Cont:
        n++;
      }
    while_break___0: /* CIL Label */;
    }
           
  }
}
static void build_tree___0(tree_desc *desc) {
  ct_data *tree;
  ct_data *stree;
  int elems;
  int n;
  int m;
  int max_code;
  int node;
  int new;
  int tmp;
  int tmp___0;
  int tmp___1;
  ush tmp___2;
  int tmp___3;

  {
    tree = desc->dyn_tree;
                              
    elems = desc->elems;
                  
    node = elems;
    heap_len = 0;
    heap_max = 573;
    n = 0;
    {
      while (1) {
                                     ;
        if (!(n < elems)) {
          goto while_break;
        }
        if ((int)(tree + n)->fc.freq != 0) {
          heap_len++;
          max_code = n;
          heap[heap_len] = max_code;
                            
        } else {
          (tree + n)->dl.len = (ush)0;
        }
        n++;
      }
    while_break: /* CIL Label */;
    }
     
                 
                                          
                              
                               
         
                   
                           
                     
                         
                
                  
         
                             
                  
                                       
                            
                  
                    
                                                   
         
       
                                     
     
    desc->max_code = max_code;
                     
     
                 
                                          
                        
                               
         
         
                              
              
         
       
                                     
     
    {
      while (1) {
                                         ;
        {
          n = heap[1];
          tmp___0 = heap_len;
          heap_len--;
          heap[1] = heap[tmp___0];
          pqdownheap(tree, 1);
          m = heap[1];
          heap_max--;
          heap[heap_max] = n;
          heap_max--;
          heap[heap_max] = m;
          (tree + node)->fc.freq =
              (ush)((int)(tree + n)->fc.freq + (int)(tree + m)->fc.freq);
        }
                                            {
                                  
        }       
                                  
         
        {
                                           
          tmp___2 = (ush)node;
          (tree + m)->dl.dad = tmp___2;
          (tree + n)->dl.dad = tmp___2;
          tmp___3 = node;
          node++;
          heap[1] = tmp___3;
          pqdownheap(tree, 1);
        }
        if (!(heap_len >= 2)) {
          goto while_break___2;
        }
      }
    while_break___2: /* CIL Label */;
    }
    {
      heap_max--;
                               
      gen_bitlen(desc);
      gen_codes(tree, max_code);
    }
           
  }
}
static void scan_tree(ct_data *tree, int max_code) {
  int n;
  int prevlen;
  int curlen;
  int nextlen;
  int count;
  int max_count;
  int min_count;

  {
                 
                                      
              
                  
                  
                       
                      
                    
     
                                                 
    n = 0;
    {
      while (1) {
                                     ;
        if (!(n <= max_code)) {
          goto while_break;
        }
        curlen = nextlen;
        nextlen = (int)(tree + (n + 1))->dl.len;
        count++;
        if (count < max_count) {
          if (curlen == nextlen) {
            goto __Cont;
          } else {
            goto _L;
          }
        } else {
        _L:
          if (count < min_count) {
            bl_tree[curlen].fc.freq =
                (ush)((int)bl_tree[curlen].fc.freq + count);
          } else {
            if (curlen != 0) {
                                      
                                         
                                                            
               
              bl_tree[16].fc.freq = (ush)((int)bl_tree[16].fc.freq + 1);
            } else {
              if (count <= 10) {
                bl_tree[17].fc.freq = (ush)((int)bl_tree[17].fc.freq + 1);
              } else {
                bl_tree[18].fc.freq = (ush)((int)bl_tree[18].fc.freq + 1);
              }
            }
          }
        }
        count = 0;
        prevlen = curlen;
                          {
          max_count = 138;
          min_count = 3;
        }       
                                  
                          
                          
                  
                          
                          
           
         
      __Cont:
        n++;
      }
    while_break: /* CIL Label */;
    }
           
  }
}
static void send_tree(ct_data *tree, int max_code) {
  int n;
  int prevlen;
  int curlen;
  int nextlen;
  int count;
  int max_count;
  int min_count;

  {
                 
    nextlen = (int)(tree + 0)->dl.len;
    count = 0;
                  
                  
                       
                      
                    
     
    n = 0;
    {
      while (1) {
                                     ;
        if (!(n <= max_code)) {
          goto while_break;
        }
        curlen = nextlen;
        nextlen = (int)(tree + (n + 1))->dl.len;
        count++;
                                
                                  
                        
                  
                    
           
               {
        _L:
                                 {
             

                        {
                                                 ;
                {
                  send_bits((int)bl_tree[curlen].fc.code,
                            (int)bl_tree[curlen].dl.len);
                  count--;
                }
                                    
                                       
                 
              }
            while_break___0: /* CIL Label */;
             

          }       
                              
                                      
                 

                                                         
                                                         
                          
                 

               
               
                                                                             
                                        
               
                    
                                
                 

                                                                               
                                          
                 

                      
                 

                                                                               
                                           
                 

               
             
           
        }
        count = 0;
        prevlen = curlen;
                           
                          
                        
               {
                                 {
                          
                          
          }       
                          
                          
           
        }
      __Cont:
        n++;
      }
    while_break: /* CIL Label */;
    }
           
  }
}
static int build_bl_tree(void) {
  int max_blindex;

  {
    {
      scan_tree(dyn_ltree, l_desc.max_code);
      scan_tree(dyn_dtree, d_desc.max_code);
      build_tree___0(&bl_desc);
      max_blindex = 18;
    }
     
                 
                                      
                                  
                           
         
                                                              
                           
         
                      
       
                                 
     
                                                            
    return (max_blindex);
  }
}
static void send_all_trees(int lcodes, int dcodes, int blcodes) {
  int rank;

  {
    {
      send_bits(lcodes - 257, 5);
      send_bits(dcodes - 1, 5);
      send_bits(blcodes - 4, 4);
      rank = 0;
    }
    {
      while (1) {
                                     ;
        if (!(rank < blcodes)) {
          goto while_break;
        }
        {
          send_bits((int)bl_tree[bl_order[rank]].dl.len, 3);
          rank++;
        }
      }
    while_break: /* CIL Label */;
    }
    {
      send_tree(dyn_ltree, lcodes - 1);
      send_tree(dyn_dtree, dcodes - 1);
    }
           
  }
}
ulg flush_block(char *buf, ulg stored_len, int eof) {
  ulg opt_lenb;
  ulg static_lenb;
  int max_blindex;

  {
                                 
                                   
       

                        
       

     
    {
      build_tree___0(&l_desc);
      build_tree___0(&d_desc);
      max_blindex = build_bl_tree();
      opt_lenb = ((opt_len + 3UL) + 7UL) >> 3;
      static_lenb = ((static_len + 3UL) + 7UL) >> 3;
                              
    }
    if (static_lenb <= opt_lenb) {
      opt_lenb = static_lenb;
    }
                                 
                
                                    
                      
                
                      
         
              
                    
       
           {
    _L___2:
                                         
                                                               
           

                              
                            
                                                                      
                                                      
                                                         
           

                
                  
         
             {
      _L:
                                      
           

                                         
                                                       
                                               
           

               {
           

            send_bits((2 << 1) + eof, 3);
            send_all_trees(l_desc.max_code + 1, d_desc.max_code + 1,
                           max_blindex + 1);
            compress_block(dyn_ltree, dyn_dtree);
                                            
           

        }
      }
    }
     
                   
     
    if (eof) {
       

        bi_windup();
                              
       

    }
    return (compressed_len >> 3);
  }
}
int ct_tally(int dist, int lc) {
  unsigned int tmp;
  int tmp___0;
  unsigned int tmp___1;
  unsigned int tmp___2;
  ulg out_length;
  ulg in_length;
  int dcode;
  int tmp___3;

  {
    tmp = last_lit;
    last_lit++;
    inbuf[tmp] = (uch)lc;
    if (dist == 0) {
      dyn_ltree[lc].fc.freq = (ush)((int)dyn_ltree[lc].fc.freq + 1);
    } else {
      dist--;
      dyn_ltree[((int)length_code[lc] + 256) + 1].fc.freq =
          (ush)((int)dyn_ltree[((int)length_code[lc] + 256) + 1].fc.freq + 1);
      if (dist < 256) {
        tmp___0 = (int)dist_code[dist];
      } else {
        tmp___0 = (int)dist_code[256 + (dist >> 7)];
      }
      dyn_dtree[tmp___0].fc.freq = (ush)((int)dyn_dtree[tmp___0].fc.freq + 1);
      tmp___1 = last_dist;
      last_dist++;
      d_buf[tmp___1] = (ush)dist;
      flags = (uch)((int)flags | (int)flag_bit);
    }
    flag_bit = (uch)((int)flag_bit << 1);
    if ((last_lit & 7U) == 0U) {
      tmp___2 = last_flags;
      last_flags++;
      flag_buf[tmp___2] = flags;
      flags = (uch)0;
      flag_bit = (uch)1;
    }
                    
                                     
                                         
                                                     
                  
         
                     
                                          
                                
                               
             
                                                         
                                                               
                    
           
                                     
         
                         
                                        
                                             
                       
           
         
       
     
                             
                  
           {
                                
                    
             {
        tmp___3 = 0;
      }
    }
    return (tmp___3);
  }
}
static void compress_block(ct_data *ltree, ct_data *dtree) {
  unsigned int dist;
  int lc;
  unsigned int lx;
  unsigned int dx;
  unsigned int fx;
  uch flag;
  unsigned int code;
  int extra;
  unsigned int tmp;
  unsigned int tmp___0;
  unsigned int tmp___1;

  {
    lx = 0U;
    dx = 0U;
            
                  
                        {
       

        while (1) {
                                       ;
          if ((lx & 7U) == 0U) {
            tmp = fx;
            fx++;
            flag = flag_buf[tmp];
          }
          tmp___0 = lx;
          lx++;
          lc = (int)inbuf[tmp___0];
          if (((int)flag & 1) == 0) {
             

              send_bits((int)(ltree + lc)->fc.code, (int)(ltree + lc)->dl.len);
             

          } else {
            {
              code = (unsigned int)length_code[lc];
              send_bits((int)(ltree + ((code + 256U) + 1U))->fc.code,
                        (int)(ltree + ((code + 256U) + 1U))->dl.len);
              extra = extra_lbits[code];
            }
            if (extra != 0) {
               

                lc -= base_length[code];
                send_bits(lc, extra);
               

            }
            tmp___1 = dx;
            dx++;
            dist = (unsigned int)d_buf[tmp___1];
            if (dist < 256U) {
              code = (unsigned int)dist_code[dist];
            } else {
              code = (unsigned int)dist_code[256U + (dist >> 7)];
            }
            {
              send_bits((int)(dtree + code)->fc.code,
                        (int)(dtree + code)->dl.len);
              extra = extra_dbits[code];
            }
                            {
               

                dist -= (unsigned int)base_dist[code];
                send_bits((int)dist, extra);
               

            }
          }
          flag = (uch)((int)flag >> 1);
          if (!(lx < last_lit)) {
            goto while_break;
          }
        }
      while_break: /* CIL Label */;
       

    }
    {
      send_bits((int)(ltree + 256)->fc.code, (int)(ltree + 256)->dl.len);
    }
           
  }
}
static void set_file_type(void) {
  int n;
  unsigned int ascii_freq;
  unsigned int bin_freq;
  int tmp;
  int tmp___0;
  int tmp___1;

   
          
                    
                  
     
                 
                                      
                       
                           
         
                
            
                                                         
       
                                 
     
     
                 
                                          
                         
                               
         
                    
            
                                                               
       
                                     
     
     
                 
                                          
                         
                               
         
                    
            
                                                             
       
                                     
     
                                     
                          
            
                          
     
           
   
}
void lm_init(int pack_level, ush *flags___0);
ulg deflate(void);
ulg window_size = 65536UL;
static unsigned int ins_h;
unsigned int prev_length;
unsigned int match_start;
static int eofile;
static unsigned int lookahead;
unsigned int max_chain_length;
static unsigned int max_lazy_match;
static int compr_level;
unsigned int good_match;
int nice_match;
static config configuration_table[10] = {
    {(ush)0, (ush)0, (ush)0, (ush)0},
    {(ush)4, (ush)4, (ush)8, (ush)4},
    {(ush)4, (ush)5, (ush)16, (ush)8},
    {(ush)4, (ush)6, (ush)32, (ush)32},
    {(ush)4, (ush)4, (ush)16, (ush)16},
    {(ush)8, (ush)16, (ush)32, (ush)32},
    {(ush)8, (ush)16, (ush)128, (ush)128},
    {(ush)8, (ush)32, (ush)128, (ush)256},
    {(ush)32, (ush)128, (ush)258, (ush)1024},
    {(ush)32, (ush)258, (ush)258, (ush)4096}};
static void fill_window(void);
static ulg deflate_fast(void);
int longest_match(IPos cur_match);
void lm_init(int pack_level, ush *flags___0) {
  register unsigned int j___0;
  unsigned int tmp;
                   
                   

  {
                         
       

                                        
       

            
                           
         

                                          
         

       
     
     
                               
                                                
                                                       
                                          
                                                                              
                                                                             
                                                                    
                        
                                                                  
     
                          
                                              
            
                            
                                                
       
     
    strstart = 0U;
                     
                            {
      tmp = 32768U;
    }       
                   
     
    {
      lookahead = (unsigned int)(*read_buf)((char *)(window), tmp);
    }
                          
                 
                     
             
            
                                     
                   
                       
               
       
     
               
     
                 
                                      
                               
                           
                             
           
                
                           
         
         
	                
	 
       
                                 
     
               
               
     
                 
                                          
                            
                               
         
                                                              
                                               
                
       
                                     
     
           
  }
}
int longest_match(IPos cur_match) {
  unsigned int chain_length;
  register uch *scan;
  register uch *match;
  register int len;
  int best_len;
  IPos limit;
  unsigned int tmp;
  register uch *strend;
  register uch scan_end1;
  register uch scan_end;

  {
                                    
    scan = window + strstart;
    best_len = (int)prev_length;
                           {
      tmp = strstart - 32506U;
    }       
               
     
    limit = tmp;
    strend = (window + strstart) + 258;
    scan_end1 = *(scan + (best_len - 1));
    scan_end = *(scan + best_len);
                                    
                         
     
    {
                {
                                     ;
        match = window + cur_match;
                                                        
                      
               {
                                                                 
                        
                 {
            if ((int)*match != (int)*scan) {
              goto __Cont;
            } else {
              match++;
                                                    
                            
               
            }
          }
        }
        scan += 2;
        match++;
        {
                    {
                                             ;
                   
                    
                                           {
                     
                      
              if ((int)*scan == (int)*match) {
                scan++;
                        
                                                
                         
                          
                                                  
                           
                            
                                                    
                             
                              
                                                      
                               
                                
                                                        
                                 
                                  
                                                          
                                                       
                                                           
                                                   
                             
                                  
                                                 
                           
                                
                                               
                         
                              
                                             
                       
                            
                                           
                     
                          
                                         
                   
                        
                                       
                 
              } else {
                                     
              }
            }       
                                   
             
          }
        while_break___0: /* CIL Label */;
        }
        len = 258 - (int)(strend - scan);
                            
                            {
          match_start = cur_match;
          best_len = len;
                                  
                             
           
                                               
                                        
        }
      __Cont:
        cur_match = (IPos)prev[cur_match & 32767U];
                                
                         
                                      
                             
           
                
                           
         
      }
    while_break: /* CIL Label */;
    }
    return (best_len);
  }
}
static void fill_window(void) {
  register unsigned int n;
  register unsigned int m;
  unsigned int more;
  unsigned int tmp;
  unsigned int tmp___0;

  {
                                                                          
                             {
      more--;
    }       
                               
         
                                                              
                                                                            
                                 
                                
                             
                                
                 
         
         
                     
                                          
                                                 
                               
             
                                                    
                              
                               
                    
                       
             
                                             
                
           
                                     
         
               
         
                     
                                              
                                
                                   
             
                                      
                              
                                   
                    
                           
             
                                   
                
           
                                         
         
                       
       
     
                 {
      {
        n = (unsigned int)(*read_buf)(((char *)(window) + strstart) + lookahead,
                                      more);
      }
                   {
        eofile = 1;
      }       
                               
                     
                
                         
         
       
    }
           
  }
}
static ulg deflate_fast(void) {
  IPos hash_head;
  int flush;
  unsigned int match_length;
  char *tmp;
  char *tmp___0;
  ulg tmp___1;

  {
                      
                     
    {
      while (1) {
                                     ;
        if (!(lookahead != 0U)) {
          goto while_break;
        }
        ins_h = ((ins_h << 5) ^ (unsigned int)window[(strstart + 3U) - 1U]) &
                ((unsigned int)(1 << 15) - 1U);
        hash_head = (IPos) * ((prev + 32768) + ins_h);
                                                 
        *((prev + 32768) + ins_h) = (ush)strstart;
        if (hash_head != 0U) {
          if (strstart - hash_head <= 32506U) {
            {
              match_length = (unsigned int)longest_match(hash_head);
            }
                                           
                                       
             
          }
        }
        if (match_length >= 3U) {
          {
            flush = ct_tally((int)(strstart - match_start),
                             (int)(match_length - 3U));
            lookahead -= match_length;
          }
                                              {
            match_length--;
            {
              while (1) {
                                                 ;
                strstart++;
                ins_h = ((ins_h << 5) ^
                         (unsigned int)window[(strstart + 3U) - 1U]) &
                        ((unsigned int)(1 << 15) - 1U);
                                                              
                                                         
                                                          
                match_length--;
                if (!(match_length != 0U)) {
                  goto while_break___0;
                }
              }
            while_break___0: /* CIL Label */;
            }
            strstart++;
          }       
                                     
                              
                                                   
                                                                          
                                                   
           
        } else {
           

            flush = ct_tally(0, (int)window[strstart]);
            lookahead--;
            strstart++;
           

        }
                    
                                  
                                                               
                  
                                      
           
           
                                                                     
                                         
           
         
         
                     
                                              
                                   
                               
                                     
               
                    
                                   
             
             
	                    
	     
           
                                         
         
      }
    while_break: /* CIL Label */;
    }
                           {
      tmp___0 = (char *)(&window[(unsigned int)block_start]);
    }       
                                    
     
    {
      tmp___1 = flush_block(tmp___0, (ulg)((long)strstart - block_start), 1);
    }
    return (tmp___1);
  }
}
ulg deflate(void) {
  IPos hash_head;
  IPos prev_match;
  int flush;
  int match_available;
  register unsigned int match_length;
  ulg tmp;
  char *tmp___0;
  char *tmp___1;
  int tmp___2;
  char *tmp___3;
  ulg tmp___4;

  {
                        
                      
    if (compr_level <= 3) {
      {
        tmp = deflate_fast();
      }
      return (tmp);
    }
    {
                {
                                     ;
                                 
                           
         
                                                                             
                                               
                                                      
                                                 
                                                  
                                   
        prev_match = match_start;
                          
                              
                                             
                                                 
               
                                                                      
               
                                             
                                         
               
                                       
                                                     
                                 
                 
               
             
           
         
                                
                                            
             
                                                                   
                                                        
                                            
                                
             
             
                         
                                                  
                           
                                       
                                                                      
                                                       
                                                              
                                                         
                                                          
                              
                                           
                                       
                 
               
                                             
             
                                
                              
                       
                        
                                      
                                                                       
                      
                                              
               
               
                                                                             
                                             
               
             
                  
                    
           
               {
        _L:
                               {
             
                                                                
             
                         {
                                     {
                tmp___1 = (char *)(&window[(unsigned int)block_start]);
              }       
                                              
               
              {
                flush_block(tmp___1, (ulg)((long)strstart - block_start), 0);
                                             
              }
            }
                       
                        
          }       
                                
                       
                        
           
        }
         
                     
                                              
                                   
                               
                                     
               
                    
                                   
             
             
	                    
	     
           
                                         
         
      }
    while_break: /* CIL Label */;
    }
                          
       

                                                
       

     
                            
                                                             
            
                                    
     
     
                                                                             
     
                     
  }
}
long header_bytes;
long time_stamp;
int save_orig_name;
int zip(int in, int out);
static ulg crc___0;
int zip(int in, int out) {
  uch flags___0;
  ush attr;
  ush deflate_flags;
  unsigned int tmp;
  unsigned int tmp___0;
  unsigned int tmp___1;
  unsigned int tmp___2;
  unsigned int tmp___3;
  unsigned int tmp___4;
  unsigned int tmp___5;
  unsigned int tmp___6;
  unsigned int tmp___7;
  unsigned int tmp___8;
  unsigned int tmp___9;
  unsigned int tmp___10;
  unsigned int tmp___11;
  unsigned int tmp___12;
  char *p;
  char *tmp___13;
  unsigned int tmp___14;
  char *tmp___15;
  unsigned int tmp___16;
  unsigned int tmp___17;
  unsigned int tmp___18;
  unsigned int tmp___19;
  unsigned int tmp___20;
  unsigned int tmp___21;
  unsigned int tmp___22;
  unsigned int tmp___23;
  unsigned int tmp___24;
  unsigned int tmp___25;
  unsigned int tmp___26;
  unsigned int tmp___27;
  unsigned int tmp___28;
  unsigned int tmp___29;
  unsigned int tmp___30;
  unsigned int tmp___31;
                    
                    

  {
                       
                  
                           
             
    ofd = out;
    outcnt = 0U;
               
                 
    outcnt++;
    outbuf[tmp] = (uch) * ("\037\213" + 0);
                           
       

                       
       

     
    tmp___0 = outcnt;
    outcnt++;
    outbuf[tmp___0] = (uch) * ("\037\213" + 1);
                           
       

                       
       

     
    tmp___1 = outcnt;
    outcnt++;
    outbuf[tmp___1] = (uch)8;
                           
       

                       
       

     
    if (save_orig_name) {
      flags___0 = (uch)((int)flags___0 | 8);
    }
    tmp___2 = outcnt;
    outcnt++;
    outbuf[tmp___2] = flags___0;
                           
       

                       
       

     
                         {
      tmp___3 = outcnt;
      outcnt++;
                                                            
      tmp___4 = outcnt;
      outcnt++;
                                                                      
    }       
                       
               
                                                            
                             
         

                         
         

       
                       
               
                                                                      
                             
         

                         
         

       
     
                         {
      tmp___7 = outcnt;
      outcnt++;
                                                               
      tmp___8 = outcnt;
      outcnt++;
                                                                        
    }       
                       
               
                                                               
                             
         

                         
         

       
                        
               
                                                                         
                             
         

                         
         

       
     
    {
                                     
      bi_init(out);
      ct_init(&attr, &method);
      lm_init(level, &deflate_flags);
      tmp___11 = outcnt;
      outcnt++;
                                            
    }
                           
       

                       
       

     
                      
    outcnt++;
                              
                           
       

                       
       

     
    if (save_orig_name) {
      {
        tmp___13 = basename(ifname);
        p = tmp___13;
      }
      {
        while (1) {
                                       ;
          tmp___14 = outcnt;
          outcnt++;
          outbuf[tmp___14] = (uch)*p;
                                 
             

                             
             

           
          tmp___15 = p;
          p++;
          if (!*tmp___15) {
            goto while_break;
          }
        }
      while_break: /* CIL Label */;
      }
    }
    {
                                  
      deflate();
    }
                          
                        
               
                                                            
                        
               
                                                                     
            
                        
               
                                                            
                             
         

                         
         

       
                        
               
                                                                     
                             
         

                         
         

       
     
                          
                        
               
                                                        
                        
               
                                                                 
            
                        
               
                                                        
                             
         

                         
         

       
                        
               
                                                                 
                             
         

                         
         

       
     
                          
                        
               
                                                           
                        
               
                                                                     
            
                        
               
                                                           
                             
         

                         
         

       
                        
               
                                                                     
                             
         

                         
         

       
     
                          
                        
               
                                                              
                        
               
                                                                       
            
                        
               
                                                              
                             
         

                         
         

       
                        
               
                                                                       
                             
         

                         
         

       
     
    {
                                                                              
      flush_outbuf();
    }
    return (0);
  }
}
int file_read(char *buf, unsigned int size) {
  unsigned int len;

  {
    {
      len = (unsigned int)read(ifd, (void *)buf, (size_t)size);
    }
                             
                        
            
                      
                          
       
     
     
                                        
                                                  
     
    return ((int)len);
  }
}
static char *license_msg[15] = {
    (char *)"   Copyright (C) 1992-1993 Jean-loup Gailly",
    (char *)"   This program is free software; you can redistribute it and/or "
            "modify",
    (char *)"   it under the terms of the GNU General Public License as "
            "published by",
    (char *)"   the Free Software Foundation; either version 2, or (at your "
            "option)",
    (char *)"   any later version.",
    (char *)"",
    (char
         *)"   This program is distributed in the hope that it will be useful,",
    (char *)"   but WITHOUT ANY WARRANTY; without even the implied warranty of",
    (char *)"   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the",
    (char *)"   GNU General Public License for more details.",
    (char *)"",
    (char *)"   You should have received a copy of the GNU General Public "
            "License",
    (char *)"   along with this program; if not, write to the Free Software",
    (char *)"   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.",
    (char *)0};
extern __attribute__((__nothrow__))
__sighandler_t(__attribute__((__leaf__)) signal)(int __sig,
                                                 void (*__handler)(int));
extern __attribute__((__nothrow__)) int(
    __attribute__((__nonnull__(1, 2), __leaf__))
    stat)(char const *__restrict __file, struct stat *__restrict __buf);
extern
    __attribute__((__nothrow__)) int(__attribute__((__nonnull__(2), __leaf__))
                                     fstat)(int __fd, struct stat *__buf);
extern __attribute__((__nothrow__)) int(
    __attribute__((__nonnull__(1, 2), __leaf__))
    lstat)(char const *__restrict __file, struct stat *__restrict __buf);
extern
    __attribute__((__nothrow__)) int(__attribute__((__nonnull__(1), __leaf__))
                                     chmod)(char const *__file,
                                            __mode_t __mode);
extern struct _IO_FILE *stdin;
extern struct _IO_FILE *stdout;
extern int fflush(FILE *__stream);
extern int printf(char const *__restrict __format, ...);
extern char *fgets(char *__restrict __s, int __n, FILE *__restrict __stream);
extern __attribute__((__nothrow__)) int(__attribute__((__leaf__))
                                        fileno)(FILE *__stream);
extern __attribute__((__nothrow__)) int(
    __attribute__((__nonnull__(1, 2), __leaf__))
    memcmp)(void const *__s1, void const *__s2, size_t __n)
    __attribute__((__pure__));
extern __attribute__((__nothrow__)) char *(
    __attribute__((__nonnull__(1, 2), __leaf__))
    strncpy)(char *__restrict __dest, char const *__restrict __src, size_t __n);
extern __attribute__((__nothrow__)) char *(
    __attribute__((__nonnull__(1, 2), __leaf__))
    strcat)(char *__restrict __dest, char const *__restrict __src);
extern __attribute__((__nothrow__)) int(
    __attribute__((__nonnull__(1, 2), __leaf__))
    strcmp)(char const *__s1, char const *__s2) __attribute__((__pure__));
extern __attribute__((__nothrow__)) int(
    __attribute__((__nonnull__(1, 2), __leaf__))
    strncmp)(char const *__s1, char const *__s2, size_t __n)
    __attribute__((__pure__));
long ifile_size;
            
extern char *optarg;
extern int optind;
extern int getopt_long(int argc, char *const *argv, char const *shortopts,
                       struct option const *longopts, int *longind);
extern __attribute__((__nothrow__)) char *(__attribute__((__leaf__))
                                           ctime)(time_t const *__timer);
extern int(__attribute__((__nonnull__(1))) open)(char const *__file,
                                                 int __oflag, ...);
extern __attribute__((__nothrow__))
__off_t(__attribute__((__leaf__)) lseek)(int __fd, __off_t __offset,
                                         int __whence);
extern int close(int __fd);
extern
    __attribute__((__nothrow__)) int(__attribute__((__nonnull__(1), __leaf__))
                                     chown)(char const *__file, __uid_t __owner,
                                            __gid_t __group);
extern __attribute__((__nothrow__)) int(__attribute__((__leaf__))
                                        isatty)(int __fd);
extern
    __attribute__((__nothrow__)) int(__attribute__((__nonnull__(1), __leaf__))
                                     unlink)(char const *__name);
extern
    __attribute__((__nothrow__)) int(__attribute__((__nonnull__(1), __leaf__))
                                     atoi)(char const *__nptr)
        __attribute__((__pure__));
extern __attribute__((__nothrow__, __noreturn__)) void(__attribute__((__leaf__))
                                                       exit)(int __status);
extern DIR *(__attribute__((__nonnull__(1))) opendir)(char const *__name);
extern int(__attribute__((__nonnull__(1))) closedir)(DIR *__dirp);
extern struct dirent *(__attribute__((__nonnull__(1))) readdir)(DIR *__dirp);
extern
    __attribute__((__nothrow__)) int(__attribute__((__nonnull__(1), __leaf__))
                                     utime)(char const *__file,
                                            struct utimbuf const *__file_times);
int ascii = 0;
int to_stdout = 0;
int decompress = 0;
int force = 0;
int no_name = -1;
int no_time = -1;
int recursive = 0;
int list = 0;
int verbose = 0;
int quiet = 0;
int do_lzw = 0;
int test = 0;
int foreground;
int maxbits = 16;
int method = 8;
int level = 6;
int exit_code = 0;
int last_member;
int part_nb;
char *env;
char **args = (char **)((void *)0);
char z_suffix[31];
int z_len;
long total_in = 0L;
long total_out = 0L;
int remove_ofname = 0;
struct stat istat;
struct option longopts[24] = {
    {"ascii", 0, (int *)0, 'a'},      {"to-stdout", 0, (int *)0, 'c'},
    {"stdout", 0, (int *)0, 'c'},     {"decompress", 0, (int *)0, 'd'},
    {"uncompress", 0, (int *)0, 'd'}, {"force", 0, (int *)0, 'f'},
    {"help", 0, (int *)0, 'h'},       {"list", 0, (int *)0, 'l'},
    {"license", 0, (int *)0, 'L'},    {"no-name", 0, (int *)0, 'n'},
    {"name", 0, (int *)0, 'N'},       {"quiet", 0, (int *)0, 'q'},
    {"silent", 0, (int *)0, 'q'},     {"recursive", 0, (int *)0, 'r'},
    {"suffix", 1, (int *)0, 'S'},     {"test", 0, (int *)0, 't'},
    {"no-time", 0, (int *)0, 'T'},    {"verbose", 0, (int *)0, 'v'},
    {"version", 0, (int *)0, 'V'},    {"fast", 0, (int *)0, '1'},
    {"best", 0, (int *)0, '9'},       {"lzw", 0, (int *)0, 'Z'},
    {"bits", 1, (int *)0, 'b'},       {(char const *)0, 0, (int *)0, 0}};
static void usage(void);
static void help(void);
static void license(void);
static void version(void);
static void treat_stdin(void);
static void treat_file(char *iname);
static int create_outfile(void);
static int do_stat(char *name, struct stat *sbuf);
static char *get_suffix(char *name);
static int get_istat(char *iname, struct stat *sbuf);
static int make_ofname(void);
static int same_file(struct stat *stat1, struct stat *stat2);
static int name_too_long(char *name, struct stat *statb);
static void shorten_name(char *name);
static int get_method(int in);
static void do_list(int ifd___0, int method___0);
static int check_ofname(void);
static void copy_stat(struct stat *ifstat);
static void do_exit(int exitcode);
int main(int argc, char **argv);
int (*work)(int infile, int outfile) = &zip;
static void treat_dir(char *dir);
static void reset_times(char *name, struct stat *statb);
static void usage(void) {

   

     
                                                
                                                                                
                                                                          
                                 
     
           
   

}
static char *help_msg[18] = {
    (char *)" -c --stdout      write on standard output, keep original files "
            "unchanged",
    (char *)" -d --decompress  decompress",
    (char *)" -f --force       force overwrite of output file and compress "
            "links",
    (char *)" -h --help        give this help",
    (char *)" -l --list        list compressed file contents",
    (char *)" -L --license     display software license",
    (char *)" -n --no-name     do not save or restore the original name and "
            "time stamp",
    (char
         *)" -N --name        save or restore the original name and time stamp",
    (char *)" -q --quiet       suppress all warnings",
    (char *)" -r --recursive   operate recursively on directories",
    (char *)" -S .suf  --suffix .suf     use suffix .suf on compressed files",
    (char *)" -t --test        test compressed file integrity",
    (char *)" -v --verbose     verbose mode",
    (char *)" -V --version     display version number",
    (char *)" -1 --fast        compress faster",
    (char *)" -9 --best        compress better",
    (char *)" file...          files to (de)compress. If none given, use "
            "standard input.",
    (char *)0};
static void help(void) {
  char **p;
  char **tmp;

  {
    {
      p = help_msg;
                                                
                                                                        
                                    
              
    }
     
                 
                                      
                  
                           
         
         
                  
              
                                                    
                                                                 
         
       
                                 
     
           
  }
}
static void license(void) {
  char **p;
  char **tmp;

  {
    {
      p = license_msg;
                                                
                                                                        
                                    
    }
     
                 
                                      
                  
                           
         
         
                  
              
                                                    
                                                                 
         
       
                                 
     
           
  }
}
static void version(void) {

   

     
                                                
                                                                        
                                    
                                                
                                                                              
                                 
                                                
                                                                
                                                
                                                                 
                                                
                                                     
     
           
   

}
int main(int argc, char **argv) {
  int file_count;
  int proglen;
  int optc;
  int tmp;
  void (*tmp___0)(int);
  void (*tmp___1)(int);
  void (*tmp___2)(int);
  int tmp___3;
  int tmp___4;
  int tmp___5;
  int tmp___6;
  int tmp___7;
  int tmp___8;
                    
                    

  {
     
                                       
                                                    
     
                      
       
                                                                       
       
                     
                                                   
       
     
     
                                                     
     
                                                           
                  
     
     
                                            
                                                                               
     
                     
       

                                                
       

     
     
                                             
     
                                                                      
       

                                                 
       

     
     
                                            
     
                                                                      
       

                                                
       

     
     
                                                                 
     
                       
                     
            
       
	                                                            
       
                         
                       
              
         
	                                                        
	 
                           
                        
                                 
                
           
	                                                      
	   
                             
                          
                                   
           
         
       
     
    {
      strncpy((char * /* __restrict  */)(z_suffix),
              (char const * /* __restrict  */) ".gz", sizeof(z_suffix) - 1UL);
                                                    
    }
    {
                {
                                     ;
        {
          optc = getopt_long(argc, (char *const *)argv,
                             "ab:cdfhH?lLmMnNqrS:tvVZ123456789",
                             (struct option const *)(longopts), (int *)0);
        }
        if (!(optc != -1)) {
          goto while_break;
        }
        {
                           
                         
           
                           
                         
           
                           
                         
           
                            
                          
           
                            
                          
           
                            
                          
           
                           
                          
           
                           
                          
           
                            
                          
           
                           
                         
           
                            
                          
           
                           
                         
           
                            
                          
           
                           
                         
           
                            
                          
           
                            
                          
           
                           
                         
           
                            
                          
           
                            
                          
           
                           
                         
           
                           
                         
           
                           
                         
           
                           
                         
           
                           
                         
           
                           
                         
           
                           
                         
           
                           
                         
           
                           
                         
           
                           
                         
           
                           
                         
           
                              
                                
                    
                            
                                 
         
                                               
         
                            
                                
                        
                            
                                 
                         
                            
                                 
                  
                            
                                 
                                 
                                  
          
                 
                     
         
                            
        case_108: /* CIL Label */
          to_stdout = 1;
          decompress = to_stdout;
          list = decompress;
                            
                                 
         
                    
                     
         
                            
                                 
                      
                            
                                
                      
                            
                                 
                      
                            
                            
                                
                      
                            
                            
                                 
                    
                      
                            
                                 
                        
                            
                                 
         
                                                    
                                                      
                                                         
         
                            
                                 
                        
                                 
                            
                            
                                 
                    
                    
                            
                                 
         
                    
                     
         
                            
                                 
         
                                                    
                                                                             
                                                                    
                            
                  
                     
         
                            
                                
                                
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                             
                            
                                        
         
                  
                     
         
                                      
        }
      }
    while_break: /* CIL Label */;
    }
                      
                           
     
                      
                           
     
    file_count = argc - optind;
                
                   
         

                                                    
                                                                                
                                                                      
                            
         

       
     
                     
                        
         

                  
                                                
                                                                               
                                
                     
         

              
                
       
            
       
                       
         

                  
                                                
                                                                               
                                
                     
         

       
     
                 
                        
                    
       
     
    if (file_count != 0) {
                      
                    
                      
                              
                          
                    
                           
                            
                      
                            
               
             
                  
                        
           
                
                      
         
              
                    
       
      {
        while (1) {
                                           ;
          if (!(optind < argc)) {
            goto while_break___0;
          }
          {
            tmp___8 = optind;
            optind++;
            treat_file(*(argv + tmp___8));
          }
        }
      while_break___0: /* CIL Label */;
      }
    } else {
       

	treat_stdin();
       

    }
               
                   
                             
           

                            
           

         
       
     
     
                         
     
    return (exit_code);
  }
}
static void treat_stdin(void) {
  char const *tmp;
  char const *tmp___0;
  struct _IO_FILE *tmp___1;
  int tmp___2;
  int tmp___3;
  int tmp___4;
  int tmp___5;
  int tmp___6;
  int tmp___7;
  int tmp___8;
  int tmp___9;
  int tmp___10;
                    
                    
                    
                    
                    
                    
                    

  {
                 
                  
                         
                          
                
                           
         
         
                                    
                                    
         
                      
                           
                       
                  
                     
           
                           
                                  
                  
                                   
           
           
                                                      
                                                                               
                                                                                
                                                                              
                                            
                                                      
                                                                               
                              
                       
           
         
       
     
                     
                  
            
                   
                    
              
                    
       
     
                
                  
                          
                      
                
                       
                        
                  
                        
           
         
              
                    
       
            
                  
     
     
                                                
                                                       
                                                
                                                        
                      
     
               
              
            
                     
            
                                
                                         
       
                           
           

                                          
           

         
                                          
       
     
     
                       
                   
                    
                  
     
                     
       
                                 
       
                       
         

                             
         

       
     
               
       
                             
       
             
     
    {
                {
                                     ;
        {
          tmp___8 = fileno(stdout);
          tmp___9 = fileno(stdin);
          tmp___10 = (*work)(tmp___9, tmp___8);
        }
                            
                 
         
                          
                           
                
                            
                             
                  
                                  
                               
             
           
         
         
	                           
	 
                         
                 
         
                       
      }
    while_break: /* CIL Label */;
    }
                  
                 
         

                                                    
                                                            
         

              
                          
           

                                                                          
                                  
                                                      
                                                           
           

         
       
     
           
  }
}
static void treat_file(char *iname) {
  int cflag;
  int tmp;
  int tmp___0;
  struct stat st;
  int tmp___1;
  int tmp___2;
  int tmp___3;
  int tmp___4;
  char const *tmp___7;
  size_t tmp___8;
  char const *tmp___9;
  size_t tmp___10;
  int tmp___11;
  int tmp___12;
                    
                    
                    
                    
                    

  {
     
                                             
     
                   
       
                          
                      
                          
       
             
     
    {
      tmp___0 = get_istat(iname, &istat);
    }
                       
             
     
                                             
                      
         

                     
                           
                                  
         

              
                     
           

                    
                                                  
                           
                                                                               
                                  
           

         
                             
                        
         
       
             
     
                                                
                   
         

                  
                                                
                                                                                
                                                                          
                                
         

       
                           
                      
       
             
     
                               
                       
                     
                       
                                       
                            
                    
                            
             
             
                                                        
                                                                             
                                                                               
                                                                          
             
           
                               
                          
           
                 
         
       
     
                               
                  
                  
                        
              
                                          
       
            
                                        
     
                    
                  
                    
           

                                                      
                                                              
           

                
                      
         
              
                    
       
           {
    _L___0 : {
	tmp___2 = make_ofname();
      }
                         
               
       
    }
               {
                       {
        tmp___3 = 0;
      }       
                    
       
    }       
                  
     
    {
      ifd = open((char const *)(ifname), tmp___3, 384);
    }
                    
       
                                                  
                                                                   
                                       
                      
       
             
     
     
                   
                  
     
    if (decompress) {
      {
        method = get_method(ifd);
      }
                       
         
                     
         
               
       
    }
    if (list) {
      {
        do_list(ifd, method);
                   
      }
      return;
    }
                    
       

                             
       

           {
      {
	tmp___4 = create_outfile();
      }
                         
               
       
                        
                             
                         
                         
               

                                                          
                                   
                                                                              
                                                  
               

             
           
         
       
    }
                          
                                
     
                  
       
                                                  
       
                                
                     
              
         
	                                           
	 
                                
                         
                
                           
         
                          
       
       
                                                  
                                                                             
       
     
    {
                {
                                     ;
        {
	  tmp___11 = (*work)(ifd, ofd);
	}
                            
                      
                           
         
                          
                           
                
                            
                             
                  
                                  
                               
             
           
         
         
	                           
	 
                         
                           
         
                       
      }
    while_break: /* CIL Label */;
    }
     
                 
     
                     
       
                              
       
                     
         

                        
         

       
     
                       
                       
         

                                         
         

       
             
     
                  
                 
         

                                                    
                                                          
         

              
                         
           

                                                                           
                                  
           

                
           

                                                                          
                                  
           

         
       
                  
                         
           

                                                      
                                                                            
                            
           

         
       
       
                                                  
                                                       
       
     
                    {
       

        copy_stat(&istat);
       

    }
           
  }
}
static int create_outfile(void) {
  struct stat ostat;
  int flags___0;
  int tmp;
  int tmp___0;
  int tmp___1;
                   

  {
    flags___0 = 193;
                
                       
                        
       
     
    {
                {
                                     ;
         
	                       
	 
                       
           
                       
           
                     
         
        {
                            
          ofd = open((char const *)(ofname), flags___0, 384);
        }
                        
           
                                           
                       
                          
           
                     
         
        {
	  tmp___0 = fstat(ofd, &ostat);
	}
        if (tmp___0 != 0) {
          {
            fprintf((FILE * /* __restrict  */) stderr,
                    (char const * /* __restrict  */) "%s: ", progname);
                                           
            close(ifd);
            close(ofd);
            unlink((char const *)(ofname));
                          
          }
                     
        }
        {
	  tmp___1 = name_too_long(ofname, &ostat);
	}
        if (!tmp___1) {
          return (0);
        }
        if (decompress) {
                      {
             

              fprintf(
                  (FILE * /* __restrict  */) stderr,
                  (char const
                       * /* __restrict  */) "%s: %s: warning, name truncated\n",
                  progname, ofname);
             

          }
                               
                          
           
                     
        }
        {
          close(ofd);
          unlink((char const *)(ofname));
                               
        }
      }
                                ;
    }
  }
}
static int do_stat(char *name, struct stat *sbuf) {
  int *tmp;
  int tmp___0;
  int tmp___1;

  {
     
                               
               
     
                     
                   
         
                                                               
                                                                  
         
                         
       
     
    {
      tmp___1 = stat((char const * /* __restrict  */)name,
                     (struct stat * /* __restrict  */) sbuf);
    }
    return (tmp___1);
  }
}
static char *known_suffixes[9] = {
    z_suffix,       (char *)".gz",  (char *)".z",
    (char *)".taz", (char *)".tgz", (char *)"-gz",
    (char *)"-z",   (char *)"_z",   (char *)((void *)0)};
static char *get_suffix(char *name) {
  int nlen;
  int slen;
  char suffix[33];
  char **suf;
  int tmp;
  int s;
  int tmp___0;
  int tmp___1;
                    

  {
    {
      suf = known_suffixes;
                                                  
    }
                   
            
     
    {
      nlen = (int)strlen((char const *)name);
    }
                    {
       

        strcpy((char * /* __restrict  */)(suffix),
               (char const * /* __restrict  */)name);
       

    }       
       

                                                  
                                                                           
       

     
    {
                     
      slen = (int)strlen((char const *)(suffix));
    }
    {
                {
                                     ;
        {
          tmp___0 = (int)strlen((char const *)*suf);
          s = tmp___0;
        }
                      {
                                                 {
            {
              tmp___1 = strcmp((char const *)((suffix + slen) - s),
                               (char const *)*suf);
            }
            if (tmp___1 == 0) {
              return ((name + nlen) - s);
            }
          }
        }
              
                                                                   
                           
         
      }
    while_break: /* CIL Label */;
    }
    return ((char *)((void *)0));
  }
}
static char *suffixes[6] = {z_suffix,     (char *)".gz", (char *)".z",
                            (char *)"-z", (char *)".Z",  (char *)((void *)0)};
static int get_istat(char *iname, struct stat *sbuf) {
  int ilen;
  char **suf;
  char *s;
  int tmp;
  int *tmp___0;
  int tmp___1;
  int tmp___2;

  {
    {
                     
      strcpy((char * /* __restrict  */)(ifname),
             (char const * /* __restrict  */)iname);
      tmp = do_stat(ifname, sbuf);
    }
                  {
      return (0);
    }
                      
       
                                       
                      
       
                 
            
       
	                             
       
                          
         
                                         
                        
         
                   
       
     
     
                             
     
                                                         
       
                                       
                      
       
                 
     
     
                                                 
                                                        
     
                       
            
     
     
                 
                                      
         
                   
                                                    
                                                    
                                          
         
                           
                     
         
                                    
              
                                                                   
                           
         
       
                                 
     
     
                                                
                                                         
                                     
                    
     
               
  }
}
static int make_ofname(void) {
  char *suff;
  int tmp;
  int tmp___0;
                   
                   

  {
    {
      strcpy((char * /* __restrict  */)(ofname),
             (char const * /* __restrict  */)(ifname));
      suff = get_suffix(ofname);
    }
    if (decompress) {
                                                              
                         
                     
                       
                  
                       
                         
             
           
         
                      
                  
                
                           
                         
               
                           
                 

                          
                                                        
                                                                                
                                                                      
                                        
                 

               
                                   
                              
               
             
           
         
                   
       
      {
                     
        tmp = strcmp((char const *)suff, ".tgz");
      }
                     
         

                                                
                                                          
         

             {
        {
	  tmp___0 = strcmp((char const *)suff, ".taz");
	}
                           
           

                                                  
                                                            
           

               {
          *suff = (char)'\000';
        }
      }
    } else {
                                                              
                      
           

                                                      
                                                                             
                                                                             
                                            
           

                
                           
                         
               

                        
                                                      
                                                                             
                                                                             
                                            
               

             
           
         
                             
                        
         
                   
             {
         

                             
          strcat((char * /* __restrict  */)(ofname),
                 (char const * /* __restrict  */)(z_suffix));
         

      }
    }
               
  }
}
static int get_method(int in) {
  uch flags___0;
  char magic[2];
  ulg stamp;
  unsigned int tmp;
  int tmp___0;
  int tmp___1;
  unsigned int tmp___2;
  int tmp___3;
  int tmp___4;
  unsigned int tmp___5;
  int tmp___6;
  int tmp___7;
  unsigned int tmp___8;
  int tmp___9;
  int tmp___10;
  unsigned int tmp___11;
  int tmp___12;
  int tmp___13;
  unsigned int tmp___14;
  int tmp___15;
  int tmp___16;
  unsigned int tmp___17;
  int tmp___18;
  int tmp___19;
  unsigned int tmp___20;
  int tmp___21;
  int tmp___22;
  unsigned int tmp___23;
  int tmp___24;
  int tmp___25;
  unsigned int tmp___26;
  int tmp___27;
  int tmp___28;
  unsigned int tmp___29;
  unsigned int tmp___30;
  unsigned int part;
  unsigned int tmp___31;
  int tmp___32;
  int tmp___33;
  unsigned int tmp___34;
  int tmp___35;
  int tmp___36;
  unsigned int len;
  unsigned int tmp___37;
  int tmp___38;
  int tmp___39;
  unsigned int tmp___40;
  int tmp___41;
  int tmp___42;
  unsigned int tmp___43;
  unsigned int tmp___44;
  char c;
  unsigned int tmp___45;
  int tmp___46;
  char *p;
  char *tmp___47;
  char *base;
  unsigned int tmp___48;
  int tmp___49;
  int tmp___50;
  char *tmp___51;
  unsigned int tmp___52;
  int tmp___53;
  int tmp___54;
  int tmp___55;
  int tmp___56;
  int tmp___57;
  int tmp___58;
  int tmp___59;
  int tmp___60;
  int tmp___61;
  int tmp___62;
                    
                    

  {
                
                      
                             
                      
                  
                                    
                
           

                                    
                              
           

         
                                 
                             
                          
                  
                                        
                
           

                                    
                              
           

         
                                 
              
                
       
           {
    _L:
                           
                        
                
                                      
             {
         

          tmp___6 = fill_inbuf(0);
          tmp___7 = tmp___6;
         

      }
      magic[0] = (char)tmp___7;
                          {
        tmp___8 = inptr;
        inptr++;
        tmp___10 = (int)inbuf[tmp___8];
      }       
         

                                  
                             
         

       
      magic[1] = (char)tmp___10;
    }
    {
                  
      part_nb++;
                        
                      
      tmp___61 =
          memcmp((void const *)(magic), (void const *)"\037\213", (size_t)2);
    }
    if (tmp___61 == 0) {
      goto _L___4;
    } else {
      {
        tmp___62 =
            memcmp((void const *)(magic), (void const *)"\037\236", (size_t)2);
      }
                         {
      _L___4:
                            {
                           
                  
                                          
        }       
           

                                     
                                
           

         
                          
                          
           
                    
                                                  
                                                                                
                                                                               
                                          
                          
           
                      
         
                      
                             
                           
                  
                                          
                
           

                                     
                                
           

         
                                  
                                         
           
                    
                                                  
                                                                              
                                                                           
                                  
                          
           
                      
         
                                        
           
                    
                                                  
                           
                                                                               
                                                                           
                                  
                          
           
                           
                        
           
         
                                          
           
                    
                                                  
                                                                                
                                                                           
                                                  
                          
           
                           
                        
           
         
                             
                           
                  
                                          
                
           

                                     
                                
           

         
                              
                             
                           
                  
                                          
                
           

                                     
                                
           

         
                                    
                             
                           
                  
                                          
                
           

                                     
                                
           

         
                                     
                             
                           
                  
                                          
                
           

                                     
                                
           

         
                                     
                           
                         
                                     
           
         
                             
                           
                  
                
           

	                  
	   

         
                             
                           
                  
                
           

	                  
	   

         
                                        
                               
                             
                    
                                            
                  
             

                                       
                                  
             

           
                                        
                               
                             
                    
                                            
                  
             

                                       
                                  
             

           
                                              
                        
             

                      
                                                    
                                                                              
                                          
             

           
         
                                        
                               
                             
                    
                                            
                  
             

                                       
                                  
             

           
                                       
                               
                             
                    
                                            
                  
             

                                       
                                  
             

           
                                             
                        
             

                                                        
                                                                                
                                                                            
                                             
             

           
           
                       
                                            
                             
                    
                              
                                 
               
                                   
                                 
                        
                      
                 

		                
		 

               
             
                                       
           
         
                                        
                        
                        
                  
                            
                          
                            
                      
                            
               
                    
                   
                                
                        
                           
                                                    
                                       
                                     
                            
                                              
                          
                     

                                               
                                         
                     

                   
                                       
                                         
                   
                 
                                               
               
                      
                 
                                              
                               
                           
                 
                 
                             
                                                      
                                         
                                       
                              
                                                      
                            
                       

                                                 
                                            
                       

                     
                                        
                                 
                        
                                              
                                           
                     
                                           
                                                                   
                       

                                                                                
                       

                     
                   
                                                 
                 
                            
                             
                             
                   
                 
               
             
           
         
                                         
           

                       
                                                
                                   
                                 
                        
                                                
                      
                 

                                           
                                      
                 

               
                                     
                                     
               
             
                                           
           

         
                          {
          header_bytes = (long)((unsigned long)inptr + 2UL * sizeof(long));
        }
      }       
         
                                                                              
                                       
         
                            
                            
             
                                                                
                                                                       
             
                                
               
                           
                              
                                             
               
                                  
                            
               
                              
                    
                          
             
                  
                        
           
                
                  
                                                                            
                                       
         
                              
                           
                       
                  
             
                                                                                
                                           
             
                                
                            
                         
                              
                    
               
                                                        
                                                                       
               
                                  
                              
                           
                                
                      
                            
                                  
                                
                                 
                                   
                                 
                                      
                     
                   
                 
               
             
           
         
       
    }
                     {
      return (method);
    }
                       
       
                
                                              
                                                                              
                              
                      
       
                  
            
                   
         

                  
                                                
                                                                             
                                                                            
                                
         

       
                           
                      
       
                  
     
  }
}
static int first_time = 1;
static char *methods[9] = {(char *)"store", (char *)"compr", (char *)"pack ",
                           (char *)"lzh  ", (char *)"",      (char *)"",
                           (char *)"",      (char *)"",      (char *)"defla"};
static void do_list(int ifd___0, int method___0) {
  ulg crc___1;
  char *date;
  __off_t tmp;
  uch buf[8];
  ssize_t tmp___0;
  char *tmp___1;
                   

  {
    if (first_time) {
                           {
                       
                      
           

                    
                                                                                
           

         
                    {
           

            printf(
                (char const * /* __restrict  */) "compressed  uncompr. ratio "
                                                 "uncompressed_name\n");
           

        }
      }       
                
       
    } else {
    _L:
                          {
                             
                 
                
                                
                   
           
         
                      
           

                                                                                
                                                                   
                                        
           

                
                       
             

                                                                             
                                
             

           
         
         
                                                                         
                                
                                                                 
         
               
      }
    }
                        
                    
                          
                         {
                        {
        {
          tmp = lseek(ifd___0, (off_t)-8, 2);
          bytes_in = tmp;
        }
                             {
          {
            bytes_in += 8L;
            tmp___0 = read(ifd___0, (void *)((char *)(buf)), sizeof(buf));
          }
                                                      
             

                           
             

           
                                                                           
                                                           
                                                                 
                            
          bytes_out = (long)((ulg)((int)((ush) * ((buf + 4) + 0)) |
                                   ((int)((ush) * ((buf + 4) + 1)) << 8)) |
                             ((ulg)((int)((ush) * (((buf + 4) + 2) + 0)) |
                                    ((int)((ush) * (((buf + 4) + 2) + 1)) << 8))
                              << 16));
        }
      }
    }
     
                                                     
                         
                                  
     
                  
       

                                                                  
                                                   
       

     
    {
      printf((char const * /* __restrict  */) "%9ld %9ld ", bytes_in,
             bytes_out);
    }
                          
                     
                        
                               
                           
            
                           
                             
       
     
                           
                      
                        
                               
                           
            
                            
                               
       
     
    {
      display_ratio(bytes_out - (bytes_in - header_bytes), bytes_out, stdout);
      printf((char const * /* __restrict  */) " %s\n", ofname);
    }
           
  }
}
static int same_file(struct stat *stat1, struct stat *stat2) {
  int tmp;

  {
                                        {
                                          {
        tmp = 1;
      }       
                
       
    }       
              
     
    return (tmp);
  }
}
static int name_too_long(char *name, struct stat *statb) {
  int s;
  int tmp;
  char c;
  struct stat tstat;
  int res;
  int tmp___0;
  int tmp___1;
  int tmp___2;
                    

  {
    {
      tmp = (int)strlen((char const *)name);
      s = tmp;
      c = *(name + (s - 1));
      tstat = *statb;
      *(name + (s - 1)) = (char)'\000';
      tmp___0 = stat((char const * /* __restrict  */)name,
                     (struct stat * /* __restrict  */)(&tstat));
    }
                       
       
                                           
       
                    
                    
              
                    
       
           {
      tmp___2 = 0;
    }
    res = tmp___2;
    *(name + (s - 1)) = c;
    return (res);
  }
}
static void shorten_name(char *name) {
  int len;
  char *trunc;
  int plen;
  int min_part;
  char *p;
  int tmp;
  char *tmp___0;
                    
                    
                    
                    
                    

   
     
                                  
                   
                                            
     
                     
                     
         

                                          
         

       
                                         
             
     
     
                           
     
                                                         
       

                                                 
       

     
                      
                       
                  
       
                                                    
       
                     
         
                                                   
                                                          
         
               
       
     
     
                 
                                      
         
	                                       
	 
                
              
                
                   
         
         
                     
                                              
                      
                                   
             
             
                                                        
                        
             
                                  
                            
             
                     
                  
             
           
                                         
         
                                                                 
                     
                                 
                             
           
                
                           
         
       
                                 
     
                                                             
       
                   
                                            
                                      
                          
                  
                          
                                 
           
         
                                       
       
              
            
       
	                                                     
       
                                                               
         

                                                          
         

       
                                   
                
       
     
     
                                             
                                                         
     
           
   
}
static int check_ofname(void) {
  struct stat ostat;
  int *tmp;
  int *tmp___0;
  int tmp___1;
  int tmp___2;
  int tmp___3;
  char const *tmp___4;
  int tmp___5;
  int tmp___6;
  char response[80];
  int tmp___7;
  int tmp___8;
  int tmp___10;
  unsigned short const **tmp___11;
  int tmp___12;
                    
                    
                    
                    
                    

  {
     
                               
               
     
     
                 
                                      
         
                                                                  
                                                                    
         
                              
                           
         
         
	                               
	 
                             
                     
         
         
	                       
	 
       
                                 
     
                      
       
                                                
       
                    
         
                               
                                                                  
                                                                    
         
                           
                     
         
       
     
     
                                          
     
                  
       
                                                                         
       
                         
                         
                         
                
                       
         
         
                                                    
                                                                               
                                                                   
                                             
         
              
         

                                                    
                             
                                                                                
                                            
         

       
                    
                 
     
                 
       
                                                    
                                                     
                                                  
                                                                          
                                  
       
                       
         
                                  
                                    
         
                      
           

                                                      
                                                                       
                                                                             
                           
                                                       
                                                
                                                    
           

         
       
       
	                           
       
                                                               
                                                
              
                                    
       
                            
         
                                                    
                                                                          
         
                             
                        
         
                   
       
     
     
                                                   
                                                
     
                   
       
                                                  
                                                                   
                                       
                      
       
                 
     
    return (0);
  }
}
static void reset_times(char *name, struct stat *statb) {
  struct utimbuf timep;
  int tmp;

   
     
                                           
                                            
                                                                        
     
              
                                                   
                     
           

                                                      
                                                                       
           

         
                             
                        
         
                     
           

                                           
           

         
       
     
           
   
}
static void copy_stat(struct stat *ifstat) {
  int tmp;
  int tmp___0;

  {
                     
                             
                                                   
                                              
                            
             

                      
                                                    
                                                                               
                          
             

           
         
       
     
     
                                  
                                                                   
     
              
                   
         

                                                    
                                                                     
         

       
                           
                      
       
                   
         

                                         
         

       
     
    {
                                                                    
                        
                                                   
      tmp___0 = unlink((char const *)(ifname));
    }
                  
                   
         

                                                    
                                                                     
         

       
                           
                      
       
                   
         

                                         
         

       
     
           
  }
}
static void treat_dir(char *dir) {
  dir_type *dp;
  DIR *dirp;
  char nbuf[1024];
  int len;
  int tmp;
  int tmp___0;
  int tmp___1;
  size_t tmp___2;
                    

   
     
                                        
     
                                                            
       
                                                  
                                                                       
                               
                      
       
             
     
     
                 
                                      
         
	                     
	 
                                                                 
                           
         
         
	                                                
	 
                       
                              
                
           
	                                                       
	   
                             
                                
           
         
         
                                               
                                                       
         
                                              
           
                                                    
                                                        
           
                         
                          
                  
                                      
           
           
                                                          
                                                                 
                             
           
                
           

                                                      
                               
                                                                               
                                               
                          
           

         
       
                                 
     
     
                     
     
           
   
}
static int in_exit = 0;
static void do_exit(int exitcode) {

   

                  
       

                       
       

     
                
                                                           
       

                          
                                  
       

     
                                                            
       

                                     
                                    
       

     
     
                     
     
   

}
void abort_gzip(void) {

   

                        
       

                   
                                       
       

     
     
                 
     
           
   

}
